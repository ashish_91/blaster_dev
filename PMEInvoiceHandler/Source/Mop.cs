﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.Json;
using Microsoft.Extensions.Logging;

namespace PMEInvoiceHandler.Source
{
    public static class Mop
    {
        public static void SendEmail(
            IEnumerable<string> receivers,
            string subject,
            string body,
            string footerHTML,
            ILogger _logger
            )
        {
            foreach (string to in receivers)
            {
                if (string.IsNullOrWhiteSpace(to))
                    continue;

                try
                {
                    string recipient = to.Trim();

                    if (Mop.IsEmailValid(recipient))
                    {
                        string fromAddress = "billing@primemedicalalert.com";
                        string password = "Seniorplanning1!";

                        string resp = Mop.SendGmail(
                            fromAddress: fromAddress,
                            toAddress: recipient,
                            password: password,
                            subject: subject,
                            body: body,
                            isBodyHtml: true,
                            _logger: _logger,
                            attachments: null,
                            addFooter: true,
                            footerHTML: footerHTML,
                            makeNewRowsBR: true
                            );

                        if (resp != null)
                            throw new Exception(resp);
                    }
                    else
                    {
                        throw new Exception("NOT VALID EMAIL ADDRESS");
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, null, null);
                }
            }

        }

        public static bool IsEmailValid(string email, bool treatEmptyAsOkay = false)
        {
            if (String.IsNullOrWhiteSpace(email)) return treatEmptyAsOkay;
            if (email.Length < 6) return false;
            if (email.IndexOf("@") <= 0) return false;
            if (email.IndexOf(".") <= 0) return false;
            if (email.EndsWith(".")) return false;
            if (email.EndsWith("@")) return false;
            if (email.StartsWith("@")) return false;

            return true;
        }

        public static void NotifyMeOnException(string module, Exception ex, ILogger _logger, bool raiseOnError, string addInfo)
        {
            try
            {
                string res = Mop.SendGmail(
                    "iwm2stuff@gmail.com",
                    "iwm2stuff@gmail.com",
                    null,
                    $"ERROR__PrimeMedicalAlertsApp {module}",
                    //addInfo + "      \n\n\n\n\n" + JsonSerializer.Serialize(ex, new JsonSerializerOptions 
                    //{ 
                    //    WriteIndented = true, 
                    //    MaxDepth = 25, 
                    //    ReferenceHandling = System.Text.Json.Serialization.ReferenceHandling.Preserve,

                    //}),

                    addInfo + "      \n\n\n\n\n" + Newtonsoft.Json.JsonConvert.SerializeObject(ex, Newtonsoft.Json.Formatting.Indented),
                    false,
                    new List<string>(),
                    _logger,
                    false,
                    null,
                    false
                    );

                if (!string.IsNullOrWhiteSpace(res))
                {
                    throw new Exception(res);
                }
            }
            catch (Exception ex2)
            {
                _logger.LogError(ex2, null, null);
                if (raiseOnError)
                {
                    throw;
                }
            }
        }

        public static string SendGmail(
            string fromAddress,
            string toAddress,
            string password,
            string subject,
            string body,
            bool isBodyHtml,
            IEnumerable<string> attachments,
            ILogger _logger,
            bool addFooter,
            string footerHTML,
            bool makeNewRowsBR
            )
        {
            if (attachments == null) attachments = new string[0];

            _logger.LogInformation("START Sending email");
            _logger.LogInformation("  from address:" + fromAddress);
            _logger.LogInformation("  to address:" + toAddress);
            //_logger.LogInformation("  password:" + (password == null ? "" : Crypto.AES.Encrypt(password, "T1eM2GaS3cr#$tPassw@wrd")));
            _logger.LogInformation("  subject:" + subject);
            _logger.LogInformation("  body:" + body);
            _logger.LogInformation("  attachments:" + string.Join(",", attachments));

            try
            {
                var from = new MailAddress(fromAddress);
                var to = new MailAddress(toAddress);

                if (fromAddress == "iwm2stuff@gmail.com")
                    password = "qapftrkwcpwkyqko";

                using (var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress, password),
                    Timeout = 20000
                })
                {

                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = subject,
                        Body = body,
                        IsBodyHtml = isBodyHtml,
                    })
                    {
                        if (attachments != null && attachments.Count() > 0)
                        {
                            foreach (var attachment in attachments)
                            {
                                message.Attachments.Add(new Attachment(attachment));
                            }
                        }

                        if (makeNewRowsBR)
                        {
                            message.Body = message.Body
                                .Replace("\n\r", "<br>")
                                .Replace("\r\n", "<br>")
                                .Replace("\n", "<br>")
                                .Replace("\r", "<br>");
                        }

                        if (addFooter)
                        {
                            message.Body += footerHTML;
                        }


                        _logger.LogInformation("Before send");
                        smtp.Send(message);
                        _logger.LogInformation("After send");

                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, null, null);
                _logger.LogInformation("ERROR: " + ex.Message);
                return ex.Message;
            }
        }
    }
}
