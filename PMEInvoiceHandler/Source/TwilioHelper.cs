﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using Twilio;
using Twilio.Rest.Fax.V1;

namespace PMEInvoiceHandler.Source
{
    public static class TwilioHelper
    {
        private const string TwilioAccountSid = "AC573369173bf462cca7538f4f1a9098ba"; // Your Account SID from www.twilio.com/console
        private const string TwilioАuthToken = "37087fdffe051fd25a1187207ec6d8cf";  // Your Auth Token from www.twilio.com/console
        public const string TwilioFaxNo = "+14804481197";
        public const string TwilioSmsNo = "+14804481197";

        public static string SendFax(string to, string uri, NLog.Logger logger, ref FaxResource info)
        {
            info = null;
            try
            {
                logger.Info($"Sending to: {to} ");
                logger.Info($"uri: {uri} ");

                TwilioClient.Init(TwilioAccountSid, TwilioАuthToken);

                info = FaxResource.Create(
                    from: TwilioFaxNo,
                    to: to,
                    mediaUrl: new Uri(uri)
                );

                logger.Info("Finished, with status: " + info.Status);

                return null;
            }
            catch(Exception ex)
            {                
                logger.Error(ex);
                return ex.Message;
            }

        }

        public static void SendSms(ILogger logger, string to, string message)
        {
            logger.LogInformation("SendSms");
            logger.LogInformation($"    TwilioAccountSid: {TwilioAccountSid}");
            logger.LogInformation($"    TwilioАuthToken: {TwilioАuthToken}");
            logger.LogInformation($"    TwilioSmsNo: {TwilioSmsNo}");
            logger.LogInformation($"    to: {to}");
            logger.LogInformation($"    message: {message}");

            logger.LogInformation("  Creating TwilioRestClient");

            TwilioClient.Init(TwilioAccountSid, TwilioАuthToken);

            logger.LogInformation("  Calling SendMessage");
            var msg = Twilio.Rest.Api.V2010.Account.MessageResource.Create(
                to: new Twilio.Types.PhoneNumber(to),
                from: new Twilio.Types.PhoneNumber(TwilioSmsNo),
                body: message
            );
            logger.LogInformation("  resp: " + JsonConvert.SerializeObject(msg));

            if (msg.ErrorMessage != null)
            {
                string error = msg.ErrorMessage;
                logger.LogError(error);
                throw new Exception(error);
            }
        }
    }
}