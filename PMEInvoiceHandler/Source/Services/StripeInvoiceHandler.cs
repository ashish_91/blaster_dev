﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;

namespace PMEInvoiceHandler.Source.Services
{
    public class StripeInvoiceHandler
    {
        private ILogger<StripeInvoiceHandler> _logger;
        private IConfiguration _configuration;

        public StripeInvoiceHandler(ILogger<StripeInvoiceHandler> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public string HandleInvoices(bool skipEmailingForNotPayedInvoices)
        {
            //Get invoices
            List<Invoice> invoices = Stripe.GetInvoicess(
                _configuration.GetValue<string>("stripe:privateKey"),
                _configuration.GetValue<int>("getInvoicesDaysAgo"));

            List<Subscription> canceledSubscriptions = Stripe.GetAllCanceledSubscriptions(
                _configuration.GetValue<string>("stripe:privateKey"));

            var reportText = new StringBuilder();

            reportText.AppendLine(); reportText.AppendLine(); reportText.AppendLine();

            reportText.AppendLine($"=== Handling invoices ===");
            reportText.AppendLine($"Invoices count downloaded: {invoices.Count}");

            //Process them
            foreach (var invoice in invoices)
            {
                _logger.LogTrace(JsonSerializer.Serialize(invoice));

                try
                {
                    reportText.AppendLine($"invoice {invoice.Id} created on {invoice.Created} | payed: {invoice.Paid} | toPay: ${invoice.AmountRemaining / 100m} | payed sum: {invoice.AmountPaid / 100m}");

                    bool isNotPayed = DetermineIsInvoicePayed(invoice);
                    bool isSubscriptionCanceled = canceledSubscriptions.Any(s => s.Id.Equals(invoice.SubscriptionId));

                    if (isSubscriptionCanceled)
                    {
                        reportText.AppendLine($"     => SUBSCR is CANCELED");
                    } else  if (isNotPayed && !skipEmailingForNotPayedInvoices)
                    {
                        //not payed at all, should notify user
                        RemindUserToPayInvoice(reportText, invoice);
                    }
                    else if (invoice.Paid)
                    {
                        reportText.AppendLine($"     => payed");
                    }
                    else
                    {
                        reportText.AppendLine($"    => skip this invoice, dueDate: {invoice.DueDate}, STATUS: {invoice.Status}");
                    }

                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, null, null);
                    Mop.NotifyMeOnException("IMPORTANT!!! PAYMENT MAY NOT BE REGISTERED!!! Stripe HandleUnpayedInvoices", ex, _logger, false, null);
                }
            }

            Mop.SendEmail(
                new string[] { "iwm2stuff@gmail.com"},
                "PMA Alert Report",
                reportText.ToString(),
                System.IO.File.ReadAllText("./Documents/Footer.html"),
                _logger
                );

            _logger.LogInformation(reportText.ToString());
            return reportText.ToString();
        }

        private void RemindUserToPayInvoice(StringBuilder reportText, Invoice invoice)
        {
            reportText.AppendLine($"     => not payed, emailed");
            string automaticText = invoice.NextPaymentAttempt.HasValue
            ? $" The next automatic attempt will be on {invoice.NextPaymentAttempt.Value.ToString("M'/'d'/'yyyy")}."
            : " "
            ;
            string emailText = $@"
Hello, 
Your account is currently overdue with Prime Medical Alert in the amount of ${invoice.AmountRemaining / 100m}.{automaticText}
<b>Please click through the following link to update your credit card</b> <a href='{invoice.HostedInvoiceUrl}'> by clicking here</a>
You can view the invoice as a pdf <a href='{invoice.InvoicePdf}'>by clicking here</a>
Feel free to call to update billing as well.
";            
            Mop.SendEmail(                
                new string[] { "iwm2stuff@gmail.com" , invoice.CustomerEmail },
                "URGENT: Action Needed Immediately",
                emailText,
                System.IO.File.ReadAllText("./Documents/Footer.html"),
                _logger
                );

            //Send SMS

            try
            {
                string shortURL = ShortifyHelper.MakeShortUrl(_logger, invoice.HostedInvoiceUrl);
                string textMessage = $"URGENT: Your account with Prime Medical Alert is past due. Please update your payment details {shortURL} or call 800 723 6442 ";
                TwilioHelper.SendSms(_logger, invoice.CustomerPhone /*"+359899995650" */, textMessage);
            }
            catch (Exception ex)
            {
                _logger.LogError(1, ex, "remind user with phone: {phone} to pay an invoice", invoice.CustomerPhone);
                Mop.NotifyMeOnException("SMS SEND NOT Payed invoice", ex, _logger, false, "");
            }
        }

        private static bool DetermineIsInvoicePayed(Invoice invoice)
        {
            bool isAutomaticInvoice;

            if (invoice.BillingReason == "manual") isAutomaticInvoice = false;
            else if (invoice.BillingReason == "subscription_create" || invoice.BillingReason == "subscription_cycle") isAutomaticInvoice = true;
            else throw new Exception($"Unsupported billing reason: {invoice.BillingReason}");

            switch (isAutomaticInvoice)
            {
                case true:
                    return invoice.Attempted
                           && invoice.AttemptCount >= 1
                           && !invoice.Paid
                           && invoice.AmountRemaining > 0;

                case false:
                    return !invoice.Paid
                           && (invoice.DueDate ?? DateTime.MaxValue) < DateTime.Now.AddDays(1);

                default: throw new NotImplementedException($"Unsupported billing reason: {invoice.BillingReason}");
            }
        }
    }
}