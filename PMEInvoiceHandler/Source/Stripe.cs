﻿using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PMEInvoiceHandler.Source
{
    public static class Stripe
    {
        public static List<Subscription> GetAllCanceledSubscriptions(string apiKey)
        {
            StripeConfiguration.ApiKey = apiKey;

            var service = new SubscriptionService();
            return service.ListAutoPaging(new SubscriptionListOptions()
            {
                Status = SubscriptionStatuses.Canceled
            }).ToList();
        }

        public static List<Invoice> GetInvoicess(string apiKey, int daysAgo)
        {
            StripeConfiguration.ApiKey = apiKey;

            var service = new InvoiceService();
            return service.ListAutoPaging(new InvoiceListOptions()
            {
                Paid = false,                
                Created = new AnyOf<DateTime?, DateRangeOptions>(new DateRangeOptions()
                {
                    GreaterThanOrEqual = DateTime.Now.AddDays(-daysAgo)
                })
            }).ToList();

            //incomplete, incomplete_expired, trialing, active, past_due, canceled, or unpaid
            //Possible values are incomplete, incomplete_expired, trialing, active, past_due, canceled, or unpaid.
            //For collection_method = charge_automatically a subscription moves into incomplete if the initial payment attempt fails. 
            //A subscription in this state can only have metadata and default_source updated. Once the first invoice is paid, the subscription moves into an active state.
            //If the first invoice is not paid within 23 hours, the subscription transitions to incomplete_expired.This is a terminal state, the open invoice will be 
            //voided and no further invoices will be generated.
            //A subscription that is currently in a trial period is trialing and moves to active when the trial period is over.
            //If subscription collection_method = charge_automatically it becomes past_due when payment to renew it fails and canceled or unpaid(depending on your subscriptions settings) 
            //when Stripe has exhausted all payment retry attempts.
            //If subscription collection_method = send_invoice it becomes past_due when its invoice is not paid by the due date, and canceled or unpaid if it is still not paid 
            //by an additional deadline after that.Note that when a subscription has a status of unpaid, no subsequent invoices will be attempted(invoices will be created, 
            //but then immediately automatically closed).After receiving updated payment information from a customer, you may choose to reopen and pay their closed invoices.
        }
    }
}
