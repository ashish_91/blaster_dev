﻿using Microsoft.Extensions.DependencyInjection;
using NLog.Extensions.Logging;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using PMEInvoiceHandler.Source.Services;

namespace PMEInvoiceHandler
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceProvider serviceProvider = ConfigureServices();

            var invoiceHandler = serviceProvider.GetService<StripeInvoiceHandler>();

            string report = invoiceHandler.HandleInvoices(false);
        }

        private static ServiceProvider ConfigureServices()
        {
            var configuration = new ConfigurationBuilder()
                            .AddJsonFile("pme-invoice-handler-settings.json")
                            .Build();

            var services = new ServiceCollection()
                .AddScoped<StripeInvoiceHandler>()

                .AddSingleton<IConfiguration>(configuration)
                .AddLogging(loggingBuilder =>
                {
                    loggingBuilder.AddNLog();
                    loggingBuilder.SetMinimumLevel(LogLevel.Trace);
                });

            ServiceProvider serviceProvider = services.BuildServiceProvider();
            return serviceProvider;
        }
    }
}
