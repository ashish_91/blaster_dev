﻿using NLog;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;

namespace AssistedLivingFacilityBlaster
{
    public static class Helper
    {
        private static Logger logger = LogManager.GetLogger("helper");

        private const string TempDirName = "temp";

        private const string BitLyAccessToken = "d245156860b127b6e89d7f618062a1338d9f7b03";

        //private const string TwilioAccountSid = "ACec7ce61022ec3e370f39ad98534e5103"; // Your Account SID from www.twilio.com/console
        //private const string TwilioАuthToken = "95dfe5ec63ec5c69f06d9a62ec9fcb54";  // Your Auth Token from www.twilio.com/console

        private const string TwilioAccountSid = "AC573369173bf462cca7538f4f1a9098ba"; // Your Account SID from www.twilio.com/console
        private const string TwilioАuthToken = "37087fdffe051fd25a1187207ec6d8cf";  // Your Auth Token from www.twilio.com/console

        //public const string TwilioSmsNo = "+15136574215";
        public const string TwilioSmsNo = "+14804481197";

        private const string YouComSeBaseAddress = "http://you.com.se/";
        private const string YouComSeShortifyAddress = "http://you.com.se/api/api/shortify/this";

        public class PatientTemplateData
        {
            public string PATIENT_FIRST;
            public string PATIENT_LAST;

            public string POC_FIRST;
            public string POC_LAST;

            public string BUDGET;
            public string BUDGET_FOR_AL;

            public int PATIENT_AGE;
            public string PATIENT_STREET;
            public string PATIENT_CITY;
            public string PATIENT_STATE;
            public string PATIENT_ZIP;
            public string PATIENT_EMAIL;
            public string PATIENT_TELEPHONE;
            public string AGENT_NAME;
            public string PATIENT_NOTES;
            public string LIST_OF_COMMUNITIES;

            public Guid patientId;
        }

        private static char[] invalidFileChars = Path.GetInvalidFileNameChars();
        public static string MakeSafeFilename(string filename, char replaceChar = '_')
        {
            foreach (char c in invalidFileChars)
            {
                filename = filename.Replace(c, replaceChar);
            }
            return filename;
        }

        public static PatientTemplateData FillTemplateData(Patients patient, List<Db.FacilityDetails> facilitiesDetails = null)
        {
            Helper.ValidateTemplates();

            string facilitiesInfo = string.Empty;
            if (facilitiesDetails != null)
            {
                string tMain = File.ReadAllText(@".\Templates\ListOfCommunities.txt");
                string tHeader = File.ReadAllText(@".\Templates\ListOfCommunitiesTH.txt");
                string tRow = File.ReadAllText(@".\Templates\ListOfCommunitiesTR.txt");

                string trs = string.Empty;
                foreach (var facility in facilitiesDetails)
                {
                    trs += Helper.ReplaceCommunityTemplate(tRow, facility);
                }

                facilitiesInfo = tMain.Replace("{TH}", tHeader).Replace("{TRS}", trs);
            }

            return new Helper.PatientTemplateData()
            {
                AGENT_NAME = Db.user.RealName,
                POC_FIRST = patient.PocFirstName,
                POC_LAST = patient.PocLastName,
                LIST_OF_COMMUNITIES = facilitiesInfo,
                PATIENT_AGE = patient.Age ?? 0,
                PATIENT_CITY = patient.City,
                PATIENT_EMAIL = patient.Email,
                PATIENT_FIRST = patient.FirstName,
                PATIENT_LAST = patient.LastName,
                PATIENT_STATE = patient.State,
                PATIENT_STREET = patient.Street,
                PATIENT_TELEPHONE = patient.Telephone,
                PATIENT_ZIP = patient.AddrZip,
                PATIENT_NOTES = patient.Notes,
                patientId = patient.Id,
                BUDGET = (patient.BudgetForCareHomes ?? 0).ToString(),
                BUDGET_FOR_AL = (patient.AssistedLivingBudget ?? 0).ToString()

            };
        }

        public static bool IsEmailValid(string email, bool treatEmptyAsOkay = false)
        {
            if (String.IsNullOrWhiteSpace(email)) return treatEmptyAsOkay;
            if (email.Length < 6) return false;
            if (email.IndexOf("@") <= 0) return false;
            if (email.IndexOf(".") <= 0) return false;
            if (email.EndsWith(".")) return false;
            if (email.EndsWith("@")) return false;
            if (email.StartsWith("@")) return false;

            return true;
        }

        public static string GetTempFileName(string fileExtension)
        {
            if (!Directory.Exists(TempDirName))
                Directory.CreateDirectory(TempDirName);

            return Path.Combine(TempDirName, Guid.NewGuid().ToString() + fileExtension);
        }

        public static void StoreToPatientsRegister(PatientTemplateData templateData, string recipient, string message, bool isToPatient, bool isHtml)
        {
            string filesDir = "patient files";
            if (!Directory.Exists(filesDir))
            {
                Directory.CreateDirectory(filesDir);
            }
            string patientNameDir = MakeSafeFilename(templateData.PATIENT_FIRST + " " + templateData.PATIENT_LAST);
            string fileName = MakeSafeFilename(DateTime.Now.ToString("yyyy-MM-dd") + "  to " + (isToPatient ? " PATIENT " : "") + recipient.Trim() + (isHtml ? ".html" : ".txt"));

            string fullPath = Path.Combine(filesDir, patientNameDir);
            if (!Directory.Exists(fullPath)) Directory.CreateDirectory(fullPath);
            File.WriteAllText(Path.Combine(fullPath, fileName), message);
        }

        public static void SendSms(string to, string message)
        {
            logger.Info("SendSms");
            logger.Info($"    TwilioAccountSid: {TwilioAccountSid}");
            logger.Info($"    TwilioАuthToken: {TwilioАuthToken}");
            logger.Info($"    TwilioSmsNo: {TwilioSmsNo}");
            logger.Info($"    to: {to}");
            logger.Info($"    message: {message}");

            logger.Info("  Creating TwilioRestClient");

            Twilio.TwilioClient.Init(TwilioAccountSid, TwilioАuthToken);

            logger.Info("  Calling SendMessage");
            var msg = Twilio.Rest.Api.V2010.Account.MessageResource.Create(
                to: new Twilio.Types.PhoneNumber(to),
                from: new Twilio.Types.PhoneNumber(TwilioSmsNo),
                body: message
            );
            logger.Info("  resp: " + JsonConvert.SerializeObject(msg));

            if (msg.ErrorMessage != null)
            {
                string error = msg.ErrorMessage;
                logger.Error(error);
                throw new Exception(error);
            }
        }

        private static DateTime googlShotenerDateTime = DateTime.Now;        

        private const string YouComSeShortTemplate = "{{ \"pass\": \"UwXD8YXpT2He9fUt9vFydZd8\", \"targetUrl\": \"{0}\"}}";

        public static string MakeShortUrl(string longUrl, int provider)
        {
            logger.Info("MakeShortUrl");
            logger.Info("   url     : " + longUrl);
            logger.Info("   provider: " + provider);

            if (provider == 0)
            {
                using (var web = new WebClient())
                {
                    throw new NotImplementedException("Shortening via bitly");
                    //string urlToDownload = $"https://api-ssl.bitly.com/v3/shorten?access_token={BitLyAccessToken}&longUrl={WebUtility.UrlEncode(longUrl)}";

                    //logger.Info($"  Exec: {urlToDownload}");
                    //string jsonStrResp = web.DownloadString(urlToDownload);
                    //logger.Info($"  resp: {jsonStrResp}");

                    //logger.Info("  Parsing...");
                    //dynamic jObj = JObject.Parse(jsonStrResp);
                    //logger.Info("  done");

                    //if (jObj.status_code != "200")
                    //{
                    //    logger.Error($"  [Exception] Bad status code: {jObj.status_code} != 200, throw exception");
                    //    throw new Exception(jObj.status_txt);
                    //}

                    //logger.Info($"  The short link is {jObj.data.url}");

                    //return jObj.data.url;

                    ////                {
                    ////  "data": {
                    ////    "global_hash": "900913", 
                    ////    "hash": "ze6poY", 
                    ////    "long_url": "http://google.com/", 
                    ////    "new_hash": 0, 
                    ////    "url": "http://bit.ly/ze6poY"
                    ////  }, 
                    ////  "status_code": 200, 
                    ////  "status_txt": "OK"
                    ////}
                }
            }
            else
            if (provider == 1)
            {
                //int sleepTime = (int)(1100d - (DateTime.Now - googlShotenerDateTime).TotalMilliseconds);
                //if (sleepTime > 0) System.Threading.Thread.Sleep(sleepTime);                

                //var m = new Google.Apis.Urlshortener.v1.Data.Url();
                //m.LongUrl = url;
                //string shortUrl = googlShortenerService.Url.Insert(m).Execute().Id;
                //googlShotenerDateTime = DateTime.Now;
                //return shortUrl;
                throw new Exception("Google shorting is not supported anymore!");

            } else
            if (provider == 2)
            {
                var client = new HttpClient();

                var content = new StringContent(string.Format(YouComSeShortTemplate, longUrl), Encoding.UTF8, "application/json");
                HttpResponseMessage result = client.PostAsync(YouComSeShortifyAddress, content).Result;

                if (!result.IsSuccessStatusCode)
                    throw new Exception($"Error while shortening: {result.ReasonPhrase}");

                JObject response = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                return YouComSeBaseAddress + response["sufix"].Value<string>();
            }
            else
            {
                throw new Exception("Unexpcted shortener providerId:" + provider);
            }
        }

        public static void ValidateTemplates()
        {
            if (!Directory.Exists(@".\Templates\"))
                throw new Exception(@"The directory for the email templates \Templates  does not exist!");

            if (!File.Exists(@".\Templates\FacilityEmail_DefaultSubject.txt"))
                throw new Exception("The template FacilityEmail_DefaultSubject.txt  does not exist!");

            if (!File.Exists(@".\Templates\FacilityEmail_DefaultBody.txt"))
                throw new Exception("The template FacilityEmail_DefaultBody.txt  does not exist!");

            if (!File.Exists(@".\Templates\ClientEmail_DefaultBody.txt"))
                throw new Exception("The template ClientEmail_DefaultBody.txt  does not exist!");

            if (!File.Exists(@".\Templates\ClientEmail_DefaultSubject.txt"))
                throw new Exception("The template ClientEmail_DefaultSubject.txt  does not exist!");

            if (!File.Exists(@".\Templates\ListOfCommunities.txt"))
                throw new Exception("The template ListOfCommunities.txt  does not exist!");

            if (!File.Exists(@".\Templates\ListOfCommunitiesTH.txt"))
                throw new Exception("The template ListOfCommunitiesTH.txt  does not exist!");

            if (!File.Exists(@".\Templates\ListOfCommunitiesTR.txt"))
                throw new Exception("The template ListOfCommunitiesTR.txt  does not exist!");
        }

        public static string SendGmail(string fromAddress, string toAddress, string password, string subject, string body, bool isBodyHtml)
        {
            Logger logger = LogManager.GetLogger("helper.gmail");
            logger.Info("===START ");
            logger.Info("  from address:" + fromAddress);
            logger.Info("  to address:" + toAddress);
            logger.Info("  password:" + Crypto.AES.Encrypt(password, "T1eM2GaS3cr#$tPassw@wrd"));
            logger.Info("  subject:" + subject);
            logger.Info("  body:" + body);

            try
            {
                var from = new MailAddress(fromAddress);
                var to = new MailAddress(toAddress);

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress, password),
                    Timeout = 20000
                };

                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = isBodyHtml
                })
                {
                    logger.Info("Before send");
                    smtp.Send(message);
                    logger.Info("After send");

                    return "OK";
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                logger.Info("ERROR: " + ex.Message);
                return ex.Message;
            }
        }

        public static string ReplaceCommunityTemplate(string text, Db.FacilityDetails facilityData)
        {
            text = text.Replace("{FACILITY_NAME}", facilityData.name);
            text = text.Replace("{FACILITY_STREET}", facilityData.addressStreet);
            text = text.Replace("{FACILITY_CITY}", facilityData.addressCity);
            text = text.Replace("{FACILITY_STATE}", facilityData.addressState);
            text = text.Replace("{FACILITY_ZIP}", facilityData.addressZip);
            text = text.Replace("{FACILITY_PHONE}", facilityData.phones.FirstOrDefault());
            text = text.Replace("{FACILITY_EMAIL}", facilityData.emails.FirstOrDefault());
            text = text.Replace("{FACILITY_NAME}", facilityData.name);
            text = text.Replace("{FACILITY_FAX}", facilityData.fax);
            text = text.Replace("{FACILITY_CONTACT_PERSON}", facilityData.contactPerson);

            return text;
        }

        public static string ReplacePatientTemplate(string text, PatientTemplateData templateData)
        {
            text = text.Replace("{PATIENT_FIRST}", templateData.PATIENT_FIRST?.Trim());
            text = text.Replace("{PATIENT_LAST}", templateData.PATIENT_LAST?.Trim());
            text = text.Replace("{POC_FIRST}", templateData.POC_FIRST?.Trim());
            text = text.Replace("{POC_LAST}", templateData.POC_LAST?.Trim());
            text = text.Replace("{PATIENT_AGE}", templateData.PATIENT_AGE.ToString()?.Trim());
            text = text.Replace("{PATIENT_STREET}", templateData.PATIENT_STREET?.Trim());
            text = text.Replace("{PATIENT_CITY}", templateData.PATIENT_CITY?.Trim());
            text = text.Replace("{PATIENT_NOTES}", templateData.PATIENT_NOTES?.Trim());
            text = text.Replace("{PATIENT_STATE}", templateData.PATIENT_STATE?.Trim());
            text = text.Replace("{PATIENT_ZIP}", templateData.PATIENT_ZIP?.Trim());
            text = text.Replace("{PATIENT_EMAIL}", templateData.PATIENT_EMAIL?.Trim());
            text = text.Replace("{PATIENT_TELEPHONE}", templateData.PATIENT_TELEPHONE?.Trim());
            text = text.Replace("{AGENT_NAME}", templateData.AGENT_NAME?.Trim());
            text = text.Replace("{BUDGET}", templateData.BUDGET?.Trim());
            text = text.Replace("{BUDGET_FOR_AL}", templateData.BUDGET_FOR_AL?.Trim());
            text = text.Replace("{LIST_OF_COMMUNITIES}", templateData.LIST_OF_COMMUNITIES?.Trim());

            return text;
        }

        public static List<CareHomes> GetAllZipsInRangeForCH(GeoCoordinate loc, decimal radiusInMiles, List<CareHomes> otherLocsList)
        {
            logger.Info("== GetAllZipsInRangeForCH ==");
            logger.Info($"  lat = {loc.Latitude}, long = {loc.Longitude}, radius = {radiusInMiles}");

            //all in :)
            if (radiusInMiles == 0 || radiusInMiles == -1)
                radiusInMiles = 1000000000;

            var res = new List<CareHomes>();

            logger.Info("== Start calculating... ==");
            foreach (var loc2 in otherLocsList)
            {
                var geoLoc = new GeoCoordinate((double)(loc2.Alt ?? 0), (double)(loc2.Lng ?? 0));

                loc2.Distance = (decimal)(loc.GetDistanceTo(geoLoc) / 1609);
                logger.Debug($" dist = {loc2.Distance} : lat = {loc2.Alt}, long = {loc2.Lng}");

                if (loc2.Distance <= radiusInMiles)
                {
                    res.Add(loc2);
                }
            }

            logger.Info($"res count: {res.Count()}");
            foreach (var r in res)
            {
                logger.Debug($"  {r.Zip}");
            }

            return res;
        }

        public static List<AssistedLiving> GetAllZipsInRangeForAL(GeoCoordinate loc, decimal radiusInMiles, List<AssistedLiving> otherLocsList)
        {
            logger.Info("== GetAllZipsInRangeForAL ==");
            logger.Info($"  lat = {loc.Latitude}, long = {loc.Longitude}, radius = {radiusInMiles}");

            //all in :)
            if (radiusInMiles == 0 || radiusInMiles == -1)
                radiusInMiles = 1000000000;

            var res = new List<AssistedLiving>();

            logger.Info("== Start calculating... ==");
            foreach (var loc2 in otherLocsList)
            {
                var geoLoc = new GeoCoordinate((double)(loc2.Alt), (double)(loc2.Lng));

                loc2.Distance = (decimal)(loc.GetDistanceTo(geoLoc) / 1609);
                logger.Debug($" dist = {loc2.Distance} : lat = {loc2.Alt}, long = {loc2.Lng}");

                if (loc2.Distance <= radiusInMiles)
                {
                    res.Add(loc2);
                }
            }

            logger.Info($"res count: {res.Count()}");
            foreach (var r in res)
            {
                logger.Debug($"  {r.Zip}");
            }

            return res;
        }
    }
}
