﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLog;
using System.Device.Location;
using System.IO;
using Newtonsoft.Json;
using AssistedLivingFacilityBlaster.Forms;

namespace AssistedLivingFacilityBlaster
{
    public static class DbCache
    {     
        private static Logger logger = LogManager.GetLogger("dbCache");

        public static FormMain formMain;

        public class ZipEntity
        {
            public string zip;
            public string city;
            public string state;
            public GeoCoordinate loc;
        }
        
        public static List<ZipEntity> fullCache;
        public static List<string> cities;        
        public static string[] zips;
        public static List<string> states;
        public static List<QueueSms> queueSms;
        public static List<QueueEmails> queueEmails;

        public static List<CareHomes> allCareHomes = null;
        public static List<AssistedLiving> allAssistedLiving = null;
        public static List<Patients> allLoadedPatiensList = null;

        public static List<InterestedPatients> lastInvitations;
        public static List<QueueEmails> lastSendEmails;
        
        private const string zipsCacheName = "zips.json";
        public static void Init(bool deleteCache = false)
        {
            if (File.Exists(zipsCacheName) && deleteCache)
            {
                File.Delete(zipsCacheName);
            }

            logger.Info("Get full cache...");
            using (var db = new DbEntities())
            {
                if (File.Exists(zipsCacheName))
                {
                    fullCache = JsonConvert.DeserializeObject<List<ZipEntity>>(File.ReadAllText(zipsCacheName));
                } else
                {
                    var temp1 = db.ZipCodesWithCities.Select(s => new
                    {
                        zip = s.zip,
                        city = s.primary_city,
                        state = s.fullState,
                        lat = (double)(s.latitude ?? 0),
                        lon = (double)(s.longitude ?? 0)
                    }).ToList();
                    
                    logger.Info("  ZipCodesWithCities done");

                    fullCache = temp1.Select(s => new ZipEntity
                    {
                        zip = s.zip,
                        city = s.city,
                        state = s.state,
                        loc = new GeoCoordinate(s.lat, s.lon)
                    }).ToList();

                    logger.Info("  success!");
                    File.WriteAllText(zipsCacheName, JsonConvert.SerializeObject(fullCache));
                }                
            }

            logger.Info("Get cities");
            cities = fullCache.Select(s => s.city).Distinct().ToList();

            logger.Info("Get zips");
            zips = fullCache.Select(s => s.zip).Distinct().ToArray();

            logger.Info("Get states");
            states = fullCache.Select(s => s.state).Distinct().ToList();

            logger.Info("== All cache done ==");
        }
    }
}