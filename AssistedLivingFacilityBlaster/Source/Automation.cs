﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistedLivingFacilityBlaster.Source
{
    public static class Automation
    {
        
        private static decimal? StrToDecimal(string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return null;
            s = s.ToLowerInvariant();
            if (s == "no") return null;
            return decimal.Parse(s.Trim());
        }

        public static void ImportAssistedLiving()
        {
            try
            {
                string[] facilities = File.ReadAllLines("Assisted Living Facilities to Add.csv");

                using (var db = new DbEntities())
                {
                    foreach (string facilityData in facilities)
                    {
                        string[] data = facilityData.Split(new char[] { ',' });

                        if (data.Length != 16)
                        {
                            throw new Exception("data len: " + facilityData);
                        }

                        string altcsStr = data[10].ToLowerInvariant().Trim();
                        bool altcs;
                        switch (altcsStr)
                        {
                            case "no altcs":
                            case "no":
                            case "null":
                            case "":
                                altcs = false;
                                break;

                            case "yes":
                                altcs = true;
                                break;

                            default: throw new Exception("altcs: " + facilityData);
                        }

                        string facName = data[0];
                        string webSite = data[1];
                        string phone = data[2];
                        string addr = data[3];
                        string zip = data[4].Trim();
                        string email1 = data[5].Trim();
                        string email2 = data[6].Trim();
                        string email3 = data[7].Trim();
                        string email4 = data[8].Trim();
                        string email5 = data[9].Trim();

                        decimal? independentLivingPrice = StrToDecimal(data[12]);
                        decimal? assistedLivingPrice = StrToDecimal(data[13]);
                        decimal? memoryCarePrice = StrToDecimal(data[14]);
                        decimal? skilledNursingCarePrice = StrToDecimal(data[15]);

                        var zipinfo = DbCache.fullCache.SingleOrDefault(s => s.zip == zip);
                        if (zipinfo == null)
                        {
                            throw new Exception("zip is null: " + zip);
                        }

                        Guid facId = Guid.NewGuid();
                        db.AssistedLiving.Add(new AssistedLiving()
                        {
                            AddressLine = addr,                            
                            ALTSC = altcs,                            
                            Name = facName,                            
                            Id = facId,                            
                            Updated = DateTime.Now,
                            Zip = zip,
                            AssistedLivingPrice = assistedLivingPrice,
                            Email1 = email1,
                            Email2 = email2,
                            Email3 = email3,
                            Email4 = email4,
                            Email5 = email5,
                            IndependentLivingPrice = independentLivingPrice,
                            MemoryCarePrice = memoryCarePrice,
                            Phone = phone,
                            WebSite = webSite,
                            Notes = data[11].Trim(),
                            SkilledNursingCarePrice = skilledNursingCarePrice
                        });                        
                    }

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception occured, nothing was saved. " + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void FormatCareHomePhones()
        {            
            using (var db = new DbEntities())
            {
                var allContacts = db.CareHomeContacts;

                foreach (var contact in allContacts)
                {
                    contact.Phone = string.Join("", contact.Phone.ToCharArray().Where(Char.IsDigit));
                    if (contact.Phone.Length == 10)
                    {
                        contact.Phone =
                            contact.Phone.Substring(0, 3) + "-" +
                            contact.Phone.Substring(3, 3) + "-" +
                            contact.Phone.Substring(6, 4);
                    }
                    else
                    {

                    }
                }

                db.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void ImportCareHomes()
        {
            try
            {
                string[] facilities = File.ReadAllLines("171020_careHomes.csv");

                using (var db = new DbEntities())
                {

                    foreach (string facilityData in facilities)
                    {
                        string[] data = facilityData.Split(new char[] { ',' });

                        if (data.Length != 8)
                        {
                            throw new Exception();
                        }

                        string altcsStr = data[5].ToLowerInvariant().Trim();
                        bool altcs;
                        switch (altcsStr)
                        {
                            case "no altcs":
                            case "no":
                                altcs = false;
                                break;

                            case "yes":
                                altcs = true;
                                break;

                            default: throw new Exception();
                        }

                        string facName = data[0];
                        string contactName = data[1];
                        string phone = data[2];
                        string addr = data[3];
                        string zip = data[4].Trim();
                        decimal privateRoomPrice = decimal.Parse(data[6]);
                        decimal semiRoomPrice = decimal.Parse(data[7]);

                        if (string.IsNullOrWhiteSpace(zip))
                            continue;

                        var zipinfo = DbCache.fullCache.SingleOrDefault(s => s.zip == zip);
                        if (zipinfo == null)
                        {
                            throw new Exception();
                        }

                        Guid facId = Guid.NewGuid();
                        db.CareHomes.Add(new CareHomes()
                        {
                            AddressLine1 = addr,
                            ALTSC = altcs ? 1 : 0,
                            City = zipinfo.city,
                            FacilityName = facName,
                            FacilityType = "Care home (sms)",
                            PrivateRoomPrice = privateRoomPrice,
                            SemiPrivateRoomPrice = semiRoomPrice,
                            Id = facId,
                            State = zipinfo.state,
                            Updated = DateTime.Now,
                            Zip = zip
                        });

                        db.CareHomeContacts.Add(new CareHomeContacts()
                        {
                            ContactName = contactName,
                            Email = null,
                            FacilityId = facId,
                            Fax = null,
                            Id = Guid.NewGuid(),
                            Phone = phone
                        });
                    }

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception occured, nothing was saved. " + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void ImportCareHomes2()
        {
            try
            {
                string[] facilities = File.ReadAllLines("New Group homes.csv");

                using (var db = new DbEntities())
                {

                    foreach (string facilityData in facilities)
                    {
                        string[] data = facilityData.Split(new char[] { ',' });

                        try
                        {

                            if (data.Length != 10)
                            {
                                throw new Exception();
                            }

                            string altcsStr = data[6].ToLowerInvariant().Trim();
                            bool altcs;
                            switch (altcsStr)
                            {
                                case "no altcs":
                                case "no":
                                    altcs = false;
                                    break;

                                case "yes":
                                    altcs = true;
                                    break;

                                default: throw new Exception();
                            }

                            string privateRoom = data[7].Replace("\"", "").Replace("$", "").Trim();
                            string semiPrivateRoom = data[8].Replace("\"", "").Replace("$", "").Trim();

                            string facName = data[0];
                            string contactName = data[1];
                            string phone1 = data[2];
                            string phone2 = data[3];
                            string addr = data[4];
                            string zip = data[5].Trim();
                            decimal? privateRoomPrice = (privateRoom == "" ? (decimal?)null: decimal.Parse(privateRoom));
                            decimal? semiRoomPrice = (semiPrivateRoom == "" ? (decimal?)null : decimal.Parse(semiPrivateRoom));
                            string email = data[9].Trim();

                            if (string.IsNullOrWhiteSpace(zip))
                                continue;

                            var zipinfo = DbCache.fullCache.SingleOrDefault(s => s.zip == zip);
                            if (zipinfo == null)
                            {
                                throw new Exception();
                            }

                            Guid facId = Guid.NewGuid();
                            db.CareHomes.Add(new CareHomes()
                            {
                                AddressLine1 = addr,
                                ALTSC = altcs ? 1 : 0,
                                City = zipinfo.city,
                                FacilityName = facName,
                                FacilityType = "Care home (sms)",
                                PrivateRoomPrice = privateRoomPrice,
                                SemiPrivateRoomPrice = semiRoomPrice,
                                Id = facId,
                                State = zipinfo.state,
                                Updated = DateTime.Now,
                                Zip = zip,
                            });

                            db.CareHomeContacts.Add(new CareHomeContacts()
                            {
                                ContactName = contactName,
                                Email = email,
                                FacilityId = facId,
                                Fax = null,
                                Id = Guid.NewGuid(),
                                Phone = phone1
                            });

                            db.CareHomeContacts.Add(new CareHomeContacts()
                            {
                                ContactName = contactName,
                                Email = null,
                                FacilityId = facId,
                                Fax = null,
                                Id = Guid.NewGuid(),
                                Phone = phone2
                            });
                        }
                        catch(Exception ex)
                        {
                            MessageBox.Show("Exception occured, nothing was saved. " + ex.Message);
                            throw ex;
                        }

                    }

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception occured, nothing was saved. " + ex.Message);
            }
        }
    }
}
