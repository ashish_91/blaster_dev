﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssistedLivingFacilityBlaster
{
    public partial class DbEntities
    {
        public DbEntities(string s) : base(s)
        {
        }

        //public DbEntities() : base("metadata=res://*/ModelDb.csdl|res://*/ModelDb.ssdl|res://*/ModelDb.msl;provider=System.Data.SqlClient;provider connection string=\"data source=77.70.98.63\\SqlExpress;initial catalog=YellowPages;persist security info=False;user id=yp;password=m2megasecuredp2sswo3434gd;multipleactiveresultsets=True;application name=EntityFramework\"")
        public DbEntities() : base("metadata=res://*/ModelDb.csdl|res://*/ModelDb.ssdl|res://*/ModelDb.msl;provider=System.Data.SqlClient;provider connection string=\"data source=sql5035.smarterasp.net;initial catalog=DB_A1A018_jubotho;persist security info=False;user id=DB_A1A018_jubotho_admin;password=1q0p1q0p1q0p;multipleactiveresultsets=True;application name=EntityFramework\"")
        {
        }
    }

    public static class Db
    {
        public const int ReportSendEmailToFacility = 1;
        public const int ReportSendEmailToClient = 2;
        public const int ReportSendSms = 3;
        public const int ReportSendQuestionsAsSms = 4;

        public enum Mode
        {
            create, edit, view, delete
        }

        public static Users user;
        public static Sessions session;

        public static List<Users> agents;

        public static void ValidateSessionWithAdmin()
        {
            ValidateSession(true);
        }

        public class FacilityDetails
        {
            public Guid id;
            public string name;
            public string addressStreet;
            public string addressCity;
            public string addressState;
            public string addressZip;

            public string fax;
            public string contactPerson;

            public List<string> phones;
            public List<string> emails;
        }

        // ========== Questions ===============
        public static List<QuestionCampaigns> LoadQuestionCampaigns()
        {
            using (var db = new DbEntities())
            {
                return db.QuestionCampaigns.ToList();
            }
        }

        public static void RemoveFromSmsQueue(List<Guid> entries)
        {
            using (var db = new DbEntities())
            {
                db.QueueSms.RemoveRange(db.QueueSms.Where(w => entries.Contains(w.Id)));
                db.SaveChanges();
            }
        }

        public static void RemoveFromEmailQueue(List<Guid> entries)
        {
            using (var db = new DbEntities())
            {
                db.QueueEmails.RemoveRange(db.QueueEmails.Where(w => entries.Contains(w.Id)));
                db.SaveChanges();
            }
        }

        public static List<InterestedPatients> LoadLastInvitations()
        {
            using (var db = new DbEntities())
            {
                DateTime _36hAgo = DateTime.Now.AddHours(-36);
                return db.InterestedPatients.Where(w => w.InvitationSent > _36hAgo).ToList();
            }
        }

        public static List<QueueEmails> LoadLastSendEmails()
        {
            using (var db = new DbEntities())
            {
                DateTime _36hAgo = DateTime.Now.AddHours(-36);
                return db.QueueEmails.Where(w => w.Updated > _36hAgo && w.StatusId == 1).ToList();
            }
        }

        /// <summary>
        /// 0 - pending
        /// -1 - error
        /// 1 - successfuly sent
        /// </summary>
        /// <returns></returns>

        public static void UpdateSmsQueue(QueueSms queueEntity)
        {
            using (var db = new DbEntities())
            {
                var queue = db.QueueSms.Single(s => s.Id == queueEntity.Id);

                queue.ShortUrl = queueEntity.ShortUrl;
                queue.Status = queueEntity.Status;
                queue.StatusId = queueEntity.StatusId;
                queue.TextMessage = queueEntity.TextMessage;
                queue.Updated = DateTime.Now;

                var inv = new InterestedPatients()
                {
                    FacilityId = queueEntity.FacilityId ?? Guid.Empty,
                    HaveInterest = 0,
                    Id = queueEntity.InterestedPatientsId,
                    InvitationSent = DateTime.Now,
                    PatientId = queueEntity.PatientId ?? Guid.Empty,
                    PhoneSentTo = queueEntity.Recipient,
                    SmsText = queueEntity.TextMessage,
                    FullURL = queueEntity.LongUrl,
                    ShortURL = queueEntity.ShortUrl,
                    Error = (queueEntity.StatusId < 0 ? queueEntity.Status : null)
                };

                db.InterestedPatients.Add(inv);
                DbCache.lastInvitations.Add(inv);

                var report = db.ActionsReport.Single(s => s.Id == queueEntity.ActionsReport);
                if (queueEntity.StatusId < 0) report.ErrorText = queueEntity.Status;
                report.Status = queueEntity.StatusId > 0 ? 1 : -1;

                db.SaveChanges();
            }
        }

        /// <summary>
        /// 0 - pending
        /// -1 - error
        /// 1 - successfuly sent
        /// </summary>
        /// <returns></returns>

        public static void UpdateEmailsQueue(QueueEmails emailEntity)
        {
            using (var db = new DbEntities())
            {
                var queue = db.QueueEmails.Single(s => s.Id == emailEntity.Id);
                
                queue.Status = emailEntity.Status;
                queue.StatusId = emailEntity.StatusId;
                queue.Updated = DateTime.Now;
                                
                DbCache.lastSendEmails.Add(emailEntity);

                var report = db.ActionsReport.Single(s => s.Id == emailEntity.ActionsReportId);
                if (emailEntity.StatusId < 0) report.ErrorText = emailEntity.Status;
                report.Status = emailEntity.StatusId > 0 ? 1 : -1;

                db.SaveChanges();
            }
        }

        public static List<QueueEmails> LoadEmailsQueueFromDb()
        {
            ValidateSession();

            using (var db = new DbEntities())
            {
                return db.QueueEmails
                    .Where(w => w.StatusId == 0 && w.UserId == user.Id)
                    .OrderBy(o => o.Created)
                    .ToList();
            }
        }

        public static List<QueueSms> LoadSmsQueueFromDb()
        {
            ValidateSession();

            using (var db = new DbEntities())
            {
                return db.QueueSms
                    .Where(w => w.StatusId == 0 && w.UserId == user.Id)
                    .OrderBy(o => o.Created)
                    .ToList();
            }
        }

        public static List<FacilityDetails> FillFacilitiesStructuredData(List<Guid> facilitiesToNotify)
        {
            if (facilitiesToNotify == null)
                return null;

            ValidateSession();

            using (var db = new DbEntities())
            {
                var fac = (from f in db.CareHomes
                           from c in db.CareHomeContacts.Where(w => w.FacilityId == f.Id).DefaultIfEmpty()
                           where facilitiesToNotify.Contains(f.Id)
                           select new
                           {
                               f.FacilityName,
                               f.Id,
                               f.AddressLine1,
                               f.City,
                               f.State,
                               f.Zip,
                               f.PrivateRoomPrice,
                               f.SemiPrivateRoomPrice,

                               c.Email,
                               c.Phone,
                               c.Fax,
                               c.ContactName
                           }).ToList();

                var fac2 = (from f in fac
                            group new { f.Email, f.Phone, f.Fax, f.ContactName } by new { f.Id, f.FacilityName, f.AddressLine1, f.City, f.State, f.Zip, f.PrivateRoomPrice, f.SemiPrivateRoomPrice } into gr
                            select new
                            {
                                gr.Key,
                                contacts = gr.ToList()
                            }).ToList();

                List<FacilityDetails> facilitiesMailDetails = new List<FacilityDetails>();

                foreach (var f in fac2)
                {
                    facilitiesMailDetails.Add(new Db.FacilityDetails()
                    {
                        id = f.Key.Id,
                        name = f.Key.FacilityName,
                        addressCity = f.Key.City,
                        addressState = f.Key.State,
                        addressStreet = f.Key.AddressLine1,
                        addressZip = f.Key.Zip,

                        contactPerson = f.contacts.Select(s => s.ContactName).FirstOrDefault(),
                        fax = f.contacts.Select(s => s.Fax).FirstOrDefault(),
                        phones = f.contacts.Select(s => s.Phone).ToList(),
                        emails = f.contacts.Select(s => s.Email).ToList()
                    });
                }

                return facilitiesMailDetails;
            }
        }

        public static List<FacilityDetails> FillAssistedLivingStructuredData(List<Guid> assistedLivingToNotify)
        {
            if (assistedLivingToNotify == null)
                return null;

            ValidateSession();

            using (var db = new DbEntities())
            {
                var fac = (from f in db.AssistedLiving
                           where assistedLivingToNotify.Contains(f.Id)
                           select new
                           {
                               f.Name,
                               f.Id,
                               f.AddressLine,
                               f.Zip,
                               f.Email1,
                               f.Email2,
                               f.Email3,
                               f.Email4,
                               f.Email5,
                               f.Phone
                           }).ToList();
                
                var facilitiesMailDetails = new List<FacilityDetails>();

                foreach (var f in fac)
                {
                    DbCache.ZipEntity selectedZip = DbCache.fullCache.AsParallel().Where(w => w.zip == f.Zip).SingleOrDefault();

                    var emails = new List<string>();
                    if (!String.IsNullOrWhiteSpace(f.Email1)) emails.Add(f.Email1);
                    if (!String.IsNullOrWhiteSpace(f.Email2)) emails.Add(f.Email2);
                    if (!String.IsNullOrWhiteSpace(f.Email3)) emails.Add(f.Email3);
                    if (!String.IsNullOrWhiteSpace(f.Email4)) emails.Add(f.Email4);
                    if (!String.IsNullOrWhiteSpace(f.Email5)) emails.Add(f.Email5);

                    facilitiesMailDetails.Add(new FacilityDetails()
                    {
                        id = f.Id,
                        name = f.Name,
                        addressCity = selectedZip.city,
                        addressState = selectedZip.state,
                        addressStreet = f.AddressLine,
                        addressZip = f.Zip,
                        
                        phones = new List<string>(new string[] { f.Phone}),
                        emails = emails                        
                    });
                }

                return facilitiesMailDetails;
            }
        }

        public static void ValidateSession(bool validateAdmin = false)
        {
            using (var db = new DbEntities())
            {
                user = db.Users.Where(w => w.Id == user.Id).SingleOrDefault();

                if (user == null) throw new Exception("Invalid user, login again!");
                if (user.Enabled != 1) throw new Exception("This user is not enabled, contact Max");

                session = db.Sessions.SingleOrDefault(s => s.Id == session.Id);
                if (session == null) throw new Exception("Invalid session, login again!");

                if (validateAdmin && user.IsAdmin != 1)
                {
                    //disable account
                    db.Users.Where(w => w.Id == user.Id).Single().Enabled = 0;
                    db.SaveChanges();
                    throw new Exception("This operation requires an Admin account! This account is locked.");
                }
            }
        }

        public static void ManageUser(Users user, Mode mode)
        {
            ValidateSessionWithAdmin();

            using (var db = new DbEntities())
            {
                switch (mode)
                {
                    case Mode.create:

                        if (db.Users.Where(w => w.UserName == user.UserName).FirstOrDefault() != null)
                        {
                            throw new Exception("User with this gmail account already exist!");
                        }

                        db.Users.Add(user);
                        break;

                    case Mode.edit:
                        Users u = db.Users.SingleOrDefault(s => s.Id == user.Id);
                        if (u == null) throw new Exception("The user can't be found");

                        u.Enabled = user.Enabled;
                        u.IsAdmin = user.IsAdmin;
                        u.LastLogin = user.LastLogin;
                        u.PasswordCrypted = user.PasswordCrypted;
                        u.PasswordHash = user.PasswordHash;
                        u.RealName = user.RealName;
                        u.UserName = user.UserName;
                        break;

                    case Mode.delete:
                        Users us = db.Users.SingleOrDefault(s => s.Id == user.Id);
                        if (us == null) throw new Exception("The user can't be found");

                        db.Users.Remove(us);
                        break;

                }

                db.SaveChanges();
            }
        }

        public static Users LoadUserInfo(Guid userId)
        {
            ValidateSessionWithAdmin();

            using (var db = new DbEntities())
            {
                return db.Users.SingleOrDefault(w => w.Id == userId);
            }
        }

        public static void LoadAgents()
        {
            ValidateSessionWithAdmin();

            using (var db = new DbEntities())
            {
                agents = db.Users.ToList();
            }
        }

        public static void LoadSessionRelatedInfo(Guid sessionId)
        {
            using (var db = new DbEntities())
            {
                Db.session = db.Sessions.SingleOrDefault(s => s.Id == sessionId);
                if (Db.session == null)
                {
                    throw new Exception("Invalid session, login again!");
                }

                Db.user = db.Users.SingleOrDefault(s => s.Id == Db.session.UserId);
                if (Db.user == null)
                {
                    throw new Exception("Invalid user, login again!");
                }

                if (Db.user.Enabled != 1)
                {
                    throw new Exception("This user is not enabled, contact Max");
                }

                Db.user.LastLogin = DateTime.Now;
                db.SaveChanges();
            }
        }

        public static void Login(string userName, string password)
        {
            using (var db = new DbEntities())
            {
                userName = userName.Trim();
                string hash = Crypto.SHA512.ComputeHash(password);

                Users user = db.Users.SingleOrDefault(w => w.UserName == userName && w.PasswordHash == hash);
                if (user == null)
                {
                    throw new Exception("Invalid username or password!");
                }

                if (user.Enabled != 1)
                {
                    throw new Exception("This user is not enabled, contact Max");
                }

                user.LastLogin = DateTime.Now;
                Sessions session = db.Sessions.SingleOrDefault(w => w.UserId == user.Id);
                if (session == null)
                {
                    session = db.Sessions.Add(new Sessions()
                    {
                        Created = DateTime.Now,
                        Id = Guid.NewGuid(),
                        UserId = user.Id
                    });
                }

                db.SaveChanges();

                Db.user = user;
                Db.session = session;
            }
        }
    }
}
