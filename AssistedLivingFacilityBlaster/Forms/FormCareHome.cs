﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistedLivingFacilityBlaster
{
    public partial class FormCareHome : Form
    {
        public enum Mode
        {
            create, edit, view
        }

        public Mode mode = Mode.view;
        public Guid careHomeId { get; set; }
        Logger logger = LogManager.GetLogger("careHomeInfo");
        public bool doneChanges = false;

        public FormCareHome()
        {
            InitializeComponent();
        }
        
        private void btnClose_Click(object sender, EventArgs e)
        {
            if (doneChanges) DialogResult = DialogResult.Yes;
            else
                DialogResult = DialogResult.No;

            Close();
        }

        private bool ValidateData()
        {
            try
            {
                if (string.IsNullOrEmpty(txtCareHomeName.Text))
                {
                    MessageBox.Show("Please enter the care home name!");
                    if (txtCareHomeName.CanFocus) txtCareHomeName.Focus();
                    return false;
                }

                string zip = txtZip.Text.Trim();
                var cacheObj = DbCache.fullCache.AsParallel().Where(w => w.zip == zip).FirstOrDefault();
                if (cacheObj == null)                    
                {
                    DialogResult dialogResult = MessageBox.Show("The zip is not found in the database. Adding it anyway?", "No valid zip?", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.No)
                    {
                        return false;
                    }

                    if (txtZip.CanFocus) txtZip.Focus();
                }

                if (!rbALTCSNo.Checked && !rbALTCSYes.Checked)
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure you want to save the Care Home with no ALTCS selected?", "No ALTCS?", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.No)
                    {
                        return false;
                    }
                }

                bool hasContacts = false;
                foreach (DataGridViewRow row in gridContacts.Rows)
                {
                    string contactName = row.Cells["ContactName"].Value?.ToString();
                    string email = row.Cells["Email"].Value?.ToString();
                    string fax = row.Cells["Fax"].Value?.ToString();
                    string notes = row.Cells["Notes"].Value?.ToString();
                    string phone = row.Cells["Phone"].Value?.ToString();

                    if (contactName != null || email != null || fax != null || notes != null || phone != null)
                    {
                        hasContacts = true;
                        break;
                    }
                }

                if (!hasContacts)
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure you want to save the Care Home with no contacts at all?", "No contatcs?", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.No)
                    {
                        return false;
                    }
                }

                return true;
            }
            catch(Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error while validating: " + ex.Message);
                return false;
            }            
                    
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateData())
                    return;

                logger.Info("Saving data: ");

                logger.Info("  " + edAddressStreet.Text);                
                logger.Info("  " + txtCity.Text);
                logger.Info("  " + txtCareHomeName.Text);                
                logger.Info("  " + txtNotes.Text);
                logger.Info("  " + cbAddressState.Text);
                logger.Info("  " + dateUpdated.Value);
                logger.Info("  " + txtWebSite.Text);
                logger.Info("  " + txtZip.Text);
                logger.Info("  " + txtPrivateRoomPrice.Value);
                logger.Info("  " + txtSemiPrivateRoomPrice.Value);                

                using (var db = new DbEntities())
                {
                    Guid id = Guid.Empty;                    

                    if (mode == Mode.create)
                    {
                        id = Guid.NewGuid();
                        careHomeId = id;
                        var fc = new CareHomes()
                        {
                            AddressLine1 = edAddressStreet.Text,
                            City = txtCity.Text,
                            FacilityName = txtCareHomeName.Text,
                            FacilityType = "Care home (sms)",
                            Id = id,
                            Notes = txtNotes.Text,
                            State = cbAddressState.Text,
                            Updated = dateUpdated.Value,
                            WebSite = txtWebSite.Text,
                            Zip = txtZip.Text,
                            PrivateRoomPrice = txtPrivateRoomPrice.Value,
                            SemiPrivateRoomPrice = txtSemiPrivateRoomPrice.Value
                        };

                        if (rbALTCSYes.Checked)
                        {
                            fc.ALTSC = 1;
                        }
                        else if (rbALTCSNo.Checked)
                        {
                            fc.ALTSC = 0;
                        }
                        else
                        {
                            fc.ALTSC = null;
                        }

                        db.CareHomes.Add(fc);
                    }
                    else
                    if (mode == Mode.edit)
                    {
                        id = careHomeId;

                        var f = db.CareHomes.Where(w => w.Id == careHomeId).Single();

                        f.AddressLine1 = edAddressStreet.Text;
                        if (rbALTCSYes.Checked)
                        {
                            f.ALTSC = 1;
                        }
                        else if (rbALTCSNo.Checked)
                        {
                            f.ALTSC = 0;
                        }
                        else
                        {
                            f.ALTSC = null;
                        }
                        
                        f.City = txtCity.Text;
                        f.FacilityName = txtCareHomeName.Text;
                        f.FacilityType = "Care home (sms)";
                        f.Id = id;
                        f.Notes = txtNotes.Text;
                        f.State = cbAddressState.Text;
                        f.Updated = dateUpdated.Value;
                        f.WebSite = txtWebSite.Text;
                        f.PrivateRoomPrice = txtPrivateRoomPrice.Value;
                        f.SemiPrivateRoomPrice = txtSemiPrivateRoomPrice.Value;
                        f.Zip = txtZip.Text;
                    }
                    
                    db.CareHomeContacts.RemoveRange(db.CareHomeContacts.Where(w => w.FacilityId == id));

                    foreach (DataGridViewRow row in gridContacts.Rows)
                    {
                        string contactName = row.Cells["ContactName"].Value?.ToString();
                        string email = row.Cells["Email"].Value?.ToString();
                        string fax = row.Cells["Fax"].Value?.ToString();
                        string notes = row.Cells["Notes"].Value?.ToString();
                        string phone = row.Cells["Phone"].Value?.ToString();

                        if (contactName != null || email != null || fax != null || notes != null || phone != null)
                        {
                            db.CareHomeContacts.Add(new CareHomeContacts()
                            {
                                ContactName = contactName,
                                FacilityId = id,
                                Id = Guid.NewGuid(),
                                Email = email,
                                Fax = fax,
                                Notes = notes,
                                Phone = phone
                            });
                        }
                    }

                    db.SaveChanges();
                    doneChanges = true;
                    logger.Info("  SUCCESS");
                }

                if (mode == Mode.create)
                {
                    MessageBox.Show("The Care Home was created successfully!");

                    DialogResult dialogResult = MessageBox.Show("Clear the form to add another Care Home?", "Clear form?", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        btnClearForm_Click(sender, e);
                    }
                    else
                    {
                        mode = Mode.view;
                        RespectMode();
                    }

                }
                else if (mode == Mode.edit)
                {
                    MessageBox.Show("The Care Home was updated successfully!");
                    Close();
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex, "Save Care Home");
                MessageBox.Show("Error occured during saving: " + ex.Message);
            }
        }

        private string ModeToString(Mode mode)
        {
            if (mode == Mode.create) return "CREATE NEW";
            if (mode == Mode.edit) return "EDIT";
            if (mode == Mode.view) return "VIEW";

            return "UNKNOWN";
        }

        private void RespectMode()
        {                      
            this.Text = $"Care Home Details ({ModeToString(mode)})";

            txtCareHomeName.Enabled = mode != Mode.view;            
            txtNotes.Enabled = mode != Mode.view;
            gridContacts.Enabled = mode != Mode.view;
            txtWebSite.Enabled = mode != Mode.view;
            dateUpdated.Enabled = mode != Mode.view;
            //txtCity.Enabled = mode != Mode.view;
            //cbAddressState.Enabled = mode != Mode.view;
            txtZip.Enabled = mode != Mode.view;
            edAddressStreet.Enabled = mode != Mode.view;
            rbALTCSNo.Enabled = mode != Mode.view;
            rbALTCSYes.Enabled = mode != Mode.view;            
            txtSemiPrivateRoomPrice.Enabled = mode != Mode.view;
            txtPrivateRoomPrice.Enabled = mode != Mode.view;

            if (mode == Mode.create)
            {
                dateUpdated.Value = DateTime.Now;
                btnDelete.Visible = false;
            }

            if (mode == Mode.view)
            {
                btnDelete.Visible = true;
                btnSave.Visible = false;
                btnClearForm.Visible = false;
            }

            if (mode == Mode.edit)
            {
                btnDelete.Visible = true;
                btnSave.Visible = true;
                btnClearForm.Visible = true;
            }
        }

        #region zip selection
        bool skipZipChanging = false;
        private void cbAddressZip_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (skipZipChanging)
                return;

            string zip = txtZip.Text.Trim();
            var cacheObj = DbCache.fullCache.AsParallel().Where(w => w.zip == zip).FirstOrDefault();
            if (cacheObj == null)
            {
                MessageBox.Show("Invalid zip! Please select zips only from the list");
                return;
            }

            //select state
            skipStateChange = true;
            try
            {
                cbAddressState.SelectedIndex = cbAddressState.Items.IndexOf(cacheObj.state);
            }
            finally
            {
                skipStateChange = false;
            }
            
            //now select the right city
            skipCityChanging = true;
            try
            {
                //cbAddressCity.SelectedIndex = cbAddressCity.Items.IndexOf(cacheObj.city);
                txtCity.Text = cacheObj.city;
            }
            finally
            {
                skipCityChanging = false;
            }
        }

        bool skipStateChange = false;        

        private void cbAddressState_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (skipStateChange)
                return;

            skipCityChanging = true;
            try
            {
                //RefreshCities();
                //cbAddressCity.SelectedIndex = -1;
                //cbAddressCity.Text = String.Empty;                
            }
            finally
            {
                skipCityChanging = false;
            }

            skipZipChanging = true;
            try
            {
                //cbAddressZip.SelectedIndex = -1;
                txtZip.Text = String.Empty;
            }
            finally
            {
                skipZipChanging = false;
            }
        }

        bool skipCityChanging = false;
        private void cbAddressCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (skipCityChanging)
                return;

            skipZipChanging = true;
            try
            {
                //cbAddressZip.SelectedIndex = -1;
                txtZip.Text = String.Empty;
            }
            finally
            {
                skipZipChanging = false;
            }
        }

        private void FormFacility_Load(object sender, EventArgs e)
        {                        
            RespectMode();
            timer1.Enabled = true;
        }
        #endregion

        private void SetProgressHint(string hint)
        {
            logger.Info(hint);
            lblProgress.Text = hint;
            Application.DoEvents();
        }

        private void btnClearForm_Click(object sender, EventArgs e)
        {
            txtCareHomeName.Text = "";
            txtWebSite.Text = "";            
            txtNotes.Text = "";
            rbALTCSNo.Checked = false;
            rbALTCSYes.Checked = false;            
            gridContacts.Rows.Clear();

            txtPrivateRoomPrice.Value = 0;
            txtSemiPrivateRoomPrice.Value = 0;

            edAddressStreet.Text = "";

            skipZipChanging = true;
            skipStateChange = true;
            skipCityChanging = true;
            try
            {
                //cbAddressZip.Text = "";                
                txtZip.Text = "";
                cbAddressState.SelectedText = "Arizona";
                //cbAddressCity.SelectedIndex = -1;
            }
            finally
            {
                skipZipChanging = false;
                skipStateChange = false;
                skipCityChanging = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Enabled = false;
            lblProgress.Visible = true;
            progressBar.Visible = true;
            try
            {
                timer1.Enabled = false;

                SetProgressHint("Load zips...");
                //cbAddressZip.Items.AddRange(DbCache.zips.ToArray());

                SetProgressHint("Load states...");
                cbAddressState.Items.AddRange(DbCache.states.ToArray());
                cbAddressState.SelectedIndex = cbAddressState.Items.IndexOf("Arizona");

                if (mode == Mode.edit || mode == Mode.view) {

                    SetProgressHint("Load Care Home info...");
                    using (var db = new DbEntities())
                    {
                        var careHome = (from f in db.CareHomes
                                        from c in db.CareHomeContacts.Where(w => w.FacilityId == f.Id).DefaultIfEmpty()
                                        where f.Id == careHomeId
                                        select new
                                        {
                                            f,
                                            c
                                        }).ToList();

                        if (careHome.Count <= 0)
                        {
                            MessageBox.Show("No databse info for that Care Home? That's very odd. Care Home Id: " + careHomeId);
                            logger.Error("No databse info for that Care Home? That's very odd. Care Home Id: " + careHomeId);
                            return;
                        }

                        txtCareHomeName.Text = careHome[0].f.FacilityName;                        
                        txtNotes.Text = careHome[0].f.Notes;
                        dateUpdated.Value = careHome[0].f.Updated ?? new DateTime(1980, 1, 1);
                        txtWebSite.Text = careHome[0].f.WebSite;
                        txtSemiPrivateRoomPrice.Value = careHome[0].f.SemiPrivateRoomPrice ?? 0;
                        txtPrivateRoomPrice.Value = careHome[0].f.PrivateRoomPrice ?? 0;

                        if (careHome[0].f.ALTSC == 1)
                        {
                            rbALTCSYes.Checked = true;
                        }
                        else
                        if (careHome[0].f.ALTSC == 0)
                        {
                            rbALTCSNo.Checked = true;
                        }
                        
                        foreach(var contact in careHome)
                        {
                            if (contact.c == null)
                                continue;

                            gridContacts.Rows.Add(
                                contact.c.ContactName,
                                contact.c.Phone,
                                contact.c.Email,
                                contact.c.Fax,
                                contact.c.Notes,
                                contact.c.Id.ToString()
                            );
                        }

                        skipCityChanging = true;
                        skipStateChange = true;
                        skipZipChanging = true;
                        try
                        {
                            edAddressStreet.Text = careHome[0].f.AddressLine1;
                            txtCity.Text = careHome[0].f.City;
                            cbAddressState.Text = careHome[0].f.State;
                            txtZip.Text = careHome[0].f.Zip;
                        }
                        finally
                        {
                            skipCityChanging = false;
                            skipStateChange = false;
                            skipZipChanging = false;
                        }


                    }
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error while loading the form: " + ex.Message);
            }
            finally
            {
                this.Enabled = true;
                progressBar.Visible = false;
                lblProgress.Visible = false;
            }
        }

        private void FormFacility_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (doneChanges) DialogResult = DialogResult.Yes;
            else
                DialogResult = DialogResult.No;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {                
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete the selected Care Home?", "Delete Care Home", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.No)
                {
                    return;
                }
                
                try
                {
                    using (var db = new DbEntities())
                    {
                        SetProgressHint("Get the id");                                                

                        SetProgressHint("Deleting the Care Home");
                        db.CareHomes.Remove(db.CareHomes.Where(w => w.Id == careHomeId).Single());
                        SetProgressHint("Deleting the Care Home contacts");
                        db.CareHomeContacts.RemoveRange(db.CareHomeContacts.Where(w => w.FacilityId == careHomeId).ToList());

                        db.SaveChanges();
                    }

                    MessageBox.Show("The Care Home has been successfuly deleted!");
                    doneChanges = true;
                    Close();
                }
                finally
                {
                    
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error while deleting Care Home: " + ex.Message);
            }
        }

        private void txtZip_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void txtZip_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtZip.Text.Length == 5)
            {
                string zip = txtZip.Text.Trim();
                var cacheObj = DbCache.fullCache.AsParallel().Where(w => w.zip == zip).FirstOrDefault();
                if (cacheObj == null)
                {
                    MessageBox.Show("Zip not found in the database!");
                    return;
                }

                //select state
                skipStateChange = true;
                try
                {
                    cbAddressState.SelectedIndex = cbAddressState.Items.IndexOf(cacheObj.state);
                }
                finally
                {
                    skipStateChange = false;
                }

                //now select the right city
                skipCityChanging = true;
                try
                {
                    //cbAddressCity.SelectedIndex = cbAddressCity.Items.IndexOf(cacheObj.city);                    
                    txtCity.Text = cacheObj.city;
                }
                finally
                {
                    skipCityChanging = false;
                }
            }
        }
    }
}
