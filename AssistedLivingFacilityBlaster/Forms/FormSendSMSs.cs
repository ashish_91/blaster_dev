﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistedLivingFacilityBlaster
{
    public partial class FormSendSMSs : Form
    {
        public enum SMSType
        {
            introducePatientToAL = 0,
            askALQuestions = 1
        }

        private const string fullUrlTemplate = "http://referrals.seniorplanningcare.org/patients-portal/{1}?eventId={0}";

        //Input params

        /// <summary>
        /// The faiclities which will be text messaged
        /// </summary>
        public List<Db.FacilityDetails> facilitiesSMSsDetails { get; set; }

        /// <summary>
        /// The logger object
        /// </summary>
        private Logger logger = LogManager.GetLogger("smsToCareHomes");

        /// <summary>
        /// !!! Relevant only for introducePatientToAL !!!
        /// The patient data filled in a way to easy fill the emails and text messages
        /// </summary>
        public Helper.PatientTemplateData patientTemplateData { get; set; }

        /// <summary>
        /// !!! Relevant only for introducePatientToAL !!!
        /// The patient for which the AL will be notified
        /// </summary>
        public Patients patient { get; set; }

        /// <summary>
        /// !!! Relevant only for askALQuestions !!!
        /// The info for the question campaign which will be used
        /// </summary>
        public Guid questionCampaignId { get; set; }

        /// <summary>
        /// the type of sms that will be used, for the moment
        /// </summary>
        public SMSType smsType;

        private string GetPageFromSMStype(SMSType smsType)
        {
            switch(smsType)
            {
                case SMSType.introducePatientToAL: return "patient-info.html";
                case SMSType.askALQuestions: return "facility-q.html";
                default: throw new NotImplementedException($"GetPageFromSMStype.smsType: {smsType}");
            }
        }

        private int GetActionIdFromSmsType(SMSType smsType)
        {            
            switch (smsType)
            {
                case SMSType.introducePatientToAL: return Db.ReportSendSms;
                case SMSType.askALQuestions: return Db.ReportSendQuestionsAsSms;
                default: throw new NotImplementedException($"GetActionIdFromSmsType.smsType: {smsType}");
            }
        }

        public FormSendSMSs()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSendSMSs_Click(object sender, EventArgs e)
        {            
            logger.Info(" === btnSendSMSs_Click");
            this.Enabled = false;
            int storedCount = 0;
            try
            {
                lblStatus.Text = "Pushing to queue, please wait...";
                lblStatus.Visible = true;
                Application.DoEvents();

                logger.Info("Free text: " + txtSmsText.Text);
                logger.Info("Fixed text: " + txtSmsFIxedText.Text);

                using (var db = new DbEntities())
                {
                    logger.Info("Start iterating....");
                    foreach (DataGridViewRow row in gridFacilities.Rows)
                    {
                        storedCount++;
                        string phonesStr = row.Cells["Phones"]?.Value?.ToString();
                        logger.Info("   PhonesStr: " + phonesStr);

                        row.Cells["Result"].Value = String.Empty;
                        Application.DoEvents();

                        if (String.IsNullOrWhiteSpace(phonesStr))
                        {
                            logger.Info("     no phone here");
                            row.Cells["Result"].Value = "ERROR: no phone";
                            Application.DoEvents();
                            continue;
                        }

                        string[] phones = phonesStr.Split(new string[] { "; " }, StringSplitOptions.RemoveEmptyEntries);
                        logger.Info("     phones detected: " + phones.Length);

                        Guid facilityId = Guid.Parse(row.Cells["Id"].Value.ToString());
                        foreach (var phone in phones)
                        {
                            Guid invitationId = Guid.NewGuid();
                            string sendToPhone = phone.Trim();

                            string fullSmsText = string.Empty;
                            Guid relationId = GetRelationId(smsType);
                            int actionId = GetActionIdFromSmsType(smsType);

                            try
                            {
                                logger.Info("Short from " + fullUrlTemplate);

                                switch (smsType)
                                {
                                    case SMSType.introducePatientToAL:
                                        logger.Info($"  patientId: {patient.Id}");
                                        break;

                                    case SMSType.askALQuestions:
                                        logger.Info($"  question campaign id: {questionCampaignId}");
                                        break;
                                }

                                logger.Info($"  facilityId: {facilityId}");
                                logger.Info($"  smsType: {smsType}");

                                string fullUrl = String.Format(fullUrlTemplate, invitationId, GetPageFromSMStype(smsType));
                                logger.Info($"  fullUrl: {fullUrl}");

                                logger.Info($"     start sending sms from {Helper.TwilioSmsNo} to {sendToPhone}");
                                fullSmsText = txtSmsText.Text + txtSmsFIxedText.Text;
                                logger.Info($"     with sms text: {fullSmsText}");

                                logger.Info($"     res: Ok\n");
                                row.Cells["Result"].Value += "Queued;";
                                row.Cells["IsSendSuccess"].Value = 1;

                                if (smsType == SMSType.introducePatientToAL)
                                {
                                    Helper.StoreToPatientsRegister(patientTemplateData, sendToPhone, fullSmsText, false, false);
                                }

                                Guid actionReportId = Guid.NewGuid();
                                db.ActionsReport.Add(new ActionsReport()
                                {
                                    ActionId = actionId,
                                    Body = fullSmsText,
                                    CorrelationId = null,
                                    ErrorText = null,
                                    FacilityId = facilityId,
                                    Id = actionReportId,
                                    PatientId = relationId,
                                    Recipient = sendToPhone,
                                    Status = 0,
                                    Subject = "queued",
                                    UserId = Db.user.Id
                                });

                                db.QueueSms.Add(new QueueSms()
                                {
                                    ActionsReport = actionReportId,
                                    Created = DateTime.Now,
                                    FacilityId = facilityId,
                                    Id = Guid.NewGuid(),
                                    InterestedPatientsId = invitationId,
                                    LongUrl = fullUrl,
                                    PatientId = relationId,
                                    Recipient = sendToPhone,
                                    ShortUrl = "queued",
                                    Status = "Pending",
                                    StatusId = 0,
                                    TextMessage = fullSmsText,
                                    Updated = DateTime.Now,
                                    UserId = Db.user.Id
                                });
                            }
                            catch (Exception ex)
                            {
                                row.Cells["Result"].Value += ex.Message + ";";
                                row.Cells["IsSendSuccess"].Value = -1;

                                db.InterestedPatients.Add(new InterestedPatients()
                                {
                                    FacilityId = facilityId,
                                    HaveInterest = 0,
                                    Id = invitationId,
                                    InvitationSent = DateTime.Now,
                                    PatientId = relationId,
                                    Error = ex.Message,
                                    PhoneSentTo = sendToPhone,
                                    SmsText = fullSmsText
                                });

                                db.ActionsReport.Add(new ActionsReport()
                                {
                                    ActionId = actionId,
                                    Body = fullSmsText,
                                    CorrelationId = null,
                                    ErrorText = ex.Message,
                                    FacilityId = facilityId,
                                    Id = Guid.NewGuid(),
                                    PatientId = relationId,
                                    Recipient = sendToPhone,
                                    Status = -1,
                                    Subject = null,
                                    UserId = Db.user.Id
                                });
                            }

                            Application.DoEvents();
                        }

                        if (storedCount > 50)
                        {                            
                            lblStatus.Text = $"Pushing to queue {storedCount} items, please wait...";
                            Application.DoEvents();
                            db.SaveChanges();
                            storedCount = 0;
                        }
                    }

                    lblStatus.Text = $"Pushing to queue {storedCount} items, please wait...";
                    Application.DoEvents();
                    db.SaveChanges();
                }

                lblStatus.Text = $"Refreshing the queue...";
                Application.DoEvents();
                DbCache.formMain.RefreshSmsQueue();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                lblStatus.Visible = false;
                this.Enabled = true;
            }
            
        }

        private Guid GetRelationId(SMSType smsType)
        {
            switch (smsType)
            {
                case SMSType.introducePatientToAL:
                    return patient.Id;

                case SMSType.askALQuestions:
                    return questionCampaignId;                    
            }

            throw new NotImplementedException($"GetRelationId, invalid smsType: {smsType}");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            try
            {
                switch(smsType)
                {
                    case SMSType.askALQuestions:
                        txtSmsText.Text = "Do you want to answer few quick quetions?";                        
                        txtSmsFIxedText.Text = " Please click the link below. {SHORT_URL} .";
                        break;

                    case SMSType.introducePatientToAL:
                        txtSmsText.Text = "{POC_FIRST} {POC_LAST} for {PATIENT_FIRST} {PATIENT_LAST} {BUDGET}/month ";
                        txtSmsText.Text = Helper.ReplacePatientTemplate(txtSmsText.Text, patientTemplateData);

                        txtSmsFIxedText.Text = "For more information, or to accept this client, please click the link below. {SHORT_URL} .";
                        break;

                    default: throw new NotImplementedException($"Invalid smsType: {smsType} when loading texts!");
                }
                
                foreach (var fac in facilitiesSMSsDetails)
                {
                    gridFacilities.Rows.Add(
                        fac.id,
                        fac.name,
                        fac.phones == null ? "" : string.Join("; ", fac.phones),
                        null,
                        0
                    );
                }
                
                RecalcChars();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show(ex.Message);
            }
        }

        private void gridFacilities_DoubleClick(object sender, EventArgs e)
        {
            if (gridFacilities.SelectedRows.Count < 1)
                return;

            string res = gridFacilities.SelectedRows[0].Cells["Result"].Value?.ToString();
            int success = int.Parse(gridFacilities.SelectedRows[0].Cells["IsSendSuccess"].Value.ToString());

            switch(success)
            {
                case 0:
                    MessageBox.Show("Looks like this facility has not received the sms yet, press the \"Send SMSs\" button bellow");
                    break;

                case -1:
                    MessageBox.Show("Error occured while sending: " + res);
                    break;

                case 1:
                    string[] urls = res.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach(string url in urls)
                    {
                        System.Diagnostics.Process.Start(url.Replace("DUPL ", ""));
                    }

                    break;
            }
        }

        private void FormSendSMSs_Shown(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void RecalcChars()
        {
            //int totalChars = txtSmsText.Text.Length + txtSmsFIxedText.Text.Replace("{SHORT_URL}", "https://goo.gl/1F6bU8 ").Length;
            int totalChars = txtSmsText.Text.Length + txtSmsFIxedText.Text.Replace("{SHORT_URL}", "http://you.com.se/flb7s2y5 ").Length; 


            int smsCount = 1;
            int tempTotalChars = totalChars - 160;
            int toDelete = 0;
            while (tempTotalChars > 0)
            {
                toDelete = tempTotalChars;
                tempTotalChars -= 146;
                smsCount++;
            }
            
            lblSMSCount.Text = smsCount.ToString();
            lblSMScharsCount.Text = totalChars.ToString();
            lblCharsToDelete.Text = toDelete.ToString();
        }

        private void txtSmsFIxedText_TextChanged(object sender, EventArgs e)
        {
            RecalcChars();
        }

        private void FormSendSMSs_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }
    }
}
