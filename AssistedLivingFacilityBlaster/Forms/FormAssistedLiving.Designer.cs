﻿namespace AssistedLivingFacilityBlaster.Forms
{
    partial class FormAssistedLiving
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblProgress = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtAssistedLivingPrice = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.txtWebSite = new System.Windows.Forms.TextBox();
            this.txtAlName = new System.Windows.Forms.TextBox();
            this.txtNotes = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtIndependentLivingPrice = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtZip = new System.Windows.Forms.TextBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbAddressState = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.edAddressStreet = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMemoryCarePrice = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtSkilledNursingCarePrice = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEmail1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEmail2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEmail3 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtEmail4 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtEmail5 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.checkALTSC = new System.Windows.Forms.CheckBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssistedLivingPrice)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIndependentLivingPrice)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMemoryCarePrice)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSkilledNursingCarePrice)).BeginInit();
            this.SuspendLayout();
            // 
            // lblProgress
            // 
            this.lblProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblProgress.AutoSize = true;
            this.lblProgress.Location = new System.Drawing.Point(26, 308);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(58, 13);
            this.lblProgress.TabIndex = 38;
            this.lblProgress.Text = "lblProgress";
            this.lblProgress.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(15, 303);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(405, 23);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.TabIndex = 37;
            this.progressBar.Visible = false;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Location = new System.Drawing.Point(426, 303);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 35;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(511, 303);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 34;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(597, 303);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 33;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtAssistedLivingPrice);
            this.groupBox7.Location = new System.Drawing.Point(183, 172);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(150, 58);
            this.groupBox7.TabIndex = 47;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Assisted Living Price ($)";
            // 
            // txtAssistedLivingPrice
            // 
            this.txtAssistedLivingPrice.Location = new System.Drawing.Point(15, 21);
            this.txtAssistedLivingPrice.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtAssistedLivingPrice.Name = "txtAssistedLivingPrice";
            this.txtAssistedLivingPrice.Size = new System.Drawing.Size(114, 20);
            this.txtAssistedLivingPrice.TabIndex = 13;
            this.txtAssistedLivingPrice.ThousandsSeparator = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 55);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 13);
            this.label11.TabIndex = 40;
            this.label11.Text = "Web Site:";
            // 
            // txtWebSite
            // 
            this.txtWebSite.Location = new System.Drawing.Point(13, 72);
            this.txtWebSite.Name = "txtWebSite";
            this.txtWebSite.Size = new System.Drawing.Size(134, 20);
            this.txtWebSite.TabIndex = 43;
            // 
            // txtAlName
            // 
            this.txtAlName.Location = new System.Drawing.Point(12, 25);
            this.txtAlName.Name = "txtAlName";
            this.txtAlName.Size = new System.Drawing.Size(135, 20);
            this.txtAlName.TabIndex = 39;
            // 
            // txtNotes
            // 
            this.txtNotes.Location = new System.Drawing.Point(156, 25);
            this.txtNotes.Multiline = true;
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtNotes.Size = new System.Drawing.Size(177, 132);
            this.txtNotes.TabIndex = 44;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 45;
            this.label1.Text = "Assisted living Name:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtIndependentLivingPrice);
            this.groupBox6.Location = new System.Drawing.Point(14, 172);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(153, 58);
            this.groupBox6.TabIndex = 46;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "IndependentL iving price ($)";
            // 
            // txtIndependentLivingPrice
            // 
            this.txtIndependentLivingPrice.Location = new System.Drawing.Point(15, 21);
            this.txtIndependentLivingPrice.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtIndependentLivingPrice.Name = "txtIndependentLivingPrice";
            this.txtIndependentLivingPrice.Size = new System.Drawing.Size(114, 20);
            this.txtIndependentLivingPrice.TabIndex = 13;
            this.txtIndependentLivingPrice.ThousandsSeparator = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtZip);
            this.groupBox3.Controls.Add(this.txtCity);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.cbAddressState);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.edAddressStreet);
            this.groupBox3.Location = new System.Drawing.Point(339, 20);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(332, 137);
            this.groupBox3.TabIndex = 41;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Address:";
            // 
            // txtZip
            // 
            this.txtZip.Location = new System.Drawing.Point(17, 38);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(131, 20);
            this.txtZip.TabIndex = 12;
            this.txtZip.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtZip_KeyUp);
            // 
            // txtCity
            // 
            this.txtCity.Enabled = false;
            this.txtCity.Location = new System.Drawing.Point(17, 92);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(131, 20);
            this.txtCity.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Zip:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(167, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "State:";
            // 
            // cbAddressState
            // 
            this.cbAddressState.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbAddressState.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbAddressState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAddressState.Enabled = false;
            this.cbAddressState.FormattingEnabled = true;
            this.cbAddressState.Location = new System.Drawing.Point(170, 38);
            this.cbAddressState.Name = "cbAddressState";
            this.cbAddressState.Size = new System.Drawing.Size(146, 21);
            this.cbAddressState.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 77);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "City:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(167, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Street:";
            // 
            // edAddressStreet
            // 
            this.edAddressStreet.Location = new System.Drawing.Point(170, 93);
            this.edAddressStreet.Name = "edAddressStreet";
            this.edAddressStreet.Size = new System.Drawing.Size(146, 20);
            this.edAddressStreet.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(156, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 48;
            this.label2.Text = "Notes:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMemoryCarePrice);
            this.groupBox1.Location = new System.Drawing.Point(339, 172);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(150, 58);
            this.groupBox1.TabIndex = 49;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Memory Care Price ($)";
            // 
            // txtMemoryCarePrice
            // 
            this.txtMemoryCarePrice.Location = new System.Drawing.Point(15, 21);
            this.txtMemoryCarePrice.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtMemoryCarePrice.Name = "txtMemoryCarePrice";
            this.txtMemoryCarePrice.Size = new System.Drawing.Size(114, 20);
            this.txtMemoryCarePrice.TabIndex = 13;
            this.txtMemoryCarePrice.ThousandsSeparator = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtSkilledNursingCarePrice);
            this.groupBox2.Location = new System.Drawing.Point(509, 172);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(162, 58);
            this.groupBox2.TabIndex = 50;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Skilled Nursing CarePrice ($)";
            // 
            // txtSkilledNursingCarePrice
            // 
            this.txtSkilledNursingCarePrice.Location = new System.Drawing.Point(15, 21);
            this.txtSkilledNursingCarePrice.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtSkilledNursingCarePrice.Name = "txtSkilledNursingCarePrice";
            this.txtSkilledNursingCarePrice.Size = new System.Drawing.Size(114, 20);
            this.txtSkilledNursingCarePrice.TabIndex = 13;
            this.txtSkilledNursingCarePrice.ThousandsSeparator = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 249);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 51;
            this.label3.Text = "Email 1:";
            // 
            // txtEmail1
            // 
            this.txtEmail1.Location = new System.Drawing.Point(14, 265);
            this.txtEmail1.Name = "txtEmail1";
            this.txtEmail1.Size = new System.Drawing.Size(113, 20);
            this.txtEmail1.TabIndex = 52;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(145, 249);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 53;
            this.label4.Text = "Email 2:";
            // 
            // txtEmail2
            // 
            this.txtEmail2.Location = new System.Drawing.Point(148, 265);
            this.txtEmail2.Name = "txtEmail2";
            this.txtEmail2.Size = new System.Drawing.Size(110, 20);
            this.txtEmail2.TabIndex = 54;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(276, 249);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 55;
            this.label5.Text = "Email 3:";
            // 
            // txtEmail3
            // 
            this.txtEmail3.Location = new System.Drawing.Point(279, 265);
            this.txtEmail3.Name = "txtEmail3";
            this.txtEmail3.Size = new System.Drawing.Size(109, 20);
            this.txtEmail3.TabIndex = 56;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(406, 249);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 57;
            this.label6.Text = "Email 4:";
            // 
            // txtEmail4
            // 
            this.txtEmail4.Location = new System.Drawing.Point(409, 265);
            this.txtEmail4.Name = "txtEmail4";
            this.txtEmail4.Size = new System.Drawing.Size(123, 20);
            this.txtEmail4.TabIndex = 58;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(550, 248);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 13);
            this.label12.TabIndex = 59;
            this.label12.Text = "Email 5:";
            // 
            // txtEmail5
            // 
            this.txtEmail5.Location = new System.Drawing.Point(553, 265);
            this.txtEmail5.Name = "txtEmail5";
            this.txtEmail5.Size = new System.Drawing.Size(118, 20);
            this.txtEmail5.TabIndex = 60;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Maroon;
            this.label13.Location = new System.Drawing.Point(266, 217);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(209, 13);
            this.label13.TabIndex = 61;
            this.label13.Text = "leave 0 for No, later I can add checkboxes";
            // 
            // checkALTSC
            // 
            this.checkALTSC.AutoSize = true;
            this.checkALTSC.Location = new System.Drawing.Point(13, 101);
            this.checkALTSC.Name = "checkALTSC";
            this.checkALTSC.Size = new System.Drawing.Size(60, 17);
            this.checkALTSC.TabIndex = 62;
            this.checkALTSC.Text = "ALTSC";
            this.checkALTSC.UseVisualStyleBackColor = true;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(13, 137);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(135, 20);
            this.txtPhone.TabIndex = 63;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 122);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 64;
            this.label14.Text = "Phone:";
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FormAssistedLiving
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 333);
            this.Controls.Add(this.txtPhone);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.checkALTSC);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtEmail5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtEmail4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtEmail3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtEmail2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtEmail1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtWebSite);
            this.Controls.Add(this.txtAlName);
            this.Controls.Add(this.txtNotes);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.lblProgress);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClose);
            this.Name = "FormAssistedLiving";
            this.Text = "Assisted Living";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAssistedLiving_FormClosing);
            this.Load += new System.EventHandler(this.FormAssistedLiving_Load);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAssistedLivingPrice)).EndInit();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtIndependentLivingPrice)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMemoryCarePrice)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSkilledNursingCarePrice)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblProgress;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.NumericUpDown txtAssistedLivingPrice;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtWebSite;
        private System.Windows.Forms.TextBox txtAlName;
        private System.Windows.Forms.TextBox txtNotes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.NumericUpDown txtIndependentLivingPrice;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtZip;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbAddressState;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox edAddressStreet;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown txtMemoryCarePrice;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown txtSkilledNursingCarePrice;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEmail1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEmail2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtEmail3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtEmail4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtEmail5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox checkALTSC;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Timer timer1;
    }
}