﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistedLivingFacilityBlaster.Forms
{
    public partial class FormAssistedLiving : Form
    {
        public enum Mode
        {
            create, edit, view
        }

        public Mode mode = Mode.view;
        public Guid assistedLivingId { get; set; }
        Logger logger = LogManager.GetLogger("assistedLivingInfo");
        public bool doneChanges = false;

        public FormAssistedLiving()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (doneChanges) DialogResult = DialogResult.Yes;
            else
                DialogResult = DialogResult.No;

            Close();
        }



        private bool ValidateData()
        {
            try
            {
                if (string.IsNullOrEmpty(txtAlName.Text))
                {
                    MessageBox.Show("Please enter the assisted living name!");
                    if (txtAlName.CanFocus) txtAlName.Focus();
                    return false;
                }

                string zip = txtZip.Text.Trim();
                var cacheObj = DbCache.fullCache.AsParallel().Where(w => w.zip == zip).FirstOrDefault();
                if (cacheObj == null)
                {
                    DialogResult dialogResult = MessageBox.Show("The zip is not found in the database. Adding it anyway?", "No valid zip?", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.No)
                    {
                        return false;
                    }

                    if (txtZip.CanFocus) txtZip.Focus();
                }
                
                if (
                    string.IsNullOrWhiteSpace(txtEmail1.Text) &&
                    string.IsNullOrWhiteSpace(txtEmail2.Text) &&
                    string.IsNullOrWhiteSpace(txtEmail3.Text) &&
                    string.IsNullOrWhiteSpace(txtEmail4.Text) &&
                    string.IsNullOrWhiteSpace(txtEmail5.Text))
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure you want to save the assisted living with no emails at all?", "No emails?", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.No)
                    {
                        return false;
                    }
                }

                if (!Helper.IsEmailValid(txtEmail1.Text, true))
                {
                    MessageBox.Show("Email 1 is invalid!");
                    if (txtEmail1.CanFocus) txtEmail1.Focus();
                    return false;
                }

                if (!Helper.IsEmailValid(txtEmail2.Text, true))
                {
                    MessageBox.Show("Email 2 is invalid!");
                    if (txtEmail2.CanFocus) txtEmail2.Focus();
                    return false;
                }

                if (!Helper.IsEmailValid(txtEmail3.Text, true))
                {
                    MessageBox.Show("Email 3 is invalid!");
                    if (txtEmail3.CanFocus) txtEmail3.Focus();
                    return false;
                }

                if (!Helper.IsEmailValid(txtEmail4.Text, true))
                {
                    MessageBox.Show("Email 4 is invalid!");
                    if (txtEmail4.CanFocus) txtEmail4.Focus();
                    return false;
                }

                if (!Helper.IsEmailValid(txtEmail5.Text, true))
                {
                    MessageBox.Show("Email 5 is invalid!");
                    if (txtEmail5.CanFocus) txtEmail5.Focus();
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error while validating: " + ex.Message);
                return false;
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateData())
                    return;

                logger.Info("Saving data: ");

                logger.Info("  " + edAddressStreet.Text);                
                logger.Info("  " + txtCity.Text);
                logger.Info("  " + txtAlName.Text);                
                logger.Info("  " + txtNotes.Text);
                logger.Info("  " + cbAddressState.Text);                
                logger.Info("  " + txtWebSite.Text);
                logger.Info("  " + txtZip.Text);
                logger.Info("  " + txtEmail1.Text);
                logger.Info("  " + txtEmail2.Text);
                logger.Info("  " + txtEmail3.Text);
                logger.Info("  " + txtEmail4.Text);
                logger.Info("  " + txtEmail5.Text);
                logger.Info("  " + txtPhone.Text);
                logger.Info("  " + txtIndependentLivingPrice.Value);
                logger.Info("  " + txtAssistedLivingPrice.Value);
                logger.Info("  " + txtMemoryCarePrice.Value);
                logger.Info("  " + txtSkilledNursingCarePrice.Value);

                using (var db = new DbEntities())
                {
                    Guid id = Guid.Empty;

                    if (mode == Mode.create)
                    {
                        id = Guid.NewGuid();
                        assistedLivingId = id;
                        var fc = new AssistedLiving()
                        {
                            AddressLine = edAddressStreet.Text,
                            ALTSC = checkALTSC.Checked,
                            AssistedLivingPrice = txtAssistedLivingPrice.Value == 0 ? null : (decimal?)txtAssistedLivingPrice.Value,
                            Email1 = txtEmail1.Text,
                            Email2 = txtEmail2.Text,
                            Email3 = txtEmail3.Text,
                            Email4 = txtEmail4.Text,
                            Email5 = txtEmail5.Text,
                            Id = id,
                            IndependentLivingPrice = txtIndependentLivingPrice.Value == 0 ? null : (decimal?)txtIndependentLivingPrice.Value,
                            MemoryCarePrice = txtMemoryCarePrice.Value == 0 ? null : (decimal?)txtMemoryCarePrice.Value,
                            Name = txtAlName.Text,
                            Notes = txtNotes.Text,
                            Phone = txtPhone.Text,
                            SkilledNursingCarePrice = txtSkilledNursingCarePrice.Value == 0 ? null : (decimal?)txtSkilledNursingCarePrice.Value,
                            Updated = DateTime.Now,
                            WebSite = txtWebSite.Text,
                            Zip = txtZip.Text
                        };
                        
                        db.AssistedLiving.Add(fc);
                    }
                    else
                    if (mode == Mode.edit)
                    {
                        id = assistedLivingId;

                        var f = db.AssistedLiving.Where(w => w.Id == assistedLivingId).Single();

                        f.AddressLine = edAddressStreet.Text;
                        f.ALTSC = checkALTSC.Checked;
                        f.AssistedLivingPrice = txtAssistedLivingPrice.Value == 0 ? null : (decimal?)txtAssistedLivingPrice.Value;
                        f.Email1 = txtEmail1.Text;
                        f.Email2 = txtEmail2.Text;
                        f.Email3 = txtEmail3.Text;
                        f.Email4 = txtEmail4.Text;
                        f.Email5 = txtEmail5.Text;                        
                        f.IndependentLivingPrice = txtIndependentLivingPrice.Value == 0 ? null : (decimal?)txtIndependentLivingPrice.Value;
                        f.MemoryCarePrice = txtMemoryCarePrice.Value == 0 ? null : (decimal?)txtMemoryCarePrice.Value;
                        f.Name = txtAlName.Text;
                        f.Notes = txtNotes.Text;
                        f.Phone = txtPhone.Text;
                        f.SkilledNursingCarePrice = txtSkilledNursingCarePrice.Value == 0 ? null : (decimal?)txtSkilledNursingCarePrice.Value;
                        f.Updated = DateTime.Now;
                        f.WebSite = txtWebSite.Text;
                        f.Zip = txtZip.Text;
                    }
                    
                    db.SaveChanges();
                    doneChanges = true;
                    logger.Info("  SUCCESS");
                }

                if (mode == Mode.create)
                {
                    MessageBox.Show("The assisted living was created successfully!");
                    
                    mode = Mode.view;
                    RespectMode();                    
                }
                else if (mode == Mode.edit)
                {
                    MessageBox.Show("The assisted living was updated successfully!");
                    Close();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Save assisted living");
                MessageBox.Show("Error occured during saving: " + ex.Message);
            }
        }

        private string ModeToString(Mode mode)
        {
            if (mode == Mode.create) return "CREATE NEW";
            if (mode == Mode.edit) return "EDIT";
            if (mode == Mode.view) return "VIEW";

            return "UNKNOWN";
        }

        private void RespectMode()
        {
            this.Text = $"Assisted living Details ({ModeToString(mode)})";

            txtAlName.Enabled = mode != Mode.view;            
            txtNotes.Enabled = mode != Mode.view;
            txtPhone.Enabled = mode != Mode.view;
            txtWebSite.Enabled = mode != Mode.view;            
            txtZip.Enabled = mode != Mode.view;
            edAddressStreet.Enabled = mode != Mode.view;
            checkALTSC.Enabled = mode != Mode.view;
            txtEmail1.Enabled = mode != Mode.view;
            txtEmail2.Enabled = mode != Mode.view;
            txtEmail3.Enabled = mode != Mode.view;
            txtEmail4.Enabled = mode != Mode.view;
            txtEmail5.Enabled = mode != Mode.view;
            txtIndependentLivingPrice.Enabled = mode != Mode.view;
            txtAssistedLivingPrice.Enabled = mode != Mode.view;
            txtMemoryCarePrice.Enabled = mode != Mode.view;
            txtSkilledNursingCarePrice.Enabled = mode != Mode.view;

            if (mode == Mode.create)
            {                
                btnDelete.Visible = false;
            }

            if (mode == Mode.view)
            {
                btnDelete.Visible = true;
                btnSave.Visible = false;                
            }

            if (mode == Mode.edit)
            {
                btnDelete.Visible = true;
                btnSave.Visible = true;                
            }
        }

        #region zip selection
        bool skipZipChanging = false;
        private void cbAddressZip_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (skipZipChanging)
                return;

            string zip = txtZip.Text.Trim();
            var cacheObj = DbCache.fullCache.AsParallel().Where(w => w.zip == zip).FirstOrDefault();
            if (cacheObj == null)
            {
                MessageBox.Show("Invalid zip! Please select zips only from the list");
                return;
            }

            //select state
            skipStateChange = true;
            try
            {
                cbAddressState.SelectedIndex = cbAddressState.Items.IndexOf(cacheObj.state);
            }
            finally
            {
                skipStateChange = false;
            }

            //now select the right city
            skipCityChanging = true;
            try
            {
                //cbAddressCity.SelectedIndex = cbAddressCity.Items.IndexOf(cacheObj.city);
                txtCity.Text = cacheObj.city;
            }
            finally
            {
                skipCityChanging = false;
            }
        }

        bool skipStateChange = false;

        private void cbAddressState_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (skipStateChange)
                return;

            skipCityChanging = true;
            try
            {
                //RefreshCities();
                //cbAddressCity.SelectedIndex = -1;
                //cbAddressCity.Text = String.Empty;                
            }
            finally
            {
                skipCityChanging = false;
            }

            skipZipChanging = true;
            try
            {
                //cbAddressZip.SelectedIndex = -1;
                txtZip.Text = String.Empty;
            }
            finally
            {
                skipZipChanging = false;
            }
        }

        bool skipCityChanging = false;
        private void cbAddressCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (skipCityChanging)
                return;

            skipZipChanging = true;
            try
            {
                //cbAddressZip.SelectedIndex = -1;
                txtZip.Text = String.Empty;
            }
            finally
            {
                skipZipChanging = false;
            }
        }

        #endregion

        private void FormAssistedLiving_Load(object sender, EventArgs e)
        {            
            RespectMode();
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Enabled = false;
            lblProgress.Visible = true;
            progressBar.Visible = true;
            try
            {
                timer1.Enabled = false;

                SetProgressHint("Load zips...");                
                SetProgressHint("Load states...");
                cbAddressState.Items.AddRange(DbCache.states.ToArray());
                cbAddressState.SelectedIndex = cbAddressState.Items.IndexOf("Arizona");

                if (mode == Mode.edit || mode == Mode.view)
                {

                    SetProgressHint("Load facility info...");
                    using (var db = new DbEntities())
                    {
                        var al = db.AssistedLiving.SingleOrDefault(s => s.Id == assistedLivingId);

                        if (al == null)
                        {
                            MessageBox.Show("No databse info for that AL? That's very odd. AL Id: " + assistedLivingId);
                            logger.Error("No databse info for that AL? That's very odd. AL Id: " + assistedLivingId);
                            return;
                        }

                        txtAlName.Text = al.Name;
                        txtPhone.Text = al.Phone;
                        txtEmail1.Text = al.Email1;
                        txtEmail2.Text = al.Email2;
                        txtEmail3.Text = al.Email3;
                        txtEmail4.Text = al.Email4;
                        txtEmail5.Text = al.Email5;
                        txtWebSite.Text = al.WebSite;
                        checkALTSC.Checked = al.ALTSC;
                        txtNotes.Text = al.Notes;
                        txtZip.Text = al.Zip;
                        txtIndependentLivingPrice.Value = al.IndependentLivingPrice ?? 0;
                        txtAssistedLivingPrice.Value = al.AssistedLivingPrice ?? 0;
                        txtMemoryCarePrice.Value = al.MemoryCarePrice ?? 0;
                        txtSkilledNursingCarePrice.Value = al.SkilledNursingCarePrice ?? 0;
                        edAddressStreet.Text = al.AddressLine;

                        txtZip_KeyUp(sender, null);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error while loading the form: " + ex.Message);
            }
            finally
            {
                this.Enabled = true;
                progressBar.Visible = false;
                lblProgress.Visible = false;
            }
        }

        private void SetProgressHint(string hint)
        {
            logger.Info(hint);
            lblProgress.Text = hint;
            Application.DoEvents();
        }

        private void FormAssistedLiving_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (doneChanges) DialogResult = DialogResult.Yes;
            else
                DialogResult = DialogResult.No;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete the selected Assisted living?", "Delete Assisted Living", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.No)
                {
                    return;
                }

                try
                {
                    using (var db = new DbEntities())
                    {
                        SetProgressHint("Get the id");

                        SetProgressHint("Deleting the AL");
                        db.AssistedLiving.Remove(db.AssistedLiving.Where(w => w.Id == assistedLivingId).Single());
                        
                        db.SaveChanges();
                    }

                    MessageBox.Show("The Assisted living has been successfuly deleted!");
                    doneChanges = true;
                    Close();
                }
                finally
                {

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error while deleting Assisted living: " + ex.Message);
            }
        }

        private void txtZip_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtZip.Text.Length == 5)
            {
                string zip = txtZip.Text.Trim();
                var cacheObj = DbCache.fullCache.AsParallel().Where(w => w.zip == zip).FirstOrDefault();
                if (cacheObj == null)
                {
                    MessageBox.Show("Zip not found in the database!");
                    return;
                }

                //select state
                skipStateChange = true;
                try
                {
                    cbAddressState.SelectedIndex = cbAddressState.Items.IndexOf(cacheObj.state);
                }
                finally
                {
                    skipStateChange = false;
                }

                //now select the right city
                skipCityChanging = true;
                try
                {
                    //cbAddressCity.SelectedIndex = cbAddressCity.Items.IndexOf(cacheObj.city);                    
                    txtCity.Text = cacheObj.city;
                }
                finally
                {
                    skipCityChanging = false;
                }
            }
        }
    }
}
