﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistedLivingFacilityBlaster
{
    public partial class FormAgent : Form
    {
        public Db.Mode mode = Db.Mode.view;
        public Guid userId { get; set; }

        private Users user;
        Logger logger = LogManager.GetLogger("agentInfo");

        public FormAgent()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (hasChanged)
                DialogResult = DialogResult.OK;
            else
                DialogResult = DialogResult.Cancel;

            Close();
        }

        private new bool Validate()
        {
            if (txtGmailAccount.Text.Length < 5)
            {
                MessageBox.Show("The account is too short!");
                if (txtGmailAccount.CanFocus) txtGmailAccount.Focus();
                return false;
            }

            if (txtGmailPassword.Text.Length < 5)
            {
                MessageBox.Show("The password is too short!");
                if (txtGmailPassword.CanFocus) txtGmailPassword.Focus();
                return false;
            }

            return true;
        }

        bool hasChanged = false;
        private void btnAction_Click(object sender, EventArgs e)
        {
            if (!Validate()) return;

            if (mode == Db.Mode.view) return;

            if (mode == Db.Mode.create)
            {
                userId = Guid.NewGuid();
            }
            user = new Users()
            {
                Created = DateTime.Now,
                Enabled = chkEnabled.Checked ? 1 : 0,
                Id = userId,
                IsAdmin = chkIsAdmin.Checked ? 1 : 0,
                LastLogin = new DateTime(1990, 1, 1),
                PasswordCrypted = Crypto.AES.Encrypt(txtGmailPassword.Text, "Th1M2gAStrongP2asss23Pword"),
                PasswordHash = Crypto.SHA512.ComputeHash(txtGmailPassword.Text),
                RealName = txtRealName.Text.Trim(),
                UserName = txtGmailAccount.Text.Trim()
            };

            try
            {
                Db.ManageUser(user, mode);
                if (mode == Db.Mode.create)
                {
                    MessageBox.Show("The agent has been added successfuly!");
                    DialogResult = DialogResult.OK;
                    Close();
                }
                else
                {
                    hasChanged = true;
                    MessageBox.Show("The agent has been edited successfuly!");
                    mode = Db.Mode.view;
                    RespectMode();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void RespectMode()
        {
            txtGmailAccount.Enabled = mode != Db.Mode.view;
            txtGmailPassword.Enabled = mode != Db.Mode.view;
            txtRealName.Enabled = mode != Db.Mode.view;
            chkEnabled.Enabled = mode != Db.Mode.view;
            chkIsAdmin.Enabled = mode != Db.Mode.view;
            btnAction.Visible = mode != Db.Mode.view;
            if (mode == Db.Mode.edit)
            {
                btnAction.Text = "Save";
            }

            if (mode == Db.Mode.create)
            {
                btnAction.Text = "Add";
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            RespectMode();
            timer1.Enabled = false;

            if (mode != Db.Mode.create)
            {
                user = Db.LoadUserInfo(userId);
                if (user == null)
                {
                    MessageBox.Show("The user can't be found, that's odd, contact Svetoslav");
                    btnAction.Enabled = false;
                    return;
                }

                txtGmailAccount.Text = user.UserName;
                txtGmailPassword.Text = Crypto.AES.Decrypt(user.PasswordCrypted, "Th1M2gAStrongP2asss23Pword");
                txtRealName.Text = user.RealName;
                chkEnabled.Checked = user.Enabled == 1;
                chkIsAdmin.Checked = user.IsAdmin == 1;
            } else
            {
                chkEnabled.Checked = true;
                chkIsAdmin.Checked = false;
            }

        }

        private void FormAgent_Shown(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }
    }
}
