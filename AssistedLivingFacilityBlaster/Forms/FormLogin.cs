﻿using AssistedLivingFacilityBlaster.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistedLivingFacilityBlaster
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {            
            if (txtGmailAccount.Text.Length < 5)
            {
                MessageBox.Show("The username is too short!");
                if (txtGmailAccount.CanFocus)
                    txtGmailAccount.Focus();

                return;                
            }

            if (txtPassword.Text.Length < 4)
            {
                MessageBox.Show("The password is too short!");
                if (txtPassword.CanFocus)
                    txtPassword.Focus();

                return;
            }
            
            try
            {
                Db.Login(txtGmailAccount.Text, txtPassword.Text);                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            
            if (chkRememberMe.Checked)
                WriteSessionId();

            HandleMainForm();            
        }

        public void HandleMainForm()
        {            
            this.Visible = false;

            FormMain main = new FormMain();
            DbCache.formMain = main;

            var res = main.ShowDialog();
            if (res == DialogResult.Cancel)
                Close();

            if (res == DialogResult.Abort)
            {
                if (System.IO.File.Exists("cookies"))
                    System.IO.File.Delete("cookies");

                txtGmailAccount.Text = "";
                txtPassword.Text = "";
                this.Visible = true;
            }
        }

        public void WriteSessionId()
        {
            System.IO.File.WriteAllText("cookies", Crypto.AES.Encrypt(Db.session.Id.ToString(), "th1s1sTheSessionCrypt0Pa$$w0rd"));
        }

        public bool ReadSessionId()
        {
            if (!System.IO.File.Exists("cookies"))
                return false;

            string sessionIdStr = Crypto.AES.Decrypt(System.IO.File.ReadAllText("cookies"), "th1s1sTheSessionCrypt0Pa$$w0rd");
            Guid sessionId;
            if (!Guid.TryParse(sessionIdStr, out sessionId))
                return false;

            Db.LoadSessionRelatedInfo(sessionId);

            return true;
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {
            try
            {
                if (ReadSessionId())
                {
                    HandleMainForm();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
