﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistedLivingFacilityBlaster
{
    public partial class FormSendMailToClient : Form
    {
        public List<Db.FacilityDetails> facilitiesMailDetails { get; set; }
        public Helper.PatientTemplateData templateData { get; set; }
        private Logger logger = LogManager.GetLogger("mailToClient");

        public FormSendMailToClient()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnSendEmails_Click(object sender, EventArgs e)
        {
            logger.Info(" === btnSendEmails_Click");
            this.Enabled = false;
            try
            {
                using (var db = new DbEntities())
                {
                    try
                    {
                        logger.Info("Subject: " + txtSubject.Text);
                        logger.Info("Body: " + txtBody.Text);

                        string email = templateData.PATIENT_EMAIL;

                        logger.Info($"  start sending from {Db.user.UserName} to {email}");

                        string res = Helper.SendGmail(
                            fromAddress: Db.user.UserName,
                            toAddress: email.Trim(),
                            body: txtBody.Text,
                            subject: txtSubject.Text,
                            isBodyHtml: true,
                            password: Crypto.AES.Decrypt(Db.user.PasswordCrypted, "Th1M2gAStrongP2asss23Pword")
                            );

                        logger.Info($"     res: {res}\n");


                        if (res == "OK")
                        {
                            Helper.StoreToPatientsRegister(templateData, email.Trim(), txtBody.Text, true, true);

                            db.ActionsReport.Add(new ActionsReport()
                            {
                                ActionId = Db.ReportSendEmailToClient,
                                Body = txtBody.Text,
                                CorrelationId = null,
                                ErrorText = null,
                                FacilityId = templateData.patientId,
                                Id = Guid.NewGuid(),
                                PatientId = templateData.patientId,
                                Recipient = email.Trim(),
                                Status = 1,
                                Subject = txtSubject.Text,
                                UserId = Db.user.Id
                            });

                            MessageBox.Show("The mail has been sent successfully");
                            db.SaveChanges();
                        }
                        else
                        {
                            throw new Exception(res);
                        }
                    }
                    catch (Exception ex)
                    {
                        db.ActionsReport.Add(new ActionsReport()
                        {
                            ActionId = Db.ReportSendEmailToClient,
                            Body = txtBody.Text,
                            CorrelationId = null,
                            ErrorText = ex.Message,
                            FacilityId = templateData.patientId,
                            Id = Guid.NewGuid(),
                            PatientId = templateData.patientId,
                            Recipient = templateData.PATIENT_EMAIL.Trim(),
                            Status = -1,
                            Subject = txtSubject.Text,
                            UserId = Db.user.Id
                        });
                        db.SaveChanges();
                        throw new Exception(ex.Message);
                    }


                }
            }
            catch (DbEntityValidationException ex)
            {
                string msg = string.Empty;
                foreach (var eve in ex.EntityValidationErrors)
                {
                    msg += String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:\n",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        msg += String.Format("- Property: \"{0}\", Error: \"{1}\"\n",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }

                logger.Error(msg);
                MessageBox.Show(msg);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Enabled = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            try
            {
                Helper.ValidateTemplates();

                txtSubject.Text = Helper.ReplacePatientTemplate(File.ReadAllText(@".\Templates\ClientEmail_DefaultSubject.txt"), templateData);
                txtBody.Text = Helper.ReplacePatientTemplate(File.ReadAllText(@".\Templates\ClientEmail_DefaultBody.txt"), templateData);
                DisplayHtml(txtBody.Text);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DisplayHtml(txtBody.Text);
        }

        private void DisplayHtml(string html)
        {
            webBrowser1.Navigate("about:blank");
            if (webBrowser1.Document != null)
            {
                webBrowser1.Document.Write(string.Empty);
            }
            webBrowser1.DocumentText = html;
        }

        private void FormSendMailToClient_Shown(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }
    }
}
