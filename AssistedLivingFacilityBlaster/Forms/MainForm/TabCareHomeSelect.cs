﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistedLivingFacilityBlaster
{
    public partial class FormMain : Form
    {
        private void btnAddNewCareHomePatient_Click(object sender, EventArgs e)
        {
            AddPatient(2); //CH
        }

        private void btnSelectOnlyNotNotified_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in gridSuitableCareHomes.Rows)
            {
                row.Cells["gridFacilitySelect"].Value = !bool.Parse(row.Cells["facilitiesNotified"].Value.ToString());
            }

            CalcSelectedFacilities();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in gridSuitableCareHomes.Rows)
            {
                row.Cells["gridFacilitySelect"].Value = bool.Parse(row.Cells["facilitiesNotified"].Value.ToString()) && row.Cells["facilitiesNotifiedStatus"].Value.ToString() == "OK";
            }

            CalcSelectedFacilities();
        }

        private void cbCareHomesPatientSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbCareHomesPatientSelect.SelectedIndex < 0)
                return;

            Guid patientId = Guid.Parse(cbCareHomesPatientSelect.SelectedValue.ToString());

            var selectedPatient = DbCache.allLoadedPatiensList.Single(s => s.Id == patientId);
            
            txtFilterPrivateRoomTo.Value = selectedPatient.PrivateRoomBudget ?? 1000000;
            txtFilterSemiPrivateRoomTo.Value = selectedPatient.SemiPrivateRoomBudget ?? 1000000;

            rbALTCSYes.Checked = selectedPatient.ALTCS == 1;
            rbALTCSNo.Checked = selectedPatient.ALTCS == 0;

            PrepareSendStyle();
        }

        private void btnFacilitiesRefresh_Click(object sender, EventArgs e)
        {
            logger.Info("Refresh care home clicked");
            SetProgressVisible(true);
            try
            {
                SetProgressHint("refreshing the care homes...");
                int selectedRow = -1;
                if (gridFacilities.SelectedRows.Count > 0)
                {
                    selectedRow = gridFacilities.SelectedRows[0].Index;
                }

                LoadCareHomes();

                if (gridFacilities.Rows.Count <= selectedRow)
                    selectedRow = gridFacilities.Rows.Count - 1;

                if (selectedRow >= 0)
                {
                    foreach (DataGridViewRow row in gridFacilities.Rows)
                    {
                        row.Selected = row.Index == selectedRow;
                        if (row.Selected) break;
                    }

                    if (gridFacilities.SelectedRows.Count > 0)
                        gridFacilities.FirstDisplayedScrollingRowIndex = gridFacilities.SelectedRows[0].Index;
                }

                logger.Info("  SUCCESS");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error while refreshing: " + ex.Message);
            }
            finally
            {
                SetProgressVisible(false);
            }
        }

        private void CalcSelectedFacilities()
        {
            int selectedCount = 0;
            foreach (DataGridViewRow row in gridSuitableCareHomes.Rows)
            {
                if (bool.Parse(row.Cells["gridFacilitySelect"]?.Value?.ToString()))
                {
                    selectedCount++;
                }
            }

            lblMainSelectedCount.Text = selectedCount.ToString();
        }

        private void PerformSearchOfMatchingCH(string zip, Patients selectedPatient)
        {
            logger.Info("=== Performing Matching CH ===");
            
            DbCache.ZipEntity selectedZip = DbCache.fullCache.AsParallel().Where(w => w.zip == zip).SingleOrDefault();

            if (selectedZip == null)
            {
                logger.Error("zip not found");
                MessageBox.Show("Zip not found in database!");
                return;
            }
            
            SetProgressVisible(true);
            try
            {
                SetProgressHint("Filtering by facility type...");
                List<CareHomes> suitableCareHomes = new List<CareHomes>();
                
                suitableCareHomes.AddRange(DbCache.allCareHomes.ToList());

                //Only for care homes
                suitableCareHomes = suitableCareHomes
                    .Where(w => (w.PrivateRoomPrice ?? 0) >= txtFilterPrivateRoomFrom.Value)
                    .Where(w => (w.PrivateRoomPrice ?? 0) <= txtFilterPrivateRoomTo.Value)
                    .Where(w => (w.SemiPrivateRoomPrice ?? 0) >= txtFilterSemiPrivateRoomFrom.Value)
                    .Where(w => (w.SemiPrivateRoomPrice ?? 0) <= txtFilterSemiPrivateRoomTo.Value)
                .ToList();

                //filter on ALTCS
                SetProgressHint("filtering by ALTSC...");
                int patientAltcs = rbALTCSYes.Checked ? 1 : 0;

                if (patientAltcs == 1)
                {
                    suitableCareHomes = suitableCareHomes.Where(w => w.ALTSC == 1).ToList();
                }

                SetProgressHint("filtering by distance...");
                //Get the zip of the pacient                


                //Convert radious from string to number
                string radiusText = cbRadius.Text;
                if (radiusText.Equals("All", StringComparison.InvariantCultureIgnoreCase))
                    radiusText = "0";
                else radiusText = radiusText.Replace(" miles", "");
                decimal radius = decimal.Parse(radiusText);

                //Get the geo location of the facilities so far
                var facilitiesZipBundle =
                    (from f in suitableCareHomes
                     join z in DbCache.fullCache on f.Zip equals z.zip
                     select new { z, f }).ToList();

                //fill suitableFacilities with lat and lng
                foreach (var b in facilitiesZipBundle)
                {
                    var sf = suitableCareHomes.Where(w => w.Id == b.f.Id).Single();
                    sf.Lng = (decimal)b.z.loc.Longitude;
                    sf.Alt = (decimal)b.z.loc.Latitude;
                }

                //Now remove all which does not fit the distance
                suitableCareHomes = Helper.GetAllZipsInRangeForCH(selectedZip.loc, radius, suitableCareHomes);

                List<ActionsReport> sendStatuses = null;
                List<InterestedPatients> interestedFacilities = null;

                using (var db = new DbEntities())
                {
                    sendStatuses = db.ActionsReport.Where(w => w.PatientId == selectedPatient.Id && w.ActionId == Db.ReportSendSms).ToList();
                    interestedFacilities = db.InterestedPatients.Where(w => w.PatientId == selectedPatient.Id).ToList();
                }

                SetProgressHint("Load all matching facilities to the grid");

                foreach (var careHome in suitableCareHomes)
                {
                    ActionsReport sendStatus = sendStatuses.Where(w => w.FacilityId == careHome.Id).OrderByDescending(o => o.Created).FirstOrDefault();

                    InterestedPatients interestedFacility = null;
                    if (interestedFacilities != null)
                    {
                        interestedFacility = interestedFacilities.Where(w => w.FacilityId == careHome.Id).OrderByDescending(o => o.InterestDateTime).FirstOrDefault();
                    }

                    gridSuitableCareHomes.Rows.Add
                    (
                        careHome.Id.ToString(),
                        true,
                        careHome.FacilityName,                        
                        careHome.ALTSC,
                        (careHome.Distance ?? -1).ToString("0.0"),
                        careHome.City,
                        careHome.PrivateRoomPrice ?? 0,
                        careHome.SemiPrivateRoomPrice ?? 0,
                        sendStatus != null,
                        interestedFacility != null && interestedFacility.HaveInterest == 1,
                        sendStatus?.Created.ToString("MM/dd/yyyy"),
                        (sendStatus != null ? sendStatus.ErrorText ?? "OK" : "")
                    );                    
                }

                CalcSelectedFacilities();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Search");
                MessageBox.Show("Error occured while searching: " + ex.Message);
            }
            finally
            {
                SetProgressVisible(false);
            }
        }

        private void btnSearch_Click_1(object sender, EventArgs e)
        {
            if (cbCareHomesPatientSelect.SelectedIndex < 0)
            {
                MessageBox.Show("Select a patient!");
                return;
            }

            Guid patientId = Guid.Parse(cbCareHomesPatientSelect.SelectedValue.ToString());

            var selectedPatient = DbCache.allLoadedPatiensList.Single(s => s.Id == patientId);

            gridSuitableCareHomes.Rows.Clear();
            PerformSearchOfMatchingCH(selectedPatient.AddrZip, selectedPatient);
            CalcSelectedFacilities();
        }

        private List<Guid> GetSelectedCareHomes(DataGridView grid)
        {
            List<Guid> facilitiesToNotify = new List<Guid>();

            foreach (DataGridViewRow row in grid.Rows)
            {
                if (bool.Parse(row.Cells["gridFacilitySelect"]?.Value?.ToString()))
                {
                    facilitiesToNotify.Add(Guid.Parse(row.Cells["facilitiesId"].Value.ToString()));
                }
            }

            if (facilitiesToNotify.Count <= 0)
            {
                return null;
            }

            return facilitiesToNotify;
        }
                
        private void btnSendClientEmail_Click(object sender, EventArgs e)
        {            
            if (cbCareHomesPatientSelect.SelectedIndex < 0)
            {
                MessageBox.Show("Select a patient for step 1!");
                return;
            }

            Guid patientId = Guid.Parse(cbCareHomesPatientSelect.SelectedValue.ToString());
            string patientName = cbCareHomesPatientSelect.SelectedText;

            var selectedPatient = DbCache.allLoadedPatiensList.Single(s => s.Id == patientId);

            try
            {                
                FormSendSMSs formSendSms = new FormSendSMSs();
                formSendSms.facilitiesSMSsDetails = Db.FillFacilitiesStructuredData(GetSelectedCareHomes(gridSuitableCareHomes));
                if (formSendSms.facilitiesSMSsDetails == null)
                {
                    MessageBox.Show("Please select at least one facility!");
                    return;
                }
                formSendSms.patientTemplateData = Helper.FillTemplateData(selectedPatient, formSendSms.facilitiesSMSsDetails);
                formSendSms.patient = selectedPatient;
                formSendSms.smsType = FormSendSMSs.SMSType.introducePatientToAL;
                formSendSms.ShowDialog();

                //refresh statuses
                List<ActionsReport> sendStatuses = null;
                using (var db = new DbEntities())
                {
                    sendStatuses = db.ActionsReport.Where(w => w.PatientId == selectedPatient.Id && w.ActionId == Db.ReportSendSms).ToList();
                }

                foreach (DataGridViewRow facility in gridSuitableCareHomes.Rows)
                {
                    Guid facilityId = Guid.Parse(facility.Cells["facilitiesId"].Value.ToString());
                    ActionsReport sendStatus = sendStatuses
                            .Where(w => w.FacilityId == facilityId)
                            .OrderByDescending(o => o.Created)
                            .FirstOrDefault();

                    facility.Cells["facilitiesNotified"].Value = sendStatus != null;
                    facility.Cells["facilitiesNotifiedOn"].Value = sendStatus?.Created.ToString("MM/dd/yyyy");
                    facility.Cells["facilitiesNotifiedStatus"].Value = (sendStatus != null ? sendStatus.ErrorText ?? "OK" : "");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show(ex.Message);
            }
        }

        bool selectAll = false;
        private void btnCheckAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in gridSuitableCareHomes.Rows)
            {
                row.Cells["gridFacilitySelect"].Value = selectAll;
            }

            selectAll = !selectAll;

            if (selectAll)
            {
                btnCheckAll.Text = "Select all";
            }
            else
            {
                btnCheckAll.Text = "Deselect all";
            }

            CalcSelectedFacilities();

        }

        private void PrepareSendStyle()
        {
            gridSuitableCareHomes.Rows.Clear();            
            CalcSelectedFacilities();
        }

        private void rbCareHomes_CheckedChanged(object sender, EventArgs e)
        {
            PrepareSendStyle();
        }
    }
}