﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistedLivingFacilityBlaster
{
    public partial class FormMain : Form
    {
        private void FillAgents(Guid lastSelectedAgent, int lastSelectedIndex)
        {
            gridAgents.Rows.Clear();

            foreach (var user in Db.agents)
            {
                gridAgents.Rows.Add(
                    user.Id.ToString(),
                    user.UserName,
                    user.RealName,
                    user.Enabled,
                    user.IsAdmin ?? 0,
                    (user.LastLogin ?? new DateTime(1990, 1, 1)).ToString("MM/dd/yyyy HH:mm:ss"),
                    user.Created.ToString("MM/dd/yyyy")
                    );
            }

            bool atLeastOneSelected = false;
            if (Guid.Empty != lastSelectedAgent)
            {

                foreach (DataGridViewRow row in gridAgents.Rows)
                {
                    row.Selected = Guid.Parse(row.Cells["AccountsColumnId"].Value.ToString()) == lastSelectedAgent;
                    if (row.Selected)
                        atLeastOneSelected = true;
                }
            }

            if (!atLeastOneSelected && lastSelectedIndex >= 0)
            {
                if (lastSelectedIndex >= gridAgents.Rows.Count)
                    lastSelectedIndex = gridAgents.Rows.Count - 1;

                for (int i = 0; i < gridAgents.Rows.Count; i++)
                {
                    gridAgents.Rows[i].Selected = i == lastSelectedIndex;
                }
            }
        }

        private void btnRefreshAgents_Click(object sender, EventArgs e)
        {
            Db.LoadAgents();
            FillAgents(Guid.Empty, -1);
        }

        private void btnDeleteAgents_Click(object sender, EventArgs e)
        {
            if (gridAgents.SelectedRows.Count == 0)
            {
                MessageBox.Show("Select an agent!");
                return;
            }

            DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete the selected agent?", "Delete agent", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
            {
                logger.Info("  Dont want to delete");
                return;
            }

            try
            {
                int selectedUserIndex = gridAgents.SelectedRows[0].Index;
                Db.ManageUser(
                    new Users() { Id = Guid.Parse(gridAgents.SelectedRows[0].Cells["AccountsColumnId"].Value.ToString()) },
                    Db.Mode.delete
                );

                MessageBox.Show("The user has been deleted successfuly!");

                Db.LoadAgents();
                FillAgents(Guid.Empty, selectedUserIndex);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnAddNewAgent_Click(object sender, EventArgs e)
        {
            FormAgent formAgent = new FormAgent();
            formAgent.mode = Db.Mode.create;
            if (formAgent.ShowDialog() == DialogResult.OK)
            {
                Db.LoadAgents();
                FillAgents(formAgent.userId, -1);
            }
        }

        private void btnEditAgent2_Click(object sender, EventArgs e)
        {
            if (gridAgents.SelectedRows.Count == 0)
            {
                MessageBox.Show("Select an agent!");
                return;
            }

            FormAgent formAgent = new FormAgent();
            formAgent.mode = Db.Mode.edit;
            formAgent.userId = Guid.Parse(gridAgents.SelectedRows[0].Cells["AccountsColumnId"].Value.ToString());

            if (formAgent.ShowDialog() == DialogResult.OK)
            {
                Db.LoadAgents();
                FillAgents(formAgent.userId, -1);
            }
        }
    }
}