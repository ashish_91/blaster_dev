﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NLog;
using System.Device.Location;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using AssistedLivingFacilityBlaster.Forms;

namespace AssistedLivingFacilityBlaster
{
    public partial class FormMain : Form
    {
        Logger logger = LogManager.GetLogger("main");

        private bool isSmsQueueWorking = true;
        private bool isSmsQueueSendingInTheMoment = false;
        private static System.Threading.Timer timerSendSms;
        private static volatile int processedTextMessagesCount = 0;

        private bool isEmailsQueueWorking = true;
        private bool isEmailsQueueSendingInTheMoment = false;
        private static System.Threading.Timer timerSendEmail;
        private static volatile int processedEmailsCount = 0;

        //=======================================================================================
        ///                               SYSTEM
        //=======================================================================================

        public FormMain()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            logger.Info("== Button form close pressed ==");
            Close();
        }

        private void formMain_Load(object sender, EventArgs e)
        {
            cbReportTypes.SelectedIndex = 0;
            logger.Info("== Form loaded ==");
            if (Db.user.IsAdmin != 1)
                tabMainControl.TabPages.RemoveByKey("tabAdmin");

            cbRadius.SelectedIndex = 1;

            this.Text = $"Hello, {Db.user.RealName}, this is the Assisted Living Facility Blaster, v{Application.ProductVersion}";

            gridFacilities.DoubleBuffered(true);
            gridReports.DoubleBuffered(true);
            gridSmsQueue.DoubleBuffered(true);
            gridPatients.DoubleBuffered(true);
            gridSuitableCareHomes.DoubleBuffered(true);
            gridAssistedLiving.DoubleBuffered(true);
            gridSuitableAL.DoubleBuffered(true);
            gridEmailsQueue.DoubleBuffered(true);

            timerSendSms = new System.Threading.Timer(_ => timerSendSMS_Tick(), null, System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
            timerSendEmail = new System.Threading.Timer(_ => timerSendEmails_Tick(), null, System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
        }

        private void timerSendEmails_Tick()
        {
            this.Invoke(new Action(() =>
            {
                lblQueueEmailsPending.Text = gridEmailsQueue.Rows.Count.ToString();
            }));
            
            if (gridEmailsQueue.Rows.Count < 1) return;

            isEmailsQueueSendingInTheMoment = true;

            var row = gridEmailsQueue.Rows[0];

            if (row.Cells["ColEmailsQueueId"].Value == null && isEmailsQueueWorking)
            {
                timerSendEmail.Change(1100, System.Threading.Timeout.Infinite);
                return;
            }

            Guid queueId = new Guid(row.Cells["ColEmailsQueueId"].Value.ToString());
            QueueEmails email = DbCache.queueEmails.Single(s => s.Id == queueId);

            //maybe this facility have been already emailed?
            var alreadySentEmails = DbCache.lastSendEmails
                .Where(w =>
                    w.PatientId == email.PatientId &&
                    w.Recipient == email.Recipient)
                .ToList();

            QueueEmails emailAlreadySend = null;
            foreach (var inv in alreadySentEmails)
            {                
                if (inv.Body == email.Body)
                {
                    emailAlreadySend = inv;
                    break;
                }
            }

            var watch = new Stopwatch();
            try
            {
                try
                {                    
                    if (emailAlreadySend == null)
                    {
                        string res = Helper.SendGmail(
                            fromAddress: email.FromAddress,
                            toAddress: email.Recipient,
                            body: email.Body,
                            subject: email.Subject,
                            isBodyHtml: email.IsBodyHtml,
                            password: Crypto.AES.Decrypt(email.CryptedPassword, "Th1M2gAStrongP2asss23Pword")
                            );

                        //TODO: store to the patietns register on the hard drive
                        //Helper.StoreToPatientsRegister(templateData, email.Trim(), txtBody.Text, false, false);

                        email.StatusId = 1;
                        email.Status = "Ok";
                    } else
                    {
                        email.StatusId = -2;
                        email.Status = "Ok";
                    }
                    
                    Db.UpdateEmailsQueue(email);

                    lblQueueEmailsSuccessfully.Invoke(new Action(() =>
                    {
                        lblQueueEmailsSuccessfully.Text = (int.Parse(lblQueueEmailsSuccessfully.Text) + 1).ToString();
                    }));

                    gridEmailsQueue.Invoke(new Action(() => gridEmailsQueue.Rows.RemoveAt(0)));
                }
                catch (Exception ex)
                {
                    string errMsg = ex.Message;
                    if (errMsg.Length > 50) errMsg = errMsg.Substring(0, 50);
                    email.Status = errMsg;
                    email.StatusId = -1;
                    Db.UpdateEmailsQueue(email);

                    lblQueueEmailsErrors.Invoke(new Action(() =>
                    {
                        lblQueueEmailsErrors.Text = (int.Parse(lblQueueEmailsErrors.Text) + 1).ToString();
                    }));

                    gridEmailsQueue.Invoke(new Action(() => gridEmailsQueue.Rows.RemoveAt(0)));
                }
            }
            finally
            {
                processedEmailsCount++;                
                isEmailsQueueSendingInTheMoment = false;

                if (isEmailsQueueWorking)
                {
                    timerSendEmail.Change(Math.Max(0, 1100 - watch.ElapsedMilliseconds), System.Threading.Timeout.Infinite);
                }
            }

        }

        public void RefreshEmailsQueue()
        {
            timerSendEmail.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
            try
            {
                this.Enabled = false;

                while (isEmailsQueueSendingInTheMoment)
                {
                    Application.DoEvents();
                    System.Threading.Thread.Sleep(10);
                }

                try
                {
                    DbCache.queueEmails = Db.LoadEmailsQueueFromDb();

                    gridEmailsQueue.Rows.Clear();
                    foreach (var email in DbCache.queueEmails)
                    {
                        gridEmailsQueue.Rows.Add(
                            email.Id,
                            email.Status,
                            email.Recipient,
                            email.FromAddress,
                            DbCache.allAssistedLiving.SingleOrDefault(s => s.Id == email.FacilityId)?.Name,
                            DbCache.allLoadedPatiensList.Where(w => w.Id == email.PatientId)?.Select(s => new { patientName = s.FirstName + " " + s.LastName })?.SingleOrDefault()?.patientName,
                            email.Subject
                            );
                    }

                }
                finally
                {
                    this.Enabled = true;
                }
            }
            finally
            {
                if (isEmailsQueueWorking)
                {
                    timerSendEmail.Change(1100, System.Threading.Timeout.Infinite);
                }
            }

            lblQueueEmailsPending.Text = gridEmailsQueue.Rows.Count.ToString();
        }

        private void SetProgressVisible(bool visible)
        {
            this.Enabled = !visible;

            lblProgress.Visible = visible;
            progressBar.Visible = visible;
        }

        private void SetProgressHint(string hint)
        {
            logger.Info(hint);
            lblProgress.Text = hint;
            Application.DoEvents();
        }

        private void btnExit_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
            Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timerLoadForm.Enabled = false;
            logger.Info("== Load db cache ==");

            SetProgressVisible(true);
            try
            {
                SetProgressHint("Loading db cache, please wait...");
                LoadCareHomes();
                LoadAssistedLiving();

                DbCache.Init();
                SetProgressHint("Load zips...");

                //cbAddressZip.Items.AddRange(DbCache.zips);                

                if (Db.user.IsAdmin == 1)
                {
                    SetProgressHint("Load agents (ADMIN)...");
                    Db.LoadAgents();
                    FillAgents(Guid.Empty, -1);
                }

                SetProgressHint("Load patients...");
                LoadPatiens();

                SetProgressHint("Load Question campaigns...");
                LoadQuestionCampaigns();

                SetProgressHint("Fill report filters...");
                FillCombos(0);

                SetProgressHint("Load Sms queue...");
                DbCache.formMain.RefreshSmsQueue();

                SetProgressHint("Load Emails queue...");
                DbCache.formMain.RefreshEmailsQueue();

                SetProgressHint("Load the last sent invitations, for dupl detection...");
                DbCache.lastInvitations = Db.LoadLastInvitations();

                SetProgressHint("Load the last sent emails, for dupl detection...");
                DbCache.lastSendEmails = Db.LoadLastSendEmails();

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Loading db cache");
                MessageBox.Show(ex.Message);
            }
            finally
            {
                SetProgressVisible(false);
                PrepareSendStyle();
            }
        }

        private void chkIndependentLiving_CheckedChanged(object sender, EventArgs e)
        {
            PrepareSendStyle();
        }

        private void chkMemoryCare_CheckedChanged(object sender, EventArgs e)
        {
            PrepareSendStyle();
        }

        private void FormMain_Shown(object sender, EventArgs e)
        {
            timerLoadForm.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Source.Automation.ImportCareHomes2();
            Source.Automation.ImportAssistedLiving();

        }

        // 0 - all
        // 1 - patients only
        // 2 - facilities only
        // 3 - care homes only TODO
        private void FillCombos(int whatToRefresh)
        {
            if (whatToRefresh < 0 || whatToRefresh > 2) throw new Exception("Incorrect whatToRefresh param");

            using (var db = new DbEntities())
            {
                if (whatToRefresh == 0 || whatToRefresh == 1)
                {
                    cbReportsInvitationsPatients.Items.Clear();
                    cbReportsInvitationsPatients.Items.Add("");
                    cbReportsInvitationsPatients.Items.AddRange(db.Patients.OrderByDescending(o => o.Created).ThenBy(o => o.FirstName).Select(s => s.FirstName + " " + s.LastName).ToArray());

                    cbReportsSentEmailsPatients.Items.Clear();
                    cbReportsSentEmailsPatients.Items.Add("");
                    cbReportsSentEmailsPatients.Items.AddRange(db.Patients.OrderByDescending(o => o.Created).ThenBy(o => o.FirstName).Select(s => s.FirstName + " " + s.LastName).ToArray());

                    cbCareHomesPatientSelect.DataSource = db.Patients.OrderByDescending(o => o.Created).Select(s => new { Text = s.FirstName + " " + s.LastName, Value = s.Id }).ToArray();
                    cbFindALPatient.DataSource = db.Patients.OrderByDescending(o => o.Created).Select(s => new { Text = s.FirstName + " " + s.LastName, Value = s.Id }).ToArray();
                }

                if (whatToRefresh == 0 || whatToRefresh == 2)
                {
                    cbReportsInvitationsCareHomes.Items.Clear();
                    cbReportsInvitationsCareHomes.Items.Add("");
                    cbReportsInvitationsCareHomes.Items.AddRange(db.CareHomes.OrderBy(o => o.FacilityName).Select(s => s.FacilityName).ToArray());

                    //just in case :)
                    cbReportsSentEmailsAL.Items.Clear();
                    cbReportsSentEmailsAL.Items.Add("");
                    cbReportsSentEmailsAL.Items.AddRange(db.AssistedLiving.OrderBy(o => o.Name).Select(s => s.Name).ToArray());
                }

                if (whatToRefresh == 0 || whatToRefresh == 3)
                {                    
                    cbReportsSentEmailsAL.Items.Clear();
                    cbReportsSentEmailsAL.Items.Add("");
                    cbReportsSentEmailsAL.Items.AddRange(db.AssistedLiving.OrderBy(o => o.Name).Select(s => s.Name).ToArray());
                }

            }
        }

        private void btnRefreshStatus_Click(object sender, EventArgs e)
        {
            if (cbCareHomesPatientSelect.SelectedIndex < 0)
            {
                MessageBox.Show("Please select a patient");
                return;
            }

            Guid patientId = Guid.Parse(cbCareHomesPatientSelect.SelectedValue.ToString());
            var selectedPatient = DbCache.allLoadedPatiensList.Single(s => s.Id == patientId);

            List<ActionsReport> sendStatuses = null;
            using (var db = new DbEntities())
            {
                sendStatuses = db.ActionsReport.Where(w => w.PatientId == selectedPatient.Id && w.ActionId == Db.ReportSendSms).ToList();
            }

            foreach (DataGridViewRow facility in gridSuitableCareHomes.Rows)
            {
                Guid facilityId = Guid.Parse(facility.Cells["facilitiesId"].Value.ToString());
                ActionsReport sendStatus = sendStatuses
                        .Where(w => w.FacilityId == facilityId)
                        .OrderByDescending(o => o.Created)
                        .FirstOrDefault();

                facility.Cells["facilitiesNotified"].Value = sendStatus != null;
                facility.Cells["facilitiesNotifiedOn"].Value = sendStatus?.Created.ToString("MM/dd/yyyy");
                facility.Cells["facilitiesNotifiedStatus"].Value = (sendStatus != null ? sendStatus.ErrorText ?? "OK" : "");
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            ExeportToFile(".csv", ",", true);
        }

        private void btnToggle_Click(object sender, EventArgs e)
        {
            ToggleSmsQueue();
        }

        private void numAutoStopCount_ValueChanged(object sender, EventArgs e)
        {
            RecalcWhenToStop();
        }

        private void RecalcWhenToStop()
        {
            Invoke(new Action(() =>
            {
                if (chkAutoStop.Checked)
                {
                    lblWillStopAfter.Text = ((int)numAutoStopCount.Value - processedTextMessagesCount).ToString();
                }
                else
                {
                    lblWillStopAfter.Text = "infinite";
                }
            }));
        }

        private void chkAutoStop_CheckedChanged(object sender, EventArgs e)
        {
            RecalcWhenToStop();
        }

        private void gridSuitableFacilities_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (gridSuitableCareHomes != null && gridSuitableCareHomes.Columns != null && gridSuitableCareHomes.Columns.Count > 0 && e.ColumnIndex == (gridSuitableCareHomes.Columns["gridFacilitySelect"]?.Index ?? -10) && e.RowIndex != -1)
            {
                gridSuitableCareHomes.CommitEdit(DataGridViewDataErrorContexts.Commit);
                Application.DoEvents();
                gridSuitableCareHomes.EndEdit();
                Application.DoEvents();
                timerUpdateSelectedCount.Enabled = true;
            }
        }

        private void timerUpdateSelectedCount_Tick(object sender, EventArgs e)
        {
            timerUpdateSelectedCount.Enabled = false;
            CalcSelectedFacilities();
            QC_CalcSelectedFacilities();
        }

        private void gridSuitableFacilities_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (gridSuitableCareHomes != null && gridSuitableCareHomes.Columns != null && gridSuitableCareHomes.Columns.Count > 0 && e.ColumnIndex == (gridSuitableCareHomes.Columns["gridFacilitySelect"]?.Index ?? -10) && e.RowIndex != -1)
            {
                gridSuitableCareHomes.CommitEdit(DataGridViewDataErrorContexts.Commit);
                Application.DoEvents();
                gridSuitableCareHomes.EndEdit();
                Application.DoEvents();
                timerUpdateSelectedCount.Enabled = true;
            }
        }

        private void LoadAssistedLiving()
        {
            SetProgressHint("Load assisted living...");
            try
            {
                using (var db = new DbEntities())
                {
                    DbCache.allAssistedLiving = db.AssistedLiving.ToList();

                    gridAssistedLiving.Rows.Clear();
                    foreach (var al in DbCache.allAssistedLiving)
                    {
                        gridAssistedLiving.Rows.Add(
                            al.Id,
                            al.Name,
                            al.WebSite,
                            al.Phone,
                            al.Zip,
                            al.IndependentLivingPrice?.ToString() ?? "No",
                            al.AssistedLivingPrice?.ToString() ?? "No",
                            al.MemoryCarePrice?.ToString() ?? "No",
                            al.SkilledNursingCarePrice?.ToString() ?? "No",

                            al.Email1,
                            al.Email2,
                            al.Email3,
                            al.Email4,
                            al.Email5,
                            al.Notes
                        );
                    }

                    logger.Info("  All added to the grid");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while loading all facilities: " + ex.Message);
                logger.Error(ex);
            }
        }

        private void btnALDelete_Click(object sender, EventArgs e)
        {
            logger.Info("Delete clicked");
            try
            {
                if (gridAssistedLiving.SelectedRows.Count <= 0)
                {
                    logger.Info("  Nothing selected");
                    MessageBox.Show("Select a assisted living to delete!");
                    return;
                }

                DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete the selected Assisted living?", "Delete Assisted living", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.No)
                {
                    logger.Info("  Dont want to delete");
                    return;
                }

                SetProgressVisible(true);
                try
                {
                    using (var db = new DbEntities())
                    {
                        SetProgressHint("Get the id");
                        Guid alId = Guid.Parse(gridAssistedLiving.SelectedRows[0].Cells["ColAlId"].Value.ToString());
                        int selectedRow = gridAssistedLiving.SelectedRows[0].Index;

                        SetProgressHint("Deleting the AL");
                        db.AssistedLiving.Remove(db.AssistedLiving.Where(w => w.Id == alId).Single());

                        db.SaveChanges();

                        LoadAssistedLiving();

                        if (gridAssistedLiving.Rows.Count <= selectedRow)
                            selectedRow = gridAssistedLiving.Rows.Count - 1;

                        SetProgressHint("Select the right row...");
                        if (selectedRow >= 0)
                        {
                            foreach (DataGridViewRow row in gridAssistedLiving.Rows)
                            {
                                row.Selected = row.Index == selectedRow;
                                if (row.Selected) break;
                            }

                            if (gridAssistedLiving.SelectedRows.Count > 0)
                                gridAssistedLiving.FirstDisplayedScrollingRowIndex = gridAssistedLiving.SelectedRows[0].Index;
                        }

                        //TODO: refill the AL combos
                        //FillCombos(2);
                    }
                }
                finally
                {
                    SetProgressVisible(false);
                }

                logger.Info("  SUCCESS");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error while deleting care home: " + ex.Message);
            }
        }

        private void btnALEdit_Click(object sender, EventArgs e)
        {
            logger.Info("EditAL");
            try
            {
                if (gridAssistedLiving.SelectedRows.Count == 0)
                {
                    logger.Info("  Nothing selected");
                    MessageBox.Show("Please select an Assisted living to edit!");
                    return;
                }

                var formAl = new FormAssistedLiving();
                formAl.mode = FormAssistedLiving.Mode.edit;
                string selectedId = gridAssistedLiving.SelectedRows[0].Cells["ColAlId"].Value.ToString();
                formAl.assistedLivingId = Guid.Parse(selectedId);

                var registerNeedRefresh = formAl.ShowDialog();

                if (registerNeedRefresh == DialogResult.Yes)
                {
                    LoadAssistedLiving();
                    foreach (DataGridViewRow row in gridAssistedLiving.Rows)
                    {
                        row.Selected = row.Cells[0].Value.ToString() == selectedId;
                        if (row.Selected) break;
                    }

                    if (gridAssistedLiving.SelectedRows.Count > 0)
                        gridAssistedLiving.FirstDisplayedScrollingRowIndex = gridAssistedLiving.SelectedRows[0].Index;
                }

                //TODO: refil AL combos
                //FillCombos(3);
                logger.Info("  SUCCESS");
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Editfacility");
                MessageBox.Show("Error occured while trying to edit the AL: " + ex.Message);
            }
        }

        private void btnALAdd_Click(object sender, EventArgs e)
        {
            logger.Info("Add new AL");
            try
            {
                var formAl = new FormAssistedLiving();
                formAl.mode = FormAssistedLiving.Mode.create;
                var registerNeedRefresh = formAl.ShowDialog();

                if (registerNeedRefresh == DialogResult.Yes)
                {
                    LoadAssistedLiving();
                    foreach (DataGridViewRow row in gridAssistedLiving.Rows)
                    {
                        row.Selected = row.Cells[0].Value.ToString() == formAl.assistedLivingId.ToString();
                        if (row.Selected) break;
                    }

                    if (gridAssistedLiving.SelectedRows.Count > 0)
                        gridAssistedLiving.FirstDisplayedScrollingRowIndex = gridAssistedLiving.SelectedRows[0].Index;

                    //TODO: refill AL combos
                    //FillCombos(3);
                }

                logger.Info("  SUCCESS");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error in while adding new facility: " + ex.Message);
            }
        }

        private void btnALnewPatient_Click(object sender, EventArgs e)
        {
            AddPatient(1);
        }

        private void btnFindAlZipCodesHelp_Click(object sender, EventArgs e)
        {
            MessageBox.Show("The first ZIP code is from the patient info\n.Enter more ZIP codes on new rows to search near them by distance.");
        }

        private void btnALSearch_Click(object sender, EventArgs e)
        {
            (bool isValid, List<string> zips) = GetFindAlZips();
            if (!isValid) return;

            if (!checkIndLiv.Checked && !checkAssLiv.Checked && !checkMemCare.Checked && !checkSkilledNursing.Checked)
            {
                logger.Error("care level not selected");
                MessageBox.Show("Select at least one care level!");
                return;
            }

            if (cbFindALPatient.SelectedIndex < 0)
            {
                MessageBox.Show("Select a patient!");
                return;
            }

            Guid patientId = Guid.Parse(cbFindALPatient.SelectedValue.ToString());
            var selectedPatient = DbCache.allLoadedPatiensList.Single(s => s.Id == patientId);

            gridSuitableAL.Rows.Clear();

            ////////////////////////////////////////
            SetProgressVisible(true);
            try
            {
                SetProgressHint("Filtering by facility type...");
                var suitableALs = new List<AssistedLiving>();

                if (checkIndLiv.Checked) suitableALs.AddRange(DbCache.allAssistedLiving.Where(w => w.IndependentLivingPrice != null).ToList());
                if (checkAssLiv.Checked) suitableALs.AddRange(DbCache.allAssistedLiving.Where(w => w.AssistedLivingPrice != null).ToList());
                if (checkMemCare.Checked) suitableALs.AddRange(DbCache.allAssistedLiving.Where(w => w.MemoryCarePrice != null).ToList());
                if (checkSkilledNursing.Checked) suitableALs.AddRange(DbCache.allAssistedLiving.Where(w => w.SkilledNursingCarePrice != null).ToList());

                //unique by id, remove duplicates
                suitableALs = suitableALs.GroupBy(g => g.Id).Select(s => s.First()).ToList();

                SetProgressHint("filtering by ALTSC...");
                int patientAltcs = rbFIndAlALTCSYes.Checked ? 1 : 0;

                if (patientAltcs == 1)
                {
                    suitableALs = suitableALs.Where(w => w.ALTSC).ToList();
                }

                SetProgressHint("filtering by distance...");

                //Get the geo location of the facilities so far
                var facilitiesZipBundle =
                    (from f in suitableALs
                     join z in DbCache.fullCache on f.Zip equals z.zip
                     select new { z, f }).ToList();

                //fill suitableFacilities with lat and lng
                foreach (var b in facilitiesZipBundle)
                {
                    var sf = suitableALs.Where(w => w.Id == b.f.Id).Single();
                    sf.Lng = (decimal)b.z.loc.Longitude;
                    sf.Alt = (decimal)b.z.loc.Latitude;
                }

                var filteredByDistance = new List<AssistedLiving>();
                foreach (string zip in zips)
                {
                    DbCache.ZipEntity selectedZip = DbCache.fullCache.AsParallel().Where(w => w.zip == zip).SingleOrDefault();
                    filteredByDistance.AddRange(Helper.GetAllZipsInRangeForAL(selectedZip.loc, txtALMilesRange.Value, suitableALs));
                }

                //unique by id, remove duplicates
                filteredByDistance = filteredByDistance.GroupBy(g => g.Id).Select(s => s.First()).ToList();

                List<ActionsReport> sendStatuses = null;

                using (var db = new DbEntities())
                {
                    sendStatuses = db.ActionsReport.Where(w => w.PatientId == selectedPatient.Id && w.ActionId == Db.ReportSendEmailToFacility).ToList();
                }

                SetProgressHint("Load all matching al to the grid");

                foreach (var al in filteredByDistance)
                {
                    ActionsReport sendStatus = sendStatuses.Where(w => w.FacilityId == al.Id).OrderByDescending(o => o.Created).FirstOrDefault();

                    gridSuitableAL.Rows.Add
                    (
                        al.Id.ToString(),
                        true,
                        al.Name,
                        al.ALTSC,
                        al.Distance.ToString("0.0"),
                        al.Zip,
                        al.IndependentLivingPrice == null ? "No" : (al.IndependentLivingPrice ?? 0).ToString("0.0"),
                        al.AssistedLivingPrice == null ? "No" : (al.AssistedLivingPrice ?? 0).ToString("0.0"),
                        al.MemoryCarePrice == null ? "No" : (al.MemoryCarePrice ?? 0).ToString("0.0"),
                        al.SkilledNursingCarePrice == null ? "No" : (al.SkilledNursingCarePrice ?? 0).ToString("0.0"),
                        sendStatus != null,
                        sendStatus?.Created.ToString("MM/dd/yyyy"),
                        (sendStatus != null ? sendStatus.ErrorText ?? "OK" : "")
                    );
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Search");
                MessageBox.Show("Error occured while searching: " + ex.Message);
            }
            finally
            {
                SetProgressVisible(false);
            }

        }

        private (bool isValid, List<string> zips) GetFindAlZips()
        {
            List<string> zips = new List<string>();
            foreach (var zip in txtFindALZips.Lines)
            {
                string tempZip = zip.Trim();
                if (zip.Length != 5 || !int.TryParse("123", out var n))
                {
                    MessageBox.Show("Invalid zip code: " + tempZip);
                    return (false, null);
                }

                zips.Add(tempZip);
            }

            if (zips.Count == 0)
            {
                MessageBox.Show("No zips to search by!");
                return (false, null);
            }

            return (true, zips);
        }

        private void cbFindALPatient_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbFindALPatient.SelectedIndex < 0)
                return;

            Guid patientId = Guid.Parse(cbFindALPatient.SelectedValue.ToString());
            var selectedPatient = DbCache.allLoadedPatiensList.Single(s => s.Id == patientId);

            txtFindALZips.Text = selectedPatient.AddrZip;

            checkIndLiv.Checked = selectedPatient.FacIndipendentLiving == 1;
            checkAssLiv.Checked = selectedPatient.FacAssistedLiving == 1;
            checkMemCare.Checked = selectedPatient.FacMemoryCare == 1;
            checkSkilledNursing.Checked = selectedPatient.FacSkilledNursingCare == 1;

            rbFIndAlALTCSYes.Checked = selectedPatient.ALTCS == 1;
            rbFIndAlALTCSNo.Checked = selectedPatient.ALTCS == 0;

            gridSuitableAL.Rows.Clear();
        }

        bool selectAllAl = false;
        private void btnFindALDeselectAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in gridSuitableAL.Rows)
            {
                row.Cells["ColFindSelect"].Value = selectAllAl;
            }

            selectAllAl = !selectAllAl;

            if (selectAllAl)
            {
                btnFindALDeselectAll.Text = "Select all";
            }
            else
            {
                btnFindALDeselectAll.Text = "Deselect all";
            }
        }

        private void btnFindAlSelectNotEmailed_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in gridSuitableAL.Rows)
            {
                row.Cells["ColFindSelect"].Value = !bool.Parse(row.Cells["ColFindALEmailed"].Value.ToString());
            }
        }

        private void btnFindALSelectEmailedOnly_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in gridSuitableAL.Rows)
            {
                row.Cells["ColFindSelect"].Value = bool.Parse(row.Cells["ColFindALEmailed"].Value.ToString()) && row.Cells["ColAlEmailStatus"].Value.ToString() == "OK";
            }
        }

        private List<Guid> GetSelectedAssistedLiving()
        {
            var facilitiesToNotify = new List<Guid>();

            foreach (DataGridViewRow row in gridSuitableAL.Rows)
            {
                if (bool.Parse(row.Cells["ColFindSelect"]?.Value?.ToString()))
                {
                    facilitiesToNotify.Add(Guid.Parse(row.Cells["ColFindALId"].Value.ToString()));
                }
            }

            if (facilitiesToNotify.Count <= 0)
            {
                return null;
            }

            return facilitiesToNotify;
        }

        private void btnALSendEmails_Click(object sender, EventArgs e)
        {
            if (cbFindALPatient.SelectedIndex < 0)
            {
                MessageBox.Show("Select a patient!");
                return;
            }

            Guid patientId = Guid.Parse(cbFindALPatient.SelectedValue.ToString());
            var selectedPatient = DbCache.allLoadedPatiensList.Single(s => s.Id == patientId);

            FormSendMailsToFacilities sndMails = new FormSendMailsToFacilities();
            sndMails.facilitiesMailDetails = Db.FillAssistedLivingStructuredData(GetSelectedAssistedLiving());
            if (sndMails.facilitiesMailDetails == null)
            {
                MessageBox.Show("Please select at least one facility!");
                return;
            }

            sndMails.templateData = Helper.FillTemplateData(selectedPatient);
            sndMails.ShowDialog();

            //refresh statuses
            List<ActionsReport> sendStatuses = null;
            using (var db = new DbEntities())
            {
                sendStatuses = db.ActionsReport.Where(w => w.PatientId == selectedPatient.Id && w.ActionId == Db.ReportSendEmailToFacility).ToList();
            }

            foreach (DataGridViewRow facility in gridSuitableAL.Rows)
            {
                Guid facilityId = Guid.Parse(facility.Cells["ColFindALId"].Value.ToString());
                ActionsReport sendStatus = sendStatuses
                        .Where(w => w.FacilityId == facilityId && w.ActionId == Db.ReportSendEmailToFacility)
                        .OrderByDescending(o => o.Created)
                        .FirstOrDefault();

                facility.Cells["ColFindALEmailed"].Value = sendStatus != null;
                facility.Cells["ColFindALEmailedOn"].Value = sendStatus?.Created.ToString("MM/dd/yyyy");
                facility.Cells["ColAlEmailStatus"].Value = (sendStatus != null ? sendStatus.ErrorText ?? "OK" : "");
            }
        }

        private void btnToggleEmailQueue_Click(object sender, EventArgs e)
        {
            Invoke(new Action(() =>
            {
                processedEmailsCount = 0;

                if (isEmailsQueueWorking)
                {
                    btnToggleEmailQueue.Text = "Start";
                    timerSendEmail.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
                }
                else
                {
                    btnToggleEmailQueue.Text = "Stop";
                    timerSendEmail.Change(1100, System.Threading.Timeout.Infinite);
                }

                isEmailsQueueWorking = !isEmailsQueueWorking;                
            }));
        }

        private void btnRemoveEmailsFromQueue_Click(object sender, EventArgs e)
        {
            if (gridEmailsQueue.SelectedRows.Count == 0)
            {
                MessageBox.Show("Select the entries you would like to remove from the Email queue!");
                return;
            }

            bool hasWorked = isEmailsQueueWorking;
            if (isEmailsQueueWorking)
            {
                timerSendEmail.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
                isEmailsQueueWorking = false;
            }

            while (isEmailsQueueSendingInTheMoment)
            {
                Application.DoEvents();
                System.Threading.Thread.Sleep(10);
            }

            var queueIds = new List<Guid>();
            foreach (DataGridViewRow row in gridEmailsQueue.SelectedRows)
            {
                queueIds.Add(Guid.Parse(row.Cells["ColEmailsQueueId"].Value.ToString()));
            }

            Db.RemoveFromEmailQueue(queueIds);

            isEmailsQueueWorking = hasWorked;
            RefreshEmailsQueue();
            MessageBox.Show("Entries successfully removed from the Email queue!");
        }

        // ================================================================================================
        //       === Question Campaign  4h 35mins
        // 1h 20mins interface for both forms
        // 1h 13mins care home filtering plus code behind plus all other buttons
        // 0h 47mins makeing the basic code behind the questions campaign
        // 0h 20mins create tables for questions and campaigns, update edmx model
        // 0h 55mins create campaigns with questions, edit questions - store to database
        // PAYED on 06/08/2018
        /*
         06/13/2018
           14:17 - 15:31  74mins
             - make context menu
             - make delete button work
             - start working on move up/down
             - cache/fill combo witl question campaigns

          06/14/2018 
            14:56 - 15:14 18mins
              - finish move up/down

          06/16/2018 
            10:17 - 11:21 64mins
              - make loading the questions and start working on editing in questions for
            
            13:11 - 13:54 43mins
              - finish editing the questions and question campaign
              
          06/20/2018
            09:17 - 10:41 84mins
              - refactor the sms queue and sms send form to be more universal, required because of now TWO types of sms
              - refactor the actions table in order to be able to distinguish the different sms types (and all related code)
            
            13:41 - 15:19 98mins
              - link searching for ch with send questions - when and is there responce
              - make sending the second type of sms and integrate it to the existing sms queue
              - integrate the second type of sms with the table for the reports

            16:15 - 16:49 34mins
              - start working on the new web page - facility-q.html
              - start working on the API for the stuff behind it
          
          06/21/2018
            13:54 - 16:29 135mins
              - make the dynamic form for showing the questioins, including the two types of quetions
              - testing which viewing variant looks best and is mobile friendly
              - write the code behind to gether the answers and pass them to the API
              - make the API method for handling the answers
              - make a table to store the answers
              - mark all other facilities with the same phone as answered the questions (the same as CH)
              - write the code to store the answers to the new table


            /// PAYED on 2018-06-22
         */

        // todo: 
        // == Questions campaign form
        //  - loading the questions in the questions campaign form
        //  - make del, more up, move down buttons work
        // == making url, shorten it, integrate with the sms queue
        // == the web part
        //  - making the required tables
        //  - saving the anwers to those tables with validations
        // == the report
        // == extensive test all this
        // ================================================================================================
        private void btnQC_AddQuestionCampaign_Click(object sender, EventArgs e)
        {
            FormQuestionCampaign formQC = new FormQuestionCampaign()
            {
                mode = FormQuestionCampaign.Mode.create
            };

            if (formQC.ShowDialog() == DialogResult.OK)
            {
                LoadQuestionCampaigns();
            }
        }

        private void btnQC_FilterCareHomes_Click(object sender, EventArgs e)
        {
            if (cbQC_QuestionsCampaignSelect.SelectedIndex < 0)
            {
                MessageBox.Show("Select a question campaign for step 1!");
                return;
            }

            if (txtQC_zipCode.Text.Trim().Length < 4)
            {
                MessageBox.Show("Please enter a valid zip code!");
                return;
            }

            Guid questionCampaignid = allQuestioncampaigns.Single(s => s.QuestionCampaignName == cbQC_QuestionsCampaignSelect.Text).Id;

            gridQC_FoundedCareHomes.Rows.Clear();

            QC_PerformSearchOfMatchingCH(questionCampaignid);

            QC_CalcSelectedFacilities();
        }

        private void QC_PerformSearchOfMatchingCH(Guid questionCampaignId)
        {
            logger.Info("=== QC Performing Matching CH FOR QC ===");

            DbCache.ZipEntity selectedZip = DbCache.fullCache.AsParallel().Where(w => w.zip == txtQC_zipCode.Text.Trim()).SingleOrDefault();

            if (selectedZip == null)
            {
                logger.Error("QC zip not found");
                MessageBox.Show("Zip not found in database!");
                return;
            }

            SetProgressVisible(true);
            try
            {
                SetProgressHint("QC Filtering by facility type...");
                List<CareHomes> suitableCareHomes = new List<CareHomes>();

                suitableCareHomes.AddRange(DbCache.allCareHomes.ToList());

                //Only for care homes
                suitableCareHomes = suitableCareHomes
                    .Where(w => (w.PrivateRoomPrice ?? 0) >= edQC_PirvateRoomFrom.Value)
                    .Where(w => (w.PrivateRoomPrice ?? 0) <= edQC_PirvateRoomTo.Value)
                    .Where(w => (w.SemiPrivateRoomPrice ?? 0) >= edQC_SemiPirvateRoomFrom.Value)
                    .Where(w => (w.SemiPrivateRoomPrice ?? 0) <= edQC_SemiPirvateRoomTo.Value)
                .ToList();

                //filter on ALTCS
                SetProgressHint("QC filtering by ALTSC...");
                int patientAltcs = radioQC_ALTCS_Yes.Checked ? 1 : 0;

                if (patientAltcs == 1)
                {
                    suitableCareHomes = suitableCareHomes.Where(w => w.ALTSC == 1).ToList();
                }

                SetProgressHint("filtering by distance...");

                //Get the geo location of the facilities so far
                var facilitiesZipBundle =
                    (from f in suitableCareHomes
                     join z in DbCache.fullCache on f.Zip equals z.zip
                     select new { z, f }).ToList();

                //fill suitableFacilities with lat and lng
                foreach (var b in facilitiesZipBundle)
                {
                    var sf = suitableCareHomes.Where(w => w.Id == b.f.Id).Single();
                    sf.Lng = (decimal)b.z.loc.Longitude;
                    sf.Alt = (decimal)b.z.loc.Latitude;
                }

                //Now remove all which does not fit the distance
                suitableCareHomes = Helper.GetAllZipsInRangeForCH(selectedZip.loc, txtQC_zipRadius.Value, suitableCareHomes);

                ////// HELP FOR SEND QUETIONARIES ETC
                List<ActionsReport> sendStatuses = null;
                List<InterestedPatients> interestedFacilities = null;

                using (var db = new DbEntities())
                {
                    sendStatuses = db.ActionsReport.Where(w => w.PatientId == questionCampaignId && w.ActionId == Db.ReportSendQuestionsAsSms).ToList();
                    interestedFacilities = db.InterestedPatients.Where(w => w.PatientId == questionCampaignId).ToList();
                }

                SetProgressHint("QC Load all matching facilities to the grid");

                foreach (var careHome in suitableCareHomes)
                {
                    ActionsReport sendStatus = sendStatuses.Where(w => w.FacilityId == careHome.Id).OrderByDescending(o => o.Created).FirstOrDefault();

                    InterestedPatients interestedFacility = null;
                    if (interestedFacilities != null)
                    {
                        interestedFacility = interestedFacilities.Where(w => w.FacilityId == careHome.Id).OrderByDescending(o => o.InterestDateTime).FirstOrDefault();
                    }

                    gridQC_FoundedCareHomes.Rows.Add
                    (
                        careHome.Id.ToString(),
                        true,
                        careHome.FacilityName,
                        careHome.ALTSC,
                        (careHome.Distance ?? -1).ToString("0.0"),
                        careHome.City,
                        careHome.PrivateRoomPrice ?? 0,
                        careHome.SemiPrivateRoomPrice ?? 0,
                        sendStatus != null,  //Questionded
                        interestedFacility != null && interestedFacility.HaveInterest == 1,  //Responce
                        sendStatus?.Created.ToString("MM/dd/yyyy") //Questioned on
                    );
                }

                QC_CalcSelectedFacilities();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Search");
                MessageBox.Show("Error occured while searching: " + ex.Message);
            }
            finally
            {
                SetProgressVisible(false);
            }
        }

        private void QC_CalcSelectedFacilities()
        {
            int selectedCount = 0;
            foreach (DataGridViewRow row in gridQC_FoundedCareHomes.Rows)
            {
                if (bool.Parse(row.Cells["gridColQC_selected"]?.Value?.ToString()))
                {
                    selectedCount++;
                }
            }

            lblQC_selectedCH.Text = selectedCount.ToString();
        }

        bool qc_selectAll = false;
        private void btnQC_DeselectAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in gridQC_FoundedCareHomes.Rows)
            {
                row.Cells["gridColQC_selected"].Value = selectAll;
            }

            qc_selectAll = !qc_selectAll;

            if (qc_selectAll)
            {
                btnQC_DeselectAll.Text = "Select all";
            }
            else
            {
                btnQC_DeselectAll.Text = "Deselect all";
            }

            QC_CalcSelectedFacilities();
        }

        private void btnQC_SelectOnlyNotQuestioned_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in gridQC_FoundedCareHomes.Rows)
            {
                row.Cells["gridColQC_selected"].Value = !bool.Parse(row.Cells["gridColQC_questioned"].Value.ToString());
            }

            QC_CalcSelectedFacilities();
        }

        private void btnQC_SelectOnlyQuestioned_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in gridQC_FoundedCareHomes.Rows)
            {
                row.Cells["gridColQC_selected"].Value = bool.Parse(row.Cells["gridColQC_questioned"].Value.ToString());
            }

            QC_CalcSelectedFacilities();
        }

        private void gridQC_FoundedCareHomes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (gridQC_FoundedCareHomes != null && gridQC_FoundedCareHomes.Columns != null && gridQC_FoundedCareHomes.Columns.Count > 0 && e.ColumnIndex == (gridQC_FoundedCareHomes.Columns["gridColQC_selected"]?.Index ?? -10) && e.RowIndex != -1)
            {
                gridQC_FoundedCareHomes.CommitEdit(DataGridViewDataErrorContexts.Commit);
                Application.DoEvents();
                gridQC_FoundedCareHomes.EndEdit();
                Application.DoEvents();
                timerUpdateSelectedCount.Enabled = true;
            }
        }

        private void gridQC_FoundedCareHomes_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (gridQC_FoundedCareHomes != null && gridQC_FoundedCareHomes.Columns != null && gridQC_FoundedCareHomes.Columns.Count > 0 && e.ColumnIndex == (gridQC_FoundedCareHomes.Columns["gridColQC_selected"]?.Index ?? -10) && e.RowIndex != -1)
            {
                gridQC_FoundedCareHomes.CommitEdit(DataGridViewDataErrorContexts.Commit);
                Application.DoEvents();
                gridQC_FoundedCareHomes.EndEdit();
                Application.DoEvents();
                timerUpdateSelectedCount.Enabled = true;
            }
        }

        private void btnQC_EditQuestionCampaign_Click(object sender, EventArgs e)
        {
            if (cbQC_QuestionsCampaignSelect.SelectedIndex < 0)
            {
                MessageBox.Show("Please select a question campaign!");
                return;
            }

            FormQuestionCampaign formQC = new FormQuestionCampaign()
            {
                mode = FormQuestionCampaign.Mode.edit,
                campaignQuestionId = allQuestioncampaigns.Single(s => s.QuestionCampaignName == cbQC_QuestionsCampaignSelect.Text).Id
            };

            if (formQC.ShowDialog() == DialogResult.OK)
            {
                LoadQuestionCampaigns();
            }
        }

        private List<Guid> QC_GetSelectedCareHomes(DataGridView grid)
        {
            List<Guid> facilitiesToNotify = new List<Guid>();

            foreach (DataGridViewRow row in grid.Rows)
            {
                if (bool.Parse(row.Cells["gridColQC_selected"]?.Value?.ToString()))
                {
                    facilitiesToNotify.Add(Guid.Parse(row.Cells["gridColQC_id"].Value.ToString()));
                }
            }

            if (facilitiesToNotify.Count <= 0)
            {
                return null;
            }

            return facilitiesToNotify;
        }

        private void btnQC_SendQuestions_Click(object sender, EventArgs e)
        {
            if (cbQC_QuestionsCampaignSelect.SelectedIndex < 0)
            {
                MessageBox.Show("Select a question campaign for step 1!");
                return;
            }

            Guid questionCampaignid = allQuestioncampaigns.Single(s => s.QuestionCampaignName == cbQC_QuestionsCampaignSelect.Text).Id;

            try
            {
                var formSendSms = new FormSendSMSs();
                formSendSms.facilitiesSMSsDetails = Db.FillFacilitiesStructuredData(QC_GetSelectedCareHomes(gridQC_FoundedCareHomes));
                if (formSendSms.facilitiesSMSsDetails == null)
                {
                    MessageBox.Show("Please select at least one facility!");
                    return;
                }

                formSendSms.questionCampaignId = questionCampaignid;
                formSendSms.smsType = FormSendSMSs.SMSType.askALQuestions;
                formSendSms.ShowDialog();

                //refresh statuses
                List<ActionsReport> sendStatuses = null;
                using (var db = new DbEntities())
                {
                    sendStatuses = db.ActionsReport.Where(w => w.PatientId == questionCampaignid && w.ActionId == Db.ReportSendQuestionsAsSms).ToList();
                }

                foreach (DataGridViewRow facility in gridQC_FoundedCareHomes.Rows)
                {
                    Guid facilityId = Guid.Parse(facility.Cells["gridColQC_id"].Value.ToString());
                    ActionsReport sendStatus = sendStatuses
                            .Where(w => w.FacilityId == facilityId)
                            .OrderByDescending(o => o.Created)
                            .FirstOrDefault();

                    facility.Cells["gridColQC_questioned"].Value = sendStatus != null;
                    facility.Cells["gridColQC_questionedOn"].Value = sendStatus?.Created.ToString("MM/dd/yyyy");                    
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show(ex.Message);
            }
        }
    }


    public static class ExtensionMethods
    {
        public static void DoubleBuffered(this DataGridView dgv, bool setting)
        {
            Type dgvType = dgv.GetType();
            PropertyInfo pi = dgvType.GetProperty("DoubleBuffered",
                BindingFlags.Instance | BindingFlags.NonPublic);
            pi.SetValue(dgv, setting, null);
        }
    }
}
