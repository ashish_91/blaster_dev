﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistedLivingFacilityBlaster
{
    public partial class FormMain : Form
    {
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            //edit facility/care home
            if (gridReports.SelectedCells.Count == 0)
            {
                MessageBox.Show("Select a care home to edit!");
                return;
            }

            if (gridReports.SelectedCells.Count > 1)
            {
                MessageBox.Show("Select only one care home to edit!");
                return;
            }

            var formFacility = new FormCareHome();
            formFacility.mode = FormCareHome.Mode.edit;
            formFacility.careHomeId = Guid.Parse(gridReports.SelectedCells[0].OwningRow.Cells[0].Value.ToString());
            var registerNeedRefresh = formFacility.ShowDialog();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //fix errors            
            if (gridReports.SelectedCells.Count == 0)
            {
                MessageBox.Show("Select a care home to mark as fixed!");
                return;
            }

            if (gridReports.SelectedCells.Count > 1)
            {
                MessageBox.Show("Select only one care home to mark as fixed!");
                return;
            }

            Guid facilityId = Guid.Parse(gridReports.SelectedCells[0].OwningRow.Cells[0].Value.ToString());

            using (var db = new DbEntities())
            {
                foreach (var patient in db.InterestedPatients.Where(w => w.FacilityId == facilityId && w.Error != null && w.Error.Length > 0 && w.Error != "Fixed"))
                {
                    patient.Error = "Fixed";
                }

                db.SaveChanges();
            }

            var rowsToRemove = new List<DataGridViewRow>();
            foreach (DataGridViewRow row in gridReports.Rows)
            {
                if (Guid.Parse(row.Cells[0].Value.ToString()) == facilityId)
                {
                    rowsToRemove.Add(row);
                }
            }

            foreach (var row in rowsToRemove)
            {
                gridReports.Rows.Remove(row);
            }
        }

        private void btnExportSelectedToExcel_Click(object sender, EventArgs e)
        {
            ExeportToFile(".txt", " ", false);
        }

        private void ExeportToFile(string extension, string delimiter, bool addQuotesIfDelimitedIsContained)
        {
            string tempFileName = Helper.GetTempFileName(extension);

            bool fileEmpty = true;
            foreach (DataGridViewRow row in gridReports.Rows)
            {
                string rowText = String.Empty;
                foreach (DataGridViewCell cell in row.Cells)
                {
                    if (cell.Selected)
                    {
                        string cellVal = cell.Value?.ToString() ?? "";

                        if (addQuotesIfDelimitedIsContained)
                        {
                            if (cellVal.Contains(delimiter))
                            {
                                //replace " with ""
                                cellVal = cellVal.Replace("\"", "\"\"");

                                //now put in qoutes
                                cellVal = "\"" + cellVal + "\"";
                            }
                        }

                        rowText += cellVal + delimiter;
                    }
                }

                if (!string.IsNullOrWhiteSpace(rowText))
                {
                    fileEmpty = false;
                    rowText = rowText.Remove(rowText.Length - delimiter.Length);
                    File.AppendAllText(tempFileName, rowText + Environment.NewLine);
                }
            }

            if (fileEmpty)
            {
                MessageBox.Show("You have selecteding nothing to export!");
            }
            else
            {
                System.Diagnostics.Process.Start(tempFileName);
            }
        }

        private void AddCol(string text, DataGridView grid, bool readOnly = true, bool visible = true)
        {
            gridReports.Columns.Add(new DataGridViewTextBoxColumn()
            {
                ReadOnly = readOnly,
                HeaderText = text,
                Name = Guid.NewGuid().ToString().Replace("-", ""),
                Visible = visible
            });
        }

        private void btnLoadReport_Click(object sender, EventArgs e)
        {
            using (var db = new DbEntities())
            {
                gridReports.Rows.Clear();
                gridReports.Columns.Clear();

                switch (cbReportTypes.SelectedIndex)
                {
                    case 0: //Sent invitations
                        ShowReportSentInvitations(db);
                        break;

                    case 1: //Sent emails reports
                        ShowReportSentEmails(db);
                        break;

                    case 2: //Sent emails reports for Parients report
                        ShowReportSentEmailsForPatientsReport(db);
                        break;

                    case 3: //Activity reports
                        ShowReportActivities(db);
                        break;

                    case 4: //Question campaign report
                        ShowReportQuestionCampaignAnswers(db);
                        break;


                }
            }
        }

        private void ShowReportQuestionCampaignAnswers(DbEntities db)
        {
            if (comboReportsQuestionCampaigns.SelectedItem is null)
            {
                MessageBox.Show("Select a question campaign!");
                return;
            }

            Guid questionCampaignId = allQuestioncampaigns.Single(s => s.QuestionCampaignName == comboReportsQuestionCampaigns.Text).Id;

            var questions = db.QuestionCampaignQuestions.Where(w => w.QuestionCampaignId == questionCampaignId).ToList();
            var questionIds = questions.Select(s => s.Id).ToList();

            var answers = db.QuestionCampaignAnswers.Where(w => questionIds.Contains(w.QuestionId)).ToList();            

            var answersCareHomesIds = answers.Select(s => s.FacilityId).ToList();
            var relatedCareHomes = db.CareHomes.Where(w => answersCareHomesIds.Contains(w.Id)).ToList();

            AddCol("CareHomeId", gridReports, visible: false);
            AddCol("Care home name", gridReports);

            List<Guid> questionsIds = new List<Guid>();
            foreach (var question in questions)
            {
                AddCol(question.QuestionText, gridReports);
                questionsIds.Add(question.Id);
            }
            
            foreach (var careHome in relatedCareHomes)
            {
                var values = new List<string>();
                
                values.Add(careHome.Id.ToString());
                values.Add(careHome.FacilityName);

                foreach (Guid questionId in questionsIds)
                {
                    var answer = answers.SingleOrDefault(s => s.QuestionId == questionId && s.FacilityId == careHome.Id);
                    if (answer == null)
                    {
                        values.Add("N/A");
                    }
                    else
                    {
                        switch (answer.QuestionTypeId)
                        {
                            case 0:
                                values.Add((answer.AnswerBool ?? false) ? "Yes" : "No");
                                break;

                            case 1:
                                values.Add(answer.AnswerText);
                                break;

                            default: throw new Exception($"QuestionTypeId: {answer.QuestionTypeId} not found");
                        }
                    }
                }

                int rowIndex = gridReports.Rows.Add(values.ToArray());                
            }

            
        }

        private void ShowReportActivities(DbEntities db)
        {
            AddCol("Id", gridReports, visible: false);
            AddCol("Action", gridReports);
            AddCol("On", gridReports);
            AddCol("Facility/Care home", gridReports);
            AddCol("Patient Name", gridReports);
            AddCol("Recipient", gridReports);
            AddCol("Status", gridReports);
            AddCol("Error", gridReports);
            AddCol("User", gridReports);

            var actionsPredicate = PredicateBuilder.False<ActionsReport>();
            if (afEmailsToFacilities.Checked) actionsPredicate = actionsPredicate.Or(p => p.ActionId == 1);
            if (afEmailsToPatients.Checked) actionsPredicate = actionsPredicate.Or(p => p.ActionId == 2);
            if (afSms.Checked) actionsPredicate = actionsPredicate.Or(p => p.ActionId == 3);

            var invs =
                (from i in db.ActionsReport.Where(actionsPredicate)
                 from f in db.CareHomes.Where(w => w.Id == i.FacilityId).DefaultIfEmpty()
                 join p in db.Patients on i.PatientId equals p.Id
                 join u in db.Users on i.UserId equals u.Id
                 select new
                 {
                     i,
                     f,
                     p,
                     u
                 }
                 ).ToList();


            foreach (var inv in invs)
            {
                string action;
                switch (inv.i.ActionId)
                {
                    case 1: action = "Facility Email"; break;
                    case 2: action = "Patient Email"; break;
                    case 3: action = "SMS sent"; break;
                    default: action = "unknown: " + inv.i.ActionId.ToString(); break;
                }

                gridReports.Rows.Add
                (
                    inv.i.Id,
                    action,
                    inv.i.Created.ToString("MM-dd-yyyy"),
                    inv.f?.FacilityName ?? "N/A",
                    inv.p.FirstName + " " + inv.p.LastName,
                    inv.i.Recipient,
                    (inv.i.Status == 1 ? "OK" : "Error"),
                    inv.i.ErrorText,
                    inv.u.UserName
                );
            }
        }

        private void ShowReportSentInvitations(DbEntities db)
        {
            AddCol("Id", gridReports, visible: false);
            AddCol("Care home", gridReports);
            AddCol("Address", gridReports);
            AddCol("City", gridReports);
            AddCol("Zip", gridReports);
            AddCol("Phone", gridReports);
            AddCol("Patient", gridReports);
            AddCol("Have interest", gridReports);
            AddCol("Invitation Sent", gridReports);
            AddCol("Exact sms text", gridReports);
            AddCol("Error", gridReports);
            AddCol("Short Url", gridReports);

            var invs =
                (from i in db.InterestedPatients
                 join f in db.CareHomes on i.FacilityId equals f.Id
                 join p in db.Patients on i.PatientId equals p.Id
                 select new
                 {
                     haveInterest = i.HaveInterest,
                     errorText = i.Error,
                     invitationSentOn = i.InvitationSent,
                     phontSentTo = i.PhoneSentTo,
                     smsText = i.SmsText,
                     shortUrl = i.ShortURL,

                     facilityName = f.FacilityName,
                     facilityId = f.Id,
                     facilityAddress = f.AddressLine1,
                     facilityCity = f.City,
                     facilityZip = f.Zip,

                     patientFirstName = p.FirstName,
                     patientLasttName = p.LastName,
                     patientId = p.Id
                 });

            if (sentInvitationOnlyInterested.Checked)
            {
                invs = invs.Where(w => w.haveInterest == 1);
            }

            if (reportSentInvitationShowErrorsOnly.Checked)
            {
                invs = invs.Where(w => w.errorText != null && w.errorText.Length > 0 && w.errorText != "Fixed");
            }

            if (!String.IsNullOrWhiteSpace(cbReportsInvitationsCareHomes.Text))
            {
                invs = invs.Where(w => w.facilityName.Contains(cbReportsInvitationsCareHomes.Text.Trim()));
            }

            if (!String.IsNullOrWhiteSpace(cbReportsInvitationsPatients.Text))
            {
                invs = invs.Where(w => (w.patientFirstName + " " + w.patientLasttName).Contains(cbReportsInvitationsPatients.Text.Trim()));
            }

            var sortInvs = invs.OrderBy(o => o.patientId).ThenByDescending(o => o.invitationSentOn);

            foreach (var inv in sortInvs.ToList())
            {
                gridReports.Rows.Add
                (
                    inv.facilityId,
                    inv.facilityName,
                    inv.facilityAddress,
                    inv.facilityCity,
                    inv.facilityZip,
                    inv.phontSentTo,
                    inv.patientFirstName + " " + inv.patientLasttName,
                    inv.haveInterest == 1 ? "Yes" : (inv.errorText == null ? "No" : "Error"),
                    inv.invitationSentOn,
                    inv.smsText,
                    inv.errorText,
                    inv.shortUrl
                );
            }

        }

        private void ShowReportSentEmails(DbEntities db)
        {
            AddCol("Id", gridReports, visible: false);
            AddCol("Assisted living", gridReports);
            AddCol("Address", gridReports);
            AddCol("City", gridReports);
            AddCol("Zip", gridReports);
            AddCol("Phone Number", gridReports);
            AddCol("Website", gridReports);
            AddCol("Patient", gridReports);
            AddCol("Email send to", gridReports);
            AddCol("Email Sent", gridReports);
            AddCol("Error", gridReports);

            var invs =
                (from i in db.ActionsReport
                 join f in db.AssistedLiving on i.FacilityId equals f.Id
                 join p in db.Patients on i.PatientId equals p.Id
                 join q in db.QueueEmails on i.Id equals q.ActionsReportId
                 select new
                 {
                     facilityId = i.FacilityId,
                     errorText = i.ErrorText,
                     recipient = i.Recipient,
                     facilityName = f.Name,
                     facilityAddress = f.AddressLine,
                     facilityZip = f.Zip,
                     facilityWebsite = f.WebSite,
                     facilityPhone = f.Phone,
                     patientFirstName = p.FirstName,
                     patientLastName = p.LastName,
                     emailSentOn = q.Updated,
                     emailSentStatus = q.StatusId
                 }
                 );

            if (chkReportsSentEmailsShowErrorsOnly.Checked)
            {
                invs = invs.Where(w => w.errorText != null && w.errorText.Length > 0 && w.errorText != "Fixed");
            }

            if (!String.IsNullOrWhiteSpace(cbReportsSentEmailsAL.Text))
            {
                invs = invs.Where(w => w.facilityName.Contains(cbReportsSentEmailsAL.Text.Trim()));
            }

            if (!String.IsNullOrWhiteSpace(cbReportsSentEmailsPatients.Text))
            {
                invs = invs.Where(w => (w.patientFirstName + " " + w.patientLastName).Contains(cbReportsSentEmailsPatients.Text.Trim()));
            }

            var sortInvs = invs.Where(w => w.emailSentStatus != -2).OrderByDescending(o => o.emailSentOn);

            foreach (var inv in sortInvs.ToList())
            {
                DbCache.ZipEntity selectedZip = DbCache.fullCache.AsParallel().Where(w => w.zip == inv.facilityZip).SingleOrDefault();
                gridReports.Rows.Add
                (
                    inv.facilityId,
                    inv.facilityName,
                    inv.facilityAddress,
                    selectedZip.city,
                    inv.facilityZip,
                    inv.facilityPhone,
                    inv.facilityWebsite,
                    inv.patientFirstName + " " + inv.patientLastName,
                    inv.recipient,
                    inv.emailSentOn,
                    inv.errorText
                );
            }

        }

        private void ShowReportSentEmailsForPatientsReport(DbEntities db)
        {
            AddCol("Id", gridReports, visible: false);
            AddCol("Assisted living", gridReports);
            AddCol("Address", gridReports);
            AddCol("City", gridReports);
            AddCol("Zip", gridReports);
            AddCol("Phone Number", gridReports);
            AddCol("Website", gridReports);
            AddCol("Email 1", gridReports);
            AddCol("Email 2", gridReports);
            AddCol("Email 3", gridReports);
            AddCol("Email 4", gridReports);
            AddCol("Email 5", gridReports);

            var invs =
                (from i in db.ActionsReport
                 join f in db.AssistedLiving on i.FacilityId equals f.Id
                 join p in db.Patients on i.PatientId equals p.Id
                 join q in db.QueueEmails on i.Id equals q.ActionsReportId
                 where i.ErrorText == null && q.StatusId == 1
                 select new
                 {
                     facilityId = i.FacilityId,
                     errorText = i.ErrorText,
                     recipient = i.Recipient,
                     facilityName = f.Name,
                     facilityAddress = f.AddressLine,
                     facilityZip = f.Zip,
                     facilityWebsite = f.WebSite,
                     facilityPhone = f.Phone,
                     patientFirstName = p.FirstName,
                     patientLastName = p.LastName,
                     emailSentOn = q.Updated
                 }
                 );

            if (!String.IsNullOrWhiteSpace(cbReportsSentEmailsAL.Text))
            {
                invs = invs.Where(w => w.facilityName.Contains(cbReportsSentEmailsAL.Text.Trim()));
            }

            if (!String.IsNullOrWhiteSpace(cbReportsSentEmailsPatients.Text))
            {
                invs = invs.Where(w => (w.patientFirstName + " " + w.patientLastName).Contains(cbReportsSentEmailsPatients.Text.Trim()));
            }

            var groupedByFacility = invs.GroupBy(g => g.facilityId).ToList();

            foreach (var inv in groupedByFacility.ToList())
            {
                var fac = inv.First();
                DbCache.ZipEntity selectedZip = DbCache.fullCache.AsParallel().Where(w => w.zip == fac.facilityZip).SingleOrDefault();
                List<string> emails = inv.Select(s => s.recipient).ToList();

                gridReports.Rows.Add
                (
                    fac.facilityId,
                    fac.facilityName,
                    fac.facilityAddress,
                    selectedZip.city,
                    fac.facilityZip,
                    fac.facilityPhone,
                    fac.facilityWebsite,
                    emails.Count > 0 ? emails[0] : "",
                    emails.Count > 1 ? emails[1] : "",
                    emails.Count > 2 ? emails[2] : "",
                    emails.Count > 3 ? emails[3] : "",
                    emails.Count > 4 ? emails[4] : ""
                );
            }

        }

        private void cbReportTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            //1 - care homes rerpot
            //2 - emails sent report
            //3 - emails sent report for patients
            //4 - activity report
            //5 - question campaign answers
            groupSentInvitationsFilters.Visible = cbReportTypes.SelectedIndex == 0;
            gbReportsEmailsGroup.Visible = (cbReportTypes.SelectedIndex == 1 || cbReportTypes.SelectedIndex == 2);
            chkReportsSentEmailsShowErrorsOnly.Visible = cbReportTypes.SelectedIndex == 1;
            groupActivityFilters.Visible = cbReportTypes.SelectedIndex == 3;
            groupReportsQuestionCampaignFilters.Visible = cbReportTypes.SelectedIndex == 4;

            switch (cbReportTypes.SelectedIndex)
            {
                case 0:
                    gridReports.ContextMenuStrip = contextReportInvitations;
                    break;

                case 1:
                    gridReports.ContextMenuStrip = null;
                    break;

                case 2:
                    gridReports.ContextMenuStrip = null;
                    break;

                case 3:
                    gridReports.ContextMenuStrip = null;
                    break;

                case 4:
                    gridReports.ContextMenuStrip = null;
                    break;
            }
        }
    }
}