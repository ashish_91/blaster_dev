﻿namespace AssistedLivingFacilityBlaster
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerLoadForm = new System.Windows.Forms.Timer(this.components);
            this.tabMainControl = new System.Windows.Forms.TabControl();
            this.tabFindCH = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.txtFilterSemiPrivateRoomTo = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.txtFilterSemiPrivateRoomFrom = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cbRadius = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtFilterPrivateRoomTo = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.txtFilterPrivateRoomFrom = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rbALTCSYes = new System.Windows.Forms.RadioButton();
            this.rbALTCSNo = new System.Windows.Forms.RadioButton();
            this.btnSearch = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.cbCareHomesLoadALPatientsToo = new System.Windows.Forms.CheckBox();
            this.btnAddNewCareHomePatient = new System.Windows.Forms.Button();
            this.cbCareHomesPatientSelect = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lblMainSelectedCount = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.btnRefreshStatus = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnSelectOnlyNotNotified = new System.Windows.Forms.Button();
            this.btnCheckAll = new System.Windows.Forms.Button();
            this.btnSendSmsToSelectedCH = new System.Windows.Forms.Button();
            this.gridSuitableCareHomes = new System.Windows.Forms.DataGridView();
            this.facilitiesId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridFacilitySelect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.facilitiesFacilityName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.facilitiesALTSC = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.facilitiesDistance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.facilitiesCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.facilitiesPrivateRoomPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.facilitiesSemiPrivateRoomPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.facilitiesNotified = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.facilitiesResponse = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.facilitiesNotifiedOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.facilitiesNotifiedStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabFindAL = new System.Windows.Forms.TabPage();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.btnFindALSelectEmailedOnly = new System.Windows.Forms.Button();
            this.btnFindAlSelectNotEmailed = new System.Windows.Forms.Button();
            this.btnFindALDeselectAll = new System.Windows.Forms.Button();
            this.btnALSendEmails = new System.Windows.Forms.Button();
            this.gridSuitableAL = new System.Windows.Forms.DataGridView();
            this.ColFindALId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFindSelect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColFindALName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFindALALTCS = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColFindALDistance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFindAlCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFindALIndependentLivingPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFindALAssistedLivingPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFindALMemoryCarePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFindALSkilledNursingCarePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFindALEmailed = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColFindALEmailedOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAlEmailStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtALMilesRange = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this.btnALSearch = new System.Windows.Forms.Button();
            this.btnFindAlZipCodesHelp = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.txtFindALZips = new System.Windows.Forms.TextBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.rbFIndAlALTCSYes = new System.Windows.Forms.RadioButton();
            this.rbFIndAlALTCSNo = new System.Windows.Forms.RadioButton();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.checkSkilledNursing = new System.Windows.Forms.CheckBox();
            this.checkMemCare = new System.Windows.Forms.CheckBox();
            this.checkAssLiv = new System.Windows.Forms.CheckBox();
            this.checkIndLiv = new System.Windows.Forms.CheckBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.btnALnewPatient = new System.Windows.Forms.Button();
            this.cbFindALPatient = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.tabRegisterPatients = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.btnDeletePatient = new System.Windows.Forms.Button();
            this.btnEditPatient = new System.Windows.Forms.Button();
            this.btnAddPatient = new System.Windows.Forms.Button();
            this.gridPatients = new System.Windows.Forms.DataGridView();
            this.ColPatientId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPatientFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPatientAge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPatientState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PatientColCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPatientEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPatientPhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPatientZip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPatientALTCS = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColPatientType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPatientNotes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabRegisterAL = new System.Windows.Forms.TabPage();
            this.btnALDelete = new System.Windows.Forms.Button();
            this.btnALEdit = new System.Windows.Forms.Button();
            this.btnALAdd = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.gridAssistedLiving = new System.Windows.Forms.DataGridView();
            this.ColAlId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CoLAlName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAlWebSite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAlPhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAlZip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAlIndependentLivingPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAlAssistedLivingPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAlMemoryCarePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAlSkilledNursingCarePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAlEmail1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAlEmail2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAlEmail3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAlNotes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabRegisterCH = new System.Windows.Forms.TabPage();
            this.btnFacilitiesRefresh = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEditFacility = new System.Windows.Forms.Button();
            this.btnAddNewFacility = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnSearchFacitilies = new System.Windows.Forms.Button();
            this.gridFacilities = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FacilityName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FacilityType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WebSite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Emails = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Phones = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Faxes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.State = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.City = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Zip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddressLine = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactPersons = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ALTSC = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Updated = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Notes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabAdmin = new System.Windows.Forms.TabPage();
            this.btnRefreshAgents = new System.Windows.Forms.Button();
            this.btnDeleteAgents = new System.Windows.Forms.Button();
            this.btnEditAgent2 = new System.Windows.Forms.Button();
            this.btnAddNewAgent = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.gridAgents = new System.Windows.Forms.DataGridView();
            this.AccountsColumnId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountsColumnUserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountsColumnAgentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountsColumnEnabled = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.AccountsColumnIsAdmin = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.AccountsColumnLastLogin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountsColumnCreated = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabReports = new System.Windows.Forms.TabPage();
            this.groupReportsQuestionCampaignFilters = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this.comboReportsQuestionCampaigns = new System.Windows.Forms.ComboBox();
            this.gbReportsEmailsGroup = new System.Windows.Forms.GroupBox();
            this.cbReportsSentEmailsAL = new System.Windows.Forms.ComboBox();
            this.cbReportsSentEmailsPatients = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.chkReportsSentEmailsShowErrorsOnly = new System.Windows.Forms.CheckBox();
            this.button3 = new System.Windows.Forms.Button();
            this.btnExportSelectedToSpaceFile = new System.Windows.Forms.Button();
            this.gridReports = new System.Windows.Forms.DataGridView();
            this.btnLoadReport = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cbReportTypes = new System.Windows.Forms.ComboBox();
            this.groupSentInvitationsFilters = new System.Windows.Forms.GroupBox();
            this.cbReportsInvitationsCareHomes = new System.Windows.Forms.ComboBox();
            this.cbReportsInvitationsPatients = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.reportSentInvitationShowErrorsOnly = new System.Windows.Forms.CheckBox();
            this.sentInvitationOnlyInterested = new System.Windows.Forms.CheckBox();
            this.groupActivityFilters = new System.Windows.Forms.GroupBox();
            this.afEmailsToPatients = new System.Windows.Forms.CheckBox();
            this.afEmailsToFacilities = new System.Windows.Forms.CheckBox();
            this.afSms = new System.Windows.Forms.CheckBox();
            this.tabSmsQueue = new System.Windows.Forms.TabPage();
            this.btnRemoveFromSmsQueue = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lblWillStopAfter = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.chkAutoStop = new System.Windows.Forms.CheckBox();
            this.numAutoStopCount = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.lblFailedSend = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lblSuccessfullySend = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lblQueueSize = new System.Windows.Forms.Label();
            this.btnToggle = new System.Windows.Forms.Button();
            this.gridSmsQueue = new System.Windows.Forms.DataGridView();
            this.GridQueueId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridQueueStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridQueueRecipient = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridQueueFacility = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridQueueAbout = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridQueueActionOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridQueueShortUrl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridQueueMessage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnRemoveEmailsFromQueue = new System.Windows.Forms.Button();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.lblQueueEmailsErrors = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.lblQueueEmailsSuccessfully = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.lblQueueEmailsPending = new System.Windows.Forms.Label();
            this.btnToggleEmailQueue = new System.Windows.Forms.Button();
            this.gridEmailsQueue = new System.Windows.Forms.DataGridView();
            this.ColEmailsQueueId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEmailsQueueStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEmailsQueueRecipient = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEmailsQueueFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEmailsQueueFacilityName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEmailsQueuePatientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEmailsQueueSubject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.btnQC_EditQuestionCampaign = new System.Windows.Forms.Button();
            this.btnQC_AddQuestionCampaign = new System.Windows.Forms.Button();
            this.cbQC_QuestionsCampaignSelect = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.lblQC_selectedCH = new System.Windows.Forms.Label();
            this.label12345 = new System.Windows.Forms.Label();
            this.btnQC_SelectOnlyQuestioned = new System.Windows.Forms.Button();
            this.btnQC_SelectOnlyNotQuestioned = new System.Windows.Forms.Button();
            this.btnQC_DeselectAll = new System.Windows.Forms.Button();
            this.btnQC_SendQuestions = new System.Windows.Forms.Button();
            this.gridQC_FoundedCareHomes = new System.Windows.Forms.DataGridView();
            this.gridColQC_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColQC_selected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.gridColQC_facilityName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColQC_altcs = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.gridColQC_distance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColQC_city = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColQC_privateRoom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColQC_semiPrivateRoom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColQC_questioned = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.gridColQC_responce = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.gridColQC_questionedOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.txtQC_zipRadius = new System.Windows.Forms.NumericUpDown();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.txtQC_zipCode = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.edQC_SemiPirvateRoomTo = new System.Windows.Forms.NumericUpDown();
            this.label32 = new System.Windows.Forms.Label();
            this.edQC_SemiPirvateRoomFrom = new System.Windows.Forms.NumericUpDown();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.edQC_PirvateRoomTo = new System.Windows.Forms.NumericUpDown();
            this.label36 = new System.Windows.Forms.Label();
            this.edQC_PirvateRoomFrom = new System.Windows.Forms.NumericUpDown();
            this.label38 = new System.Windows.Forms.Label();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.radioQC_ALTCS_Yes = new System.Windows.Forms.RadioButton();
            this.radioQC_ALTCS_No = new System.Windows.Forms.RadioButton();
            this.btnQC_FilterCareHomes = new System.Windows.Forms.Button();
            this.lblProgress = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.contextReportInvitations = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.timerUpdateSelectedCount = new System.Windows.Forms.Timer(this.components);
            this.tabMainControl.SuspendLayout();
            this.tabFindCH.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterSemiPrivateRoomTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterSemiPrivateRoomFrom)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterPrivateRoomTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterPrivateRoomFrom)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSuitableCareHomes)).BeginInit();
            this.tabFindAL.SuspendLayout();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSuitableAL)).BeginInit();
            this.groupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtALMilesRange)).BeginInit();
            this.groupBox18.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.tabRegisterPatients.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPatients)).BeginInit();
            this.tabRegisterAL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAssistedLiving)).BeginInit();
            this.tabRegisterCH.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridFacilities)).BeginInit();
            this.tabAdmin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAgents)).BeginInit();
            this.tabReports.SuspendLayout();
            this.groupReportsQuestionCampaignFilters.SuspendLayout();
            this.gbReportsEmailsGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridReports)).BeginInit();
            this.groupSentInvitationsFilters.SuspendLayout();
            this.groupActivityFilters.SuspendLayout();
            this.tabSmsQueue.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAutoStopCount)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSmsQueue)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.groupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEmailsQueue)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridQC_FoundedCareHomes)).BeginInit();
            this.groupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtQC_zipRadius)).BeginInit();
            this.groupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edQC_SemiPirvateRoomTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edQC_SemiPirvateRoomFrom)).BeginInit();
            this.groupBox23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edQC_PirvateRoomTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edQC_PirvateRoomFrom)).BeginInit();
            this.groupBox24.SuspendLayout();
            this.contextReportInvitations.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerLoadForm
            // 
            this.timerLoadForm.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tabMainControl
            // 
            this.tabMainControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabMainControl.Controls.Add(this.tabFindCH);
            this.tabMainControl.Controls.Add(this.tabFindAL);
            this.tabMainControl.Controls.Add(this.tabRegisterPatients);
            this.tabMainControl.Controls.Add(this.tabRegisterAL);
            this.tabMainControl.Controls.Add(this.tabRegisterCH);
            this.tabMainControl.Controls.Add(this.tabAdmin);
            this.tabMainControl.Controls.Add(this.tabReports);
            this.tabMainControl.Controls.Add(this.tabSmsQueue);
            this.tabMainControl.Controls.Add(this.tabPage1);
            this.tabMainControl.Controls.Add(this.tabPage2);
            this.tabMainControl.Location = new System.Drawing.Point(4, 3);
            this.tabMainControl.Name = "tabMainControl";
            this.tabMainControl.SelectedIndex = 0;
            this.tabMainControl.Size = new System.Drawing.Size(1185, 641);
            this.tabMainControl.TabIndex = 18;
            // 
            // tabFindCH
            // 
            this.tabFindCH.Controls.Add(this.groupBox2);
            this.tabFindCH.Controls.Add(this.groupBox1);
            this.tabFindCH.Controls.Add(this.groupBox8);
            this.tabFindCH.Location = new System.Drawing.Point(4, 22);
            this.tabFindCH.Name = "tabFindCH";
            this.tabFindCH.Padding = new System.Windows.Forms.Padding(3);
            this.tabFindCH.Size = new System.Drawing.Size(1177, 615);
            this.tabFindCH.TabIndex = 0;
            this.tabFindCH.Text = "Find Care Home";
            this.tabFindCH.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.groupBox9);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.cbRadius);
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.btnSearch);
            this.groupBox2.Location = new System.Drawing.Point(383, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(700, 113);
            this.groupBox2.TabIndex = 32;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "                                                                        ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(6, -1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(145, 16);
            this.label6.TabIndex = 62;
            this.label6.Text = "2. Care home select";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.txtFilterSemiPrivateRoomTo);
            this.groupBox9.Controls.Add(this.label17);
            this.groupBox9.Controls.Add(this.txtFilterSemiPrivateRoomFrom);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Location = new System.Drawing.Point(267, 25);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(246, 73);
            this.groupBox9.TabIndex = 34;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Semi-Private room patient budget:";
            // 
            // txtFilterSemiPrivateRoomTo
            // 
            this.txtFilterSemiPrivateRoomTo.Location = new System.Drawing.Point(147, 34);
            this.txtFilterSemiPrivateRoomTo.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtFilterSemiPrivateRoomTo.Name = "txtFilterSemiPrivateRoomTo";
            this.txtFilterSemiPrivateRoomTo.Size = new System.Drawing.Size(82, 20);
            this.txtFilterSemiPrivateRoomTo.TabIndex = 3;
            this.txtFilterSemiPrivateRoomTo.ThousandsSeparator = true;
            this.txtFilterSemiPrivateRoomTo.Value = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(144, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(73, 13);
            this.label17.TabIndex = 2;
            this.label17.Text = "To (in dollars):";
            // 
            // txtFilterSemiPrivateRoomFrom
            // 
            this.txtFilterSemiPrivateRoomFrom.Location = new System.Drawing.Point(23, 34);
            this.txtFilterSemiPrivateRoomFrom.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtFilterSemiPrivateRoomFrom.Name = "txtFilterSemiPrivateRoomFrom";
            this.txtFilterSemiPrivateRoomFrom.Size = new System.Drawing.Size(82, 20);
            this.txtFilterSemiPrivateRoomFrom.TabIndex = 1;
            this.txtFilterSemiPrivateRoomFrom.ThousandsSeparator = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(20, 20);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "From (in dollars):";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(604, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Radius (in miles):";
            // 
            // cbRadius
            // 
            this.cbRadius.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRadius.FormattingEnabled = true;
            this.cbRadius.Items.AddRange(new object[] {
            "5 miles",
            "10 miles",
            "15 miles",
            "20 miles",
            "25 miles",
            "30 miles",
            "35 miles",
            "40 miles",
            "45 miles",
            "50 miles",
            "55 miles",
            "60 miles",
            "65 miles",
            "70 miles",
            "75 miles",
            "80 miles",
            "85 miles",
            "90 miles",
            "95 miles",
            "100 miles",
            "All"});
            this.cbRadius.Location = new System.Drawing.Point(607, 45);
            this.cbRadius.Name = "cbRadius";
            this.cbRadius.Size = new System.Drawing.Size(83, 21);
            this.cbRadius.TabIndex = 28;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtFilterPrivateRoomTo);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.txtFilterPrivateRoomFrom);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Location = new System.Drawing.Point(15, 25);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(246, 73);
            this.groupBox6.TabIndex = 33;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Private room patient budget:";
            // 
            // txtFilterPrivateRoomTo
            // 
            this.txtFilterPrivateRoomTo.Location = new System.Drawing.Point(141, 34);
            this.txtFilterPrivateRoomTo.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtFilterPrivateRoomTo.Name = "txtFilterPrivateRoomTo";
            this.txtFilterPrivateRoomTo.Size = new System.Drawing.Size(82, 20);
            this.txtFilterPrivateRoomTo.TabIndex = 3;
            this.txtFilterPrivateRoomTo.ThousandsSeparator = true;
            this.txtFilterPrivateRoomTo.Value = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(138, 20);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "To (in dollars):";
            // 
            // txtFilterPrivateRoomFrom
            // 
            this.txtFilterPrivateRoomFrom.Location = new System.Drawing.Point(23, 34);
            this.txtFilterPrivateRoomFrom.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtFilterPrivateRoomFrom.Name = "txtFilterPrivateRoomFrom";
            this.txtFilterPrivateRoomFrom.Size = new System.Drawing.Size(82, 20);
            this.txtFilterPrivateRoomFrom.TabIndex = 1;
            this.txtFilterPrivateRoomFrom.ThousandsSeparator = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(20, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "From (in dollars):";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rbALTCSYes);
            this.groupBox5.Controls.Add(this.rbALTCSNo);
            this.groupBox5.Location = new System.Drawing.Point(530, 25);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(71, 73);
            this.groupBox5.TabIndex = 58;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "ALTCS";
            // 
            // rbALTCSYes
            // 
            this.rbALTCSYes.AutoSize = true;
            this.rbALTCSYes.Location = new System.Drawing.Point(12, 20);
            this.rbALTCSYes.Name = "rbALTCSYes";
            this.rbALTCSYes.Size = new System.Drawing.Size(43, 17);
            this.rbALTCSYes.TabIndex = 6;
            this.rbALTCSYes.TabStop = true;
            this.rbALTCSYes.Text = "Yes";
            this.rbALTCSYes.UseVisualStyleBackColor = true;
            // 
            // rbALTCSNo
            // 
            this.rbALTCSNo.AutoSize = true;
            this.rbALTCSNo.Checked = true;
            this.rbALTCSNo.Location = new System.Drawing.Point(12, 44);
            this.rbALTCSNo.Name = "rbALTCSNo";
            this.rbALTCSNo.Size = new System.Drawing.Size(39, 17);
            this.rbALTCSNo.TabIndex = 7;
            this.rbALTCSNo.TabStop = true;
            this.rbALTCSNo.Text = "No";
            this.rbALTCSNo.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSearch.Location = new System.Drawing.Point(607, 75);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(83, 23);
            this.btnSearch.TabIndex = 26;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click_1);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.cbCareHomesLoadALPatientsToo);
            this.groupBox1.Controls.Add(this.btnAddNewCareHomePatient);
            this.groupBox1.Controls.Add(this.cbCareHomesPatientSelect);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(7, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(355, 113);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "                                                   ";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(270, 15);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 27);
            this.button2.TabIndex = 32;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cbCareHomesLoadALPatientsToo
            // 
            this.cbCareHomesLoadALPatientsToo.AutoSize = true;
            this.cbCareHomesLoadALPatientsToo.Location = new System.Drawing.Point(16, 27);
            this.cbCareHomesLoadALPatientsToo.Name = "cbCareHomesLoadALPatientsToo";
            this.cbCareHomesLoadALPatientsToo.Size = new System.Drawing.Size(128, 17);
            this.cbCareHomesLoadALPatientsToo.TabIndex = 65;
            this.cbCareHomesLoadALPatientsToo.Text = "Load also AL patients";
            this.cbCareHomesLoadALPatientsToo.UseVisualStyleBackColor = true;
            // 
            // btnAddNewCareHomePatient
            // 
            this.btnAddNewCareHomePatient.Location = new System.Drawing.Point(270, 57);
            this.btnAddNewCareHomePatient.Name = "btnAddNewCareHomePatient";
            this.btnAddNewCareHomePatient.Size = new System.Drawing.Size(70, 23);
            this.btnAddNewCareHomePatient.TabIndex = 64;
            this.btnAddNewCareHomePatient.Text = "Add new...";
            this.btnAddNewCareHomePatient.UseVisualStyleBackColor = true;
            this.btnAddNewCareHomePatient.Click += new System.EventHandler(this.btnAddNewCareHomePatient_Click);
            // 
            // cbCareHomesPatientSelect
            // 
            this.cbCareHomesPatientSelect.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbCareHomesPatientSelect.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCareHomesPatientSelect.DisplayMember = "Text";
            this.cbCareHomesPatientSelect.FormattingEnabled = true;
            this.cbCareHomesPatientSelect.Location = new System.Drawing.Point(87, 58);
            this.cbCareHomesPatientSelect.Name = "cbCareHomesPatientSelect";
            this.cbCareHomesPatientSelect.Size = new System.Drawing.Size(177, 21);
            this.cbCareHomesPatientSelect.TabIndex = 63;
            this.cbCareHomesPatientSelect.ValueMember = "Value";
            this.cbCareHomesPatientSelect.SelectedIndexChanged += new System.EventHandler(this.cbCareHomesPatientSelect_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 62;
            this.label1.Text = "Patient name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(10, -1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(132, 16);
            this.label5.TabIndex = 61;
            this.label5.Text = "1. Select a patient";
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(2, 293);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1021, 6);
            this.groupBox3.TabIndex = 35;
            this.groupBox3.TabStop = false;
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox8.Controls.Add(this.lblMainSelectedCount);
            this.groupBox8.Controls.Add(this.label22);
            this.groupBox8.Controls.Add(this.btnRefreshStatus);
            this.groupBox8.Controls.Add(this.button1);
            this.groupBox8.Controls.Add(this.btnSelectOnlyNotNotified);
            this.groupBox8.Controls.Add(this.btnCheckAll);
            this.groupBox8.Controls.Add(this.btnSendSmsToSelectedCH);
            this.groupBox8.Controls.Add(this.gridSuitableCareHomes);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(7, 126);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(1164, 483);
            this.groupBox8.TabIndex = 30;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Founded care homes:";
            // 
            // lblMainSelectedCount
            // 
            this.lblMainSelectedCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblMainSelectedCount.AutoSize = true;
            this.lblMainSelectedCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMainSelectedCount.Location = new System.Drawing.Point(613, 459);
            this.lblMainSelectedCount.Name = "lblMainSelectedCount";
            this.lblMainSelectedCount.Size = new System.Drawing.Size(14, 13);
            this.lblMainSelectedCount.TabIndex = 67;
            this.lblMainSelectedCount.Text = "0";
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(533, 459);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(82, 13);
            this.label22.TabIndex = 66;
            this.label22.Text = "Selected count:";
            // 
            // btnRefreshStatus
            // 
            this.btnRefreshStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefreshStatus.Location = new System.Drawing.Point(643, 118);
            this.btnRefreshStatus.Name = "btnRefreshStatus";
            this.btnRefreshStatus.Size = new System.Drawing.Size(175, 23);
            this.btnRefreshStatus.TabIndex = 65;
            this.btnRefreshStatus.Text = "Refresh facilities for this patient";
            this.btnRefreshStatus.UseVisualStyleBackColor = true;
            this.btnRefreshStatus.Visible = false;
            this.btnRefreshStatus.Click += new System.EventHandler(this.btnRefreshStatus_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(315, 454);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(212, 23);
            this.button1.TabIndex = 64;
            this.button1.Text = "Select successfully texted";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSelectOnlyNotNotified
            // 
            this.btnSelectOnlyNotNotified.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSelectOnlyNotNotified.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectOnlyNotNotified.Location = new System.Drawing.Point(97, 454);
            this.btnSelectOnlyNotNotified.Name = "btnSelectOnlyNotNotified";
            this.btnSelectOnlyNotNotified.Size = new System.Drawing.Size(212, 23);
            this.btnSelectOnlyNotNotified.TabIndex = 63;
            this.btnSelectOnlyNotNotified.Text = "Select only not texted";
            this.btnSelectOnlyNotNotified.UseVisualStyleBackColor = true;
            this.btnSelectOnlyNotNotified.Click += new System.EventHandler(this.btnSelectOnlyNotNotified_Click);
            // 
            // btnCheckAll
            // 
            this.btnCheckAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCheckAll.Location = new System.Drawing.Point(16, 454);
            this.btnCheckAll.Name = "btnCheckAll";
            this.btnCheckAll.Size = new System.Drawing.Size(75, 23);
            this.btnCheckAll.TabIndex = 32;
            this.btnCheckAll.Text = "Deselect all";
            this.btnCheckAll.UseVisualStyleBackColor = true;
            this.btnCheckAll.Click += new System.EventHandler(this.btnCheckAll_Click);
            // 
            // btnSendSmsToSelectedCH
            // 
            this.btnSendSmsToSelectedCH.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendSmsToSelectedCH.Location = new System.Drawing.Point(1017, 454);
            this.btnSendSmsToSelectedCH.Name = "btnSendSmsToSelectedCH";
            this.btnSendSmsToSelectedCH.Size = new System.Drawing.Size(133, 23);
            this.btnSendSmsToSelectedCH.TabIndex = 31;
            this.btnSendSmsToSelectedCH.Text = "3. Send Text messages";
            this.btnSendSmsToSelectedCH.UseVisualStyleBackColor = true;
            this.btnSendSmsToSelectedCH.Click += new System.EventHandler(this.btnSendClientEmail_Click);
            // 
            // gridSuitableCareHomes
            // 
            this.gridSuitableCareHomes.AllowUserToAddRows = false;
            this.gridSuitableCareHomes.AllowUserToDeleteRows = false;
            this.gridSuitableCareHomes.AllowUserToOrderColumns = true;
            this.gridSuitableCareHomes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSuitableCareHomes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridSuitableCareHomes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.facilitiesId,
            this.gridFacilitySelect,
            this.facilitiesFacilityName,
            this.facilitiesALTSC,
            this.facilitiesDistance,
            this.facilitiesCity,
            this.facilitiesPrivateRoomPrice,
            this.facilitiesSemiPrivateRoomPrice,
            this.facilitiesNotified,
            this.facilitiesResponse,
            this.facilitiesNotifiedOn,
            this.facilitiesNotifiedStatus});
            this.gridSuitableCareHomes.Location = new System.Drawing.Point(8, 22);
            this.gridSuitableCareHomes.Name = "gridSuitableCareHomes";
            this.gridSuitableCareHomes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridSuitableCareHomes.Size = new System.Drawing.Size(1142, 426);
            this.gridSuitableCareHomes.TabIndex = 29;
            this.gridSuitableCareHomes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridSuitableFacilities_CellContentClick_1);
            this.gridSuitableCareHomes.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridSuitableFacilities_CellValueChanged);
            // 
            // facilitiesId
            // 
            this.facilitiesId.HeaderText = "Id";
            this.facilitiesId.Name = "facilitiesId";
            this.facilitiesId.Visible = false;
            // 
            // gridFacilitySelect
            // 
            this.gridFacilitySelect.FillWeight = 40F;
            this.gridFacilitySelect.HeaderText = "Select";
            this.gridFacilitySelect.Name = "gridFacilitySelect";
            this.gridFacilitySelect.Width = 40;
            // 
            // facilitiesFacilityName
            // 
            this.facilitiesFacilityName.FillWeight = 185F;
            this.facilitiesFacilityName.HeaderText = "Facility Name";
            this.facilitiesFacilityName.MinimumWidth = 100;
            this.facilitiesFacilityName.Name = "facilitiesFacilityName";
            this.facilitiesFacilityName.ReadOnly = true;
            this.facilitiesFacilityName.Width = 185;
            // 
            // facilitiesALTSC
            // 
            this.facilitiesALTSC.FillWeight = 50F;
            this.facilitiesALTSC.HeaderText = "ALTCS";
            this.facilitiesALTSC.Name = "facilitiesALTSC";
            this.facilitiesALTSC.ReadOnly = true;
            this.facilitiesALTSC.Width = 50;
            // 
            // facilitiesDistance
            // 
            this.facilitiesDistance.FillWeight = 60F;
            this.facilitiesDistance.HeaderText = "Distance";
            this.facilitiesDistance.Name = "facilitiesDistance";
            this.facilitiesDistance.ReadOnly = true;
            this.facilitiesDistance.Width = 60;
            // 
            // facilitiesCity
            // 
            this.facilitiesCity.HeaderText = "City";
            this.facilitiesCity.Name = "facilitiesCity";
            this.facilitiesCity.ReadOnly = true;
            // 
            // facilitiesPrivateRoomPrice
            // 
            this.facilitiesPrivateRoomPrice.HeaderText = "Private room";
            this.facilitiesPrivateRoomPrice.Name = "facilitiesPrivateRoomPrice";
            this.facilitiesPrivateRoomPrice.ReadOnly = true;
            this.facilitiesPrivateRoomPrice.Width = 80;
            // 
            // facilitiesSemiPrivateRoomPrice
            // 
            this.facilitiesSemiPrivateRoomPrice.HeaderText = "Semi-Private room";
            this.facilitiesSemiPrivateRoomPrice.Name = "facilitiesSemiPrivateRoomPrice";
            this.facilitiesSemiPrivateRoomPrice.ReadOnly = true;
            this.facilitiesSemiPrivateRoomPrice.Width = 80;
            // 
            // facilitiesNotified
            // 
            this.facilitiesNotified.HeaderText = "Notified";
            this.facilitiesNotified.Name = "facilitiesNotified";
            this.facilitiesNotified.ReadOnly = true;
            this.facilitiesNotified.Width = 50;
            // 
            // facilitiesResponse
            // 
            this.facilitiesResponse.HeaderText = "Responce";
            this.facilitiesResponse.Name = "facilitiesResponse";
            this.facilitiesResponse.ReadOnly = true;
            this.facilitiesResponse.Width = 70;
            // 
            // facilitiesNotifiedOn
            // 
            this.facilitiesNotifiedOn.HeaderText = "Notified On";
            this.facilitiesNotifiedOn.Name = "facilitiesNotifiedOn";
            this.facilitiesNotifiedOn.ReadOnly = true;
            this.facilitiesNotifiedOn.Width = 85;
            // 
            // facilitiesNotifiedStatus
            // 
            this.facilitiesNotifiedStatus.HeaderText = "Notified Msg";
            this.facilitiesNotifiedStatus.Name = "facilitiesNotifiedStatus";
            this.facilitiesNotifiedStatus.ReadOnly = true;
            this.facilitiesNotifiedStatus.Width = 150;
            // 
            // tabFindAL
            // 
            this.tabFindAL.Controls.Add(this.groupBox19);
            this.tabFindAL.Controls.Add(this.label23);
            this.tabFindAL.Controls.Add(this.groupBox16);
            this.tabFindAL.Controls.Add(this.groupBox13);
            this.tabFindAL.Location = new System.Drawing.Point(4, 22);
            this.tabFindAL.Name = "tabFindAL";
            this.tabFindAL.Padding = new System.Windows.Forms.Padding(3);
            this.tabFindAL.Size = new System.Drawing.Size(1177, 615);
            this.tabFindAL.TabIndex = 7;
            this.tabFindAL.Text = "Find Assisted Living";
            this.tabFindAL.UseVisualStyleBackColor = true;
            // 
            // groupBox19
            // 
            this.groupBox19.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox19.Controls.Add(this.label26);
            this.groupBox19.Controls.Add(this.label27);
            this.groupBox19.Controls.Add(this.btnFindALSelectEmailedOnly);
            this.groupBox19.Controls.Add(this.btnFindAlSelectNotEmailed);
            this.groupBox19.Controls.Add(this.btnFindALDeselectAll);
            this.groupBox19.Controls.Add(this.btnALSendEmails);
            this.groupBox19.Controls.Add(this.gridSuitableAL);
            this.groupBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox19.Location = new System.Drawing.Point(7, 136);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(1164, 483);
            this.groupBox19.TabIndex = 65;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Founded assisted living:";
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(613, 451);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(14, 13);
            this.label26.TabIndex = 67;
            this.label26.Text = "0";
            this.label26.Visible = false;
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(533, 451);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(82, 13);
            this.label27.TabIndex = 66;
            this.label27.Text = "Selected count:";
            this.label27.Visible = false;
            // 
            // btnFindALSelectEmailedOnly
            // 
            this.btnFindALSelectEmailedOnly.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFindALSelectEmailedOnly.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindALSelectEmailedOnly.Location = new System.Drawing.Point(315, 446);
            this.btnFindALSelectEmailedOnly.Name = "btnFindALSelectEmailedOnly";
            this.btnFindALSelectEmailedOnly.Size = new System.Drawing.Size(212, 23);
            this.btnFindALSelectEmailedOnly.TabIndex = 64;
            this.btnFindALSelectEmailedOnly.Text = "Select successfully emailed";
            this.btnFindALSelectEmailedOnly.UseVisualStyleBackColor = true;
            this.btnFindALSelectEmailedOnly.Click += new System.EventHandler(this.btnFindALSelectEmailedOnly_Click);
            // 
            // btnFindAlSelectNotEmailed
            // 
            this.btnFindAlSelectNotEmailed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFindAlSelectNotEmailed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindAlSelectNotEmailed.Location = new System.Drawing.Point(97, 446);
            this.btnFindAlSelectNotEmailed.Name = "btnFindAlSelectNotEmailed";
            this.btnFindAlSelectNotEmailed.Size = new System.Drawing.Size(212, 23);
            this.btnFindAlSelectNotEmailed.TabIndex = 63;
            this.btnFindAlSelectNotEmailed.Text = "Select only not emailed";
            this.btnFindAlSelectNotEmailed.UseVisualStyleBackColor = true;
            this.btnFindAlSelectNotEmailed.Click += new System.EventHandler(this.btnFindAlSelectNotEmailed_Click);
            // 
            // btnFindALDeselectAll
            // 
            this.btnFindALDeselectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFindALDeselectAll.Location = new System.Drawing.Point(16, 446);
            this.btnFindALDeselectAll.Name = "btnFindALDeselectAll";
            this.btnFindALDeselectAll.Size = new System.Drawing.Size(75, 23);
            this.btnFindALDeselectAll.TabIndex = 32;
            this.btnFindALDeselectAll.Text = "Deselect all";
            this.btnFindALDeselectAll.UseVisualStyleBackColor = true;
            this.btnFindALDeselectAll.Click += new System.EventHandler(this.btnFindALDeselectAll_Click);
            // 
            // btnALSendEmails
            // 
            this.btnALSendEmails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnALSendEmails.Location = new System.Drawing.Point(1017, 446);
            this.btnALSendEmails.Name = "btnALSendEmails";
            this.btnALSendEmails.Size = new System.Drawing.Size(133, 23);
            this.btnALSendEmails.TabIndex = 31;
            this.btnALSendEmails.Text = "3. Send Emails";
            this.btnALSendEmails.UseVisualStyleBackColor = true;
            this.btnALSendEmails.Click += new System.EventHandler(this.btnALSendEmails_Click);
            // 
            // gridSuitableAL
            // 
            this.gridSuitableAL.AllowUserToAddRows = false;
            this.gridSuitableAL.AllowUserToDeleteRows = false;
            this.gridSuitableAL.AllowUserToOrderColumns = true;
            this.gridSuitableAL.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSuitableAL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridSuitableAL.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColFindALId,
            this.ColFindSelect,
            this.ColFindALName,
            this.ColFindALALTCS,
            this.ColFindALDistance,
            this.ColFindAlCity,
            this.ColFindALIndependentLivingPrice,
            this.ColFindALAssistedLivingPrice,
            this.ColFindALMemoryCarePrice,
            this.ColFindALSkilledNursingCarePrice,
            this.ColFindALEmailed,
            this.ColFindALEmailedOn,
            this.ColAlEmailStatus});
            this.gridSuitableAL.Location = new System.Drawing.Point(8, 22);
            this.gridSuitableAL.Name = "gridSuitableAL";
            this.gridSuitableAL.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridSuitableAL.Size = new System.Drawing.Size(1142, 413);
            this.gridSuitableAL.TabIndex = 29;
            // 
            // ColFindALId
            // 
            this.ColFindALId.HeaderText = "Id";
            this.ColFindALId.Name = "ColFindALId";
            this.ColFindALId.Visible = false;
            // 
            // ColFindSelect
            // 
            this.ColFindSelect.FillWeight = 40F;
            this.ColFindSelect.HeaderText = "Select";
            this.ColFindSelect.Name = "ColFindSelect";
            this.ColFindSelect.Width = 40;
            // 
            // ColFindALName
            // 
            this.ColFindALName.FillWeight = 185F;
            this.ColFindALName.HeaderText = "Facility Name";
            this.ColFindALName.MinimumWidth = 100;
            this.ColFindALName.Name = "ColFindALName";
            this.ColFindALName.ReadOnly = true;
            this.ColFindALName.Width = 185;
            // 
            // ColFindALALTCS
            // 
            this.ColFindALALTCS.FillWeight = 50F;
            this.ColFindALALTCS.HeaderText = "ALTCS";
            this.ColFindALALTCS.Name = "ColFindALALTCS";
            this.ColFindALALTCS.ReadOnly = true;
            this.ColFindALALTCS.Width = 50;
            // 
            // ColFindALDistance
            // 
            this.ColFindALDistance.FillWeight = 60F;
            this.ColFindALDistance.HeaderText = "Distance";
            this.ColFindALDistance.Name = "ColFindALDistance";
            this.ColFindALDistance.ReadOnly = true;
            this.ColFindALDistance.Width = 60;
            // 
            // ColFindAlCity
            // 
            this.ColFindAlCity.HeaderText = "City";
            this.ColFindAlCity.Name = "ColFindAlCity";
            this.ColFindAlCity.ReadOnly = true;
            // 
            // ColFindALIndependentLivingPrice
            // 
            this.ColFindALIndependentLivingPrice.HeaderText = "Ind Liv $";
            this.ColFindALIndependentLivingPrice.Name = "ColFindALIndependentLivingPrice";
            this.ColFindALIndependentLivingPrice.ReadOnly = true;
            // 
            // ColFindALAssistedLivingPrice
            // 
            this.ColFindALAssistedLivingPrice.HeaderText = "Assisted Liv $";
            this.ColFindALAssistedLivingPrice.Name = "ColFindALAssistedLivingPrice";
            this.ColFindALAssistedLivingPrice.ReadOnly = true;
            // 
            // ColFindALMemoryCarePrice
            // 
            this.ColFindALMemoryCarePrice.HeaderText = "Mem Care $";
            this.ColFindALMemoryCarePrice.Name = "ColFindALMemoryCarePrice";
            this.ColFindALMemoryCarePrice.ReadOnly = true;
            // 
            // ColFindALSkilledNursingCarePrice
            // 
            this.ColFindALSkilledNursingCarePrice.HeaderText = "Skilled Nur $";
            this.ColFindALSkilledNursingCarePrice.Name = "ColFindALSkilledNursingCarePrice";
            this.ColFindALSkilledNursingCarePrice.ReadOnly = true;
            // 
            // ColFindALEmailed
            // 
            this.ColFindALEmailed.HeaderText = "Emailed";
            this.ColFindALEmailed.Name = "ColFindALEmailed";
            this.ColFindALEmailed.ReadOnly = true;
            this.ColFindALEmailed.Width = 50;
            // 
            // ColFindALEmailedOn
            // 
            this.ColFindALEmailedOn.HeaderText = "Emailed on";
            this.ColFindALEmailedOn.Name = "ColFindALEmailedOn";
            this.ColFindALEmailedOn.ReadOnly = true;
            this.ColFindALEmailedOn.Width = 85;
            // 
            // ColAlEmailStatus
            // 
            this.ColAlEmailStatus.HeaderText = "Email Status";
            this.ColAlEmailStatus.Name = "ColAlEmailStatus";
            this.ColAlEmailStatus.ReadOnly = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(379, 6);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(145, 16);
            this.label23.TabIndex = 63;
            this.label23.Text = "2. Care home select";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.label28);
            this.groupBox16.Controls.Add(this.txtALMilesRange);
            this.groupBox16.Controls.Add(this.label25);
            this.groupBox16.Controls.Add(this.btnALSearch);
            this.groupBox16.Controls.Add(this.btnFindAlZipCodesHelp);
            this.groupBox16.Controls.Add(this.label24);
            this.groupBox16.Controls.Add(this.groupBox18);
            this.groupBox16.Controls.Add(this.groupBox17);
            this.groupBox16.Controls.Add(this.groupBox15);
            this.groupBox16.Location = new System.Drawing.Point(370, 7);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(541, 123);
            this.groupBox16.TabIndex = 64;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "                                                                     ";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(500, 66);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(0, 13);
            this.label28.TabIndex = 70;
            // 
            // txtALMilesRange
            // 
            this.txtALMilesRange.Location = new System.Drawing.Point(444, 58);
            this.txtALMilesRange.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.txtALMilesRange.Name = "txtALMilesRange";
            this.txtALMilesRange.Size = new System.Drawing.Size(83, 20);
            this.txtALMilesRange.TabIndex = 69;
            this.txtALMilesRange.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(441, 40);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(86, 13);
            this.label25.TabIndex = 68;
            this.label25.Text = "Radius (in miles):";
            // 
            // btnALSearch
            // 
            this.btnALSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnALSearch.Location = new System.Drawing.Point(444, 86);
            this.btnALSearch.Name = "btnALSearch";
            this.btnALSearch.Size = new System.Drawing.Size(83, 23);
            this.btnALSearch.TabIndex = 67;
            this.btnALSearch.Text = "Search";
            this.btnALSearch.UseVisualStyleBackColor = true;
            this.btnALSearch.Click += new System.EventHandler(this.btnALSearch_Click);
            // 
            // btnFindAlZipCodesHelp
            // 
            this.btnFindAlZipCodesHelp.Location = new System.Drawing.Point(403, 13);
            this.btnFindAlZipCodesHelp.Name = "btnFindAlZipCodesHelp";
            this.btnFindAlZipCodesHelp.Size = new System.Drawing.Size(25, 20);
            this.btnFindAlZipCodesHelp.TabIndex = 66;
            this.btnFindAlZipCodesHelp.Text = "?";
            this.btnFindAlZipCodesHelp.UseVisualStyleBackColor = true;
            this.btnFindAlZipCodesHelp.Click += new System.EventHandler(this.btnFindAlZipCodesHelp_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.Color.Maroon;
            this.label24.Location = new System.Drawing.Point(303, 108);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(0, 13);
            this.label24.TabIndex = 61;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.txtFindALZips);
            this.groupBox18.Location = new System.Drawing.Point(296, 19);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(138, 94);
            this.groupBox18.TabIndex = 60;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "List of zip codes";
            // 
            // txtFindALZips
            // 
            this.txtFindALZips.Location = new System.Drawing.Point(6, 19);
            this.txtFindALZips.Multiline = true;
            this.txtFindALZips.Name = "txtFindALZips";
            this.txtFindALZips.Size = new System.Drawing.Size(126, 63);
            this.txtFindALZips.TabIndex = 65;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.rbFIndAlALTCSYes);
            this.groupBox17.Controls.Add(this.rbFIndAlALTCSNo);
            this.groupBox17.Location = new System.Drawing.Point(218, 19);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(71, 94);
            this.groupBox17.TabIndex = 59;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "ALTCS";
            // 
            // rbFIndAlALTCSYes
            // 
            this.rbFIndAlALTCSYes.AutoSize = true;
            this.rbFIndAlALTCSYes.Location = new System.Drawing.Point(12, 25);
            this.rbFIndAlALTCSYes.Name = "rbFIndAlALTCSYes";
            this.rbFIndAlALTCSYes.Size = new System.Drawing.Size(43, 17);
            this.rbFIndAlALTCSYes.TabIndex = 6;
            this.rbFIndAlALTCSYes.TabStop = true;
            this.rbFIndAlALTCSYes.Text = "Yes";
            this.rbFIndAlALTCSYes.UseVisualStyleBackColor = true;
            // 
            // rbFIndAlALTCSNo
            // 
            this.rbFIndAlALTCSNo.AutoSize = true;
            this.rbFIndAlALTCSNo.Checked = true;
            this.rbFIndAlALTCSNo.Location = new System.Drawing.Point(12, 54);
            this.rbFIndAlALTCSNo.Name = "rbFIndAlALTCSNo";
            this.rbFIndAlALTCSNo.Size = new System.Drawing.Size(39, 17);
            this.rbFIndAlALTCSNo.TabIndex = 7;
            this.rbFIndAlALTCSNo.TabStop = true;
            this.rbFIndAlALTCSNo.Text = "No";
            this.rbFIndAlALTCSNo.UseVisualStyleBackColor = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.checkSkilledNursing);
            this.groupBox15.Controls.Add(this.checkMemCare);
            this.groupBox15.Controls.Add(this.checkAssLiv);
            this.groupBox15.Controls.Add(this.checkIndLiv);
            this.groupBox15.Location = new System.Drawing.Point(12, 19);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(200, 94);
            this.groupBox15.TabIndex = 34;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Assisted living type";
            // 
            // checkSkilledNursing
            // 
            this.checkSkilledNursing.AutoSize = true;
            this.checkSkilledNursing.Location = new System.Drawing.Point(7, 73);
            this.checkSkilledNursing.Name = "checkSkilledNursing";
            this.checkSkilledNursing.Size = new System.Drawing.Size(121, 17);
            this.checkSkilledNursing.TabIndex = 3;
            this.checkSkilledNursing.Text = "Skilled Nursing Care";
            this.checkSkilledNursing.UseVisualStyleBackColor = true;
            // 
            // checkMemCare
            // 
            this.checkMemCare.AutoSize = true;
            this.checkMemCare.Location = new System.Drawing.Point(7, 54);
            this.checkMemCare.Name = "checkMemCare";
            this.checkMemCare.Size = new System.Drawing.Size(87, 17);
            this.checkMemCare.TabIndex = 2;
            this.checkMemCare.Text = "Memory care";
            this.checkMemCare.UseVisualStyleBackColor = true;
            // 
            // checkAssLiv
            // 
            this.checkAssLiv.AutoSize = true;
            this.checkAssLiv.Location = new System.Drawing.Point(7, 35);
            this.checkAssLiv.Name = "checkAssLiv";
            this.checkAssLiv.Size = new System.Drawing.Size(92, 17);
            this.checkAssLiv.TabIndex = 1;
            this.checkAssLiv.Text = "Assisted living";
            this.checkAssLiv.UseVisualStyleBackColor = true;
            // 
            // checkIndLiv
            // 
            this.checkIndLiv.AutoSize = true;
            this.checkIndLiv.Location = new System.Drawing.Point(7, 17);
            this.checkIndLiv.Name = "checkIndLiv";
            this.checkIndLiv.Size = new System.Drawing.Size(117, 17);
            this.checkIndLiv.TabIndex = 0;
            this.checkIndLiv.Text = "Independent Living";
            this.checkIndLiv.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox13.Controls.Add(this.checkBox1);
            this.groupBox13.Controls.Add(this.btnALnewPatient);
            this.groupBox13.Controls.Add(this.cbFindALPatient);
            this.groupBox13.Controls.Add(this.label3);
            this.groupBox13.Controls.Add(this.label2);
            this.groupBox13.Controls.Add(this.groupBox14);
            this.groupBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox13.Location = new System.Drawing.Point(7, 7);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(355, 123);
            this.groupBox13.TabIndex = 32;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "                                                   ";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(16, 27);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(130, 17);
            this.checkBox1.TabIndex = 65;
            this.checkBox1.Text = "Load also CH patients";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // btnALnewPatient
            // 
            this.btnALnewPatient.Location = new System.Drawing.Point(270, 57);
            this.btnALnewPatient.Name = "btnALnewPatient";
            this.btnALnewPatient.Size = new System.Drawing.Size(70, 23);
            this.btnALnewPatient.TabIndex = 64;
            this.btnALnewPatient.Text = "Add new...";
            this.btnALnewPatient.UseVisualStyleBackColor = true;
            this.btnALnewPatient.Click += new System.EventHandler(this.btnALnewPatient_Click);
            // 
            // cbFindALPatient
            // 
            this.cbFindALPatient.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbFindALPatient.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbFindALPatient.DisplayMember = "Text";
            this.cbFindALPatient.FormattingEnabled = true;
            this.cbFindALPatient.Location = new System.Drawing.Point(87, 58);
            this.cbFindALPatient.Name = "cbFindALPatient";
            this.cbFindALPatient.Size = new System.Drawing.Size(177, 21);
            this.cbFindALPatient.TabIndex = 63;
            this.cbFindALPatient.ValueMember = "Value";
            this.cbFindALPatient.SelectedIndexChanged += new System.EventHandler(this.cbFindALPatient_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 62;
            this.label3.Text = "Patient name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(10, -1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 16);
            this.label2.TabIndex = 61;
            this.label2.Text = "1. Select a patient";
            // 
            // groupBox14
            // 
            this.groupBox14.Location = new System.Drawing.Point(2, 293);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(1021, 6);
            this.groupBox14.TabIndex = 35;
            this.groupBox14.TabStop = false;
            // 
            // tabRegisterPatients
            // 
            this.tabRegisterPatients.Controls.Add(this.groupBox11);
            this.tabRegisterPatients.Controls.Add(this.btnDeletePatient);
            this.tabRegisterPatients.Controls.Add(this.btnEditPatient);
            this.tabRegisterPatients.Controls.Add(this.btnAddPatient);
            this.tabRegisterPatients.Controls.Add(this.gridPatients);
            this.tabRegisterPatients.Location = new System.Drawing.Point(4, 22);
            this.tabRegisterPatients.Name = "tabRegisterPatients";
            this.tabRegisterPatients.Padding = new System.Windows.Forms.Padding(3);
            this.tabRegisterPatients.Size = new System.Drawing.Size(1177, 615);
            this.tabRegisterPatients.TabIndex = 5;
            this.tabRegisterPatients.Text = "Patients register";
            this.tabRegisterPatients.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Location = new System.Drawing.Point(6, 6);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(1165, 71);
            this.groupBox11.TabIndex = 57;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Filters?";
            // 
            // btnDeletePatient
            // 
            this.btnDeletePatient.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeletePatient.ForeColor = System.Drawing.Color.Maroon;
            this.btnDeletePatient.Location = new System.Drawing.Point(934, 586);
            this.btnDeletePatient.Name = "btnDeletePatient";
            this.btnDeletePatient.Size = new System.Drawing.Size(75, 23);
            this.btnDeletePatient.TabIndex = 56;
            this.btnDeletePatient.Text = "Delete";
            this.btnDeletePatient.UseVisualStyleBackColor = true;
            this.btnDeletePatient.Click += new System.EventHandler(this.btnDeletePatient_Click);
            // 
            // btnEditPatient
            // 
            this.btnEditPatient.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditPatient.Location = new System.Drawing.Point(1015, 586);
            this.btnEditPatient.Name = "btnEditPatient";
            this.btnEditPatient.Size = new System.Drawing.Size(75, 23);
            this.btnEditPatient.TabIndex = 55;
            this.btnEditPatient.Text = "Edit";
            this.btnEditPatient.UseVisualStyleBackColor = true;
            this.btnEditPatient.Click += new System.EventHandler(this.btnEditPatient_Click);
            // 
            // btnAddPatient
            // 
            this.btnAddPatient.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddPatient.Location = new System.Drawing.Point(1096, 586);
            this.btnAddPatient.Name = "btnAddPatient";
            this.btnAddPatient.Size = new System.Drawing.Size(75, 23);
            this.btnAddPatient.TabIndex = 54;
            this.btnAddPatient.Text = "Add new";
            this.btnAddPatient.UseVisualStyleBackColor = true;
            this.btnAddPatient.Click += new System.EventHandler(this.btnAddPatient_Click);
            // 
            // gridPatients
            // 
            this.gridPatients.AllowUserToAddRows = false;
            this.gridPatients.AllowUserToDeleteRows = false;
            this.gridPatients.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridPatients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPatients.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColPatientId,
            this.ColPatientFirstName,
            this.ColPatientAge,
            this.ColPatientState,
            this.PatientColCity,
            this.ColPatientEmail,
            this.ColPatientPhone,
            this.ColPatientZip,
            this.ColPatientALTCS,
            this.ColPatientType,
            this.ColPatientNotes});
            this.gridPatients.Location = new System.Drawing.Point(6, 83);
            this.gridPatients.MultiSelect = false;
            this.gridPatients.Name = "gridPatients";
            this.gridPatients.ReadOnly = true;
            this.gridPatients.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridPatients.Size = new System.Drawing.Size(1165, 497);
            this.gridPatients.TabIndex = 53;
            // 
            // ColPatientId
            // 
            this.ColPatientId.HeaderText = "Id";
            this.ColPatientId.Name = "ColPatientId";
            this.ColPatientId.ReadOnly = true;
            this.ColPatientId.Visible = false;
            // 
            // ColPatientFirstName
            // 
            this.ColPatientFirstName.HeaderText = "Name";
            this.ColPatientFirstName.Name = "ColPatientFirstName";
            this.ColPatientFirstName.ReadOnly = true;
            this.ColPatientFirstName.Width = 120;
            // 
            // ColPatientAge
            // 
            this.ColPatientAge.HeaderText = "Age";
            this.ColPatientAge.Name = "ColPatientAge";
            this.ColPatientAge.ReadOnly = true;
            this.ColPatientAge.Width = 50;
            // 
            // ColPatientState
            // 
            this.ColPatientState.HeaderText = "State";
            this.ColPatientState.Name = "ColPatientState";
            this.ColPatientState.ReadOnly = true;
            this.ColPatientState.Width = 50;
            // 
            // PatientColCity
            // 
            this.PatientColCity.HeaderText = "City";
            this.PatientColCity.Name = "PatientColCity";
            this.PatientColCity.ReadOnly = true;
            this.PatientColCity.Width = 110;
            // 
            // ColPatientEmail
            // 
            this.ColPatientEmail.HeaderText = "Email";
            this.ColPatientEmail.Name = "ColPatientEmail";
            this.ColPatientEmail.ReadOnly = true;
            this.ColPatientEmail.Width = 140;
            // 
            // ColPatientPhone
            // 
            this.ColPatientPhone.HeaderText = "Phone";
            this.ColPatientPhone.Name = "ColPatientPhone";
            this.ColPatientPhone.ReadOnly = true;
            this.ColPatientPhone.Width = 90;
            // 
            // ColPatientZip
            // 
            this.ColPatientZip.HeaderText = "ZIP";
            this.ColPatientZip.Name = "ColPatientZip";
            this.ColPatientZip.ReadOnly = true;
            this.ColPatientZip.Width = 50;
            // 
            // ColPatientALTCS
            // 
            this.ColPatientALTCS.HeaderText = "ALTCS";
            this.ColPatientALTCS.Name = "ColPatientALTCS";
            this.ColPatientALTCS.ReadOnly = true;
            this.ColPatientALTCS.Width = 40;
            // 
            // ColPatientType
            // 
            this.ColPatientType.HeaderText = "Type";
            this.ColPatientType.Name = "ColPatientType";
            this.ColPatientType.ReadOnly = true;
            this.ColPatientType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColPatientType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColPatientType.Width = 50;
            // 
            // ColPatientNotes
            // 
            this.ColPatientNotes.HeaderText = "Notes";
            this.ColPatientNotes.Name = "ColPatientNotes";
            this.ColPatientNotes.ReadOnly = true;
            this.ColPatientNotes.Width = 250;
            // 
            // tabRegisterAL
            // 
            this.tabRegisterAL.Controls.Add(this.btnALDelete);
            this.tabRegisterAL.Controls.Add(this.btnALEdit);
            this.tabRegisterAL.Controls.Add(this.btnALAdd);
            this.tabRegisterAL.Controls.Add(this.groupBox12);
            this.tabRegisterAL.Controls.Add(this.gridAssistedLiving);
            this.tabRegisterAL.Location = new System.Drawing.Point(4, 22);
            this.tabRegisterAL.Name = "tabRegisterAL";
            this.tabRegisterAL.Padding = new System.Windows.Forms.Padding(3);
            this.tabRegisterAL.Size = new System.Drawing.Size(1177, 615);
            this.tabRegisterAL.TabIndex = 6;
            this.tabRegisterAL.Text = "Assisted living Register";
            this.tabRegisterAL.UseVisualStyleBackColor = true;
            // 
            // btnALDelete
            // 
            this.btnALDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnALDelete.ForeColor = System.Drawing.Color.Maroon;
            this.btnALDelete.Location = new System.Drawing.Point(934, 586);
            this.btnALDelete.Name = "btnALDelete";
            this.btnALDelete.Size = new System.Drawing.Size(75, 23);
            this.btnALDelete.TabIndex = 7;
            this.btnALDelete.Text = "Delete";
            this.btnALDelete.UseVisualStyleBackColor = true;
            this.btnALDelete.Click += new System.EventHandler(this.btnALDelete_Click);
            // 
            // btnALEdit
            // 
            this.btnALEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnALEdit.Location = new System.Drawing.Point(1015, 586);
            this.btnALEdit.Name = "btnALEdit";
            this.btnALEdit.Size = new System.Drawing.Size(75, 23);
            this.btnALEdit.TabIndex = 6;
            this.btnALEdit.Text = "Edit";
            this.btnALEdit.UseVisualStyleBackColor = true;
            this.btnALEdit.Click += new System.EventHandler(this.btnALEdit_Click);
            // 
            // btnALAdd
            // 
            this.btnALAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnALAdd.Location = new System.Drawing.Point(1096, 586);
            this.btnALAdd.Name = "btnALAdd";
            this.btnALAdd.Size = new System.Drawing.Size(75, 23);
            this.btnALAdd.TabIndex = 5;
            this.btnALAdd.Text = "Add new";
            this.btnALAdd.UseVisualStyleBackColor = true;
            this.btnALAdd.Click += new System.EventHandler(this.btnALAdd_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.Location = new System.Drawing.Point(6, 7);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(1165, 49);
            this.groupBox12.TabIndex = 1;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Filters?";
            // 
            // gridAssistedLiving
            // 
            this.gridAssistedLiving.AllowUserToAddRows = false;
            this.gridAssistedLiving.AllowUserToDeleteRows = false;
            this.gridAssistedLiving.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridAssistedLiving.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridAssistedLiving.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColAlId,
            this.CoLAlName,
            this.ColAlWebSite,
            this.ColAlPhone,
            this.ColAlZip,
            this.ColAlIndependentLivingPrice,
            this.ColAlAssistedLivingPrice,
            this.ColAlMemoryCarePrice,
            this.ColAlSkilledNursingCarePrice,
            this.ColAlEmail1,
            this.ColAlEmail2,
            this.ColAlEmail3,
            this.ColAlNotes});
            this.gridAssistedLiving.Location = new System.Drawing.Point(6, 62);
            this.gridAssistedLiving.MultiSelect = false;
            this.gridAssistedLiving.Name = "gridAssistedLiving";
            this.gridAssistedLiving.ReadOnly = true;
            this.gridAssistedLiving.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridAssistedLiving.Size = new System.Drawing.Size(1165, 518);
            this.gridAssistedLiving.TabIndex = 0;
            // 
            // ColAlId
            // 
            this.ColAlId.HeaderText = "Id";
            this.ColAlId.Name = "ColAlId";
            this.ColAlId.ReadOnly = true;
            this.ColAlId.Visible = false;
            // 
            // CoLAlName
            // 
            this.CoLAlName.HeaderText = "Name";
            this.CoLAlName.Name = "CoLAlName";
            this.CoLAlName.ReadOnly = true;
            // 
            // ColAlWebSite
            // 
            this.ColAlWebSite.HeaderText = "WebSite";
            this.ColAlWebSite.Name = "ColAlWebSite";
            this.ColAlWebSite.ReadOnly = true;
            this.ColAlWebSite.Width = 60;
            // 
            // ColAlPhone
            // 
            this.ColAlPhone.HeaderText = "Phone";
            this.ColAlPhone.Name = "ColAlPhone";
            this.ColAlPhone.ReadOnly = true;
            this.ColAlPhone.Width = 60;
            // 
            // ColAlZip
            // 
            this.ColAlZip.HeaderText = "Zip";
            this.ColAlZip.Name = "ColAlZip";
            this.ColAlZip.ReadOnly = true;
            this.ColAlZip.Width = 45;
            // 
            // ColAlIndependentLivingPrice
            // 
            this.ColAlIndependentLivingPrice.HeaderText = "Ind. Living $";
            this.ColAlIndependentLivingPrice.Name = "ColAlIndependentLivingPrice";
            this.ColAlIndependentLivingPrice.ReadOnly = true;
            this.ColAlIndependentLivingPrice.Width = 75;
            // 
            // ColAlAssistedLivingPrice
            // 
            this.ColAlAssistedLivingPrice.HeaderText = "Assis Living $";
            this.ColAlAssistedLivingPrice.Name = "ColAlAssistedLivingPrice";
            this.ColAlAssistedLivingPrice.ReadOnly = true;
            this.ColAlAssistedLivingPrice.Width = 75;
            // 
            // ColAlMemoryCarePrice
            // 
            this.ColAlMemoryCarePrice.HeaderText = "Mem. Care $";
            this.ColAlMemoryCarePrice.Name = "ColAlMemoryCarePrice";
            this.ColAlMemoryCarePrice.ReadOnly = true;
            this.ColAlMemoryCarePrice.Width = 75;
            // 
            // ColAlSkilledNursingCarePrice
            // 
            this.ColAlSkilledNursingCarePrice.HeaderText = "Skilled Nursing $";
            this.ColAlSkilledNursingCarePrice.Name = "ColAlSkilledNursingCarePrice";
            this.ColAlSkilledNursingCarePrice.ReadOnly = true;
            this.ColAlSkilledNursingCarePrice.Width = 75;
            // 
            // ColAlEmail1
            // 
            this.ColAlEmail1.HeaderText = "Email 1";
            this.ColAlEmail1.Name = "ColAlEmail1";
            this.ColAlEmail1.ReadOnly = true;
            this.ColAlEmail1.Width = 75;
            // 
            // ColAlEmail2
            // 
            this.ColAlEmail2.HeaderText = "Email 2";
            this.ColAlEmail2.Name = "ColAlEmail2";
            this.ColAlEmail2.ReadOnly = true;
            this.ColAlEmail2.Width = 75;
            // 
            // ColAlEmail3
            // 
            this.ColAlEmail3.HeaderText = "Email 3";
            this.ColAlEmail3.Name = "ColAlEmail3";
            this.ColAlEmail3.ReadOnly = true;
            this.ColAlEmail3.Width = 75;
            // 
            // ColAlNotes
            // 
            this.ColAlNotes.HeaderText = "Notes";
            this.ColAlNotes.Name = "ColAlNotes";
            this.ColAlNotes.ReadOnly = true;
            this.ColAlNotes.Width = 250;
            // 
            // tabRegisterCH
            // 
            this.tabRegisterCH.Controls.Add(this.btnFacilitiesRefresh);
            this.tabRegisterCH.Controls.Add(this.btnDelete);
            this.tabRegisterCH.Controls.Add(this.btnEditFacility);
            this.tabRegisterCH.Controls.Add(this.btnAddNewFacility);
            this.tabRegisterCH.Controls.Add(this.groupBox7);
            this.tabRegisterCH.Controls.Add(this.gridFacilities);
            this.tabRegisterCH.Location = new System.Drawing.Point(4, 22);
            this.tabRegisterCH.Name = "tabRegisterCH";
            this.tabRegisterCH.Padding = new System.Windows.Forms.Padding(3);
            this.tabRegisterCH.Size = new System.Drawing.Size(1177, 615);
            this.tabRegisterCH.TabIndex = 1;
            this.tabRegisterCH.Text = "Care homes register";
            this.tabRegisterCH.UseVisualStyleBackColor = true;
            // 
            // btnFacilitiesRefresh
            // 
            this.btnFacilitiesRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFacilitiesRefresh.Location = new System.Drawing.Point(6, 581);
            this.btnFacilitiesRefresh.Name = "btnFacilitiesRefresh";
            this.btnFacilitiesRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnFacilitiesRefresh.TabIndex = 5;
            this.btnFacilitiesRefresh.Text = "Refresh";
            this.btnFacilitiesRefresh.UseVisualStyleBackColor = true;
            this.btnFacilitiesRefresh.Click += new System.EventHandler(this.btnFacilitiesRefresh_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.ForeColor = System.Drawing.Color.Maroon;
            this.btnDelete.Location = new System.Drawing.Point(934, 581);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEditFacility
            // 
            this.btnEditFacility.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditFacility.Location = new System.Drawing.Point(1015, 581);
            this.btnEditFacility.Name = "btnEditFacility";
            this.btnEditFacility.Size = new System.Drawing.Size(75, 23);
            this.btnEditFacility.TabIndex = 3;
            this.btnEditFacility.Text = "Edit";
            this.btnEditFacility.UseVisualStyleBackColor = true;
            this.btnEditFacility.Click += new System.EventHandler(this.btnEditFacility_Click);
            // 
            // btnAddNewFacility
            // 
            this.btnAddNewFacility.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNewFacility.Location = new System.Drawing.Point(1096, 581);
            this.btnAddNewFacility.Name = "btnAddNewFacility";
            this.btnAddNewFacility.Size = new System.Drawing.Size(75, 23);
            this.btnAddNewFacility.TabIndex = 2;
            this.btnAddNewFacility.Text = "Add new";
            this.btnAddNewFacility.UseVisualStyleBackColor = true;
            this.btnAddNewFacility.Click += new System.EventHandler(this.btnAddNewFacility_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.label13);
            this.groupBox7.Controls.Add(this.btnSearchFacitilies);
            this.groupBox7.Location = new System.Drawing.Point(5, 7);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(1166, 73);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Filter results";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(236, 32);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(178, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "What filter settings you need, if any?";
            // 
            // btnSearchFacitilies
            // 
            this.btnSearchFacitilies.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearchFacitilies.Location = new System.Drawing.Point(1088, 44);
            this.btnSearchFacitilies.Name = "btnSearchFacitilies";
            this.btnSearchFacitilies.Size = new System.Drawing.Size(72, 23);
            this.btnSearchFacitilies.TabIndex = 0;
            this.btnSearchFacitilies.Text = "Filter";
            this.btnSearchFacitilies.UseVisualStyleBackColor = true;
            // 
            // gridFacilities
            // 
            this.gridFacilities.AllowUserToAddRows = false;
            this.gridFacilities.AllowUserToDeleteRows = false;
            this.gridFacilities.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridFacilities.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridFacilities.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.FacilityName,
            this.FacilityType,
            this.WebSite,
            this.Emails,
            this.Phones,
            this.Faxes,
            this.State,
            this.City,
            this.Zip,
            this.AddressLine,
            this.ContactPersons,
            this.ALTSC,
            this.Updated,
            this.Notes});
            this.gridFacilities.Location = new System.Drawing.Point(6, 86);
            this.gridFacilities.Name = "gridFacilities";
            this.gridFacilities.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridFacilities.Size = new System.Drawing.Size(1165, 485);
            this.gridFacilities.TabIndex = 0;
            this.gridFacilities.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dbGrid_CellContentDoubleClick);
            this.gridFacilities.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dbGrid_KeyDown);
            this.gridFacilities.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dbGrid_KeyPress);
            this.gridFacilities.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dbGrid_KeyUp);
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            // 
            // FacilityName
            // 
            this.FacilityName.HeaderText = "Facility Name";
            this.FacilityName.Name = "FacilityName";
            this.FacilityName.ReadOnly = true;
            // 
            // FacilityType
            // 
            this.FacilityType.HeaderText = "Facility Type";
            this.FacilityType.Name = "FacilityType";
            this.FacilityType.ReadOnly = true;
            this.FacilityType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // WebSite
            // 
            this.WebSite.HeaderText = "WebSite";
            this.WebSite.Name = "WebSite";
            this.WebSite.ReadOnly = true;
            // 
            // Emails
            // 
            this.Emails.HeaderText = "Emails";
            this.Emails.Name = "Emails";
            this.Emails.ReadOnly = true;
            // 
            // Phones
            // 
            this.Phones.HeaderText = "Phones";
            this.Phones.Name = "Phones";
            this.Phones.ReadOnly = true;
            // 
            // Faxes
            // 
            this.Faxes.HeaderText = "Faxes";
            this.Faxes.Name = "Faxes";
            this.Faxes.ReadOnly = true;
            // 
            // State
            // 
            this.State.HeaderText = "State";
            this.State.Name = "State";
            this.State.ReadOnly = true;
            this.State.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // City
            // 
            this.City.HeaderText = "City";
            this.City.Name = "City";
            this.City.ReadOnly = true;
            this.City.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Zip
            // 
            this.Zip.HeaderText = "Zip";
            this.Zip.Name = "Zip";
            this.Zip.ReadOnly = true;
            this.Zip.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // AddressLine
            // 
            this.AddressLine.HeaderText = "Address Line";
            this.AddressLine.Name = "AddressLine";
            this.AddressLine.ReadOnly = true;
            // 
            // ContactPersons
            // 
            this.ContactPersons.HeaderText = "Contact Persons";
            this.ContactPersons.Name = "ContactPersons";
            this.ContactPersons.ReadOnly = true;
            // 
            // ALTSC
            // 
            this.ALTSC.HeaderText = "ALTSC";
            this.ALTSC.Name = "ALTSC";
            this.ALTSC.ReadOnly = true;
            this.ALTSC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Updated
            // 
            this.Updated.HeaderText = "Updated";
            this.Updated.Name = "Updated";
            this.Updated.ReadOnly = true;
            // 
            // Notes
            // 
            this.Notes.HeaderText = "Notes";
            this.Notes.Name = "Notes";
            this.Notes.ReadOnly = true;
            // 
            // tabAdmin
            // 
            this.tabAdmin.Controls.Add(this.btnRefreshAgents);
            this.tabAdmin.Controls.Add(this.btnDeleteAgents);
            this.tabAdmin.Controls.Add(this.btnEditAgent2);
            this.tabAdmin.Controls.Add(this.btnAddNewAgent);
            this.tabAdmin.Controls.Add(this.label14);
            this.tabAdmin.Controls.Add(this.gridAgents);
            this.tabAdmin.Location = new System.Drawing.Point(4, 22);
            this.tabAdmin.Name = "tabAdmin";
            this.tabAdmin.Padding = new System.Windows.Forms.Padding(3);
            this.tabAdmin.Size = new System.Drawing.Size(1177, 615);
            this.tabAdmin.TabIndex = 2;
            this.tabAdmin.Tag = "tabAdmin";
            this.tabAdmin.Text = "Agents register (Admin only)";
            this.tabAdmin.UseVisualStyleBackColor = true;
            // 
            // btnRefreshAgents
            // 
            this.btnRefreshAgents.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRefreshAgents.Location = new System.Drawing.Point(18, 579);
            this.btnRefreshAgents.Name = "btnRefreshAgents";
            this.btnRefreshAgents.Size = new System.Drawing.Size(75, 23);
            this.btnRefreshAgents.TabIndex = 5;
            this.btnRefreshAgents.Text = "Refresh";
            this.btnRefreshAgents.UseVisualStyleBackColor = true;
            this.btnRefreshAgents.Click += new System.EventHandler(this.btnRefreshAgents_Click);
            // 
            // btnDeleteAgents
            // 
            this.btnDeleteAgents.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteAgents.ForeColor = System.Drawing.Color.Maroon;
            this.btnDeleteAgents.Location = new System.Drawing.Point(864, 579);
            this.btnDeleteAgents.Name = "btnDeleteAgents";
            this.btnDeleteAgents.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteAgents.TabIndex = 4;
            this.btnDeleteAgents.Text = "Delete";
            this.btnDeleteAgents.UseVisualStyleBackColor = true;
            this.btnDeleteAgents.Click += new System.EventHandler(this.btnDeleteAgents_Click);
            // 
            // btnEditAgent2
            // 
            this.btnEditAgent2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditAgent2.Location = new System.Drawing.Point(945, 579);
            this.btnEditAgent2.Name = "btnEditAgent2";
            this.btnEditAgent2.Size = new System.Drawing.Size(75, 23);
            this.btnEditAgent2.TabIndex = 3;
            this.btnEditAgent2.Text = "Edit";
            this.btnEditAgent2.UseVisualStyleBackColor = true;
            this.btnEditAgent2.Click += new System.EventHandler(this.btnEditAgent2_Click);
            // 
            // btnAddNewAgent
            // 
            this.btnAddNewAgent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNewAgent.Location = new System.Drawing.Point(1026, 579);
            this.btnAddNewAgent.Name = "btnAddNewAgent";
            this.btnAddNewAgent.Size = new System.Drawing.Size(75, 23);
            this.btnAddNewAgent.TabIndex = 2;
            this.btnAddNewAgent.Text = "Add New";
            this.btnAddNewAgent.UseVisualStyleBackColor = true;
            this.btnAddNewAgent.Click += new System.EventHandler(this.btnAddNewAgent_Click);
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(423, 24);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "All agents:";
            // 
            // gridAgents
            // 
            this.gridAgents.AllowUserToAddRows = false;
            this.gridAgents.AllowUserToDeleteRows = false;
            this.gridAgents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridAgents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridAgents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AccountsColumnId,
            this.AccountsColumnUserName,
            this.AccountsColumnAgentName,
            this.AccountsColumnEnabled,
            this.AccountsColumnIsAdmin,
            this.AccountsColumnLastLogin,
            this.AccountsColumnCreated});
            this.gridAgents.Location = new System.Drawing.Point(18, 40);
            this.gridAgents.MultiSelect = false;
            this.gridAgents.Name = "gridAgents";
            this.gridAgents.ReadOnly = true;
            this.gridAgents.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridAgents.Size = new System.Drawing.Size(1083, 527);
            this.gridAgents.TabIndex = 0;
            // 
            // AccountsColumnId
            // 
            this.AccountsColumnId.HeaderText = "Id";
            this.AccountsColumnId.Name = "AccountsColumnId";
            this.AccountsColumnId.ReadOnly = true;
            this.AccountsColumnId.Visible = false;
            // 
            // AccountsColumnUserName
            // 
            this.AccountsColumnUserName.HeaderText = "Gmail account";
            this.AccountsColumnUserName.Name = "AccountsColumnUserName";
            this.AccountsColumnUserName.ReadOnly = true;
            this.AccountsColumnUserName.Width = 200;
            // 
            // AccountsColumnAgentName
            // 
            this.AccountsColumnAgentName.HeaderText = "Agent Name";
            this.AccountsColumnAgentName.Name = "AccountsColumnAgentName";
            this.AccountsColumnAgentName.ReadOnly = true;
            this.AccountsColumnAgentName.Width = 200;
            // 
            // AccountsColumnEnabled
            // 
            this.AccountsColumnEnabled.HeaderText = "Agent Enabled";
            this.AccountsColumnEnabled.Name = "AccountsColumnEnabled";
            this.AccountsColumnEnabled.ReadOnly = true;
            this.AccountsColumnEnabled.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AccountsColumnEnabled.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // AccountsColumnIsAdmin
            // 
            this.AccountsColumnIsAdmin.HeaderText = "Is Admin";
            this.AccountsColumnIsAdmin.Name = "AccountsColumnIsAdmin";
            this.AccountsColumnIsAdmin.ReadOnly = true;
            this.AccountsColumnIsAdmin.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AccountsColumnIsAdmin.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // AccountsColumnLastLogin
            // 
            this.AccountsColumnLastLogin.HeaderText = "Last Login";
            this.AccountsColumnLastLogin.Name = "AccountsColumnLastLogin";
            this.AccountsColumnLastLogin.ReadOnly = true;
            this.AccountsColumnLastLogin.Width = 140;
            // 
            // AccountsColumnCreated
            // 
            this.AccountsColumnCreated.HeaderText = "Created";
            this.AccountsColumnCreated.Name = "AccountsColumnCreated";
            this.AccountsColumnCreated.ReadOnly = true;
            // 
            // tabReports
            // 
            this.tabReports.Controls.Add(this.groupReportsQuestionCampaignFilters);
            this.tabReports.Controls.Add(this.gbReportsEmailsGroup);
            this.tabReports.Controls.Add(this.button3);
            this.tabReports.Controls.Add(this.btnExportSelectedToSpaceFile);
            this.tabReports.Controls.Add(this.gridReports);
            this.tabReports.Controls.Add(this.btnLoadReport);
            this.tabReports.Controls.Add(this.label4);
            this.tabReports.Controls.Add(this.cbReportTypes);
            this.tabReports.Controls.Add(this.groupSentInvitationsFilters);
            this.tabReports.Controls.Add(this.groupActivityFilters);
            this.tabReports.Location = new System.Drawing.Point(4, 22);
            this.tabReports.Name = "tabReports";
            this.tabReports.Padding = new System.Windows.Forms.Padding(3);
            this.tabReports.Size = new System.Drawing.Size(1177, 615);
            this.tabReports.TabIndex = 3;
            this.tabReports.Text = "Reports";
            this.tabReports.UseVisualStyleBackColor = true;
            // 
            // groupReportsQuestionCampaignFilters
            // 
            this.groupReportsQuestionCampaignFilters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupReportsQuestionCampaignFilters.Controls.Add(this.label34);
            this.groupReportsQuestionCampaignFilters.Controls.Add(this.comboReportsQuestionCampaigns);
            this.groupReportsQuestionCampaignFilters.Location = new System.Drawing.Point(412, 7);
            this.groupReportsQuestionCampaignFilters.Name = "groupReportsQuestionCampaignFilters";
            this.groupReportsQuestionCampaignFilters.Size = new System.Drawing.Size(746, 81);
            this.groupReportsQuestionCampaignFilters.TabIndex = 15;
            this.groupReportsQuestionCampaignFilters.TabStop = false;
            this.groupReportsQuestionCampaignFilters.Text = "Question Campaign Answers";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(13, 20);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(102, 13);
            this.label34.TabIndex = 1;
            this.label34.Text = "Question Campaign:";
            // 
            // comboReportsQuestionCampaigns
            // 
            this.comboReportsQuestionCampaigns.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboReportsQuestionCampaigns.FormattingEnabled = true;
            this.comboReportsQuestionCampaigns.Location = new System.Drawing.Point(16, 33);
            this.comboReportsQuestionCampaigns.Name = "comboReportsQuestionCampaigns";
            this.comboReportsQuestionCampaigns.Size = new System.Drawing.Size(229, 21);
            this.comboReportsQuestionCampaigns.TabIndex = 0;
            // 
            // gbReportsEmailsGroup
            // 
            this.gbReportsEmailsGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbReportsEmailsGroup.Controls.Add(this.cbReportsSentEmailsAL);
            this.gbReportsEmailsGroup.Controls.Add(this.cbReportsSentEmailsPatients);
            this.gbReportsEmailsGroup.Controls.Add(this.label29);
            this.gbReportsEmailsGroup.Controls.Add(this.label30);
            this.gbReportsEmailsGroup.Controls.Add(this.chkReportsSentEmailsShowErrorsOnly);
            this.gbReportsEmailsGroup.Location = new System.Drawing.Point(412, 7);
            this.gbReportsEmailsGroup.Name = "gbReportsEmailsGroup";
            this.gbReportsEmailsGroup.Size = new System.Drawing.Size(748, 81);
            this.gbReportsEmailsGroup.TabIndex = 14;
            this.gbReportsEmailsGroup.TabStop = false;
            this.gbReportsEmailsGroup.Text = "Sent Emails Report Filters";
            this.gbReportsEmailsGroup.Visible = false;
            // 
            // cbReportsSentEmailsAL
            // 
            this.cbReportsSentEmailsAL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbReportsSentEmailsAL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbReportsSentEmailsAL.FormattingEnabled = true;
            this.cbReportsSentEmailsAL.Location = new System.Drawing.Point(287, 43);
            this.cbReportsSentEmailsAL.Name = "cbReportsSentEmailsAL";
            this.cbReportsSentEmailsAL.Size = new System.Drawing.Size(158, 21);
            this.cbReportsSentEmailsAL.TabIndex = 12;
            // 
            // cbReportsSentEmailsPatients
            // 
            this.cbReportsSentEmailsPatients.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbReportsSentEmailsPatients.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbReportsSentEmailsPatients.FormattingEnabled = true;
            this.cbReportsSentEmailsPatients.Location = new System.Drawing.Point(287, 17);
            this.cbReportsSentEmailsPatients.Name = "cbReportsSentEmailsPatients";
            this.cbReportsSentEmailsPatients.Size = new System.Drawing.Size(158, 21);
            this.cbReportsSentEmailsPatients.TabIndex = 11;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(194, 46);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(87, 13);
            this.label29.TabIndex = 10;
            this.label29.Text = "Care home name";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(212, 19);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(69, 13);
            this.label30.TabIndex = 8;
            this.label30.Text = "Patient name";
            // 
            // chkReportsSentEmailsShowErrorsOnly
            // 
            this.chkReportsSentEmailsShowErrorsOnly.AutoSize = true;
            this.chkReportsSentEmailsShowErrorsOnly.Location = new System.Drawing.Point(13, 19);
            this.chkReportsSentEmailsShowErrorsOnly.Name = "chkReportsSentEmailsShowErrorsOnly";
            this.chkReportsSentEmailsShowErrorsOnly.Size = new System.Drawing.Size(104, 17);
            this.chkReportsSentEmailsShowErrorsOnly.TabIndex = 2;
            this.chkReportsSentEmailsShowErrorsOnly.Text = "Show errors only";
            this.chkReportsSentEmailsShowErrorsOnly.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(109, 58);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(105, 23);
            this.button3.TabIndex = 6;
            this.button3.Text = "To csv file";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // btnExportSelectedToSpaceFile
            // 
            this.btnExportSelectedToSpaceFile.Location = new System.Drawing.Point(220, 58);
            this.btnExportSelectedToSpaceFile.Name = "btnExportSelectedToSpaceFile";
            this.btnExportSelectedToSpaceFile.Size = new System.Drawing.Size(105, 23);
            this.btnExportSelectedToSpaceFile.TabIndex = 6;
            this.btnExportSelectedToSpaceFile.Text = "To space del file";
            this.btnExportSelectedToSpaceFile.UseVisualStyleBackColor = true;
            this.btnExportSelectedToSpaceFile.Click += new System.EventHandler(this.btnExportSelectedToExcel_Click);
            // 
            // gridReports
            // 
            this.gridReports.AllowUserToAddRows = false;
            this.gridReports.AllowUserToDeleteRows = false;
            this.gridReports.AllowUserToOrderColumns = true;
            this.gridReports.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridReports.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridReports.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridReports.Location = new System.Drawing.Point(18, 94);
            this.gridReports.Name = "gridReports";
            this.gridReports.ReadOnly = true;
            this.gridReports.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.gridReports.Size = new System.Drawing.Size(1142, 498);
            this.gridReports.TabIndex = 3;
            // 
            // btnLoadReport
            // 
            this.btnLoadReport.Location = new System.Drawing.Point(331, 58);
            this.btnLoadReport.Name = "btnLoadReport";
            this.btnLoadReport.Size = new System.Drawing.Size(75, 23);
            this.btnLoadReport.TabIndex = 2;
            this.btnLoadReport.Text = "Show";
            this.btnLoadReport.UseVisualStyleBackColor = true;
            this.btnLoadReport.Click += new System.EventHandler(this.btnLoadReport_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Report Type:";
            // 
            // cbReportTypes
            // 
            this.cbReportTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbReportTypes.FormattingEnabled = true;
            this.cbReportTypes.Items.AddRange(new object[] {
            "Sent invitations",
            "Sent emails",
            "Sent emails for Patients Report",
            "Activity actions",
            "Question Campaign Answers"});
            this.cbReportTypes.Location = new System.Drawing.Point(19, 31);
            this.cbReportTypes.Name = "cbReportTypes";
            this.cbReportTypes.Size = new System.Drawing.Size(387, 21);
            this.cbReportTypes.TabIndex = 0;
            this.cbReportTypes.SelectedIndexChanged += new System.EventHandler(this.cbReportTypes_SelectedIndexChanged);
            // 
            // groupSentInvitationsFilters
            // 
            this.groupSentInvitationsFilters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupSentInvitationsFilters.Controls.Add(this.cbReportsInvitationsCareHomes);
            this.groupSentInvitationsFilters.Controls.Add(this.cbReportsInvitationsPatients);
            this.groupSentInvitationsFilters.Controls.Add(this.label8);
            this.groupSentInvitationsFilters.Controls.Add(this.label7);
            this.groupSentInvitationsFilters.Controls.Add(this.reportSentInvitationShowErrorsOnly);
            this.groupSentInvitationsFilters.Controls.Add(this.sentInvitationOnlyInterested);
            this.groupSentInvitationsFilters.Location = new System.Drawing.Point(412, 7);
            this.groupSentInvitationsFilters.Name = "groupSentInvitationsFilters";
            this.groupSentInvitationsFilters.Size = new System.Drawing.Size(748, 81);
            this.groupSentInvitationsFilters.TabIndex = 5;
            this.groupSentInvitationsFilters.TabStop = false;
            this.groupSentInvitationsFilters.Text = "Sent Invitations Report Filters";
            // 
            // cbReportsInvitationsCareHomes
            // 
            this.cbReportsInvitationsCareHomes.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbReportsInvitationsCareHomes.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbReportsInvitationsCareHomes.FormattingEnabled = true;
            this.cbReportsInvitationsCareHomes.Location = new System.Drawing.Point(287, 43);
            this.cbReportsInvitationsCareHomes.Name = "cbReportsInvitationsCareHomes";
            this.cbReportsInvitationsCareHomes.Size = new System.Drawing.Size(158, 21);
            this.cbReportsInvitationsCareHomes.TabIndex = 12;
            // 
            // cbReportsInvitationsPatients
            // 
            this.cbReportsInvitationsPatients.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbReportsInvitationsPatients.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbReportsInvitationsPatients.FormattingEnabled = true;
            this.cbReportsInvitationsPatients.Location = new System.Drawing.Point(287, 17);
            this.cbReportsInvitationsPatients.Name = "cbReportsInvitationsPatients";
            this.cbReportsInvitationsPatients.Size = new System.Drawing.Size(158, 21);
            this.cbReportsInvitationsPatients.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(194, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Care home name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(212, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Patient name";
            // 
            // reportSentInvitationShowErrorsOnly
            // 
            this.reportSentInvitationShowErrorsOnly.AutoSize = true;
            this.reportSentInvitationShowErrorsOnly.Location = new System.Drawing.Point(13, 44);
            this.reportSentInvitationShowErrorsOnly.Name = "reportSentInvitationShowErrorsOnly";
            this.reportSentInvitationShowErrorsOnly.Size = new System.Drawing.Size(104, 17);
            this.reportSentInvitationShowErrorsOnly.TabIndex = 2;
            this.reportSentInvitationShowErrorsOnly.Text = "Show errors only";
            this.reportSentInvitationShowErrorsOnly.UseVisualStyleBackColor = true;
            // 
            // sentInvitationOnlyInterested
            // 
            this.sentInvitationOnlyInterested.AutoSize = true;
            this.sentInvitationOnlyInterested.Checked = true;
            this.sentInvitationOnlyInterested.CheckState = System.Windows.Forms.CheckState.Checked;
            this.sentInvitationOnlyInterested.Location = new System.Drawing.Point(13, 18);
            this.sentInvitationOnlyInterested.Name = "sentInvitationOnlyInterested";
            this.sentInvitationOnlyInterested.Size = new System.Drawing.Size(176, 17);
            this.sentInvitationOnlyInterested.TabIndex = 0;
            this.sentInvitationOnlyInterested.Text = "Show only intrested care homes";
            this.sentInvitationOnlyInterested.UseVisualStyleBackColor = true;
            // 
            // groupActivityFilters
            // 
            this.groupActivityFilters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupActivityFilters.Controls.Add(this.afEmailsToPatients);
            this.groupActivityFilters.Controls.Add(this.afEmailsToFacilities);
            this.groupActivityFilters.Controls.Add(this.afSms);
            this.groupActivityFilters.Location = new System.Drawing.Point(412, 7);
            this.groupActivityFilters.Name = "groupActivityFilters";
            this.groupActivityFilters.Size = new System.Drawing.Size(748, 81);
            this.groupActivityFilters.TabIndex = 4;
            this.groupActivityFilters.TabStop = false;
            this.groupActivityFilters.Text = "Activity Report Filters";
            this.groupActivityFilters.Visible = false;
            // 
            // afEmailsToPatients
            // 
            this.afEmailsToPatients.AutoSize = true;
            this.afEmailsToPatients.Checked = true;
            this.afEmailsToPatients.CheckState = System.Windows.Forms.CheckState.Checked;
            this.afEmailsToPatients.Location = new System.Drawing.Point(16, 40);
            this.afEmailsToPatients.Name = "afEmailsToPatients";
            this.afEmailsToPatients.Size = new System.Drawing.Size(160, 17);
            this.afEmailsToPatients.TabIndex = 2;
            this.afEmailsToPatients.Text = "Show sent emails to patients";
            this.afEmailsToPatients.UseVisualStyleBackColor = true;
            // 
            // afEmailsToFacilities
            // 
            this.afEmailsToFacilities.AutoSize = true;
            this.afEmailsToFacilities.Checked = true;
            this.afEmailsToFacilities.CheckState = System.Windows.Forms.CheckState.Checked;
            this.afEmailsToFacilities.Location = new System.Drawing.Point(16, 19);
            this.afEmailsToFacilities.Name = "afEmailsToFacilities";
            this.afEmailsToFacilities.Size = new System.Drawing.Size(160, 17);
            this.afEmailsToFacilities.TabIndex = 1;
            this.afEmailsToFacilities.Text = "Show sent emails to facilities";
            this.afEmailsToFacilities.UseVisualStyleBackColor = true;
            // 
            // afSms
            // 
            this.afSms.AutoSize = true;
            this.afSms.Checked = true;
            this.afSms.CheckState = System.Windows.Forms.CheckState.Checked;
            this.afSms.Location = new System.Drawing.Point(16, 60);
            this.afSms.Name = "afSms";
            this.afSms.Size = new System.Drawing.Size(151, 17);
            this.afSms.TabIndex = 0;
            this.afSms.Text = "Show sent text messasges";
            this.afSms.UseVisualStyleBackColor = true;
            // 
            // tabSmsQueue
            // 
            this.tabSmsQueue.Controls.Add(this.btnRemoveFromSmsQueue);
            this.tabSmsQueue.Controls.Add(this.groupBox4);
            this.tabSmsQueue.Controls.Add(this.groupBox10);
            this.tabSmsQueue.Controls.Add(this.btnToggle);
            this.tabSmsQueue.Controls.Add(this.gridSmsQueue);
            this.tabSmsQueue.Location = new System.Drawing.Point(4, 22);
            this.tabSmsQueue.Name = "tabSmsQueue";
            this.tabSmsQueue.Padding = new System.Windows.Forms.Padding(3);
            this.tabSmsQueue.Size = new System.Drawing.Size(1177, 615);
            this.tabSmsQueue.TabIndex = 4;
            this.tabSmsQueue.Text = "The SMS Queue";
            this.tabSmsQueue.UseVisualStyleBackColor = true;
            // 
            // btnRemoveFromSmsQueue
            // 
            this.btnRemoveFromSmsQueue.ForeColor = System.Drawing.Color.Maroon;
            this.btnRemoveFromSmsQueue.Location = new System.Drawing.Point(953, 586);
            this.btnRemoveFromSmsQueue.Name = "btnRemoveFromSmsQueue";
            this.btnRemoveFromSmsQueue.Size = new System.Drawing.Size(121, 23);
            this.btnRemoveFromSmsQueue.TabIndex = 13;
            this.btnRemoveFromSmsQueue.Text = "Remove selected";
            this.btnRemoveFromSmsQueue.UseVisualStyleBackColor = true;
            this.btnRemoveFromSmsQueue.Click += new System.EventHandler(this.btnRemoveFromSmsQueue_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.lblWillStopAfter);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.chkAutoStop);
            this.groupBox4.Controls.Add(this.numAutoStopCount);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Location = new System.Drawing.Point(227, 7);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(320, 74);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Control";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(216, 41);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "text messages";
            // 
            // lblWillStopAfter
            // 
            this.lblWillStopAfter.AutoSize = true;
            this.lblWillStopAfter.Location = new System.Drawing.Point(149, 42);
            this.lblWillStopAfter.Name = "lblWillStopAfter";
            this.lblWillStopAfter.Size = new System.Drawing.Size(13, 13);
            this.lblWillStopAfter.TabIndex = 8;
            this.lblWillStopAfter.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(67, 41);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Will stop after: ";
            // 
            // chkAutoStop
            // 
            this.chkAutoStop.AutoSize = true;
            this.chkAutoStop.Location = new System.Drawing.Point(9, 20);
            this.chkAutoStop.Name = "chkAutoStop";
            this.chkAutoStop.Size = new System.Drawing.Size(135, 17);
            this.chkAutoStop.TabIndex = 4;
            this.chkAutoStop.Text = "Auto stop after sending";
            this.chkAutoStop.UseVisualStyleBackColor = true;
            this.chkAutoStop.CheckedChanged += new System.EventHandler(this.chkAutoStop_CheckedChanged);
            // 
            // numAutoStopCount
            // 
            this.numAutoStopCount.Location = new System.Drawing.Point(150, 18);
            this.numAutoStopCount.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numAutoStopCount.Name = "numAutoStopCount";
            this.numAutoStopCount.Size = new System.Drawing.Size(59, 20);
            this.numAutoStopCount.TabIndex = 5;
            this.numAutoStopCount.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numAutoStopCount.ValueChanged += new System.EventHandler(this.numAutoStopCount_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(215, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "text messages";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label19);
            this.groupBox10.Controls.Add(this.lblFailedSend);
            this.groupBox10.Controls.Add(this.label20);
            this.groupBox10.Controls.Add(this.lblSuccessfullySend);
            this.groupBox10.Controls.Add(this.label21);
            this.groupBox10.Controls.Add(this.lblQueueSize);
            this.groupBox10.Location = new System.Drawing.Point(6, 7);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(215, 74);
            this.groupBox10.TabIndex = 11;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Dashboard";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(16, 49);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(118, 13);
            this.label19.TabIndex = 6;
            this.label19.Text = "Errors while processing:";
            // 
            // lblFailedSend
            // 
            this.lblFailedSend.AutoSize = true;
            this.lblFailedSend.ForeColor = System.Drawing.Color.Red;
            this.lblFailedSend.Location = new System.Drawing.Point(187, 50);
            this.lblFailedSend.Name = "lblFailedSend";
            this.lblFailedSend.Size = new System.Drawing.Size(13, 13);
            this.lblFailedSend.TabIndex = 7;
            this.lblFailedSend.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Green;
            this.label20.Location = new System.Drawing.Point(16, 32);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(171, 13);
            this.label20.TabIndex = 4;
            this.label20.Text = "Successfully processed messages:";
            // 
            // lblSuccessfullySend
            // 
            this.lblSuccessfullySend.AutoSize = true;
            this.lblSuccessfullySend.ForeColor = System.Drawing.Color.Green;
            this.lblSuccessfullySend.Location = new System.Drawing.Point(187, 33);
            this.lblSuccessfullySend.Name = "lblSuccessfullySend";
            this.lblSuccessfullySend.Size = new System.Drawing.Size(13, 13);
            this.lblSuccessfullySend.TabIndex = 5;
            this.lblSuccessfullySend.Text = "0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(16, 16);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(119, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "Pending text messages:";
            // 
            // lblQueueSize
            // 
            this.lblQueueSize.AutoSize = true;
            this.lblQueueSize.Location = new System.Drawing.Point(187, 16);
            this.lblQueueSize.Name = "lblQueueSize";
            this.lblQueueSize.Size = new System.Drawing.Size(13, 13);
            this.lblQueueSize.TabIndex = 3;
            this.lblQueueSize.Text = "0";
            // 
            // btnToggle
            // 
            this.btnToggle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnToggle.Location = new System.Drawing.Point(1080, 586);
            this.btnToggle.Name = "btnToggle";
            this.btnToggle.Size = new System.Drawing.Size(91, 23);
            this.btnToggle.TabIndex = 10;
            this.btnToggle.Text = "Stop";
            this.btnToggle.UseVisualStyleBackColor = true;
            this.btnToggle.Click += new System.EventHandler(this.btnToggle_Click);
            // 
            // gridSmsQueue
            // 
            this.gridSmsQueue.AllowUserToAddRows = false;
            this.gridSmsQueue.AllowUserToDeleteRows = false;
            this.gridSmsQueue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSmsQueue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridSmsQueue.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GridQueueId,
            this.GridQueueStatus,
            this.GridQueueRecipient,
            this.GridQueueFacility,
            this.GridQueueAbout,
            this.GridQueueActionOn,
            this.GridQueueShortUrl,
            this.GridQueueMessage});
            this.gridSmsQueue.Location = new System.Drawing.Point(6, 87);
            this.gridSmsQueue.Name = "gridSmsQueue";
            this.gridSmsQueue.ReadOnly = true;
            this.gridSmsQueue.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridSmsQueue.Size = new System.Drawing.Size(1165, 493);
            this.gridSmsQueue.TabIndex = 9;
            // 
            // GridQueueId
            // 
            this.GridQueueId.HeaderText = "Id";
            this.GridQueueId.Name = "GridQueueId";
            this.GridQueueId.ReadOnly = true;
            this.GridQueueId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.GridQueueId.Visible = false;
            // 
            // GridQueueStatus
            // 
            this.GridQueueStatus.HeaderText = "Status";
            this.GridQueueStatus.Name = "GridQueueStatus";
            this.GridQueueStatus.ReadOnly = true;
            this.GridQueueStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // GridQueueRecipient
            // 
            this.GridQueueRecipient.HeaderText = "Recipient";
            this.GridQueueRecipient.Name = "GridQueueRecipient";
            this.GridQueueRecipient.ReadOnly = true;
            this.GridQueueRecipient.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // GridQueueFacility
            // 
            this.GridQueueFacility.HeaderText = "Facility Name";
            this.GridQueueFacility.Name = "GridQueueFacility";
            this.GridQueueFacility.ReadOnly = true;
            this.GridQueueFacility.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.GridQueueFacility.Width = 130;
            // 
            // GridQueueAbout
            // 
            this.GridQueueAbout.HeaderText = "About";
            this.GridQueueAbout.Name = "GridQueueAbout";
            this.GridQueueAbout.ReadOnly = true;
            this.GridQueueAbout.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.GridQueueAbout.Width = 110;
            // 
            // GridQueueActionOn
            // 
            this.GridQueueActionOn.HeaderText = "Action On";
            this.GridQueueActionOn.Name = "GridQueueActionOn";
            this.GridQueueActionOn.ReadOnly = true;
            this.GridQueueActionOn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // GridQueueShortUrl
            // 
            this.GridQueueShortUrl.HeaderText = "Url (if any)";
            this.GridQueueShortUrl.Name = "GridQueueShortUrl";
            this.GridQueueShortUrl.ReadOnly = true;
            this.GridQueueShortUrl.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // GridQueueMessage
            // 
            this.GridQueueMessage.HeaderText = "Text Message";
            this.GridQueueMessage.Name = "GridQueueMessage";
            this.GridQueueMessage.ReadOnly = true;
            this.GridQueueMessage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.GridQueueMessage.Width = 200;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnRemoveEmailsFromQueue);
            this.tabPage1.Controls.Add(this.groupBox21);
            this.tabPage1.Controls.Add(this.btnToggleEmailQueue);
            this.tabPage1.Controls.Add(this.gridEmailsQueue);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1177, 615);
            this.tabPage1.TabIndex = 8;
            this.tabPage1.Text = "The Emails Queue";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnRemoveEmailsFromQueue
            // 
            this.btnRemoveEmailsFromQueue.ForeColor = System.Drawing.Color.Maroon;
            this.btnRemoveEmailsFromQueue.Location = new System.Drawing.Point(953, 585);
            this.btnRemoveEmailsFromQueue.Name = "btnRemoveEmailsFromQueue";
            this.btnRemoveEmailsFromQueue.Size = new System.Drawing.Size(121, 23);
            this.btnRemoveEmailsFromQueue.TabIndex = 18;
            this.btnRemoveEmailsFromQueue.Text = "Remove selected";
            this.btnRemoveEmailsFromQueue.UseVisualStyleBackColor = true;
            this.btnRemoveEmailsFromQueue.Click += new System.EventHandler(this.btnRemoveEmailsFromQueue_Click);
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.label35);
            this.groupBox21.Controls.Add(this.lblQueueEmailsErrors);
            this.groupBox21.Controls.Add(this.label37);
            this.groupBox21.Controls.Add(this.lblQueueEmailsSuccessfully);
            this.groupBox21.Controls.Add(this.label39);
            this.groupBox21.Controls.Add(this.lblQueueEmailsPending);
            this.groupBox21.Location = new System.Drawing.Point(6, 6);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(215, 74);
            this.groupBox21.TabIndex = 16;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Dashboard";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.ForeColor = System.Drawing.Color.Red;
            this.label35.Location = new System.Drawing.Point(16, 49);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(118, 13);
            this.label35.TabIndex = 6;
            this.label35.Text = "Errors while processing:";
            // 
            // lblQueueEmailsErrors
            // 
            this.lblQueueEmailsErrors.AutoSize = true;
            this.lblQueueEmailsErrors.ForeColor = System.Drawing.Color.Red;
            this.lblQueueEmailsErrors.Location = new System.Drawing.Point(187, 50);
            this.lblQueueEmailsErrors.Name = "lblQueueEmailsErrors";
            this.lblQueueEmailsErrors.Size = new System.Drawing.Size(13, 13);
            this.lblQueueEmailsErrors.TabIndex = 7;
            this.lblQueueEmailsErrors.Text = "0";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.ForeColor = System.Drawing.Color.Green;
            this.label37.Location = new System.Drawing.Point(16, 32);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(153, 13);
            this.label37.TabIndex = 4;
            this.label37.Text = "Successfully processed emails:";
            // 
            // lblQueueEmailsSuccessfully
            // 
            this.lblQueueEmailsSuccessfully.AutoSize = true;
            this.lblQueueEmailsSuccessfully.ForeColor = System.Drawing.Color.Green;
            this.lblQueueEmailsSuccessfully.Location = new System.Drawing.Point(187, 33);
            this.lblQueueEmailsSuccessfully.Name = "lblQueueEmailsSuccessfully";
            this.lblQueueEmailsSuccessfully.Size = new System.Drawing.Size(13, 13);
            this.lblQueueEmailsSuccessfully.TabIndex = 5;
            this.lblQueueEmailsSuccessfully.Text = "0";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(16, 16);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(81, 13);
            this.label39.TabIndex = 2;
            this.label39.Text = "Pending emails:";
            // 
            // lblQueueEmailsPending
            // 
            this.lblQueueEmailsPending.AutoSize = true;
            this.lblQueueEmailsPending.Location = new System.Drawing.Point(187, 16);
            this.lblQueueEmailsPending.Name = "lblQueueEmailsPending";
            this.lblQueueEmailsPending.Size = new System.Drawing.Size(13, 13);
            this.lblQueueEmailsPending.TabIndex = 3;
            this.lblQueueEmailsPending.Text = "0";
            // 
            // btnToggleEmailQueue
            // 
            this.btnToggleEmailQueue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnToggleEmailQueue.Location = new System.Drawing.Point(1080, 585);
            this.btnToggleEmailQueue.Name = "btnToggleEmailQueue";
            this.btnToggleEmailQueue.Size = new System.Drawing.Size(91, 23);
            this.btnToggleEmailQueue.TabIndex = 15;
            this.btnToggleEmailQueue.Text = "Stop";
            this.btnToggleEmailQueue.UseVisualStyleBackColor = true;
            this.btnToggleEmailQueue.Click += new System.EventHandler(this.btnToggleEmailQueue_Click);
            // 
            // gridEmailsQueue
            // 
            this.gridEmailsQueue.AllowUserToAddRows = false;
            this.gridEmailsQueue.AllowUserToDeleteRows = false;
            this.gridEmailsQueue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridEmailsQueue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridEmailsQueue.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColEmailsQueueId,
            this.ColEmailsQueueStatus,
            this.ColEmailsQueueRecipient,
            this.ColEmailsQueueFrom,
            this.ColEmailsQueueFacilityName,
            this.ColEmailsQueuePatientName,
            this.ColEmailsQueueSubject});
            this.gridEmailsQueue.Location = new System.Drawing.Point(6, 86);
            this.gridEmailsQueue.Name = "gridEmailsQueue";
            this.gridEmailsQueue.ReadOnly = true;
            this.gridEmailsQueue.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridEmailsQueue.Size = new System.Drawing.Size(1165, 493);
            this.gridEmailsQueue.TabIndex = 14;
            // 
            // ColEmailsQueueId
            // 
            this.ColEmailsQueueId.HeaderText = "Id";
            this.ColEmailsQueueId.Name = "ColEmailsQueueId";
            this.ColEmailsQueueId.ReadOnly = true;
            this.ColEmailsQueueId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColEmailsQueueId.Visible = false;
            // 
            // ColEmailsQueueStatus
            // 
            this.ColEmailsQueueStatus.HeaderText = "Status";
            this.ColEmailsQueueStatus.Name = "ColEmailsQueueStatus";
            this.ColEmailsQueueStatus.ReadOnly = true;
            this.ColEmailsQueueStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColEmailsQueueRecipient
            // 
            this.ColEmailsQueueRecipient.HeaderText = "Recipient";
            this.ColEmailsQueueRecipient.Name = "ColEmailsQueueRecipient";
            this.ColEmailsQueueRecipient.ReadOnly = true;
            this.ColEmailsQueueRecipient.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColEmailsQueueFrom
            // 
            this.ColEmailsQueueFrom.HeaderText = "Sender";
            this.ColEmailsQueueFrom.Name = "ColEmailsQueueFrom";
            this.ColEmailsQueueFrom.ReadOnly = true;
            // 
            // ColEmailsQueueFacilityName
            // 
            this.ColEmailsQueueFacilityName.HeaderText = "Facility Name";
            this.ColEmailsQueueFacilityName.Name = "ColEmailsQueueFacilityName";
            this.ColEmailsQueueFacilityName.ReadOnly = true;
            this.ColEmailsQueueFacilityName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColEmailsQueueFacilityName.Width = 130;
            // 
            // ColEmailsQueuePatientName
            // 
            this.ColEmailsQueuePatientName.HeaderText = "Patient Name";
            this.ColEmailsQueuePatientName.Name = "ColEmailsQueuePatientName";
            this.ColEmailsQueuePatientName.ReadOnly = true;
            this.ColEmailsQueuePatientName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColEmailsQueuePatientName.Width = 110;
            // 
            // ColEmailsQueueSubject
            // 
            this.ColEmailsQueueSubject.HeaderText = "Subject";
            this.ColEmailsQueueSubject.Name = "ColEmailsQueueSubject";
            this.ColEmailsQueueSubject.ReadOnly = true;
            this.ColEmailsQueueSubject.Width = 220;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox26);
            this.tabPage2.Controls.Add(this.groupBox25);
            this.tabPage2.Controls.Add(this.groupBox20);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1177, 615);
            this.tabPage2.TabIndex = 9;
            this.tabPage2.Text = "Question campaigns";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox26
            // 
            this.groupBox26.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox26.Controls.Add(this.btnQC_EditQuestionCampaign);
            this.groupBox26.Controls.Add(this.btnQC_AddQuestionCampaign);
            this.groupBox26.Controls.Add(this.cbQC_QuestionsCampaignSelect);
            this.groupBox26.Controls.Add(this.label42);
            this.groupBox26.Controls.Add(this.label41);
            this.groupBox26.Controls.Add(this.groupBox27);
            this.groupBox26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox26.Location = new System.Drawing.Point(6, 6);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(355, 113);
            this.groupBox26.TabIndex = 35;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "                                                   ";
            // 
            // btnQC_EditQuestionCampaign
            // 
            this.btnQC_EditQuestionCampaign.Location = new System.Drawing.Point(193, 80);
            this.btnQC_EditQuestionCampaign.Name = "btnQC_EditQuestionCampaign";
            this.btnQC_EditQuestionCampaign.Size = new System.Drawing.Size(75, 23);
            this.btnQC_EditQuestionCampaign.TabIndex = 65;
            this.btnQC_EditQuestionCampaign.Text = "Edit";
            this.btnQC_EditQuestionCampaign.UseVisualStyleBackColor = true;
            this.btnQC_EditQuestionCampaign.Click += new System.EventHandler(this.btnQC_EditQuestionCampaign_Click);
            // 
            // btnQC_AddQuestionCampaign
            // 
            this.btnQC_AddQuestionCampaign.Location = new System.Drawing.Point(274, 80);
            this.btnQC_AddQuestionCampaign.Name = "btnQC_AddQuestionCampaign";
            this.btnQC_AddQuestionCampaign.Size = new System.Drawing.Size(70, 23);
            this.btnQC_AddQuestionCampaign.TabIndex = 64;
            this.btnQC_AddQuestionCampaign.Text = "Add new...";
            this.btnQC_AddQuestionCampaign.UseVisualStyleBackColor = true;
            this.btnQC_AddQuestionCampaign.Click += new System.EventHandler(this.btnQC_AddQuestionCampaign_Click);
            // 
            // cbQC_QuestionsCampaignSelect
            // 
            this.cbQC_QuestionsCampaignSelect.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbQC_QuestionsCampaignSelect.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbQC_QuestionsCampaignSelect.DisplayMember = "Text";
            this.cbQC_QuestionsCampaignSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbQC_QuestionsCampaignSelect.FormattingEnabled = true;
            this.cbQC_QuestionsCampaignSelect.Location = new System.Drawing.Point(18, 44);
            this.cbQC_QuestionsCampaignSelect.Name = "cbQC_QuestionsCampaignSelect";
            this.cbQC_QuestionsCampaignSelect.Size = new System.Drawing.Size(326, 21);
            this.cbQC_QuestionsCampaignSelect.TabIndex = 63;
            this.cbQC_QuestionsCampaignSelect.ValueMember = "Value";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(15, 25);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(135, 13);
            this.label42.TabIndex = 62;
            this.label42.Text = "Questions campaign name:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label41.Location = new System.Drawing.Point(-1, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(224, 16);
            this.label41.TabIndex = 61;
            this.label41.Text = "1. Select a questions campaign";
            // 
            // groupBox27
            // 
            this.groupBox27.Location = new System.Drawing.Point(2, 293);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(1021, 6);
            this.groupBox27.TabIndex = 35;
            this.groupBox27.TabStop = false;
            // 
            // groupBox25
            // 
            this.groupBox25.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox25.Controls.Add(this.lblQC_selectedCH);
            this.groupBox25.Controls.Add(this.label12345);
            this.groupBox25.Controls.Add(this.btnQC_SelectOnlyQuestioned);
            this.groupBox25.Controls.Add(this.btnQC_SelectOnlyNotQuestioned);
            this.groupBox25.Controls.Add(this.btnQC_DeselectAll);
            this.groupBox25.Controls.Add(this.btnQC_SendQuestions);
            this.groupBox25.Controls.Add(this.gridQC_FoundedCareHomes);
            this.groupBox25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox25.Location = new System.Drawing.Point(6, 125);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(1164, 484);
            this.groupBox25.TabIndex = 34;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "3. Founded care homes:";
            // 
            // lblQC_selectedCH
            // 
            this.lblQC_selectedCH.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblQC_selectedCH.AutoSize = true;
            this.lblQC_selectedCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQC_selectedCH.Location = new System.Drawing.Point(613, 460);
            this.lblQC_selectedCH.Name = "lblQC_selectedCH";
            this.lblQC_selectedCH.Size = new System.Drawing.Size(14, 13);
            this.lblQC_selectedCH.TabIndex = 67;
            this.lblQC_selectedCH.Text = "0";
            // 
            // label12345
            // 
            this.label12345.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label12345.AutoSize = true;
            this.label12345.Location = new System.Drawing.Point(533, 460);
            this.label12345.Name = "label12345";
            this.label12345.Size = new System.Drawing.Size(82, 13);
            this.label12345.TabIndex = 66;
            this.label12345.Text = "Selected count:";
            // 
            // btnQC_SelectOnlyQuestioned
            // 
            this.btnQC_SelectOnlyQuestioned.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnQC_SelectOnlyQuestioned.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQC_SelectOnlyQuestioned.Location = new System.Drawing.Point(315, 455);
            this.btnQC_SelectOnlyQuestioned.Name = "btnQC_SelectOnlyQuestioned";
            this.btnQC_SelectOnlyQuestioned.Size = new System.Drawing.Size(212, 23);
            this.btnQC_SelectOnlyQuestioned.TabIndex = 64;
            this.btnQC_SelectOnlyQuestioned.Text = "Select successfully questioned";
            this.btnQC_SelectOnlyQuestioned.UseVisualStyleBackColor = true;
            this.btnQC_SelectOnlyQuestioned.Click += new System.EventHandler(this.btnQC_SelectOnlyQuestioned_Click);
            // 
            // btnQC_SelectOnlyNotQuestioned
            // 
            this.btnQC_SelectOnlyNotQuestioned.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnQC_SelectOnlyNotQuestioned.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQC_SelectOnlyNotQuestioned.Location = new System.Drawing.Point(97, 455);
            this.btnQC_SelectOnlyNotQuestioned.Name = "btnQC_SelectOnlyNotQuestioned";
            this.btnQC_SelectOnlyNotQuestioned.Size = new System.Drawing.Size(212, 23);
            this.btnQC_SelectOnlyNotQuestioned.TabIndex = 63;
            this.btnQC_SelectOnlyNotQuestioned.Text = "Select only not questioned";
            this.btnQC_SelectOnlyNotQuestioned.UseVisualStyleBackColor = true;
            this.btnQC_SelectOnlyNotQuestioned.Click += new System.EventHandler(this.btnQC_SelectOnlyNotQuestioned_Click);
            // 
            // btnQC_DeselectAll
            // 
            this.btnQC_DeselectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnQC_DeselectAll.Location = new System.Drawing.Point(9, 455);
            this.btnQC_DeselectAll.Name = "btnQC_DeselectAll";
            this.btnQC_DeselectAll.Size = new System.Drawing.Size(75, 23);
            this.btnQC_DeselectAll.TabIndex = 32;
            this.btnQC_DeselectAll.Text = "Deselect all";
            this.btnQC_DeselectAll.UseVisualStyleBackColor = true;
            this.btnQC_DeselectAll.Click += new System.EventHandler(this.btnQC_DeselectAll_Click);
            // 
            // btnQC_SendQuestions
            // 
            this.btnQC_SendQuestions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQC_SendQuestions.Location = new System.Drawing.Point(903, 455);
            this.btnQC_SendQuestions.Name = "btnQC_SendQuestions";
            this.btnQC_SendQuestions.Size = new System.Drawing.Size(247, 23);
            this.btnQC_SendQuestions.TabIndex = 31;
            this.btnQC_SendQuestions.Text = "4. Send Question Campaigns via text message";
            this.btnQC_SendQuestions.UseVisualStyleBackColor = true;
            this.btnQC_SendQuestions.Click += new System.EventHandler(this.btnQC_SendQuestions_Click);
            // 
            // gridQC_FoundedCareHomes
            // 
            this.gridQC_FoundedCareHomes.AllowUserToAddRows = false;
            this.gridQC_FoundedCareHomes.AllowUserToDeleteRows = false;
            this.gridQC_FoundedCareHomes.AllowUserToOrderColumns = true;
            this.gridQC_FoundedCareHomes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridQC_FoundedCareHomes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridQC_FoundedCareHomes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gridColQC_id,
            this.gridColQC_selected,
            this.gridColQC_facilityName,
            this.gridColQC_altcs,
            this.gridColQC_distance,
            this.gridColQC_city,
            this.gridColQC_privateRoom,
            this.gridColQC_semiPrivateRoom,
            this.gridColQC_questioned,
            this.gridColQC_responce,
            this.gridColQC_questionedOn});
            this.gridQC_FoundedCareHomes.Location = new System.Drawing.Point(8, 22);
            this.gridQC_FoundedCareHomes.Name = "gridQC_FoundedCareHomes";
            this.gridQC_FoundedCareHomes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridQC_FoundedCareHomes.Size = new System.Drawing.Size(1142, 427);
            this.gridQC_FoundedCareHomes.TabIndex = 29;
            this.gridQC_FoundedCareHomes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridQC_FoundedCareHomes_CellContentClick);
            this.gridQC_FoundedCareHomes.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridQC_FoundedCareHomes_CellValueChanged);
            // 
            // gridColQC_id
            // 
            this.gridColQC_id.HeaderText = "Id";
            this.gridColQC_id.Name = "gridColQC_id";
            this.gridColQC_id.Visible = false;
            // 
            // gridColQC_selected
            // 
            this.gridColQC_selected.FillWeight = 40F;
            this.gridColQC_selected.HeaderText = "Select";
            this.gridColQC_selected.Name = "gridColQC_selected";
            this.gridColQC_selected.Width = 40;
            // 
            // gridColQC_facilityName
            // 
            this.gridColQC_facilityName.FillWeight = 185F;
            this.gridColQC_facilityName.HeaderText = "Facility Name";
            this.gridColQC_facilityName.MinimumWidth = 100;
            this.gridColQC_facilityName.Name = "gridColQC_facilityName";
            this.gridColQC_facilityName.ReadOnly = true;
            this.gridColQC_facilityName.Width = 185;
            // 
            // gridColQC_altcs
            // 
            this.gridColQC_altcs.FillWeight = 50F;
            this.gridColQC_altcs.HeaderText = "ALTCS";
            this.gridColQC_altcs.Name = "gridColQC_altcs";
            this.gridColQC_altcs.ReadOnly = true;
            this.gridColQC_altcs.Width = 50;
            // 
            // gridColQC_distance
            // 
            this.gridColQC_distance.HeaderText = "Distance";
            this.gridColQC_distance.Name = "gridColQC_distance";
            // 
            // gridColQC_city
            // 
            this.gridColQC_city.HeaderText = "City";
            this.gridColQC_city.Name = "gridColQC_city";
            this.gridColQC_city.ReadOnly = true;
            // 
            // gridColQC_privateRoom
            // 
            this.gridColQC_privateRoom.HeaderText = "Private room";
            this.gridColQC_privateRoom.Name = "gridColQC_privateRoom";
            this.gridColQC_privateRoom.ReadOnly = true;
            this.gridColQC_privateRoom.Width = 80;
            // 
            // gridColQC_semiPrivateRoom
            // 
            this.gridColQC_semiPrivateRoom.HeaderText = "Semi-Private room";
            this.gridColQC_semiPrivateRoom.Name = "gridColQC_semiPrivateRoom";
            this.gridColQC_semiPrivateRoom.ReadOnly = true;
            this.gridColQC_semiPrivateRoom.Width = 80;
            // 
            // gridColQC_questioned
            // 
            this.gridColQC_questioned.HeaderText = "Questioned";
            this.gridColQC_questioned.Name = "gridColQC_questioned";
            this.gridColQC_questioned.ReadOnly = true;
            this.gridColQC_questioned.Width = 75;
            // 
            // gridColQC_responce
            // 
            this.gridColQC_responce.HeaderText = "Responce";
            this.gridColQC_responce.Name = "gridColQC_responce";
            this.gridColQC_responce.ReadOnly = true;
            this.gridColQC_responce.Width = 150;
            // 
            // gridColQC_questionedOn
            // 
            this.gridColQC_questionedOn.HeaderText = "Questioned On";
            this.gridColQC_questionedOn.Name = "gridColQC_questionedOn";
            this.gridColQC_questionedOn.ReadOnly = true;
            this.gridColQC_questionedOn.Width = 85;
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.txtQC_zipRadius);
            this.groupBox20.Controls.Add(this.label44);
            this.groupBox20.Controls.Add(this.label43);
            this.groupBox20.Controls.Add(this.txtQC_zipCode);
            this.groupBox20.Controls.Add(this.label31);
            this.groupBox20.Controls.Add(this.groupBox22);
            this.groupBox20.Controls.Add(this.groupBox23);
            this.groupBox20.Controls.Add(this.groupBox24);
            this.groupBox20.Controls.Add(this.btnQC_FilterCareHomes);
            this.groupBox20.Location = new System.Drawing.Point(367, 6);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(789, 113);
            this.groupBox20.TabIndex = 33;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "                                                                        ";
            // 
            // txtQC_zipRadius
            // 
            this.txtQC_zipRadius.Location = new System.Drawing.Point(614, 75);
            this.txtQC_zipRadius.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.txtQC_zipRadius.Name = "txtQC_zipRadius";
            this.txtQC_zipRadius.Size = new System.Drawing.Size(83, 20);
            this.txtQC_zipRadius.TabIndex = 71;
            this.txtQC_zipRadius.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(611, 57);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(86, 13);
            this.label44.TabIndex = 70;
            this.label44.Text = "Radius (in miles):";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(611, 16);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(47, 13);
            this.label43.TabIndex = 64;
            this.label43.Text = "ZipCode";
            // 
            // txtQC_zipCode
            // 
            this.txtQC_zipCode.Location = new System.Drawing.Point(614, 32);
            this.txtQC_zipCode.Name = "txtQC_zipCode";
            this.txtQC_zipCode.Size = new System.Drawing.Size(81, 20);
            this.txtQC_zipCode.TabIndex = 63;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label31.Location = new System.Drawing.Point(6, -1);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(145, 16);
            this.label31.TabIndex = 62;
            this.label31.Text = "2. Care home select";
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.edQC_SemiPirvateRoomTo);
            this.groupBox22.Controls.Add(this.label32);
            this.groupBox22.Controls.Add(this.edQC_SemiPirvateRoomFrom);
            this.groupBox22.Controls.Add(this.label33);
            this.groupBox22.Location = new System.Drawing.Point(267, 25);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(246, 73);
            this.groupBox22.TabIndex = 34;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Semi-Private room patient budget:";
            // 
            // edQC_SemiPirvateRoomTo
            // 
            this.edQC_SemiPirvateRoomTo.Location = new System.Drawing.Point(147, 34);
            this.edQC_SemiPirvateRoomTo.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.edQC_SemiPirvateRoomTo.Name = "edQC_SemiPirvateRoomTo";
            this.edQC_SemiPirvateRoomTo.Size = new System.Drawing.Size(82, 20);
            this.edQC_SemiPirvateRoomTo.TabIndex = 3;
            this.edQC_SemiPirvateRoomTo.ThousandsSeparator = true;
            this.edQC_SemiPirvateRoomTo.Value = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(144, 20);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(73, 13);
            this.label32.TabIndex = 2;
            this.label32.Text = "To (in dollars):";
            // 
            // edQC_SemiPirvateRoomFrom
            // 
            this.edQC_SemiPirvateRoomFrom.Location = new System.Drawing.Point(23, 34);
            this.edQC_SemiPirvateRoomFrom.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.edQC_SemiPirvateRoomFrom.Name = "edQC_SemiPirvateRoomFrom";
            this.edQC_SemiPirvateRoomFrom.Size = new System.Drawing.Size(82, 20);
            this.edQC_SemiPirvateRoomFrom.TabIndex = 1;
            this.edQC_SemiPirvateRoomFrom.ThousandsSeparator = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(20, 20);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(83, 13);
            this.label33.TabIndex = 0;
            this.label33.Text = "From (in dollars):";
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.edQC_PirvateRoomTo);
            this.groupBox23.Controls.Add(this.label36);
            this.groupBox23.Controls.Add(this.edQC_PirvateRoomFrom);
            this.groupBox23.Controls.Add(this.label38);
            this.groupBox23.Location = new System.Drawing.Point(15, 25);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(246, 73);
            this.groupBox23.TabIndex = 33;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Private room patient budget:";
            // 
            // edQC_PirvateRoomTo
            // 
            this.edQC_PirvateRoomTo.Location = new System.Drawing.Point(141, 34);
            this.edQC_PirvateRoomTo.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.edQC_PirvateRoomTo.Name = "edQC_PirvateRoomTo";
            this.edQC_PirvateRoomTo.Size = new System.Drawing.Size(82, 20);
            this.edQC_PirvateRoomTo.TabIndex = 3;
            this.edQC_PirvateRoomTo.ThousandsSeparator = true;
            this.edQC_PirvateRoomTo.Value = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(138, 20);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(73, 13);
            this.label36.TabIndex = 2;
            this.label36.Text = "To (in dollars):";
            // 
            // edQC_PirvateRoomFrom
            // 
            this.edQC_PirvateRoomFrom.Location = new System.Drawing.Point(23, 34);
            this.edQC_PirvateRoomFrom.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.edQC_PirvateRoomFrom.Name = "edQC_PirvateRoomFrom";
            this.edQC_PirvateRoomFrom.Size = new System.Drawing.Size(82, 20);
            this.edQC_PirvateRoomFrom.TabIndex = 1;
            this.edQC_PirvateRoomFrom.ThousandsSeparator = true;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(20, 20);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(83, 13);
            this.label38.TabIndex = 0;
            this.label38.Text = "From (in dollars):";
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.radioQC_ALTCS_Yes);
            this.groupBox24.Controls.Add(this.radioQC_ALTCS_No);
            this.groupBox24.Location = new System.Drawing.Point(530, 25);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(71, 73);
            this.groupBox24.TabIndex = 58;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "ALTCS";
            // 
            // radioQC_ALTCS_Yes
            // 
            this.radioQC_ALTCS_Yes.AutoSize = true;
            this.radioQC_ALTCS_Yes.Location = new System.Drawing.Point(12, 20);
            this.radioQC_ALTCS_Yes.Name = "radioQC_ALTCS_Yes";
            this.radioQC_ALTCS_Yes.Size = new System.Drawing.Size(43, 17);
            this.radioQC_ALTCS_Yes.TabIndex = 6;
            this.radioQC_ALTCS_Yes.TabStop = true;
            this.radioQC_ALTCS_Yes.Text = "Yes";
            this.radioQC_ALTCS_Yes.UseVisualStyleBackColor = true;
            // 
            // radioQC_ALTCS_No
            // 
            this.radioQC_ALTCS_No.AutoSize = true;
            this.radioQC_ALTCS_No.Checked = true;
            this.radioQC_ALTCS_No.Location = new System.Drawing.Point(12, 44);
            this.radioQC_ALTCS_No.Name = "radioQC_ALTCS_No";
            this.radioQC_ALTCS_No.Size = new System.Drawing.Size(39, 17);
            this.radioQC_ALTCS_No.TabIndex = 7;
            this.radioQC_ALTCS_No.TabStop = true;
            this.radioQC_ALTCS_No.Text = "No";
            this.radioQC_ALTCS_No.UseVisualStyleBackColor = true;
            // 
            // btnQC_FilterCareHomes
            // 
            this.btnQC_FilterCareHomes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnQC_FilterCareHomes.Location = new System.Drawing.Point(705, 75);
            this.btnQC_FilterCareHomes.Name = "btnQC_FilterCareHomes";
            this.btnQC_FilterCareHomes.Size = new System.Drawing.Size(71, 23);
            this.btnQC_FilterCareHomes.TabIndex = 26;
            this.btnQC_FilterCareHomes.Text = "Search";
            this.btnQC_FilterCareHomes.UseVisualStyleBackColor = true;
            this.btnQC_FilterCareHomes.Click += new System.EventHandler(this.btnQC_FilterCareHomes_Click);
            // 
            // lblProgress
            // 
            this.lblProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblProgress.AutoSize = true;
            this.lblProgress.Location = new System.Drawing.Point(23, 653);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(58, 13);
            this.lblProgress.TabIndex = 30;
            this.lblProgress.Text = "lblProgress";
            this.lblProgress.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(8, 647);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(1015, 23);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.TabIndex = 29;
            this.progressBar.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Location = new System.Drawing.Point(1110, 647);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 24;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click_1);
            // 
            // btnLogout
            // 
            this.btnLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogout.Location = new System.Drawing.Point(1029, 647);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(75, 23);
            this.btnLogout.TabIndex = 31;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // contextReportInvitations
            // 
            this.contextReportInvitations.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem1,
            this.toolStripSeparator1});
            this.contextReportInvitations.Name = "contextReportInvitations";
            this.contextReportInvitations.Size = new System.Drawing.Size(253, 54);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(252, 22);
            this.toolStripMenuItem2.Text = "Open care home for edit";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(252, 22);
            this.toolStripMenuItem1.Text = "Mark this care home error as fixed";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(249, 6);
            // 
            // timerUpdateSelectedCount
            // 
            this.timerUpdateSelectedCount.Interval = 200;
            this.timerUpdateSelectedCount.Tick += new System.EventHandler(this.timerUpdateSelectedCount_Tick);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1193, 675);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.lblProgress);
            this.Controls.Add(this.tabMainControl);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnExit);
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "Assisted Living Facility Blaster";
            this.Load += new System.EventHandler(this.formMain_Load);
            this.Shown += new System.EventHandler(this.FormMain_Shown);
            this.tabMainControl.ResumeLayout(false);
            this.tabFindCH.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterSemiPrivateRoomTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterSemiPrivateRoomFrom)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterPrivateRoomTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterPrivateRoomFrom)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSuitableCareHomes)).EndInit();
            this.tabFindAL.ResumeLayout(false);
            this.tabFindAL.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSuitableAL)).EndInit();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtALMilesRange)).EndInit();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.tabRegisterPatients.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridPatients)).EndInit();
            this.tabRegisterAL.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridAssistedLiving)).EndInit();
            this.tabRegisterCH.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridFacilities)).EndInit();
            this.tabAdmin.ResumeLayout(false);
            this.tabAdmin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAgents)).EndInit();
            this.tabReports.ResumeLayout(false);
            this.tabReports.PerformLayout();
            this.groupReportsQuestionCampaignFilters.ResumeLayout(false);
            this.groupReportsQuestionCampaignFilters.PerformLayout();
            this.gbReportsEmailsGroup.ResumeLayout(false);
            this.gbReportsEmailsGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridReports)).EndInit();
            this.groupSentInvitationsFilters.ResumeLayout(false);
            this.groupSentInvitationsFilters.PerformLayout();
            this.groupActivityFilters.ResumeLayout(false);
            this.groupActivityFilters.PerformLayout();
            this.tabSmsQueue.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAutoStopCount)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSmsQueue)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEmailsQueue)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridQC_FoundedCareHomes)).EndInit();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtQC_zipRadius)).EndInit();
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edQC_SemiPirvateRoomTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edQC_SemiPirvateRoomFrom)).EndInit();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edQC_PirvateRoomTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edQC_PirvateRoomFrom)).EndInit();
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.contextReportInvitations.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timerLoadForm;
        private System.Windows.Forms.TabControl tabMainControl;
        private System.Windows.Forms.TabPage tabFindCH;
        private System.Windows.Forms.Label lblProgress;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.ComboBox cbRadius;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TabPage tabRegisterCH;
        private System.Windows.Forms.DataGridView gridFacilities;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView gridSuitableCareHomes;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnEditFacility;
        private System.Windows.Forms.Button btnAddNewFacility;
        private System.Windows.Forms.Button btnFacilitiesRefresh;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn FacilityName;
        private System.Windows.Forms.DataGridViewTextBoxColumn FacilityType;
        private System.Windows.Forms.DataGridViewTextBoxColumn WebSite;
        private System.Windows.Forms.DataGridViewTextBoxColumn Emails;
        private System.Windows.Forms.DataGridViewTextBoxColumn Phones;
        private System.Windows.Forms.DataGridViewTextBoxColumn Faxes;
        private System.Windows.Forms.DataGridViewTextBoxColumn State;
        private System.Windows.Forms.DataGridViewTextBoxColumn City;
        private System.Windows.Forms.DataGridViewTextBoxColumn Zip;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddressLine;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContactPersons;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ALTSC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Updated;
        private System.Windows.Forms.DataGridViewTextBoxColumn Notes;
        private System.Windows.Forms.TabPage tabAdmin;
        private System.Windows.Forms.DataGridView gridAgents;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnDeleteAgents;
        private System.Windows.Forms.Button btnEditAgent2;
        private System.Windows.Forms.Button btnAddNewAgent;
        private System.Windows.Forms.Button btnRefreshAgents;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountsColumnId;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountsColumnUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountsColumnAgentName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn AccountsColumnEnabled;
        private System.Windows.Forms.DataGridViewCheckBoxColumn AccountsColumnIsAdmin;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountsColumnLastLogin;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountsColumnCreated;
        private System.Windows.Forms.Button btnSendSmsToSelectedCH;
        private System.Windows.Forms.Button btnCheckAll;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.NumericUpDown txtFilterPrivateRoomTo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown txtFilterPrivateRoomFrom;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.NumericUpDown txtFilterSemiPrivateRoomTo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown txtFilterSemiPrivateRoomFrom;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSearchFacitilies;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rbALTCSYes;
        private System.Windows.Forms.RadioButton rbALTCSNo;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TabPage tabReports;
        private System.Windows.Forms.ComboBox cbReportTypes;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnLoadReport;
        private System.Windows.Forms.DataGridView gridReports;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnSelectOnlyNotNotified;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupActivityFilters;
        private System.Windows.Forms.GroupBox groupSentInvitationsFilters;
        private System.Windows.Forms.CheckBox sentInvitationOnlyInterested;
        private System.Windows.Forms.CheckBox afEmailsToPatients;
        private System.Windows.Forms.CheckBox afEmailsToFacilities;
        private System.Windows.Forms.CheckBox afSms;
        private System.Windows.Forms.Button btnExportSelectedToSpaceFile;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox reportSentInvitationShowErrorsOnly;
        private System.Windows.Forms.ContextMenuStrip contextReportInvitations;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbReportsInvitationsCareHomes;
        private System.Windows.Forms.ComboBox cbReportsInvitationsPatients;
        private System.Windows.Forms.Button btnRefreshStatus;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TabPage tabSmsQueue;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblWillStopAfter;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkAutoStop;
        private System.Windows.Forms.NumericUpDown numAutoStopCount;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblFailedSend;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lblSuccessfullySend;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblQueueSize;
        private System.Windows.Forms.Button btnToggle;
        private System.Windows.Forms.DataGridView gridSmsQueue;
        private System.Windows.Forms.Button btnRemoveFromSmsQueue;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridQueueId;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridQueueStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridQueueRecipient;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridQueueFacility;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridQueueAbout;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridQueueActionOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridQueueShortUrl;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridQueueMessage;
        private System.Windows.Forms.Label lblMainSelectedCount;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Timer timerUpdateSelectedCount;
        private System.Windows.Forms.TabPage tabRegisterPatients;
        private System.Windows.Forms.Button btnDeletePatient;
        private System.Windows.Forms.Button btnEditPatient;
        private System.Windows.Forms.Button btnAddPatient;
        private System.Windows.Forms.DataGridView gridPatients;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ComboBox cbCareHomesPatientSelect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddNewCareHomePatient;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox cbCareHomesLoadALPatientsToo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPatientId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPatientFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPatientAge;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPatientState;
        private System.Windows.Forms.DataGridViewTextBoxColumn PatientColCity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPatientEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPatientPhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPatientZip;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColPatientALTCS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPatientType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPatientNotes;
        private System.Windows.Forms.TabPage tabRegisterAL;
        private System.Windows.Forms.DataGridView gridAssistedLiving;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button btnALDelete;
        private System.Windows.Forms.Button btnALEdit;
        private System.Windows.Forms.Button btnALAdd;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAlId;
        private System.Windows.Forms.DataGridViewTextBoxColumn CoLAlName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAlWebSite;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAlPhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAlZip;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAlIndependentLivingPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAlAssistedLivingPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAlMemoryCarePrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAlSkilledNursingCarePrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAlEmail1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAlEmail2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAlEmail3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAlNotes;
        private System.Windows.Forms.TabPage tabFindAL;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button btnALnewPatient;
        private System.Windows.Forms.ComboBox cbFindALPatient;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.CheckBox checkSkilledNursing;
        private System.Windows.Forms.CheckBox checkMemCare;
        private System.Windows.Forms.CheckBox checkAssLiv;
        private System.Windows.Forms.CheckBox checkIndLiv;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.RadioButton rbFIndAlALTCSYes;
        private System.Windows.Forms.RadioButton rbFIndAlALTCSNo;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.TextBox txtFindALZips;
        private System.Windows.Forms.Button btnFindAlZipCodesHelp;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button btnALSearch;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button btnFindALSelectEmailedOnly;
        private System.Windows.Forms.Button btnFindAlSelectNotEmailed;
        private System.Windows.Forms.Button btnFindALDeselectAll;
        private System.Windows.Forms.Button btnALSendEmails;
        private System.Windows.Forms.DataGridView gridSuitableAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn facilitiesId;
        private System.Windows.Forms.DataGridViewCheckBoxColumn gridFacilitySelect;
        private System.Windows.Forms.DataGridViewTextBoxColumn facilitiesFacilityName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn facilitiesALTSC;
        private System.Windows.Forms.DataGridViewTextBoxColumn facilitiesDistance;
        private System.Windows.Forms.DataGridViewTextBoxColumn facilitiesCity;
        private System.Windows.Forms.DataGridViewTextBoxColumn facilitiesPrivateRoomPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn facilitiesSemiPrivateRoomPrice;
        private System.Windows.Forms.DataGridViewCheckBoxColumn facilitiesNotified;
        private System.Windows.Forms.DataGridViewCheckBoxColumn facilitiesResponse;
        private System.Windows.Forms.DataGridViewTextBoxColumn facilitiesNotifiedOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn facilitiesNotifiedStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFindALId;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColFindSelect;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFindALName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColFindALALTCS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFindALDistance;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFindAlCity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFindALIndependentLivingPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFindALAssistedLivingPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFindALMemoryCarePrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFindALSkilledNursingCarePrice;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColFindALEmailed;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFindALEmailedOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAlEmailStatus;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.NumericUpDown txtALMilesRange;
        private System.Windows.Forms.GroupBox gbReportsEmailsGroup;
        private System.Windows.Forms.ComboBox cbReportsSentEmailsAL;
        private System.Windows.Forms.ComboBox cbReportsSentEmailsPatients;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.CheckBox chkReportsSentEmailsShowErrorsOnly;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnRemoveEmailsFromQueue;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label lblQueueEmailsErrors;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label lblQueueEmailsSuccessfully;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label lblQueueEmailsPending;
        private System.Windows.Forms.Button btnToggleEmailQueue;
        private System.Windows.Forms.DataGridView gridEmailsQueue;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEmailsQueueId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEmailsQueueStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEmailsQueueRecipient;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEmailsQueueFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEmailsQueueFacilityName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEmailsQueuePatientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEmailsQueueSubject;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.Button btnQC_AddQuestionCampaign;
        private System.Windows.Forms.ComboBox cbQC_QuestionsCampaignSelect;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.Label lblQC_selectedCH;
        private System.Windows.Forms.Label label12345;
        private System.Windows.Forms.Button btnQC_SelectOnlyQuestioned;
        private System.Windows.Forms.Button btnQC_SelectOnlyNotQuestioned;
        private System.Windows.Forms.Button btnQC_DeselectAll;
        private System.Windows.Forms.Button btnQC_SendQuestions;
        private System.Windows.Forms.DataGridView gridQC_FoundedCareHomes;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.NumericUpDown edQC_SemiPirvateRoomTo;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.NumericUpDown edQC_SemiPirvateRoomFrom;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.NumericUpDown edQC_PirvateRoomTo;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.NumericUpDown edQC_PirvateRoomFrom;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.RadioButton radioQC_ALTCS_Yes;
        private System.Windows.Forms.RadioButton radioQC_ALTCS_No;
        private System.Windows.Forms.Button btnQC_FilterCareHomes;
        private System.Windows.Forms.Button btnQC_EditQuestionCampaign;
        private System.Windows.Forms.NumericUpDown txtQC_zipRadius;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtQC_zipCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColQC_id;
        private System.Windows.Forms.DataGridViewCheckBoxColumn gridColQC_selected;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColQC_facilityName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn gridColQC_altcs;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColQC_distance;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColQC_city;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColQC_privateRoom;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColQC_semiPrivateRoom;
        private System.Windows.Forms.DataGridViewCheckBoxColumn gridColQC_questioned;
        private System.Windows.Forms.DataGridViewCheckBoxColumn gridColQC_responce;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColQC_questionedOn;
        private System.Windows.Forms.GroupBox groupReportsQuestionCampaignFilters;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox comboReportsQuestionCampaigns;
    }
}

