﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistedLivingFacilityBlaster
{
    public partial class FormMain : Form
    {        
        private void LoadCareHomes()
        {
            SetProgressHint("Load facilities... First time can be slow...");
            try
            {

                using (var db = new DbEntities())
                {
                    DbCache.allCareHomes = db.CareHomes.ToList();

                    var fac = (from f in db.CareHomes
                               from c in db.CareHomeContacts.Where(w => w.FacilityId == f.Id).DefaultIfEmpty()
                               select new
                               {
                                   f,
                                   c
                               }).ToList();

                    logger.Info("  count: " + fac.Count);

                    var fac2 = (from f in fac
                                group new { f.c } by new { f.f } into gr
                                select new
                                {
                                    gr.Key,
                                    contacts = gr.ToList()
                                }).ToList();

                    logger.Info("  grouped by contacts");

                    gridFacilities.Rows.Clear();
                    foreach (var facility in fac2)
                    {
                        gridFacilities.Rows.Add(
                            facility.Key.f.Id,
                            facility.Key.f.FacilityName,
                            facility.Key.f.FacilityType,
                            facility.Key.f.WebSite,
                            facility.contacts[0].c == null ? "" : string.Join(", ", facility.contacts.Select(s => s.c.Email).ToArray()),
                            facility.contacts[0].c == null ? "" : string.Join(", ", facility.contacts.Select(s => s.c.Phone).ToArray()),
                            facility.contacts[0].c == null ? "" : string.Join(", ", facility.contacts.Select(s => s.c.Fax).ToArray()),
                            facility.Key.f.State,
                            facility.Key.f.City,
                            facility.Key.f.Zip,
                            facility.Key.f.AddressLine1,
                            facility.contacts[0].c == null ? "" : string.Join(", ", facility.contacts.Select(s => s.c.ContactName).ToArray()),
                            facility.Key.f.ALTSC,
                            facility.Key.f.Updated,
                            facility.Key.f.Notes
                        );
                    }

                    logger.Info("  All added to the grid");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while loading all facilities: " + ex.Message);
                logger.Error(ex);
            }

        }

        private void btnAddNewFacility_Click(object sender, EventArgs e)
        {
            logger.Info("Add new facility");
            try
            {
                var formFacility = new FormCareHome();
                formFacility.mode = FormCareHome.Mode.create;
                var registerNeedRefresh = formFacility.ShowDialog();

                if (registerNeedRefresh == DialogResult.Yes)
                {
                    LoadCareHomes();
                    foreach (DataGridViewRow row in gridFacilities.Rows)
                    {
                        row.Selected = row.Cells[0].Value.ToString() == formFacility.careHomeId.ToString();
                        if (row.Selected) break;
                    }

                    if (gridFacilities.SelectedRows.Count > 0)
                        gridFacilities.FirstDisplayedScrollingRowIndex = gridFacilities.SelectedRows[0].Index;

                    FillCombos(2);
                }

                logger.Info("  SUCCESS");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error in while adding new facility: " + ex.Message);
            }
        }

        private void dbGrid_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 3)
                {
                    string webSite = gridFacilities.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                    System.Diagnostics.Process.Start(webSite);
                }
                else
                {
                    Editfacility();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error double click: " + ex.Message);
            }
        }

        private void Editfacility()
        {
            logger.Info("Editfacility");
            try
            {
                if (gridFacilities.SelectedRows.Count == 0)
                {
                    logger.Info("  Nothing selected");
                    MessageBox.Show("Please select a facility to edit!");
                    return;
                }

                var formFacility = new FormCareHome();
                formFacility.mode = FormCareHome.Mode.edit;
                string selectedId = gridFacilities.SelectedRows[0].Cells["Id"].Value.ToString();
                formFacility.careHomeId = Guid.Parse(selectedId);

                var registerNeedRefresh = formFacility.ShowDialog();

                if (registerNeedRefresh == DialogResult.Yes)
                {
                    LoadCareHomes();
                    foreach (DataGridViewRow row in gridFacilities.Rows)
                    {
                        row.Selected = row.Cells[0].Value.ToString() == selectedId;
                        if (row.Selected) break;
                    }

                    if (gridFacilities.SelectedRows.Count > 0)
                        gridFacilities.FirstDisplayedScrollingRowIndex = gridFacilities.SelectedRows[0].Index;
                }

                FillCombos(2);
                logger.Info("  SUCCESS");
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Editfacility");
                MessageBox.Show("Error occured while trying to edit the faiclity: " + ex.Message);
            }
        }

        private void btnEditFacility_Click(object sender, EventArgs e)
        {
            Editfacility();
        }

        private void dbGrid_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                e.Handled = true;
            }
        }

        private void dbGrid_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                e.Handled = true;
                Editfacility();
            }
        }

        private void dbGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            logger.Info("Delete clicked");
            try
            {
                if (gridFacilities.SelectedRows.Count <= 0)
                {
                    logger.Info("  Nothing selected");
                    MessageBox.Show("Select a care home to delete!");
                    return;
                }

                DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete the selected care home?", "Delete care home", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.No)
                {
                    logger.Info("  Dont want to delete");
                    return;
                }

                SetProgressVisible(true);
                try
                {
                    using (var db = new DbEntities())
                    {
                        SetProgressHint("Get the id");
                        Guid facilityId = Guid.Parse(gridFacilities.SelectedRows[0].Cells["Id"].Value.ToString());
                        int selectedRow = gridFacilities.SelectedRows[0].Index;

                        SetProgressHint("Deleting the care home");
                        db.CareHomes.Remove(db.CareHomes.Where(w => w.Id == facilityId).Single());
                        SetProgressHint("Deleting the care home contacts");
                        db.CareHomeContacts.RemoveRange(db.CareHomeContacts.Where(w => w.FacilityId == facilityId).ToList());

                        db.SaveChanges();

                        LoadCareHomes();

                        if (gridFacilities.Rows.Count <= selectedRow)
                            selectedRow = gridFacilities.Rows.Count - 1;

                        SetProgressHint("Select the right row...");
                        if (selectedRow >= 0)
                        {
                            foreach (DataGridViewRow row in gridFacilities.Rows)
                            {
                                row.Selected = row.Index == selectedRow;
                                if (row.Selected) break;
                            }

                            if (gridFacilities.SelectedRows.Count > 0)
                                gridFacilities.FirstDisplayedScrollingRowIndex = gridFacilities.SelectedRows[0].Index;
                        }

                        FillCombos(2);
                    }
                }
                finally
                {
                    SetProgressVisible(false);
                }

                logger.Info("  SUCCESS");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error while deleting care home: " + ex.Message);
            }
        }
    }
}