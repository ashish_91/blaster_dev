﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistedLivingFacilityBlaster
{
    public partial class FormMain : Form
    {
        private void btnRemoveFromSmsQueue_Click(object sender, EventArgs e)
        {
            if (gridSmsQueue.SelectedRows.Count == 0)
            {
                MessageBox.Show("Select the entries you would like to remove from the queue!");
                return;
            }

            bool hasWorked = isSmsQueueWorking;
            if (isSmsQueueWorking)
            {
                timerSendSms.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
                isSmsQueueWorking = false;
            }

            while (isSmsQueueSendingInTheMoment)
            {
                Application.DoEvents();
                System.Threading.Thread.Sleep(10);
            }

            var queueIds = new List<Guid>();
            foreach (DataGridViewRow row in gridSmsQueue.SelectedRows)
            {
                queueIds.Add(Guid.Parse(row.Cells["GridQueueId"].Value.ToString()));
            }

            Db.RemoveFromSmsQueue(queueIds);

            isSmsQueueWorking = hasWorked;
            RefreshSmsQueue();
            MessageBox.Show("Entries successfully removed from the queue!");
        }

        private void timerSendSMS_Tick()
        {
            this.Invoke(new Action(() =>
            {
                lblQueueSize.Text = gridSmsQueue.Rows.Count.ToString();
            }));

            if (chkAutoStop.Checked && ((int)numAutoStopCount.Value - processedTextMessagesCount == 0))
            {
                ToggleSmsQueue();
                return;
            }

            if (gridSmsQueue.Rows.Count < 1) return;

            isSmsQueueSendingInTheMoment = true;

            var row = gridSmsQueue.Rows[0];

            if (row.Cells["GridQueueId"].Value == null && isSmsQueueWorking)
            {
                timerSendSms.Change(1100, System.Threading.Timeout.Infinite);
                return;
            }

            Guid queueId = new Guid(row.Cells["GridQueueId"].Value.ToString());
            QueueSms sms = DbCache.queueSms.Single(s => s.Id == queueId);

            while (DbCache.lastInvitations == null)
            {
                System.Threading.Thread.Sleep(1500);
                continue;
            }

            //maybe this patient has already been sent to this number?
            var alreadySentInvitations = DbCache.lastInvitations
                .Where(w =>
                    w.PatientId == sms.PatientId &&
                    w.PhoneSentTo == sms.Recipient)
                .ToList();

            InterestedPatients alreadySentInvitation = null;
            foreach (var inv in alreadySentInvitations)
            {
                var thMsg = sms.TextMessage.Replace("{SHORT_URL}", inv.ShortURL);
                if (inv.SmsText == thMsg)
                {
                    alreadySentInvitation = inv;
                    break;
                }
            }

            var watch = new Stopwatch();
            try
            {
                try
                {
                    sms.ShortUrl = alreadySentInvitation != null ? alreadySentInvitation.ShortURL : Helper.MakeShortUrl(sms.LongUrl.Trim(), 2);
                    sms.TextMessage = sms.TextMessage.Replace("{SHORT_URL}", sms.ShortUrl);

                    if (alreadySentInvitation == null)
                    {
                        Helper.SendSms(sms.Recipient, sms.TextMessage);
                    }

                    sms.StatusId = 1;
                    sms.Status = "Ok";
                    Db.UpdateSmsQueue(sms);

                    lblSuccessfullySend.Invoke(new Action(() =>
                    {
                        lblSuccessfullySend.Text = (int.Parse(lblSuccessfullySend.Text) + 1).ToString();
                    }));

                    gridSmsQueue.Invoke(new Action(() => gridSmsQueue.Rows.RemoveAt(0)));
                }
                catch (Exception ex)
                {
                    string errMsg = ex.Message;
                    if (errMsg.Length > 50) errMsg = errMsg.Substring(0, 50);
                    sms.Status = errMsg;
                    sms.StatusId = -1;
                    Db.UpdateSmsQueue(sms);

                    lblFailedSend.Invoke(new Action(() =>
                    {
                        lblFailedSend.Text = (int.Parse(lblFailedSend.Text) + 1).ToString();
                    }));

                    gridSmsQueue.Invoke(new Action(() => gridSmsQueue.Rows.RemoveAt(0)));
                }
            }
            finally
            {
                processedTextMessagesCount++;
                RecalcWhenToStop();
                isSmsQueueSendingInTheMoment = false;

                if (isSmsQueueWorking)
                {
                    timerSendSms.Change(Math.Max(0, 1100 - watch.ElapsedMilliseconds), System.Threading.Timeout.Infinite);
                }
            }

        }

        public void RefreshSmsQueue()
        {
            timerSendSms.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
            try
            {
                this.Enabled = false;

                while (isSmsQueueSendingInTheMoment)
                {
                    Application.DoEvents();
                    System.Threading.Thread.Sleep(10);
                }

                try
                {
                    DbCache.queueSms = Db.LoadSmsQueueFromDb();

                    gridSmsQueue.Rows.Clear();
                    foreach (var sms in DbCache.queueSms)
                    {
                        gridSmsQueue.Rows.Add(
                            sms.Id,
                            sms.Status,
                            sms.Recipient,
                            DbCache.allCareHomes.SingleOrDefault(s => s.Id == sms.FacilityId)?.FacilityName,
                            DbCache.allLoadedPatiensList.Where(w => w.Id == sms.PatientId)?.Select(s => new { patientName = s.FirstName + " " + s.LastName })?.SingleOrDefault()?.patientName,
                            sms.Updated.ToString("MM/dd/yyyy"),
                            sms.LongUrl,
                            sms.TextMessage
                            );
                    }

                }
                finally
                {
                    this.Enabled = true;
                }
            }
            finally
            {
                if (isSmsQueueWorking)
                {
                    timerSendSms.Change(1100, System.Threading.Timeout.Infinite);
                }
            }

            lblQueueSize.Text = gridSmsQueue.Rows.Count.ToString();
        }

        private void ToggleSmsQueue()
        {
            Invoke(new Action(() =>
            {
                processedTextMessagesCount = 0;

                if (isSmsQueueWorking)
                {
                    btnToggle.Text = "Start";
                    timerSendSms.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
                }
                else
                {
                    btnToggle.Text = "Stop";
                    timerSendSms.Change(1100, System.Threading.Timeout.Infinite);
                }

                isSmsQueueWorking = !isSmsQueueWorking;
                RecalcWhenToStop();
            }));
        }
    }
}
