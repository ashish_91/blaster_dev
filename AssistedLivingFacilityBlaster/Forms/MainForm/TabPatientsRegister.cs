﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistedLivingFacilityBlaster
{
    public partial class FormMain : Form
    {
        public void AddPatient(int defaultPatientType)
        {
            logger.Info("Add new patient");
            try
            {
                var formPatient = new Forms.FormPatient()
                {
                    mode = Forms.FormPatient.Mode.create,
                    defaultPatientType = defaultPatientType
                };

                var registerNeedRefresh = formPatient.ShowDialog();

                if (registerNeedRefresh == DialogResult.Yes)
                {
                    LoadPatiens();
                    Application.DoEvents();
                    foreach (DataGridViewRow row in gridPatients.Rows)
                    {
                        row.Selected = row.Cells[0].Value.ToString() == formPatient.patientId.ToString();
                        if (row.Selected)
                            break;
                    }
                    gridPatients.FirstDisplayedScrollingRowIndex = gridPatients.SelectedRows[0].Index;
                    PrepareSendStyle();
                    FillCombos(1);
                }

                logger.Info("  SUCCESS");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error in while adding new patient: " + ex.Message);
            }
        }
        private void btnAddPatient_Click(object sender, EventArgs e)
        {
            AddPatient(1); //AL
        }

        private List<QuestionCampaigns> allQuestioncampaigns = null;
        public void LoadQuestionCampaigns()
        {            
            allQuestioncampaigns = Db.LoadQuestionCampaigns();
            var questionCampaigns = allQuestioncampaigns.OrderByDescending(o => o.Created).Select(s => s.QuestionCampaignName).ToArray();

            cbQC_QuestionsCampaignSelect.Items.Clear();
            cbQC_QuestionsCampaignSelect.Items.AddRange(questionCampaigns);

            comboReportsQuestionCampaigns.Items.Clear();
            comboReportsQuestionCampaigns.Items.AddRange(questionCampaigns);

        }

        public void LoadPatiens()
        {
            SetProgressHint("Load the patients list...");
            try
            {
                using (var db = new DbEntities())
                {
                    DbCache.allLoadedPatiensList = db.Patients.OrderByDescending(o => o.Created).ToList();

                    logger.Info("  count: " + DbCache.allLoadedPatiensList.Count);

                    gridPatients.Rows.Clear();
                    foreach (var patient in DbCache.allLoadedPatiensList)
                    {                        
                        gridPatients.Rows.Add(
                            patient.Id,
                            patient.FirstName + " " + patient.LastName,
                            patient.Age,
                            patient.State,
                            patient.City,
                            patient.Email,
                            patient.Telephone,

                            patient.AddrZip,
                            ((patient.ALTCS == null || patient.ALTCS == 0) ? false : true),
                            patient.IsFacilityRadioSelected == 1 ? "AL" : "CH",
                            patient.Notes
                        );
                    }

                    logger.Info("  All added to the grid");
                    PrepareSendStyle();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while loading all facilities: " + ex.Message);
                logger.Error(ex);
            }
        }

        private void btnLoadPatients_Click(object sender, EventArgs e)
        {
            LoadPatiens();
        }

        private void btnEditPatient_Click(object sender, EventArgs e)
        {
            logger.Info("Edit Patient");
            try
            {

                if (gridPatients.SelectedRows.Count == 0)
                {
                    logger.Info("  Nothing selected");
                    MessageBox.Show("Please select a patient to edit!");
                    return;
                }

                var formPatient = new Forms.FormPatient();
                formPatient.mode = Forms.FormPatient.Mode.edit;
                string selectedId = gridPatients.SelectedRows[0].Cells["ColPatientId"].Value.ToString();
                formPatient.patientId = Guid.Parse(selectedId);

                var registerNeedRefresh = formPatient.ShowDialog();

                if (registerNeedRefresh == DialogResult.Yes)
                {
                    LoadPatiens();
                    foreach (DataGridViewRow row in gridPatients.Rows)
                    {
                        row.Selected = row.Cells[0].Value.ToString() == selectedId;
                        if (row.Selected) break;
                    }

                    gridPatients.FirstDisplayedScrollingRowIndex = gridPatients.SelectedRows[0].Index;
                    FillCombos(1);
                }

                logger.Info("  SUCCESS");
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Edit Patient");
                MessageBox.Show("Error occured while trying to edit the patient: " + ex.Message);
            }
        }

        private void btnDeletePatient_Click(object sender, EventArgs e)
        {
            logger.Info("Delete patient");
            try
            {
                if (gridPatients.SelectedRows.Count <= 0)
                {
                    logger.Info("  Nothing selected");
                    MessageBox.Show("Select a patient to delete!");
                    return;
                }

                DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete the selected patient?", "Delete patient", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.No)
                {
                    logger.Info("  Dont want to delete");
                    return;
                }

                SetProgressVisible(true);
                try
                {
                    using (var db = new DbEntities())
                    {
                        SetProgressHint("Get the id");
                        Guid patientId = Guid.Parse(gridPatients.SelectedRows[0].Cells["ColPatientId"].Value.ToString());
                        int selectedRow = gridPatients.SelectedRows[0].Index;

                        SetProgressHint("Deleting the patient");
                        db.Patients.Remove(db.Patients.Where(w => w.Id == patientId).Single());
                        db.SaveChanges();

                        LoadPatiens();

                        if (gridPatients.Rows.Count <= selectedRow)
                            selectedRow = gridPatients.Rows.Count - 1;

                        SetProgressHint("Select the right row...");
                        if (selectedRow >= 0)
                        {
                            foreach (DataGridViewRow row in gridPatients.Rows)
                            {
                                row.Selected = row.Index == selectedRow;
                                if (row.Selected) break;
                            }

                            if (gridPatients.SelectedRows.Count > 0)
                                gridPatients.FirstDisplayedScrollingRowIndex = gridPatients.SelectedRows[0].Index;
                        }
                        PrepareSendStyle();
                    }

                    FillCombos(1);
                }
                finally
                {
                    SetProgressVisible(false);
                }

                logger.Info("  SUCCESS");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error while deleting patient: " + ex.Message);
            }
        }
    }
}
