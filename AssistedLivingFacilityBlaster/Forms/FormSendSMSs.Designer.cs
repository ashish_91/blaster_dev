﻿namespace AssistedLivingFacilityBlaster
{
    partial class FormSendSMSs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSendSMSs = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.gridFacilities = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FacilityName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Phones = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Result = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsSendSuccess = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSmsText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.txtSmsFIxedText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblSMScharsCount = new System.Windows.Forms.Label();
            this.lblSMSCount = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblCharsToDelete = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gridFacilities)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSendSMSs
            // 
            this.btnSendSMSs.Location = new System.Drawing.Point(762, 359);
            this.btnSendSMSs.Name = "btnSendSMSs";
            this.btnSendSMSs.Size = new System.Drawing.Size(89, 23);
            this.btnSendSMSs.TabIndex = 12;
            this.btnSendSMSs.Text = "Push to queue";
            this.btnSendSMSs.UseVisualStyleBackColor = true;
            this.btnSendSMSs.Click += new System.EventHandler(this.btnSendSMSs_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(857, 359);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(289, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(258, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "List of facilities to notify (double click to follow the url):";
            // 
            // gridFacilities
            // 
            this.gridFacilities.AllowUserToAddRows = false;
            this.gridFacilities.AllowUserToDeleteRows = false;
            this.gridFacilities.AllowUserToResizeRows = false;
            this.gridFacilities.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridFacilities.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.FacilityName,
            this.Phones,
            this.Result,
            this.IsSendSuccess});
            this.gridFacilities.Location = new System.Drawing.Point(292, 32);
            this.gridFacilities.MultiSelect = false;
            this.gridFacilities.Name = "gridFacilities";
            this.gridFacilities.ReadOnly = true;
            this.gridFacilities.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridFacilities.Size = new System.Drawing.Size(640, 320);
            this.gridFacilities.TabIndex = 9;
            this.gridFacilities.DoubleClick += new System.EventHandler(this.gridFacilities_DoubleClick);
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // FacilityName
            // 
            this.FacilityName.HeaderText = "Care home name";
            this.FacilityName.Name = "FacilityName";
            this.FacilityName.ReadOnly = true;
            this.FacilityName.Width = 175;
            // 
            // Phones
            // 
            this.Phones.HeaderText = "Telehopne number";
            this.Phones.Name = "Phones";
            this.Phones.ReadOnly = true;
            this.Phones.Width = 150;
            // 
            // Result
            // 
            this.Result.HeaderText = "Result";
            this.Result.Name = "Result";
            this.Result.ReadOnly = true;
            this.Result.Width = 250;
            // 
            // IsSendSuccess
            // 
            this.IsSendSuccess.HeaderText = "IsSendSuccess";
            this.IsSendSuccess.Name = "IsSendSuccess";
            this.IsSendSuccess.ReadOnly = true;
            this.IsSendSuccess.Visible = false;
            // 
            // txtSmsText
            // 
            this.txtSmsText.Location = new System.Drawing.Point(11, 32);
            this.txtSmsText.Multiline = true;
            this.txtSmsText.Name = "txtSmsText";
            this.txtSmsText.Size = new System.Drawing.Size(275, 213);
            this.txtSmsText.TabIndex = 8;
            this.txtSmsText.TextChanged += new System.EventHandler(this.txtSmsFIxedText_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "The sms Free text:";
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 268);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(170, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Automatically added at the bottom:";
            // 
            // txtSmsFIxedText
            // 
            this.txtSmsFIxedText.Enabled = false;
            this.txtSmsFIxedText.Location = new System.Drawing.Point(11, 284);
            this.txtSmsFIxedText.Multiline = true;
            this.txtSmsFIxedText.Name = "txtSmsFIxedText";
            this.txtSmsFIxedText.Size = new System.Drawing.Size(275, 47);
            this.txtSmsFIxedText.TabIndex = 14;
            this.txtSmsFIxedText.TextChanged += new System.EventHandler(this.txtSmsFIxedText_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 334);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(213, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "SHORT_URL is unique for each care home";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(117, 364);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Total characters:";
            // 
            // lblSMScharsCount
            // 
            this.lblSMScharsCount.AutoSize = true;
            this.lblSMScharsCount.Location = new System.Drawing.Point(202, 365);
            this.lblSMScharsCount.Name = "lblSMScharsCount";
            this.lblSMScharsCount.Size = new System.Drawing.Size(13, 13);
            this.lblSMScharsCount.TabIndex = 18;
            this.lblSMScharsCount.Text = "0";
            // 
            // lblSMSCount
            // 
            this.lblSMSCount.AutoSize = true;
            this.lblSMSCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSMSCount.Location = new System.Drawing.Point(82, 364);
            this.lblSMSCount.Name = "lblSMSCount";
            this.lblSMSCount.Size = new System.Drawing.Size(14, 13);
            this.lblSMSCount.TabIndex = 20;
            this.lblSMSCount.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 364);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "SMS count:";
            // 
            // lblCharsToDelete
            // 
            this.lblCharsToDelete.AutoSize = true;
            this.lblCharsToDelete.Location = new System.Drawing.Point(427, 365);
            this.lblCharsToDelete.Name = "lblCharsToDelete";
            this.lblCharsToDelete.Size = new System.Drawing.Size(13, 13);
            this.lblCharsToDelete.TabIndex = 22;
            this.lblCharsToDelete.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(260, 365);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(167, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Characters to delete for less SMS:";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.Red;
            this.lblStatus.Location = new System.Drawing.Point(326, 174);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(60, 24);
            this.lblStatus.TabIndex = 23;
            this.lblStatus.Text = "label5";
            this.lblStatus.Visible = false;
            // 
            // FormSendSMSs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 390);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblCharsToDelete);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lblSMSCount);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblSMScharsCount);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSmsFIxedText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSendSMSs);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.gridFacilities);
            this.Controls.Add(this.txtSmsText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSendSMSs";
            this.Text = "Sending SMSs to care homes";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSendSMSs_FormClosing);
            this.Shown += new System.EventHandler(this.FormSendSMSs_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.gridFacilities)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSendSMSs;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView gridFacilities;
        private System.Windows.Forms.TextBox txtSmsText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSmsFIxedText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn FacilityName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Phones;
        private System.Windows.Forms.DataGridViewTextBoxColumn Result;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsSendSuccess;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblSMScharsCount;
        private System.Windows.Forms.Label lblSMSCount;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblCharsToDelete;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblStatus;
    }
}