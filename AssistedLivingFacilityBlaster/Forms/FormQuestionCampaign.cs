﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistedLivingFacilityBlaster.Forms
{
    public partial class FormQuestionCampaign : Form
    {
        public enum Mode
        {
            create, edit, view
        }

        public Mode mode = Mode.view;
        public Guid campaignQuestionId { get; set; }
        Logger logger = LogManager.GetLogger("questions");
        public bool doneChanges = false;

        public FormQuestionCampaign()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (doneChanges)
                DialogResult = DialogResult.OK;
            else
                DialogResult = DialogResult.No;

            Close();
        }

        private bool ValidateData()
        {
            try
            {
                txtCampaignName.Text = txtCampaignName.Text.Trim();
                if (txtCampaignName.Text.Length < 4)
                {
                    MessageBox.Show("The campaign name is too short!");
                    return false;
                }

                for (int i = 0; i < gridQuestions.Rows.Count; i++)
                {
                    var row = gridQuestions.Rows[i];

                    string questionText = row.Cells[1].Value?.ToString();

                    if (i == gridQuestions.Rows.Count - 1 && string.IsNullOrWhiteSpace(questionText))
                        continue;

                    if (string.IsNullOrWhiteSpace(row.Cells[2].Value?.ToString()))
                    {
                        MessageBox.Show($"For question no {i+1} with text '{questionText}' there is error: the question type is not selected");
                        return false;
                    }

                    if (string.IsNullOrWhiteSpace(questionText))
                    {
                        MessageBox.Show($"For question no {i+1} the text is empty!");
                        return false;
                    }
                }

                using (var db = new DbEntities())
                {
                    if (mode == Mode.create)
                    {
                        if (db.QuestionCampaigns.Any(s => s.QuestionCampaignName == txtCampaignName.Text))
                        {
                            MessageBox.Show("Campaign name with this name already exist!");
                            return false;
                        }
                    }

                    if (mode == Mode.edit)
                    {
                        if (db.QuestionCampaigns.Any(s => s.QuestionCampaignName == txtCampaignName.Text && s.Id != campaignQuestionId))
                        {
                            MessageBox.Show("Campaign name with this name already exist!");
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error while validating: " + ex.Message);
                return false;
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateData())
                    return;

                logger.Info("Saving data: ");

                logger.Info("  " + txtCampaignName.Text);

                using (var db = new DbEntities())
                {
                    if (mode == Mode.create)
                    {
                        campaignQuestionId = Guid.NewGuid();

                        db.QuestionCampaigns.Add(new QuestionCampaigns
                        {
                            Id = campaignQuestionId,
                            QuestionCampaignName = txtCampaignName.Text
                        });

                    }
                    else if (mode == Mode.edit)
                    {
                        //update title
                        db.QuestionCampaigns.Single(w => w.Id == campaignQuestionId).QuestionCampaignName = txtCampaignName.Text;

                        //remove all questions
                        db.QuestionCampaignQuestions.RemoveRange(db.QuestionCampaignQuestions.Where(w => w.QuestionCampaignId == campaignQuestionId));
                    }

                    //insert all questions, again
                    for (int i = 0; i < gridQuestions.Rows.Count; i++)
                    {
                        var row = gridQuestions.Rows[i];

                        string questionText = row.Cells[1].Value?.ToString();

                        if (i == gridQuestions.Rows.Count - 1 && string.IsNullOrWhiteSpace(questionText))
                            continue;

                        db.QuestionCampaignQuestions.Add(new QuestionCampaignQuestions()
                        {
                            Id = Guid.NewGuid(),
                            Order = i,
                            QuestionCampaignId = campaignQuestionId,
                            QuestionText = questionText,
                            QuestionTypeId = ((DataGridViewComboBoxCell)row.Cells[2]).Items.IndexOf(row.Cells[2].Value)
                        });
                    }

                    db.SaveChanges();
                    doneChanges = true;
                    logger.Info("  SUCCESS");
                    
                }

                if (mode == Mode.create)
                {
                    MessageBox.Show("The Question Campaign was created successfully!");
                    mode = Mode.view;
                    RespectMode();
                }
                else if (mode == Mode.edit)
                {
                    MessageBox.Show("The Question Campaign was updated successfully!");
                    Close();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Save Question Campaign");
                MessageBox.Show("Error occured during saving: " + ex.Message);
            }
        }

        private string ModeToString(Mode mode)
        {
            if (mode == Mode.create) return "CREATE NEW";
            if (mode == Mode.edit) return "EDIT";
            if (mode == Mode.view) return "VIEW";

            return "UNKNOWN";
        }

        private void RespectMode()
        {
            this.Text = $"Question Campaign ({ModeToString(mode)})";

            txtCampaignName.Enabled = mode != Mode.view;

            if (mode == Mode.create)
            {
                btnDelete.Visible = false;
                gridQuestions.Enabled = true;
                txtCampaignName.Enabled = true;
            }

            if (mode == Mode.view)
            {
                btnDelete.Visible = true;
                btnSave.Visible = false;
                btnClearForm.Visible = false;
                gridQuestions.Enabled = false;
                txtCampaignName.Enabled = false;
                contextMenuStrip1.Enabled = false;
            }

            if (mode == Mode.edit)
            {
                btnDelete.Visible = true;
                btnSave.Visible = true;
                btnClearForm.Visible = true;
                gridQuestions.Enabled = true;
                txtCampaignName.Enabled = true;
            }
        }

        private void FormQuestionCampaign_Load(object sender, EventArgs e)
        {
            RespectMode();
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Enabled = false;
            lblProgress.Visible = true;
            progressBar.Visible = true;
            try
            {
                timer1.Enabled = false;

                if (mode == Mode.edit || mode == Mode.view)
                {
                    SetProgressHint("Loading Question Campaign info...");
                    using (var db = new DbEntities())
                    {
                        var questionCampaign = db.QuestionCampaigns.Single(s => s.Id == campaignQuestionId);
                        var questions = db.QuestionCampaignQuestions.Where(w => w.QuestionCampaignId == campaignQuestionId).OrderBy(o => o.Order);

                        txtCampaignName.Text = questionCampaign.QuestionCampaignName;

                        gridQuestions.Rows.Clear();
                        foreach(var question in questions)
                        {
                            gridQuestions.Rows.Add(
                                question.Id,
                                question.QuestionText,
                                (question.QuestionTypeId == 0 ? "Yes/No Question" : 
                                        (question.QuestionTypeId == 1 ? "Opened (Free text)" : throw new NotImplementedException($"Invalid QuestionTypeId: {question.QuestionTypeId} in questiom campaign id {question.QuestionCampaignId}")
                                ))
                                );
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error while loading the form: " + ex.Message);
            }
            finally
            {
                this.Enabled = true;
                progressBar.Visible = false;
                lblProgress.Visible = false;
            }
        }

        private void SetProgressHint(string hint)
        {
            logger.Info(hint);
            lblProgress.Text = hint;
            Application.DoEvents();
        }

        private void btnClearForm_Click(object sender, EventArgs e)
        {
            txtCampaignName.Text = "";
        }

        private void FormQuestionCampaign_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (doneChanges)
                DialogResult = DialogResult.OK;
            else
                DialogResult = DialogResult.No;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete the selected Questions Campaign?", "Delete Question Campaign", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.No)
                {
                    return;
                }

                try
                {
                    using (var db = new DbEntities())
                    {
                        SetProgressHint("Deleting the Question Campaign");
                        db.QuestionCampaigns.Remove(
                            db.QuestionCampaigns.Where(w => w.Id == campaignQuestionId).Single());

                        db.QuestionCampaignQuestions.RemoveRange(
                            db.QuestionCampaignQuestions.Where(w => w.QuestionCampaignId == campaignQuestionId));

                        db.SaveChanges();
                    }

                    MessageBox.Show("The Question campaign has been successfuly deleted!");
                    doneChanges = true;                    
                    Close();
                }
                finally
                {

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error while deleting Question Campaign: " + ex.Message);
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            DataGridViewRow selectedRow = GetSelectedRow(true);
            menuDeleteRow.Enabled = selectedRow != null;
            menuMoveUp.Enabled = selectedRow != null && selectedRow.Index > 0;
            menuMoveDown.Enabled = selectedRow != null && selectedRow.Index < gridQuestions.RowCount - 2;
        }

        private void btnDeleteRow_Click(object sender, EventArgs e)
        {
            DataGridViewRow selectedRow = GetSelectedRow();
            if (selectedRow == null) return;
        }

        private void DeleteTheSelectedRow()
        {
            DataGridViewRow selectedRow = GetSelectedRow();
            if (selectedRow == null) return;

            gridQuestions.Rows.RemoveAt(selectedRow.Index);
        }

        private DataGridViewRow GetSelectedRow(bool supressMessages = false)
        {
            DataGridViewRow selectedRow = null;
            if (gridQuestions.SelectedCells.Count <= 0 || (selectedRow = gridQuestions.Rows[gridQuestions.SelectedCells[0].RowIndex]).IsNewRow )
            {
                if (!supressMessages) MessageBox.Show("Select a cell, please");
                return null;
            }

            return selectedRow;
        }

        private void menuDeleteRow_Click(object sender, EventArgs e)
        {
            DeleteTheSelectedRow();
        }

        private void menuMoveUp_Click(object sender, EventArgs e)
        {
            DataGridViewRow selectedRow = GetSelectedRow();
            DataGridViewRow upperRow = gridQuestions.Rows[selectedRow.Index - 1];
            gridQuestions.Rows.Remove(selectedRow);
            gridQuestions.Rows.Insert(upperRow.Index, selectedRow);


        }

        private void menuMoveDown_Click(object sender, EventArgs e)
        {
            DataGridViewRow selectedRow = GetSelectedRow();
            DataGridViewRow bellowRow = gridQuestions.Rows[selectedRow.Index + 1];
            gridQuestions.Rows.Remove(selectedRow);
            gridQuestions.Rows.Insert(bellowRow.Index + 1, selectedRow);
        }
    }
}
