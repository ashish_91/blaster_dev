﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistedLivingFacilityBlaster.Forms
{
    public partial class FormPatient : Form
    {
        public enum Mode
        {
            create, edit, view
        }

        public Mode mode = Mode.view;
        public Guid patientId { get; set; }
        Logger logger = LogManager.GetLogger("patientForm");
        public bool doneChanges = false;

        //1 - AL   2 - CH
        public int defaultPatientType = 1;

        public FormPatient()
        {
            InitializeComponent();
        }

        private void btnClearForm_Click(object sender, EventArgs e)
        {
            logger.Info("== Button Clear form pressed ==");

            edPatientFirstName.Text = "";
            edPatientLastName.Text = "";
            edPatientAge.Value = 55;
            edContactFirstName.Text = "";
            edContactLastName.Text = "";            
            edTelephone.Text = "";
            edEmail.Text = "";
            chkAssistedLiving.Checked = false;
            chkIndependentLiving.Checked = false;
            chkMemoryCare.Checked = false;
            chkSkilledNursingCare.Checked = false;
            rbALTCSYes.Checked = false;
            rbALTCSNo.Checked = true;            
            txtZip.Text = "";
            cbAddressState.SelectedText = "Arizona";
            cbAddressCity.SelectedIndex = -1;

            rbFacilities.Checked = defaultPatientType == 1;
            rbCareHomes.Checked = defaultPatientType == 2;
        }
        
        private void RefreshCities()
        {
            try
            {
                logger.Info("RefreshCities");
                cbAddressCity.Items.Clear();
                logger.Info("  items cleared");
                var cities = DbCache.fullCache
                        .Where(w => w.state == cbAddressState.Text)
                        .Select(s => s.city)
                        .Distinct()
                        .OrderBy(o => o)
                        .ToArray();
                logger.Info("  cities loaded from the db cache");

                cbAddressCity.Items.AddRange(cities);
                logger.Info("  added to the combo box");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error in RefreshCities: " + ex.Message);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {            
            this.Enabled = false;
            lblProgress.Visible = true;
            progressBar.Visible = true;
            try
            {
                timer1.Enabled = false;

                SetProgressHint("Load zips...");                
                SetProgressHint("Load states...");
                cbAddressState.Items.AddRange(DbCache.states.ToArray());
                cbAddressState.SelectedIndex = cbAddressState.Items.IndexOf("Arizona");

                if (mode == Mode.edit || mode == Mode.view)
                {
                    SetProgressHint("Load patient info...");
                    using (var db = new DbEntities())
                    {
                        var patient = db.Patients.Where(w => w.Id == patientId).SingleOrDefault();

                        if (patient == null)
                        {
                            MessageBox.Show("No databse info for that patient? That's very odd. Patient Id: " + patientId);
                            logger.Error("No databse info for that patient? That's very odd. Patient Id: " + patientId);
                            return;
                        }

                        edPatientFirstName.Text = patient.FirstName;
                        edPatientLastName.Text = patient.LastName;
                        edPatientAge.Value = patient.Age ?? 0;
                        edContactFirstName.Text = patient.PocFirstName;
                        edContactLastName.Text = patient.PocLastName;
                        edEmail.Text = patient.Email;
                        edTelephone.Text = patient.Telephone;
                        txtPatientNotes.Text = patient.Notes;
                        rbFacilities.Checked = (patient.IsFacilityRadioSelected == null || patient.IsFacilityRadioSelected == 1);
                        rbCareHomes.Checked = !rbFacilities.Checked;
                        chkIndependentLiving.Checked = patient.FacIndipendentLiving == 1;
                        chkAssistedLiving.Checked = patient.FacAssistedLiving == 1;
                        chkCareHomes.Checked = patient.CareHome == 1;
                        chkMemoryCare.Checked = patient.FacMemoryCare == 1;
                        chkSkilledNursingCare.Checked = patient.FacSkilledNursingCare == 1;
                        rbALTCSYes.Checked = patient.ALTCS == 1;
                        rbALTCSNo.Checked = !rbALTCSYes.Checked;
                        txtPrivateRoomPrice.Value = patient.PrivateRoomBudget ?? 0;
                        txtSemiPrivateRoomPrice.Value = patient.SemiPrivateRoomBudget ?? 0;
                        txtBudgetForCareHomes.Value = patient.BudgetForCareHomes ?? 0;
                        txtAssistedLivingBudget.Value = patient.AssistedLivingBudget ?? 0;
                        edAddressStreet.Text = patient.Street;
                        txtAddressCity.Text = patient.City;
                        cbAddressState.Text = patient.State;
                        txtZip.Text = patient.AddrZip;

                        rbCareHomes_CheckedChanged(sender, e);
                    }
                } else if (mode == Mode.create)
                {
                    rbFacilities.Checked = defaultPatientType == 1;
                    rbCareHomes.Checked = defaultPatientType == 2;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error while loading the form: " + ex.Message);
            }
            finally
            {
                this.Enabled = true;
                progressBar.Visible = false;
                lblProgress.Visible = false;
            }
        }

        private void SetProgressHint(string hint)
        {
            logger.Info(hint);
            lblProgress.Text = hint;
            Application.DoEvents();
        }

        private void txtZip_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtZip.Text.Length == 5)
            {
                string zip = txtZip.Text.Trim();
                var cacheObj = DbCache.fullCache.AsParallel().Where(w => w.zip == zip).FirstOrDefault();
                if (cacheObj == null)
                {
                    MessageBox.Show("Zip not found in the database!");
                    return;
                }

                cbAddressState.SelectedIndex = cbAddressState.Items.IndexOf(cacheObj.state);
                txtAddressCity.Text = cacheObj.city;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (doneChanges) DialogResult = DialogResult.Yes;
            else
                DialogResult = DialogResult.No;

            Close();
        }

        private string ModeToString(Mode mode)
        {
            if (mode == Mode.create) return "CREATE NEW";
            if (mode == Mode.edit) return "EDIT";
            if (mode == Mode.view) return "VIEW";

            return "UNKNOWN";
        }

        private void RespectMode()
        {
            this.Text = $"Patient Details ({ModeToString(mode)})";

            edPatientFirstName.Enabled = mode != Mode.view;
            edPatientLastName.Enabled = mode != Mode.view;
            edPatientAge.Enabled = mode != Mode.view;
            edContactFirstName.Enabled = mode != Mode.view;
            edContactLastName.Enabled = mode != Mode.view;
            edEmail.Enabled = mode != Mode.view;
            edTelephone.Enabled = mode != Mode.view;
            txtPatientNotes.Enabled = mode != Mode.view;
            rbFacilities.Enabled = mode != Mode.view;
            rbCareHomes.Enabled = mode != Mode.view;
            chkIndependentLiving.Enabled = mode != Mode.view;
            chkAssistedLiving.Enabled = mode != Mode.view;
            chkCareHomes.Enabled = mode != Mode.view;
            chkMemoryCare.Enabled = mode != Mode.view;
            chkSkilledNursingCare.Enabled = mode != Mode.view;
            rbALTCSYes.Enabled = mode != Mode.view;
            rbALTCSNo.Enabled = mode != Mode.view;
            txtSemiPrivateRoomPrice.Enabled = mode != Mode.view;
            txtPrivateRoomPrice.Enabled = mode != Mode.view;
            txtBudgetForCareHomes.Enabled = mode != Mode.view;
            txtAssistedLivingBudget.Enabled = mode != Mode.view;

            if (mode == Mode.create)
            {                
                btnDelete.Visible = false;
            }

            if (mode == Mode.view)
            {
                btnDelete.Visible = true;
                btnSave.Visible = false;
                btnClearForm.Visible = false;
            }

            if (mode == Mode.edit)
            {
                btnDelete.Visible = true;
                btnSave.Visible = true;
                btnClearForm.Visible = true;
            }
        }

        private bool ValidateData()
        {
            try
            {
                if (string.IsNullOrEmpty(edPatientFirstName.Text))
                {
                    MessageBox.Show("Please enter the patient first name!");
                    if (edPatientFirstName.CanFocus) edPatientFirstName.Focus();
                    return false;
                }

                if (string.IsNullOrEmpty(edPatientLastName.Text))
                {
                    MessageBox.Show("Please enter the patient last name!");
                    if (edPatientLastName.CanFocus) edPatientLastName.Focus();
                    return false;
                }

                string zip = txtZip.Text.Trim();
                var cacheObj = DbCache.fullCache.AsParallel().Where(w => w.zip == zip).FirstOrDefault();
                if (cacheObj == null)
                {
                    DialogResult dialogResult = MessageBox.Show("The zip is not found in the database. Adding it anyway?", "No valid zip?", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.No)
                    {
                        return false;
                    }

                    if (txtZip.CanFocus) txtZip.Focus();
                }

                if (!rbALTCSNo.Checked && !rbALTCSYes.Checked)
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure you want to save the patient with no ALTCS selected?", "No ALTCS?", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.No)
                    {
                        return false;
                    }
                }
                
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error while validating: " + ex.Message);
                return false;
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateData())
                    return;

                logger.Info("Saving data: ");
                logger.Info("  " + edAddressStreet.Text);                
                logger.Info("  " + cbAddressState.Text);                
                logger.Info("  " + txtZip.Text);                

                using (var db = new DbEntities())
                {
                    Guid id = Guid.Empty;

                    if (mode == Mode.create)
                    {
                        id = Guid.NewGuid();
                        patientId = id;
                        
                        var pat = new Patients()
                        {
                            AddrZip = txtZip.Text,
                            Age = (int)edPatientAge.Value,
                            CareHome = chkCareHomes.Checked ? 1 : 0,
                            City = txtAddressCity.Text,
                            Created = DateTime.Now,
                            Email = edEmail.Text,
                            FacAssistedLiving = chkAssistedLiving.Checked ? 1 : 0,
                            FacIndipendentLiving = chkIndependentLiving.Checked ? 1 : 0,
                            FacMemoryCare = chkMemoryCare.Checked ? 1 : 0,
                            FacSkilledNursingCare = chkSkilledNursingCare.Checked ? 1 : 0,
                            FirstName = edPatientFirstName.Text,
                            Id = id,
                            IsFacilityRadioSelected = rbFacilities.Checked ? 1 : 0,
                            LastName = edPatientLastName.Text,
                            Notes = txtPatientNotes.Text,
                            PocFirstName = edContactFirstName.Text,
                            PocLastName = edContactLastName.Text,
                            State = cbAddressState.Text,
                            Street = edAddressStreet.Text,
                            Telephone = edTelephone.Text,
                            PrivateRoomBudget = (txtPrivateRoomPrice.Value == 0 ? null : (int?)txtPrivateRoomPrice.Value),
                            SemiPrivateRoomBudget = (txtSemiPrivateRoomPrice.Value == 0 ? null : (int?)txtSemiPrivateRoomPrice.Value),
                            BudgetForCareHomes = (txtBudgetForCareHomes.Value == 0 ? null : (int?)txtBudgetForCareHomes.Value),
                            AssistedLivingBudget = (txtAssistedLivingBudget.Value == 0 ? null : (int?)txtAssistedLivingBudget.Value),

                        };

                        if (rbALTCSYes.Checked)
                        {
                            pat.ALTCS = 1;
                        }
                        else if (rbALTCSNo.Checked)
                        {
                            pat.ALTCS = 0;
                        }                        

                        db.Patients.Add(pat);
                    }
                    else
                    if (mode == Mode.edit)
                    {
                        id = patientId;

                        var pat = db.Patients.Where(w => w.Id == patientId).Single();

                        if (rbALTCSYes.Checked)
                        {
                            pat.ALTCS = 1;
                        }
                        else if (rbALTCSNo.Checked)
                        {
                            pat.ALTCS = 0;
                        }

                        pat.AddrZip = txtZip.Text;
                        pat.Age = (int)edPatientAge.Value;
                        pat.CareHome = chkCareHomes.Checked ? 1 : 0;
                        pat.City = txtAddressCity.Text;
                        pat.Created = DateTime.Now;
                        pat.Email = edEmail.Text;
                        pat.FacAssistedLiving = chkAssistedLiving.Checked ? 1 : 0;
                        pat.FacIndipendentLiving = chkIndependentLiving.Checked ? 1 : 0;
                        pat.FacMemoryCare = chkMemoryCare.Checked ? 1 : 0;
                        pat.FacSkilledNursingCare = chkSkilledNursingCare.Checked ? 1 : 0;
                        pat.FirstName = edPatientFirstName.Text;
                        pat.Id = id;
                        pat.IsFacilityRadioSelected = rbFacilities.Checked ? 1 : 0;
                        pat.LastName = edPatientLastName.Text;
                        pat.Notes = txtPatientNotes.Text;
                        pat.PocFirstName = edContactFirstName.Text;
                        pat.PocLastName = edContactLastName.Text;
                        pat.State = cbAddressState.Text;
                        pat.Street = edAddressStreet.Text;
                        pat.Telephone = edTelephone.Text;
                        pat.PrivateRoomBudget = (txtPrivateRoomPrice.Value <= 0 ? null : (int?)txtPrivateRoomPrice.Value);
                        pat.SemiPrivateRoomBudget = (txtSemiPrivateRoomPrice.Value <= 0 ? null : (int?)txtSemiPrivateRoomPrice.Value);
                        pat.BudgetForCareHomes = (txtBudgetForCareHomes.Value <= 0 ? null : (int?)txtBudgetForCareHomes.Value);
                        pat.AssistedLivingBudget = (txtAssistedLivingBudget.Value <= 0 ? null : (int?)txtAssistedLivingBudget.Value);
                    }
                    
                    db.SaveChanges();
                    doneChanges = true;
                    logger.Info("  SUCCESS");
                }

                if (mode == Mode.create)
                {
                    Close();                    
                }
                else if (mode == Mode.edit)
                {                    
                    Close();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Save patient");
                MessageBox.Show("Error occured during saving: " + ex.Message);
            }
        }

        private void FormPatient_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (doneChanges) DialogResult = DialogResult.Yes;
            else
                DialogResult = DialogResult.No;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete the selected patient?", "Delete patient", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.No)
                {
                    return;
                }

                try
                {
                    using (var db = new DbEntities())
                    {
                        SetProgressHint("Get the id");

                        SetProgressHint("Deleting the facility");
                        db.Patients.Remove(db.Patients.Where(w => w.Id == patientId).Single());                        

                        db.SaveChanges();
                    }

                    MessageBox.Show("The patient has been successfuly deleted!");
                    doneChanges = true;
                    Close();
                }
                finally
                {

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("Error while deleting patient: " + ex.Message);
            }
        }

        private void FormPatient_Load(object sender, EventArgs e)
        {            
            RespectMode();
        }

        private void rbCareHomes_CheckedChanged(object sender, EventArgs e)
        {
            gbCareHomes.Enabled = !rbFacilities.Checked;
            gbFacilities.Enabled = rbFacilities.Checked;

            groupCareHomePrices.Visible = !rbFacilities.Checked;
            groupAssistedLivingPrices.Visible = rbFacilities.Checked;
        }

        private void FormPatient_Shown(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This is what the care home will see as the patient budget.\nA quote:\"a budget that the care homes see. I don't want them to see the search parameters. For instance if I want care homes under 2500 for a semi private or a private, I would like to send them a budget of 1800.00 to see if they will accept. Kind of like a negotiation.\"");
        }
    }
}