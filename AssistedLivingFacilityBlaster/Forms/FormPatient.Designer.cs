﻿namespace AssistedLivingFacilityBlaster.Forms
{
    partial class FormPatient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.txtPatientNotes = new System.Windows.Forms.TextBox();
            this.rbFacilities = new System.Windows.Forms.RadioButton();
            this.rbCareHomes = new System.Windows.Forms.RadioButton();
            this.gbCareHomes = new System.Windows.Forms.GroupBox();
            this.chkCareHomes = new System.Windows.Forms.CheckBox();
            this.btnClearForm = new System.Windows.Forms.Button();
            this.gbFacilities = new System.Windows.Forms.GroupBox();
            this.chkSkilledNursingCare = new System.Windows.Forms.CheckBox();
            this.chkMemoryCare = new System.Windows.Forms.CheckBox();
            this.chkAssistedLiving = new System.Windows.Forms.CheckBox();
            this.chkIndependentLiving = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rbALTCSYes = new System.Windows.Forms.RadioButton();
            this.rbALTCSNo = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.edTelephone = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.edEmail = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtZip = new System.Windows.Forms.TextBox();
            this.txtAddressCity = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbAddressState = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbAddressCity = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.edAddressStreet = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.edContactLastName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.edContactFirstName = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.edPatientAge = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.edPatientLastName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.edPatientFirstName = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblProgress = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPrivateRoomPrice = new System.Windows.Forms.NumericUpDown();
            this.txtSemiPrivateRoomPrice = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupCareHomePrices = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtBudgetForCareHomes = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.groupAssistedLivingPrices = new System.Windows.Forms.GroupBox();
            this.txtAssistedLivingBudget = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox10.SuspendLayout();
            this.gbCareHomes.SuspendLayout();
            this.gbFacilities.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edPatientAge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrivateRoomPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSemiPrivateRoomPrice)).BeginInit();
            this.groupCareHomePrices.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBudgetForCareHomes)).BeginInit();
            this.groupAssistedLivingPrices.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssistedLivingBudget)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this.txtPatientNotes);
            this.groupBox10.Location = new System.Drawing.Point(363, 2);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(337, 128);
            this.groupBox10.TabIndex = 43;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Patient Notes:";
            // 
            // txtPatientNotes
            // 
            this.txtPatientNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPatientNotes.Location = new System.Drawing.Point(7, 16);
            this.txtPatientNotes.Multiline = true;
            this.txtPatientNotes.Name = "txtPatientNotes";
            this.txtPatientNotes.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPatientNotes.Size = new System.Drawing.Size(324, 106);
            this.txtPatientNotes.TabIndex = 0;
            // 
            // rbFacilities
            // 
            this.rbFacilities.AutoSize = true;
            this.rbFacilities.Checked = true;
            this.rbFacilities.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbFacilities.Location = new System.Drawing.Point(370, 168);
            this.rbFacilities.Name = "rbFacilities";
            this.rbFacilities.Size = new System.Drawing.Size(125, 17);
            this.rbFacilities.TabIndex = 37;
            this.rbFacilities.TabStop = true;
            this.rbFacilities.Text = "Assisted living (eMail)";
            this.rbFacilities.UseVisualStyleBackColor = true;
            // 
            // rbCareHomes
            // 
            this.rbCareHomes.AutoSize = true;
            this.rbCareHomes.Location = new System.Drawing.Point(513, 168);
            this.rbCareHomes.Name = "rbCareHomes";
            this.rbCareHomes.Size = new System.Drawing.Size(108, 17);
            this.rbCareHomes.TabIndex = 38;
            this.rbCareHomes.TabStop = true;
            this.rbCareHomes.Text = "Care homes (sms)";
            this.rbCareHomes.UseVisualStyleBackColor = true;
            this.rbCareHomes.CheckedChanged += new System.EventHandler(this.rbCareHomes_CheckedChanged);
            // 
            // gbCareHomes
            // 
            this.gbCareHomes.Controls.Add(this.chkCareHomes);
            this.gbCareHomes.Enabled = false;
            this.gbCareHomes.Location = new System.Drawing.Point(507, 170);
            this.gbCareHomes.Name = "gbCareHomes";
            this.gbCareHomes.Size = new System.Drawing.Size(112, 116);
            this.gbCareHomes.TabIndex = 42;
            this.gbCareHomes.TabStop = false;
            // 
            // chkCareHomes
            // 
            this.chkCareHomes.AutoSize = true;
            this.chkCareHomes.Checked = true;
            this.chkCareHomes.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCareHomes.Location = new System.Drawing.Point(6, 22);
            this.chkCareHomes.Name = "chkCareHomes";
            this.chkCareHomes.Size = new System.Drawing.Size(82, 17);
            this.chkCareHomes.TabIndex = 0;
            this.chkCareHomes.Text = "Care homes";
            this.chkCareHomes.UseVisualStyleBackColor = true;
            // 
            // btnClearForm
            // 
            this.btnClearForm.Location = new System.Drawing.Point(9, 415);
            this.btnClearForm.Name = "btnClearForm";
            this.btnClearForm.Size = new System.Drawing.Size(75, 23);
            this.btnClearForm.TabIndex = 41;
            this.btnClearForm.Text = "Clear form";
            this.btnClearForm.UseVisualStyleBackColor = true;
            this.btnClearForm.Click += new System.EventHandler(this.btnClearForm_Click);
            // 
            // gbFacilities
            // 
            this.gbFacilities.Controls.Add(this.chkSkilledNursingCare);
            this.gbFacilities.Controls.Add(this.chkMemoryCare);
            this.gbFacilities.Controls.Add(this.chkAssistedLiving);
            this.gbFacilities.Controls.Add(this.chkIndependentLiving);
            this.gbFacilities.Location = new System.Drawing.Point(364, 170);
            this.gbFacilities.Name = "gbFacilities";
            this.gbFacilities.Size = new System.Drawing.Size(137, 116);
            this.gbFacilities.TabIndex = 36;
            this.gbFacilities.TabStop = false;
            // 
            // chkSkilledNursingCare
            // 
            this.chkSkilledNursingCare.AutoSize = true;
            this.chkSkilledNursingCare.Checked = true;
            this.chkSkilledNursingCare.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSkilledNursingCare.Location = new System.Drawing.Point(6, 91);
            this.chkSkilledNursingCare.Name = "chkSkilledNursingCare";
            this.chkSkilledNursingCare.Size = new System.Drawing.Size(118, 17);
            this.chkSkilledNursingCare.TabIndex = 3;
            this.chkSkilledNursingCare.Text = "Skilled nursing care";
            this.chkSkilledNursingCare.UseVisualStyleBackColor = true;
            // 
            // chkMemoryCare
            // 
            this.chkMemoryCare.AutoSize = true;
            this.chkMemoryCare.Checked = true;
            this.chkMemoryCare.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMemoryCare.Location = new System.Drawing.Point(6, 45);
            this.chkMemoryCare.Name = "chkMemoryCare";
            this.chkMemoryCare.Size = new System.Drawing.Size(87, 17);
            this.chkMemoryCare.TabIndex = 2;
            this.chkMemoryCare.Text = "Memory care";
            this.chkMemoryCare.UseVisualStyleBackColor = true;
            // 
            // chkAssistedLiving
            // 
            this.chkAssistedLiving.AutoSize = true;
            this.chkAssistedLiving.Checked = true;
            this.chkAssistedLiving.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAssistedLiving.Location = new System.Drawing.Point(6, 68);
            this.chkAssistedLiving.Name = "chkAssistedLiving";
            this.chkAssistedLiving.Size = new System.Drawing.Size(92, 17);
            this.chkAssistedLiving.TabIndex = 1;
            this.chkAssistedLiving.Text = "Assisted living";
            this.chkAssistedLiving.UseVisualStyleBackColor = true;
            // 
            // chkIndependentLiving
            // 
            this.chkIndependentLiving.AutoSize = true;
            this.chkIndependentLiving.Checked = true;
            this.chkIndependentLiving.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIndependentLiving.Location = new System.Drawing.Point(6, 22);
            this.chkIndependentLiving.Name = "chkIndependentLiving";
            this.chkIndependentLiving.Size = new System.Drawing.Size(113, 17);
            this.chkIndependentLiving.TabIndex = 0;
            this.chkIndependentLiving.Text = "Independent living";
            this.chkIndependentLiving.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rbALTCSYes);
            this.groupBox5.Controls.Add(this.rbALTCSNo);
            this.groupBox5.Location = new System.Drawing.Point(625, 171);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(71, 115);
            this.groupBox5.TabIndex = 39;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "ALTCS";
            // 
            // rbALTCSYes
            // 
            this.rbALTCSYes.AutoSize = true;
            this.rbALTCSYes.Location = new System.Drawing.Point(12, 20);
            this.rbALTCSYes.Name = "rbALTCSYes";
            this.rbALTCSYes.Size = new System.Drawing.Size(43, 17);
            this.rbALTCSYes.TabIndex = 6;
            this.rbALTCSYes.TabStop = true;
            this.rbALTCSYes.Text = "Yes";
            this.rbALTCSYes.UseVisualStyleBackColor = true;
            // 
            // rbALTCSNo
            // 
            this.rbALTCSNo.AutoSize = true;
            this.rbALTCSNo.Checked = true;
            this.rbALTCSNo.Location = new System.Drawing.Point(12, 44);
            this.rbALTCSNo.Name = "rbALTCSNo";
            this.rbALTCSNo.Size = new System.Drawing.Size(39, 17);
            this.rbALTCSNo.TabIndex = 7;
            this.rbALTCSNo.TabStop = true;
            this.rbALTCSNo.Text = "No";
            this.rbALTCSNo.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.edTelephone);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.edEmail);
            this.groupBox4.Location = new System.Drawing.Point(10, 322);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(332, 83);
            this.groupBox4.TabIndex = 40;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Contacts:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(152, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Telephone:";
            // 
            // edTelephone
            // 
            this.edTelephone.Location = new System.Drawing.Point(155, 40);
            this.edTelephone.Name = "edTelephone";
            this.edTelephone.Size = new System.Drawing.Size(120, 20);
            this.edTelephone.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "eMail:";
            // 
            // edEmail
            // 
            this.edEmail.Location = new System.Drawing.Point(17, 40);
            this.edEmail.Name = "edEmail";
            this.edEmail.Size = new System.Drawing.Size(120, 20);
            this.edEmail.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtZip);
            this.groupBox3.Controls.Add(this.txtAddressCity);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.cbAddressState);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.cbAddressCity);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.edAddressStreet);
            this.groupBox3.Location = new System.Drawing.Point(10, 175);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(332, 132);
            this.groupBox3.TabIndex = 35;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Address::";
            // 
            // txtZip
            // 
            this.txtZip.Location = new System.Drawing.Point(17, 39);
            this.txtZip.MaxLength = 5;
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(131, 20);
            this.txtZip.TabIndex = 16;
            this.txtZip.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtZip_KeyUp);
            // 
            // txtAddressCity
            // 
            this.txtAddressCity.Enabled = false;
            this.txtAddressCity.Location = new System.Drawing.Point(17, 93);
            this.txtAddressCity.Name = "txtAddressCity";
            this.txtAddressCity.Size = new System.Drawing.Size(131, 20);
            this.txtAddressCity.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Zip:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(167, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "State:";
            // 
            // cbAddressState
            // 
            this.cbAddressState.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbAddressState.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbAddressState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAddressState.Enabled = false;
            this.cbAddressState.FormattingEnabled = true;
            this.cbAddressState.Location = new System.Drawing.Point(170, 38);
            this.cbAddressState.Name = "cbAddressState";
            this.cbAddressState.Size = new System.Drawing.Size(146, 21);
            this.cbAddressState.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 77);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "City:";
            // 
            // cbAddressCity
            // 
            this.cbAddressCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbAddressCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbAddressCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAddressCity.Enabled = false;
            this.cbAddressCity.FormattingEnabled = true;
            this.cbAddressCity.Location = new System.Drawing.Point(17, 93);
            this.cbAddressCity.Name = "cbAddressCity";
            this.cbAddressCity.Size = new System.Drawing.Size(131, 21);
            this.cbAddressCity.TabIndex = 13;
            this.cbAddressCity.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(167, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Street:";
            // 
            // edAddressStreet
            // 
            this.edAddressStreet.Location = new System.Drawing.Point(170, 93);
            this.edAddressStreet.Name = "edAddressStreet";
            this.edAddressStreet.Size = new System.Drawing.Size(146, 20);
            this.edAddressStreet.TabIndex = 14;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.edContactLastName);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.edContactFirstName);
            this.groupBox2.Location = new System.Drawing.Point(10, 85);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(332, 77);
            this.groupBox2.TabIndex = 34;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Patient Point of Contact:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(176, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Last Name:";
            // 
            // edContactLastName
            // 
            this.edContactLastName.Location = new System.Drawing.Point(179, 40);
            this.edContactLastName.Name = "edContactLastName";
            this.edContactLastName.Size = new System.Drawing.Size(137, 20);
            this.edContactLastName.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "First Name:";
            // 
            // edContactFirstName
            // 
            this.edContactFirstName.Location = new System.Drawing.Point(17, 40);
            this.edContactFirstName.Name = "edContactFirstName";
            this.edContactFirstName.Size = new System.Drawing.Size(144, 20);
            this.edContactFirstName.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.edPatientAge);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.edPatientLastName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.edPatientFirstName);
            this.groupBox1.Location = new System.Drawing.Point(10, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(332, 77);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Patient Name:";
            // 
            // edPatientAge
            // 
            this.edPatientAge.Location = new System.Drawing.Point(259, 40);
            this.edPatientAge.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.edPatientAge.Name = "edPatientAge";
            this.edPatientAge.Size = new System.Drawing.Size(57, 20);
            this.edPatientAge.TabIndex = 6;
            this.edPatientAge.Value = new decimal(new int[] {
            55,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(258, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Age:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(138, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Last Name:";
            // 
            // edPatientLastName
            // 
            this.edPatientLastName.Location = new System.Drawing.Point(138, 40);
            this.edPatientLastName.Name = "edPatientLastName";
            this.edPatientLastName.Size = new System.Drawing.Size(105, 20);
            this.edPatientLastName.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "First Name:";
            // 
            // edPatientFirstName
            // 
            this.edPatientFirstName.Location = new System.Drawing.Point(17, 40);
            this.edPatientFirstName.Name = "edPatientFirstName";
            this.edPatientFirstName.Size = new System.Drawing.Size(104, 20);
            this.edPatientFirstName.TabIndex = 0;
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblProgress
            // 
            this.lblProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblProgress.AutoSize = true;
            this.lblProgress.Location = new System.Drawing.Point(100, 420);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(58, 13);
            this.lblProgress.TabIndex = 45;
            this.lblProgress.Text = "lblProgress";
            this.lblProgress.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(90, 415);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(358, 23);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.TabIndex = 44;
            this.progressBar.Visible = false;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Location = new System.Drawing.Point(454, 415);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 48;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(539, 415);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 47;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(625, 415);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 46;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(370, 148);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(168, 13);
            this.label12.TabIndex = 49;
            this.label12.Text = "Patient search prefferences:";
            // 
            // txtPrivateRoomPrice
            // 
            this.txtPrivateRoomPrice.Location = new System.Drawing.Point(14, 34);
            this.txtPrivateRoomPrice.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtPrivateRoomPrice.Name = "txtPrivateRoomPrice";
            this.txtPrivateRoomPrice.Size = new System.Drawing.Size(137, 20);
            this.txtPrivateRoomPrice.TabIndex = 13;
            this.txtPrivateRoomPrice.ThousandsSeparator = true;
            // 
            // txtSemiPrivateRoomPrice
            // 
            this.txtSemiPrivateRoomPrice.Location = new System.Drawing.Point(14, 78);
            this.txtSemiPrivateRoomPrice.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtSemiPrivateRoomPrice.Name = "txtSemiPrivateRoomPrice";
            this.txtSemiPrivateRoomPrice.Size = new System.Drawing.Size(137, 20);
            this.txtSemiPrivateRoomPrice.TabIndex = 13;
            this.txtSemiPrivateRoomPrice.ThousandsSeparator = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(117, 13);
            this.label13.TabIndex = 52;
            this.label13.Text = "Private room budget ($)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 62);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(143, 13);
            this.label14.TabIndex = 53;
            this.label14.Text = "Semi-Private room budget ($)";
            // 
            // groupCareHomePrices
            // 
            this.groupCareHomePrices.Controls.Add(this.button1);
            this.groupCareHomePrices.Controls.Add(this.txtBudgetForCareHomes);
            this.groupCareHomePrices.Controls.Add(this.label15);
            this.groupCareHomePrices.Controls.Add(this.txtSemiPrivateRoomPrice);
            this.groupCareHomePrices.Controls.Add(this.label14);
            this.groupCareHomePrices.Controls.Add(this.txtPrivateRoomPrice);
            this.groupCareHomePrices.Controls.Add(this.label13);
            this.groupCareHomePrices.Location = new System.Drawing.Point(364, 292);
            this.groupCareHomePrices.Name = "groupCareHomePrices";
            this.groupCareHomePrices.Size = new System.Drawing.Size(330, 113);
            this.groupCareHomePrices.TabIndex = 54;
            this.groupCareHomePrices.TabStop = false;
            this.groupCareHomePrices.Text = "Budgets";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(292, 33);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(24, 22);
            this.button1.TabIndex = 56;
            this.button1.Text = "?";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtBudgetForCareHomes
            // 
            this.txtBudgetForCareHomes.Location = new System.Drawing.Point(175, 34);
            this.txtBudgetForCareHomes.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtBudgetForCareHomes.Name = "txtBudgetForCareHomes";
            this.txtBudgetForCareHomes.Size = new System.Drawing.Size(117, 20);
            this.txtBudgetForCareHomes.TabIndex = 54;
            this.txtBudgetForCareHomes.ThousandsSeparator = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(173, 19);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(129, 13);
            this.label15.TabIndex = 55;
            this.label15.Text = "Budget for care homes ($)";
            // 
            // groupAssistedLivingPrices
            // 
            this.groupAssistedLivingPrices.Controls.Add(this.txtAssistedLivingBudget);
            this.groupAssistedLivingPrices.Controls.Add(this.label18);
            this.groupAssistedLivingPrices.Location = new System.Drawing.Point(364, 292);
            this.groupAssistedLivingPrices.Name = "groupAssistedLivingPrices";
            this.groupAssistedLivingPrices.Size = new System.Drawing.Size(330, 113);
            this.groupAssistedLivingPrices.TabIndex = 57;
            this.groupAssistedLivingPrices.TabStop = false;
            this.groupAssistedLivingPrices.Text = "Budgets";
            // 
            // txtAssistedLivingBudget
            // 
            this.txtAssistedLivingBudget.Location = new System.Drawing.Point(14, 34);
            this.txtAssistedLivingBudget.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtAssistedLivingBudget.Name = "txtAssistedLivingBudget";
            this.txtAssistedLivingBudget.Size = new System.Drawing.Size(137, 20);
            this.txtAssistedLivingBudget.TabIndex = 13;
            this.txtAssistedLivingBudget.ThousandsSeparator = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(12, 19);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(91, 13);
            this.label18.TabIndex = 52;
            this.label18.Text = "Patient budget ($)";
            // 
            // FormPatient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 442);
            this.Controls.Add(this.groupAssistedLivingPrices);
            this.Controls.Add(this.groupCareHomePrices);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblProgress);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.rbFacilities);
            this.Controls.Add(this.rbCareHomes);
            this.Controls.Add(this.gbCareHomes);
            this.Controls.Add(this.btnClearForm);
            this.Controls.Add(this.gbFacilities);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormPatient";
            this.Text = "Patient Info";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormPatient_FormClosing);
            this.Load += new System.EventHandler(this.FormPatient_Load);
            this.Shown += new System.EventHandler(this.FormPatient_Shown);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.gbCareHomes.ResumeLayout(false);
            this.gbCareHomes.PerformLayout();
            this.gbFacilities.ResumeLayout(false);
            this.gbFacilities.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edPatientAge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrivateRoomPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSemiPrivateRoomPrice)).EndInit();
            this.groupCareHomePrices.ResumeLayout(false);
            this.groupCareHomePrices.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBudgetForCareHomes)).EndInit();
            this.groupAssistedLivingPrices.ResumeLayout(false);
            this.groupAssistedLivingPrices.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssistedLivingBudget)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox txtPatientNotes;
        private System.Windows.Forms.RadioButton rbFacilities;
        private System.Windows.Forms.RadioButton rbCareHomes;
        private System.Windows.Forms.GroupBox gbCareHomes;
        private System.Windows.Forms.CheckBox chkCareHomes;
        private System.Windows.Forms.Button btnClearForm;
        private System.Windows.Forms.GroupBox gbFacilities;
        private System.Windows.Forms.CheckBox chkSkilledNursingCare;
        private System.Windows.Forms.CheckBox chkMemoryCare;
        private System.Windows.Forms.CheckBox chkAssistedLiving;
        private System.Windows.Forms.CheckBox chkIndependentLiving;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rbALTCSYes;
        private System.Windows.Forms.RadioButton rbALTCSNo;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox edTelephone;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox edEmail;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtZip;
        private System.Windows.Forms.TextBox txtAddressCity;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbAddressState;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbAddressCity;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox edAddressStreet;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox edContactLastName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox edContactFirstName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown edPatientAge;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edPatientLastName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edPatientFirstName;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblProgress;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown txtPrivateRoomPrice;
        private System.Windows.Forms.NumericUpDown txtSemiPrivateRoomPrice;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupCareHomePrices;
        private System.Windows.Forms.NumericUpDown txtBudgetForCareHomes;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupAssistedLivingPrices;
        private System.Windows.Forms.NumericUpDown txtAssistedLivingBudget;
        private System.Windows.Forms.Label label18;
    }
}