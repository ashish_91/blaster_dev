﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistedLivingFacilityBlaster
{
    public partial class FormSendMailsToFacilities : Form
    {
        public List<Db.FacilityDetails> facilitiesMailDetails { get; set; }
        public Helper.PatientTemplateData templateData { get; set; }
        private Logger logger = LogManager.GetLogger("mailToFacilities");

        public FormSendMailsToFacilities()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            try
            {
                Helper.ValidateTemplates();

                txtSubject.Text = Helper.ReplacePatientTemplate(File.ReadAllText(@".\Templates\FacilityEmail_DefaultSubject.txt"), templateData);
                txtBody.Text = Helper.ReplacePatientTemplate(File.ReadAllText(@".\Templates\FacilityEmail_DefaultBody.txt"), templateData);

                foreach (var fac in facilitiesMailDetails)
                {
                    gridFacilities.Rows.Add(
                        fac.id,
                        fac.name,
                        fac.emails == null ? "" : string.Join("; ", fac.emails)
                    );
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
        
        private void btnSendEmails_Click(object sender, EventArgs e)
        {
            logger.Info(" === btnSendEmails_Click");
            Enabled = false;
            int storedCount = 0;
            try
            {
                lblStatus.Text = "Pushing emails to the queue, please wait...";
                lblStatus.Visible = true;
                Application.DoEvents();

                using (var db = new DbEntities())
                {                    
                    logger.Info("Subject: " + txtSubject.Text);
                    logger.Info("Body: " + txtBody.Text);

                    logger.Info("Start iterating....");
                    string mainEmail = string.Empty;
                    foreach (DataGridViewRow row in gridFacilities.Rows)
                    {
                        try
                        {
                            string emailsStr = row.Cells["Emails"]?.Value?.ToString();
                            logger.Info("   emailStr: " + emailsStr);

                            row.Cells["Result"].Value = String.Empty;
                            Application.DoEvents();

                            if (String.IsNullOrWhiteSpace(emailsStr))
                            {
                                logger.Info("     no email here");
                                row.Cells["Result"].Value = "ERROR: no email";
                                Application.DoEvents();
                                continue;
                            }

                            string[] emails = emailsStr.Split(new string[] { "; " }, StringSplitOptions.RemoveEmptyEntries);
                            logger.Info("     emails detected: " + emails.Length);

                            foreach (var email in emails)
                            {
                                storedCount++;
                                mainEmail = email;
                                logger.Info($"     start queuing from {Db.user.UserName} to {email}");
                                
                                Guid actionReportId = Guid.NewGuid();
                                Guid emailQueueId = Guid.NewGuid();

                                db.QueueEmails.Add(new QueueEmails()
                                {
                                    ActionsReportId = actionReportId,
                                    Created = DateTime.Now,
                                    Body = txtBody.Text,
                                    Subject = txtSubject.Text,
                                    CorrelationId = null,
                                    FacilityId = Guid.Parse(row.Cells["Id"].Value.ToString()),
                                    Id = emailQueueId,
                                    PatientId = templateData.patientId,
                                    Recipient = email.Trim(),
                                    Status = "Pending",
                                    StatusId = 0,
                                    Updated = DateTime.Now,
                                    CryptedPassword = Db.user.PasswordCrypted,
                                    FromAddress = Db.user.UserName,
                                    IsBodyHtml = false,
                                    UserId = Db.user.Id
                                });
                                
                                row.Cells["Result"].Value += "Queued;";
                                Application.DoEvents();                                
                                
                                db.ActionsReport.Add(new ActionsReport()
                                {
                                    ActionId = Db.ReportSendEmailToFacility,
                                    Body = txtBody.Text,
                                    CorrelationId = emailQueueId,
                                    ErrorText = null,
                                    FacilityId = Guid.Parse(row.Cells["Id"].Value.ToString()),
                                    Id = actionReportId,
                                    PatientId = templateData.patientId,
                                    Recipient = email.Trim(),
                                    Status = 1,
                                    Subject = txtSubject.Text,
                                    UserId = Db.user.Id,
                                    Created = DateTime.Now
                                });
                            }                            
                        }
                        catch (Exception ex)
                        {
                            db.ActionsReport.Add(new ActionsReport()
                            {
                                ActionId = Db.ReportSendEmailToFacility,
                                Body = txtBody.Text,
                                CorrelationId = null,
                                ErrorText = ex.Message,
                                FacilityId = Guid.Parse(row.Cells["Id"].Value.ToString()),
                                Id = Guid.NewGuid(),
                                PatientId = templateData.patientId,
                                Recipient = mainEmail.Trim(),
                                Status = -1,
                                Subject = txtSubject.Text,
                                UserId = Db.user.Id
                            });
                        }

                        if (storedCount >= 50)
                        {
                            lblStatus.Text = $"Pushing to queue {storedCount} emails, please wait...";
                            Application.DoEvents();
                            db.SaveChanges();
                            storedCount = 0;
                        }
                    }

                    lblStatus.Text = $"Pushing to queue {storedCount} emails, please wait...";
                    Application.DoEvents();
                    db.SaveChanges();
                }

                lblStatus.Text = $"Refreshing the email queue...";
                Application.DoEvents();
                
                DbCache.formMain.RefreshEmailsQueue();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Enabled = true;
                lblStatus.Visible = false;
            }
        }
        
        private void FormSendMailsToFacilities_Shown(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }
    }
}
