﻿namespace AssistedLivingFacilityBlaster
{
    using System;
    using System.Collections.Generic;

    public partial class AssistedLiving
    {
        public decimal Lng;
        public decimal Alt;
        public decimal Distance;
    }
}