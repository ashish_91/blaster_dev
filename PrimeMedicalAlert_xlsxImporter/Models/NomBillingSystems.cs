﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PrimeMedicalAlert.Db
{
    [Table("Nom_BillingSystems", Schema = "pma")]
    public partial class NomBillingSystems
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string BillingSystem { get; set; }
        public int Priority { get; set; }
    }
}