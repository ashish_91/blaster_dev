﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PrimeMedicalAlert.Db
{
    [Table("Nom_MonitoringServices", Schema = "pma")]
    public partial class NomMonitoringServices
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string MonitoringService { get; set; }
        public int Priority { get; set; }
    }
}