﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using ExcelDataReader;
using PrimeMedicalAlert.Db;

namespace PrimeMedicalAlert
{
    class Program
    {
        private const string StripeApiKey = "rk_live_szc5U9qwrjHd7Xo37BD8FdOs00OHSpCEM5";
        static void Main()
        {
            ChangeDecimalSeparator();
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            string filePath = Path.Combine("Files", "2020-05-05-Prime Medical Alert Clients-clean.xlsx");

            using var db = new Db.PmaDbModel();

            List<Db.Customers> customers = db.Customers.ToList();
            List<Db.NomBillingSystems> nomBillingSystems = db.NomBillingSystems.ToList();
            List<Db.NomCustomerStatuses> nomCustomerStatuses = db.NomCustomerStatuses.ToList();
            List<Db.NomMonitoringServices> nomMonitoringServices = db.NomMonitoringServices.ToList();
            List<Db.NomUnitTypes> nomUnitTypes = db.NomUnitTypes.ToList();
            List<Db.NomPaySchedule> nomPaySchedules = db.NomPaySchedule.ToList();
            List<Stripe.Customer> allStripeCustomers = GetAllStripeCustomers(StripeApiKey).ToList();

            var stripeConvertedPhones = new Dictionary<Stripe.Customer, long>();
            foreach(var customer in allStripeCustomers)
            {
                string phone = customer.Phone;
                if (string.IsNullOrWhiteSpace(phone)) phone = customer.Shipping?.Phone;
                if (string.IsNullOrWhiteSpace(phone)) continue;

                if (phone.Length > 10)
                    phone = phone.Substring(phone.Length - 10, 10);

                stripeConvertedPhones.Add(customer, GetLongFromString(phone));
            }

            Random rand = new Random();

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    bool skipFirstRow = true;
                    while (reader.Read())
                    {
                        if (skipFirstRow)
                        {
                            skipFirstRow = false;
                            continue;
                        }
                        
                        string telephone = GetStringFromXlsx(reader, 8);
                        long longPhone = rand.Next(int.MinValue, int.MaxValue);

                        if (!string.IsNullOrWhiteSpace(telephone))
                        {
                            longPhone = GetLongFromString(telephone);
                        } else
                        {

                        }

                        Stripe.Customer stripeCustomer = stripeConvertedPhones.FirstOrDefault(w => w.Value.Equals(longPhone)).Key;

                        string emailAddress = GetStringFromXlsx(reader, 2);
                        string address = GetStringFromXlsx(reader, 3);
                        string address2 = GetStringFromXlsx(reader, 4);
                        string clientName = GetStringFromXlsx(reader, 1);
                        string city = GetStringFromXlsx(reader, 5);
                        string state = GetStringFromXlsx(reader, 6);
                        string zip = GetStringFromXlsx(reader, 7);
                        string paySchedule = GetStringFromXlsx(reader, 9);
                        string ratePerMonth = GetStringFromXlsx(reader, 11);

                        if (stripeCustomer != null)
                        {
                            if (stripeCustomer.Email != null) emailAddress = stripeCustomer.Email;

                            string tempStr = stripeCustomer.Address?.Line1 ?? stripeCustomer.Shipping?.Address?.Line1;
                            if (tempStr != null) address = tempStr;

                            tempStr = stripeCustomer.Address?.Line2 ?? stripeCustomer.Shipping?.Address?.Line2;
                            if (!string.IsNullOrEmpty(tempStr)) address2 = tempStr;

                            if (stripeCustomer.Name != null) clientName = stripeCustomer.Name;

                            tempStr = stripeCustomer.Address?.City ?? stripeCustomer.Shipping?.Address?.City;
                            if (!string.IsNullOrEmpty(tempStr)) city = tempStr;

                            tempStr = stripeCustomer.Address?.State ?? stripeCustomer.Shipping?.Address?.State;
                            if (!string.IsNullOrEmpty(tempStr)) state = tempStr;

                            tempStr = stripeCustomer.Address?.PostalCode ?? stripeCustomer.Shipping?.Address?.PostalCode;
                            if (!string.IsNullOrEmpty(tempStr)) zip = tempStr;

                            tempStr = stripeCustomer.Phone ?? stripeCustomer.Shipping?.Phone;
                            if (!string.IsNullOrEmpty(tempStr)) telephone = tempStr;
                        }

                        string accountNumber = GetStringFromXlsx(reader, 0);
                        string unitType = GetStringFromXlsx(reader, 10);
                        string status = GetStringFromXlsx(reader, 12) ?? "1";

                        string strStartDate = GetStringFromXlsx(reader, 13);
                        if (strStartDate == "032/19/2020") strStartDate = "03/19/2020";
                        if (strStartDate == "0/18/2018") strStartDate = "1/18/2018";
                        if (strStartDate == "/10/9/2018") strStartDate = "10/9/2018";
                        if (strStartDate == "05/117/2019") strStartDate = "05/17/2019";
                        if (strStartDate == "0316/2020") strStartDate = "03/16/2020"; 
                        if (strStartDate == "04/02/202") strStartDate = "04/02/2020"; 
                        if (string.IsNullOrEmpty(strStartDate)) strStartDate = "01/01/2019";

                        DateTime startDate;
                        try
                        {
                            if (!DateTime.TryParseExact(strStartDate, "M'/'d'/'yyyy", null, System.Globalization.DateTimeStyles.None, out startDate))
                            {
                                if (!DateTime.TryParseExact(strStartDate, "dd.MM.yyyy", null, System.Globalization.DateTimeStyles.None, out startDate))
                                {
                                    if (!DateTime.TryParseExact(strStartDate.Substring(0, 10), "dd.MM.yyyy", null, System.Globalization.DateTimeStyles.None, out startDate))
                                    {
                                        if (!DateTime.TryParseExact(strStartDate.Substring(0, 10), "M'/'d'/'yyyy", null, System.Globalization.DateTimeStyles.None, out startDate))
                                        {
                                            if (!DateTime.TryParseExact(strStartDate.Substring(0, 10), "M'/'d.yyyy", null, System.Globalization.DateTimeStyles.None, out startDate))
                                            {
                                                if (!DateTime.TryParseExact(strStartDate.Substring(0, 9).Trim(), "M'/'d'/'yyyy", null, System.Globalization.DateTimeStyles.None, out startDate))
                                                {
                                                    throw new Exception("");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch(Exception ex)
                        {
                            //throw;
                            startDate = DateTime.MinValue;
                        }

                        string arpm = GetStringFromXlsx(reader, 14);
                        string billingSystem = GetStringFromXlsx(reader, 15);
                        string monitoringSystem = GetStringFromXlsx(reader, 16);

                        NomBillingSystems cachedBillingSystem = MatchBillingSystem(db, nomBillingSystems, billingSystem);
                        NomCustomerStatuses cachedCustomerStatus = MatchCustomerStatus(db, nomCustomerStatuses, status);
                        NomMonitoringServices cachedMonitoringService = MatchMonitoringService(db, nomMonitoringServices, monitoringSystem);
                        NomUnitTypes cachedUnitTypes = MatchUnitType(db, nomUnitTypes, unitType);
                        NomPaySchedule cachedPaySchedule = MatchPaySchedule(db, nomPaySchedules, paySchedule);

                        Db.Customers customer = customers.SingleOrDefault(s => 
                            s.AccountNumber.Equals(accountNumber, StringComparison.OrdinalIgnoreCase)
                            && s.ClientName.Equals(clientName, StringComparison.OrdinalIgnoreCase)
                            );

                        if (customer is null)
                        {
                            customer = new Db.Customers()
                            {
                                AccountNumber = accountNumber,
                                Address = address,
                                Address2 = address2 ?? "",
                                BillingSystem = cachedBillingSystem?.Id ?? -1,
                                Status = cachedCustomerStatus.Id,
                                Arpm = GetDecimalFromString(arpm),
                                City = city,
                                ClientName = clientName,
                                ClientTypeIsStripeUserOrNot = stripeCustomer != null ? 1 : 0,
                                EmailAddress = emailAddress ?? "",
                                Id = Guid.NewGuid(),
                                MonitoringService = cachedMonitoringService?.Id ?? -1,
                                PayScheduleId = cachedPaySchedule?.Id ?? -1,
                                RatePerMonth = GetDecimalFromString(ratePerMonth),
                                StartDate = startDate,
                                State = state,
                                StripeClientId = stripeCustomer?.Id,
                                Telephone = telephone ?? "",
                                Zip = zip,
                                UnitType = cachedUnitTypes.Id,
                                IsDue = false
                            };

                            db.Customers.Add(customer);

                            Console.WriteLine("ADD NEW " + customer.AccountNumber);
                        } else
                        {
                            Console.WriteLine("UPDATE " + customer.AccountNumber);
                            continue;

                            customer.AccountNumber = accountNumber;
                            customer.Address = address;
                            customer.Address2 = address2 ?? "";
                            customer.BillingSystem = cachedBillingSystem?.Id ?? -1;
                            customer.Status = cachedCustomerStatus.Id;
                            customer.Arpm = GetDecimalFromString(arpm);
                            customer.City = city;
                            customer.ClientName = clientName;
                            customer.ClientTypeIsStripeUserOrNot = stripeCustomer != null ? 1 : 0;
                            customer.EmailAddress = emailAddress ?? "";                            
                            customer.MonitoringService = cachedMonitoringService?.Id ?? -1;
                            customer.PayScheduleId = cachedPaySchedule?.Id ?? -1;
                            customer.RatePerMonth = GetDecimalFromString(ratePerMonth);
                            customer.StartDate = startDate;
                            customer.State = state;
                            customer.StripeClientId = stripeCustomer?.Id;
                            customer.Telephone = telephone ?? "";
                            customer.Zip = zip;
                            customer.UnitType = cachedUnitTypes.Id;
                        }

                        db.SaveChanges();
                        
                    }
                }
            }
        }

        private static string GetStringFromXlsx(IExcelDataReader reader, int index)
        {
            return reader.GetValue(index)?.ToString()?.Trim();
        }

        private static DateTime GetDateFromXlsx(IExcelDataReader reader, int index)
        {
            return reader.GetDateTime(index);
        }

        private static decimal GetDecimalFromString(string val)
        {
            if (string.IsNullOrWhiteSpace(val)) return 0;
            
            return decimal.Parse(new String(val.Where(w => Char.IsDigit(w) || w == '.').ToArray()), System.Globalization.NumberStyles.Any);
        }

        private static long GetLongFromString(string val)
        {
            return long.Parse(new String(val.Where(w => Char.IsDigit(w)).ToArray()));
        }

        public static IEnumerable<Stripe.Customer> GetAllStripeCustomers(string apiKey)
        {
            Stripe.StripeConfiguration.ApiKey = apiKey;

            var service = new Stripe.CustomerService();
            return service.ListAutoPaging();
        }

        private static NomBillingSystems MatchBillingSystem(PmaDbModel db, List<NomBillingSystems> nomBillingSystems, string billingSystem)
        {
            if (billingSystem == null) return null;

            Db.NomBillingSystems cachedBillingSystem = nomBillingSystems.SingleOrDefault(s => s.BillingSystem.Equals(billingSystem, StringComparison.OrdinalIgnoreCase));
            if (cachedBillingSystem is null)
            {
                int maxId = nomBillingSystems.Select(s => s.Id).DefaultIfEmpty(0).Max(m => m) + 1;

                cachedBillingSystem = new Db.NomBillingSystems()
                {
                    BillingSystem = billingSystem,
                    Id = maxId,
                    Priority = maxId
                };

                nomBillingSystems.Add(cachedBillingSystem);

                db.NomBillingSystems.Add(cachedBillingSystem);
                db.SaveChanges();
            }

            return cachedBillingSystem;
        }

        private static NomUnitTypes MatchUnitType(PmaDbModel db, List<NomUnitTypes> nomUnitTypes, string unitType)
        {
            Db.NomUnitTypes cachedUnitType = nomUnitTypes.SingleOrDefault(s => s.UnitType.Equals(unitType, StringComparison.OrdinalIgnoreCase));
            if (cachedUnitType is null)
            {
                int maxId = nomUnitTypes.Select(s => s.Id).DefaultIfEmpty(0).Max(m => m) + 1;

                cachedUnitType = new Db.NomUnitTypes()
                {
                    UnitType = unitType,
                    Id = maxId,
                    Priority = maxId
                };

                nomUnitTypes.Add(cachedUnitType);

                db.NomUnitTypes.Add(cachedUnitType);
                db.SaveChanges();
            }

            return cachedUnitType;
        }

        private static NomPaySchedule MatchPaySchedule(PmaDbModel db, List<NomPaySchedule> nomPaySchedule, string paySchedule)
        {
            Db.NomPaySchedule cachedPaySchedule = nomPaySchedule.SingleOrDefault(s => s.PaySchedule.Equals(paySchedule, StringComparison.OrdinalIgnoreCase));
            if (cachedPaySchedule is null)
            {
                int maxId = nomPaySchedule.Select(s => s.Id).DefaultIfEmpty(0).Max(m => m) + 1;

                cachedPaySchedule = new Db.NomPaySchedule()
                {
                    PaySchedule = paySchedule,
                    Id = maxId,
                    Priority = maxId
                };

                nomPaySchedule.Add(cachedPaySchedule);

                db.NomPaySchedule.Add(cachedPaySchedule);
                db.SaveChanges();
            }

            return cachedPaySchedule;
        }

        private static NomMonitoringServices MatchMonitoringService(PmaDbModel db, List<NomMonitoringServices> nomMonitoringService, string monitoringService)
        {
            if (string.IsNullOrWhiteSpace(monitoringService)) return null;

            Db.NomMonitoringServices cachedMonitoringservice = nomMonitoringService.SingleOrDefault(s => s.MonitoringService.Equals(monitoringService, StringComparison.OrdinalIgnoreCase));
            if (cachedMonitoringservice is null)
            {
                int maxId = nomMonitoringService.Select(s => s.Id).DefaultIfEmpty(0).Max(m => m) + 1;

                cachedMonitoringservice = new Db.NomMonitoringServices()
                {
                    MonitoringService = monitoringService,
                    Id = maxId,
                    Priority = maxId
                };

                nomMonitoringService.Add(cachedMonitoringservice);

                db.NomMonitoringServices.Add(cachedMonitoringservice);
                db.SaveChanges();
            }

            return cachedMonitoringservice;
        }

        private static NomCustomerStatuses MatchCustomerStatus(PmaDbModel db, List<NomCustomerStatuses> nomCustomerStatuses, string customerStatus)
        {
            Db.NomCustomerStatuses cachedCustomerStatus = nomCustomerStatuses.SingleOrDefault(s => s.Status.Equals(customerStatus, StringComparison.OrdinalIgnoreCase));
            if (cachedCustomerStatus is null)
            {
                int maxId = nomCustomerStatuses.Select(s => s.Id).DefaultIfEmpty(0).Max(m => m) + 1;

                cachedCustomerStatus = new Db.NomCustomerStatuses()
                {
                    Status = customerStatus,
                    Id = maxId,
                    Priority = maxId
                };

                nomCustomerStatuses.Add(cachedCustomerStatus);

                db.NomCustomerStatuses.Add(cachedCustomerStatus);
                db.SaveChanges();
            }

            return cachedCustomerStatus;
        }

        public static void ChangeDecimalSeparator()
        {
            string CultureName = Thread.CurrentThread.CurrentCulture.Name;
            var ci = new System.Globalization.CultureInfo(CultureName);
            if (ci.NumberFormat.NumberDecimalSeparator != ".")
            {
                // Forcing use of decimal separator for numerical values
                ci.NumberFormat.NumberDecimalSeparator = ".";
                Thread.CurrentThread.CurrentCulture = ci;
            }
        }
    }
}