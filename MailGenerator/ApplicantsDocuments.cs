
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace Shared
{

using System;
    using System.Collections.Generic;
    
public partial class ApplicantsDocuments
{

    public System.Guid Id { get; set; }

    public System.Guid ApplicantId { get; set; }

    public System.DateTime CreatedOn { get; set; }

    public System.Guid CreatedByUserId { get; set; }

    public string DocumentName { get; set; }

    public string DocumentType { get; set; }

    public int Version { get; set; }

    public int Status { get; set; }

    public Nullable<int> DocumentTypeId { get; set; }

    public Nullable<int> DocumentCategoryId { get; set; }

    public Nullable<int> DocumentExtraStatus { get; set; }

    public Nullable<System.Guid> CorrelationId { get; set; }

    public string Notes { get; set; }

    public string RejectReason { get; set; }

    public Nullable<System.Guid> DocumentParentId { get; set; }

}

}
