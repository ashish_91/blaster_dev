﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Enums
{
    public enum subscriptionStatus
    {
        Active = 2,
        PastDue = 1,
        Cancel = 0     
       
    }
}
