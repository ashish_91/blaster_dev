﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Source.DB
{
    public static class Documents
    {
        public static void DeleteCategory(int categoryId, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { categoryId})}");

            ApplicantsApp.Db.ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var toDelete = db.ApplicantsDocuments.Where(w => w.DocumentCategoryId == categoryId).ToList();

                db.ApplicantsDocuments.RemoveRange(toDelete);

                db.Applicants_DocumentCategories.Remove(db.Applicants_DocumentCategories.Single(s => s.Id.Equals(categoryId)));
                db.SaveChanges();
            }
        }

        public static ApplicantsDocuments GetDocumentById(
            Guid documentId,
            NLog.Logger logger
        )
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { documentId })}");

            using (var db = new DbModel())
            {
                return
                    db.ApplicantsDocuments.SingleOrDefault(w => w.Id.Equals(documentId))
                    ?? throw new Exception($"Document with ID {documentId} does not exist");
            }
        }

    }
}
