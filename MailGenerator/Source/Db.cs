﻿using ApplicantsApp.Models.Applicant.Applicant;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Shared;
using Shared.Source;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp
{
    public static class Db
    {
        public static Users user;
        public static Sessions session;


        ///////////// STRIPE
        ///

        public static Applicants_CustomersSubscriptions GetCustomersSubscriptionById(NLog.Logger logger, Guid CustomersSubscriptionId)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { CustomersSubscriptionId })}");

            using (var db = new DbModel())
            {
                return db.Applicants_CustomersSubscriptions.Single(s => s.Id.Equals(CustomersSubscriptionId));
            }
        }

        public static void UpdateCustomersTotalPayedSoFar(NLog.Logger logger, Guid CustomersSubscriptionId, decimal totalPayedSoFar,int subscriptionStatus)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { CustomersSubscriptionId, totalPayedSoFar })}");

            using (var db = new DbModel())
            {
               // db.Applicants_CustomersSubscriptions.Single(s => s.Id.Equals(CustomersSubscriptionId)).TotalPayedSoFar = totalPayedSoFar;
               var customerSubscription= db.Applicants_CustomersSubscriptions.Single(s => s.Id.Equals(CustomersSubscriptionId));
                customerSubscription.TotalPayedSoFar = totalPayedSoFar;
                customerSubscription.SubscriptionStatus = subscriptionStatus;
                db.SaveChanges();
            }
        }

        public static Applicants_CustomersSubscriptions IncrementCustomerToPayTotal(NLog.Logger logger, Guid CustomersSubscriptionId, decimal incrementTargetToPayTotal)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { CustomersSubscriptionId, incrementTargetToPayTotal })}");

            using (var db = new DbModel())
            {
                var app = db.Applicants_CustomersSubscriptions.Single(s => s.Id.Equals(CustomersSubscriptionId));
                app.TotalTargetSum += incrementTargetToPayTotal;
                db.SaveChanges();

                return app;
            }
        }

        public static ApplicantsNew GetApplicantByCustomerStripeId(NLog.Logger logger, string stripeCustomerId)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { stripeCustomerId })}");

            using (var db = new DbModel())
            {
                return db.ApplicantsNew.SingleOrDefault(s => s.StripeCustomerId.Equals(stripeCustomerId));
            }
        }

        public static Applicants_CustomersSubscriptions AddSubscription(int dayOfMonthToInvoice, DbModel db, Guid applicantId, Subscription subscription, Product product, decimal totalOwnedForThisProduct, decimal totalPeyedSoFar,int subscriptionStatus=2)
        {
            Applicants_CustomersSubscriptions dbSubscription = new Shared.Applicants_CustomersSubscriptions()
            {
                ApplicantId = applicantId,
                Created = subscription.Created.Value,
                CreatedBy = Guid.Empty,
                DayOfMonthToInvoice = dayOfMonthToInvoice,
                Id = Guid.NewGuid(),
                IsActive = true,
                MonthlySum = (subscription?.Plan?.Amount.GetValueOrDefault() ?? 0) / 100m,
                ProductName = product.Name,
                StripePlanId = subscription.Plan.Id,
                StripeProductId = product.Id,
                SubscriptionId = subscription.Id,
                TotalTargetSum = totalOwnedForThisProduct,
                TotalPayedSoFar = totalPeyedSoFar,
                SubscriptionStatus=subscriptionStatus
            };

            db.Applicants_CustomersSubscriptions.Add(dbSubscription);
            return dbSubscription;
        }

        //public static void InsertSubscription(NLog.Logger logger,
        //                                      Guid applicantId,
        //                                      int dayOfMonthToInvoice,
        //                                      decimal totalSum,
        //                                      Stripe.Customer customer,
        //                                      Stripe.Product product,
        //                                      Stripe.Plan plan,
        //                                      Stripe.Subscription subscription)
        //{
        //    logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
        //        $"with params {JsonConvert.SerializeObject(new { totalSum, applicantId, subscription, plan, product, customer, session.Id })}");

        //    ValidateSession(false, logger);

        //    using (var db = new DbModel())
        //    {
        //        db.Applicants_CustomersSubscriptions.Add(new Applicants_CustomersSubscriptions()
        //        {
        //            ApplicantId = applicantId,
        //            Created = DateTime.Now,
        //            CreatedBy = GetUserId(),
        //            DayOfMonthToInvoice = dayOfMonthToInvoice,
        //            IsActive = true,
        //            MonthlySum = plan.AmountDecimal.Value,
        //            Id = Guid.NewGuid(),
        //            ProductName = product.Name,
        //            StripePlanId = plan.Id,
        //            StripeProductId = product.Id,
        //            TotalTargetSum = totalSum
        //        });

        //        db.SaveChanges();
        //    }
        //}

        public static DateTime? DeleteAllScheduledEmails(
            Guid applicantId,
            NLog.Logger logger,
            Models.Communication.EmailTypes emailType)
        {
            Dictionary<Models.Communication.EmailTypes, DateTime?> resp = DeleteAllScheduledEmailsOfTypeAndReturnLastSentDates(applicantId, logger, new List<Models.Communication.EmailTypes>() { emailType });
            if (resp.ContainsKey(emailType))
            {
                return resp[emailType];
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="logger"></param>
        /// <param name="emailTypes"></param>
        /// <returns></returns>
        public static Dictionary<Models.Communication.EmailTypes, DateTime?> DeleteAllScheduledEmailsOfTypeAndReturnLastSentDates(
        Guid applicantId,
        NLog.Logger logger,
        List<Models.Communication.EmailTypes> emailTypes)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { emailTypes, session.Id })}");

            if (emailTypes.Count == 0)
                return new Dictionary<Models.Communication.EmailTypes, DateTime?>();

            ValidateSession(false, logger);
            var lastSentByType = new Dictionary<Models.Communication.EmailTypes, DateTime?>();
            using (var db = new DbModel())
            {
                db.Database.Log = s => logger.Info(s);

                foreach (var emailType in emailTypes)
                {
                    //delete all scheduled of that type
                    IEnumerable<ApplicantsActionsEmailDetails> allRelatedEmails = db.ApplicantsActionsEmailDetails
                        .Where(w => w.ApplicantId.Equals(applicantId))
                        .Where(w => w.EmailTypeId == (int)emailType)
                        .Where(w => w.Result == (int)Models.Communication.EmailStatusCodes.Scheduled);

                    db.ApplicantsActionsEmailDetails.RemoveRange(allRelatedEmails);

                    if (allRelatedEmails.Count() > 0)
                    {
                        InsertAction(
                            applicantId,
                            db,
                            Models.ReportActions.ApplicantDeleteScheduledEmail,
                            JsonConvert.SerializeObject(new
                            {
                                emailType = emailType.ToString(),
                                allRelatedEmailsCount = allRelatedEmails.Count()
                            }),
                            allRelatedEmails.First().Id
                            );
                    }

                    //get last sent, if any                    
                    lastSentByType.Add(emailType, db.ApplicantsActionsEmailDetails
                        .Where(w => w.ApplicantId.Equals(applicantId))
                        .Where(w => w.EmailTypeId == (int)emailType)
                        .Where(w =>
                            w.Result == (int)Models.Communication.EmailStatusCodes.Success ||
                            w.Result == (int)Models.Communication.EmailStatusCodes.Scheduled_AlreadySent ||
                            w.Result == (int)Models.Communication.EmailStatusCodes.Scheduled_ConditionsNotMeetToSend ||
                            w.Result == (int)Models.Communication.EmailStatusCodes.Error)
                        .OrderByDescending(o => o.Created)
                        .Select(s => s.Created)
                        .FirstOrDefault());
                }

                db.SaveChanges();
            }

            return lastSentByType;
        }

        public static List<ApplicantsActionsEmailDetails> GetAllScheduledEmails(NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { })}");

            using (var db = new DbModel())
            {
                db.Database.Log = s => logger.Info(s);

                DateTime scheduledAfter = DateTime.Now.AddDays(-2);
                return db.ApplicantsActionsEmailDetails
                    .Where(w => w.Result == (int)Models.Communication.EmailStatusCodes.Scheduled)
                    .Where(w => w.ScheduledFor > scheduledAfter)
                    //.Where(w => w.EmailTypeId == (int)Models.Communication.EmailTypes.CaringComAPICall)  JUST FOR TESTING< DON't UNCOMMENT
                    .Where(w => w.ScheduledFor < DateTime.Now)
                    .ToList();
            }
        }

        public static void UpdateStatusAndCorrelationIdEmailDetails(NLog.Logger logger, ApplicantsActionsEmailDetails emailDetails)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { emailDetails })}");

            using (var db = new DbModel())
            {
                db.Database.Log = s => logger.Info(s);

                var em = db.ApplicantsActionsEmailDetails.Single(s => s.Id.Equals(emailDetails.Id));
                em.Result = emailDetails.Result;
                em.CorrelationId = emailDetails.CorrelationId;
                em.ErrorMessage = emailDetails.ErrorMessage;

                db.SaveChanges();
            }
        }

        public static List<Applicants_NomStatuses> GetAllStatuses(NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { session.Id })}");

            ValidateSession(false, logger);
            using (var db = new DbModel())
            {
                return db.Applicants_NomStatuses
                    .OrderBy(o => o.Priority)                    
                    .ToList();
            }
        }

        public static void UpdateApplicantLastStatusChangeDate(ApplicantsNew applicant)
        {
            using (var db = new DbModel())
            {
                db.ApplicantsNew.Single(s => s.Id.Equals(applicant.Id)).LastStatusChanged = DateTime.Now;
                db.SaveChanges();
            }
        }

        //////// 
        //Categories
        //////// 
        public static List<Applicants_DocumentCategories> GetAllDocumentCategories(NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { })}");


            using (var db = new DbModel())
            {
                return db.Applicants_DocumentCategories
                    .OrderBy(o => o.Priority)
                    .ToList();
            }
        }

        public static bool IsCategoryNameUnique(string categoryName, int categoryId, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { categoryName, categoryId, session.Id })}");

            using (var db = new DbModel())
            {
                var categories = db.Applicants_DocumentCategories.Where(s => s.Category.Equals(categoryName)).ToList();
                return !categories.Any(a => !a.Id.Equals(categoryId));
            }
        }

        public static Applicants_DocumentCategories CreateCategory(Applicants_DocumentCategories category, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { category, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                category.CreatedByUserId = GetUserId();
                category.CreatedOn = DateTime.Now;
                category.Priority = category.Priority;

                db.Applicants_DocumentCategories.Add(category);
                db.SaveChanges();

                return category;
            }
        }

        public static Guid GetUserId()
        {
            return user?.Id ?? new Guid("00000000-0000-0000-0000-000000000001");
        }

        public static void UpdateCategory(Applicants_DocumentCategories category, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { category, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var dbCategory = db.Applicants_DocumentCategories.Single(s => s.Id.Equals(category.Id));

                dbCategory.Category = category.Category;
                dbCategory.Description = category.Description;
                dbCategory.Priority = category.Priority;

                db.SaveChanges();
            }
        }



        public static Applicants_DocumentCategories GetCategory(int categoryId, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { categoryId })}");

            using (var db = new DbModel())
            {
                return db.Applicants_DocumentCategories.Single(s => s.Id == categoryId);
            }
        }


        //////// 
        /// Documents
        //////// 
        public static List<ApplicantsDocuments> GetApplicantDocuments(
            Guid applicantId,
            NLog.Logger logger,
            Models.Documents.DocumentStatus documentStatus,
            Models.Documents.DocumentType documentType = Models.Documents.DocumentType.unspecified
            )
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId })}");

            //ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var appDocQuery =
                    db.ApplicantsDocuments
                        .Where(w => w.ApplicantId == applicantId);

                if (documentStatus != Models.Documents.DocumentStatus.unspecified)
                {
                    appDocQuery = appDocQuery
                        .Where(w => w.Status == (int)documentStatus);
                }

                if (documentType != Models.Documents.DocumentType.unspecified)
                {
                    appDocQuery = appDocQuery
                        .Where(w => w.DocumentType == documentType.ToString());
                }

                return appDocQuery
                    .OrderByDescending(o => o.CreatedOn)
                    .ToList();
            }
        }

        public static ApplicantsDocuments AddApplicantDocument(ApplicantsDocuments document, NLog.Logger logger)
        {
            //document.ApplicantId - The id of the related applicant
            //document.CorrelationId - Some correlationId
            //document.DocumentCategoryId - WHat is the document category in the RFI, 
            //                              this is id from table Applicants_DocumentCategories            
            //                              it is loaded as TAG in the nodeRFI categories
            //document.DocumentExtraStatus - for RFI shows is the document just a placeholder or it's uploaded (and accepted)
            //                               Models.Documents.DocumentExtraStatus
            //document.DocumentType - string ident showing what type is the document
            //                        this info is taken from Models.Documents.DocumentType
            //document.DocumentTypeId - shows the document Type in the TREE
            //                          Models.Documents.DocumentTreeType
            //document.Status - shows is the edocument active, deleted or old
            //                  Models.Documents.DocumentStatus            



            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { document, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                document.CreatedByUserId = GetUserId();
                document.CreatedOn = DateTime.Now;
                document.Status = 1;

                db.ApplicantsDocuments.Add(document);
                UpdateApplicantAccessDate(document.ApplicantId, db);

                db.SaveChanges();

                return document;
            }
        }

        private static void UpdateApplicantAccessDate(Guid applicantId, DbModel db)
        {
            var applicant = db.ApplicantsNew.Single(s => s.Id == applicantId);
            applicant.UpdatedOn = DateTime.Now;
            applicant.UpdatedByUserId = GetUserId();
        }

        private static void UpdateApplicantAccessDateObject(ApplicantsNew applicant, DbModel db)
        {
            applicant.UpdatedOn = DateTime.Now;
            applicant.UpdatedByUserId = GetUserId();
        }

        /// <summary>
        /// Use to indidate deleted, active, old, and so on
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="newDocumentStatus"></param>
        /// <param name="logger"></param>
        public static void ChangeApplicantDocumentStatus(
            Guid documentId,
            Models.Documents.DocumentStatus newDocumentStatus,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { documentId, newDocumentStatus, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                ApplicantsDocuments document = db.ApplicantsDocuments.Single(s => s.Id.Equals(documentId));
                document.Status = (int)newDocumentStatus;

                UpdateApplicantAccessDate(document.ApplicantId, db);
                db.SaveChanges();
            }
        }

        public static void DeletePlaceholderAndAllRelatedDocuments(
            Guid documentId,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { documentId, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                ApplicantsDocuments document = db.ApplicantsDocuments.Single(s => s.Id.Equals(documentId));
                document.Status = (int)Models.Documents.DocumentStatus.deleted;

                InsertAction(document.ApplicantId,
                             db,
                             document.DocumentParentId == null ? Models.ReportActions.RFIDeletePlaceholder : Models.ReportActions.DocumentDelete,
                             document.DocumentName,
                             document.Id);

                if (document.DocumentParentId == null)
                {
                    //This is opalceholder, whcih means we should try delete all related documents
                    foreach (ApplicantsDocuments childDoc in db.ApplicantsDocuments.Where(w => w.DocumentParentId == documentId))
                    {
                        childDoc.Status = (int)Models.Documents.DocumentStatus.deleted;
                        InsertAction(childDoc.ApplicantId, db, Models.ReportActions.DocumentDelete, childDoc.DocumentName, childDoc.Id);
                    }
                }
                else
                {
                    //not a placeholder, so now check is a docuiment now empty
                    int currentDocumentsCount = db.ApplicantsDocuments.Count(w =>
                        w.DocumentParentId == document.DocumentParentId &&
                        w.Status == (int)Models.Documents.DocumentStatus.active);

                    if (currentDocumentsCount == 1)
                    {
                        //The last file will be deleted
                        ApplicantsDocuments parentDocument = db.ApplicantsDocuments.Single(s => s.Id == document.DocumentParentId);
                        parentDocument.DocumentExtraStatus = (int)Models.Documents.DocumentExtraStatus.justPlaceHolder;
                    }
                }

                UpdateApplicantAccessDate(document.ApplicantId, db);
                db.SaveChanges();
            }
        }

        public static void ChangeApplicantDocumentName(
            Guid documentId,
            string newDocumentName,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { documentId, newDocumentName, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                ApplicantsDocuments document = db.ApplicantsDocuments.Single(s => s.Id.Equals(documentId));
                document.DocumentName = newDocumentName;

                UpdateApplicantAccessDate(document.ApplicantId, db);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// USe for RFI document indicating is a placeholder, inReview, Approved
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="newDocumentExtraStatus"></param>
        /// <param name="logger"></param>
        public static void ChangeApplicantDocumentExtraStatus(
            Guid documentId,
            Models.Documents.DocumentExtraStatus newDocumentExtraStatus,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { documentId, newDocumentExtraStatus, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                ApplicantsDocuments document = db.ApplicantsDocuments.Single(s => s.Id.Equals(documentId));
                document.DocumentExtraStatus = (int)newDocumentExtraStatus;

                UpdateApplicantAccessDate(document.ApplicantId, db);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// The ExtraStatus will remains, also the document category, which should not be a issue
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="documentTreeType"></param>
        /// <param name="logger"></param>
        public static void ChangeApplicantDocumentTreeType(
            Guid documentId,
            Models.Documents.DocumentTreeType documentTreeType,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { documentId, documentTreeType, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                ApplicantsDocuments document = db.ApplicantsDocuments.Single(s => s.Id.Equals(documentId));
                document.DocumentTypeId = (int)documentTreeType;

                UpdateApplicantAccessDate(document.ApplicantId, db);
                db.SaveChanges();
            }
        }

        public static void ChangeDocumentRejectReason(
            Guid documentId,
            string rejectReason,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { documentId, rejectReason, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                ApplicantsDocuments document = db.ApplicantsDocuments.Single(s => s.Id.Equals(documentId));
                document.RejectReason = rejectReason;

                UpdateApplicantAccessDate(document.ApplicantId, db);
                db.SaveChanges();
            }
        }

        public static void ChangeDocumentNotes(
            Guid documentId,
            string notes,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { documentId, notes, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                ApplicantsDocuments document = db.ApplicantsDocuments.Single(s => s.Id.Equals(documentId));
                document.Notes = notes;

                UpdateApplicantAccessDate(document.ApplicantId, db);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="newDocumentStatus"></param>
        /// <param name="logger"></param>
        public static void ChangeApplicantDocumentsStatus(
            IEnumerable<Guid> documentsIds,
            Guid applicantId,
            Models.Documents.DocumentStatus newDocumentStatus,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { documentsIds, newDocumentStatus, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                foreach (var doc in db.ApplicantsDocuments.Where(w => documentsIds.Contains(w.Id)))
                {
                    doc.Status = (int)newDocumentStatus;
                }

                UpdateApplicantAccessDate(applicantId, db);
                db.SaveChanges();
            }
        }

        public static void ChangeApplicantCustomCategoryDescriptions(
            Guid applicantId,
            string json,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, json, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var applicant = db.ApplicantsNew.Single(s => s.Id == applicantId);
                applicant.CustomCategoryDescriptions = json;
                UpdateApplicantAccessDate(applicantId, db);

                db.SaveChanges();
            }
        }

        public static void ChangeApplicantDueDate(
            Guid applicantId,
            DateTime dueDate,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, dueDate, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var applicant = db.ApplicantsNew.Single(s => s.Id == applicantId);
                applicant.DueDate = dueDate;
                UpdateApplicantAccessDateObject(applicant, db);

                db.SaveChanges();
            }
        }

        public static void ChangeApplicantTransferSum(
            Guid applicantId,
            decimal transferSum,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, transferSum, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var applicant = db.ApplicantsNew.Single(s => s.Id == applicantId);
                applicant.Transfers = transferSum;
                UpdateApplicantAccessDateObject(applicant, db);
                InsertAction(applicantId, db, Models.ReportActions.ApplicantChangeTransferSum, transferSum.ToString());

                db.SaveChanges();
            }
        }

        private static void InsertAction(Guid applicantId, DbModel db, Models.ReportActions action, string data = null, Guid? correlationId = null)
        {
            db.ApplicantsActions.Add(new ApplicantsActions()
            {
                Id = Guid.NewGuid(),
                ActionId = (int)action,
                Data = data,
                UserId = GetUserId(),
                ApplicantId = applicantId,
                CorrelationId = correlationId,
                Created = DateTime.Now,
            });
        }

        public static void ChangeApplicantTransferDate(
            Guid applicantId,
            DateTime transferDate,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, transferDate, session.Id })}");

            //ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var applicant = db.ApplicantsNew.Single(s => s.Id == applicantId);
                applicant.TransferDate = transferDate;
                UpdateApplicantAccessDateObject(applicant, db);

                InsertAction(applicantId, db, Models.ReportActions.ApplicantChangeTransferDate, transferDate.ToString("yyyy-MM-dd"));

                db.SaveChanges();
            }
        }

        public static void ChangeFinancialInterviewDate(
            Guid applicantId,
            DateTime date,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, date, session.Id })}");

            //ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var applicant = db.ApplicantsNew.Single(s => s.Id == applicantId);
                applicant.FinancialInterviewDate = date;
                UpdateApplicantAccessDateObject(applicant, db);

                InsertAction(applicantId, db, Models.ReportActions.ApplicantChangeFinancialInterviewDate, date.ToString("yyyy-MM-dd"));

                db.SaveChanges();
            }
        }

        public static void ChangeBilled(
            Guid applicantId,
            bool billed,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, billed, session.Id })}");

            //ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var applicant = db.ApplicantsNew.Single(s => s.Id == applicantId);
                applicant.Billed = billed;
                UpdateApplicantAccessDateObject(applicant, db);

                InsertAction(applicantId, db, Models.ReportActions.ApplicantChangeBilled, billed.ToString());

                db.SaveChanges();
            }
        }

        public static void ChangePASApproved(
            Guid applicantId,
            bool pasApproved,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, pasApproved, session.Id })}");

            //ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var applicant = db.ApplicantsNew.Single(s => s.Id == applicantId);
                applicant.PASApproved = pasApproved ? 1 : 0;
                UpdateApplicantAccessDateObject(applicant, db);

                InsertAction(applicantId, db, Models.ReportActions.ApplicantChangeBilled, pasApproved.ToString());

                db.SaveChanges();
            }
        }

        public static void ChangeCSRAAmount(
            Guid applicantId,
            decimal CSRAAmount,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, CSRAAmount, session.Id })}");

            //ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var applicant = db.ApplicantsNew.Single(s => s.Id == applicantId);
                applicant.CSRAAmount = CSRAAmount;
                UpdateApplicantAccessDateObject(applicant, db);

                InsertAction(applicantId, db, Models.ReportActions.ApplicantChangeCSRAAmount, CSRAAmount.ToString());

                db.SaveChanges();
            }
        }

        public static void ChangeApplicantRestartDate(
            Guid applicantId,
            DateTime restartDate,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, restartDate, session.Id })}");

            //ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var applicant = db.ApplicantsNew.Single(s => s.Id == applicantId);
                applicant.ApproxRestartDate = restartDate;
                UpdateApplicantAccessDateObject(applicant, db);

                InsertAction(applicantId, db, Models.ReportActions.ApplicantChangeApproxRestartDate, restartDate.ToString());

                db.SaveChanges();
            }
        }

        public static void ChangeApplicantMoveDate(
            Guid applicantId,
            DateTime moveDate,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, moveDate, session.Id })}");

            //ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var applicant = db.ApplicantsNew.Single(s => s.Id == applicantId);
                applicant.MoveDate = moveDate;
                UpdateApplicantAccessDateObject(applicant, db);

                InsertAction(applicantId, db, Models.ReportActions.ApplicantChangeMoveDate, moveDate.ToString());

                db.SaveChanges();
            }
        }

        public static void ChangeApplicantSubmitDateAndStatus(
            Guid applicantId,
            DateTime submitDate,
            Models.Documents.DocumentType documentType,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, submitDate, session.Id })}");

            using (var db = new DbModel())
            {
                var applicant = db.ApplicantsNew.Single(s => s.Id == applicantId);

                Models.ReportActions action;
                switch (documentType)
                {
                    case Models.Documents.DocumentType.de101:
                        applicant.RFISubmitDate = submitDate;
                        applicant.Status = 8; //DE 101 SUBMITTED
                        action = Models.ReportActions.ApplicantAutoChangeRFISubmitDate;
                        break;

                    case Models.Documents.DocumentType.de101_pas:
                        applicant.PASSubmitDate = submitDate;
                        applicant.Status = 13; //PAS SUBMITTED
                        action = Models.ReportActions.ApplicantAutoChangePASSubmitDate;
                        break;

                    case Models.Documents.DocumentType.de101_csra:
                        applicant.CSRASubmitDate = submitDate;
                        applicant.Status = 19; //CSRA SUBMITTED
                        action = Models.ReportActions.ApplicantAutoChangeCSRASubmitDate;
                        break;

                    default:
                        throw new Exception($"{documentType} not supported!");
                }


                UpdateApplicantAccessDateObject(applicant, db);

                InsertAction(applicantId, db, action, submitDate.ToString());

                db.SaveChanges();
            }
        }

        public static void ChangeApplicantException(
            Guid applicantid,
            string exceptionMessage,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { exceptionMessage, session.Id })}");

            using (var db = new DbModel())
            {
                if (string.IsNullOrWhiteSpace(exceptionMessage))
                    exceptionMessage = null;

                var applicant = db.ApplicantsNew.Single(s => s.Id.Equals(applicantid));
                applicant.ExceptionMessage = exceptionMessage;

                InsertAction(applicant.Id, db, Models.ReportActions.ApplicantChangeExceptionMessage, exceptionMessage);

                db.SaveChanges();
            }
        }

        public static void GetApplicantData(
            Guid applicantId,
            out ApplicantsNew applicant,
            out Models.Applicant.Applicant.FullApplicantInfo aplicantInfo)
        {
            using (var db = new DbModel())
            {
                applicant = db.ApplicantsNew.Single(s => s.Id == applicantId);
                aplicantInfo = JsonConvert.DeserializeObject<Models.Applicant.Applicant.FullApplicantInfo>(applicant.Data);
            }
        }

        public static void ChangeIncomeOnlyTrust(
            Guid applicantId,
            string incomeOnlyTrust,
            NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, incomeOnlyTrust, session.Id })}");

            //ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var applicant = db.ApplicantsNew.Single(s => s.Id == applicantId);
                applicant.IncomeOnlyTrust = incomeOnlyTrust;
                UpdateApplicantAccessDateObject(applicant, db);

                InsertAction(applicantId, db, Models.ReportActions.ApplicantChangeIncomeOnlyTrust, incomeOnlyTrust);

                db.SaveChanges();
            }
        }

        public static List<Countries> GetCountries(NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                return db.Countries.ToList();
            }
        }

        //////
        ///////// Doctors
        //////

        public static List<Guid> GetDoctorsByApplicantId(Guid applicantId, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                return db.Applicants_ApplicantsDoctors
                    .Where(w => w.ApplicantId == applicantId).Select(s => s.DoctorId)
                    .ToList();
            }
        }

        public static void UpdateDoctorsByApplicantId(List<Guid> doctorsIds, Guid applicantId, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { doctorsIds, applicantId, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                logger.Info("Delete all doctors related data");
                db.Applicants_ApplicantsDoctors.RemoveRange(db.Applicants_ApplicantsDoctors.Where(w => w.ApplicantId.Equals(applicantId)));

                logger.Info("Generate objects");
                var appDocs = new List<Applicants_ApplicantsDoctors>();
                foreach (Guid doctorId in doctorsIds)
                {
                    appDocs.Add(new Applicants_ApplicantsDoctors
                    {
                        ApplicantId = applicantId,
                        CreatedByUserId = GetUserId(),
                        CreatedOn = DateTime.Now,
                        DoctorId = doctorId
                    });
                }

                logger.Info("Add and save objects to db");
                db.Applicants_ApplicantsDoctors.AddRange(appDocs);
                db.SaveChanges();
            }
        }


        public static List<ApplicantsDoctors> GetAllDoctors(NLog.Logger logger)
        {
            logger.Info("== GetAllDoctors");

            using (var db = new DbModel())
            {
                return db.ApplicantsDoctors.ToList();
            }
        }

        public static void UpdateDoctor(ApplicantsDoctors doctor, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { doctor, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var dbDoctor = db.ApplicantsDoctors.Single(s => s.Id == doctor.Id);

                dbDoctor.Name = doctor.Name;
                dbDoctor.OfficeFax = doctor.OfficeFax;
                dbDoctor.OfficeTelephoneNumber = doctor.OfficeTelephoneNumber;
                dbDoctor.Address = doctor.Address;
                dbDoctor.Email = doctor.Email;
                dbDoctor.Type = doctor.Type;

                db.SaveChanges();
            }
        }

        public static ApplicantsDoctors GetDoctor(Guid doctorId, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { doctorId })}");

            using (var db = new DbModel())
            {
                return db.ApplicantsDoctors.Single(s => s.Id == doctorId);
            }
        }

        public static void CreateDoctor(ApplicantsDoctors doctor, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { doctor, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                doctor.CreatedByUserId = GetUserId();
                doctor.CreationDate = DateTime.Now;

                if (doctor.Id.Equals(Guid.Empty))
                    doctor.Id = Guid.NewGuid();

                db.ApplicantsDoctors.Add(doctor);
                db.SaveChanges();
            }
        }

        //////
        ///////// Representatives
        //////
        public static List<ApplicantsRepresentatives> GetAllRepresentatives(NLog.Logger logger)
        {
            logger.Info("== GetAllRepresentatives");

            using (var db = new DbModel())
            {
                return db.ApplicantsRepresentatives.ToList();
            }
        }

        public static ApplicantsRepresentatives LoadRepresentative(Guid representativeId, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { representativeId })}");

            using (var db = new DbModel())
            {
                return db.ApplicantsRepresentatives.Single(s => s.Id == representativeId);
            }
        }

        public static void RemoveRepresentative(Guid representativeId, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { representativeId })}");

            using (var db = new DbModel())
            {
                db.ApplicantsRepresentatives.Remove(db.ApplicantsRepresentatives.Single(s => s.Id == representativeId));
                db.SaveChanges();
            }
        }

        public static void InsertRepresentative(ApplicantsRepresentatives representative, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { representative, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                db.ApplicantsRepresentatives.Add(representative);
                db.SaveChanges();
            }
        }

        public static void UpdateRepresentative(ApplicantsRepresentatives representative, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { representative, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var dbrep = db.ApplicantsRepresentatives.Single(s => s.Id == representative.Id);

                dbrep.Data = representative.Data;
                dbrep.RepresentativeEmail = representative.RepresentativeEmail;
                dbrep.RepresentativeName = representative.RepresentativeName;
                dbrep.Status = representative.Status;
                dbrep.Type = representative.Type;

                db.SaveChanges();
            }
        }

        public static List<Models.DTOs.EmailReportDTO> LoadSentEmails(Guid? applicantId, NLog.Logger logger)
        {
            using (var db = new DbModel())
            {
                var sendEmails =
                    //from actions in db.ApplicantsActions
                    from eMails in db.ApplicantsActionsEmailDetails //on actions.CorrelationId equals eMails.CorrelationId
                    from responsibleUser in db.Users.Where(w => w.Id == eMails.UserId).DefaultIfEmpty()
                    from applicant in db.ApplicantsNew.Where(w => w.Id == eMails.ApplicantId).DefaultIfEmpty()
                        //where actions.ActionId == (int)Models.ReportActions.SendEmail
                    orderby eMails.Created descending
                    select new Models.DTOs.EmailReportDTO()
                    {
                        ApplicantId = eMails.ApplicantId,
                        Body = eMails.Body,
                        CorrelationId = eMails.CorrelationId,
                        Created = eMails.Created,
                        ErrorMessage = eMails.ErrorMessage,
                        Id = eMails.Id,
                        OtherFiles = eMails.OtherFiles,
                        ResponsibleUser = responsibleUser.UserName,
                        ApplicantName = applicant.Name,
                        Result = eMails.Result,
                        Subject = eMails.Subject,
                        To = eMails.To,
                        UserId = eMails.UserId,
                        EmailTypeId = eMails.EmailTypeId,
                        ScheduledFor = eMails.ScheduledFor
                    };

                if (applicantId.HasValue)
                {
                    sendEmails = sendEmails.Where(w => w.ApplicantId == applicantId.Value);
                }

                return sendEmails.ToList();
            }
        }

        public static List<Models.DTOs.FaxReportDTO> LoadSentFaxes(Guid? applicantId, NLog.Logger logger)
        {
            using (var db = new DbModel())
            {
                var sendEmails =
                    from fax in db.ApplicantsActionsFaxDetails
                    from responsibleUser in db.Users.Where(w => w.Id == fax.UserId).DefaultIfEmpty()
                    from applicant in db.ApplicantsNew.Where(w => w.Id == fax.ApplicantId).DefaultIfEmpty()
                    select new Models.DTOs.FaxReportDTO()
                    {
                        ApplicantId = fax.ApplicantId,
                        CorrelationId = fax.CorrelationId,
                        Created = fax.Created,
                        ErrorMessage = fax.ErrorMessage,
                        Id = fax.Id,
                        ResponsibleUser = responsibleUser.UserName,
                        ApplicantName = applicant.Name,
                        Result = fax.Result,
                        To = fax.To,
                        UserId = fax.UserId,
                        BlobFileName = fax.BlobFileName, //This is from the public container BECAUSE of the fax thing
                        FileName = fax.FileName,
                        FileType = fax.FileType,
                        Price = fax.Price,
                        Uri = fax.Uri //THIS is the fax public url accessable address
                    };

                if (applicantId.HasValue)
                {
                    sendEmails = sendEmails.Where(w => w.ApplicantId == applicantId.Value);
                }

                return sendEmails.ToList();
            }
        }

        public static List<Models.DTOs.ApplicantActionDTO> LoadActions(Guid? applicantId, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { })}");

            ValidateSession(true, logger);

            using (var db = new DbModel())
            {
                db.Database.Log = s => logger.Info(s);

                var allActions =
                    (from action in db.ApplicantsActions
                     from responsibleUser in db.Users.Where(w => w.Id == action.UserId).DefaultIfEmpty()
                     from applicant in db.ApplicantsNew.Where(w => w.Id == action.ApplicantId).DefaultIfEmpty()
                     orderby action.Created descending
                     select new Models.DTOs.ApplicantActionDTO
                     {
                         ActionId = action.ActionId,
                         ApplicantId = (applicant == null ? Guid.Empty : applicant.Id),
                         applicantName = (applicant == null ? "none" : applicant.Name),
                         CorrelationId = action.CorrelationId,
                         Created = action.Created,
                         Data = action.Data.Substring(0, Math.Min(action.Data.Length, 256)),
                         Id = action.Id,
                         responsibleUserName = responsibleUser == null ? "none" : responsibleUser.UserName,
                         UserId = action.UserId
                     });

                if (applicantId.HasValue)
                {
                    allActions = allActions.Where(w => w.ApplicantId == applicantId.Value);
                }
                else
                {
                    allActions = allActions.OrderByDescending(o => o.Created).Take(500);
                }

                return allActions.ToList();
            }
        }

        public static void InsertAction(
            NLog.Logger logger,
            Guid applicantId,
            Models.ReportActions reportAction,
            Guid? correlationId = null,
            string data = null
            )
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, reportAction, correlationId, data })}");

            using (var db = new DbModel())
            {
                db.ApplicantsActions.Add(new ApplicantsActions()
                {
                    Id = Guid.NewGuid(),
                    ActionId = (int)reportAction,
                    Data = data,
                    UserId = GetUserId(),
                    ApplicantId = applicantId,
                    CorrelationId = correlationId,
                    Created = DateTime.Now
                });

                db.SaveChanges();
            }
        }

        public static void InsertActions(
            NLog.Logger logger,
            Guid applicantId,
            Models.ReportActions reportAction,
            List<Guid> correlationsId = null,
            List<string> datas = null
            )
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, reportAction, correlationsId, datas })}");

            if (correlationsId?.Count != datas?.Count)
                throw new Exception("Multiple InsertActions params mismatch!");

            using (var db = new DbModel())
            {
                for (int i = 0; i < correlationsId.Count; i++)
                {
                    db.ApplicantsActions.Add(new ApplicantsActions()
                    {
                        Id = Guid.NewGuid(),
                        ActionId = (int)reportAction,
                        Data = datas[i],
                        UserId = GetUserId(),
                        ApplicantId = applicantId,
                        CorrelationId = correlationsId[i]
                    });
                }

                db.SaveChanges();
            }
        }

        public static ApplicantsActionsEmailDetails InsertActionEmail(
            NLog.Logger logger,
            ApplicantsActionsEmailDetails entity,
            Models.ReportActions reportAction,
            Guid userId
            )
        {
            entity.UserId = userId;
            entity.Id = Guid.NewGuid();
            entity.Created = DateTime.Now;

            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { entity })}");

            using (var db = new DbModel())
            {
                db.ApplicantsActionsEmailDetails.Add(entity);

                Db.InsertAction(
                    logger,
                    entity.ApplicantId,
                    reportAction,
                    entity.Id);

                db.SaveChanges();
            }

            return entity;
        }

        public static ApplicantsActionsFaxDetails InsertActionFax(
            NLog.Logger logger,
            ApplicantsActionsFaxDetails entity
            )
        {
            entity.UserId = GetUserId();
            entity.Id = Guid.NewGuid();
            entity.Created = DateTime.Now;

            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { entity })}");

            using (var db = new DbModel())
            {
                db.ApplicantsActionsFaxDetails.Add(entity);
                db.SaveChanges();
            }

            return entity;
        }

        public static void ValidateSession(bool validateAdminPermission, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { validateAdminPermission })}");

            using (var db = new DbModel())
            {
                user = db.Users.Where(w => w.Id == user.Id).SingleOrDefault();

                if (user == null) throw new Exception("Invalid user, login again!");
                if (user.Enabled != 1) throw new Exception("This user is not enabled, contact Max");

                session = db.Sessions.SingleOrDefault(s => s.Id == session.Id);
                if (session == null) throw new Exception("Invalid session, login again!");

                if (validateAdminPermission && user.IsAdmin != 1)
                {
                    //disable account
                    db.Users.Where(w => w.Id == user.Id).Single().Enabled = 0;
                    db.SaveChanges();
                    throw new Exception("This operation requires an Admin account! This account is now locked.");
                }
            }
        }

        public static void ApplicantChangeStatus(Guid applicantId, int statusTypeId, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, statusTypeId })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                ApplicantsNew newAppData = db.ApplicantsNew.Single(s => s.Id == applicantId);
                newAppData.Status = statusTypeId;
                newAppData.UpdatedByUserId = GetUserId();
                newAppData.UpdatedOn = DateTime.Now;

                InsertAction(logger, applicantId, Models.ReportActions.ApplicantStatusChange, data: JsonConvert.SerializeObject(statusTypeId));

                db.SaveChanges();
            }
        }

        public static void InsertApplicant(ApplicantsNew applicant, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicant, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                applicant.UpdatedOn = DateTime.Now;
                applicant.CreatedOn = DateTime.Now;
                applicant.UpdatedOn = DateTime.Now;
                applicant.UpdatedByUserId = GetUserId();

                db.ApplicantsNew.Add(applicant);
                db.SaveChanges();
            }
        }

        public static ApplicantsNew LoadApplicant(Guid applicantId, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                return db.ApplicantsNew.Single(s => s.Id == applicantId);
            }
        }

        public static void UpdateApplicant(ApplicantsNew applicant, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicant, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var applicantDb = db.ApplicantsNew.Single(s => s.Id == applicant.Id);

                applicantDb.Name = applicant.Name;
                applicantDb.Data = applicant.Data;
                applicantDb.Status = applicant.Status;
                applicantDb.UpdatedOn = DateTime.Now;
                applicantDb.UpdatedByUserId = GetUserId();
                applicantDb.RepresentativeId = applicant.RepresentativeId;
                applicantDb.MedicalRepresentativeId = applicant.MedicalRepresentativeId;
                applicantDb.Tasks = applicant.Tasks;
                applicantDb.ScheduledInfo = applicant.ScheduledInfo;

                applicantDb.FinancialWorkerEmail = applicant.FinancialWorkerEmail;
                applicantDb.FinancialWorkerName = applicant.FinancialWorkerName;
                applicantDb.FinancialWorkerPhoneNumber = applicant.FinancialWorkerPhoneNumber;

                applicantDb.MedicalWorkerEmail = applicant.MedicalWorkerEmail;
                applicantDb.MedicalWorkerName = applicant.MedicalWorkerName;
                applicantDb.MedicalWorkerPhoneNumber = applicant.MedicalWorkerPhoneNumber;

                db.SaveChanges();
            }
        }

        public static void UpdateApplicantData(Guid applicantId, string applicantData, NLog.Logger logger)
        {            
            using (var db = new DbModel())
            {
                var applicantDb = db.ApplicantsNew.Single(s => s.Id == applicantId);

                applicantDb.Data = applicantData;                
                applicantDb.UpdatedOn = DateTime.Now;
                applicantDb.UpdatedByUserId = GetUserId();


                db.SaveChanges();
            }
        }

        public static void UpdateApplicantTask(Guid applicantId, string tasks, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var applicantDb = db.ApplicantsNew.Single(s => s.Id == applicantId);

                applicantDb.Tasks = tasks;
                applicantDb.UpdatedOn = DateTime.Now;
                applicantDb.UpdatedByUserId = GetUserId();

                db.SaveChanges();
            }
        }

        public static void UpdateApplicantScheduledInfo(Guid applicantId, string scheduledInfo, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantId, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {
                var applicantDb = db.ApplicantsNew.Single(s => s.Id == applicantId);

                applicantDb.ScheduledInfo = scheduledInfo;
                applicantDb.UpdatedOn = DateTime.Now;
                applicantDb.UpdatedByUserId = GetUserId();

                db.SaveChanges();
            }
        }

        public static Guid CreateApplicantFromSource(
            string stripeApiKey,
            NLog.Logger logger,
            string json,
            Customer customer,
            out FullApplicantInfo convertedApplicantData)
        {
            using (var db = new DbModel())
            {
                //Get representative
                logger.Info("Start converting...");
                Shared.ApplicantsRepresentatives represenrtative = db.ApplicantsRepresentatives.Single(s => s.Id.Equals(new Guid(Resources.RepresentativeId_Jacob)));
                logger.Info("represenrtative loaded successfully");

                ApplicantsApp.Models.Representative.RepresentativeInfo repInfo
                    = JsonConvert.DeserializeObject<ApplicantsApp.Models.Representative.RepresentativeInfo>(represenrtative.Data);
                logger.Info("represenrtative Info loaded successfully");

                int status;
                //GetConvert applicant
                if (json != null)
                {
                    status = 3; //NEW
                    convertedApplicantData = GravityConverter.FillApplicantModelFromGravityForm(json, repInfo, represenrtative.Id, logger);
                }
                else
                if (customer != null)
                {
                    status = 20; //Z_STRIPE_CREATED
                    string[] names = customer.Name.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    convertedApplicantData = new FullApplicantInfo()
                    {
                        autoLiteRFIEmailSettings = new Models.Applicant.Applicant.AutoEmails.AutoSmaillRFIEmailSettings()
                        {
                            enabled = true
                        },

                        personalInfo = new PersonalInfo()
                        {
                            applicantAddress = new Models.Applicant.Applicant.Common.Address()
                            {
                                country = "United States",
                                state = customer.Address?.State,
                                zip = customer.Address?.PostalCode,
                                steerAddress = customer.Address?.Line1,
                                addressLine2 = customer.Address?.Line2,
                                city = customer.Address?.City
                            },
                            applicantMailingAddress = new Models.Applicant.Applicant.Common.Address()
                            {
                                country = "United States",
                                state = customer.Shipping?.Address?.State,
                                zip = customer.Shipping?.Address?.PostalCode,
                                steerAddress = customer.Shipping?.Address?.Line1,
                                addressLine2 = customer.Shipping?.Address?.Line2,
                                city = customer.Shipping?.Address?.City,
                            },
                            detailedPersonalInfo = new DetailedPersonalInfo()
                            {
                                email = customer.Email,
                                phone = customer.Phone,
                                firstName = names[0],
                                middleName = names.Count() > 2 ? names[1] : string.Empty,
                                lastName = names.Count() > 2 ? names[2] : names[1],
                            }
                        }
                    };
                }
                else
                {
                    throw new Exception("Both customer and applicant gravity json are null!");
                }

                logger.Info("applicant converted");

                ApplicantsNew appilcantDb = null;
                Guid applicantId = Guid.NewGuid();
                string customerStripeId = null;

                if (json != null)
                {
                    JObject jo = JObject.Parse(json);
                    customerStripeId = jo["Customer Stripe Id"]?.ToString()?.Trim();
                    if (customerStripeId != null)
                    {
                        appilcantDb = db.ApplicantsNew.SingleOrDefault(s => s.StripeCustomerId == customerStripeId);
                        if (appilcantDb != null)
                        {
                            applicantId = appilcantDb.Id;

                            appilcantDb.Data = JsonConvert.SerializeObject(convertedApplicantData);
                            appilcantDb.Status = status;
                            appilcantDb.UpdatedOn = DateTime.Now;
                            appilcantDb.Name = convertedApplicantData.personalInfo.detailedPersonalInfo.GetFullName();
                        }
                    }
                }

                if (appilcantDb == null)
                {
                    appilcantDb = new ApplicantsNew()
                    {
                        CreatedByUserId = new Guid("00000000-0000-0000-0000-000000000001"),
                        CreatedOn = DateTime.Now,
                        Data = JsonConvert.SerializeObject(convertedApplicantData),
                        Id = applicantId,
                        Name = convertedApplicantData.personalInfo.detailedPersonalInfo.GetFullName(),
                        RepresentativeId = represenrtative.Id,
                        MedicalRepresentativeId = new Guid("0AA58D6E-B156-491C-84C3-92F8DE84F6C5"),
                        Status = status,
                        UpdatedByUserId = new Guid("00000000-0000-0000-0000-000000000001"),
                        UpdatedOn = DateTime.Now,
                    };

                    try
                    {
                        if (json != null)
                        {
                            if (string.IsNullOrWhiteSpace(customerStripeId))
                            {
                                customerStripeId = Shared.Source.Stripe.CreateCustomer(stripeApiKey, convertedApplicantData, applicantId);
                            }

                            appilcantDb.StripeCustomerId = customerStripeId;
                        }
                        else if (customer != null)
                        {
                            appilcantDb.StripeCustomerId = customer.Id;
                        }

                    }
                    catch (Exception ex)
                    {
                        Shared.Source.Mop.NotifyMeOnException("Gravity Stripe Create", ex, logger, false, "");
                    }

                    db.ApplicantsNew.Add(appilcantDb);
                }

                db.SaveChanges();


                //Handle AUTO emails HERE
                bool RFIDiff = convertedApplicantData?.autoRFIEmailSettings?.enabled ?? false;
                bool interimDiff = convertedApplicantData?.autoInterimEmailSettings?.enabled ?? false;
                bool smallRFIDiff = convertedApplicantData?.autoLiteRFIEmailSettings?.enabled ?? false;
                Shared.Source.MailGeneration.EmailProcessor.ResheduleAllAutomaticEmails(RFIDiff, interimDiff, smallRFIDiff, applicantId, logger, convertedApplicantData);

                return applicantId;
            }
        }

        public static List<Models.DTOs.ApplicantNewDTO> LoadApplicantsNew(int applicantStatusTypeId, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { applicantStatusTypeId, session.Id })}");

            ValidateSession(false, logger);

            using (var db = new DbModel())
            {

                var allApplicants =
                    from app in db.ApplicantsNew
                    from createdUser in db.Users.Where(w => w.Id == app.CreatedByUserId).DefaultIfEmpty()
                    from updatedUser in db.Users.Where(w => w.Id == app.UpdatedByUserId).DefaultIfEmpty()
                    from docs in db.ApplicantsDocuments
                       .Where(w =>
                           w.ApplicantId.Equals(app.Id)
                           && w.DocumentCategoryId != null
                           && w.DocumentExtraStatus == (int)Models.Documents.DocumentExtraStatus.justPlaceHolder
                           && w.Status == (int)Models.Documents.DocumentStatus.active
                           )
                       .GroupBy(g => g.ApplicantId)
                       .DefaultIfEmpty()
                    orderby app.UpdatedOn descending
                    select new Models.DTOs.ApplicantNewDTO
                    {
                        CreatedByUserId = app.CreatedByUserId,
                        createdByUserName = createdUser.UserName,
                        updatedByUserName = updatedUser.UserName,
                        CreatedOn = app.CreatedOn,
                        Id = app.Id,
                        Name = app.Name,
                        Status = app.Status,
                        UpdatedByUserId = updatedUser.Id,
                        UpdatedOn = app.UpdatedOn,
                        requiredDocuments = docs.Count(),
                        DueDate = app.DueDate ?? new DateTime(2000, 1, 1),
                        StripeCustomerId = app.StripeCustomerId,
                        Data = null,  //!!! Save data, we don't need it in the moment,                                  
                    };

                return allApplicants.ToList();
            }
        }

        public static void LoadSessionRelatedInfo(Guid sessionId, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { sessionId })}");

            using (var db = new DbModel())
            {
                Db.session = db.Sessions.SingleOrDefault(s => s.Id == sessionId);
                if (Db.session == null)
                {
                    throw new Exception("Invalid session, login again!");
                }

                Db.user = db.Users.SingleOrDefault(s => s.Id == Db.session.UserId);
                if (Db.user == null)
                {
                    throw new Exception("Invalid user, login again!");
                }

                if (Db.user.Enabled != 1)
                {
                    throw new Exception("This user is not enabled, contact Max");
                }

                Db.user.LastLogin = DateTime.Now;
                db.SaveChanges();
            }
        }

        public static void Login(string userName, string password, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { userName })}");

            using (var db = new DbModel())
            {
                userName = userName.Trim();
                string hash = Crypto.SHA512.ComputeHash(password);

                Users user = db.Users.SingleOrDefault(w => w.UserName == userName && w.PasswordHash == hash);
                if (user == null)
                {
                    throw new Exception("Invalid username or password!");
                }

                if (user.Enabled != 1)
                {
                    throw new Exception("This user is not enabled, contact Max");
                }

                user.LastLogin = DateTime.Now;
                Sessions session = db.Sessions.SingleOrDefault(w => w.UserId == user.Id);
                if (session == null)
                {
                    session = db.Sessions.Add(new Sessions()
                    {
                        Created = DateTime.Now,
                        Id = Guid.NewGuid(),
                        UserId = user.Id
                    });
                }

                db.SaveChanges();

                Db.user = user;
                Db.session = session;
            }
        }


    }
}
