﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Source
{
    public static class Resources
    {
        public const string RepresentativeId_Jacob = "0DA30150-C7DB-4B50-80E7-AF522F85D4FE";

        public const string USADateTime = "MM'/'dd'/'yyyy hh:mm tt";
        public const string USADateOnly = "MM'/'dd'/'yyyy";
        public const string USATimeOnly = "hh:mm tt";
    }
}
