﻿using Nager.Date.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Source
{
    public static class Mop
    {
        private const string TempPath = @".\temp";
        private static char DEICMAL_SEPARATOR = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0];

        public static ApplicantsApp.Models.ReportActions GetActionFromEmailType(ApplicantsApp.Models.Communication.EmailTypes emailType)
        {
            ApplicantsApp.Models.ReportActions action;

            switch (emailType)
            {
                case ApplicantsApp.Models.Communication.EmailTypes.RFI:
                    action = ApplicantsApp.Models.ReportActions.SendManualRFIEmail;
                    break;

                case ApplicantsApp.Models.Communication.EmailTypes.AutoRFI:
                    action = ApplicantsApp.Models.ReportActions.SendAutomaticRFIEmail;
                    break;

                case ApplicantsApp.Models.Communication.EmailTypes.FreeText:
                    action = ApplicantsApp.Models.ReportActions.SendManualEmail;
                    break;

                case ApplicantsApp.Models.Communication.EmailTypes.PasAssssorAutoRequest:
                    action = ApplicantsApp.Models.ReportActions.ApplicantEmailSendToAskForPASassssor_AUTO;
                    break;

                case ApplicantsApp.Models.Communication.EmailTypes.LiteRFI:
                    action = ApplicantsApp.Models.ReportActions.SendAutomaticLiteRFIEmail;
                    break;

                default:
                    throw new Exception($"Can't match email type {emailType} with reportType");
            }

            return action;
        }

        public static ApplicantsApp.Models.Communication.EmailTypes GetEmailTypeFromAction(ApplicantsApp.Models.ReportActions action)
        {
            ApplicantsApp.Models.Communication.EmailTypes emailType;

            switch (action)
            {
                case ApplicantsApp.Models.ReportActions.SendManualRFIEmail:
                    emailType = ApplicantsApp.Models.Communication.EmailTypes.RFI;
                    break;

                case ApplicantsApp.Models.ReportActions.SendAutomaticRFIEmail:
                    emailType = ApplicantsApp.Models.Communication.EmailTypes.AutoRFI;
                    break;

                case ApplicantsApp.Models.ReportActions.SendManualEmail:
                    emailType = ApplicantsApp.Models.Communication.EmailTypes.FreeText;
                    break;

                case ApplicantsApp.Models.ReportActions.SendAutomaticInterimEmail:
                    emailType = ApplicantsApp.Models.Communication.EmailTypes.AutoInterim;
                    break;

                case ApplicantsApp.Models.ReportActions.SendNotPayedInvoiceEmail:
                    emailType = ApplicantsApp.Models.Communication.EmailTypes.NotPayedInvoice;
                    break;

                case ApplicantsApp.Models.ReportActions.CallCaringComAPI:
                    emailType = ApplicantsApp.Models.Communication.EmailTypes.CaringComAPICall;
                    break;

                case ApplicantsApp.Models.ReportActions.ApplicantEmailSendToAskForPASassssor:
                    emailType = ApplicantsApp.Models.Communication.EmailTypes.PasAssssorAutoRequest;
                    break;

                case ApplicantsApp.Models.ReportActions.ApplicantEmailSendToAskForPASassssor_AUTO:
                    emailType = ApplicantsApp.Models.Communication.EmailTypes.PasAssssorAutoRequest;
                    break;

                case ApplicantsApp.Models.ReportActions.ApplicantEmailSendToConfirmMedicalExac:
                    emailType = ApplicantsApp.Models.Communication.EmailTypes.MedicalEvaluationConfirmation;
                    break;

                case ApplicantsApp.Models.ReportActions.SendAutomaticLiteRFIEmail:
                    emailType = ApplicantsApp.Models.Communication.EmailTypes.LiteRFI;
                    break;

                default:
                    throw new Exception($"Can't match email type {action} with reportType");
            }

            return emailType;
        }

        public  static string GetCategoryDescriptionWithRespectToCustom(
            int rfiCategoryId,
            List<ApplicantsApp.Models.Documents.CustomCategoryDescription> customCategoryDescriptions,
            List<Applicants_DocumentCategories> documentCategories
            )
        {
            ApplicantsApp.Models.Documents.CustomCategoryDescription customCategory = customCategoryDescriptions?.SingleOrDefault(s => s.categoryId.Equals(rfiCategoryId));
            if (customCategory != null)
            {
                return customCategory.categoryDescription;
            }

            Applicants_DocumentCategories selectedCategory = documentCategories.Single(s => s.Id == rfiCategoryId);
            return selectedCategory.Description;
        }

        public static List<string> EnumerateAnEnum(Type type)
        {
            var values = new List<string>();
            foreach (var en in Enum.GetValues(type))
            {
                values.Add(en.ToString());
            }

            return values;
        }

        public static string MakeValidFileName(string name)
        {
            string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(Path.GetInvalidFileNameChars()));
            string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return System.Text.RegularExpressions.Regex.Replace(name, invalidRegStr, "_");
        }

        public static bool GetBoolFromGravityForm(JObject jo, string key, NLog.Logger logger)
        {
            string boolValue = jo?[key]
                ?.ToString()
                ?.ToLowerInvariant()
                ?.Trim();

            if (boolValue == "no") return false;
            if (boolValue == "yes") return true;

            if (string.IsNullOrWhiteSpace(boolValue))
            {
                logger.Warn($"'{key}' is not provided, automatically putting 'No'");
                return false;
            }

            logger.Warn($"'{key}' has strange value of '{boolValue}' while we expect 'Yes' or 'No'. Automatically putting 'No'");
            return false;
        }

        public static DateTime GetDateTimeFromGravityForm(JObject jo, string key, NLog.Logger logger)
        {
            string dateValue = jo?[key]
                ?.ToString()
                ?.Trim();

            if (string.IsNullOrWhiteSpace(dateValue))
            {
                logger.Info($"'{key}' is not provided, automatically putting '01/01/1900'");
                return new DateTime(1900, 1, 1);
            }

            if (DateTime.TryParseExact(dateValue, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out DateTime dateOfBirth))
            {
                return dateOfBirth;
            }
            else
            {
                logger.Info($"'{key}', passed as '{dateValue}' is not in the expected 'yyyy-MM-dd' format, automatically putting 01/01/1900");
                return new DateTime(1900, 1, 1);
            }
        }

        public static bool GetMultiCheckFromGravityForm(JObject jo, string key, string lookFor)
        {
            string dateValue = jo?[key]
                ?.ToString()
                ?.Trim()
                ?.ToLowerInvariant();

            if (string.IsNullOrWhiteSpace(dateValue)) return false;

            string[] parts = dateValue.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var part in parts)
            {
                if (part.Trim().ToLowerInvariant().Equals(lookFor.Trim().ToLowerInvariant()))
                    return true;
            }

            return false;
        }

        public static decimal? GetDecimalFromGravityForm(
            JObject jo,
            string key,
            NLog.Logger logger,
            decimal? defaultEmptyValue = null)
        {
            string value = jo?[key]
                ?.ToString()
                ?.Trim()
                ?.ToLowerInvariant();

            if (string.IsNullOrWhiteSpace(value))
            {
                
                    logger.Warn($"'{key}' is not provided!, automatically putting '{defaultEmptyValue}'");
                

                return defaultEmptyValue;
            }

            value = string.Concat(value?.Where(c => char.IsNumber(c) || c == DEICMAL_SEPARATOR) ?? "");

            if (decimal.TryParse(value, out decimal decimalValue))
            {
                return decimalValue;
            }
            else
            {
                logger.Warn($"'{key}', passed as '{value}' is not in the expected decimal format, automatically putting '{defaultEmptyValue}'");

                return defaultEmptyValue;
            }
        }

        public static string GetTempFileName(string prefix = "", string exactFileName = "", string extension = "")
        {
            if (!Directory.Exists(TempPath))
            {
                Directory.CreateDirectory(TempPath);
            }

            string targetFileName;

            if (!string.IsNullOrWhiteSpace(exactFileName))
            {
                targetFileName = Path.GetFileName(exactFileName);
            }
            else
            {
                extension = extension?.Trim();

                if (!string.IsNullOrWhiteSpace(extension))
                {
                    if (!extension.StartsWith("."))
                        extension = "." + extension;
                }

                targetFileName = prefix + Guid.NewGuid().ToString() + extension;
            }

            return Path.GetFullPath(Path.Combine(TempPath, targetFileName));
        }

        public static bool IsEmailValid(string email, bool treatEmptyAsOkay = false)
        {
            if (String.IsNullOrWhiteSpace(email)) return treatEmptyAsOkay;
            if (email.Length < 6) return false;
            if (email.IndexOf("@") <= 0) return false;
            if (email.IndexOf(".") <= 0) return false;
            if (email.EndsWith(".")) return false;
            if (email.EndsWith("@")) return false;
            if (email.StartsWith("@")) return false;

            return true;
        }

        public static string ForceValidDir(string folderName)
        {
            if (string.IsNullOrEmpty(folderName)) return folderName;

            foreach (var c in Path.GetInvalidFileNameChars())
                folderName = folderName.Replace(c.ToString(), string.Empty);

            foreach (var c in Path.GetInvalidPathChars())
                folderName = folderName.Replace(c.ToString(), string.Empty);

            return folderName;
        }

        public static void NotifyMeOnException(string module, Exception ex, NLog.Logger logger, bool raiseOnError, string addInfo)
        {
            try
            {
                string res = Mop.SendGmail(
                    "iwm2stuff@gmail.com",
                    "iwm2stuff@gmail.com",
                    null,
                    $"ERROR__ApplicantsApp {module}",
                    addInfo + "      \n\n\n\n\n" + JsonConvert.SerializeObject(ex, Formatting.Indented),
                    false,
                    new List<string>(),
                    logger,
                    false,
                    null,
                    false
                    );

                if (!string.IsNullOrWhiteSpace(res))
                {
                    throw new Exception(res);
                }
            }
            catch (Exception ex2)
            {
                logger.Error(ex2);
                if (raiseOnError)
                {
                    throw;
                }                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="module"></param>
        /// <param name="ex"></param>
        /// <param name="logger"></param>
        /// <param name="raiseOnError"></param>
        public static void NotifyMe(string module, string body, NLog.Logger logger, bool raiseOnError)
        {
            try
            {
                string res = Mop.SendGmail(
                    "iwm2stuff@gmail.com",
                    "iwm2stuff@gmail.com",
                    null,
                    $"NOTIFY {module}",
                    body,
                    true,
                    new List<string>(),
                    logger,
                    false,
                    null,
                    true
                    );

                if (!string.IsNullOrWhiteSpace(res))
                {
                    throw new Exception(res);
                }
            }
            catch (Exception ex2)
            {
                logger.Error(ex2);
                if (raiseOnError)
                {
                    throw;
                }
            }
        }

        public static DateTime GetNextBusinessDay()
        {
            var date = DateTime.Now; //Set start date
            var countryCode = Nager.Date.CountryCode.US; //Set country

            do
            {
                date = date.AddDays(1);
            } while (Nager.Date.DateSystem.IsPublicHoliday(date, countryCode) || date.IsWeekend(countryCode));

            return date;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromAddress"></param>
        /// <param name="toAddress"></param>
        /// <param name="password"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isBodyHtml"></param>
        /// <param name="attachments"></param>
        /// <param name="logger"></param>
        /// <param name="addFooter"></param>
        /// <returns></returns>
        public static string SendGmail(
            string fromAddress,
            string toAddress,
            string password,
            string subject,
            string body,
            bool isBodyHtml,
            IEnumerable<string> attachments,
            NLog.Logger logger,
            bool addFooter,
            string footerHTML,
            bool makeNewRowsBR
            )
        {
            logger.Info("START Sending email");
            logger.Info("  from address:" + fromAddress);
            logger.Info("  to address:" + toAddress);
            logger.Info("  password:" + (password == null ? "" : Crypto.AES.Encrypt(password, "T1eM2GaS3cr#$tPassw@wrd")));
            logger.Info("  subject:" + subject);
            logger.Info("  body:" + body);
            logger.Info("  attachments:" + string.Join(",", attachments));

            try
            {
                var from = new MailAddress(fromAddress);
                var to = new MailAddress(toAddress);

                if (fromAddress == "iwm2stuff@gmail.com")
                    password = "qapftrkwcpwkyqko";

                using (var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress, password),
                    Timeout = 20000
                })
                {

                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = subject,
                        Body = body,
                        IsBodyHtml = isBodyHtml,
                    })
                    {
                        if (attachments != null && attachments.Count() > 0)
                        {
                            foreach (var attachment in attachments)
                            {
                                message.Attachments.Add(new Attachment(attachment));
                            }
                        }

                        if (addFooter)
                        {
                            message.Body += footerHTML;
                        }

                        if (makeNewRowsBR)
                        {
                            message.Body = message.Body
                                .Replace("\n\r", "<br>")
                                .Replace("\r\n", "<br>")
                                .Replace("\n", "<br>")
                                .Replace("\r", "<br>");
                        }

                        logger.Info("Before send");
                        smtp.Send(message);
                        logger.Info("After send");

                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                logger.Info("ERROR: " + ex.Message);
                return ex.Message;
            }
        }
    }
}
