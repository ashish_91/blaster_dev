﻿using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Source
{
    public static class Stripe
    {
        public const decimal TOTAL_INFINITY_TO_PAY_SUM = 2000000000.00m;
        public static PaymentIntent CreateManualPaymentIntent(string apiKey,
                                                     decimal ammount,
                                                     string customerId,
                                                     string desc,
                                                     string subscriptionId,
                                                     string planId,
                                                     string productId
            )
        {
            StripeConfiguration.ApiKey = apiKey;

            var service = new PaymentIntentService();

            return service.Create(new PaymentIntentCreateOptions()
            {
                Amount = (long)(ammount * 100m),
                Currency = "usd",
                Customer = customerId,
                Description = desc,
                Confirm = false,
                Metadata = new Dictionary<string, string>()
                {
                    { "custom_type", "manual_charge" },
                    { "customerId", customerId },
                    { "subscriptionId", subscriptionId },
                    { "planId", planId },
                    { "productId", productId },
                }
            });
        }

        public static Subscription CancelSubscriptionById(string apiKey, string subscriptionId)
        {
            StripeConfiguration.ApiKey = apiKey;

            var service = new SubscriptionService();

            return service.Cancel(subscriptionId, new SubscriptionCancelOptions()
            {
                InvoiceNow = false,
                Prorate = false,
            });
        }

        public static List<Subscription> GetAllSubscriptions(string apiKey)
        {
            StripeConfiguration.ApiKey = apiKey;
            var subscriptions = new List<Subscription>();

            var service = new SubscriptionService();

            return service.ListAutoPaging(new SubscriptionListOptions()
            {
                Limit = 100,
            }).ToList();
        }

        public static Subscription GetSubscriptionById(string apiKey, string subscriptionId)
        {
            StripeConfiguration.ApiKey = apiKey;

            var service = new SubscriptionService();

            return service.Get(subscriptionId);
        }

        public static Product GetProductById(string apiKey, string productId)
        {
            StripeConfiguration.ApiKey = apiKey;
            var service = new ProductService();
            return service.Get(productId);
        }

        public static List<Refund> GetRefunds(string apiKey, int daysAgo)
        {
            StripeConfiguration.ApiKey = apiKey;

            var service = new RefundService();
            return service.ListAutoPaging(new RefundListOptions()
            {
                Created = new AnyOf<DateTime?, DateRangeOptions>(new DateRangeOptions()
                {
                    GreaterThanOrEqual = DateTime.Now.AddDays(-daysAgo)
                })
            }).ToList();
        }

        public static List<Invoice> GetInvoicess(string apiKey, int daysAgo)
        {
            StripeConfiguration.ApiKey = apiKey;

            var service = new InvoiceService();
            return service.ListAutoPaging(new InvoiceListOptions()
            {
                Created = new AnyOf<DateTime?, DateRangeOptions>(new DateRangeOptions()
                {
                    GreaterThanOrEqual = DateTime.Now.AddDays(-daysAgo)
                })
            }).ToList();

            //incomplete, incomplete_expired, trialing, active, past_due, canceled, or unpaid
            //Possible values are incomplete, incomplete_expired, trialing, active, past_due, canceled, or unpaid.
            //For collection_method = charge_automatically a subscription moves into incomplete if the initial payment attempt fails. 
            //A subscription in this state can only have metadata and default_source updated. Once the first invoice is paid, the subscription moves into an active state.
            //If the first invoice is not paid within 23 hours, the subscription transitions to incomplete_expired.This is a terminal state, the open invoice will be 
            //voided and no further invoices will be generated.
            //A subscription that is currently in a trial period is trialing and moves to active when the trial period is over.
            //If subscription collection_method = charge_automatically it becomes past_due when payment to renew it fails and canceled or unpaid(depending on your subscriptions settings) 
            //when Stripe has exhausted all payment retry attempts.
            //If subscription collection_method = send_invoice it becomes past_due when its invoice is not paid by the due date, and canceled or unpaid if it is still not paid 
            //by an additional deadline after that.Note that when a subscription has a status of unpaid, no subsequent invoices will be attempted(invoices will be created, 
            //but then immediately automatically closed).After receiving updated payment information from a customer, you may choose to reopen and pay their closed invoices.
        }

        public static List<PaymentIntent> GetPaymentIntents(string apiKey, int daysAgo)
        {
            StripeConfiguration.ApiKey = apiKey;

            var service = new PaymentIntentService();            
            return service.ListAutoPaging(new PaymentIntentListOptions()
            {
                Created = new AnyOf<DateTime?, DateRangeOptions>(new DateRangeOptions()
                {
                    GreaterThanOrEqual = DateTime.Now.AddDays(-daysAgo),
                })
            }).ToList();
        }

        public static List<Invoice> GetUnpayedInvoicess(string apiKey)
        {
            StripeConfiguration.ApiKey = apiKey;

            var service = new InvoiceService();
            return service.ListAutoPaging(new InvoiceListOptions()
            {                
                Paid = false,
                Created = new AnyOf<DateTime?, DateRangeOptions>(new DateRangeOptions()
                {
                    GreaterThanOrEqual = new DateTime(2019, 10, 1)
                })
            }).ToList();
        }

        public static Subscription CreateSubscription(string apiKey, string customerId, string planId, int dayOfMonth)
        {
            DateTime now = DateTime.Now;
            DateTime nextInvoice = new DateTime(now.Year, now.Month, dayOfMonth);
            if (nextInvoice < DateTime.Now)
            {
                nextInvoice = nextInvoice.AddMonths(1);
            }

            StripeConfiguration.ApiKey = apiKey;

            var service = new SubscriptionService();

            List<SubscriptionItemOption> items = new List<SubscriptionItemOption>();
            items.Add(new SubscriptionItemOption()
            {
                Plan = planId,
                Quantity = 1,
            });

            return service.Create(new SubscriptionCreateOptions()
            {
                BillingCycleAnchor = nextInvoice,
                Customer = customerId,
                Items = items,
                Prorate = false
            });
        }

        public static Customer GetCustomer(string apiKey, string customerId)
        {
            StripeConfiguration.ApiKey = apiKey;

            var service = new CustomerService();
            return service.Get(customerId);
        }

        public static int GetSubscriptionsCountOfCustomerByProduct(string apiKey, string customerId, string productId)
        {
            StripeConfiguration.ApiKey = apiKey;

            var service = new SubscriptionService();
            var subscriptions = service.ListAutoPaging(new SubscriptionListOptions()
            {
                Customer = customerId,
            }).ToList();

            return subscriptions.Count(w => w.Plan.ProductId.Equals(productId));
        }

        public static List<Subscription> GetSubscriptionsOfCustomerByProduct(string apiKey, string customerId, string productId)
        {
            StripeConfiguration.ApiKey = apiKey;

            var service = new SubscriptionService();
            var subscriptions = service.ListAutoPaging(new SubscriptionListOptions()
            {
                Customer = customerId,
            }).ToList();

            return subscriptions
                .Where(w => w.Plan.ProductId.Equals(productId))
                .ToList();
        }

        public static List<Product> GetProducts(string apiKey)
        {
            StripeConfiguration.ApiKey = apiKey;

            var service = new ProductService();
            return service.ListAutoPaging().ToList();
        }

        public static List<Plan> GetPlans(string apiKey, string productId = null)
        {
            StripeConfiguration.ApiKey = apiKey;

            var service = new PlanService();
            if (productId != null)
            {
                return service.ListAutoPaging(new PlanListOptions()
                {
                    Product = productId
                }).ToList();
            }
            else
            {
                return service.ListAutoPaging().ToList();
            }
        }

        public static string CreateCustomer(string apiKey, ApplicantsApp.Models.Applicant.Applicant.FullApplicantInfo applicantInfo, Guid applicantId)
        {
            StripeConfiguration.ApiKey = apiKey;
            var options = new CustomerCreateOptions
            {
                Address = new AddressOptions()
                {
                    City = applicantInfo.personalInfo.applicantAddress.city,
                    Country = applicantInfo.personalInfo.applicantAddress.country,
                    Line1 = applicantInfo.personalInfo.applicantAddress.steerAddress,
                    Line2 = applicantInfo.personalInfo.applicantAddress.addressLine2,
                    PostalCode = applicantInfo.personalInfo.applicantAddress.zip,
                    State = applicantInfo.personalInfo.applicantAddress.state
                },
                Email = applicantInfo.pointOfContact.email.Trim(),
                Name = applicantInfo.personalInfo.detailedPersonalInfo.GetFullName(),
                Phone = applicantInfo.personalInfo.detailedPersonalInfo.phone,
                Description = applicantInfo.personalInfo.detailedPersonalInfo.GetFullName(),
                Metadata = new Dictionary<string, string>()
                {
                    {"ApplicantId", applicantId.ToString() },
                    {"Name", applicantInfo.personalInfo.detailedPersonalInfo.GetFullName() }
                },                
            };

            var service = new CustomerService();
            Customer customer = service.Create(options);

            return customer.Id;
        }

        public static void DeleteAllCustomers(string apiKey)
        {
            throw new NotImplementedException();
            StripeConfiguration.ApiKey = apiKey;

            var service = new CustomerService();
            foreach (var customer in service.ListAutoPaging(new CustomerListOptions()
            {
                Limit = 10,
            }))
            {
                service.Delete(customer.Id);
            }
        }
    }
}
