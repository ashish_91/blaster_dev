﻿using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Shared.Source
{
    public static class Azure
    {
        public const string AzureConnStr = "DefaultEndpointsProtocol=https;AccountName=gottliebapplicantsapp;AccountKey=aqVyO5Eqhs2gpl4f1F2TbQkPP6pJK3mlFeF2dx6HaQryHjwPR450m+uXoKEdivoDYh6JEccb4NZOyGQIJUi1sA==;EndpointSuffix=core.windows.net";
        /// <summary>
        /// Azure storage core operations for updating, downloading, moving and etcfiles from the database.
        /// </summary>
        public class StorageManager
        {
            public static void DownloadFileFromAzure(Guid applicantId, string remoteFileName, string localFileName)
            {
                if (remoteFileName is null)
                    return;

                DownloadToFile(
                    connectionString: AzureConnStr,
                    containerName: "applicants",
                    remoteFileName: Path.Combine(applicantId.ToString(), remoteFileName),
                    filePath: localFileName
                );
            }

            /// <summary>
            /// Upload file to the azure container.
            /// </summary>
            /// <param name="connectionString"></param>
            /// <param name="filePath"></param>
            /// <param name="containerName"></param>
            /// <param name="remoteFileName"></param>
            /// <returns></returns>
            private static Uri UploadFile(string connectionString, string filePath, string containerName, string remoteFileName)
            {
                CloudBlobContainer cloudBlobContainer = InitializeBlobContainer(connectionString, containerName);
                CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(remoteFileName);
                cloudBlockBlob.UploadFromFile(filePath);

                return cloudBlockBlob.StorageUri.PrimaryUri;
            }

            /// <summary>
            /// Upload file to the azure container.
            /// </summary>
            /// <param name="connectionString"></param>
            /// <param name="localFilePath"></param>
            /// <param name="containerName"></param>
            /// <param name="remoteFileName"></param>
            /// <returns></returns>
            public static Uri UploadFile(string localFilePath, Guid applicantId, string remoteFileName, NLog.Logger logger, string containerName = "applicants")
            {
                logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { localFilePath, applicantId, remoteFileName })}");

                CloudBlobContainer cloudBlobContainer = InitializeBlobContainer(AzureConnStr, containerName);

                string remoteLocation = $@"{applicantId}\{Path.GetFileName(remoteFileName)}";
                logger.Info($"remoteLocation: {remoteLocation}");

                CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(remoteLocation);
                cloudBlockBlob.UploadFromFile(localFilePath);

                return cloudBlockBlob.StorageUri.PrimaryUri;
            }

            /// <summary>
            /// Moving file from one directory from the container to other.
            /// </summary>
            /// <param name="connectionString"></param>
            /// <param name="containerName"></param>
            /// <param name="oldRemoteFilePath"></param>
            /// <param name="newRemoteFilePath"></param>
            public static void MoveFileToDirectory(string connectionString, string containerName, string oldRemoteFilePath, string newRemoteFilePath)
            {
                CloudBlobContainer cloudBlobContainer = InitializeBlobContainer(connectionString, containerName);
                CloudBlockBlob newBlob = cloudBlobContainer.GetBlockBlobReference(newRemoteFilePath);

                //TODO: отнема време, защото ходи до азуре-то, може рази проверка дас е махне, ако всичко работи добре
                if (newBlob.Exists()) throw new Exception($"New File {newRemoteFilePath} already exists!");
                CloudBlockBlob existingBlob = cloudBlobContainer.GetBlockBlobReference(oldRemoteFilePath);

                //Това е добре са си остане, защото може все още тъмбнейлите да не са готови
                if (!existingBlob.Exists())
                    return;

                newBlob.StartCopyAsync(existingBlob).Wait();
                existingBlob.DeleteIfExistsAsync().Wait();
            }

            /// <summary>
            /// Downloads file to local directory.
            /// </summary>
            /// <param name="connectionString"></param>
            /// <param name="containerName"></param>
            /// <param name="remoteFileName"></param>
            /// <param name="filePath"></param>
            public static void DownloadToFile(string connectionString, string containerName, string remoteFileName, string filePath)
            {
                if (remoteFileName is null)
                {
                    return;
                }

                CloudBlobContainer cloudBlobContainer = InitializeBlobContainer(connectionString, containerName);
                CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(remoteFileName);
                cloudBlockBlob.DownloadToFile(filePath, FileMode.Create);
            }

            /// <summary>
            /// Deletes folder from the container recursively with all of its content inside.
            /// </summary>
            /// <param name="connectionString"></param>
            /// <param name="containerName"></param>
            /// <param name="remoteDirectoryName"></param>
            public static void DeleteFolderRecursively(string connectionString, string containerName, string remoteDirectoryName)
            {
                CloudBlobContainer cloudBlobContainer = InitializeBlobContainer(connectionString, containerName);

                foreach (var blob in cloudBlobContainer.GetDirectoryReference(remoteDirectoryName).ListBlobs(true))
                {
                    cloudBlobContainer.GetBlockBlobReference(((CloudBlockBlob)blob).Name).DeleteIfExists();
                }
            }

            private static CloudBlobContainer InitializeBlobContainer(string connectionString, string containerName)
            {
                var storageAccount = CloudStorageAccount.Parse(connectionString);
                var cloudBlobClient = storageAccount.CreateCloudBlobClient();
                var cloudBlobContainer = cloudBlobClient.GetContainerReference(containerName);

                return cloudBlobContainer;
            }
        }
    }
}
