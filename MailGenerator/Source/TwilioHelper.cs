﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Fax.V1;

namespace Shared.Source
{
    public static class TwilioHelper
    {
        private const string TwilioAccountSid = "AC573369173bf462cca7538f4f1a9098ba"; // Your Account SID from www.twilio.com/console
        private const string TwilioАuthToken = "37087fdffe051fd25a1187207ec6d8cf";  // Your Auth Token from www.twilio.com/console
        public const string TwilioFaxNo = "+14804481197";
        public const string TwilioSmsNo = "+14804481197";

        public static string SendFax(string to, string uri, NLog.Logger logger, ref FaxResource info)
        {
            info = null;
            try
            {
                logger.Info($"Sending to: {to} ");
                logger.Info($"uri: {uri} ");

                TwilioClient.Init(TwilioAccountSid, TwilioАuthToken);
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072; //TLS 1.2

                info = FaxResource.Create(
                    from: TwilioFaxNo,
                    to: to,
                    mediaUrl: new Uri(uri)
                );

                logger.Info("Finished, with status: " + info.Status);

                return null;
            }
            catch(Exception ex)
            {                
                logger.Error(ex);
                return ex.Message;
            }

        }

        public static void SendSms(NLog.Logger logger, string to, string message)
        {
            logger.Info("SendSms");
            logger.Info($"    TwilioAccountSid: {TwilioAccountSid}");
            logger.Info($"    TwilioАuthToken: {TwilioАuthToken}");
            logger.Info($"    TwilioSmsNo: {TwilioSmsNo}");
            logger.Info($"    to: {to}");
            logger.Info($"    message: {message}");

            logger.Info("  Creating TwilioRestClient");

            TwilioClient.Init(TwilioAccountSid, TwilioАuthToken);

            logger.Info("  Calling SendMessage");
            var msg = Twilio.Rest.Api.V2010.Account.MessageResource.Create(
                to: new Twilio.Types.PhoneNumber(to),
                from: new Twilio.Types.PhoneNumber(TwilioSmsNo),
                body: message
            );
            logger.Info("  resp: " + JsonConvert.SerializeObject(msg));

            if (msg.ErrorMessage != null)
            {
                string error = msg.ErrorMessage;
                logger.Error(error);
                throw new Exception(error);
            }
        }
    }
}