﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;

namespace Shared.Source
{
    public static class ShortifyHelper
    {
        private const string YouComSeBaseAddress = "http://you.com.se/";
        private const string YouComSeShortifyAddress = "http://you.com.se/api/api/shortify/this";

        private const string YouComSeShortTemplate = "{{ \"pass\": \"UwXD8YXpT2He9fUt9vFydZd8\", \"targetUrl\": \"{0}\"}}";

        public static string MakeShortUrl(Logger logger, string longUrl)
        {
            logger.Info("MakeShortUrl");
            logger.Info("   url     : " + longUrl);

            using (var client = new HttpClient())
            {
                var content = new StringContent(string.Format(YouComSeShortTemplate, longUrl), Encoding.UTF8, "application/json");
                HttpResponseMessage result = client.PostAsync(YouComSeShortifyAddress, content).Result;

                if (!result.IsSuccessStatusCode)
                    throw new Exception($"Error while shortening: {result.ReasonPhrase}");

                JObject response = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                return YouComSeBaseAddress + response["sufix"].Value<string>();
            }

        }
    }
}
