﻿using ApplicantsApp.Models.Representative;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Source.MailGeneration
{
    public class BodyGenerators
    {
        public class EmailDataResponse
        {
            public string subject;
            public string emailBody;
            public ApplicantsApp.Models.Communication.EmailTypes emailType;
            public List<ApplicantsDocuments> attachedDocuments;
            public ApplicantsRepresentatives representative;
        }

        public class EmailDataRequest
        {
            public ApplicantsActionsEmailDetails emailInfo;
            public ApplicantsNew applicant;
            public ApplicantsApp.Models.Applicant.Applicant.FullApplicantInfo applicantModel;
            public ApplicantsRepresentatives representative;
        }

        public static EmailDataResponse GeneratePASInfoRequestData(EmailDataRequest emailDataRequest)
        {
            Guid repId = emailDataRequest.applicantModel.representativeId ?? new Guid(Resources.RepresentativeId_Jacob);

            return new EmailDataResponse()
            {
                representative = ApplicantsApp.Db.LoadRepresentative(repId, NLog.LogManager.CreateNullLogger()),
                subject = "Email and phone number of the PAS assssor",
                attachedDocuments = null,
                emailType = ApplicantsApp.Models.Communication.EmailTypes.PasAssssorAutoRequest,
                emailBody = $"Can you please send us the email and phone number of the PAS " +
                    $"assessor assigned to {emailDataRequest.applicantModel.personalInfo.detailedPersonalInfo.GetFullName()}, " +
                    $"DOB: {emailDataRequest.applicantModel.personalInfo.detailedPersonalInfo.dateOfBirth.ToString(Resources.USADateOnly)}"
            };
        }

        public static EmailDataResponse GenerateMedicalEvaluationConfirmationEmail(string examDate)
        {
            return new EmailDataResponse()
            {
                subject = "Medical Evaluation Confirmation",
                emailType = ApplicantsApp.Models.Communication.EmailTypes.MedicalEvaluationConfirmation,
                emailBody = $"Just want to confirm that the medical evaluation was approved on {examDate} and that you need nothing further in regard to the medical part of this application. Please let us know if your records reflect something different."
            };
        }

        public static string GenerateFooter(ApplicantsRepresentatives representative, string rawFooter, string customRepresentativeName = null)
        {
            string footer = "<br><br>";

            string repPhone = null;
            if (representative != null)
            {
                RepresentativeInfo representativeInfo = JsonConvert.DeserializeObject<RepresentativeInfo>(representative.Data);
                repPhone = representativeInfo.phoneNumber;
            }

            rawFooter = rawFooter.Replace("{REPRESENTATIVE}", (customRepresentativeName != null ? customRepresentativeName : representative.RepresentativeName));
            rawFooter = rawFooter.Replace("{REPRESENTATIVE_PHONE}", repPhone);
            rawFooter = rawFooter.Replace("{REPRESENTATIVE_EMAIL}", representative?.RepresentativeEmail);

            footer += rawFooter;

            return footer;
        }

        public static string GenerateRFIBody(
            List<ApplicantsDocuments> applicantDocuments,
            List<Applicants_DocumentCategories> documentCategories,
            List<ApplicantsApp.Models.Documents.CustomCategoryDescription> customCategoryDescriptions,
            ApplicantsNew applicantData,
            out int requesteDocumentsCount
            )
        {
            requesteDocumentsCount = 0;
            StringBuilder emailBody = new StringBuilder();

            //get rejected documents
            List<ApplicantsDocuments> rejectedDocuments = applicantDocuments
                    .Where(w => w.DocumentExtraStatus == (int)ApplicantsApp.Models.Documents.DocumentExtraStatus.justPlaceHolder)
                    .Where(w => w.DocumentTypeId == (int)ApplicantsApp.Models.Documents.DocumentTreeType.clientRFI)
                    .Where(w => !string.IsNullOrEmpty(w.RejectReason))
                    .ToList();

            //handle rejected documents
            if (rejectedDocuments.Count > 0)
            {
                emailBody.AppendLine("Thank you for submitting documents. Your request had some issues. Please see below: ");
                emailBody.AppendLine("<b>Please Resubmit:</b>");

                int i = 1;
                foreach (var rejectedDocument in rejectedDocuments)
                {
                    requesteDocumentsCount++;
                    emailBody.AppendLine($"{i++}.) {rejectedDocument.DocumentName} - {rejectedDocument.RejectReason}");
                }

                emailBody.AppendLine();
            }

            //get requested documents, grouped by category
            var requestedDocumentsGroupedByCategory = applicantDocuments
                    .Where(w => w.DocumentExtraStatus == (int)ApplicantsApp.Models.Documents.DocumentExtraStatus.justPlaceHolder)
                    .Where(w => w.DocumentTypeId == (int)ApplicantsApp.Models.Documents.DocumentTreeType.clientRFI)
                    .GroupBy(g => g.DocumentCategoryId)
                    .ToList();

            //handle requested documents
            if (requestedDocumentsGroupedByCategory.Count > 0)
            {
                emailBody.AppendLine("Please return all documents to applications@seniorplanning.org");

                emailBody.AppendLine($"Senior Planning needs the following financial documents to substantiate {applicantData.Name} financial eligibility for ALTCS. We need the following: ");
                emailBody.AppendLine();

                var categoryIdAndPriorityList = documentCategories
                    .Select(s => new { s.Id, s.Priority })
                    .AsEnumerable()
                    .Select(s => new Tuple<int, int>(s.Id, s.Priority))
                    .OrderBy(p => p.Item2);

                foreach (var categoryIdAndPriority in categoryIdAndPriorityList)
                //foreach (var requestedCategoryGroup in requestedDocumentsGroupedByCategory.OrderBy(o => o.Key))
                {
                    int categoryId = categoryIdAndPriority.Item1;

                    var requestedCategoryGroup = requestedDocumentsGroupedByCategory.SingleOrDefault(s => s.Key.Equals(categoryId));
                    if (requestedCategoryGroup is null) continue;

                    Applicants_DocumentCategories category = documentCategories.SingleOrDefault(s => s.Id == categoryId);
                    if (category == null)
                        throw new Exception($"Category with id: '{categoryId}' was not found, maybe it was deleted, but is still in use?");

                    //if categry description - add it
                    //if NO category description - skip it, BUT INCLUDE DOCUMENT DESCRIPTIOn
                    bool addDocumentInfo = string.IsNullOrWhiteSpace(category.Description);

                    emailBody.Append($"<b>{category.Category}</b>");
                    if (!addDocumentInfo)
                    {
                        emailBody.Append(" - " + Shared.Source.Mop.GetCategoryDescriptionWithRespectToCustom(categoryId, customCategoryDescriptions, documentCategories));
                    }

                    emailBody.AppendLine();

                    //It is category
                    int i = 1;
                    foreach (ApplicantsDocuments document in requestedCategoryGroup)
                    {
                        requesteDocumentsCount++;
                        emailBody.Append($"{i}.) {document.DocumentName}");
                        if (addDocumentInfo)
                        {
                            emailBody.Append($" - {document.Notes}");
                        }

                        emailBody.AppendLine();
                        i++;
                    }
                    emailBody.AppendLine();

                }
            }

            return emailBody.ToString();
        }

        const string HTML_TickTdElement = "<img src='http://referrals.seniorplanningcare.org/patients-portal/assets/images/tick_3.png' alt='' width='32' height='32' />";
        public static string Generate__LiteRFI__HTML(
            List<ApplicantsDocuments> applicantDocuments,
            List<Applicants_DocumentCategories> documentCategories,
            List<ApplicantsApp.Models.Documents.CustomCategoryDescription> customCategoryDescriptions,
            ApplicantsNew applicantData,
            out int missingDocuments
            )
        {
            missingDocuments = 0;
            var emailBody = new StringBuilder();

            //HANDLE rejected documents, mark them as MISSING
            List <ApplicantsDocuments> rejectedDocuments = applicantDocuments
                    .Where(w => w.DocumentExtraStatus == (int)ApplicantsApp.Models.Documents.DocumentExtraStatus.justPlaceHolder)
                    .Where(w => w.DocumentTypeId == (int)ApplicantsApp.Models.Documents.DocumentTreeType.clientRFI)
                    .Where(w => !string.IsNullOrEmpty(w.RejectReason))
                    .ToList();
            
            foreach (var rejectedDocument in rejectedDocuments)
            {
                emailBody.AppendLine($"<tr><td style='border: 1px solid #dddddd;text-align: left;padding: 5px;' height='24'>&nbsp;</td><td style='border: 1px solid #dddddd;text-align: left;padding: 5px;width:100%'>{rejectedDocument.DocumentName}</td></tr>");
                missingDocuments++;
            }
            //

            //get requested documents, grouped by category
            var requestedDocumentsGroupedByCategory = applicantDocuments                    
                    .Where(w => w.DocumentTypeId == (int)ApplicantsApp.Models.Documents.DocumentTreeType.clientRFI)
                    .GroupBy(g => g.DocumentCategoryId)
                    .ToList();

            //handle requested documents
            var categoryIdAndPriorityList = documentCategories
                .Select(s => new { s.Id, s.Priority })
                .AsEnumerable()
                .Select(s => new Tuple<int, int>(s.Id, s.Priority))
                .OrderBy(p => p.Item2);

            foreach (var categoryIdAndPriority in categoryIdAndPriorityList)
            //foreach (var requestedCategoryGroup in requestedDocumentsGroupedByCategory.OrderBy(o => o.Key))
            {
                int categoryId = categoryIdAndPriority.Item1;

                var requestedCategoryGroup = requestedDocumentsGroupedByCategory.SingleOrDefault(s => s.Key.Equals(categoryId));
                if (requestedCategoryGroup is null) 
                    continue;
                
                foreach (ApplicantsDocuments document in requestedCategoryGroup)
                {
                    string tickText;
                    if (document.DocumentExtraStatus == (int)ApplicantsApp.Models.Documents.DocumentExtraStatus.justPlaceHolder)
                    {
                        tickText = "&nbsp;";
                        missingDocuments++;
                    } else
                    {
                        tickText = HTML_TickTdElement;
                    }

                    emailBody.AppendLine($"<tr><td style='border: 1px solid #dddddd;text-align: left;padding: 5px;' height='24'>{tickText}</td><td style='border: 1px solid #dddddd;text-align: left;padding: 5px;;width:100%'>{document.DocumentName}</td></tr>");
                }
                emailBody.AppendLine();
            }

            return emailBody.ToString();
        }
    }
}