﻿using ApplicantsApp.Models.Applicant.Applicant;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Source.MailGeneration
{
    public static class EmailProcessor
    {
        public static void ResheduleAllAutomaticEmails(bool RFIDiff, bool interimDiff, bool smallRFIDiff, Guid applicantId, Logger logger, FullApplicantInfo applicantInfo)
        {
            var emailTypesWeAreInterestedIn = new List<ApplicantsApp.Models.Communication.EmailTypes>();

            if (RFIDiff)
                emailTypesWeAreInterestedIn.Add(ApplicantsApp.Models.Communication.EmailTypes.AutoRFI);

            if (interimDiff)
                emailTypesWeAreInterestedIn.Add(ApplicantsApp.Models.Communication.EmailTypes.AutoInterim);

            if (smallRFIDiff)
                emailTypesWeAreInterestedIn.Add(ApplicantsApp.Models.Communication.EmailTypes.LiteRFI);

            Dictionary<ApplicantsApp.Models.Communication.EmailTypes, DateTime?> sentDates
                = ApplicantsApp.Db.DeleteAllScheduledEmailsOfTypeAndReturnLastSentDates(applicantId, logger, emailTypesWeAreInterestedIn);

            foreach (var sentDate in sentDates)
            {
                MailGenerator.ScheduleEMail(logger, ApplicantsApp.Db.GetUserId(), applicantId, applicantInfo, sentDate.Key, sentDate.Value);
            }
        }
    }
}
