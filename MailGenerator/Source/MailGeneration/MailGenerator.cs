﻿using ApplicantsApp;
using ApplicantsApp.Models;
using ApplicantsApp.Models.Applicant.Applicant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Source.MailGeneration
{
    public static class MailGenerator
    {
        public static List<ApplicantsActionsEmailDetails> SendEmail(
            ReportActions action,
            Guid correlationId,
            ref string info,
            ref string errors,
            List<Guid> selectedDocumentsIds,
            List<ApplicantsApp.Models.DTOs.AzureToLocalFileDTO> FilesToSend,
            IEnumerable<string> receivers,
            string body,
            string subject,
            NLog.Logger logger,
            Guid applicantId,
            int FromSpecialAccount,
            ApplicantsRepresentatives applicantsRepresentative,
            string rawFooter,
            Guid userId,
            string customRepresentativeName = null,
            bool convertNewRowsToBr = true
            )
        {
            var attachments = new List<string>();
            string sentDocumentIds = null;
            var emailObjects = new List<ApplicantsActionsEmailDetails>();

            if (FilesToSend != null)
            {
                attachments.AddRange(
                    FilesToSend
                        .Where(w => selectedDocumentsIds.Contains(w.documentId))
                        .Select(s => s.localFilePath)
                );

                sentDocumentIds = string.Join(",", FilesToSend
                    .Where(w => selectedDocumentsIds.Contains(w.documentId))
                    .Select(s => s.documentId.ToString()));
            }

            string footerHTML = BodyGenerators.GenerateFooter(applicantsRepresentative, rawFooter, customRepresentativeName);

            foreach (string to in receivers)
            {
                if (string.IsNullOrWhiteSpace(to))
                    continue;

                var applicantsActionsEmailDetails = new ApplicantsActionsEmailDetails()
                {
                    ApplicantId = applicantId,
                    Body = body,
                    CorrelationId = correlationId,
                    Subject = subject,
                    OtherFiles = sentDocumentIds,
                    EmailTypeId = (int)Mop.GetEmailTypeFromAction(action)
                };

                try
                {
                    //progress.SetProgressText($"Sending email to '{to?.Trim()}'");

                    string recipient = to.Trim();
                    applicantsActionsEmailDetails.To = recipient;

                    if (Mop.IsEmailValid(recipient))
                    {
                        string fromAddress;
                        string password;

                        switch (FromSpecialAccount)
                        {
                            case 0:
                                fromAddress = Db.user.UserName;
                                password = Crypto.AES.Decrypt(Db.user.PasswordCrypted, "Th1M2gAStrongP2asss23Pword");
                                break;

                            case 1:
                                fromAddress = "applications@seniorplanning.org";
                                password = "its a silly one2";
                                break;

                            case 2:
                                fromAddress = "billing@seniorplanning.org";
                                password = "Seniorplanning1!";
                                break;

                            default:
                                throw new Exception("Not expected From Special accoutn with id: " + FromSpecialAccount);
                        }

                        string resp = Mop.SendGmail(
                            fromAddress: fromAddress,
                            toAddress: recipient,
                            password: password,
                            subject: subject,
                            body: body,
                            isBodyHtml: true,
                            logger: logger,
                            attachments: attachments,
                            addFooter: true,
                            footerHTML: footerHTML,
                            makeNewRowsBR: convertNewRowsToBr
                            );

                        if (resp != null)
                            throw new Exception(resp);

                        info += $"Message successfully sended to '{to}'" + Environment.NewLine;
                        applicantsActionsEmailDetails.ErrorMessage = null;
                        applicantsActionsEmailDetails.Result = (int)ApplicantsApp.Models.Communication.EmailStatusCodes.Success;
                    }
                    else
                    {
                        throw new Exception("NOT VALID EMAIL ADDRESS");
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    errors += $"Sending to '{to}' failed. {ex.Message.ToUpperInvariant()}" + Environment.NewLine;
                    applicantsActionsEmailDetails.ErrorMessage = ex.Message;
                    applicantsActionsEmailDetails.Result = (int)ApplicantsApp.Models.Communication.EmailStatusCodes.Error;
                }
                finally
                {
                    emailObjects.Add(Db.InsertActionEmail(logger, applicantsActionsEmailDetails, action, userId));
                }

            }

            return emailObjects;
        }



        public static void ScheduleEMail(NLog.Logger logger,
                                         Guid userId,
                                         Guid applicantId,
                                         FullApplicantInfo applicantModelLocal,
                                         ApplicantsApp.Models.Communication.EmailTypes emailType,
                                         DateTime? lastSentDate
                                         )
        {
            var scheduledEmail = new ApplicantsActionsEmailDetails()
            {
                ApplicantId = applicantId,
                Body = "WILL NE GENERATED JUST BEFORE SENDING",
                Created = DateTime.Now,
                CorrelationId = Guid.NewGuid(),
                ErrorMessage = null,
                Id = Guid.NewGuid(),
                OtherFiles = null,
                Result = (int)ApplicantsApp.Models.Communication.EmailStatusCodes.Scheduled,
                To = null,
            };

            if (lastSentDate.HasValue && lastSentDate.Value.Equals(default))
                lastSentDate = null;

            ReportActions reportAction;
            switch (emailType)
            {
                case ApplicantsApp.Models.Communication.EmailTypes.AutoRFI:
                    reportAction = ReportActions.SendAutomaticRFIEmail;

                    if (applicantModelLocal.autoRFIEmailSettings?.enabled == true)
                    {
                        scheduledEmail.ScheduledFor = lastSentDate.HasValue ?
                            lastSentDate.Value.AddDays(applicantModelLocal.autoRFIEmailSettings.sendIntervalInDays) :
                            DateTime.Now.AddHours(1);

                        scheduledEmail.EmailTypeId = (int)ApplicantsApp.Models.Communication.EmailTypes.AutoRFI;
                        scheduledEmail.Subject = "Auto RFI";
                        scheduledEmail.To = applicantModelLocal.pointOfContact.email;
                    }
                    else
                    {
                        scheduledEmail = null;
                    }
                    break;

                case ApplicantsApp.Models.Communication.EmailTypes.LiteRFI:
                    reportAction = ReportActions.SendAutomaticLiteRFIEmail;

                    if (applicantModelLocal.autoLiteRFIEmailSettings?.enabled == true)
                    {
                        scheduledEmail.ScheduledFor = lastSentDate.HasValue ?
                            lastSentDate.Value.AddDays(4) :
                            DateTime.Now.AddHours(1);

                        //skip weekends
                        if (scheduledEmail.ScheduledFor.Value.DayOfWeek == DayOfWeek.Saturday)
                        {
                            scheduledEmail.ScheduledFor = scheduledEmail.ScheduledFor.Value.AddDays(2);
                        }
                        else
                        if (scheduledEmail.ScheduledFor.Value.DayOfWeek == DayOfWeek.Sunday)
                        {
                            scheduledEmail.ScheduledFor = scheduledEmail.ScheduledFor.Value.AddDays(1);
                        }

                        scheduledEmail.EmailTypeId = (int)emailType;
                        scheduledEmail.Subject = "Auto Lite RFI";
                        scheduledEmail.To = applicantModelLocal.pointOfContact.email;
                    }
                    else
                    {
                        scheduledEmail = null;
                    }
                    break;

                case ApplicantsApp.Models.Communication.EmailTypes.AutoInterim:
                    reportAction = ReportActions.SendAutomaticInterimEmail;

                    if (applicantModelLocal.autoInterimEmailSettings?.enabled == true)
                    {
                        scheduledEmail.ScheduledFor = lastSentDate.HasValue ?
                            lastSentDate.Value.AddDays(applicantModelLocal.autoInterimEmailSettings.sendIntervalInDays) :
                            DateTime.Now.AddHours(1);

                        scheduledEmail.EmailTypeId = (int)ApplicantsApp.Models.Communication.EmailTypes.AutoInterim;
                        scheduledEmail.Subject = "Auto Interim";
                        scheduledEmail.To = applicantModelLocal.pointOfContact.email;
                    }
                    else
                    {
                        scheduledEmail = null;
                    }
                    break;

                case ApplicantsApp.Models.Communication.EmailTypes.PasAssssorAutoRequest:

                    BodyGenerators.EmailDataResponse resp = BodyGenerators.GeneratePASInfoRequestData(new BodyGenerators.EmailDataRequest()
                    {
                        applicantModel = applicantModelLocal
                    });

                    reportAction = ReportActions.ApplicantEmailSendToAskForPASassssor_AUTO;
                    scheduledEmail.ScheduledFor = DateTime.Now.AddDays(3);
                    scheduledEmail.EmailTypeId = (int)resp.emailType;
                    scheduledEmail.Subject = resp.subject;

                    if (applicantModelLocal.financialCaseWorkerId.HasValue && !applicantModelLocal.financialCaseWorkerId.Value.Equals(Guid.Empty))
                    {
                        scheduledEmail.To = Db.GetDoctor(applicantModelLocal.financialCaseWorkerId.Value, logger).Email;
                    }

                    break;

                default:
                    throw new Exception($"That's odd, sendDateType come in: {emailType}");
            }
            
            if (scheduledEmail != null)
            {
                Db.InsertActionEmail(logger, scheduledEmail, reportAction, userId);
            }
        }
    }
}
