﻿using ApplicantsApp.Models.Applicant.Applicant;
using ApplicantsApp.Models.Representative;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Source
{
    public static class GravityConverter
    {
        public static FullApplicantInfo FillApplicantModelFromGravityForm(string data, RepresentativeInfo repInfo, Guid representativeId, NLog.Logger logger)
        {
            JObject jo = JObject.Parse(data);

            var appInfo = new FullApplicantInfo();
            appInfo.personalInfo = new PersonalInfo();

            appInfo.autoInterimEmailSettings = new ApplicantsApp.Models.Applicant.Applicant.AutoEmails.AutoInterimEmailSettings()
            {
                sendIntervalInDays = 1,
            };

            appInfo.autoRFIEmailSettings = new ApplicantsApp.Models.Applicant.Applicant.AutoEmails.AutoRFIEmailSettings()
            {
                sendIntervalInDays = 1,
            };

            appInfo.autoLiteRFIEmailSettings = new ApplicantsApp.Models.Applicant.Applicant.AutoEmails.AutoSmaillRFIEmailSettings()
            {
                enabled = true
            };

            appInfo.gravityStuff = new GravityStuff()
            {
                dateEntered = DateTime.Now, //no longer avaliable
                entryDate = jo["Entry Date"]?.ToString(),
                entryId = jo["Entry ID"]?.ToString(),
                fromTitle = jo["Form Title"]?.ToString(),
                sourceUrl = jo["Source Url"]?.ToString(),
                userIp = jo["77.70.98.63"]?.ToString(),
            };

            appInfo.personalInfo.detailedPersonalInfo = new DetailedPersonalInfo()
            {
                firstName = jo["Applicant name (First)"]?.ToString(),
                middleName = jo["Applicant name (Middle)"]?.ToString(),
                lastName = jo["Applicant name (Last)"]?.ToString(),
                dateOfBirth = Mop.GetDateTimeFromGravityForm(jo, "Applicant Date of birth", logger),
                ethnicity = jo["Applicant Ethnicity"]?.ToString(),
                gender = jo["Applicant gender"]?.ToString() == "Male" ? (short)1 : (short)2, //1 is male, 2 is female
                isOnAHCCSProgram = Mop.GetBoolFromGravityForm(jo, "Already on AHCCCS programs?", logger),
                isRegisteredVoter = Mop.GetBoolFromGravityForm(jo, "Registered Voter", logger),
                isUSCitizen = Mop.GetBoolFromGravityForm(jo, "Applicant US Citizen?", logger),
                livingAt = jo["Living at:"]?.ToString(),
                placeOfBirth = jo["Applicant Place of birth"]?.ToString(),
                SSN = jo["Applicant Social Security Number"]?.ToString(),
                phone = jo["Applicant Phone"]?.ToString(),
            };

            if (string.IsNullOrWhiteSpace(appInfo.personalInfo.detailedPersonalInfo.email?.Trim()))
            {
                appInfo.personalInfo.detailedPersonalInfo.email = repInfo.email;
            }

            if (string.IsNullOrWhiteSpace(appInfo.personalInfo.detailedPersonalInfo.phone?.Trim()))
            {
                appInfo.personalInfo.detailedPersonalInfo.phone = repInfo.phoneNumber;
            }

            appInfo.personalInfo.applicantAddress = new ApplicantsApp.Models.Applicant.Applicant.Common.Address()
            {
                addressLine2 = jo["Applicant Address (Address Line 2)"]?.ToString(),
                city = jo["Applicant Address (City)"]?.ToString(),
                country = jo["Applicant Address (Country)"]?.ToString(),
                state = jo["Applicant Address (State / Province)"]?.ToString(),
                steerAddress = jo["Applicant Address (Street Address)"]?.ToString(),
                zip = jo["Applicant Address (ZIP / Postal Code)"]?.ToString(),
            };

            //the same as applicant address
            appInfo.personalInfo.applicantMailingAddress = new ApplicantsApp.Models.Applicant.Applicant.Common.Address()
            {
                addressLine2 = jo["Applicant Address (Address Line 2)"]?.ToString(),
                city = jo["Applicant Address (City)"]?.ToString(),
                country = jo["Applicant Address (Country)"]?.ToString(),
                state = jo["Applicant Address (State / Province)"]?.ToString(),
                steerAddress = jo["Applicant Address (Street Address)"]?.ToString(),
                zip = jo["Applicant Address (ZIP / Postal Code)"]?.ToString(),
            };

            appInfo.personalInfo.martialStatus = new MartialStatus()
            {
                daveOfMarriageDivorceOrWidowed = Mop.GetDateTimeFromGravityForm(jo, "Date of Marriage, Divorce or Widowed", logger),
                martialStatus = jo["Applicant Marital Status"]?.ToString(),
                spouseDateOfBirth = Mop.GetDateTimeFromGravityForm(jo, "Spouse date of birth", logger),
                spouseFirstName = jo["Name of spouse (First)"]?.ToString(),
                spouseLastName = jo["Name of spouse (Last)"]?.ToString(),
                spouseMiddleName = jo["Name of spouse (Middle)"]?.ToString(),
                spouseSSN = jo["Spouse Social Security Number"]?.ToString(),
            };

            appInfo.personalInfo.militaryService = new MilitaryService()
            {
                IsMilitaryService = Mop.GetBoolFromGravityForm(jo, "Applicant Military Service", logger),
                Branch = jo["Applicant Branch"]?.ToString(),
                FromYear = jo["Applicant Military service start year"]?.ToString(),
                ToYear = jo["Applicant Military service end year"]?.ToString(),
            };

            appInfo.pointOfContact = new PointOfContact()
            {
                isPOCAvaliable = Mop.GetBoolFromGravityForm(jo, "Is there a point of contact?", logger),
                address = new ApplicantsApp.Models.Applicant.Applicant.Common.Address()
                {
                    addressLine2 = jo["POC Address (Street Address)"]?.ToString(),
                    city = jo["POC Address (City)"]?.ToString(),
                    country = jo["POC Address (Country)"]?.ToString(),
                    state = jo["POC Address (State / Province)"]?.ToString(),
                    steerAddress = jo["POC Address (Address Line 2)"]?.ToString(),
                    zip = jo["POC Address (ZIP / Postal Code)"]?.ToString(),
                },
                email = jo["POC Email"]?.ToString(),
                firstName = jo["POC Name (First)"]?.ToString(),
                lastName = jo["POC Name (Last)"]?.ToString(),
                middleName = jo["POC Name (Middle)"]?.ToString(),
                phone = jo["POC Phone"]?.ToString(),
                relationship = jo["POC Relationship"]?.ToString(),
            };

            appInfo.ltcCommunity = new LTCCommunity()
            {
                address = new ApplicantsApp.Models.Applicant.Applicant.Common.Address()
                {
                    addressLine2 = jo["LTC Facility Address (Address Line 2)"]?.ToString(),
                    city = jo["LTC Facility Address (City)"]?.ToString(),
                    country = jo["LTC Facility Address (Country)"]?.ToString(),
                    state = jo["LTC Facility Address (State / Province)"]?.ToString(),
                    steerAddress = jo["LTC Facility Address (Street Address)"]?.ToString(),
                    zip = jo["LTC Facility Address (ZIP / Postal Code)"]?.ToString(),
                },
                dateEntered = Mop.GetDateTimeFromGravityForm(jo, "Date entered LTC facility", logger),
                facilityName = jo["LTC Facility Name"]?.ToString(),
                facilityPhone = jo["LTC Facility Phone"]?.ToString(),
                facilityType = jo["LTC Facility Type"]?.ToString(),
                IsLtcCommunity = Mop.GetBoolFromGravityForm(jo, "Is there a LTC community?", logger),
                POCFirstName = jo["LTC Facility point of contact (First)"]?.ToString(),
                POCLastName = jo["LTC Facility point of contact (Last)"]?.ToString(),
            };

            appInfo.healthAndMedInfo = new HealthAndMedInfo()
            {
                bathingNotes = jo["BATHING notes"]?.ToString(),

                DDDNotes = jo["Diagnosed with DDD applicable issues before 18 notes"]?.ToString(),
                dressingNotes = jo["DRESSING notes"]?.ToString(),
                eatingOrMealNotes = jo["EATING/MEAL PREPARATION notes"]?.ToString(),
                incontinenceNotes = jo["INCONTINENCE notes"]?.ToString(),

                isAggression = Mop.GetMultiCheckFromGravityForm(jo, "ISSUES WITH BEHAVIOR", "Aggression"),
                isSelfInjury = Mop.GetMultiCheckFromGravityForm(jo, "ISSUES WITH BEHAVIOR", "Self injury"),
                isWandering = Mop.GetMultiCheckFromGravityForm(jo, "ISSUES WITH BEHAVIOR", "Wandering"),
                isDisruptive = Mop.GetMultiCheckFromGravityForm(jo, "ISSUES WITH BEHAVIOR", "Disruptive"),
                behaviorNotes = jo["ISSUES WITH BEHAVIOR notes"]?.ToString(),

                isBathingIssues = Mop.GetBoolFromGravityForm(jo, "Issues with BATHING", logger),
                isDDD = Mop.GetBoolFromGravityForm(jo, "Diagnosed with DDD applicable issues before 18", logger),

                isDressingIssues = Mop.GetBoolFromGravityForm(jo, "Issues with DRESSING", logger),
                isEatingOrMealIssues = Mop.GetBoolFromGravityForm(jo, "Issues with EATING/MEAL PREPARATION", logger),

                isIncontinenceBladder = Mop.GetMultiCheckFromGravityForm(jo, "INCONTINENCE", "Bladder"),
                isIncontinenceBowel = Mop.GetMultiCheckFromGravityForm(jo, "INCONTINENCE", "Bowel"),

                isLegallyBlind = Mop.GetBoolFromGravityForm(jo, "Legally blind", logger),
                isMobilityissues = Mop.GetBoolFromGravityForm(jo, "Issues with MOBILITY", logger),
                isOrientationIssues = Mop.GetBoolFromGravityForm(jo, "ORIENTATION TO TIME AND PLACE", logger),
                isOther = Mop.GetBoolFromGravityForm(jo, "OTHER DIGNOSIS", logger),
                isOxygen = Mop.GetBoolFromGravityForm(jo, "OXYGEN TREATMENTS", logger),
                isParalysisIssues = Mop.GetBoolFromGravityForm(jo, "PARALYSIS", logger),

                isToiletingIssues = Mop.GetBoolFromGravityForm(jo, "Issues with TOILETING", logger),
                isTransferringIssues = Mop.GetBoolFromGravityForm(jo, "Issues with TRANSFERRING", logger),
                isVisionIssues = Mop.GetBoolFromGravityForm(jo, "Issues with VISION", logger),


                mobilityNotes = jo["MOBILITY notes"]?.ToString(),
                orientationNotes = jo["ORIENTATION TO TIME AND PLACE notes"]?.ToString(),
                otherNotes = jo["OTHER DIAGNOSIS notes"]?.ToString(),
                oxygenNotes = jo["OXYGEN TREATMENTS frequency and duration"]?.ToString(),
                paralisysNotes = jo["PARALYSIS notes"]?.ToString(),
                toiletingNotes = jo["TOILETING notes"]?.ToString(),
                transferringNotes = jo["TRANSFERRING notes"]?.ToString(),
                visionNotes = jo["VISION notes"]?.ToString(),

                alzheimer = new Alzheimer()
                {
                    diagnosingFirstName = jo["Name of diagnosing physician (First)"]?.ToString(),
                    diagnosingLastName = jo["Name of diagnosing physician (Last)"]?.ToString(),
                    diagnosisDate = Mop.GetDateTimeFromGravityForm(jo, "Date of alzheimer’s disease or dementia diagnosis", logger),
                    IsAlzheimer = Mop.GetBoolFromGravityForm(jo, "ALZHEIMER’S DISEASE OR DEMENTIA", logger),
                    physicianField = jo["Physician field"]?.ToString(),
                }
            };

            appInfo.finnancialInfo = new FinnancialInfo()
            {
                burialArrangementAmmount = Mop.GetDecimalFromGravityForm(jo, "Amount", logger, 0) ?? 0,
                homeOwned = new HomeOwned()
                {
                    isHomeOwned = Mop.GetBoolFromGravityForm(jo, "Home Owned?", logger),
                    mortgage = Mop.GetBoolFromGravityForm(jo, "Mortgage", logger),
                    otherOccupants = Mop.GetBoolFromGravityForm(jo, "Other Occupants", logger),
                    reverseMortgage = Mop.GetBoolFromGravityForm(jo, "Reverse Mortgage", logger),
                },
                IsBurialArrangement = Mop.GetBoolFromGravityForm(jo, "BURIAL ARRANGEMENT", logger),
                isOwnPolicy = Mop.GetBoolFromGravityForm(jo, "Does the applicant own a policy on someone other than themselves?", logger),
                lifeInsuranceInstitution = jo["Life Insurance Institution"]?.ToString(),
                liveInsuranceCash = jo["Life Insurance Cash Surrender Value"]?.ToString(),
                ownPolicyNotes = jo["Explain"]?.ToString(),

                countableResources = new List<CountableResource>(),
                monthlyIncomes = new List<MonthlyIncome>(),
                retirements = new List<Retirement>()
            };

            appInfo.lookBackPeriod = new LookBackPeriod()
            {
                isCarSold = Mop.GetBoolFromGravityForm(jo, "Have you sold or transferred ownership of a car within the last five years?", logger),
                isFundsTransferred = Mop.GetBoolFromGravityForm(jo, "Have you transferred any funds out of your name in the last five years?", logger),
                isGiftGiven = Mop.GetBoolFromGravityForm(jo, "Have you given a cash gift in the last five years? (not including small gifts for birthdays or holidays)", logger),
                isHomeSold = Mop.GetBoolFromGravityForm(jo, "Have you sold or transferred ownership of a home within the last five years?", logger),
                otherMajorFinancialCommitments = jo["Do you have any other major financial commitments, or have you made a major financial decision in the past five years? If so, please describe briefly below."]?.ToString(),
            };

            appInfo.clientNeeds = new ClientNeeds()
            {
                isALTCSApplicant = Mop.GetMultiCheckFromGravityForm(jo, "Client needs assistance with", "ALTCS application"),
                isDocumentPreparation = Mop.GetMultiCheckFromGravityForm(jo, "Client needs assistance with", "Document Preparation"),
                isPlacement = Mop.GetMultiCheckFromGravityForm(jo, "Client needs assistance with", "Placement"),
                isRealEstate = Mop.GetMultiCheckFromGravityForm(jo, "Client needs assistance with", "Real estate"),
                isVeteranBenefits = Mop.GetMultiCheckFromGravityForm(jo, "Client needs assistance with", "Veterans benefits"),
                otherNotes = jo["Additional Notes"]?.ToString(),
            };

            appInfo.representativeId = representativeId;
            appInfo.medicalRepresentativeId = new Guid("0AA58D6E-B156-491C-84C3-92F8DE84F6C5");

            //THE DE STUFF

            appInfo.currentLivingArrangement = new ApplicantsApp.Models.Applicant.Applicant.de.CurrentLivingArrangement()
            {
                currentLivingType = ApplicantsApp.Models.Applicant.Applicant.Enums.CurrentLiving.None,
                expectedDateOfDIscharge = DateTime.Now,
                
                address = new ApplicantsApp.Models.Applicant.Applicant.Common.Address()
                {
                    addressLine2 = null,
                    country = "United States",                    
                }
            };

            appInfo.accommodationForPrintedLetters = new ApplicantsApp.Models.Applicant.Applicant.de.AccommodationForPrintedLetters()
            {
                
            };

            appInfo.additionalQuestions = new ApplicantsApp.Models.Applicant.Applicant.de.AdditionalQuestions()
            {                
                serviceBegan = DateTime.Now
            };

            appInfo.interviewInfo = new ApplicantsApp.Models.Applicant.Applicant.de.InterviewInfo()
            {
                
            };

            appInfo.personCompletingDeForm = new ApplicantsApp.Models.Applicant.Applicant.de.PersonCompletingDeForm()
            {
                name = repInfo.name,
                phone = repInfo.phoneNumber,
                whoIsCompletingTheForm = ApplicantsApp.Models.Applicant.Applicant.Enums.CompletingFormPersonType.None
            };

            //THE MEDICAL STUFF  
            appInfo.disclosedInformation = new ApplicantsApp.Models.Applicant.Applicant.Medical.DisclosedInformation()
            {
                periodFrom = DateTime.Now,
                periodTo = DateTime.Now
            };

            appInfo.authorizationPurpose = new ApplicantsApp.Models.Applicant.Applicant.Medical.AuthorizationPurpose()
            {
                
            };

            appInfo.endOfAuth = new ApplicantsApp.Models.Applicant.Applicant.Medical.EndOfAuth()
            {
                endOn = DateTime.Now,
            };

            appInfo.patientRights = new ApplicantsApp.Models.Applicant.Applicant.Medical.PatientRights()
            {
                authRepSignDate = DateTime.Now, 
                patientSignatureDate = DateTime.Now
            };

            appInfo.additionalConsent = new ApplicantsApp.Models.Applicant.Applicant.Medical.AdditionalConsentForConditions()
            {                
                date = DateTime.Now
            };

            appInfo.additionalConsentForHIV = new ApplicantsApp.Models.Applicant.Applicant.Medical.AdditionalCOnsentForHIV()
            {
                date = DateTime.Now
            };

            //Accounts
            appInfo.finnancialInfo.countableResources = new List<CountableResource>();

            string bankAccountTypeTemplate = "Bank Account {0} Type";
            string bankAccountInstitutionTemplate = "Bank Account {0} Institution";
            string bankAccountBalanceTemplate = "Bank Account {0} Balance";

            for (int i = 0; i < 20; i++)
            {
                string bankTypeKey = string.Format(bankAccountTypeTemplate, i);
                string bankInsKey = string.Format(bankAccountInstitutionTemplate, i);
                string bankBalanceKey = string.Format(bankAccountBalanceTemplate, i);

                if (string.IsNullOrWhiteSpace(jo[bankTypeKey]?.ToString())
                    && string.IsNullOrWhiteSpace(jo[bankInsKey]?.ToString())
                    && string.IsNullOrWhiteSpace(jo[bankBalanceKey]?.ToString())
                )
                    continue;

                appInfo.finnancialInfo.countableResources.Add(new CountableResource()
                {
                    ballance = Mop.GetDecimalFromGravityForm(jo, bankBalanceKey, logger, 0) ?? 0,
                    institution = jo[bankInsKey]?.ToString(),
                    type = jo[bankTypeKey]?.ToString(),
                });
            }

            //Income
            appInfo.finnancialInfo.monthlyIncomes = new List<MonthlyIncome>();
            string IncomeSourceTemplate = "Source {0}";
            string IncomeAmountTemplate = "Source {0} Amount Monthly";

            for (int i = 0; i < 20; i++)
            {
                string incomeSourceKey = string.Format(IncomeSourceTemplate, i);
                string incomeAmountKey = string.Format(IncomeAmountTemplate, i);

                if (string.IsNullOrWhiteSpace(jo[incomeSourceKey]?.ToString())
                    && string.IsNullOrWhiteSpace(jo[incomeAmountKey]?.ToString())
                )
                    continue;

                appInfo.finnancialInfo.monthlyIncomes.Add(new MonthlyIncome()
                {
                    source = jo[incomeSourceKey]?.ToString(),
                    ammount = Mop.GetDecimalFromGravityForm(jo, incomeAmountKey, logger, 0) ?? 0,
                });
            }

            //Retirement
            appInfo.finnancialInfo.retirements = new List<Retirement>();

            string RetirementTypeTemplate = "Retirement {0}";
            string RetirementInstitutionTemplate = "Retirement {0} Institution";
            string RetirementBalanceTemplate = "Retirement {0} Balance";

            for (int i = 0; i < 20; i++)
            {
                string retirementTypeKey = string.Format(RetirementTypeTemplate, i);
                string retirementInsKey = string.Format(RetirementInstitutionTemplate, i);
                string retirementBalanceKey = string.Format(RetirementBalanceTemplate, i);

                if (string.IsNullOrWhiteSpace(jo[retirementTypeKey]?.ToString())
                    && string.IsNullOrWhiteSpace(jo[retirementInsKey]?.ToString())
                    && string.IsNullOrWhiteSpace(jo[retirementBalanceKey]?.ToString())
                )
                    continue;

                appInfo.finnancialInfo.retirements.Add(new Retirement()
                {
                    ballance = Mop.GetDecimalFromGravityForm(jo, retirementBalanceKey, logger, 0) ?? 0,
                    institution = jo[retirementInsKey]?.ToString(),
                    retirement = jo[retirementTypeKey]?.ToString(),
                });
            }

            //vehicles
            appInfo.finnancialInfo.vehicles = new List<Vehicle>();
            string VehicleMakeTemplate = "Vehicle {0} (First)";
            string VehicleModelTemplate = "Vehicle {0} (Middle)";
            string VehicleYearTemplate = "Vehicle {0} (Last)";
            string VehicleMilageTemplate = "Vehicle {0} (Suffix)";

            for (int i = 0; i < 20; i++)
            {
                string vehicleMake = string.Format(VehicleMakeTemplate, i);
                string vehicleModel = string.Format(VehicleModelTemplate, i);
                string vehicleYear = string.Format(VehicleYearTemplate, i);
                string vehicleMilage = string.Format(VehicleMilageTemplate, i);

                if (string.IsNullOrWhiteSpace(jo[vehicleMake]?.ToString())
                    && string.IsNullOrWhiteSpace(jo[vehicleModel]?.ToString())
                    && string.IsNullOrWhiteSpace(jo[vehicleYear]?.ToString())
                    && string.IsNullOrWhiteSpace(jo[vehicleMilage]?.ToString())
                )
                    continue;

                appInfo.finnancialInfo.vehicles.Add(new Vehicle()
                {
                    make = jo[vehicleMake]?.ToString(),
                    model = jo[vehicleModel]?.ToString(),
                    year = jo[vehicleYear]?.ToString(),
                    milage = jo[vehicleMilage]?.ToString(),
                });
            }

            //medical
            appInfo.authorizedPartiesIds = new List<Guid>();            

            return appInfo;
        }
    }
}
