﻿using iText.IO.Image;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Pdf.Extgstate;
using iText.Kernel.Utils;
using iText.Layout;
using iText.Layout.Element;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Source
{
    public static class PDFWorker
    {
        public static string StampPdf(string pdfFilePath, string targetFileName, string stampText)
        {
            PdfDocument pdfDoc = new PdfDocument(new PdfReader(pdfFilePath), new PdfWriter(targetFileName));
            Document document = new Document(pdfDoc);
            Rectangle pageSize;
            PdfCanvas canvas;
            int n = pdfDoc.GetNumberOfPages();
            for (int i = 1; i <= n; i++)
            {
                PdfPage page = pdfDoc.GetPage(i);
                pageSize = page.GetPageSize();
                canvas = new PdfCanvas(page);

                Paragraph p = new Paragraph(stampText).SetFontSize(60);
                canvas.SaveState();
                PdfExtGState gs1 = new PdfExtGState().SetFillOpacity(0.30f);
                canvas.SetExtGState(gs1);
                document.ShowTextAligned(p,
                                         pageSize.GetWidth() / 2,
                                         pageSize.GetHeight() / 2,
                                         pdfDoc.GetPageNumber(page),
                                         iText.Layout.Properties.TextAlignment.CENTER,
                                         iText.Layout.Properties.VerticalAlignment.MIDDLE,
                                         45);

                canvas.RestoreState();
            }
            pdfDoc.Close();

            return targetFileName;
        }

        public static string ImageToPdf(string imageFileName, string targetFileName, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { imageFileName, targetFileName })}");

            PdfDocument pdfDocument = new PdfDocument(new PdfWriter(targetFileName));
            Document document = new Document(pdfDocument);
            ImageData imageData = ImageDataFactory.Create(imageFileName);
            Image image = new Image(imageData).SetAutoScale(true);// ScaleAbsolute(100, 200).SetFixedPosition(1, 25, 25);
            // This adds the image to the page
            document.Add(image);

            // Don't forget to close the document.
            // When you use Document, you should close it rather than PdfDocument instance
            document.Close();
            pdfDocument.Close();

            return targetFileName;
        }

        public static string MergePDFFiles(IEnumerable<string> pdfFiles, string targetFile, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { pdfFiles, targetFile })}");

            PdfDocument pdf = new PdfDocument(new PdfWriter(targetFile));
            try
            {
                PdfMerger merger = new PdfMerger(pdf);

                foreach (string pdfFilePath in pdfFiles)
                {
                    string fileExtension = System.IO.Path.GetExtension(pdfFilePath);

                    string pdfFileName;
                    switch (fileExtension)
                    {
                        case ".pdf":
                            pdfFileName = pdfFilePath;
                            break;

                        case ".jpg":
                        case ".jpeg":
                        case ".png":
                        case ".tif":
                        case ".tiff":
                        case ".bmp":
                        case ".gif":
                        case ".giff":
                            pdfFileName = Mop.GetTempFileName(extension: ".pdf");
                            pdfFileName = ImageToPdf(pdfFilePath, pdfFileName, logger);
                            break;

                        default: throw new ArgumentException($"The file '{System.IO.Path.GetFileName(pdfFilePath)}' type: '{fileExtension}' can''t be converted to pdf.");
                    }

                    PdfDocument sourcePdf = new PdfDocument(new PdfReader(pdfFileName));
                    try
                    {
                        merger.Merge(sourcePdf, 1, sourcePdf.GetNumberOfPages());
                    }
                    finally
                    {
                        sourcePdf.Close();
                    }
                }
            }
            finally
            {
                pdf.Close();
            }

            return System.IO.Path.GetFullPath(targetFile);

        }
    }
}
