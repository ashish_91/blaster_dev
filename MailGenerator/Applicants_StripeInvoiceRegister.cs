
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace Shared
{

using System;
    using System.Collections.Generic;
    
public partial class Applicants_StripeInvoiceRegister
{

    public string InvoiceId { get; set; }

    public decimal AmountDue { get; set; }

    public decimal AmountPaid { get; set; }

    public decimal AmountRemaining { get; set; }

    public int AttemptCount { get; set; }

    public bool Attempted { get; set; }

    public string CustomerId { get; set; }

    public bool Paid { get; set; }

    public string Status { get; set; }

    public string SubscriptionId { get; set; }

    public string PlanId { get; set; }

    public System.DateTime CreationDate { get; set; }

    public int InvoiceStatus { get; set; }

    public string InvoiceMessage { get; set; }

    public string SerializedObject { get; set; }

    public int InvoiceType { get; set; }

    public string ChargeId { get; set; }

    public string PaymentId { get; set; }

}

}
