﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Documents
{
    public enum DocumentTreeType
    {
        clientRFI = 1,
        miscDocument = 2,
        uncategorizedDocument = 5,
        rejectedDocument = 6
    }
}
