﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Documents
{
    public enum DocumentExtraStatus
    {
        justPlaceHolder = 1,
        fileIsUploadedAndReviewIsPending = 2,
        fileIsUploadedAndAccepted = 3
    }
}
