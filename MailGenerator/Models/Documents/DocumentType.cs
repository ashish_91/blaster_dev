﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Documents
{
    public enum DocumentType
    {
        unspecified = 0,
        other = 1,
        de101 = 2,
        de112 = 3,
        medical = 4,
        de101_pas = 5,
        de101_csra = 6,        
    }
}
