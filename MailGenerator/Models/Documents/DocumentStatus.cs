﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Documents
{
    public enum DocumentStatus
    {        
        deleted = 0,
        active = 1,        
        oldVersion = 2,
        unspecified = byte.MaxValue,
    }
}