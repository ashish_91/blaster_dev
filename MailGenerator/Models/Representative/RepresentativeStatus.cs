﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Representative
{
    public enum RepresentativeStatus
    {
        Enabled,
        Disabled
    }
}
