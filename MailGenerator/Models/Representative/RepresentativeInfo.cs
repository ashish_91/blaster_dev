﻿using Shared;
using ApplicantsApp.Models.Applicant.Applicant.Enums;

namespace ApplicantsApp.Models.Representative
{
    public class RepresentativeInfo
    {
        public string name;
        public string relationship;
        public string spokenLanguage;
        public string writtenLanguage;
        public Applicant.Applicant.Common.Address address;
        public string email;

        public string phoneNumber;
        public RepresentativePhoneNumberType phoneNumberType;
        public string phoneNumberTypeOther;

        public string otherPhoneNumber;
        public RepresentativePhoneNumberType otherphoneNumberType;
        public string otherPhoneNumberTypeOther;

        public string witnessName;

        public bool isheaPlusEmailNotify;
        public string heaPlusEmailNotifyAddress;

        public bool isheaPlusTextNotify;
        public string heaPlusTextNotifyNumber;

        public Guardian guardian;
    }

    public class Guardian
    {
        public string nameOfGuardian;
        public string guardianRelationship;
        public bool isRepresentativeAGuardian;
        public Applicant.Applicant.Common.Address address;
        public string phoneNumber;
        public string emailAddress;
    }
}
