﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Representative
{
    public enum RepresentativeType
    {        
        [Description("Representative (Employee)")]
        RepresentativeEmployee,
        
        [Description("Financial ALTCS Rep")]
        FinancialALTCSRep,
        
        [Description("Medical ALTCS Rep")]
        MedicalALTCSRep,
        
        [Description("Financial Institution")]
        FinancialInstitution,
        
        [Description("Physician")]
        Physician
    }
}
