﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.DTOs
{
    public class ApplicantNewDTO : ApplicantsNew
    {
        public string createdByUserName;
        public string updatedByUserName;
        public int requiredDocuments;
    }
}
