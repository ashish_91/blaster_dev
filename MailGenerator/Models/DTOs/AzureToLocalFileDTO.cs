﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.DTOs
{
    public class AzureToLocalFileDTO
    {
        public string localFilePath;
        public Guid documentId;
        public Documents.DocumentType documentType;
        public string humanFileName;
    }
}
