﻿using Shared;

namespace ApplicantsApp.Models.DTOs
{
    public class ApplicantActionDTO : ApplicantsActions
    {
        public string responsibleUserName;
        public string applicantName;
    }
}
