﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.DTOs
{
    public class EmailReportDTO : ApplicantsActionsEmailDetails
    {
        public string ResponsibleUser { get; set; }
        public string ApplicantName { get; set; }
    }
}
