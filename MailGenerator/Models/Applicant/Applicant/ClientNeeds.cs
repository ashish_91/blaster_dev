﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant
{
    public class ClientNeeds
    {
        public bool isALTCSApplicant;
        public bool isVeteranBenefits;
        public bool isPlacement;
        public bool isDocumentPreparation;
        public bool isRealEstate;
        public string otherNotes;
    }
}
