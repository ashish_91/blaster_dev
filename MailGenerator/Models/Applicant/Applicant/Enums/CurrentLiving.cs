﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant.Enums
{
    public enum CurrentLiving
    {
        Hospital,
        Nursing,
        Home,
        Other,
        None
    }
}
