﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant
{
    public class PersonalInfo
    {
        public DetailedPersonalInfo detailedPersonalInfo;
        public MartialStatus martialStatus;
        public MilitaryService militaryService;
        public Common.Address applicantAddress;
        public Common.Address applicantMailingAddress;
    }

    public class MilitaryService
    {
        public bool IsMilitaryService;
        public string Branch;
        public string FromYear;
        public string ToYear;
    }

    public class DetailedPersonalInfo
    {        
        [Required]
        public string firstName;

        [Required]
        public string middleName;

        [Required]
        public string lastName;

        [Required]
        public short gender;

        [Required]
        public DateTime dateOfBirth;

        [Required]
        public string SSN;
        public bool isUSCitizen;
        public bool isOnAHCCSProgram;
        public bool isRegisteredVoter;

        [Required]
        public string livingAt;

        [Required]
        public string placeOfBirth;

        [Required]
        public string ethnicity;

        [Required]
        public string phone;

        [Required]
        public string email;

        public string GetFullName()
        {
            return (firstName + " " + middleName + " " + lastName).Replace("  ", " ");
        }
}

    public class MartialStatus
    {
        [Required]
        public string martialStatus;
        public DateTime? spouseDateOfBirth;
        public DateTime? daveOfMarriageDivorceOrWidowed;
        public string spouseFirstName;
        public string spouseMiddleName;
        public string spouseLastName;
        public string spouseSSN;

        public string spouseFullName
        {
            get
            {
                return (spouseFirstName + " " + spouseMiddleName + " " + spouseLastName).Replace("  ", " ");
            }
            private set { }
        }
    }
}
