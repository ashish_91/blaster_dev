﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant
{
    public class FullApplicantInfo
    {
        //auto emails
        public AutoEmails.AutoRFIEmailSettings autoRFIEmailSettings;
        public AutoEmails.AutoInterimEmailSettings autoInterimEmailSettings;
        public AutoEmails.AutoEmailEvents autoEmailEvents;
        public AutoEmails.AutoSmaillRFIEmailSettings autoLiteRFIEmailSettings;

        //gravity
        public PersonalInfo personalInfo;
        public PointOfContact pointOfContact;
        public LTCCommunity ltcCommunity;
        public HealthAndMedInfo healthAndMedInfo;
        public FinnancialInfo finnancialInfo;
        public LookBackPeriod lookBackPeriod;
        public ClientNeeds clientNeeds;

        //other
        //public CaseWorker caseWorker;
        //public CaseWorker financialCaseWorker;
        //public CaseWorker financialCaseWorkerSupervisor;
        //public CaseWorker medicalCaseWorker;

        //NEW other
        public Guid? financialCaseWorkerId;
        public Guid? financialCaseWorkerSupervisorId;
        public Guid? medicalCaseWorkerId;

        //NEW med exam, located in medical case worker tab
        public DateTime? medApprovalDate;

        //de
        //public Representative.RepresentativeInfo representativeinfo;
        public Guid? representativeId;
        public Guid? medicalRepresentativeId;
        public de.CurrentLivingArrangement currentLivingArrangement;
        public de.AccommodationForPrintedLetters accommodationForPrintedLetters;
        public de.AdditionalQuestions additionalQuestions;
        public de.InterviewInfo interviewInfo;
        public de.PersonCompletingDeForm personCompletingDeForm;

        //medical
        public List<Guid> authorizedPartiesIds;
        public Medical.DisclosedInformation disclosedInformation;
        public Medical.AuthorizationPurpose authorizationPurpose;
        public Medical.EndOfAuth endOfAuth;
        public Medical.PatientRights patientRights;
        public Medical.AdditionalConsentForConditions additionalConsent;
        public Medical.AdditionalCOnsentForHIV additionalConsentForHIV;

        //gravityForm
        public GravityStuff gravityStuff;
    }

    public class GravityStuff
    {
        public string fromTitle;
        public string userIp;
        public string entryDate;
        public DateTime dateEntered;
        public string entryId;
        public string sourceUrl;

    }

}