﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant
{
    public class LookBackPeriod
    {
        public bool isHomeSold;
        public bool isCarSold;
        public bool isFundsTransferred;
        public bool isGiftGiven;
        public string otherMajorFinancialCommitments;
    }
}
