﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant
{
    public class CaseWorker
    {
        public string Name;        
        public string Fax;
        public string Email;
        public string Phone;        
    }
}
