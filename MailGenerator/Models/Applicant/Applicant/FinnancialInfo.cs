﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant
{
    public class FinnancialInfo
    {
        public List<CountableResource> countableResources;
        public List<Retirement> retirements;
        public List<MonthlyIncome> monthlyIncomes;

        public string lifeInsuranceInstitution;
        public string liveInsuranceCash;

        public bool isOwnPolicy;
        public string ownPolicyNotes;

        public bool IsBurialArrangement;
        public decimal burialArrangementAmmount;

        public HomeOwned homeOwned;

        public List<Vehicle> vehicles;
    }

    public class CountableResource
    {
        public string type;
        public string institution;
        public decimal ballance;
    }

    public class Retirement
    {
        public string retirement;
        public string institution;
        public decimal ballance;
    }

    public class MonthlyIncome
    {
        public string source;
        public decimal ammount;        
    }

    public class HomeOwned
    {
        public bool isHomeOwned;
        public bool mortgage;
        public bool reverseMortgage;
        public bool otherOccupants;
    }

    public class Vehicle
    {
        public string make;
        public string model;
        public string year;
        public string milage;
    }
}
