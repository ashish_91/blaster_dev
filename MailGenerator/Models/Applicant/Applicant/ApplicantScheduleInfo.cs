﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant
{
    public class ApplicantScheduleInfo
    {
        public DateTime PostponeDateTime { get; set; }
        public string WorkerName { get; set; }
        public string WorkerFax { get; set; }
        public string WorkerPhone { get; set; }
        public string WorkerEmailAddress { get; set; }
    }
}
