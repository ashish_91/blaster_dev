﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant
{
    public class Common
    {
        public class Address
        {
            public string steerAddress;
            public string addressLine2;
            public string city;
            public string state;
            public string zip;
            public string country;

            public string fullStreetAddress
            {
                get
                {
                    return (steerAddress + " " + addressLine2)?.Trim();
                }

                private set { }
            }

        }
    }
}
