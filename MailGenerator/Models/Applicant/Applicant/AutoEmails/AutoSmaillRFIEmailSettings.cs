﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant.AutoEmails
{
    public class AutoSmaillRFIEmailSettings
    {
        public bool enabled;

        public bool pausedUntilMoreDocumentsAreRequired;
    }
}
