﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant.AutoEmails
{
    public class AutoEmailEvents
    {
        public bool autoSendMailOnDocumentApproved;
        public bool autoSendMailOnDocumenRejected;
        public bool autoSendMailOnStatusChanged;
    }
}
