﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant.AutoEmails
{
    public class AutoRFIEmailSettings
    {
        public bool enabled;

        public int sendIntervalInDays;

        public bool onlyMondayToFriday;

        public string customTopTextMessage;
        public bool addCustomMessage;
    }
}