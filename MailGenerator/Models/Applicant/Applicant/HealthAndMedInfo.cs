﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant
{
    public class HealthAndMedInfo
    {
        public bool isMobilityissues;
        public string mobilityNotes;

        public bool isTransferringIssues;
        public string transferringNotes;

        public bool isBathingIssues;
        public string bathingNotes;

        public bool isDressingIssues;
        public string dressingNotes;

        public bool isEatingOrMealIssues;
        public string eatingOrMealNotes;

        public bool isToiletingIssues;
        public string toiletingNotes;

        public bool isIncontinenceBladder;
        public bool isIncontinenceBowel;
        public string incontinenceNotes;

        public bool isVisionIssues;
        public bool isLegallyBlind;
        public string visionNotes;

        public bool isOrientationIssues;
        public string orientationNotes;
        
        public bool isAggression;
        public bool isSelfInjury;
        public bool isWandering;
        public bool isDisruptive;
        public string behaviorNotes;

        public bool isParalysisIssues;
        public string paralisysNotes;

        public Alzheimer alzheimer;

        public bool isOxygen;
        public string oxygenNotes;

        public bool isDDD;
        public string DDDNotes;

        public bool isOther;
        public string otherNotes;
    }

    public class Alzheimer
    {
        public bool IsAlzheimer;
        public string diagnosingFirstName;
        public string diagnosingLastName;
        public string physicianField;
        public DateTime? diagnosisDate;
    }
}
