﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant.de
{
    public class InterviewInfo
    {
        public bool isMonday;
        public string mondayTime;

        public bool isTuesday;
        public string tuesdayTime;

        public bool isWednesday;
        public string wednesdayTime;

        public bool isThursday;
        public string thursdayTime;

        public bool isFriday;
        public string fridayTime;

        public bool isInterpreterNeeded;
        public string interpreterLanguage;
        public string interpreterHomeAddress;
        public string interpreterMajorCrossRoads;

    }
}
