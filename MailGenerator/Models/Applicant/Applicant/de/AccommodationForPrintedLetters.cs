﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant.de
{
    public class AccommodationForPrintedLetters
    {
        public bool isAlternativeFormatPrinterLetters;
        public string whoNeedsThis;
        public bool isReadablePDF;
        public bool isLargePrint;
        public bool isOther;
        public string otherText;
    }
}
