﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant.de
{
    public class CurrentLivingArrangement
    {
        public Enums.CurrentLiving currentLivingType;
        public string currentLivingText;
        public DateTime expectedDateOfDIscharge;
        public string facilityName;
        public Common.Address address;
        public string phoneNumber;
    }
}
