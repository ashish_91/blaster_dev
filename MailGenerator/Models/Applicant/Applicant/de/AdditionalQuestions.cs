﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant.de
{
    public class AdditionalQuestions
    {
        public bool isHelpNeededForPayingMedicalBills;
        public List<string> helpPayingMedicalBillsMonths;

        public bool isReceiveServiceFromDES;
        public DateTime serviceBegan;

        public bool isBef18Autism;
        public bool isBef18Cerebral;
        public bool isBef18IntelectualDisability;
        public bool isBef18SeizureDisorder;

        public bool isDevelopmentDelayBefore6;
        public bool isTrustor;
        public bool isPregnant;
    }
}
