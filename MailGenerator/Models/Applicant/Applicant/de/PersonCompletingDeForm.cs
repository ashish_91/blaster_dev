﻿using ApplicantsApp.Models.Applicant.Applicant.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant.de
{
    public class PersonCompletingDeForm
    {
        public string name;
        public string phone;
        public CompletingFormPersonType whoIsCompletingTheForm;
    }
}
