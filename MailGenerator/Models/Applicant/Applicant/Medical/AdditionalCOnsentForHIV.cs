﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant.Medical
{
    public class AdditionalCOnsentForHIV
    {
        public bool allowRelease;
        public bool notAllowRelease;

        public DateTime date;
    }
}