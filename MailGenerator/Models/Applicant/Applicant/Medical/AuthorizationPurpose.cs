﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant.Medical
{
    public class AuthorizationPurpose
    {
        public bool isAtMyRequest;
        public bool isOther;
        public string otherText;
        public bool authToCommunicate;
        public bool authToSell;
    }
}
