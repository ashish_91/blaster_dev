﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant.Medical
{
    public class PatientRights
    {
        public DateTime patientSignatureDate;

        public bool isPatientMinorOrUnableToSign;

        public bool isPatientMinor;
        public string patientMinorAge;

        public bool isUnableToSign;
        public string unableToSignBecause;

        public DateTime authRepSignDate;

        public bool isRepParent;
        public bool isRepGuardian;
        public bool isRepCourtOrder;

        public bool isRepOther;
        public string repAuthOtherText;
    }
}
