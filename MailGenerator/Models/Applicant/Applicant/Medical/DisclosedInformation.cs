﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant.Medical
{
    public class DisclosedInformation
    {
        public bool isAllInfo;
        public bool isCertainCondition;
        public string theCondition;
        public bool isForPeriod;
        public DateTime periodFrom;
        public DateTime periodTo;

        public bool isOther;
        public string otherText;

        public string disclosedToEmail;
    }
}
