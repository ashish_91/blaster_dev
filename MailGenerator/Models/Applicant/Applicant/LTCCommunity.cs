﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant
{
    public class LTCCommunity
    {
        public bool IsLtcCommunity;
        public string facilityName;
        public string facilityPhone;
        public string POCFirstName;
        public string POCLastName;
        public Common.Address address;
        public string facilityType;
        public DateTime? dateEntered;
    }
}
