﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Applicant.Applicant
{    
    public class PointOfContact
    {
        public bool isPOCAvaliable;
        public string relationship;
        public string phone;
        public string email;
        public string firstName;
        public string middleName;
        public string lastName;
        public Common.Address address;
    }
}
