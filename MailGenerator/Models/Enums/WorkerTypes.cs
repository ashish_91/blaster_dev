﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Enums
{
    public enum WorkerTypes
    {
        doctor = 0,
        medicalWorker = 1,
        financialWorker = 2,
        financialSupervisor = 3
    }

    public static partial class Helper
    {
        public static WorkerTypes GetWorkerTypeFromComboIndex(int selectedIndex)
        {
            switch (selectedIndex)
            {
                case 0: return WorkerTypes.doctor;
                case 1: return WorkerTypes.medicalWorker;
                case 2: return WorkerTypes.financialWorker;
                case 3: return WorkerTypes.financialSupervisor;
                default: throw new Exception($"Can't find worker type with index: {selectedIndex}");
            }
        }

        public static int GetComboIndexByWorkerType(WorkerTypes workerType)
        {
            switch (workerType)
            {
                case WorkerTypes.doctor: return 0;
                case WorkerTypes.medicalWorker: return 1;
                case WorkerTypes.financialWorker: return 2;
                case WorkerTypes.financialSupervisor: return 3;
                default: throw new ArgumentException($"SetComboIndexByWorkerType: {workerType}");
            }
        }

        public static string GetWorkerTypeNiceNameFromEnum(WorkerTypes workerType)
        {
            switch (workerType)
            {
                case WorkerTypes.doctor: return "Doctor/Physician";
                case WorkerTypes.medicalWorker: return "Medical Worker";
                case WorkerTypes.financialWorker: return "Financial Worker";
                case WorkerTypes.financialSupervisor: return "Financial Supervisor";
                default: throw new ArgumentException($"GetWorkerTypeNiceNameFromEnum: {workerType}");
            }
        }
    }
    /// <summary>
    /// Addition by Ashish on 15-09-2020
    /// </summary>
    public enum subscriptionStatus
    {
        Active = 2,
        PastDue = 1,
        Cancel = 0

    }
}
