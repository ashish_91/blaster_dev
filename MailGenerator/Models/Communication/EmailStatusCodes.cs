﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Communication
{
    public enum EmailStatusCodes
    {
        Success = 1,
        Error = -1,
        Scheduled = 2,
        Scheduled_AlreadySent = 4,
        Scheduled_ConditionsNotMeetToSend = 5
    }
}