﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Communication
{
    public enum EmailTypes
    {
        FreeText = 1,
        RFI = 2,
        AutoRFI = 3,
        AutoInterim = 4,
        AutoDocumentApproved = 5,
        AutoDocumentRejected = 6,
        AutoStatusChanged = 7,
        CaringComAPICall = 8,
        NotPayedInvoice = 9,
        PasAssssorAutoRequest = 10,
        MedicalEvaluationConfirmation = 11,
        LiteRFI = 12,
    }
}