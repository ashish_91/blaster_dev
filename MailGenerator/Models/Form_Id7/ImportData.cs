﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Form_Id7
{
    public class ImportData
    {
        public string EntryId;
        public string SourceUrl;
        public string LastName;
        public string ZipCode;
        public string Entry;
        public string FirstName;
        public string EntryDate;
        public bool InterestedInVeteranBenefits;
        public bool InterestedVeteranSpouseOfVeteran;
        public string FormTitle;
        public string Email;
        public string UserIP;
        public string Referer;
        public string TypeofCare;
        public string Phone;        
    }
}
