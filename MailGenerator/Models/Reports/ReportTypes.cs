﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models
{
    public enum ReportTypes
    {
        AllActions = 1,
        SentEmails = 2,
        SentFaxes = 3
    }
}
