﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models
{
    public enum ReportActions
    {
        
        //Applicant
        ApplicantCreate = 1,
        ApplicantRead = 2,        
        ApplicantUpdate = 3,
        ApplicantDelete = 4,
        ApplicantStatusChange = 5,
        ApplicantConvertFromGravityForm = 6,
        ApplicantChangeDueDate = 7,
        ApplicantChangeTransferSum = 8,
        ApplicantChangeTransferDate = 9,
        ApplicantChangeFinancialInterviewDate = 1000,
        ApplicantChangeIncomeOnlyTrust = 1001,
        ApplicantChangeFinancialCaseWorkName = 1002,
        ApplicantChangeFinancialCasePhoneNumber = 1003,
        ApplicantChangeFinancialCaseEmail = 1004,
        ApplicantChangeMedicalCaseWorkName = 1005,
        ApplicantChangeMedicalCasePhone = 1006,
        ApplicantChangeMedicalCaseEmail = 1007,
        ApplicantChangeBilled = 1008,
        ApplicantChangeCSRAAmount = 1009,
        ApplicantChangeApproxRestartDate = 1010,
        ApplicantChangeMoveDate = 1011,
        ApplicantAutoChangeRFISubmitDate = 1012,
        ApplicantAutoChangePASSubmitDate = 1013,
        ApplicantAutoChangeCSRASubmitDate = 1014,
        ApplicantChangeExceptionMessage = 1015,
        ApplicantChangePASApproved = 1016,
        ApplicantConvertFromStripe = 1017,
        ApplicantEmailSendToAskForPASassssor = 1018,
        ApplicantEmailSendToAskForPASassssor_AUTO = 1019,
        ApplicantDeleteScheduledEmail = 1020,
        ApplicantEmailSendToConfirmMedicalExac = 1021,

        //Sent stuff
        SendManualRFIEmail = 10,
        SendFax = 11,
        SendAutomaticRFIEmail = 12,
        SendAutomaticInterimEmail = 13,
        SendManualEmail = 14,
        CallCaringComAPI = 15,
        SendNotPayedInvoiceEmail = 16,
        SendAutomaticLiteRFIEmail = 17,

        //Representatives
        RepresentativeCreate = 20,        
        RepresentativeUpdate = 22,
        RepresentativeDelete = 23,

        //Medical/Physician 
        PhysycianCreate = 30,
        
        PhysycianUpdate = 32,
        PhysycianDelete = 33,

        //Documents
        DocumentCreate = 40,
        DocumentPreview = 41,        
        DocumentDelete = 43,
        DocumentDownload = 44,

        //Tasks
        TaskCreate = 50,
        TaskEdit = 51,
        TaskDelete = 52,
        TaskMarkAsDone = 53,
        TaskMarkAsNotDone = 54,
        TaskShowToUser = 55,

        //Scheduling
        ScheduleAdd = 60,
        ScheduleShowToUser = 61,

        //RFI
        RFIAddPlaceholder = 70,
        RFIDeletePlaceholder = 71,
        RFIUploadDocumentToPlaceHolder = 72,
        RFIDeleteDocument = 73,
        RFIViewDocument = 74,
        RFIRenameDocument = 75,
        RFIRejectDocument = 76,
        RFIApproveDocument = 77,
        ChangeDocumentNotes = 78,
        RFICategoryAddCustomDescription = 79,
        RFIUploadDocumentDragAndDrop = 80,


        //Categopries
        CategoryCreate = 100,
        CategoryEdit = 101,
        CategoryDelete = 102

    }
}
