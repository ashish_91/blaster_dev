var sessionId = null;
var userId = null;
var userName = null;

function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}

function ValidateUser() {
    console.log('Validate User');

    $.ajax({
        url: urlp + 'ValidateUser',
        type: 'POST',
        data: JSON.stringify({
            'sessionId': sessionId
        }),
        success: function (msg) {
            console.log(msg);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            ShowModalError('Error while validating user', GetErrMsg(xhr));
        }
    });
}

function showNotificationInfo(text) {
    $.notify({
        // options
        message: text
    }, {
            // settings
            type: 'info',
            offset: {
                x: 10,
                y: 100
            }, animate: {
                enter: 'animated bounceInDown',
                exit: 'animated bounceOutUp'
            }
        });
}

function showProgressBar() {
    $('#progress-bar').attr('hidden', false);
}

function hideProgressBar() {
    $('#progress-bar').attr('hidden', true);
}

function showNotificationSuccess(text) {
    $.notify({
        // options
        message: text
    }, {
            // settings
            type: 'success',
            offset: {
                x: 10,
                y: 100
            }, animate: {
                enter: 'animated bounceInDown',
                exit: 'animated bounceOutUp'
            }
        });
}

function showNotificationError(text) {
    $.notify({
        // options
        message: text
    }, {
            // settings
            type: 'danger',
            offset: {
                x: 10,
                y: 100
            }, animate: {
                enter: 'animated bounceInDown',
                exit: 'animated bounceOutUp'
            }
        });
}

function logout() {
    $.removeCookie("cookSessionId");
    $.removeCookie("cookUserId");
    $.removeCookie("cookUserName");
    window.location.href = "login.html";
}

function ValidateSession(callback) {
    $.ajax({
        url: urlp + 'ValidateSession',
        type: 'POST',
        data: JSON.stringify({
            'sessionId': sessionId
        }),
        success: function (msg) {
            callback;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            var msg = GetErrMsg(xhr);
            if (msg != null) {
                ShowModalError("Error while validating session", msg);
            }
        }
    });
}

function InitSession() {
    if ($.cookie("cookSessionId") && $.cookie("cookUserId") && $.cookie("cookUserName")) {
        sessionId = $.cookie("cookSessionId");
        userId = $.cookie("cookUserId");
        userName = $.cookie("cookUserName");
        ValidateSession();        

        return true;
    }

    return false;
}

function InsertNav(activate) {
    //console.log('asd');
    document.getElementById('nav-menu').innerHTML = '\
        <nav class="navbar navbar-default">\
            <div class="container-fluid">\
                <div class="navbar-header">\
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">\
                        <span class="icon-bar"></span>\
                        <span class="icon-bar"></span>\
                        <span class="icon-bar"></span>\
                    </button>\
                    <a href="login.html" class="navbar-brand" id="menu-user""></a>\
                </div>\
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">\
                    <ul class="nav navbar-nav">\
                        <li id="menu-search"><a href="javascript:window.location.href = \'search.html\'">Търсене</a></li>\
                        <li id="menu-new"><a href="javascript:window.location.href = \'addRecipe.html\'" >Нова</a></li>\
                        <li id="menu-profile"><a href="#" >Профил</a></li>\
                    </ul>\
                    <ul class="nav navbar-nav navbar-right">\
                        <li><a href="javascript:$.removeCookie(\'cookUserId\');$.removeCookie(\'cookSessionId\');window.location.href = \'login.html\'" id="menu-exit">Изход</a></li>\
                    </ul>\
                </div>\
            </div>\
        </nav>';

    if (activate)
        $('#' + activate).addClass('active');

    $('#menu-user').text(userName);
}

function GetNoms(nomName, assignTo, notAddAll) {
    $.ajax({
        url: urlp + 'GetNom?nomName=' + nomName + '&sessionId=' + sessionId,
        type: 'GET',
        success: function (msg) {
            if (assignTo) assignTo.innerHTML = CreateOptionsFromArray(msg, null, notAddAll);
            if (nomName === 'recipeSubTypes') recipeSubTypes = msg;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            ShowModalError('Грешка при зареждане', GetErrMsg(xhr));
        }
    });
}

function CreateOptionsFromArray(arr, val, notAddAll) {
    if (!notAddAll) var res = "<option value='0'>Всички</option>";
    for (e in arr) {
        if (val) {
            if (arr[e].typeId === val) {
                res += "<option value='" + arr[e].id + "'>" + arr[e].name + "</option>";
            }
        } else {
            res += "<option value='" + arr[e].id + "'>" + arr[e].name + "</option>";
        }
    }

    return res;
}

function ShowTextBoxModalPicker(title, textBoxTitle, okFunc, cancelFunc) {
    var $textAndPic = $('<div></div>');

    $textAndPic.append('<label class="form-control-label" for="modal-name">' + textBoxTitle + '</label><input type="text" class="form-control" name="modal-name" id="modal-name" value="">');

    BootstrapDialog.show({
        title: title,
        message: $textAndPic,

        buttons: [{
            label: 'OK',
            action: okFunc,
            cssClass: 'btn-primary'
        }, {
            label: 'Cancel',
            action: cancelFunc
        }]
    });
}

function getLocalTime() {
    var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
    return (new Date(Date.now() - tzoffset)).toISOString();
}

function ShowModalError(title, msg, func) {
    BootstrapDialog.alert({
        title: title,
        message: msg,
        type: BootstrapDialog.TYPE_DANGER,
        draggable: true,
        closable: true,
        buttonLabel: 'Close',
        callback: function (result) {
            if (func) func();
        }
    });
}

function ShowModalSuccess(title, msg, func) {
    BootstrapDialog.alert({
        title: title,
        message: msg,
        type: BootstrapDialog.TYPE_SUCCESS,
        draggable: true,
        closable: true,
        buttonLabel: 'Close',
        callback: function (result) {
            if (func) func();
        }
    });
}

function GetErrMsg(xhr) {
    var errMsg = 'undefined error';
    if (xhr) {
        if (!xhr.responseText) {
            errMsg = xhr.statusText;
        }
        else {
            var res = JSON.parse(xhr.responseText);

            if (res.Message)
                errMsg = res.Message;
            else
                errMsg = xhr.responseText;
        }
    }

    if (errMsg === 'internal:choose-price-plan') {
        window.location.href = 'my-account.html?tab=price-plans';
        return null;
    }
    if (errMsg === 'internal:bad-credit-card') {
        window.location.href = 'my-account.html?tab=payment-methods&msg=missing-payment-method';
        return null;
    }
    if (errMsg === 'internal:invalid-session') {
        console.log(location.pathname.substring(location.pathname.lastIndexOf("/") + 1));
        if (location.pathname.substring(location.pathname.lastIndexOf("/") + 1) !== 'login.html') {
            ShowModalError("Invalid session!", "Invalid session! Please login again.");
            window.location.href = 'login.html';            
        }
        
        return null;
    }

    return errMsg;
}

function CalcSHA512(param) {
    var jsSha = new jsSHA("kjhjkh786&&&6df6" + param + "00**777766", "ASCII");
    return jsSha.getHash("SHA-512", "HEX");
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function IsValidEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function Combofy(nomId, ident, addInfo) {
    if (!addInfo) addInfo = false;

    $.get("http://91.230.245.69:99/anyRest/api/any/GetNom?nomId=" + nomId + "&createOption=true&addInfo=" + addInfo, function (data, status) {
        var element = $('#' + ident);
        element.append(data);
        element.combobox();
    });
}

function Tokenfy(nomId, ident, addInfo) {
    if (!addInfo) addInfo = false;
    var element = $('#' + ident);

    if (nomId) {
        $.get("http://91.230.245.69:99/anyRest/api/any/GetNom?nomId=" + nomId + "&createOption=false&addInfo=" + addInfo, function (data, status) {

            element.tokenfield({
                autocomplete: {
                    source: data,
                    delay: 100
                },
                showAutocompleteOnFocus: true
            });
        });
    } else {
        element.tokenfield({
            autocomplete: {
                source: [],
                delay: 100
            },
            showAutocompleteOnFocus: true
        });
    }

    element.on('tokenfield:createtoken', function (event) {
        PreventDuplicateToken(event, this);
    });
}

function ChangeTab(toWhat) {
    var calcedUrl = '.nav-pills a[href=' + toWhat + ']';
    $(calcedUrl).tab('show');
    //console.log(calcedUrl);
}

function PreventDuplicateToken(e, self) {
    var existingTokens = $(self).tokenfield('getTokens');
    $.each(existingTokens, function (index, token) {
        if (token.value === e.attrs.value)
            e.preventDefault();
    });
}

function InitCheckBoxSkin() {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '50%' // optional
    });

}

function ConvertFormToJSON(form) {
    return jQuery(form).serializeArray();
}

/**
 *
 *  Base64 encode / decode
 *  http://www.webtoolkit.info/
 *
 **/
var Base64 = {

    // private property
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

    // public method for encoding
    encode: function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = (chr1 & 3) << 4 | chr2 >> 4;
            enc3 = (chr2 & 15) << 2 | chr3 >> 6;
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
                this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
                this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },

    // public method for decoding
    decode: function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = enc1 << 2 | enc2 >> 4;
            chr2 = (enc2 & 15) << 4 | enc3 >> 2;
            chr3 = (enc3 & 3) << 6 | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 !== 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 !== 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = Base64._utf8_decode(output);

        return output;

    },

    // private method for UTF-8 encoding
    _utf8_encode: function (string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    // private method for UTF-8 decoding
    _utf8_decode: function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while (i < utftext.length) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }

};