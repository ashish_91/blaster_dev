function doLogin(){

	console.log('logging in...');
	
    $('#buttonLogin').prop('disabled', true);

    $.ajax({
        url: urlp + 'Login',
        type: 'POST',
		data: JSON.stringify({
				'username' : $('#username').val(),
				'password' : CalcSHA512($('#password').val())
			}),
        success: function (msg) {
            $('#buttonLogin').prop('disabled', false);
            sessionId = msg.sessionId;
            userId = msg.userId;
            userName = msg.userName;

            if ($('#remember_me').prop('checked')) {
                $.cookie("cookSessionId", sessionId, { expires : 365 });
                $.cookie("cookUserId", userId, { expires : 365 });
                $.cookie("cookUserName", userName, { expires : 365 });
            } else {
                $.cookie("cookSessionId", sessionId);
                $.cookie("cookUserId", userId);
                $.cookie("cookUserName", userName);
            }

            window.location.href = "index.html";
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            $.removeCookie("cookSessionId");
            $('#buttonLogin').prop('disabled', false);
            ShowModalError('Error during login', GetErrMsg(xhr));
        }
    });
}

function InitSession() {
    if ($.cookie("cookSessionId") && $.cookie("cookUserId") && $.cookie("cookUserName")) {
        sessionId = $.cookie("cookSessionId");
        userId = $.cookie("cookUserId");
        userName = $.cookie("cookUserName");
        return true;
    }

    return false;
}

function goToRegister() {
    window.location.href = "register.html";
}

function goToForgetPassword() {
    window.location.href = "forgotpassword.html";
}

function goToIndex(){
    window.location.href = "index.html";
}

function onReady() {
	
	console.log('ready');
    if (InitSession()) {
        if (ValidateSession(goToIndex));
    }

	$("#buttonLogin").click(doLogin);
    $("#button-register").click(goToRegister);	
    $("#button-forget-password").click(goToForgetPassword);	

	InitForm();
	
    //$.when(
    //    $.getScript("libs/include.js"),
    //    $.getScript("libs/loginValidator.js"),
    //    $.Deferred(function( deferred ){
    //        $( deferred.resolve );
    //    })
    //).done(function(){
    //        LoadCommonLibraries(InitForm);
    //});

    if (getURLParameter("showSuccessRegister") === "true") {		
		ShowModalSuccess("Successful activation", "Your account have been activated successfully, you can now login.");
	}

    if (getURLParameter("showSuccessPasswordReset") === "true") {		
		ShowModalSuccess("Successful password reset", "Your password has been changed successfully! Now you can login.");
    }

    $("form").bind("keypress", function (e) {
        if (e.keyCode === 13) {
            doLogin();
            return false; 
        }
    });
}

function InitForm() {    
    InitCheckBoxSkin();

    //Validate();
}