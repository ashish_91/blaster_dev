function onIHaveInterestClick(){

	console.log('trying to show interest');
    
    $('#button-i-have-interest').prop('disabled', true);

    $.ajax({
        url: urlp + 'YapIAmInterested',
        type: 'POST',
        data: JSON.stringify({
            'eventId': eventId
        }),
        success: function (msg) {
            ShowModalSuccess("Info", msg.note);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);            
            $('#button-i-have-interest').prop('disabled', false);
            ShowModalError('Error during trying to store your vote', GetErrMsg(xhr));
        }
    });
}

var eventId;
function LoadData() {
    //Hello, {{specific-facility-name}}, do you have interest in this patient?

    eventId = getURLParameter("eventId");  
	console.log(eventId);

    $.ajax({
        url: urlp + 'GetPatientPlusFacilityInfo',
        type: 'POST',
        data: JSON.stringify({
            'eventId': eventId
        }),
        success: function (msg) {
            hideProgressBar();
            console.log(msg);
            if (msg.flName) {
                $('#firstLastName').text(msg.flName);
                $('#patient-budget').text(msg.patientBudget);
				$('#semi-private-room-budget').text(msg.semiPrivateBudget);
                $('#age').text(msg.age);
                $('#customMessage').html(msg.customMessage);
				$('#poc').text(msg.poc);
				
            } else {
                ShowModalError('Patient', 'The patient no longer exists. Sorry for the inconvenience.');
            }

            if (msg.facilityName) {
                $('#text-hello').text('Hello, ' + msg.facilityName + ', do you have interest in this patient?');
            } else {
                ShowModalError('Care home', 'Sorry, but this care home can\'t bne found in our database. If you, however, are real care home and have interest in that patient please call 0101010. Thank you.');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            hideProgressBar();
            console.log(xhr);            
            ShowModalError('Error while loading data', GetErrMsg(xhr));
        }
    });
}

function onReady() {	
	console.log('ready');
	$("#button-i-have-interest").click(onIHaveInterestClick);
    InitCheckBoxSkin();

    LoadData();
}