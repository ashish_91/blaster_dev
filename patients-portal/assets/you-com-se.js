function onSubmitClick() {

    console.log('Submitting answers...');

    $('#button-submit').prop('disabled', true);

    var data = {};

    $.each($(":checkbox"), function (index, value) {
        console.log(value.id, $(value).prop('checked'));
        data[value.id] = $(value).prop('checked');
    });

    $.each($("textarea"), function (index, value) {
        console.log(value.id, $(value).val());
        data[value.id] = $(value).val();
    });

    console.log(data);

    $.ajax({
        url: urlp + 'SubmitAnswers',
        type: 'POST',
        data: JSON.stringify({
            'eventId': eventId,
            'answers': data
        }),
        success: function (msg) {
            ShowModalSuccess("Info", msg.note);
			console.log('success');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            $('#button-submit')..prop('disabled', false);
            ShowModalError('Error during trying to store your vote', GetErrMsg(xhr));
        }
    });
}

var eventId;
function LoadData() {    
    eventId = getURLParameter("eventId");
    console.log(eventId);

    $.ajax({
        url: urlp + 'LoadQuestionCampaign',
        type: 'POST',
        data: JSON.stringify({
            'eventId': eventId
        }),
        success: function (msg) {
            hideProgressBar();
            $("#div-table").html(msg.table);
            $(":checkbox").bootstrapToggle();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            hideProgressBar();
            console.log(xhr);
            ShowModalError('Error while loading data', GetErrMsg(xhr));
        }
    });
}

function onReady() {    
    console.log('ready');
}