﻿using Flurl.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;

namespace PortalREST
{
    public static class Helper
    {
        private static string apPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;

        public static int? ParseInt(string s)
        {
            if (String.IsNullOrWhiteSpace(s))
                return null;

            if (int.TryParse(s, out int res))
                return res;

            return null;
        }

        public static void LogByIdent(decimal uniqueId, string message)
        {
            try
            {
                string dir = Path.Combine(apPath, "requests", DateTime.Now.ToString("yyyy-MM-dd"));
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }

                message = String.Format("[{0}] {1}\n", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), message);

                File.AppendAllText(Path.Combine(dir, uniqueId.ToString() + ".txt"), message);
            }
            catch
            {
                //how to log, huh?
            }
        }

        public static void LogHeader(HttpRequestMessage request, string method, string data, decimal uniqueIdent)
        {
            string formattedData = TryFormatJSON(data);

            string header = String.Format(@"INPUT
execId: {3}
request: {0}
method: {1}
data: {2}
HTTPMethod: {4}
", request.RequestUri.OriginalString, method, formattedData, uniqueIdent, request.Method);

            LogByIdent(uniqueIdent, header);
        }

        private static string TryFormatJSON(string data)
        {
            string formattedData = data;
            if (!string.IsNullOrWhiteSpace(data))
            {
                try
                {
                    formattedData = JObject.Parse(data).ToString(Formatting.Indented);
                }
                catch { }
            }
            return formattedData;
        }

        public static JsonSerializerSettings convertSettings = new JsonSerializerSettings()
        {
            NullValueHandling = NullValueHandling.Ignore,
            DateFormatString = "yyyy.MM.ddTHH:mm:ss",
            DateTimeZoneHandling = DateTimeZoneHandling.Local
        };

        public static HttpResponseMessage HandleError(Logger logger, Exception ex, HttpStatusCode code)
        {
            if (logger!=null) logger.Error(ex);
            return Resp(ex, code);
        }

        public static HttpResponseMessage HandleError(Logger logger, string errMsg, HttpStatusCode code)
        {
            logger.Error(errMsg);
            return Resp(errMsg.Replace("','", "', '"), code);
        }

        public static JObject GetContent(HttpRequestMessage request)
        {
            string res = request.Content.ReadAsStringAsync().Result;

            if (String.IsNullOrWhiteSpace(res)) return new JObject();

            return JObject.Parse(res);
        }

        public static string GetVersionIdent(Assembly ass)
        {
            if (ass == null) return "empty";
            AssemblyName name = ass.GetName();
            if (name == null) return "empty";

            return name.Name + " " + name.Version.ToString();
        }

        public static string GetVersionString()
        {
            return
                GetVersionIdent(Assembly.GetExecutingAssembly()) + " " +
                GetVersionIdent(Assembly.GetCallingAssembly()) + " " +
                GetVersionIdent(Assembly.GetEntryAssembly());
        }

        public static HttpResponseMessage Resp(object message, HttpStatusCode code = HttpStatusCode.OK)
        {
            return new HttpResponseMessage(code)
            {
                Content = new StringContent(JsonConvert.SerializeObject(message, convertSettings), Encoding.UTF8, "application/json")
            };
        }

        public static string GetClientIp(HttpRequestMessage request)
        {
            try
            {
                if (request.Properties.ContainsKey("MS_HttpContext"))
                {
                    return ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
                }
                else if (HttpContext.Current != null)
                {
                    return HttpContext.Current.Request.UserHostAddress;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static void CallCaring(NLog.Logger logger, string caringData)
        {
            logger.Info("Calling 'https://dir.caring.com/api/v2/inquiries.json?pid=da5a7a7cd60c'");

            ServicePointManager.ServerCertificateValidationCallback +=
                  (sender, certificate, chain, sslPolicyErrors) => true;

            HttpResponseMessage resp = "http://dir.caring.com/api/v2/inquiries.json?pid=da5a7a7cd60c"
                .WithHeader("Content-Type", "application/json")
                .WithHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134")
                .WithHeader("Accept", "*/*")
                .AllowHttpStatus("100-700")
                .PostStringAsync(caringData)
                .Result;

            logger.Info($"Resp code: {resp.StatusCode}");
            string respBody = resp.Content.ReadAsStringAsync().Result;
            logger.Info($"Resp body: {respBody}");

            if (resp.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception(resp.StatusCode.ToString() + " " + respBody);
            }

        }

        public static decimal? StringToNullDecimal(string input)
        {
            if (String.IsNullOrWhiteSpace(input))
                return null;

            string sep = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            input = input.Replace(".", sep).Replace(",", sep).Trim();

            return decimal.Parse(input);
        }

        public static double? StringToNullDouble(string input)
        {
            if (String.IsNullOrWhiteSpace(input))
                return null;

            string sep = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            input = input.Replace(".", sep).Replace(",", sep).Trim();

            return double.Parse(input);
        }
    }
}