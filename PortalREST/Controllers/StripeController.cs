﻿using Newtonsoft.Json;
using Shared;
using Shared.Source.MailGeneration;
using Stripe;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace PortalREST.Controllers
{
    public class StripeController : ApiController
    {
        //public const string StripeApiKey = "rk_test_uVvYJyPPghQAvmzjWnAbfCxj00hcbOhJy6";
        public const string StripeApiKey = "sk_live_7hMRqrf9iejUj7HahNxxZMhu00kVzfhdq2";

        private const string BaseCustomersAddress = "https://dashboard.stripe.com/test/customers/";

        private enum InvoiceType
        {
            manual = 1,
            automatic = 2,
            other = 3,
            refund = 4,
            paymentIntent = 5
        }

        [HttpGet]
        public IHttpActionResult HandleInvoices(bool? skipEmailingForNotPayedInvoices = false)
        {
            if (Request.Method == HttpMethod.Options)
                return Ok();

            StringBuilder reportText = new StringBuilder();

            WebInit webData = new WebInit();
            try
            {
                webData.InitMethod(Request, System.Reflection.MethodBase.GetCurrentMethod().Name);

                HandleInvoices(skipEmailingForNotPayedInvoices, reportText, webData);
                HandlePaymentIntents(reportText, webData);
                HandleRefunds(reportText, webData);
            }
            finally
            {
                Shared.Source.Mop.NotifyMe("Stripe", reportText.ToString(), webData.logger, false);
            }

            return Ok(reportText.ToString());
        }

        private static void HandlePaymentIntents(StringBuilder reportText, WebInit webData)
        {
            int daysAgo = (int)(DateTime.Now - new DateTime(2020, 3, 1)).TotalDays;
            List<PaymentIntent> paymentIntents = Shared.Source.Stripe.GetPaymentIntents(StripeApiKey, daysAgo);
            reportText.AppendLine(); reportText.AppendLine(); reportText.AppendLine();

            reportText.AppendLine($"=== Handling Payment Intents ===");
            reportText.AppendLine($"PI count downloaded: {paymentIntents.Count}");

            //Process them
            foreach (PaymentIntent pi in paymentIntents)
            {
                //will be handled from invoice handler
                if (pi.InvoiceId != null) continue;

                webData.logger.Trace(JsonConvert.SerializeObject(pi));

                try
                {
                    reportText.Append($"PI {pi.Id} created on {pi.Created} | status: {pi.Status} | payed: {pi.AmountReceived / 100m} | TO pay: {pi.Amount / 100m}");

                    using (var db = new DbModel())
                    {
                        ApplicantsNew applicant = GetOrCreateApplicantByStripeId(webData.logger, reportText, pi.CustomerId);
                        var applicantModel = JsonConvert.DeserializeObject<ApplicantsApp.Models.Applicant.Applicant.FullApplicantInfo>(applicant.Data);

                        var dbPi = db.Applicants_StripeInvoiceRegister.SingleOrDefault(s => s.InvoiceId.Equals(pi.Id));

                        string status = pi.Status;
                        bool isPayed = pi.AmountReceived == pi.Amount;
                        bool isInDb = dbPi != null;
                        bool sumChanged = isInDb && (dbPi.AmountPaid * 100.0m != pi.AmountReceived);
                        bool skipHandling = GetBoolFromMetadata(pi, "inv-handler_skip");

                        if (sumChanged || !isInDb)
                        {
                            dbPi = CreateOrUpdateInvoiceFromPaymentIntent(pi, db);
                        }

                        if (!isPayed)
                        {
                            if (
                                status.Equals("processing")
                                || status.Equals("requires_confirmation")
                                || status.Equals("requires_action")
                                || status.Equals("requires_capture")
                                )
                            {
                                if ((DateTime.Now - pi.Created.Value).TotalDays < 9)
                                {
                                    reportText.AppendLine("  => skip, still processing");
                                    continue;
                                }

                                if (skipHandling)
                                {
                                    reportText.AppendLine("  => skip, inv-handler_skip == true");
                                    continue;
                                }

                                //Notify local STUFF
                                var representative = ApplicantsApp.Db.LoadRepresentative(applicant.RepresentativeId.Value, webData.logger);
                                string info = null, errors = null;
                                List<Shared.ApplicantsActionsEmailDetails> emailInfos = MailGenerator.SendEmail(
                                    ApplicantsApp.Models.ReportActions.SendNotPayedInvoiceEmail,
                                    Guid.NewGuid(),
                                    ref info,
                                    ref errors,
                                    null,
                                    null,
                                    new string[] { "iwm2stuff@gmail.com", "jacob@seniorplanning.org", "jaeger@seniorplanning.org" },
                                    $"Stripe customer with Id: {pi.CustomerId} has Payment Intent with id: {pi.Id} with status: {pi.Status} for more than 2 days!",
                                    "URGENT: Action Needed",
                                    webData.logger,
                                    applicant.Id,
                                    2,
                                    representative,
                                    System.IO.File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath(@"~/Documents/email_footer_rep.html")),
                                    new Guid("00000000-0000-0000-0000-000000000002"),
                                    "Billing Department"
                                    );

                                continue;
                            }

                            if (status.Equals("canceled"))
                            {
                                reportText.AppendLine("  => skip, it is canceled");
                                continue;
                            }

                            var retries = paymentIntents
                                .Where(w => w.CustomerId.Equals(pi.CustomerId))
                                .Where(w => w.Created.Value > pi.Created.Value)
                                .Where(w => w.Amount.Equals(pi.Amount))
                                .Where(w => w.Status.Equals("succeeded"))
                                .ToList();

                            if (retries.Count > 0)
                            {
                                reportText.AppendLine($"  => SKIP, RETRY detected, count: {retries.Count}, first piID: {retries.First().Id}");
                                continue;
                            }

                            if (skipHandling)
                            {
                                reportText.AppendLine("  => skip, inv-handler_skip == true");
                                continue;
                            }

                            //statuses:
                            //      requires_payment_method, 
                            //      requires_confirmation, 
                            //      requires_action, 
                            //      processing, 
                            //      requires_capture, 
                            //      canceled, 
                            //      succeeded                             
                            RemindUserToPayPaymentIntent(
                                reportText,
                                webData,
                                pi,
                                InvoiceType.paymentIntent,
                                db,
                                applicant,
                                applicantModel,
                                webData.logger
                                );
                        }

                        //If is payed AND is not in the database at all OR the sum just changed
                        if (isPayed && (!isInDb || sumChanged))
                        {
                            //see is this a CHARGE to subscription
                            //{ "custom_type", "manual_charge" },
                            //{ "customerId", customerId },
                            //{ "subscriptionId", subscriptionId },
                            //{ "planId", planId },
                            //{ "productId", productId },

                            if (pi.Metadata?.ContainsKey("custom_type") == true)
                            {
                                string customType = pi.Metadata["custom_type"];
                                string planId = pi.Metadata["planId"];
                                string productId = pi.Metadata["productId"];
                                string subscriptionId = pi.Metadata["subscriptionId"];

                                if (customType.Equals("manual_charge"))
                                {
                                    //yeah, it is a charge!
                                    var sub = db.Applicants_CustomersSubscriptions
                                        .Where(w => w.ApplicantId.Equals(applicant.Id))
                                        .Where(w => w.StripePlanId.Equals(planId))
                                        .Where(w => w.StripeProductId.Equals(productId))
                                        .Where(w => w.SubscriptionId.Equals(subscriptionId))
                                        .SingleOrDefault();

                                    if (sub == null)
                                    {
                                        throw new Exception($"Charge can't be detected, add info: {pi.Id} => {JsonConvert.SerializeObject(pi.Metadata)}");
                                    }

                                    sub.TotalPayedSoFar += pi.AmountReceived.Value / 100.0m;
                                    db.SaveChanges();
                                    reportText.AppendLine($"   => CHARGE DETECTED! sub: ${pi.AmountReceived.Value / 100.0m}");
                                }
                            }

                        }

                    }
                }
                catch (Exception ex)
                {
                    webData.logger.Error(ex);
                    Shared.Source.Mop.NotifyMeOnException("IMPORTANT!!! PAYMENT MAY NOT BE REGISTERED!!! Stripe HandleUnpayedInvoices", ex, webData.logger, false, webData?.json?.ToString());
                }
            }
        }

        private static bool GetBoolFromMetadata(PaymentIntent pi, string key)
        {
            if (pi.Metadata.ContainsKey(key))
            {
                string skipNotify = pi.Metadata[key];
                if (skipNotify?.Trim()?.ToLowerInvariant()?.Equals("true") == true
                    || skipNotify?.Trim()?.ToLowerInvariant()?.Equals("1") == true
                    )
                {
                    return true;
                }
            }

            return false;
        }

        private static void HandleInvoices(bool? skipEmailingForNotPayedInvoices, StringBuilder reportText, WebInit webData)
        {
            //Get invoices
            List<Invoice> invoices = GetInvoices(webData.logger);

            reportText.AppendLine(); reportText.AppendLine(); reportText.AppendLine();

            reportText.AppendLine($"=== Handling invoices ===");
            reportText.AppendLine($"Invoices count downloaded: {invoices.Count}");

            //Process them
            foreach (var invoice in invoices)
            {
                webData.logger.Trace(JsonConvert.SerializeObject(invoice));

                try
                {
                    reportText.AppendLine($"invoice {invoice.Id} created on {invoice.Created} | payed: {invoice.Paid} | toPay: ${invoice.AmountRemaining / 100m} | payed sum: {invoice.AmountPaid / 100m}");

                    InvoiceType billingReason = InvoiceType.other;

                    if (invoice.BillingReason == "manual") billingReason = InvoiceType.manual;
                    else if (invoice.BillingReason == "subscription_create" || invoice.BillingReason == "subscription_cycle") billingReason = InvoiceType.automatic;
                    else throw new Exception($"Unsupported billing reason: {invoice.BillingReason}");

                    using (var db = new Shared.DbModel())
                    {
                        ApplicantsNew applicant = GetOrCreateApplicantByStripeId(webData.logger, reportText, invoice.CustomerId);

                        Subscription subscription = null;
                        if (billingReason == InvoiceType.automatic)
                        {
                            subscription = Shared.Source.Stripe.GetSubscriptionById(StripeApiKey, invoice.SubscriptionId);
                        }

                        bool isNotPayed = DetermineIsInvoicePayed(invoice, billingReason);

                        if (billingReason == InvoiceType.automatic) { }

                        //not payed at all, should notify user
                        if (isNotPayed)
                        {
                            if (!(skipEmailingForNotPayedInvoices ?? false))
                            {
                                RemindUserToPayInvoice(reportText, webData, invoice, billingReason, db, applicant, subscription, webData.logger);
                                db.SaveChanges();
                            }

                            //If condition added by Ashish on 17-09-2020
                            if (subscription != null)
                            {
                                CreateSubscriptionIfNotExists(invoice, db, applicant, subscription, out decimal totalOwnedForThisProduct, out Applicants_CustomersSubscriptions dbSubscription);
                            }
                        }
                        else if (invoice.Paid)
                        {
                            var dbInvoice = db.Applicants_StripeInvoiceRegister.SingleOrDefault(s => s.InvoiceId.Equals(invoice.Id));

                            //we have it in the db and it's already payed, in the db
                            if (dbInvoice != null && dbInvoice.Paid)
                            {
                                reportText.AppendLine($"     => payed and already register in DB");
                                continue;
                            }

                            //update it, or create it                                
                            dbInvoice = HandleNewPayedInvoice(reportText, invoice, billingReason, db, applicant, subscription);

                        }
                        else
                        {
                            reportText.AppendLine($"    => skip this invoice, dueDate: {invoice.DueDate}, STATUS: {invoice.Status}");
                        }

                    }
                }
                catch (Exception ex)
                {
                    webData.logger.Error(ex);
                    Shared.Source.Mop.NotifyMeOnException("IMPORTANT!!! PAYMENT MAY NOT BE REGISTERED!!! Stripe HandleUnpayedInvoices", ex, webData.logger, false, webData?.json?.ToString());
                }
            }
        }

        private static Applicants_StripeInvoiceRegister HandleNewPayedInvoice(StringBuilder reportText, Invoice invoice, InvoiceType billingReason, DbModel db, ApplicantsNew applicant, Subscription subscription)
        {
            Applicants_StripeInvoiceRegister dbInvoice = CreateOrUpdateInvoice(invoice, subscription, db, billingReason);

            if (billingReason == InvoiceType.automatic)
            {
                decimal totalOwnedForThisProduct;
                Applicants_CustomersSubscriptions dbSubscription;
                CreateSubscriptionIfNotExists(invoice, db, applicant, subscription, out totalOwnedForThisProduct, out dbSubscription);

                dbSubscription.TotalPayedSoFar += invoice.AmountPaid / 100m;

                db.SaveChanges();

                reportText.AppendLine($"     => handled! total payed: ${dbSubscription.TotalPayedSoFar}");

                if (dbSubscription.TotalPayedSoFar >= totalOwnedForThisProduct)
                {
                    //we should stop the subsr
                    reportText.AppendLine($"!!!!! STOP SUBSCRIPTION =>  {subscription.Id}");

                    Subscription sub = Shared.Source.Stripe.CancelSubscriptionById(StripeApiKey, subscription.Id);
                    if (!sub.Id.Equals(subscription.Id)) throw new Exception("Impossible! Check the source!");
                    dbSubscription.IsActive = false;
                    db.SaveChanges();
                }
            }

            return dbInvoice;
        }

        private static void CreateSubscriptionIfNotExists(Invoice invoice, DbModel db, ApplicantsNew applicant, Subscription subscription, out decimal totalOwnedForThisProduct, out Applicants_CustomersSubscriptions dbSubscription)
        {
            Product product = Shared.Source.Stripe.GetProductById(StripeApiKey, subscription.Plan.ProductId);

            totalOwnedForThisProduct = decimal.Parse(product.Metadata["total_amount"]);

            //but first maybe the customer does not have subscription in our database?
            dbSubscription = db.Applicants_CustomersSubscriptions
                .Where(w => w.StripeProductId.Equals(product.Id))
                .Where(w => w.ApplicantId.Equals(applicant.Id))
                .Where(w => w.StripePlanId.Equals(subscription.Plan.Id))
                .Where(W => W.SubscriptionId.Equals(subscription.Id))
                .SingleOrDefault();

            //no sub? create it
            if (dbSubscription == null)
            {
                //need to isnert it
                dbSubscription = ApplicantsApp.Db.AddSubscription(invoice.Created.Day, db, applicant.Id, subscription, product, totalOwnedForThisProduct, 0);
                db.SaveChanges();
            }
        }

        private static void RemindUserToPayInvoice(StringBuilder reportText, WebInit webData, Invoice invoice, InvoiceType billingReason, DbModel db, ApplicantsNew applicant, Subscription subscription, NLog.Logger logger)
        {
            reportText.AppendLine($"     => not payed, emailed");
            string automaticText = invoice.NextPaymentAttempt.HasValue
            ? $" Next automatic attempt will be on {invoice.NextPaymentAttempt.Value.ToString("f")}."
            : " "
            ;
            string emailText = $@"
Hello, 
Your account is currently overdue with Senior Planning in the amount of ${invoice.AmountRemaining / 100m}.{automaticText}
<b>Please click through the following link to update your credit card:</b> {invoice.HostedInvoiceUrl}
You can view the invoice as a pdf here: {invoice.InvoicePdf}
Feel free to call to update billing as well.
";
            string info = string.Empty;
            string errors = string.Empty;

            var representative = ApplicantsApp.Db.LoadRepresentative(applicant.RepresentativeId.Value, webData.logger);

            if (invoice.ChargeId != null)
            {
                Shared.Applicants_StripeInvoiceRegister dbInvoice = CreateOrUpdateInvoice(invoice, subscription, db, billingReason);
            }

            List<Shared.ApplicantsActionsEmailDetails> emailInfos = MailGenerator.SendEmail(
                ApplicantsApp.Models.ReportActions.SendNotPayedInvoiceEmail,
                Guid.NewGuid(),
                ref info,
                ref errors,
                null,
                null,
                new string[] { "iwm2stuff@gmail.com", invoice.CustomerEmail },
                emailText,
                "URGENT: Action Needed Immediately",
                webData.logger,
                applicant.Id,
                2,
                representative,
                System.IO.File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath(@"~/Documents/email_footer_rep.html")),
                new Guid("00000000-0000-0000-0000-000000000002"),
                "Billing Department"
                );

            foreach (var emailInfo in emailInfos)
            {
                db.Applicants_StripeInvoiceTransactions.Add(new Shared.Applicants_StripeInvoiceTransactions()
                {
                    ActionId = -1,  //email sent for not payed invoice
                    ActionMessage = $"Email sent to {emailInfo.To}",
                    CorrelationId = emailInfo.Id,
                    CreationDate = DateTime.Now,
                    InvoiceId = invoice.Id
                });
            }

            //Send SMS

            try
            {
                string shortURL = Shared.Source.ShortifyHelper.MakeShortUrl(logger, invoice.HostedInvoiceUrl);
                string textMessage = $"URGENT: Your account with Senior Planning is past due. Please update your payment details {shortURL} or call 1-800-531-5118 ";
                Shared.Source.TwilioHelper.SendSms(logger, invoice.CustomerPhone, textMessage);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                Shared.Source.Mop.NotifyMeOnException("SMS SEND NOT Payed invoice", ex, logger, false, "");
            }
        }

        private static void RemindUserToPayPaymentIntent(
            StringBuilder reportText,
            WebInit webData,
            PaymentIntent paymentIntent,
            InvoiceType billingReason,
            DbModel db,
            ApplicantsNew applicant,
            ApplicantsApp.Models.Applicant.Applicant.FullApplicantInfo applicantInfo,
            NLog.Logger logger)
        {
            reportText.AppendLine($"     => not payed, emailed");

            string emailText = $@"
Hello, 
Your account is currently overdue with Senior Planning in the amount of ${ (paymentIntent.Amount - paymentIntent.AmountReceived) / 100m}.
Reason is {paymentIntent.Description}
Feel free to call to update the billing method.
";
            string info = string.Empty;
            string errors = string.Empty;

            var representative = ApplicantsApp.Db.LoadRepresentative(applicant.RepresentativeId.Value, webData.logger);

            List<Shared.ApplicantsActionsEmailDetails> emailInfos = MailGenerator.SendEmail(
                ApplicantsApp.Models.ReportActions.SendNotPayedInvoiceEmail,
                Guid.NewGuid(),
                ref info,
                ref errors,
                null,
                null,
                new string[] { "iwm2stuff@gmail.com", applicantInfo.pointOfContact.email },
                emailText,
                "URGENT: Action Needed Immediately",
                webData.logger,
                applicant.Id,
                2,
                representative,
                System.IO.File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath(@"~/Documents/email_footer_rep.html")),
                new Guid("00000000-0000-0000-0000-000000000002"),
                "Billing Department"
                );

            foreach (var emailInfo in emailInfos)
            {
                db.Applicants_StripeInvoiceTransactions.Add(new Shared.Applicants_StripeInvoiceTransactions()
                {
                    ActionId = -1,  //email sent for not payed invoice
                    ActionMessage = $"Email sent to {emailInfo.To}",
                    CorrelationId = emailInfo.Id,
                    CreationDate = DateTime.Now,
                    InvoiceId = paymentIntent.Id
                });
            }

            //Send SMS

            try
            {
                string textMessage = $"URGENT: Your account with Senior Planning is past due. Please call 1-800-531-5118 ";
                Shared.Source.TwilioHelper.SendSms(logger, applicantInfo.pointOfContact.phone, textMessage);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                Shared.Source.Mop.NotifyMeOnException("SMS SEND NOT Payed Payment Method", ex, logger, false, "");
            }
        }

        private static bool DetermineIsInvoicePayed(Invoice invoice, InvoiceType billingReason)
        {
            switch (billingReason)
            {
                case InvoiceType.automatic:
                    return (
                                   invoice.Attempted
                                && invoice.AttemptCount >= 1
                                && !invoice.Paid
                                && invoice.AmountRemaining > 0
                           ) ||
                           (
                                   (invoice.Status == "uncollectible" || invoice.Status == "void")
                                && !invoice.Paid
                                && invoice.AmountRemaining > 0
                           );

                case InvoiceType.manual:
                    if (invoice.Status == "uncollectible" || invoice.Status == "void")
                        return false;

                    return !invoice.Paid
                           && (invoice.DueDate ?? DateTime.MaxValue) < DateTime.Now.AddDays(1);

                default: throw new NotImplementedException($"Unsupported billing reason: {invoice.BillingReason}");
            }
        }

        private static ApplicantsNew GetOrCreateApplicantByStripeId(NLog.Logger logger, StringBuilder reportText, string customerId)
        {
            ApplicantsNew applicant = ApplicantsApp.Db.GetApplicantByCustomerStripeId(logger, customerId);
            if (applicant is null)
            {
                //if (customerId.Equals("cus_GA2zWYDPNnQMQj") ||
                //    customerId.Equals("cus_FPcDUjT9iKVSG0") ||
                //    customerId.Equals("cus_FN0PIxS5H9NSeJ"))
                //{
                //    reportText.AppendLine($"   => SKIP, OLD");
                //    return null;
                //}

                //Need to create that client
                Customer customer = Shared.Source.Stripe.GetCustomer(StripeApiKey, customerId);

                Guid applicantId = ApplicantsApp.Db.CreateApplicantFromSource(
                    StripeApiKey,
                    logger,
                    null,
                    customer,
                    out ApplicantsApp.Models.Applicant.Applicant.FullApplicantInfo convertedApplicantData);

                logger.Info($"  done! appId: {applicantId}");

                ApplicantsApp.Db.InsertAction(
                    logger,
                    applicantId,
                    ApplicantsApp.Models.ReportActions.ApplicantConvertFromGravityForm,
                    null,
                    data: JsonConvert.SerializeObject(new
                    {
                        originalData = customer.ToJson(),
                        newData = JsonConvert.SerializeObject(convertedApplicantData)
                    })
                );

                applicant = ApplicantsApp.Db.GetApplicantByCustomerStripeId(logger, customerId);

                reportText.AppendLine($"   => Customer with id {customerId} CREATED, applicantId: {applicantId}!");
            }

            return applicant;
        }

        private static List<Invoice> GetInvoices(NLog.Logger logger)
        {
            try
            {
                List<Invoice> invoices = Shared.Source.Stripe.GetInvoicess(StripeApiKey, 14);
                List<Invoice> unpayedInvoices = Shared.Source.Stripe.GetUnpayedInvoicess(StripeApiKey);

                IEnumerable<string> unpayedInvoicesIds = unpayedInvoices.Select(s => s.Id);
                IEnumerable<Invoice> doubledInvoices = invoices.Where(w => unpayedInvoicesIds.Contains(w.Id));

                invoices = invoices.Except(doubledInvoices).ToList();

                invoices.AddRange(unpayedInvoices);

                return invoices;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                Shared.Source.Mop.NotifyMeOnException("Stripe HandleUnpayedInvoices", ex, logger, false, "GetInvoices");
                throw;
            }
        }

        private static void HandleRefunds(StringBuilder reportText, WebInit webData)
        {
            //Get all refunds
            reportText.AppendLine($"=== Handling refunds ===");
            List<Refund> refunds = Shared.Source.Stripe.GetRefunds(StripeApiKey, 14);

            using (var db = new Shared.DbModel())
            {
                List<Applicants_StripeInvoiceRegister> dbRefundedInvoices = db.Applicants_StripeInvoiceRegister
                    .Where(w => w.InvoiceType == (int)InvoiceType.refund)
                    .ToList();

                foreach (var refund in refunds)
                {
                    reportText.Append($"id: {refund.Id} | status: {refund.Status} | ");

                    if (refund.Status == "succeeded")
                    {
                        Applicants_StripeInvoiceRegister invoice = dbRefundedInvoices.SingleOrDefault(s => s.ChargeId == refund.ChargeId);
                        if (invoice != null)
                        {
                            reportText.AppendLine($" => ALREADY REFUNDED");
                            continue;
                        }

                        Applicants_StripeInvoiceRegister payedInvoice = db.Applicants_StripeInvoiceRegister.SingleOrDefault(s => s.ChargeId == refund.ChargeId && s.InvoiceType != (int)InvoiceType.refund);
                        if (payedInvoice == null)
                        {
                            reportText.AppendLine($" => CAN'T MATCH DB INVOICE");
                            continue;  //invoice not charged
                        }

                        Shared.ApplicantsNew applicant = ApplicantsApp.Db.GetApplicantByCustomerStripeId(webData.logger, payedInvoice.CustomerId);
                        if (applicant == null)
                        {
                            reportText.AppendLine($" => CAN'T MATCH APPLICANT");
                            continue;
                        }

                        Shared.Applicants_CustomersSubscriptions dbSubscription = db.Applicants_CustomersSubscriptions
                                    .Where(w => w.ApplicantId.Equals(applicant.Id))
                                    .Where(w => w.StripePlanId.Equals(payedInvoice.PlanId))
                                    .Where(W => W.SubscriptionId.Equals(payedInvoice.SubscriptionId))
                                    .SingleOrDefault();

                        if (dbSubscription == null)
                        {
                            reportText.AppendLine($" => CAN'T MATCH SUBSCRIPTION FROM DB");
                            continue;
                        }

                        db.Applicants_StripeInvoiceRegister.Add(new Applicants_StripeInvoiceRegister()
                        {
                            AmountDue = 0,
                            AmountPaid = -refund.Amount / 100.0m,
                            AmountRemaining = 0,
                            AttemptCount = 0,
                            Attempted = true,
                            ChargeId = refund.ChargeId,
                            CreationDate = refund.Created,
                            CustomerId = payedInvoice.CustomerId,
                            InvoiceId = refund.Id,
                            InvoiceMessage = string.Empty,
                            InvoiceStatus = 0,
                            InvoiceType = (int)InvoiceType.refund,
                            Paid = true,
                            PaymentId = refund.PaymentIntentId,
                            PlanId = payedInvoice.PlanId,
                            SerializedObject = JsonConvert.SerializeObject(refund),
                            Status = refund.Status,
                            SubscriptionId = payedInvoice.SubscriptionId
                        });

                        dbSubscription.TotalPayedSoFar -= refund.Amount / 100m;

                        db.SaveChanges();

                        reportText.AppendLine($" => PROCESSED, refunded: ${refund.Amount / 100m}");
                    }
                    else
                    {
                        reportText.AppendLine($" => BAD STATUS");
                    }
                }
            }
        }

        private static Shared.Applicants_StripeInvoiceRegister CreateOrUpdateInvoice(
            Invoice invoice,
            Subscription subscription,
            Shared.DbModel db,
            InvoiceType invoiceType)
        {
            var dbInvoice = db.Applicants_StripeInvoiceRegister.SingleOrDefault(s => s.InvoiceId.Equals(invoice.Id));
            if (dbInvoice == null)
            {
                dbInvoice = new Shared.Applicants_StripeInvoiceRegister()
                {
                    AmountDue = invoice.AmountDue / 100m,
                    AmountPaid = invoice.AmountPaid / 100m,
                    AmountRemaining = invoice.AmountRemaining / 100m,
                    AttemptCount = (int)invoice.AttemptCount,
                    Attempted = invoice.Attempted,
                    CreationDate = invoice.Created,
                    CustomerId = invoice.CustomerId,
                    InvoiceId = invoice.Id,
                    Paid = invoice.Paid,
                    PlanId = subscription?.Plan?.Id ?? invoiceType.ToString(),
                    SerializedObject = JsonConvert.SerializeObject(invoice),
                    SubscriptionId = invoice.SubscriptionId ?? invoiceType.ToString(),
                    Status = invoice.Status,
                    InvoiceStatus = (int)Shared.Models.Stripe.InoviceStatuses.New,
                    InvoiceMessage = "",
                    InvoiceType = (int)invoiceType,
                    ChargeId = invoice.ChargeId,
                    PaymentId = invoice.PaymentIntentId,
                };

                db.Applicants_StripeInvoiceRegister.Add(dbInvoice);
            }
            else
            {
                dbInvoice.AmountDue = invoice.AmountDue / 100m;
                dbInvoice.AmountPaid = invoice.AmountPaid / 100m;
                dbInvoice.AmountRemaining = invoice.AmountRemaining / 100m;
                dbInvoice.AttemptCount = (int)invoice.AttemptCount;
                dbInvoice.Attempted = invoice.Attempted;
                dbInvoice.Paid = invoice.Paid;
                dbInvoice.SerializedObject = JsonConvert.SerializeObject(invoice);
                dbInvoice.SubscriptionId = invoice.SubscriptionId ?? invoiceType.ToString();
                dbInvoice.Status = invoice.Status;
                dbInvoice.InvoiceStatus = invoice.Paid ? (int)Shared.Models.Stripe.InoviceStatuses.Payed : (int)Shared.Models.Stripe.InoviceStatuses.NotPayed;
                dbInvoice.PlanId = subscription?.Plan?.Id ?? invoiceType.ToString();
                dbInvoice.InvoiceType = (int)invoiceType;
                dbInvoice.ChargeId = invoice.ChargeId;
                dbInvoice.PaymentId = invoice.PaymentIntentId;
            }

            db.SaveChanges();
            return dbInvoice;
        }

        private static Shared.Applicants_StripeInvoiceRegister CreateOrUpdateInvoiceFromPaymentIntent(
            PaymentIntent pi,
            DbModel db
            )
        {
            var dbInvoice = db.Applicants_StripeInvoiceRegister.SingleOrDefault(s => s.InvoiceId.Equals(pi.Id));
            if (dbInvoice == null)
            {
                if (pi.Charges.Count() > 1) throw new Exception($"More than one charge for PI: {pi.Id}");
                if (pi.Charges.Count() == 0) throw new Exception($"No charge for PI: {pi.Id}");

                dbInvoice = new Shared.Applicants_StripeInvoiceRegister()
                {
                    AmountDue = pi.Amount.Value / 100m,
                    AmountPaid = pi.AmountReceived.Value / 100m,
                    AmountRemaining = (pi.Amount.Value - pi.AmountReceived.Value) / 100m,
                    AttemptCount = 1,
                    Attempted = true,
                    CreationDate = pi.Created.Value,
                    CustomerId = pi.CustomerId,
                    InvoiceId = pi.Id,
                    Paid = pi.Status == "succeeded",
                    PlanId = InvoiceType.paymentIntent.ToString(),
                    SerializedObject = JsonConvert.SerializeObject(pi),
                    SubscriptionId = InvoiceType.paymentIntent.ToString(),
                    Status = pi.Status,
                    InvoiceStatus = (int)Shared.Models.Stripe.InoviceStatuses.New,
                    InvoiceMessage = pi.Description + " " + pi.StatementDescriptor + " " + pi.StatementDescriptorSuffix,
                    InvoiceType = (int)InvoiceType.paymentIntent,
                    ChargeId = pi.Charges.Single().Id,
                    PaymentId = pi.Charges.Single().PaymentIntentId,
                };

                db.Applicants_StripeInvoiceRegister.Add(dbInvoice);
            }
            else
            {
                dbInvoice.AmountDue = pi.Amount.Value / 100m;
                dbInvoice.AmountPaid = pi.AmountReceived.Value / 100m;
                dbInvoice.AmountRemaining = (pi.Amount.Value - pi.AmountReceived.Value) / 100m;
                dbInvoice.AttemptCount = 1;
                dbInvoice.Attempted = true;
                dbInvoice.Paid = pi.Status == "succeeded";
                dbInvoice.SerializedObject = JsonConvert.SerializeObject(pi);
                dbInvoice.SubscriptionId = InvoiceType.paymentIntent.ToString();
                dbInvoice.Status = pi.Status;
                dbInvoice.InvoiceStatus = (int)Shared.Models.Stripe.InoviceStatuses.New;
                dbInvoice.InvoiceMessage = pi.Description + " " + pi.StatementDescriptor + " " + pi.StatementDescriptorSuffix;
                dbInvoice.PlanId = InvoiceType.paymentIntent.ToString();
                dbInvoice.InvoiceType = (int)InvoiceType.paymentIntent;
                dbInvoice.ChargeId = pi.Charges.Single().Id;
                dbInvoice.PaymentId = pi.Charges.Single().PaymentIntentId;
            }

            db.SaveChanges();
            return dbInvoice;
        }



        [HttpPost]
        public IHttpActionResult OnStripeEvent()
        {
            if (Request.Method == HttpMethod.Options)
                return Ok();

            WebInit webData = new WebInit();
            webData.InitMethod(Request, System.Reflection.MethodBase.GetCurrentMethod().Name);

            try
            {
                webData.logger.Info("ParseEvent...");
                Event stripeEvent = EventUtility.ParseEvent(webData.json.ToString());
                webData.logger.Info("res:\n" + JsonConvert.SerializeObject(stripeEvent, Formatting.Indented));

                webData.logger.Info($"stripeEvent.Type: {stripeEvent.Type}");

                // Handle the event
                if (stripeEvent.Type == Events.PaymentIntentSucceeded)
                {
                    PaymentIntent paymentIntent = stripeEvent.Data.Object as PaymentIntent;
                    webData.logger.Info("paymentIntent:\n" + JsonConvert.SerializeObject(paymentIntent, Formatting.Indented));
                }
                else if (stripeEvent.Type == Events.PaymentMethodAttached)
                {
                    PaymentMethod paymentMethod = stripeEvent.Data.Object as PaymentMethod;
                    webData.logger.Info("paymentMethod:\n" + JsonConvert.SerializeObject(paymentMethod, Formatting.Indented));
                }
                else if (stripeEvent.Type == Events.InvoicePaymentSucceeded)
                {
                    Invoice invoice = stripeEvent.Data.Object as Invoice;
                    webData.logger.Info("invoice:\n" + JsonConvert.SerializeObject(invoice, Formatting.Indented));

                    if (!invoice.Paid) throw new Exception("Invoice PAYED is false, which is not expected because type is 'InvoicePaymentSucceeded'");
                    if (invoice.Currency != "usd") throw new Exception($"Invoice currency is unexpected: {invoice.Currency}");
                    if (invoice.Status != "paid") throw new Exception($"Invoice status is unexpected: {invoice.Status}"); //draft, open, paid, uncollectible, or void

                    string invoiceId = invoice.Id;
                    decimal amountPaid = invoice.AmountPaid / 100m;
                    string collectionMethod = invoice.CollectionMethod; //charge_automatically or send_invoice
                    string customerId = invoice.CustomerId;
                    string hostedInvoiceUrl = invoice.HostedInvoiceUrl;
                    string invoicePdf = invoice.InvoicePdf;
                    string subsctiptionId = invoice.SubscriptionId;

                    webData.logger.Info(JsonConvert.SerializeObject(new
                    {
                        invoiceId,
                        amountPaid,
                        collectionMethod,
                        customerId,
                        hostedInvoiceUrl,
                        invoicePdf,
                        subsctiptionId
                    }, Formatting.Indented));
                }
                else
                {
                    webData.logger.Info("Some other event we don't care");
                }
            }
            catch (StripeException e)
            {
                webData.logger.Error(e);
                Shared.Source.Mop.NotifyMeOnException("Stripe hook", e, webData.logger, false, webData?.json?.ToString());
                return BadRequest();
            }
            catch (Exception ex)
            {
                webData.logger.Error(ex);
                Shared.Source.Mop.NotifyMeOnException("Stripe hook", ex, webData.logger, false, webData?.json?.ToString());
                return BadRequest();
            }

            return Ok();
        }
    }
}
