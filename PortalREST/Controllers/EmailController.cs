﻿using ApplicantsApp;
using Flurl;
using Newtonsoft.Json;
using Shared.Source.MailGeneration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PortalREST.Controllers
{
    public class EmailController : ApiController
    {
        private NLog.Logger logger = NLog.LogManager.GetLogger("mail-scheduler");

        [HttpGet, HttpOptions]
        [EnableCors("*", "*", "*")]
        public HttpResponseMessage SendScheduledEmails()
        {
            try
            {
                var scheduledEmails = ApplicantsApp.Db.GetAllScheduledEmails(logger);
                logger.Info($"Count: {scheduledEmails.Count}");

                foreach (var emailInfo in scheduledEmails)
                {
                    switch ((ApplicantsApp.Models.Communication.EmailTypes)emailInfo.EmailTypeId)
                    {
                        //call carring
                        case ApplicantsApp.Models.Communication.EmailTypes.CaringComAPICall:
                            try
                            {                                
                                Helper.CallCaring(logger, emailInfo.Body);

                                emailInfo.Result = (int)ApplicantsApp.Models.Communication.EmailStatusCodes.Success;                                
                            }
                            catch (Exception ex)
                            {
                                emailInfo.Result = (int)ApplicantsApp.Models.Communication.EmailStatusCodes.Error;
                                emailInfo.ErrorMessage = ex.Message;
                            }

                            ApplicantsApp.Db.UpdateStatusAndCorrelationIdEmailDetails(logger, emailInfo);

                            break;

                        //regular email, this is the default, yeah
                        default:
                            //Prepare data
                            ApplicantsApp.Db.GetApplicantData(
                                emailInfo.ApplicantId,
                                out Shared.ApplicantsNew applicant,
                                out ApplicantsApp.Models.Applicant.Applicant.FullApplicantInfo applicantInfo);

                            var representative = Db.LoadRepresentative(applicant.RepresentativeId.Value, logger);

                            //Send the Email
                            List<Shared.ApplicantsActionsEmailDetails> sentEmails = SendTheEmail(emailInfo, applicant, representative);

                            if (sentEmails is null)
                            {
                                emailInfo.Result = (int)ApplicantsApp.Models.Communication.EmailStatusCodes.Scheduled_ConditionsNotMeetToSend;
                            }
                            else
                            {
                                emailInfo.Result = (int)ApplicantsApp.Models.Communication.EmailStatusCodes.Scheduled_AlreadySent;
                                emailInfo.CorrelationId = sentEmails.First().Id;
                            }

                            Db.UpdateStatusAndCorrelationIdEmailDetails(logger, emailInfo);

                            //Schedule next email
                            MailGenerator.ScheduleEMail(
                                logger,
                                new Guid("00000000-0000-0000-0000-000000000002"),
                                emailInfo.ApplicantId,
                                applicantInfo,
                                (ApplicantsApp.Models.Communication.EmailTypes)emailInfo.EmailTypeId,
                                DateTime.Now);

                            break;
                    }

                }

            }
            catch (Exception ex)
            {
                logger.Error(ex);
                Shared.Source.Mop.NotifyMeOnException("Email Scheduler", ex, logger, false, "");
            }

            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        private List<Shared.ApplicantsActionsEmailDetails> SendTheEmail(
            Shared.ApplicantsActionsEmailDetails emailInfo,
            Shared.ApplicantsNew applicant,
            Shared.ApplicantsRepresentatives representative
            )
        {
            //Send the actual email, which also may require generating the email body
            BodyGenerators.EmailDataResponse emailData;

            bool convertNewRowsToBr = true;
            ApplicantsApp.Models.Applicant.Applicant.FullApplicantInfo applicantModel = JsonConvert.DeserializeObject<ApplicantsApp.Models.Applicant.Applicant.FullApplicantInfo>(applicant.Data);

            switch ((ApplicantsApp.Models.Communication.EmailTypes)emailInfo.EmailTypeId)
            {
                case ApplicantsApp.Models.Communication.EmailTypes.AutoRFI:
                    {
                        emailData = new BodyGenerators.EmailDataResponse()
                        {
                            subject = "Outstanding Documents Needed",
                            attachedDocuments = null,
                            emailType = ApplicantsApp.Models.Communication.EmailTypes.AutoRFI
                        };

                        List<Shared.ApplicantsDocuments> applicantDocuments = ApplicantsApp.Db.GetApplicantDocuments(applicant.Id, logger, ApplicantsApp.Models.Documents.DocumentStatus.active);
                        List<Shared.Applicants_DocumentCategories> documentCategories = ApplicantsApp.Db.GetAllDocumentCategories(logger);
                        List<ApplicantsApp.Models.Documents.CustomCategoryDescription> customCategoryDescriptions = null;

                        if (applicant.CustomCategoryDescriptions != null)
                        {
                            customCategoryDescriptions = JsonConvert.DeserializeObject<List<ApplicantsApp.Models.Documents.CustomCategoryDescription>>(applicant.CustomCategoryDescriptions);
                        }

                        emailData.emailBody = BodyGenerators.GenerateRFIBody(applicantDocuments, documentCategories, customCategoryDescriptions, applicant, out int requestedDocumentsCount);
                        if (requestedDocumentsCount <= 0)
                        {
                            return null;
                        }
                    }

                    break;

                case ApplicantsApp.Models.Communication.EmailTypes.LiteRFI:
                    {
                        convertNewRowsToBr = false;
                        emailData = new BodyGenerators.EmailDataResponse()
                        {
                            subject = "List reminder - Outstanding Documents Needed",
                            attachedDocuments = null,
                            emailType = ApplicantsApp.Models.Communication.EmailTypes.LiteRFI
                        };

                        List<Shared.ApplicantsDocuments> applicantDocuments = Db.GetApplicantDocuments(applicant.Id, logger, ApplicantsApp.Models.Documents.DocumentStatus.active);
                        List<Shared.Applicants_DocumentCategories> documentCategories = Db.GetAllDocumentCategories(logger);
                        List<ApplicantsApp.Models.Documents.CustomCategoryDescription> customCategoryDescriptions = null;

                        if (applicant.CustomCategoryDescriptions != null)
                        {
                            customCategoryDescriptions = JsonConvert.DeserializeObject<List<ApplicantsApp.Models.Documents.CustomCategoryDescription>>(applicant.CustomCategoryDescriptions);
                        }

                        string tableHtml = BodyGenerators.Generate__LiteRFI__HTML(applicantDocuments, documentCategories, customCategoryDescriptions, applicant, out int missingDocuments);
                        if (missingDocuments <= 0)
                        {
                            //if it's paused and still the count of the required documents is NONE - do not sent emails
                            if (applicantModel.autoLiteRFIEmailSettings?.pausedUntilMoreDocumentsAreRequired == true)
                            {
                                return null;
                            }
                            else
                            {
                                //if it's false, and no success is sent so far - sent one last email
                                applicantModel.autoLiteRFIEmailSettings.pausedUntilMoreDocumentsAreRequired = true;
                                Db.UpdateApplicantData(applicant.Id, JsonConvert.SerializeObject(applicantModel), NLog.LogManager.CreateNullLogger());
                            }
                        }
                        else
                        {
                            //if are paused, and now have more documents - set to false, sent the document
                            if (applicantModel.autoLiteRFIEmailSettings?.pausedUntilMoreDocumentsAreRequired == true)
                            {
                                applicantModel.autoLiteRFIEmailSettings.pausedUntilMoreDocumentsAreRequired = false;                                
                                Db.UpdateApplicantData(applicant.Id, JsonConvert.SerializeObject(applicantModel), logger);
                            }
                        }

                        var representativeData = JsonConvert.DeserializeObject<ApplicantsApp.Models.Representative.RepresentativeInfo>(representative.Data);

                        string mainText = missingDocuments > 0 ?
                            "We are currently waiting on the following documents before we can proceed." :
                            "Your request has been completed. Please watch your email for additional future requests.";

                        string htmlTemplate = System.IO.File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath(@"~/Documents/lite-rfi-body.html"));
                        htmlTemplate = htmlTemplate.Replace("{{TABLE_BODY}}", tableHtml);
                        htmlTemplate = htmlTemplate.Replace("{FIN_REP_NAME}", representative.RepresentativeName);
                        htmlTemplate = htmlTemplate.Replace("{FIN_REP_PHPNE}", representativeData.phoneNumber);
                        htmlTemplate = htmlTemplate.Replace("{{MAIN_TEXT}}", mainText);

                        emailData.emailBody = htmlTemplate;
                    }

                    break;

                case ApplicantsApp.Models.Communication.EmailTypes.AutoInterim:
                    throw new NotImplementedException();

                case ApplicantsApp.Models.Communication.EmailTypes.PasAssssorAutoRequest:
                    emailData = BodyGenerators.GeneratePASInfoRequestData(new BodyGenerators.EmailDataRequest()
                    {
                        applicantModel = applicantModel
                    });
                    break;

                default:
                    throw new Exception($"Odd email type: {emailInfo.EmailTypeId}");
            }

            List<ApplicantsApp.Models.DTOs.AzureToLocalFileDTO> files = null;
            if (emailData.attachedDocuments != null && emailData.attachedDocuments.Count > 0)
            {
                //files = GetAzureBundle(attachedDocuments);
                throw new NotImplementedException();
            }

            ApplicantsApp.Models.ReportActions action = Shared.Source.Mop.GetActionFromEmailType(emailData.emailType);
            Guid correlationId = Guid.NewGuid();

            string info = string.Empty;
            string errors = string.Empty;

            try
            {
                Shared.Source.Mop.NotifyMe("eMail Scheduler", emailData.emailBody, logger, false);
            }
            catch (Exception ex)
            {

            }

            return MailGenerator.SendEmail(
                    action,
                    correlationId,
                    ref info,
                    ref errors,
                    emailData.attachedDocuments?.Select(s => s.Id)?.ToList(),
                    files,
                    new string[] { emailInfo.To },
                    emailData.emailBody,
                    emailData.subject,
                    logger,
                    applicant.Id,
                    1,
                    representative,
                    System.IO.File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath(@"~/Documents/email_footer_rep.html")),
                    new Guid("00000000-0000-0000-0000-000000000002"),
                    null,
                    convertNewRowsToBr
                    );


        }
    }
}
