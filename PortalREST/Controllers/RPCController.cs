﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Shared;

namespace PortalREST.Controllers
{
    public class RPCController : ApiController
    {
        [HttpGet, HttpPost, HttpOptions]
        [EnableCors("*", "*", "*")]
        public HttpResponseMessage RefaxDe101()
        {
            if (Request.Method == HttpMethod.Options)
                return new HttpResponseMessage(HttpStatusCode.OK);

            var webData = new WebInit();
            webData.InitMethod(Request, System.Reflection.MethodBase.GetCurrentMethod().Name);

            try
            {
                List<ApplicantsNew> applicantsToRefaxDe101 = GetApplicantsWithDe101StatusMoreThanXDays(webData);

                foreach (var applicant in applicantsToRefaxDe101)
                {
                    webData.logger.Info("== applicantId: " + applicant.Id);

                    ApplicantsDocuments lastDe101 = GetLastDe101Document(applicant);
                    webData.logger.Info("   lastDe101: " + lastDe101?.Id);
                    if (lastDe101 == null) continue;

                    try
                    {
                        string localFileName = System.Web.Hosting.HostingEnvironment.MapPath($@"~/Documents/{lastDe101.DocumentName}");
                        webData.logger.Info("   localFileName: " + localFileName);
                        Shared.Source.Azure.StorageManager.DownloadFileFromAzure(applicant.Id, lastDe101.Id.ToString(), localFileName);
                        try
                        {
                            string blobFileName = Guid.NewGuid().ToString() + ".pdf";
                            webData.logger.Info("   blobFileName: " + blobFileName);
                            //upload localFile to azure public container
                            Uri publicAddress = Shared.Source.Azure.StorageManager.UploadFile(
                                localFilePath: localFileName,
                                containerName: "fax-files",
                                remoteFileName: blobFileName,
                                logger: webData.logger,
                                applicantId: applicant.Id
                                );
                            webData.logger.Info("   publicAddress: " + publicAddress);


                            string recipient = "+18885073313";
                            webData.logger.Info("   recipient: " + recipient);

                            Twilio.Rest.Fax.V1.FaxResource faxInfo = null;

                            string resp = Shared.Source.TwilioHelper.SendFax(
                                to: recipient,
                                uri: publicAddress.ToString(),
                                logger: webData.logger,
                                info: ref faxInfo
                                );
                            webData.logger.Info("   resp: " + resp);

                            if (resp != null)
                            {
                                throw new Exception(resp);
                            }

                            UpdateApplicantLastStatusChangeDate(applicant);
                            webData.logger.Info("   ALL OK!");
                            webData.logger.Info("   ");
                            Shared.Source.Mop.NotifyMe(
                                $"Re-faxing de-101 OK, applicantId: {applicant.Id}, lastDe101: {lastDe101.Id}", 
                                "", 
                                webData.logger, 
                                false);
                        }
                        finally
                        {
                            System.Threading.Thread.Sleep(250);
                            System.IO.File.Delete(localFileName);
                        }
                    }
                    catch (Exception ex)
                    {
                        webData.logger.Error(ex);
                        Shared.Source.Mop.NotifyMeOnException($"Re-faxing de-101 failed, applicantId: {applicant.Id}, lastDe101: {lastDe101.Id}", ex, webData.logger, false, webData?.json?.ToString());
                    }
                }

                return Helper.Resp(new
                {

                });

            }
            catch (Exception ex)
            {
                webData.logger.Error(ex);
                Shared.Source.Mop.NotifyMeOnException("Refaxing de-101 failed", ex, webData.logger, false, webData?.json?.ToString());
                return Helper.HandleError(webData.logger, ex, HttpStatusCode.NotAcceptable);
            }
        }

        private static void UpdateApplicantLastStatusChangeDate(ApplicantsNew applicant)
        {
            using (var db = new DbModel())
            {
                db.ApplicantsNew.Single(s => s.Id.Equals(applicant.Id)).LastStatusChanged = DateTime.Now;
                db.SaveChanges();
            }
        }

        private static ApplicantsDocuments GetLastDe101Document(ApplicantsNew applicant)
        {
            ApplicantsDocuments lastde101;
            using (var db = new DbModel())
            {
                lastde101 = db.ApplicantsDocuments
                        .Where(w => w.ApplicantId == applicant.Id)
                        .Where(w => w.DocumentType == "de101")
                        .OrderByDescending(o => o.Version)
                        .FirstOrDefault();
            }

            return lastde101;
        }

        private static List<ApplicantsNew> GetApplicantsWithDe101StatusMoreThanXDays(WebInit webData)
        {
            List<ApplicantsNew> applicantsToRefaxDe101;
            using (var db = new DbModel())
            {
                db.Database.Log = s => webData.logger.Info(s);

                //8	DE 101 SUBMITTED
                DateTime lastChangedMax = DateTime.Now.AddDays(-21);

                applicantsToRefaxDe101 = db.ApplicantsNew
                    .Where(w => w.Status == 8)
                    .Where(w => w.LastStatusChanged <= lastChangedMax)
                    .Take(1)
                    .ToList();
            }

            return applicantsToRefaxDe101;
        }
    }
}
