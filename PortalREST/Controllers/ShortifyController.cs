﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PortalREST.Controllers
{
    public class ShortifyController : ApiController
    {
        //http://jubotho-001-site1.itempurl.com/api/api/any/
        //http://jubotho-001-site1.itempurl.com/api/api/Shortify/WhatsThis?sufix=g3me711g

        [HttpGet]
        [EnableCors("*", "*", "*")]
        [Route("{sufix}")]
        public IHttpActionResult WhatsThis(string sufix)
        {            
            WebInit webData = new WebInit();
            webData.InitMethod(Request, System.Reflection.MethodBase.GetCurrentMethod().Name);

            try
            {                
                using (var db = new PortalEntities())
                {                    
                    return Redirect(db.ShortUrls.Single(s => s.UrlSufix == sufix).TargetUrl);                    
                }

            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [HttpGet, HttpPost, HttpOptions]
        [EnableCors("*", "*", "*")]
        public HttpResponseMessage This()
        {
            if (Request.Method == HttpMethod.Options)
                return new HttpResponseMessage(HttpStatusCode.OK);

            WebInit webData = new WebInit();
            webData.InitMethod(Request, System.Reflection.MethodBase.GetCurrentMethod().Name);

            string pass = webData.json["pass"].ToString();
            webData.logger.Info($" pass: {pass}");

            string targetUrl = webData.json["targetUrl"].ToString();
            webData.logger.Info($" targetUrl: {targetUrl}");

            try
            {
                if (pass != "UwXD8YXpT2He9fUt9vFydZd8")
                    throw new Exception("The sessionId is invalid");

                if (!(Uri.TryCreate(targetUrl, UriKind.Absolute, out Uri uriResult)
                    && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps)))
                {
                    throw new Exception($"'{targetUrl}' is not valid URL");
                }

                var random = new Random();
                int retries = 15;
                string sufix = "ERROR";

                while (true)
                {
                    try
                    {
                        var stringChars = new char[8];

                        for (int i = 0; i < stringChars.Length; i++)
                        {
                            stringChars[i] = chars[random.Next(chars.Length)];
                        }

                        sufix = new string(stringChars);

                        using (var db = new PortalEntities())
                        {
                            db.ShortUrls.Add(new ShortUrls()
                            {
                                TargetUrl = targetUrl,
                                UrlSufix = sufix
                            });

                            db.SaveChanges();

                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        webData.logger.Error(ex);

                        if (retries-- <= 0)
                        {
                            throw new Exception("Can't shorten, error: " + ex.Message);
                        }
                    }
                }

                return Helper.Resp(new
                {
                    sufix
                });

            }
            catch (Exception ex)
            {
                return Helper.HandleError(null, ex, HttpStatusCode.NotAcceptable);
            }
        }

        private const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";

    }
}
