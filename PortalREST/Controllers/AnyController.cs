﻿using ApplicantsApp.Models.Applicant.Applicant;
using Flurl.Http;
using Nager.Date.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PortalREST.Controllers
{
    public class AnyController : ApiController
    {
        //http://localhost:50359/api/Any/ShowSomething?sessionId=ff
        //http://jubotho-001-site1.itempurl.com/api/api/any/ShowSomething?sessionId=asd
        [HttpGet, HttpPost, HttpOptions]
        [EnableCors("*", "*", "*")]
        public HttpResponseMessage ShowSomething(string sessionId)
        {
            if (Request.Method == HttpMethod.Options)
                return new HttpResponseMessage(HttpStatusCode.OK);

            WebInit webData = new WebInit();
            webData.InitMethod(Request, System.Reflection.MethodBase.GetCurrentMethod().Name, true);

            try
            {
                using (var db = new PortalEntities())
                {
                    return Helper.Resp(new
                    {
                        sessionId = sessionId,
                        facility = db.CareHomes.First()
                    });
                }
            }
            catch (Exception ex)
            {
                return Helper.HandleError(null, ex, HttpStatusCode.NotAcceptable);
            }
        }

        const string const_YesNoQuestion =
@"<tr>
    <td style='border: 1px solid black;padding: 10px;'><label>{QUESTION_TEXT}</label></td>
    <td style='border: 1px solid black;padding: 10px;'><input question-type='{QUESTION_TYPE}' id='{ELEMENT_ID}' type='checkbox' data-toggle='toggle' data-on='Yes' data-off='No'></td>
</tr>";

        const string const_FreeTextAnswer =
@"<tr>
    <td style='border: 1px solid black;padding: 10px;' colspan='2'>
        <label>{QUESTION_TEXT}</label><br />
        <textarea placeholder='Please write your answer here...' question-type='{QUESTION_TYPE}' id='{ELEMENT_ID}' style='width:100%' rows='3'></textarea>
    </td>
</tr>";
        [HttpGet, HttpPost, HttpOptions]
        [EnableCors("*", "*", "*")]
        public HttpResponseMessage LoadQuestionCampaign()
        {
            if (Request.Method == HttpMethod.Options)
                return new HttpResponseMessage(HttpStatusCode.OK);

            var webData = new WebInit();
            webData.InitMethod(Request, System.Reflection.MethodBase.GetCurrentMethod().Name);

            try
            {
                Guid eventId = Guid.Parse(webData.json["eventId"].ToString());

                using (var db = new PortalEntities())
                {
                    var invitaion = db.InterestedPatients.SingleOrDefault(s => s.Id == eventId);
                    if (invitaion == null) throw new Exception("This question campaign can't be found, sorry for that.");

                    if (invitaion.HaveInterest == 1) throw new Exception("Thank you, you already participate in this question campaign!");

                    var questionCampaign = db.QuestionCampaigns.SingleOrDefault(s => s.Id == invitaion.PatientId);
                    if (questionCampaign == null) throw new Exception("Sorry, something went wrong, this question campaign can't be found, sorry for that.");

                    var facilityInfo = db.CareHomes.SingleOrDefault(s => s.Id == invitaion.FacilityId);
                    if (facilityInfo == null) throw new Exception("Sorry, something went wrong, your facility can't be found, sorry for that.");

                    var questions = db.QuestionCampaignQuestions.Where(w => w.QuestionCampaignId == questionCampaign.Id)
                        .Select(s => new { s.Id, s.Order, s.QuestionText, s.QuestionTypeId })
                        .OrderBy(o => o.Order)
                        .ToList();

                    var table = new StringBuilder("<table class=\"col-sm-12 col-xs-12 col-md-9 col-lg-6\">");
                    foreach (var q in questions)
                    {
                        table.AppendLine("<tr>");
                        string row = string.Empty;
                        switch (q.QuestionTypeId)
                        {
                            case 0:
                                row = const_YesNoQuestion;
                                break;

                            case 1:
                                row = const_FreeTextAnswer;
                                break;
                        }

                        table.AppendLine(row
                                        .Replace("{QUESTION_TEXT}", q.QuestionText)
                                        .Replace("{ELEMENT_ID}", q.Id.ToString())
                                        .Replace("{QUESTION_TYPE}", q.QuestionTypeId.ToString()));

                        table.AppendLine("</tr>");
                    }
                    table.AppendLine(@"</table>");

                    return Helper.Resp(new
                    {
                        campaignName = questionCampaign.QuestionCampaignName,
                        //questions,
                        table = table.ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                return Helper.HandleError(webData.logger, ex, HttpStatusCode.NotAcceptable);
            }
        }

        [HttpGet, HttpPost, HttpOptions]
        [EnableCors("*", "*", "*")]
        public HttpResponseMessage YapIAmInterested()
        {
            if (Request.Method == HttpMethod.Options)
                return new HttpResponseMessage(HttpStatusCode.OK);

            WebInit webData = new WebInit();
            webData.InitMethod(Request, System.Reflection.MethodBase.GetCurrentMethod().Name);

            try
            {
                Guid eventId = Guid.Parse(webData.json["eventId"].ToString());

                using (var db = new PortalEntities())
                {
                    var invitaion = db.InterestedPatients.SingleOrDefault(s => s.Id == eventId);
                    if (invitaion == null) throw new Exception("This invitation can't be found, sorry for that.");

                    var patientInfo = db.Patients.SingleOrDefault(s => s.Id == invitaion.PatientId);
                    if (patientInfo == null) throw new Exception("Sorry, something went wrong, this patient can't be found");

                    var facilityInfo = db.CareHomes.SingleOrDefault(s => s.Id == invitaion.FacilityId);
                    if (facilityInfo == null) throw new Exception("Sorry, something went wrong, this facility can't be found");

                    if (invitaion.HaveInterest == 1) throw new Exception("Thank you, you already said you have interest for that patient, we will contact you as soon as possible!");

                    invitaion.HaveInterest = 1;
                    invitaion.InterestDateTime = DateTime.Now;

                    db.SaveChanges();

                    //try match other care homes with same number for the same patient
                    try
                    {
                        string phone = db.CareHomeContacts.FirstOrDefault(f => f.FacilityId == facilityInfo.Id)?.Phone;

                        if (!String.IsNullOrWhiteSpace(phone))
                        {

                            var otherFacilitiesWithSamePhone = (from ip in db.InterestedPatients
                                                                join c in db.CareHomeContacts on ip.FacilityId equals c.FacilityId
                                                                where c.Phone == phone && ip.PatientId == patientInfo.Id && ip.HaveInterest != 1
                                                                select ip
                             ).ToList();

                            foreach (var fac in otherFacilitiesWithSamePhone)
                            {
                                fac.HaveInterest = 1;
                                fac.InterestDateTime = DateTime.Now;
                            }

                            db.SaveChanges();
                        }

                    }
                    catch (Exception ex)
                    {
                        webData.logger.Error(ex);
                    }

                    return Helper.Resp(new { note = "Thank you, we will contact you as soon as possible!" });
                }
            }
            catch (Exception ex)
            {
                return Helper.HandleError(webData.logger, ex, HttpStatusCode.NotAcceptable);
            }
        }

        [HttpGet, HttpPost, HttpOptions]
        [EnableCors("*", "*", "*")]
        public HttpResponseMessage SubmitAnswers()
        {
            if (Request.Method == HttpMethod.Options)
                return new HttpResponseMessage(HttpStatusCode.OK);

            WebInit webData = new WebInit();
            webData.InitMethod(Request, System.Reflection.MethodBase.GetCurrentMethod().Name);

            CareHomes facilityInfo;
            QuestionCampaigns questionCampaign;
            try
            {
                Guid eventId = Guid.Parse(webData.json["eventId"].ToString());
                using (var db = new PortalEntities())
                {
                    var invitaion = db.InterestedPatients.SingleOrDefault(s => s.Id == eventId);
                    if (invitaion == null) throw new Exception("This question campaign can't be found, sorry for that.");

                    if (invitaion.HaveInterest == 1) throw new Exception("Thank you, you already participate in this question campaign!");

                    questionCampaign = db.QuestionCampaigns.SingleOrDefault(s => s.Id == invitaion.PatientId);
                    if (questionCampaign == null) throw new Exception("Sorry, something went wrong, this question campaign can't be found, sorry for that.");

                    facilityInfo = db.CareHomes.SingleOrDefault(s => s.Id == invitaion.FacilityId);
                    if (facilityInfo == null) throw new Exception("Sorry, something went wrong, your facility can't be found, sorry for that.");

                    var questions = db.QuestionCampaignQuestions.Where(w => w.QuestionCampaignId == questionCampaign.Id)
                        .ToList();

                    foreach (var answer in webData.json["answers"])
                    {
                        Guid questionId = Guid.Parse(((Newtonsoft.Json.Linq.JProperty)answer).Name);
                        QuestionCampaignQuestions questionFromCampaign = questions.Single(a => a.Id == questionId);

                        string answerText = ((Newtonsoft.Json.Linq.JProperty)answer).Value.ToString();
                        db.QuestionCampaignAnswers.Add(new QuestionCampaignAnswers()
                        {
                            Id = Guid.NewGuid(),
                            FacilityId = facilityInfo.Id,
                            QuestionId = questionId,
                            QuestionTypeId = questionFromCampaign.QuestionTypeId,
                            AnswerBool = questionFromCampaign.QuestionTypeId == 0 ? (bool?)(bool.Parse(answerText)) : null,
                            AnswerText = questionFromCampaign.QuestionTypeId == 1 ? answerText : null
                        });
                    }

                    invitaion.HaveInterest = 1;
                    invitaion.InterestDateTime = DateTime.Now;

                    webData.logger.Info("Before saving...");
                    db.SaveChanges();
                    webData.logger.Info("After saving");
                }

                webData.logger.Info("  Find other facilities...");
                //find other facilities
                using (var db = new PortalEntities())
                {
                    try
                    {
                        webData.logger.Info("Searching for phone...");
                        string phone = db.CareHomeContacts.FirstOrDefault(f => f.FacilityId == facilityInfo.Id)?.Phone;
                        webData.logger.Info($"  phone: {phone}");

                        if (!String.IsNullOrWhiteSpace(phone))
                        {
                            webData.logger.Info($"  not null, get other facilities with the same phone");

                            var otherFacilitiesWithSamePhone = (from ip in db.InterestedPatients
                                                                join c in db.CareHomeContacts on ip.FacilityId equals c.FacilityId
                                                                where c.Phone == phone && ip.PatientId == questionCampaign.Id && ip.HaveInterest != 1
                                                                select ip
                             ).ToList();

                            webData.logger.Info($"detected count: {otherFacilitiesWithSamePhone}");

                            foreach (var fac in otherFacilitiesWithSamePhone)
                            {
                                fac.HaveInterest = 1;
                                fac.InterestDateTime = DateTime.Now;
                            }

                            webData.logger.Info("Before saving...");
                            db.SaveChanges();
                            webData.logger.Info("After saving");
                        }
                    }
                    catch (Exception ex)
                    {
                        webData.logger.Error(ex);
                    }
                }

                return Helper.Resp(new { note = "Thank you, your answers are stored!" });
            }
            catch (Exception ex)
            {
                return Helper.HandleError(webData.logger, ex, HttpStatusCode.InternalServerError);
            }
        }

        //http://localhost/html5/patients-portal/patient-info.html?pid=86e01e5f-a271-4764-bd2b-ea7a374183ee&fid=99CAE638-FDC8-48EA-9D96-7688A24AD915
        [HttpGet, HttpPost, HttpOptions]
        [EnableCors("*", "*", "*")]
        public HttpResponseMessage GetPatientPlusFacilityInfo()
        {
            if (Request.Method == HttpMethod.Options)
                return new HttpResponseMessage(HttpStatusCode.OK);

            WebInit webData = new WebInit();
            webData.InitMethod(Request, System.Reflection.MethodBase.GetCurrentMethod().Name);

            try
            {
                Guid eventId = Guid.Parse(webData.json["eventId"].ToString());

                using (var db = new PortalEntities())
                {
                    //pid 86e01e5f-a271-4764-bd2b-ea7a374183ee

                    var invitaion = db.InterestedPatients.SingleOrDefault(s => s.Id == eventId);
                    if (invitaion == null) throw new Exception("This invitation can't be found, sorry for that.");

                    var patientInfo = db.Patients.SingleOrDefault(s => s.Id == invitaion.PatientId);
                    var facilityInfo = db.CareHomes.SingleOrDefault(s => s.Id == invitaion.FacilityId);

                    //if (patientInfo.SemiPrivateRoomBudget == null)
                    //{
                    //    semiPrivateRoomBudget = "N/A";
                    //} else
                    //{
                    //    semiPrivateRoomBudget = "$" + patientInfo.SemiPrivateRoomBudget.ToString();
                    //}

                    //string privateBudget;
                    //if (patientInfo.PrivateRoomBudget == null)
                    //{
                    //    privateBudget = "N/A";
                    //}
                    //else
                    //{
                    //    privateBudget = "$" + patientInfo.PrivateRoomBudget.ToString();
                    //}

                    string patientBudget;
                    if (patientInfo.BudgetForCareHomes == null)
                    {
                        patientBudget = "N/A";
                    }
                    else
                    {
                        patientBudget = "$" + patientInfo.BudgetForCareHomes.ToString();
                    }

                    return Helper.Resp(new
                    {
                        flName = patientInfo.FirstName + " " + patientInfo.LastName,
                        patientBudget = patientBudget,
                        customMessage = patientInfo.Notes,
                        age = patientInfo.Age,
                        poc = patientInfo.PocFirstName + " " + patientInfo.PocLastName,
                        facilityName = facilityInfo.FacilityName
                    });
                }
            }
            catch (Exception ex)
            {
                return Helper.HandleError(webData.logger, ex, HttpStatusCode.NotAcceptable);
            }
        }


        [HttpPost, HttpOptions]
        [EnableCors("*", "*", "*")]
        public HttpResponseMessage Insert_FormID_7()
        {
            if (Request.Method == HttpMethod.Options)
                return new HttpResponseMessage(HttpStatusCode.OK);

            WebInit webData = new WebInit();
            webData.InitMethod(Request, System.Reflection.MethodBase.GetCurrentMethod().Name);

            try
            {
                if (Request.Headers.SingleOrDefault(s => s.Key == "Pass").Value?.FirstOrDefault() != "Zkha8C36ss6Bf7B5t9ws95ng99xJlL2iE670P5Sz")
                {
                    throw new Exception("Pass not provided or does not match!");
                }

                using (var db = new Shared.DbModel())
                {
                    webData.logger.Info("Start reading...");
                    Shared.Models.Form_Id7.ImportData data = new Shared.Models.Form_Id7.ImportData()
                    {
                        Email = webData.json["Email"]?.ToString(),
                        Entry = webData.json["Entry"]?.ToString(),

                        EntryDate = webData.json["Entry Date"]?.ToString(),
                        EntryId = webData.json["Entry ID"]?.ToString(),
                        FirstName = webData.json["First Name"]?.ToString(),
                        FormTitle = webData.json["Form Title"]?.ToString(),

                        InterestedInVeteranBenefits = webData.json["Veteran Benefits"]?.ToString()?.Equals("Veteran Benefits") ?? false,
                        InterestedVeteranSpouseOfVeteran = webData.json["Spouse of Veteran"]?.ToString()?.Equals("Spouse of Veteran") ?? false,
                        LastName = webData.json["Last Name"]?.ToString(),
                        Phone = webData.json["Phone"]?.ToString(),

                        Referer = webData.json["Referer"]?.ToString(),
                        SourceUrl = webData.json["Source Url"]?.ToString(),
                        TypeofCare = webData.json["Type of Care"]?.ToString(),
                        UserIP = webData.json["User IP"]?.ToString(),
                        ZipCode = webData.json["Zip Code"]?.ToString(),
                    };
                    webData.logger.Info("Done! Here is the object:" + JsonConvert.SerializeObject(data, Formatting.Indented));

                    var caringData = new
                    {
                        email_address = data.Email,
                        browser = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134",
                        care_location = data.ZipCode,
                        first_name = data.FirstName,
                        last_name = data.LastName,
                        phone_number = data.Phone,
                        type_of_care = data.TypeofCare,
                        ip_address = data.UserIP
                    };
                    webData.logger.Info("Caring data generated object: " + JsonConvert.SerializeObject(caringData, Formatting.Indented));

                    var apiCallDetails = new Shared.ApplicantsActionsEmailDetails()
                    {
                        ApplicantId = new Guid("00000000-0000-0000-0000-000000000002"),
                        Body = JsonConvert.SerializeObject(caringData),
                        CorrelationId = Guid.NewGuid(),
                        Created = DateTime.Now,
                        EmailTypeId = (int)ApplicantsApp.Models.Communication.EmailTypes.CaringComAPICall,
                        ErrorMessage = null,
                        Id = Guid.NewGuid(),
                        OtherFiles = null,
                        Subject = "caring.com",
                        To = "https://dir.caring.com/api/v2/inquiries.json?pid=da5a7a7cd60c",
                        UserId = new Guid("00000000-0000-0000-0000-000000000002")
                    };

                    //schedule or not
                    if (data.InterestedVeteranSpouseOfVeteran || data.InterestedInVeteranBenefits)
                    {
                        //schedule the api call
                        apiCallDetails.Result = (int)ApplicantsApp.Models.Communication.EmailStatusCodes.Scheduled;
                        apiCallDetails.ScheduledFor = Shared.Source.Mop.GetNextBusinessDay();
                    }
                    else
                    {
                        //call api right now
                        try
                        {
                            Helper.CallCaring(webData.logger, JsonConvert.SerializeObject(caringData));
                            apiCallDetails.Result = (int)ApplicantsApp.Models.Communication.EmailStatusCodes.Success;
                        }
                        catch (Exception ex)
                        {
                            apiCallDetails.Result = (int)ApplicantsApp.Models.Communication.EmailStatusCodes.Error;
                            apiCallDetails.ErrorMessage = ex.Message;
                        }
                    }

                    webData.logger.Info("Api Call Db Data: " + JsonConvert.SerializeObject(apiCallDetails, Formatting.Indented));
                    ApplicantsApp.Db.InsertActionEmail(
                            webData.logger,
                            apiCallDetails,
                            ApplicantsApp.Models.ReportActions.CallCaringComAPI,
                            new Guid("00000000-0000-0000-0000-000000000002"));
                }

                return Helper.Resp(new { note = "Form Id 7 data intercepted successfully!" });
            }
            catch (Exception ex)
            {
                Shared.Source.Mop.NotifyMeOnException("FORM ID 7 Convert", ex, webData.logger, false, webData?.json?.ToString());
                return Helper.HandleError(webData.logger, ex, HttpStatusCode.NotAcceptable);
            }
        }

        [HttpPost, HttpOptions]
        [EnableCors("*", "*", "*")]
        public HttpResponseMessage InsertApplicant()
        {
            if (Request.Method == HttpMethod.Options)
                return new HttpResponseMessage(HttpStatusCode.OK);

            WebInit webData = new WebInit();
            webData.InitMethod(Request, System.Reflection.MethodBase.GetCurrentMethod().Name);

            try
            {
                if (Request.Headers.SingleOrDefault(s => s.Key == "Pass").Value?.FirstOrDefault() != "Zkha8C36ss6Bf7B5t9ws95ng99xJlL2iE670P5Sz")
                {
                    throw new Exception("Pass not provided or does not match!");
                }

                Guid applicantId = ApplicantsApp.Db.CreateApplicantFromSource(
                    StripeController.StripeApiKey,
                    webData.logger,
                    webData.json.ToString(),
                    null,
                    out FullApplicantInfo convertedApplicantData);

                webData.logger.Info("  done!");

                ApplicantsApp.Db.InsertAction(
                    webData.logger,
                    applicantId,
                    ApplicantsApp.Models.ReportActions.ApplicantConvertFromGravityForm,
                    null,
                    data: JsonConvert.SerializeObject(new
                    {
                        originalData = webData.json.ToString(),
                        newData = JsonConvert.SerializeObject(convertedApplicantData)
                    })
                );

                return Helper.Resp(new { note = "Thank you, The applicant is created successfully!" });

            }
            catch (Exception ex)
            {
                Shared.Source.Mop.NotifyMeOnException("Gravity Convert", ex, webData.logger, false, webData?.json?.ToString());
                return Helper.HandleError(webData.logger, ex, HttpStatusCode.NotAcceptable);
            }
        }

    }
}
