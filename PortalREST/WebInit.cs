﻿using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace PortalREST
{
    public class WebInit
    {
        private static Random rand = new Random();

        public decimal uniqueIdent;
        public Logger logger;
        public JObject json;
        public string ip;

        public void InitMethod(HttpRequestMessage request, string methodName, bool disableLogging = false)
        {
            uniqueIdent = ((DateTime.Now.Ticks) * 10000000m) + rand.Next(0, 9999999);

            if (!disableLogging)
            {
                logger = LogManager.GetLogger(methodName + "." + uniqueIdent.ToString());

                logger.Info("WS === {0} ===", methodName);
                logger.Info("WS  RequestUri: {0}", request.RequestUri);

                
            }

            json = Helper.GetContent(request);
            ip = Helper.GetClientIp(request);

            if (!disableLogging)
            {
                logger.Info("WS   ip: {1}   json: {0}", json.ToString(), ip);
                Helper.LogHeader(request, methodName, json.ToString(), uniqueIdent);
            }
            
        }
    }
}