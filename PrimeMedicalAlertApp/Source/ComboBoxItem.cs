﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeMedicalAlertApp.Source
{
    public class ComboboxItem<T>
    {
        public string Text { get; set; }
        public T Value { get; set; }

        public string MetaString1 { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}
