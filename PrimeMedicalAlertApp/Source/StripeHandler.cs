﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeMedicalAlertApp.Source
{
    public static class StripeHandler
    {
        private const string StripeApiKey = "rk_live_szc5U9qwrjHd7Xo37BD8FdOs00OHSpCEM5";

        public static IEnumerable<Stripe.Customer> GetAllStripeCustomers()
        {
            Stripe.StripeConfiguration.ApiKey = StripeApiKey;

            var service = new Stripe.CustomerService();
            return service.ListAutoPaging();
        }

        public static Stripe.Customer GetStripeCustomerById(string customerId)
        {
            Stripe.StripeConfiguration.ApiKey = StripeApiKey;

            var service = new Stripe.CustomerService();
            return service.Get(customerId);
        }
    }
}
