﻿using System.Windows.Forms;

namespace PrimeMedicalAlertApp.Source
{
    public class Progress
    {
        private ProgressBar progressBar;
        private Label progressBarText;
        private Form form;
        private NLog.Logger logger;

        public Progress(ProgressBar progressBar, Label progressBarText, Form form, NLog.Logger logger)
        {
            this.progressBar = progressBar;
            this.progressBarText = progressBarText;
            this.form = form;
            this.logger = logger;
        }

        public void ShowProgressBar(string text)
        {
            logger.Info($"  ShowProgressBar: {text}");
            progressBar.Style = ProgressBarStyle.Marquee;
            progressBar.Visible = true;
            progressBarText.Visible = true;
            progressBarText.Text = text;
            form.Enabled = false;
            progressBarText.Enabled = true;
            progressBarText.BringToFront();
            Application.DoEvents();
        }

        public void HideProgressBar()
        {
            logger.Info($"  HideProgressBar");
            progressBar.Visible = false;
            progressBarText.Visible = false;
            progressBarText.Text = "";
            form.Enabled = true;
            Application.DoEvents();
        }

        public void SetProgressText(string text)
        {
            logger.Info($"  SetProgressText: {text}");
            progressBarText.Text = text;
            progressBarText.BringToFront();
            Application.DoEvents();
        }
    }
}
