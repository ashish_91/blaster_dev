﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Newtonsoft.Json;

namespace PrimeMedicalAlertApp.Source
{
    public static class Db
    {
        public static Users user;
        public static Sessions session;
        private static IMapper mapper;

        static Db()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Customers, Customers>();
            });

            configuration.AssertConfigurationIsValid();
            mapper = configuration.CreateMapper();
        }

        //======================
        //   SECURITY
        //======================
        #region security
        public static void Login(string userName, string password, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { userName })}");

            using (var db = new DbEntities())
            {
                userName = userName.Trim();
                string hash = Crypto.SHA512.ComputeHash(password);

                Users user = db.Users.SingleOrDefault(w => w.UserName == userName && w.PasswordHash == hash);
                if (user == null)
                {
                    throw new Exception("Invalid username or password!");
                }

                if (user.Enabled != 1)
                {
                    throw new Exception("This user is not enabled, contact Max");
                }

                user.LastLogin = DateTime.Now;
                Sessions session = db.Sessions.SingleOrDefault(w => w.UserId == user.Id);
                if (session == null)
                {
                    session = db.Sessions.Add(new Sessions()
                    {
                        Created = DateTime.Now,
                        Id = Guid.NewGuid(),
                        UserId = user.Id
                    });
                }

                db.SaveChanges();

                Db.user = user;
                Db.session = session;
            }
        }
        #endregion

        //======================
        //   CUSTOMER/CLIENT
        //======================
        #region customers or clients
        public static Customers CreateCustomer(Customers customer, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { customer })}");

            using (var db = new DbEntities())
            {
                db.Customers.Add(customer);
                db.SaveChanges();
                return customer;
            }
        }

        public static Customers GetCustomer(Guid customerId, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { customerId })}");

            using (var db = new DbEntities())
            {
                return db.Customers.Single(s => s.Id.Equals(customerId));
            }
        }

        public static Customers UpdateCustomer(Customers customer, NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name}, " +
                $"with params {JsonConvert.SerializeObject(new { customer })}");

            using (var db = new DbEntities())
            {
                var dbCustomer = db.Customers.Single(s => s.Id.Equals(customer.Id));
                dbCustomer = mapper.Map(customer, dbCustomer);
                db.SaveChanges();
                return dbCustomer;
            }
        }
        #endregion

        //==========================
        //      NOMS
        //==========================
        #region noms
        public static List<Nom_UnitTypes> GetUnitTypes(NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name} with params {JsonConvert.SerializeObject(new { })}");

            using (var db = new DbEntities())
            {
                return db.Nom_UnitTypes.ToList();
            }
        }

        public static List<Nom_MonitoringServices> GetMonitoringServices(NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name} with params {JsonConvert.SerializeObject(new { })}");

            using (var db = new DbEntities())
            {
                return db.Nom_MonitoringServices.ToList();
            }
        }

        public static List<Nom_CustomerStatuses> GetCustomerStatuses(NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name} with params {JsonConvert.SerializeObject(new { })}");

            using (var db = new DbEntities())
            {
                return db.Nom_CustomerStatuses.ToList();
            }
        }

        public static List<Nom_BillingSystems> GetBillingSystems(NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name} with params {JsonConvert.SerializeObject(new { })}");

            using (var db = new DbEntities())
            {
                return db.Nom_BillingSystems.ToList();
            }
        }

        public static List<Nom_PaySchedule> GetPaySchedules(NLog.Logger logger)
        {
            logger.Info($"Executing {System.Reflection.MethodBase.GetCurrentMethod().Name} with params {JsonConvert.SerializeObject(new { })}");

            using (var db = new DbEntities())
            {
                return db.Nom_PaySchedule.ToList();
            }
        }
        #endregion


    }
}
