﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrimeMedicalAlertApp.Source
{
    public static class Mop
    {
        public enum FormMode
        {
            New, Edit
        }

        public static T GetComboValue<T>(ComboBox combo)
        {
            if (combo is null || combo.SelectedItem is null) return default(T);
            return ((ComboboxItem<T>)combo.SelectedItem).Value;
        }

        public static void SetComboItemByValue<T>(ComboBox combo, T value)
        {
            foreach (object item in combo.Items)
            {
                if (((ComboboxItem<T>)item).Value.Equals(value))
                {
                    combo.SelectedItem = item;
                    break;
                }
            }
        }

        public static void FillCombo<T>(ComboBox combo, Dictionary<T, string> values)
        {
            combo.Items.Clear();

            foreach (var pair in values)
            {
                combo.Items.Add(new ComboboxItem<T>
                {
                    Text = pair.Value,
                    Value = pair.Key
                });
            }
        }

        public static void NotifyMe(string module, string body, NLog.Logger logger, bool raiseOnError)
        {
            try
            {
                string res = Mop.SendGmail(
                    "iwm2stuff@gmail.com",
                    "iwm2stuff@gmail.com",
                    null,
                    $"NOTIFY {module}",
                    body,
                    true,
                    new List<string>(),
                    logger,
                    false,
                    null,
                    true
                    );

                if (!string.IsNullOrWhiteSpace(res))
                {
                    throw new Exception(res);
                }
            }
            catch (Exception ex2)
            {
                logger.Error(ex2);
                if (raiseOnError)
                {
                    throw;
                }
            }
        }

        public static void NotifyMeOnException(string module, Exception ex, NLog.Logger logger, bool raiseOnError, string addInfo)
        {
            try
            {
                string res = Mop.SendGmail(
                    "iwm2stuff@gmail.com",
                    "iwm2stuff@gmail.com",
                    null,
                    $"ERROR__PrimeMedicalAlertsApp {module}",
                    addInfo + "      \n\n\n\n\n" + JsonConvert.SerializeObject(ex, Formatting.Indented),
                    false,
                    new List<string>(),
                    logger,
                    false,
                    null,
                    false
                    );

                if (!string.IsNullOrWhiteSpace(res))
                {
                    throw new Exception(res);
                }
            }
            catch (Exception ex2)
            {
                logger.Error(ex2);
                if (raiseOnError)
                {
                    throw;
                }
            }
        }

        public static string SendGmail(
            string fromAddress,
            string toAddress,
            string password,
            string subject,
            string body,
            bool isBodyHtml,
            IEnumerable<string> attachments,
            NLog.Logger logger,
            bool addFooter,
            string footerHTML,
            bool makeNewRowsBR
            )
        {
            logger.Info("START Sending email");
            logger.Info("  from address:" + fromAddress);
            logger.Info("  to address:" + toAddress);
            logger.Info("  password:" + (password == null ? "" : Crypto.AES.Encrypt(password, "T1eM2GaS3cr#$tPassw@wrd")));
            logger.Info("  subject:" + subject);
            logger.Info("  body:" + body);
            logger.Info("  attachments:" + string.Join(",", attachments));

            try
            {
                var from = new MailAddress(fromAddress);
                var to = new MailAddress(toAddress);

                if (fromAddress == "iwm2stuff@gmail.com")
                    password = "qapftrkwcpwkyqko";

                using (var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress, password),
                    Timeout = 20000
                })
                {

                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = subject,
                        Body = body,
                        IsBodyHtml = isBodyHtml,
                    })
                    {
                        if (attachments != null && attachments.Count() > 0)
                        {
                            foreach (var attachment in attachments)
                            {
                                message.Attachments.Add(new Attachment(attachment));
                            }
                        }

                        if (addFooter)
                        {
                            message.Body += footerHTML;
                        }

                        if (makeNewRowsBR)
                        {
                            message.Body = message.Body
                                .Replace("\n\r", "<br>")
                                .Replace("\r\n", "<br>")
                                .Replace("\n", "<br>")
                                .Replace("\r", "<br>");
                        }

                        logger.Info("Before send");
                        smtp.Send(message);
                        logger.Info("After send");

                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                logger.Info("ERROR: " + ex.Message);
                return ex.Message;
            }
        }
    }
}