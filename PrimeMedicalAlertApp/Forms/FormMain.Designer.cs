﻿namespace PrimeMedicalAlertApp.Forms
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.colIsDue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridCustomers = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAccountNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colZip = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaySchedule = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitTypeText = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRatePerMonth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusText = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colARPM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingSystemText = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMonitoringServiceText = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStripeClientId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingSystem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientType_IsStripeUserOrNot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMonitoringService = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnExit = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnAddClient = new System.Windows.Forms.ToolStripButton();
            this.btnEditClient = new System.Windows.Forms.ToolStripButton();
            this.btnDeleteClient = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridCustomers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // colIsDue
            // 
            this.colIsDue.FieldName = "IsDue";
            this.colIsDue.Name = "colIsDue";
            this.colIsDue.OptionsColumn.AllowEdit = false;
            this.colIsDue.Visible = true;
            this.colIsDue.VisibleIndex = 3;
            // 
            // gridCustomers
            // 
            this.gridCustomers.DataSource = typeof(PrimeMedicalAlertApp.viewCustomers);
            this.gridCustomers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCustomers.Location = new System.Drawing.Point(2, 2);
            this.gridCustomers.MainView = this.gridView1;
            this.gridCustomers.Name = "gridCustomers";
            this.gridCustomers.Size = new System.Drawing.Size(1209, 516);
            this.gridCustomers.TabIndex = 0;
            this.gridCustomers.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAccountNumber,
            this.colClientName,
            this.colEmailAddress,
            this.colIsDue,
            this.colAddress,
            this.colAddress2,
            this.colCity,
            this.colState,
            this.colZip,
            this.colTelephone,
            this.colPaySchedule,
            this.colUnitTypeText,
            this.colRatePerMonth,
            this.colStatusText,
            this.colStartDate,
            this.colARPM,
            this.colBillingSystemText,
            this.colMonitoringServiceText,
            this.colStripeClientId,
            this.colId,
            this.colBillingSystem,
            this.colClientType_IsStripeUserOrNot,
            this.colStatus,
            this.colMonitoringService,
            this.colUnitType});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colIsDue;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.BackColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseBackColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = true;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridCustomers;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.Behavior = DevExpress.XtraEditors.FindPanelBehavior.Filter;
            // 
            // colAccountNumber
            // 
            this.colAccountNumber.FieldName = "AccountNumber";
            this.colAccountNumber.Name = "colAccountNumber";
            this.colAccountNumber.OptionsColumn.AllowEdit = false;
            this.colAccountNumber.Visible = true;
            this.colAccountNumber.VisibleIndex = 0;
            // 
            // colClientName
            // 
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 1;
            // 
            // colEmailAddress
            // 
            this.colEmailAddress.FieldName = "EmailAddress";
            this.colEmailAddress.Name = "colEmailAddress";
            this.colEmailAddress.OptionsColumn.AllowEdit = false;
            this.colEmailAddress.Visible = true;
            this.colEmailAddress.VisibleIndex = 2;
            // 
            // colAddress
            // 
            this.colAddress.FieldName = "Address";
            this.colAddress.Name = "colAddress";
            this.colAddress.OptionsColumn.AllowEdit = false;
            this.colAddress.Visible = true;
            this.colAddress.VisibleIndex = 4;
            // 
            // colAddress2
            // 
            this.colAddress2.FieldName = "Address2";
            this.colAddress2.Name = "colAddress2";
            this.colAddress2.OptionsColumn.AllowEdit = false;
            this.colAddress2.Visible = true;
            this.colAddress2.VisibleIndex = 5;
            // 
            // colCity
            // 
            this.colCity.FieldName = "City";
            this.colCity.Name = "colCity";
            this.colCity.OptionsColumn.AllowEdit = false;
            this.colCity.Visible = true;
            this.colCity.VisibleIndex = 6;
            // 
            // colState
            // 
            this.colState.FieldName = "State";
            this.colState.Name = "colState";
            this.colState.OptionsColumn.AllowEdit = false;
            this.colState.Visible = true;
            this.colState.VisibleIndex = 7;
            // 
            // colZip
            // 
            this.colZip.FieldName = "Zip";
            this.colZip.Name = "colZip";
            this.colZip.OptionsColumn.AllowEdit = false;
            this.colZip.Visible = true;
            this.colZip.VisibleIndex = 8;
            // 
            // colTelephone
            // 
            this.colTelephone.FieldName = "Telephone";
            this.colTelephone.Name = "colTelephone";
            this.colTelephone.OptionsColumn.AllowEdit = false;
            this.colTelephone.Visible = true;
            this.colTelephone.VisibleIndex = 9;
            // 
            // colPaySchedule
            // 
            this.colPaySchedule.FieldName = "PaySchedule";
            this.colPaySchedule.Name = "colPaySchedule";
            this.colPaySchedule.OptionsColumn.AllowEdit = false;
            this.colPaySchedule.Visible = true;
            this.colPaySchedule.VisibleIndex = 10;
            // 
            // colUnitTypeText
            // 
            this.colUnitTypeText.FieldName = "UnitTypeText";
            this.colUnitTypeText.Name = "colUnitTypeText";
            this.colUnitTypeText.OptionsColumn.AllowEdit = false;
            this.colUnitTypeText.Visible = true;
            this.colUnitTypeText.VisibleIndex = 11;
            // 
            // colRatePerMonth
            // 
            this.colRatePerMonth.FieldName = "RatePerMonth";
            this.colRatePerMonth.Name = "colRatePerMonth";
            this.colRatePerMonth.OptionsColumn.AllowEdit = false;
            this.colRatePerMonth.Visible = true;
            this.colRatePerMonth.VisibleIndex = 12;
            // 
            // colStatusText
            // 
            this.colStatusText.FieldName = "StatusText";
            this.colStatusText.Name = "colStatusText";
            this.colStatusText.OptionsColumn.AllowEdit = false;
            this.colStatusText.Visible = true;
            this.colStatusText.VisibleIndex = 13;
            // 
            // colStartDate
            // 
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 14;
            // 
            // colARPM
            // 
            this.colARPM.FieldName = "ARPM";
            this.colARPM.Name = "colARPM";
            this.colARPM.OptionsColumn.AllowEdit = false;
            this.colARPM.Visible = true;
            this.colARPM.VisibleIndex = 15;
            // 
            // colBillingSystemText
            // 
            this.colBillingSystemText.FieldName = "BillingSystemText";
            this.colBillingSystemText.Name = "colBillingSystemText";
            this.colBillingSystemText.OptionsColumn.AllowEdit = false;
            this.colBillingSystemText.Visible = true;
            this.colBillingSystemText.VisibleIndex = 16;
            // 
            // colMonitoringServiceText
            // 
            this.colMonitoringServiceText.FieldName = "MonitoringServiceText";
            this.colMonitoringServiceText.Name = "colMonitoringServiceText";
            this.colMonitoringServiceText.OptionsColumn.AllowEdit = false;
            this.colMonitoringServiceText.Visible = true;
            this.colMonitoringServiceText.VisibleIndex = 17;
            // 
            // colStripeClientId
            // 
            this.colStripeClientId.FieldName = "StripeClientId";
            this.colStripeClientId.Name = "colStripeClientId";
            this.colStripeClientId.OptionsColumn.AllowEdit = false;
            this.colStripeClientId.Visible = true;
            this.colStripeClientId.VisibleIndex = 18;
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            // 
            // colBillingSystem
            // 
            this.colBillingSystem.FieldName = "BillingSystem";
            this.colBillingSystem.Name = "colBillingSystem";
            this.colBillingSystem.OptionsColumn.AllowEdit = false;
            // 
            // colClientType_IsStripeUserOrNot
            // 
            this.colClientType_IsStripeUserOrNot.FieldName = "ClientType_IsStripeUserOrNot";
            this.colClientType_IsStripeUserOrNot.Name = "colClientType_IsStripeUserOrNot";
            this.colClientType_IsStripeUserOrNot.OptionsColumn.AllowEdit = false;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            // 
            // colMonitoringService
            // 
            this.colMonitoringService.FieldName = "MonitoringService";
            this.colMonitoringService.Name = "colMonitoringService";
            this.colMonitoringService.OptionsColumn.AllowEdit = false;
            // 
            // colUnitType
            // 
            this.colUnitType.FieldName = "UnitType";
            this.colUnitType.Name = "colUnitType";
            this.colUnitType.OptionsColumn.AllowEdit = false;
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.gridCustomers);
            this.panelControl1.Location = new System.Drawing.Point(0, 42);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1213, 520);
            this.panelControl1.TabIndex = 1;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Location = new System.Drawing.Point(1126, 568);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "Close";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddClient,
            this.btnEditClient,
            this.btnDeleteClient});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1213, 39);
            this.toolStrip1.TabIndex = 9;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnAddClient
            // 
            this.btnAddClient.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddClient.Image = ((System.Drawing.Image)(resources.GetObject("btnAddClient.Image")));
            this.btnAddClient.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddClient.Name = "btnAddClient";
            this.btnAddClient.Size = new System.Drawing.Size(36, 36);
            this.btnAddClient.Text = "toolStripButton1";
            this.btnAddClient.ToolTipText = "New Applicant";
            this.btnAddClient.Click += new System.EventHandler(this.btnAddClient_Click);
            // 
            // btnEditClient
            // 
            this.btnEditClient.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEditClient.Image = ((System.Drawing.Image)(resources.GetObject("btnEditClient.Image")));
            this.btnEditClient.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditClient.Name = "btnEditClient";
            this.btnEditClient.Size = new System.Drawing.Size(36, 36);
            this.btnEditClient.ToolTipText = "Edit Applicant";
            this.btnEditClient.Click += new System.EventHandler(this.btnEditClient_Click);
            // 
            // btnDeleteClient
            // 
            this.btnDeleteClient.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDeleteClient.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteClient.Image")));
            this.btnDeleteClient.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDeleteClient.Name = "btnDeleteClient";
            this.btnDeleteClient.Size = new System.Drawing.Size(36, 36);
            this.btnDeleteClient.Text = "toolStripButton3";
            this.btnDeleteClient.ToolTipText = "Delete applicant";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1213, 603);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.panelControl1);
            this.Name = "FormMain";
            this.Text = "Prime Medical Alert Clients Register";
            ((System.ComponentModel.ISupportInitialize)(this.gridCustomers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridCustomers;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Button btnExit;
        private DevExpress.XtraGrid.Columns.GridColumn colAccountNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress2;
        private DevExpress.XtraGrid.Columns.GridColumn colCity;
        private DevExpress.XtraGrid.Columns.GridColumn colState;
        private DevExpress.XtraGrid.Columns.GridColumn colZip;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colPaySchedule;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitTypeText;
        private DevExpress.XtraGrid.Columns.GridColumn colRatePerMonth;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusText;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colARPM;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingSystemText;
        private DevExpress.XtraGrid.Columns.GridColumn colMonitoringServiceText;
        private DevExpress.XtraGrid.Columns.GridColumn colStripeClientId;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingSystem;
        private DevExpress.XtraGrid.Columns.GridColumn colClientType_IsStripeUserOrNot;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colMonitoringService;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitType;
        private DevExpress.XtraGrid.Columns.GridColumn colIsDue;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnAddClient;
        private System.Windows.Forms.ToolStripButton btnEditClient;
        private System.Windows.Forms.ToolStripButton btnDeleteClient;
    }
}