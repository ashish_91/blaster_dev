﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using PrimeMedicalAlertApp.Source;

namespace PrimeMedicalAlertApp.Forms
{
    public partial class FormCustomer : Form
    {
        public bool NeedRefresh { get; private set; } = false;
        public Guid? clientId;

        public Mop.FormMode FormMode { get; set; }
        private Customers Customer { get; set; }
        private NLog.Logger logger = NLog.LogManager.GetLogger("info");
        private Progress progress;
        private bool isBillingSystemStripe;
        private bool isAccountLinkedToStripe;

        private List<Nom_BillingSystems> billingSystems;
        private List<Nom_CustomerStatuses> customerStatuses;
        private List<Nom_MonitoringServices> monitoringServices;
        private List<Nom_UnitTypes> unitTypes;
        private List<Nom_PaySchedule> paySchedules;

        public FormCustomer()
        {
            InitializeComponent();
        }

        private void LoadNoms()
        {
            billingSystems = Db.GetBillingSystems(logger);
            customerStatuses = Db.GetCustomerStatuses(logger);
            monitoringServices = Db.GetMonitoringServices(logger);
            unitTypes = Db.GetUnitTypes(logger);
            paySchedules = Db.GetPaySchedules(logger);
        }

        private void InitCombos()
        {
            Mop.FillCombo(comboBillingSystem, billingSystems.ToDictionary(k => k.Id, v => v.BillingSystem));
            Mop.FillCombo(comboMonitoringService, monitoringServices.ToDictionary(k => k.Id, v => v.MonitoringService));
            Mop.FillCombo(comboPaySchedule, paySchedules.ToDictionary(k => k.Id, v => v.PaySchedule));
            Mop.FillCombo(comboUnitType, unitTypes.ToDictionary(k => k.Id, v => v.UnitType));

            comboBillingSystem.SelectedIndex = 0;
            comboMonitoringService.SelectedIndex = 0;
            comboPaySchedule.SelectedIndex = 0;
            comboUnitType.SelectedIndex = 0;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormCustomer_Load(object sender, EventArgs e)
        {
            progress = new Progress(progressBar1, labelProgress, this, logger);

            progress.ShowProgressBar("Loading data...");
            try
            {
                LoadNoms();
                InitCombos();

                if (FormMode == Mop.FormMode.New)
                {
                    Customer = new Customers();
                    Customer.Id = Guid.NewGuid();
                }

                if (FormMode == Mop.FormMode.Edit)
                {
                    Customer = Db.GetCustomer(clientId.Value, logger);

                    isAccountLinkedToStripe = !string.IsNullOrWhiteSpace(Customer.StripeClientId);

                    txtAccountNumber.Text = Customer.AccountNumber;
                    txtClientName.Text = Customer.ClientName;
                    txtEmailAddress.Text = Customer.EmailAddress;
                    Mop.SetComboItemByValue(comboUnitType, Customer.UnitType);
                    Mop.SetComboItemByValue(comboMonitoringService, Customer.MonitoringService);
                    checkIsActive.Checked = true;

                    txtAddress.Text = Customer.Address;
                    txtAddress2.Text = Customer.Address2;
                    txtCity.Text = Customer.City;
                    txtState.Text = Customer.State;
                    txtZip.Text = Customer.Zip;
                    txtTelephone.Text = Customer.Telephone;
                    checkIsDueManual.Checked = Customer.IsDue;

                    Mop.SetComboItemByValue(comboBillingSystem, Customer.BillingSystem);
                    txtStripeCustomerNo.Text = Customer.StripeClientId;
                    Mop.SetComboItemByValue(comboPaySchedule, Customer.PayScheduleId.Value);
                    txtRatePerMonth.Value = Customer.RatePerMonth;
                    dateStartDate.Value = Customer.StartDate;
                    txtARPM.Value = Customer.ARPM;

                }

                RespectMode();
            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void RespectMode()
        {
            switch (FormMode)
            {
                case Mop.FormMode.New:
                    Text = $"Client [ {FormMode.ToString().ToUpperInvariant()} ]";
                    break;

                case Mop.FormMode.Edit:
                    Text = $"Client  '{Customer.ClientName}'    [ {FormMode.ToString().ToUpperInvariant()} ]";
                    break;

                default: throw new Exception($"Unsupported form mode {FormMode}");
            }

            btnSave.Visible = true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!IsFormValid())
                return;

            progress.ShowProgressBar("Saving client");
            try
            {
                Customer.AccountNumber = txtAccountNumber.Text;
                Customer.ClientName = txtClientName.Text;
                Customer.EmailAddress = txtEmailAddress.Text;
                Customer.UnitType = Mop.GetComboValue<int>(comboUnitType);
                Customer.MonitoringService = Mop.GetComboValue<int>(comboMonitoringService);
                checkIsActive.Checked = true;

                Customer.Address = txtAddress.Text;
                Customer.Address2 = txtAddress2.Text;
                Customer.City = txtCity.Text;
                Customer.State = txtState.Text;
                Customer.Zip = txtZip.Text;
                Customer.Telephone = txtTelephone.Text;
                Customer.IsDue = checkIsDueManual.Checked;

                Customer.BillingSystem = Mop.GetComboValue<int>(comboBillingSystem);
                Customer.StripeClientId = isBillingSystemStripe ? txtStripeCustomerNo.Text : "";
                Customer.PayScheduleId = Mop.GetComboValue<int>(comboPaySchedule);
                Customer.RatePerMonth = txtRatePerMonth.Value;
                Customer.StartDate = dateStartDate.Value;
                Customer.ARPM = txtARPM.Value;

                switch (FormMode)
                {
                    case Mop.FormMode.New:
                        Db.CreateCustomer(Customer, logger);
                        break;

                    case Mop.FormMode.Edit:
                        Db.UpdateCustomer(Customer, logger);
                        break;

                    default:
                        throw new Exception($"Unexpected form mode: {FormMode}");
                }

                //TODO: audit?
            }
            finally
            {
                progress.HideProgressBar();
            }

            MessageBox.Show("The client was stored successfully!");
            if (FormMode == Mop.FormMode.New)
            {
                FormMode = Mop.FormMode.Edit;
                Customer = Db.GetCustomer(Customer.Id, logger);

                RespectMode();
            }

            NeedRefresh = true;
        }

        private bool IsFormValid()
        {
            if (txtClientName.TextLength < 7)
            {
                MessageBox.Show("Client Name is too short!");
                if (txtClientName.CanFocus) txtClientName.Focus();
                return false;
            }

            if (txtAccountNumber.TextLength < 5)
            {
                MessageBox.Show("Account number is too short!");
                if (txtAccountNumber.CanFocus) txtAccountNumber.Focus();
                return false;
            }

            //stripe
            if (isBillingSystemStripe && !isAccountLinkedToStripe)
            {
                MessageBox.Show("The billing system is 'Stripe' but the account is not linked to stripe user!");
                if (btnLinkStripe.CanFocus) btnLinkStripe.Focus();
                return false;
            }

            return true;
        }

        private void FormCustomer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (NeedRefresh)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                DialogResult = DialogResult.Abort;
            }
        }

        private void comboBillingSystem_SelectedIndexChanged(object sender, EventArgs e)
        {
            isBillingSystemStripe = comboBillingSystem.Text.ToLowerInvariant().Trim().Equals("stripe");

            txtStripeCustomerNo.ReadOnly = !isBillingSystemStripe;
            btnLinkStripe.Enabled = isBillingSystemStripe;

            txtRatePerMonth.ReadOnly = isBillingSystemStripe;
            dateStartDate.Enabled = !isBillingSystemStripe;
            txtARPM.ReadOnly = isBillingSystemStripe;
            checkIsDueManual.Enabled = !isBillingSystemStripe;

            txtAddress.ReadOnly = isBillingSystemStripe;
            txtAddress2.ReadOnly = isBillingSystemStripe;
            txtTelephone.ReadOnly = isBillingSystemStripe;
            txtState.ReadOnly = isBillingSystemStripe;
            txtCity.ReadOnly = isBillingSystemStripe;
            txtZip.ReadOnly = isBillingSystemStripe;

            txtClientName.ReadOnly = isBillingSystemStripe;
            txtEmailAddress.ReadOnly = isBillingSystemStripe;
        }

        private void btnLinkStripe_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtStripeCustomerNo.Text))
            {
                MessageBox.Show("For 'Billing system' - 'Stripe' you need to enter a valid customer stripe id!");
                return;
            }

            Stripe.Customer stripeCustomer = StripeHandler.GetStripeCustomerById(txtStripeCustomerNo.Text);

            txtClientName.Text = stripeCustomer.Name;
            txtEmailAddress.Text = stripeCustomer.Email;
            txtAddress.Text = stripeCustomer.Address?.Line1 ?? stripeCustomer.Shipping?.Address?.Line1;
            txtAddress2.Text = stripeCustomer.Address?.Line2 ?? stripeCustomer.Shipping?.Address?.Line2;
            txtCity.Text = stripeCustomer.Address?.City ?? stripeCustomer.Shipping?.Address?.City;
            txtState.Text = stripeCustomer.Address?.State ?? stripeCustomer.Shipping?.Address?.State;
            txtZip.Text = stripeCustomer.Address?.PostalCode ?? stripeCustomer.Shipping?.Address?.PostalCode;
            txtTelephone.Text = stripeCustomer.Phone;

            int index = 0;
            foreach (var subscription in stripeCustomer.Subscriptions)
            {
                index++;
                bool isTheOnlyOneOrLastSubscription = stripeCustomer.Subscriptions.Count() == 1 || stripeCustomer.Subscriptions.Count() == index;

                if (!isTheOnlyOneOrLastSubscription && subscription.Status != "active") continue;

                txtRatePerMonth.Value = (subscription.Plan.AmountDecimal ?? 0) / 100m;
                txtARPM.Value = (subscription.Plan.AmountDecimal ?? 0) / 100m;
                dateStartDate.Value = subscription.StartDate ?? DateTime.Now;
                //checkIsDueManual.Checked = false;

                string payScheme;
                if (subscription.Plan.Interval == "month")
                {
                    if (subscription.Plan.IntervalCount == 1)
                    {
                        payScheme = "M";
                    }
                    else if (subscription.Plan.IntervalCount == 3)
                    {
                        payScheme = "Q";
                    }
                    else
                    {
                        payScheme = "NOT DETECTED";
                    }
                }
                else if (subscription.Plan.Interval == "year")
                {
                    payScheme = "Y";
                }
                else
                {
                    payScheme = "NOT DETECTED";
                }

                MessageBox.Show($"Plan detected and applied: {subscription.Plan.Nickname}. Pay schedule Automatically set to: '{payScheme}'");
                comboPaySchedule.Text = payScheme;
                isAccountLinkedToStripe = true;

                break;
            }
        }
    }
}