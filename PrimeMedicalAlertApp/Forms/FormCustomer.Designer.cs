﻿namespace PrimeMedicalAlertApp.Forms
{
    partial class FormCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtAccountNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtClientName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEmailAddress = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtState = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtZip = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTelephone = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.comboPaySchedule = new System.Windows.Forms.ComboBox();
            this.comboUnitType = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBillingSystem = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.comboMonitoringService = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkIsDueManual = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtARPM = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.dateStartDate = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtRatePerMonth = new System.Windows.Forms.NumericUpDown();
            this.btnLinkStripe = new System.Windows.Forms.Button();
            this.txtStripeCustomerNo = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupClientAddress = new System.Windows.Forms.GroupBox();
            this.groupClientInfo = new System.Windows.Forms.GroupBox();
            this.checkIsActive = new System.Windows.Forms.CheckBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.labelProgress = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtARPM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRatePerMonth)).BeginInit();
            this.groupClientAddress.SuspendLayout();
            this.groupClientInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtAccountNumber
            // 
            this.txtAccountNumber.Location = new System.Drawing.Point(10, 45);
            this.txtAccountNumber.Name = "txtAccountNumber";
            this.txtAccountNumber.Size = new System.Drawing.Size(129, 20);
            this.txtAccountNumber.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Account Number:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(159, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Client Name:";
            // 
            // txtClientName
            // 
            this.txtClientName.Location = new System.Drawing.Point(159, 45);
            this.txtClientName.Name = "txtClientName";
            this.txtClientName.Size = new System.Drawing.Size(155, 20);
            this.txtClientName.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(336, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Email Address:";
            // 
            // txtEmailAddress
            // 
            this.txtEmailAddress.Location = new System.Drawing.Point(336, 45);
            this.txtEmailAddress.Name = "txtEmailAddress";
            this.txtEmailAddress.Size = new System.Drawing.Size(152, 20);
            this.txtEmailAddress.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Address:";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(13, 41);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(231, 20);
            this.txtAddress.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(261, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Address 2:";
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(264, 41);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(224, 20);
            this.txtAddress2.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "City:";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(13, 91);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(128, 20);
            this.txtCity.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(161, 75);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "State:";
            // 
            // txtState
            // 
            this.txtState.Location = new System.Drawing.Point(161, 91);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(61, 20);
            this.txtState.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(245, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Zip:";
            // 
            // txtZip
            // 
            this.txtZip.Location = new System.Drawing.Point(245, 91);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(73, 20);
            this.txtZip.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(340, 75);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Telephone:";
            // 
            // txtTelephone
            // 
            this.txtTelephone.Location = new System.Drawing.Point(340, 91);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(148, 20);
            this.txtTelephone.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(414, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Pay schedule:";
            // 
            // comboPaySchedule
            // 
            this.comboPaySchedule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboPaySchedule.FormattingEnabled = true;
            this.comboPaySchedule.Location = new System.Drawing.Point(414, 36);
            this.comboPaySchedule.Name = "comboPaySchedule";
            this.comboPaySchedule.Size = new System.Drawing.Size(74, 21);
            this.comboPaySchedule.TabIndex = 15;
            // 
            // comboUnitType
            // 
            this.comboUnitType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboUnitType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboUnitType.FormattingEnabled = true;
            this.comboUnitType.Location = new System.Drawing.Point(11, 99);
            this.comboUnitType.Name = "comboUnitType";
            this.comboUnitType.Size = new System.Drawing.Size(128, 21);
            this.comboUnitType.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 83);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Unit Type:";
            // 
            // comboBillingSystem
            // 
            this.comboBillingSystem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBillingSystem.FormattingEnabled = true;
            this.comboBillingSystem.Location = new System.Drawing.Point(15, 36);
            this.comboBillingSystem.Name = "comboBillingSystem";
            this.comboBillingSystem.Size = new System.Drawing.Size(194, 21);
            this.comboBillingSystem.TabIndex = 12;
            this.comboBillingSystem.SelectedIndexChanged += new System.EventHandler(this.comboBillingSystem_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "Billing System:";
            // 
            // comboMonitoringService
            // 
            this.comboMonitoringService.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboMonitoringService.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboMonitoringService.FormattingEnabled = true;
            this.comboMonitoringService.Location = new System.Drawing.Point(159, 99);
            this.comboMonitoringService.Name = "comboMonitoringService";
            this.comboMonitoringService.Size = new System.Drawing.Size(198, 21);
            this.comboMonitoringService.TabIndex = 4;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(159, 83);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(98, 13);
            this.label15.TabIndex = 28;
            this.label15.Text = "Monitoring Service:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkIsDueManual);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.txtARPM);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.dateStartDate);
            this.groupBox1.Controls.Add(this.comboPaySchedule);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtRatePerMonth);
            this.groupBox1.Controls.Add(this.btnLinkStripe);
            this.groupBox1.Controls.Add(this.txtStripeCustomerNo);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.comboBillingSystem);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Location = new System.Drawing.Point(17, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(508, 127);
            this.groupBox1.TabIndex = 32;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Payment Info (If the client is in STRIPE all data bellow will be loaded automatic" +
    "ally)";
            // 
            // checkIsDueManual
            // 
            this.checkIsDueManual.AutoSize = true;
            this.checkIsDueManual.Location = new System.Drawing.Point(372, 89);
            this.checkIsDueManual.Name = "checkIsDueManual";
            this.checkIsDueManual.Size = new System.Drawing.Size(111, 17);
            this.checkIsDueManual.TabIndex = 40;
            this.checkIsDueManual.Text = "Is Due (MANUAL)";
            this.checkIsDueManual.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(251, 90);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(13, 13);
            this.label18.TabIndex = 39;
            this.label18.Text = "$";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(264, 72);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 13);
            this.label19.TabIndex = 38;
            this.label19.Text = "ARPM:";
            // 
            // txtARPM
            // 
            this.txtARPM.DecimalPlaces = 2;
            this.txtARPM.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.txtARPM.Location = new System.Drawing.Point(264, 88);
            this.txtARPM.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.txtARPM.Name = "txtARPM";
            this.txtARPM.Size = new System.Drawing.Size(70, 20);
            this.txtARPM.TabIndex = 18;
            this.txtARPM.ThousandsSeparator = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(109, 72);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 13);
            this.label16.TabIndex = 36;
            this.label16.Text = "Start date:";
            // 
            // dateStartDate
            // 
            this.dateStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateStartDate.Location = new System.Drawing.Point(110, 88);
            this.dateStartDate.Name = "dateStartDate";
            this.dateStartDate.Size = new System.Drawing.Size(125, 20);
            this.dateStartDate.TabIndex = 17;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 90);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 13);
            this.label13.TabIndex = 34;
            this.label13.Text = "$";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 71);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 13);
            this.label12.TabIndex = 33;
            this.label12.Text = "Rate per month:";
            // 
            // txtRatePerMonth
            // 
            this.txtRatePerMonth.DecimalPlaces = 2;
            this.txtRatePerMonth.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.txtRatePerMonth.Location = new System.Drawing.Point(26, 88);
            this.txtRatePerMonth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.txtRatePerMonth.Name = "txtRatePerMonth";
            this.txtRatePerMonth.Size = new System.Drawing.Size(70, 20);
            this.txtRatePerMonth.TabIndex = 16;
            this.txtRatePerMonth.ThousandsSeparator = true;
            // 
            // btnLinkStripe
            // 
            this.btnLinkStripe.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnLinkStripe.Location = new System.Drawing.Point(352, 36);
            this.btnLinkStripe.Name = "btnLinkStripe";
            this.btnLinkStripe.Size = new System.Drawing.Size(49, 20);
            this.btnLinkStripe.TabIndex = 14;
            this.btnLinkStripe.Text = "Link";
            this.btnLinkStripe.UseVisualStyleBackColor = true;
            this.btnLinkStripe.Click += new System.EventHandler(this.btnLinkStripe_Click);
            // 
            // txtStripeCustomerNo
            // 
            this.txtStripeCustomerNo.Location = new System.Drawing.Point(232, 36);
            this.txtStripeCustomerNo.Name = "txtStripeCustomerNo";
            this.txtStripeCustomerNo.Size = new System.Drawing.Size(120, 20);
            this.txtStripeCustomerNo.TabIndex = 13;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(232, 21);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(101, 13);
            this.label17.TabIndex = 28;
            this.label17.Text = "Stripe Customer No:";
            // 
            // groupClientAddress
            // 
            this.groupClientAddress.Controls.Add(this.label4);
            this.groupClientAddress.Controls.Add(this.txtAddress);
            this.groupClientAddress.Controls.Add(this.txtAddress2);
            this.groupClientAddress.Controls.Add(this.label5);
            this.groupClientAddress.Controls.Add(this.txtCity);
            this.groupClientAddress.Controls.Add(this.label6);
            this.groupClientAddress.Controls.Add(this.label9);
            this.groupClientAddress.Controls.Add(this.txtState);
            this.groupClientAddress.Controls.Add(this.txtTelephone);
            this.groupClientAddress.Controls.Add(this.label7);
            this.groupClientAddress.Controls.Add(this.label8);
            this.groupClientAddress.Controls.Add(this.txtZip);
            this.groupClientAddress.Location = new System.Drawing.Point(13, 291);
            this.groupClientAddress.Name = "groupClientAddress";
            this.groupClientAddress.Size = new System.Drawing.Size(508, 128);
            this.groupClientAddress.TabIndex = 33;
            this.groupClientAddress.TabStop = false;
            this.groupClientAddress.Text = "Client Address";
            // 
            // groupClientInfo
            // 
            this.groupClientInfo.Controls.Add(this.checkIsActive);
            this.groupClientInfo.Controls.Add(this.label1);
            this.groupClientInfo.Controls.Add(this.txtAccountNumber);
            this.groupClientInfo.Controls.Add(this.txtClientName);
            this.groupClientInfo.Controls.Add(this.comboMonitoringService);
            this.groupClientInfo.Controls.Add(this.label15);
            this.groupClientInfo.Controls.Add(this.label2);
            this.groupClientInfo.Controls.Add(this.comboUnitType);
            this.groupClientInfo.Controls.Add(this.txtEmailAddress);
            this.groupClientInfo.Controls.Add(this.label11);
            this.groupClientInfo.Controls.Add(this.label3);
            this.groupClientInfo.Location = new System.Drawing.Point(17, 143);
            this.groupClientInfo.Name = "groupClientInfo";
            this.groupClientInfo.Size = new System.Drawing.Size(508, 138);
            this.groupClientInfo.TabIndex = 34;
            this.groupClientInfo.TabStop = false;
            this.groupClientInfo.Text = "Client Info";
            // 
            // checkIsActive
            // 
            this.checkIsActive.AutoSize = true;
            this.checkIsActive.Checked = true;
            this.checkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkIsActive.Location = new System.Drawing.Point(372, 101);
            this.checkIsActive.Name = "checkIsActive";
            this.checkIsActive.Size = new System.Drawing.Size(67, 17);
            this.checkIsActive.TabIndex = 5;
            this.checkIsActive.Text = "Is Active";
            this.checkIsActive.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(446, 425);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 35;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(364, 425);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 35;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(12, 425);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(346, 23);
            this.progressBar1.TabIndex = 36;
            this.progressBar1.Visible = false;
            // 
            // labelProgress
            // 
            this.labelProgress.AutoSize = true;
            this.labelProgress.Location = new System.Drawing.Point(14, 432);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(25, 13);
            this.labelProgress.TabIndex = 37;
            this.labelProgress.Text = "_  _";
            this.labelProgress.Visible = false;
            // 
            // FormCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 456);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupClientInfo);
            this.Controls.Add(this.groupClientAddress);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormCustomer";
            this.Text = "Client";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormCustomer_FormClosing);
            this.Load += new System.EventHandler(this.FormCustomer_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtARPM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRatePerMonth)).EndInit();
            this.groupClientAddress.ResumeLayout(false);
            this.groupClientAddress.PerformLayout();
            this.groupClientInfo.ResumeLayout(false);
            this.groupClientInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtAccountNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtClientName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEmailAddress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAddress2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtState;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtZip;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTelephone;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboPaySchedule;
        private System.Windows.Forms.ComboBox comboUnitType;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBillingSystem;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboMonitoringService;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtStripeCustomerNo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnLinkStripe;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DateTimePicker dateStartDate;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown txtRatePerMonth;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown txtARPM;
        private System.Windows.Forms.GroupBox groupClientAddress;
        private System.Windows.Forms.GroupBox groupClientInfo;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label labelProgress;
        private System.Windows.Forms.CheckBox checkIsActive;
        private System.Windows.Forms.CheckBox checkIsDueManual;
    }
}