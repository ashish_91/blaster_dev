﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Source
{
    public static class Resources
    {
        public const string USADateTime = "MM'/'dd'/'yyyy hh:mm tt";
        public const string USADateOnly = "MM'/'dd'/'yyyy";
        public const string USATimeOnly = "hh:mm tt";
    }
}
