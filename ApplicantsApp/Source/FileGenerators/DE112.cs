﻿using ApplicantsApp.Models.Applicant.Applicant;
using ApplicantsApp.Models.Applicant.Applicant.Enums;
using ApplicantsApp.Models.Representative;
using System;

namespace ApplicantsApp.Source.FileGenerators
{
    public static class DE112
    {
        private const string de112TemplateFileName = @".\Documents\DE-112_english_editable.pdf";

        /// <summary>
        /// Generates DE112 file
        /// </summary>
        /// <param name="targetFileName">The file which will be created</param>
        /// <param name="applicantInfo">The applicant info</param>
        /// <param name="representative">The representative info</param>
        /// <param name="logger">A logger</param>
        public static string Fill(            
            FullApplicantInfo applicantInfo,
            RepresentativeInfo representative,
            Guid documentId,
            NLog.Logger logger)
        {
            string targetFileName = Shared.Source.Mop.GetTempFileName(
                "",
                $"de-112__{documentId.ToString()}.pdf"
                );

            var pdfFiller = new Source.PdfFiller(de112TemplateFileName, logger);

            pdfFiller.SetText("RepresentativesName", representative.name);
            pdfFiller.SetBool("IsRepresentativeYourLegalGuardian_Yes", representative.guardian.isRepresentativeAGuardian);
            pdfFiller.SetBool("IsRepresentativeYourLegalGuardian_No", !representative.guardian.isRepresentativeAGuardian);

            pdfFiller.SetText("RepresentativesMailingAddress", representative.address.fullStreetAddress);
            pdfFiller.SetText("RepresentativeCity", representative.address.city);
            pdfFiller.SetText("RepresentativeState", representative.address.state);
            pdfFiller.SetText("RepresentativeZip", representative.address.zip);

            pdfFiller.SetText("RepresentativePhone", representative.phoneNumber);
            pdfFiller.SetBool("RepresentativePhone_Home", representative.phoneNumberType == RepresentativePhoneNumberType.Home);
            pdfFiller.SetBool("RepresentativePhone_Cell", representative.phoneNumberType == RepresentativePhoneNumberType.Cell);
            pdfFiller.SetBool("RepresentativePhone_Work", representative.phoneNumberType == RepresentativePhoneNumberType.Work);
            pdfFiller.SetBool("RepresentativePhone_Message", representative.phoneNumberType == RepresentativePhoneNumberType.Message);
            pdfFiller.SetBool("RepresentativePhone_Other", representative.phoneNumberType == RepresentativePhoneNumberType.Other);
            pdfFiller.SetText("RepresentativePhone_OtherText", 
                representative.phoneNumberType == RepresentativePhoneNumberType.Other ? representative.phoneNumberTypeOther : string.Empty);

            pdfFiller.SetText("RepresentativeOtherPhone", representative.otherPhoneNumber);
            pdfFiller.SetBool("RepresentativeOtherPhone_Home", representative.otherphoneNumberType == RepresentativePhoneNumberType.Home);
            pdfFiller.SetBool("RepresentativeOtherPhone_Cell", representative.otherphoneNumberType == RepresentativePhoneNumberType.Cell);
            pdfFiller.SetBool("RepresentativeOtherPhone_Work", representative.otherphoneNumberType == RepresentativePhoneNumberType.Work);
            pdfFiller.SetBool("RepresentativeOtherPhone_Message", representative.otherphoneNumberType == RepresentativePhoneNumberType.Message);
            pdfFiller.SetBool("RepresentativeOtherPhone_Other", representative.otherphoneNumberType == RepresentativePhoneNumberType.Other);
            pdfFiller.SetText("RepresentativeOtherPhone_OtherText", representative.otherphoneNumberType == RepresentativePhoneNumberType.Other ? representative.otherPhoneNumberTypeOther : string.Empty);

            pdfFiller.SetBool("RepresentativeSpokoen_English", representative.spokenLanguage == "English");
            pdfFiller.SetBool("RepresentativeSpokoen_Spanish", representative.spokenLanguage == "Spanish");
            pdfFiller.SetBool("RepresentativeSpokoen_Other", representative.spokenLanguage != "English" && representative.spokenLanguage == "Spanish");
            pdfFiller.SetText("RepresentativeSpokoen_OtherText", representative.spokenLanguage != "English" && representative.spokenLanguage == "Spanish" ? representative.spokenLanguage : string.Empty);

            pdfFiller.SetBool("RepresentativeWritten_English", representative.writtenLanguage == "English");
            pdfFiller.SetBool("RepresentativeWritten_Spanish", representative.writtenLanguage == "Spanish");
            pdfFiller.SetBool("RepresentativeWritten_Other", representative.writtenLanguage != "English" && representative.spokenLanguage == "Spanish");
            pdfFiller.SetText("RepresentativeWritten_OtherText", representative.writtenLanguage != "English" && representative.writtenLanguage == "Spanish" ? representative.writtenLanguage : string.Empty);

            pdfFiller.SetBool("RepresentativeEmail_Yes", representative.isheaPlusEmailNotify);
            pdfFiller.SetBool("RepresentativeEmail_No", !representative.isheaPlusEmailNotify);
            pdfFiller.SetText("RepresentativeEmailAddress", representative.isheaPlusEmailNotify ? representative.heaPlusEmailNotifyAddress : string.Empty);

            pdfFiller.SetBool("RepresentativeText_Yes", representative.isheaPlusTextNotify);
            pdfFiller.SetBool("RepresentativeText_No", !representative.isheaPlusTextNotify);
            pdfFiller.SetText("RepresentativeText_Number", representative.isheaPlusTextNotify ? representative.heaPlusTextNotifyNumber : string.Empty);

            pdfFiller.SetText("CustomerName", applicantInfo.personalInfo.detailedPersonalInfo.GetFullName());
            pdfFiller.SetText("RepresentativeName", representative.name);
            pdfFiller.SetText("WitnessName", representative.witnessName);

            pdfFiller.SaveToFile(targetFileName, true);

            return targetFileName;
        }
    }
}