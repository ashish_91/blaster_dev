﻿using ApplicantsApp.Models.Applicant.Applicant;
using ApplicantsApp.Models.Applicant.Applicant.Enums;
using ApplicantsApp.Models.Representative;
using System;
using System.Linq;

namespace ApplicantsApp.Source.FileGenerators
{
    public static class DE101
    {
        private const string de101TemplateFileName = @".\Documents\DE-101_english_2019_07_editable.pdf";

        /// <summary>
        /// Generates DE101 file
        /// </summary>
        /// <param name="targetFileName">The file which will be created</param>
        /// <param name="applicantInfo">The applicant info</param>
        /// <param name="representative">The representative info</param>
        /// <param name="logger">A logger</param>
        public static string Fill(
            FullApplicantInfo applicantInfo, 
            RepresentativeInfo representative, 
            Guid documentId,
            NLog.Logger logger)
        {
            string targetFileName = Shared.Source.Mop.GetTempFileName(
                null,
                $"de-101__{documentId.ToString()}.pdf"
                );

            var pdfFiller = new PdfFiller(de101TemplateFileName, logger);

            //CUSTOMER INFORMATION
            pdfFiller.SetText("CustomerLastFirstMiddle", applicantInfo.personalInfo.detailedPersonalInfo.GetFullName());

            pdfFiller.SetText("CustomerDateOfBirth", applicantInfo.personalInfo.detailedPersonalInfo.dateOfBirth.ToString(Resources.USADateOnly));
            pdfFiller.SetText("CustomerSOcialSecurityNumber", applicantInfo.personalInfo.detailedPersonalInfo.SSN);

            pdfFiller.SetText("SouseNames", applicantInfo.personalInfo.martialStatus.spouseFullName);
            pdfFiller.SetText("SouseBirth", (applicantInfo.personalInfo.martialStatus.spouseDateOfBirth ?? new DateTime(1900, 1, 1)).ToString(Resources.USADateOnly));
            pdfFiller.SetText("SouseSOcualSecurityNumber", applicantInfo.personalInfo.martialStatus.spouseSSN);

            pdfFiller.SetText("CustomerState", applicantInfo.personalInfo.applicantAddress.state);
            pdfFiller.SetText("CustomerCity", applicantInfo.personalInfo.applicantAddress.city);
            pdfFiller.SetText("CustomerHomeAddress", applicantInfo.personalInfo.applicantAddress.fullStreetAddress);
            pdfFiller.SetText("CustomerZipCode", applicantInfo.personalInfo.applicantAddress.zip);

            pdfFiller.SetText("CustomerMailingZipCode", applicantInfo.personalInfo.applicantMailingAddress.zip);
            pdfFiller.SetText("CustomerMailingState", applicantInfo.personalInfo.applicantMailingAddress.state);
            pdfFiller.SetText("CustomerMailingCity", applicantInfo.personalInfo.applicantMailingAddress.city);
            pdfFiller.SetText("CustomerMailingAddress", applicantInfo.personalInfo.applicantMailingAddress.fullStreetAddress);

            pdfFiller.SetText("CustomerPhoneNumber", applicantInfo.personalInfo.detailedPersonalInfo.phone);
            pdfFiller.SetText("CustomerEmailAddress", applicantInfo.personalInfo.detailedPersonalInfo.email);

            pdfFiller.SetBool("CustomerIsMale", applicantInfo.personalInfo.detailedPersonalInfo.gender == 1);
            pdfFiller.SetBool("CustomerIsFemale", applicantInfo.personalInfo.detailedPersonalInfo.gender == 2);

            pdfFiller.SetBool("MartialStatusWidowed", applicantInfo.personalInfo.martialStatus.martialStatus == "Widowed");
            pdfFiller.SetBool("MartialStatusMarried", applicantInfo.personalInfo.martialStatus.martialStatus == "Married");
            pdfFiller.SetBool("MartialStatusDivorced", applicantInfo.personalInfo.martialStatus.martialStatus == "Divorced");
            pdfFiller.SetBool("MartialStatusNeverMarried", applicantInfo.personalInfo.martialStatus.martialStatus == "Never Married");

            if (applicantInfo.personalInfo.martialStatus.martialStatus == "Widowed")
            {
                pdfFiller.SetText("MartialStatusDateOfSpouseDeath", (applicantInfo.personalInfo.martialStatus.daveOfMarriageDivorceOrWidowed ?? new DateTime(1900, 1, 1)).ToString(Resources.USADateOnly));
            }
            else
            {
                pdfFiller.SetText("MartialStatusDateOfSpouseDeath", string.Empty);
            }

            //Authorized Representative and Legal Guardian/Conservator Information            
            pdfFiller.SetText("AuthorizedRepresentativeNames", representative.name);
            pdfFiller.SetText("AuthorizedRepresentativeRelationship", representative.relationship);
            pdfFiller.SetText("LegalGuardianNames", representative.guardian.nameOfGuardian);
            pdfFiller.SetText("LegalGuardianRelationShip", representative.guardian.guardianRelationship);
            pdfFiller.SetText("AuthorizedRepresentativeMailingAddress", representative.address.fullStreetAddress);
            pdfFiller.SetText("AuthorizedRepresentativeCity", representative.address.city);
            pdfFiller.SetText("AuthorizedRepresentativeState", representative.address.state);
            pdfFiller.SetText("AuthorizedRepresentativeZipCode", representative.address.zip);
            pdfFiller.SetText("AuthorizedRepresentativePhoneNumber", representative.phoneNumber);
            pdfFiller.SetText("AuthorizedRepresentativeEmailAddress", representative.email);

            pdfFiller.SetText("LegalGuardianMailingAddress", representative.guardian.address.fullStreetAddress);
            pdfFiller.SetText("LegalGuardianCity", representative.guardian.address.city);
            pdfFiller.SetText("LegalGuardianState", representative.guardian.address.state);
            pdfFiller.SetText("LegalGuardianZipCode", representative.guardian.address.zip);
            pdfFiller.SetText("LegalGuardianPhoneNumber", representative.guardian.phoneNumber);
            pdfFiller.SetText("LegalGuardianEmailAddress", representative.guardian.emailAddress);

            //Customer’s Current Living Arrangement
            pdfFiller.SetBool("CustomerCurrentLiving_Hospital", applicantInfo.currentLivingArrangement.currentLivingType == CurrentLiving.Hospital);
            pdfFiller.SetBool("CustomerCurrentLiving_Nursing", applicantInfo.currentLivingArrangement.currentLivingType == CurrentLiving.Nursing);
            pdfFiller.SetBool("CustomerCurrentLiving_Home", applicantInfo.currentLivingArrangement.currentLivingType == CurrentLiving.Home);
            pdfFiller.SetBool("CustomerCurrentLiving_Other", applicantInfo.currentLivingArrangement.currentLivingType == CurrentLiving.Other);

            if (applicantInfo.currentLivingArrangement.currentLivingType == CurrentLiving.Other)
            {
                pdfFiller.SetText("CustomerCurrentLivingOther_Text", applicantInfo.currentLivingArrangement.currentLivingText);
            }
            else
            {
                pdfFiller.SetText("CustomerCurrentLivingOther_Text", string.Empty);
            }

            if (applicantInfo.currentLivingArrangement.currentLivingType == CurrentLiving.Nursing)
            {
                pdfFiller.SetText("CustomerCurrentLiving_ExpectedDateOfDischarge", applicantInfo.currentLivingArrangement.expectedDateOfDIscharge.ToString(Resources.USADateOnly));
            }
            else
            {
                pdfFiller.SetText("CustomerCurrentLiving_ExpectedDateOfDischarge", string.Empty);
            }

            pdfFiller.SetText("CustomerCurrentLiving_NameOfHospital", applicantInfo.currentLivingArrangement.facilityName);
            pdfFiller.SetText("CustomerCurrentLiving_PhoneNumber", applicantInfo.currentLivingArrangement.phoneNumber);
            pdfFiller.SetText("CustomerCurrentLiving_HospitalAddress", applicantInfo.currentLivingArrangement.address.fullStreetAddress);
            pdfFiller.SetText("CustomerCurrentLiving_City", applicantInfo.currentLivingArrangement.address.city);
            pdfFiller.SetText("CustomerCurrentLiving_State", applicantInfo.currentLivingArrangement.address.state);
            pdfFiller.SetText("CustomerCurrentLiving_ZipCode", applicantInfo.currentLivingArrangement.address.zip);

            //Accommodations for Printed Letters
            pdfFiller.SetBool("AccomodationsForPrintedLetters_Yes", applicantInfo.accommodationForPrintedLetters.isAlternativeFormatPrinterLetters);
            pdfFiller.SetBool("AccomodationsForPrintedLetters_No", !applicantInfo.accommodationForPrintedLetters.isAlternativeFormatPrinterLetters);            

            if (applicantInfo.accommodationForPrintedLetters.isAlternativeFormatPrinterLetters)
            {
                pdfFiller.SetText("AccomodationsForPrintedLetters_WhoIfYes", applicantInfo.accommodationForPrintedLetters.whoNeedsThis);
                pdfFiller.SetBool("AccomodationsForPrintedLetters_Email", applicantInfo.accommodationForPrintedLetters.isReadablePDF);
                pdfFiller.SetBool("AccomodationsForPrintedLetters_LargePrint", applicantInfo.accommodationForPrintedLetters.isLargePrint);
                pdfFiller.SetBool("AccomodationsForPrintedLetters_Other", applicantInfo.accommodationForPrintedLetters.isOther);

                if (applicantInfo.accommodationForPrintedLetters.isOther)
                {
                    pdfFiller.SetText("AccomodationsForPrintedLetters_OtherText", applicantInfo.accommodationForPrintedLetters.otherText);
                }
                else
                {
                    pdfFiller.SetText("AccomodationsForPrintedLetters_OtherText", string.Empty);
                }
            }
            else
            {
                pdfFiller.SetText("AccomodationsForPrintedLetters_WhoIfYes", string.Empty);
            }

            ///Additional Questions
            ///

            //1
            pdfFiller.SetBool("AdditionalQuestions_1_Yes", applicantInfo.additionalQuestions.isHelpNeededForPayingMedicalBills);
            pdfFiller.SetBool("AdditionalQuestions_1_No", !applicantInfo.additionalQuestions.isHelpNeededForPayingMedicalBills);

            if (applicantInfo.additionalQuestions.isHelpNeededForPayingMedicalBills)
            {
                pdfFiller.SetText("AdditionalQuestions_1_Yes_Month1", 
                    applicantInfo.additionalQuestions.helpPayingMedicalBillsMonths.Count() > 0 ? 
                        applicantInfo.additionalQuestions.helpPayingMedicalBillsMonths[0] : 
                        string.Empty);

                pdfFiller.SetText("AdditionalQuestions_1_Yes_Month2",
                    applicantInfo.additionalQuestions.helpPayingMedicalBillsMonths.Count() > 1 ?
                        applicantInfo.additionalQuestions.helpPayingMedicalBillsMonths[1] :
                        string.Empty);

                pdfFiller.SetText("AdditionalQuestions_1_Yes_Month3",
                    applicantInfo.additionalQuestions.helpPayingMedicalBillsMonths.Count() > 2 ?
                        applicantInfo.additionalQuestions.helpPayingMedicalBillsMonths[2] :
                        string.Empty);
            }
            else
            {
                pdfFiller.SetText("AdditionalQuestions_1_Yes_Month1", String.Empty);
                pdfFiller.SetText("AdditionalQuestions_1_Yes_Month2", String.Empty);
                pdfFiller.SetText("AdditionalQuestions_1_Yes_Month3", String.Empty);
            }

            //2
            pdfFiller.SetBool("AdditionalQuestions_2_Yes", applicantInfo.additionalQuestions.isReceiveServiceFromDES);
            pdfFiller.SetBool("AdditionalQuestions_2_No", !applicantInfo.additionalQuestions.isReceiveServiceFromDES);

            if (applicantInfo.additionalQuestions.isReceiveServiceFromDES)
            {
                pdfFiller.SetText("AdditionalQuestions_2_Yes_Date", applicantInfo.additionalQuestions.serviceBegan.ToString(Resources.USADateOnly));
            }
            else
            {
                pdfFiller.SetText("AdditionalQuestions_2_Yes_Date", string.Empty);
            }

            //3
            pdfFiller.SetBool("AdditionalQuestions_3_Autism", applicantInfo.additionalQuestions.isBef18Autism);
            pdfFiller.SetBool("AdditionalQuestions_3_CerebralPalsy", applicantInfo.additionalQuestions.isBef18Cerebral);
            pdfFiller.SetBool("AdditionalQuestions_3_Intellectual", applicantInfo.additionalQuestions.isBef18IntelectualDisability);
            pdfFiller.SetBool("AdditionalQuestions_3_Seizure", applicantInfo.additionalQuestions.isBef18SeizureDisorder);

            //4
            pdfFiller.SetBool("AdditionalQuestions_4_Yes", applicantInfo.additionalQuestions.isDevelopmentDelayBefore6);
            pdfFiller.SetBool("AdditionalQuestions_4_No", !applicantInfo.additionalQuestions.isDevelopmentDelayBefore6);

            //5
            pdfFiller.SetBool("AdditionalQuestions_5_Yes", applicantInfo.additionalQuestions.isTrustor);
            pdfFiller.SetBool("AdditionalQuestions_5_No", !applicantInfo.additionalQuestions.isTrustor);

            //6
            pdfFiller.SetBool("AdditionalQuestions_6_Yes", applicantInfo.lookBackPeriod.isHomeSold);
            pdfFiller.SetBool("AdditionalQuestions_6_No", !applicantInfo.lookBackPeriod.isHomeSold);

            //Pregnant
            pdfFiller.SetBool("IsPregnant_CustomerYes", applicantInfo.additionalQuestions.isPregnant);
            pdfFiller.SetBool("IsPregnant_CustomerNo", !applicantInfo.additionalQuestions.isPregnant);

            ///Interview Information
            pdfFiller.SetBool("Interview_Monday", applicantInfo.interviewInfo.isMonday);
            pdfFiller.SetBool("Interview_Tuesday", applicantInfo.interviewInfo.isTuesday);
            pdfFiller.SetBool("Interview_Wednesday", applicantInfo.interviewInfo.isWednesday);
            pdfFiller.SetBool("Interview_Thursday", applicantInfo.interviewInfo.isThursday);
            pdfFiller.SetBool("Interview_Friday", applicantInfo.interviewInfo.isFriday);

            pdfFiller.SetText("Interview_Monday_Time", applicantInfo.interviewInfo.isMonday ? applicantInfo.interviewInfo.mondayTime : string.Empty);
            pdfFiller.SetText("Interview_Tuesday_Time", applicantInfo.interviewInfo.isTuesday ? applicantInfo.interviewInfo.tuesdayTime : string.Empty);
            pdfFiller.SetText("Interview_Wednesday_Time", applicantInfo.interviewInfo.isWednesday ? applicantInfo.interviewInfo.wednesdayTime : string.Empty);
            pdfFiller.SetText("Interview_Thursday_Time", applicantInfo.interviewInfo.isThursday ? applicantInfo.interviewInfo.thursdayTime : string.Empty);
            pdfFiller.SetText("Interview_Friday_Time", applicantInfo.interviewInfo.isFriday ? applicantInfo.interviewInfo.fridayTime : string.Empty);

            pdfFiller.SetBool("InterviewInterpretter_Yes", applicantInfo.interviewInfo.isInterpreterNeeded);
            pdfFiller.SetBool("InterviewInterpretter_No", !applicantInfo.interviewInfo.isInterpreterNeeded);
            pdfFiller.SetText("InterviewInterpretter_Language", applicantInfo.interviewInfo.isInterpreterNeeded ? applicantInfo.interviewInfo.interpreterLanguage : string.Empty);

            pdfFiller.SetText("InterviewInterpretter_HomeAddress", applicantInfo.interviewInfo.interpreterHomeAddress);
            pdfFiller.SetText("InterviewInterpretter_MajorCrossroads", applicantInfo.interviewInfo.interpreterMajorCrossRoads);

            pdfFiller.SetText("PersonCompleteingTheForm_Name", applicantInfo.personCompletingDeForm.name);
            pdfFiller.SetText("PersonCompleteingTheForm_Phone", applicantInfo.personCompletingDeForm.phone);

            pdfFiller.SetBool("PersonCompleteingTheForm_IsCustomer", applicantInfo.personCompletingDeForm.whoIsCompletingTheForm == CompletingFormPersonType.Customer);
            pdfFiller.SetBool("PersonCompleteingTheForm_IsSpouse", applicantInfo.personCompletingDeForm.whoIsCompletingTheForm == CompletingFormPersonType.Spouse);
            pdfFiller.SetBool("PersonCompleteingTheForm_IsParent", applicantInfo.personCompletingDeForm.whoIsCompletingTheForm == CompletingFormPersonType.Parent);

            pdfFiller.SaveToFile(targetFileName, true);

            return targetFileName;
        }
    }
}