﻿using ApplicantsApp.Models.Representative;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApplicantsApp.Source.FileGenerators
{
    public class Medical
    {
        private const string medicalTemplate = @".\Documents\HIPPA Medical Record Auth 06-10-2019_placeholders.docx";
        private Xceed.Words.NET.DocX docX;

        public string Fill(
            Models.Applicant.Applicant.FullApplicantInfo applicantInfo,
            RepresentativeInfo representative,
            List<ApplicantsDoctors> doctors,
            Guid documentId,
            NLog.Logger logger)
        {
            string targetFileName = Shared.Source.Mop.GetTempFileName(
                "",
                $"medical__{documentId.ToString()}.docx"
                );

            string docxFileName = medicalTemplate;
            docX = Xceed.Words.NET.DocX.Load(docxFileName);

            //patient info
            docX.ReplaceText("{{PatientName}}", applicantInfo.personalInfo.detailedPersonalInfo.GetFullName());
            docX.ReplaceText("{{DateOfBirth}}", applicantInfo.personalInfo.detailedPersonalInfo.dateOfBirth.ToString(Resources.USADateOnly));
            docX.ReplaceText("{{SSN}}", applicantInfo.personalInfo.detailedPersonalInfo.SSN);

            //filling the doctor's names and other stuff
            var doctorsString = new StringBuilder();
            foreach (Guid doctorId in applicantInfo.authorizedPartiesIds)
            {                
                ApplicantsDoctors doctor = doctors.SingleOrDefault(s => s.Id == doctorId);
                if (doctor == null)
                {
                    doctorsString.AppendLine("Auth party (physician or doctor) deleted!");
                }
                else
                {
                    doctorsString.AppendLine($"MD Practice name: {doctor.Name}, phone: {doctor.OfficeTelephoneNumber}, fax: {doctor.OfficeFax}");
                }
            }
            HandleText("{{AuthParties}}", true, doctorsString.ToString());

            //auth conditions
            HandleCheck("{{TickHelth}}", applicantInfo.disclosedInformation.isAllInfo);

            HandleCheck("{{THelthCon}}", applicantInfo.disclosedInformation.isCertainCondition);
            HandleText("{{HelthCondition}}", applicantInfo.disclosedInformation.isCertainCondition, applicantInfo.disclosedInformation.theCondition);

            HandleCheck("{{THelthPeriod}}", applicantInfo.disclosedInformation.isForPeriod);
            HandleText("{{HePerFr}}", applicantInfo.disclosedInformation.isForPeriod, applicantInfo.disclosedInformation.periodFrom.ToString(Resources.USADateOnly));
            HandleText("{{HePerTo}}", applicantInfo.disclosedInformation.isForPeriod, applicantInfo.disclosedInformation.periodTo.ToString(Resources.USADateOnly));

            HandleCheck("{{TickOther}}", applicantInfo.disclosedInformation.isOther);
            HandleText("{{OtherText}}", applicantInfo.disclosedInformation.isOther, applicantInfo.disclosedInformation.otherText);

            docX.ReplaceText("{{email}}", applicantInfo.disclosedInformation.disclosedToEmail);

            //Purpose
            HandleCheck("{{TickPurpose_MyRequest}}", applicantInfo.authorizationPurpose.isAtMyRequest);
            HandleCheck("{{TickPurpose_Other}}", applicantInfo.authorizationPurpose.isOther);
            HandleText("{{Purpose_OtherText}}", applicantInfo.authorizationPurpose.isOther, applicantInfo.authorizationPurpose.otherText);
            HandleCheck("{{TickPurpose_AuthParty}}", applicantInfo.authorizationPurpose.authToCommunicate);
            HandleCheck("{{TickPurpose_AuthPartySell}}", applicantInfo.authorizationPurpose.authToSell);

            //end
            HandleCheck("{{TickAuth_EndsOnDate}}", applicantInfo.endOfAuth.isOnDate);
            HandleText("{{Auth_EndsonDate_Text}}", applicantInfo.endOfAuth.isOnDate, applicantInfo.endOfAuth.endOn.ToString(Resources.USADateOnly));

            HandleCheck("{{TickAuth_Ends_Event}}", applicantInfo.endOfAuth.isOnEvent);
            HandleText("{{Auth_Ends_Event_Text}}", applicantInfo.endOfAuth.isOnEvent, applicantInfo.endOfAuth.theEvent);

            //rights
            docX.ReplaceText("{{Event_Date1}}", applicantInfo.patientRights.patientSignatureDate.ToString(Resources.USADateOnly));

            HandleCheck("{{TickPatientMinor}}", applicantInfo.patientRights.isPatientMinor);
            HandleText("{{TickPatientMinorAge}}", applicantInfo.patientRights.isPatientMinor, applicantInfo.patientRights.patientMinorAge);

            HandleCheck("{{TickPatientNoSign}}", applicantInfo.patientRights.isUnableToSign);
            HandleText("{{TickPatientNoSign_Why}}", applicantInfo.patientRights.isUnableToSign, applicantInfo.patientRights.unableToSignBecause);

            docX.ReplaceText("{{Event_Date2}}", applicantInfo.patientRights.authRepSignDate.ToString(Resources.USADateOnly));
            docX.ReplaceText("{{AuthRep}}", representative.name);

            //Authority of representative to sign on behalf of the patient
            HandleCheck("{{T1}}", applicantInfo.patientRights.isRepParent);
            HandleCheck("{{T2}}", applicantInfo.patientRights.isRepGuardian);
            HandleCheck("{{T3}}", applicantInfo.patientRights.isRepCourtOrder);
            HandleCheck("{{T4}}", applicantInfo.patientRights.isRepOther);
            HandleText("{{T4_Text}}", applicantInfo.patientRights.isRepOther, applicantInfo.patientRights.repAuthOtherText);

            //III. Additional Consent for Certain Conditions
            HandleCheck("{{T5}}", applicantInfo.additionalConsent.allowRelease);
            HandleCheck("{{T6}}", applicantInfo.additionalConsent.notAllowRelease);

            docX.ReplaceText("{{Event_Date3}}", applicantInfo.additionalConsent.date.ToString(Resources.USADateOnly));
            docX.ReplaceText("{{Event_Time3}}", applicantInfo.additionalConsent.date.ToString(Resources.USATimeOnly));

            //IV. Additional Consent for HIV/AIDS
            HandleCheck("{{T7}}", applicantInfo.additionalConsentForHIV.allowRelease);
            HandleCheck("{{T8}}", applicantInfo.additionalConsentForHIV.notAllowRelease);

            docX.ReplaceText("{{Event_Date4}}", applicantInfo.additionalConsentForHIV.date.ToString(Resources.USADateOnly));
            docX.ReplaceText("{{Event_Time4}}", applicantInfo.additionalConsentForHIV.date.ToString(Resources.USATimeOnly));

            docX.SaveAs(targetFileName);

            return targetFileName;
        }

        private void HandleCheck(string placeholder, bool value)
        {
            docX.ReplaceText(placeholder, value ? "☑" : "☐");
        }

        private void HandleText(string placeholder, bool predicate, string text)
        {
            docX.ReplaceText(placeholder, predicate ? text : "");
        }
    }
}
