﻿using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Source
{
    public class PdfFiller
    {
        public string PdfFieldCheckSetConst { get; set; } = "Yes";
        public string PdfFieldCheckNotSetConst { get; set; } = "Off";

        private PdfReader reader;
        private MemoryStream outStream;
        private PdfStamper stamper;
        private AcroFields form;
        private ICollection<string> fieldKeys;
        private NLog.Logger logger;

        public PdfFiller(string pdfFileName, NLog.Logger logger)
        {
            this.logger = logger;

            logger.Info($"Loading {pdfFileName}");

            reader = new PdfReader(pdfFileName);
            outStream = new MemoryStream();
            stamper = new PdfStamper(reader, outStream);

            form = stamper.AcroFields;
            fieldKeys = form.Fields.Keys;
        }

        ~PdfFiller()
        {
            try
            {
                logger.Info($"Freeing the pdf stuff");
                if (reader != null) reader.Close();
                if (outStream != null) outStream.Close();
                if (stamper != null) stamper.Close();
                logger.Info($"Done");
            }
            catch(Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void SetBool(string pdfFieldName, bool isChecked)
        {
            string[] checkboxstates = form.GetAppearanceStates(pdfFieldName);

            string setTo;


            if (
                checkboxstates.Any(a => a == PdfFieldCheckSetConst)
                && checkboxstates.Any(a => a == PdfFieldCheckNotSetConst)
                && checkboxstates.Length == 2
                )
            {
                setTo = isChecked ? PdfFieldCheckSetConst : PdfFieldCheckNotSetConst;
                SetText(pdfFieldName, setTo);
            }
            else

            if (
                checkboxstates.Any(a => a == "On")
                && checkboxstates.Length == 1
                )
            {
                if (isChecked)
                {
                    SetText(pdfFieldName, "On");
                }
            }
            else
            {
                throw new Exception($"For PDF Field with name: '{pdfFieldName}' checkBox states are as not expected: {string.Join(",", checkboxstates)}");
            }

        }

        public void SetText(string pdfFieldName, string text)
        {
            form.SetField(pdfFieldName, text);
        }

        public void SaveToFile(string fileName, bool formFlattening)
        {
            stamper.FormFlattening = formFlattening;

            stamper.Close();
            reader.Close();

            File.WriteAllBytes(fileName, outStream.ToArray());
            outStream.Close();
        }
    }
}
