﻿using ApplicantsApp.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp.Source
{
    public static class Mop
    {
        private const string TempDirName = "temp";

        public static Guid? GetComboValue(ComboBox combo)
        {
            if (combo is null || combo.SelectedItem is null) return null;
            return (Guid)((ComboboxItem)combo.SelectedItem).Value;
        }

        public static void SetComboItemByValue(ComboBox combo, Guid? value)
        {
            if (!value.HasValue || value.Value.Equals(Guid.Empty))
            {
                combo.SelectedItem = null;
                return;
            }

            foreach (object item in combo.Items)
            {
                if (((Guid)((ComboboxItem)item).Value).Equals(value))
                {
                    combo.SelectedItem = item;
                    break;
                }
            }
        }


        public static bool ValidateChildControls(Control parentControl)
        {
            foreach (Control control in parentControl.Controls)
            {
                if (control.Tag != null && (string)control.Tag == "1" && string.IsNullOrWhiteSpace(control.Text))
                {
                    MessageBox.Show($"Filling '{control.AccessibleDescription}' is mandatory!");
                    if (control.CanFocus)
                        control.Focus();

                    return false;
                }

                if (control.HasChildren)
                {
                    if (!ValidateChildControls(control))
                    {
                        return false;
                    }
                }
            }

            return true;

        }

        public static bool ValidateControl(bool predicate, string controlName, Control control)
        {
            if (predicate)
            {
                MessageBox.Show($"Filling '{controlName}' is mandatory!");

                if (control.CanFocus)
                    control.Focus();

                return false;
            }

            return true;
        }

        private static char[] invalidFileChars = Path.GetInvalidFileNameChars();
        public static string MakeSafeFilename(string filename, char replaceChar = '_')
        {
            foreach (char c in invalidFileChars)
            {
                filename = filename.Replace(c, replaceChar);
            }
            return filename;
        }


        public static bool IsEmailValid(string email, bool treatEmptyAsOkay = false)
        {
            if (String.IsNullOrWhiteSpace(email)) return treatEmptyAsOkay;
            if (email.Length < 6) return false;
            if (email.IndexOf("@") <= 0) return false;
            if (email.IndexOf(".") <= 0) return false;
            if (email.EndsWith(".")) return false;
            if (email.EndsWith("@")) return false;
            if (email.StartsWith("@")) return false;

            return true;
        }

        public static string GetTempFileName(string fileExtension)
        {
            if (!Directory.Exists(TempDirName))
                Directory.CreateDirectory(TempDirName);

            return Path.Combine(TempDirName, Guid.NewGuid().ToString() + fileExtension);
        }
    }


}
