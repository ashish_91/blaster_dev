﻿using ApplicantsApp.Models;
using ApplicantsApp.Models.Applicant.Applicant;
using ApplicantsApp.Source;
using Newtonsoft.Json;
using Shared;
using System;
using System.Windows.Forms;

namespace ApplicantsApp.Forms
{
    public partial class FormScheduleUser : Form
    {
        private NLog.Logger logger = NLog.LogManager.GetLogger("schedule-user");
        public FormModes FormMode { get; set; }
        public ApplicantsNew ApplicantData { get; set; }
        public bool NeedRefresh { get; private set; } = false;
        private Progress progress;

        private ApplicantScheduleInfo applicantScheduleInfo;

        public FormScheduleUser()
        {
            InitializeComponent();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textWorkerName.Text))
            {
                MessageBox.Show("The worker name is required!");
                if (textWorkerName.CanFocus)
                    textWorkerName.Focus();

                return;
            }

            if (string.IsNullOrWhiteSpace(textWorkerPhone.Text))
            {
                MessageBox.Show("The worker phone is required!");
                if (textWorkerPhone.CanFocus)
                    textWorkerPhone.Focus();

                return;
            }

            applicantScheduleInfo = new ApplicantScheduleInfo()
            {
                PostponeDateTime = datePostponeDate.Value.Date +
                    new TimeSpan(
                        datePostponeTime.Value.Hour,
                        datePostponeTime.Value.Minute,
                        datePostponeTime.Value.Second),
                WorkerEmailAddress = textWorkerEmail.Text,
                WorkerFax = textWorkerFax.Text,
                WorkerName = textWorkerName.Text,
                WorkerPhone = textWorkerPhone.Text
            };

            Db.UpdateApplicantScheduledInfo(ApplicantData.Id, JsonConvert.SerializeObject(applicantScheduleInfo), logger);
            AddAuditEvent(ReportActions.ScheduleAdd, applicantScheduleInfo);

            NeedRefresh = true;
            DialogResult = DialogResult.OK;
        }

        private void FormScheduleUser_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (NeedRefresh)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                DialogResult = DialogResult.Cancel;
            }
        }

        private void FormScheduleUser_Load(object sender, EventArgs e)
        {
            progress = new Progress(progressBar1, lblProgress, this, logger);
        }

        private void TimerFormLoad_Tick(object sender, EventArgs e)
        {
            timerFormLoad.Enabled = false;

            if (!string.IsNullOrWhiteSpace(ApplicantData.ScheduledInfo))
            {

                progress.ShowProgressBar("Loading scheduled info...");
                try
                {

                    applicantScheduleInfo = JsonConvert.DeserializeObject<ApplicantScheduleInfo>(ApplicantData.ScheduledInfo);
                    AddAuditEvent(ReportActions.ScheduleShowToUser, applicantScheduleInfo);

                    datePostponeDate.Value = applicantScheduleInfo.PostponeDateTime.Date;
                    datePostponeTime.Value = applicantScheduleInfo.PostponeDateTime;

                    textWorkerName.Text = applicantScheduleInfo.WorkerName;
                    textWorkerFax.Text = applicantScheduleInfo.WorkerFax;
                    textWorkerPhone.Text = applicantScheduleInfo.WorkerPhone;
                    textWorkerEmail.Text = applicantScheduleInfo.WorkerEmailAddress;

                }
                finally
                {
                    progress.HideProgressBar();
                }
            }
        }

        private void AddAuditEvent(ReportActions reportAction, object data)
        {
            progress.SetProgressText($"Writing audit event: {reportAction}");
            Db.InsertAction(
                logger,
                ApplicantData.Id,
                reportAction,
                null,
                data: JsonConvert.SerializeObject(data)
            );
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
