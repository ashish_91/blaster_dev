﻿namespace ApplicantsApp.Forms
{
    partial class FormCategoriesRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCategoriesRegister));
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.btnExit = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolBtnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolBtnEdit = new System.Windows.Forms.ToolStripButton();
            this.toolBtnDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.gridCategories = new System.Windows.Forms.DataGridView();
            this.ColCatId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCatName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCatPriority = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCatDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelProgress = new System.Windows.Forms.Label();
            this.menuStripAdd = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCategories)).BeginInit();
            this.menuStripAdd.SuspendLayout();
            this.SuspendLayout();
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(12, 421);
            this.progressBar.MarqueeAnimationSpeed = 30;
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(695, 23);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.TabIndex = 23;
            this.progressBar.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Location = new System.Drawing.Point(713, 421);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 22;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolBtnAdd,
            this.toolBtnEdit,
            this.toolBtnDelete,
            this.toolStripSeparator3});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 39);
            this.toolStrip1.TabIndex = 21;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolBtnAdd
            // 
            this.toolBtnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnAdd.Image")));
            this.toolBtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnAdd.Name = "toolBtnAdd";
            this.toolBtnAdd.Size = new System.Drawing.Size(36, 36);
            this.toolBtnAdd.Text = "toolStripButton1";
            this.toolBtnAdd.ToolTipText = "New Doctor or Psysician";
            this.toolBtnAdd.Click += new System.EventHandler(this.toolBtnAdd_Click);
            // 
            // toolBtnEdit
            // 
            this.toolBtnEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnEdit.Enabled = false;
            this.toolBtnEdit.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnEdit.Image")));
            this.toolBtnEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnEdit.Name = "toolBtnEdit";
            this.toolBtnEdit.Size = new System.Drawing.Size(36, 36);
            this.toolBtnEdit.ToolTipText = "Edit Doctor or Psysician";
            this.toolBtnEdit.Click += new System.EventHandler(this.toolBtnEdit_Click);
            // 
            // toolBtnDelete
            // 
            this.toolBtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnDelete.Enabled = false;
            this.toolBtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnDelete.Image")));
            this.toolBtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnDelete.Name = "toolBtnDelete";
            this.toolBtnDelete.Size = new System.Drawing.Size(36, 36);
            this.toolBtnDelete.Text = "toolStripButton3";
            this.toolBtnDelete.ToolTipText = "Delete Doctor or Psysician";
            this.toolBtnDelete.Click += new System.EventHandler(this.toolBtnDelete_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // gridCategories
            // 
            this.gridCategories.AllowUserToAddRows = false;
            this.gridCategories.AllowUserToDeleteRows = false;
            this.gridCategories.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridCategories.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridCategories.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColCatId,
            this.ColCatName,
            this.ColCatPriority,
            this.ColCatDesc});
            this.gridCategories.ContextMenuStrip = this.menuStripAdd;
            this.gridCategories.Location = new System.Drawing.Point(12, 48);
            this.gridCategories.Name = "gridCategories";
            this.gridCategories.ReadOnly = true;
            this.gridCategories.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridCategories.Size = new System.Drawing.Size(776, 367);
            this.gridCategories.TabIndex = 20;
            this.gridCategories.SelectionChanged += new System.EventHandler(this.gridCategories_SelectionChanged);
            // 
            // ColCatId
            // 
            this.ColCatId.HeaderText = "Id";
            this.ColCatId.Name = "ColCatId";
            this.ColCatId.ReadOnly = true;
            this.ColCatId.Visible = false;
            // 
            // ColCatName
            // 
            this.ColCatName.HeaderText = "Category Name";
            this.ColCatName.Name = "ColCatName";
            this.ColCatName.ReadOnly = true;
            this.ColCatName.Width = 200;
            // 
            // ColCatPriority
            // 
            this.ColCatPriority.HeaderText = "Priority";
            this.ColCatPriority.Name = "ColCatPriority";
            this.ColCatPriority.ReadOnly = true;
            this.ColCatPriority.Width = 60;
            // 
            // ColCatDesc
            // 
            this.ColCatDesc.HeaderText = "Cateogry Desc";
            this.ColCatDesc.Name = "ColCatDesc";
            this.ColCatDesc.ReadOnly = true;
            this.ColCatDesc.Width = 350;
            // 
            // labelProgress
            // 
            this.labelProgress.AutoSize = true;
            this.labelProgress.Location = new System.Drawing.Point(20, 427);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(58, 13);
            this.labelProgress.TabIndex = 24;
            this.labelProgress.Text = "                 ";
            // 
            // menuStripAdd
            // 
            this.menuStripAdd.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.menuStripEdit,
            this.menuStripDelete});
            this.menuStripAdd.Name = "menuStripAdd";
            this.menuStripAdd.Size = new System.Drawing.Size(181, 92);
            this.menuStripAdd.Opening += new System.ComponentModel.CancelEventHandler(this.menuStripAdd_Opening);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.addToolStripMenuItem.Text = "Add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // menuStripEdit
            // 
            this.menuStripEdit.Name = "menuStripEdit";
            this.menuStripEdit.Size = new System.Drawing.Size(180, 22);
            this.menuStripEdit.Text = "Edit";
            this.menuStripEdit.Click += new System.EventHandler(this.menuStripEdit_Click);
            // 
            // menuStripDelete
            // 
            this.menuStripDelete.Name = "menuStripDelete";
            this.menuStripDelete.Size = new System.Drawing.Size(180, 22);
            this.menuStripDelete.Text = "Delete";
            this.menuStripDelete.Click += new System.EventHandler(this.menuStripDelete_Click);
            // 
            // FormCategoriesRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.gridCategories);
            this.Name = "FormCategoriesRegister";
            this.Text = "Categories Register";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormCategoriesRegister_FormClosing);
            this.Load += new System.EventHandler(this.FormCategoriesRegister_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCategories)).EndInit();
            this.menuStripAdd.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolBtnAdd;
        private System.Windows.Forms.ToolStripButton toolBtnEdit;
        private System.Windows.Forms.ToolStripButton toolBtnDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.DataGridView gridCategories;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCatId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCatName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCatPriority;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCatDesc;
        private System.Windows.Forms.Label labelProgress;
        private System.Windows.Forms.ContextMenuStrip menuStripAdd;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuStripEdit;
        private System.Windows.Forms.ToolStripMenuItem menuStripDelete;
    }
}