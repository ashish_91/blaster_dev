﻿namespace ApplicantsApp.Forms
{
    partial class FormStripeCustomerSubscriptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormStripeCustomerSubscriptions));
            this.label5 = new System.Windows.Forms.Label();
            this.textStartDay = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPaymentMethods = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtStripeEmail = new System.Windows.Forms.TextBox();
            this.txtStripeCustomerName = new System.Windows.Forms.TextBox();
            this.btnShowCustomer = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboStatus = new System.Windows.Forms.ComboBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnHellpInitialAmount = new System.Windows.Forms.Button();
            this.textInitialAmount = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textTotalSum = new System.Windows.Forms.NumericUpDown();
            this.labelWarning = new System.Windows.Forms.Label();
            this.btnShowPlan = new System.Windows.Forms.Button();
            this.btnShowProduct = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.comboPlans = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboProducts = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtSave = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnRefresh = new System.Windows.Forms.Button();
            this.comboApplicants = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupAddCharge = new System.Windows.Forms.GroupBox();
            this.txtChargeReason = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btnAddCharge = new System.Windows.Forms.Button();
            this.txtChargeAmmount = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.textStartDay)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textInitialAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textTotalSum)).BeginInit();
            this.groupAddCharge.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtChargeAmmount)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Start date:";
            // 
            // textStartDay
            // 
            this.textStartDay.Location = new System.Drawing.Point(76, 23);
            this.textStartDay.Maximum = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.textStartDay.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.textStartDay.Name = "textStartDay";
            this.textStartDay.Size = new System.Drawing.Size(53, 20);
            this.textStartDay.TabIndex = 5;
            this.textStartDay.Value = new decimal(new int[] {
            17,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(129, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 15);
            this.label6.TabIndex = 6;
            this.label6.Text = "th of every month";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox1.Controls.Add(this.txtPaymentMethods);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtStripeEmail);
            this.groupBox1.Controls.Add(this.txtStripeCustomerName);
            this.groupBox1.Controls.Add(this.btnShowCustomer);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(506, 124);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Stripe Customer Info";
            // 
            // txtPaymentMethods
            // 
            this.txtPaymentMethods.Enabled = false;
            this.txtPaymentMethods.Location = new System.Drawing.Point(24, 91);
            this.txtPaymentMethods.Name = "txtPaymentMethods";
            this.txtPaymentMethods.ReadOnly = true;
            this.txtPaymentMethods.Size = new System.Drawing.Size(473, 20);
            this.txtPaymentMethods.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(109, 15);
            this.label9.TabIndex = 11;
            this.label9.Text = "Payment methods:";
            // 
            // txtStripeEmail
            // 
            this.txtStripeEmail.Enabled = false;
            this.txtStripeEmail.Location = new System.Drawing.Point(281, 41);
            this.txtStripeEmail.Name = "txtStripeEmail";
            this.txtStripeEmail.ReadOnly = true;
            this.txtStripeEmail.Size = new System.Drawing.Size(189, 20);
            this.txtStripeEmail.TabIndex = 10;
            // 
            // txtStripeCustomerName
            // 
            this.txtStripeCustomerName.Enabled = false;
            this.txtStripeCustomerName.Location = new System.Drawing.Point(24, 41);
            this.txtStripeCustomerName.Name = "txtStripeCustomerName";
            this.txtStripeCustomerName.ReadOnly = true;
            this.txtStripeCustomerName.Size = new System.Drawing.Size(251, 20);
            this.txtStripeCustomerName.TabIndex = 9;
            // 
            // btnShowCustomer
            // 
            this.btnShowCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnShowCustomer.Location = new System.Drawing.Point(473, 40);
            this.btnShowCustomer.Name = "btnShowCustomer";
            this.btnShowCustomer.Size = new System.Drawing.Size(24, 22);
            this.btnShowCustomer.TabIndex = 8;
            this.btnShowCustomer.Text = "S";
            this.btnShowCustomer.UseVisualStyleBackColor = true;
            this.btnShowCustomer.Click += new System.EventHandler(this.btnShowCustomer_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(278, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Stripe recipient e-mail:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Applicant/Stripe Customer Name:";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox2.Controls.Add(this.comboStatus);
            this.groupBox2.Controls.Add(this.lblStatus);
            this.groupBox2.Controls.Add(this.btnHellpInitialAmount);
            this.groupBox2.Controls.Add(this.textInitialAmount);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.textTotalSum);
            this.groupBox2.Controls.Add(this.labelWarning);
            this.groupBox2.Controls.Add(this.btnShowPlan);
            this.groupBox2.Controls.Add(this.btnShowProduct);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.comboPlans);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.comboProducts);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textStartDay);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(12, 174);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(506, 158);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Recurring Inovice Info  (all this is read from stripe with their API)";
            // 
            // comboStatus
            // 
            this.comboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboStatus.FormattingEnabled = true;
            this.comboStatus.Location = new System.Drawing.Point(381, 76);
            this.comboStatus.Name = "comboStatus";
            this.comboStatus.Size = new System.Drawing.Size(116, 21);
            this.comboStatus.TabIndex = 23;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(317, 76);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(41, 15);
            this.lblStatus.TabIndex = 22;
            this.lblStatus.Text = "Status";
            // 
            // btnHellpInitialAmount
            // 
            this.btnHellpInitialAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnHellpInitialAmount.Location = new System.Drawing.Point(281, 101);
            this.btnHellpInitialAmount.Name = "btnHellpInitialAmount";
            this.btnHellpInitialAmount.Size = new System.Drawing.Size(24, 22);
            this.btnHellpInitialAmount.TabIndex = 21;
            this.btnHellpInitialAmount.Text = "?";
            this.btnHellpInitialAmount.UseVisualStyleBackColor = true;
            this.btnHellpInitialAmount.Click += new System.EventHandler(this.btnHellpInitialAmount_Click);
            // 
            // textInitialAmount
            // 
            this.textInitialAmount.DecimalPlaces = 2;
            this.textInitialAmount.Location = new System.Drawing.Point(98, 102);
            this.textInitialAmount.Maximum = new decimal(new int[] {
            2000000001,
            0,
            0,
            0});
            this.textInitialAmount.Name = "textInitialAmount";
            this.textInitialAmount.Size = new System.Drawing.Size(180, 20);
            this.textInitialAmount.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(5, 104);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 17);
            this.label10.TabIndex = 19;
            this.label10.Text = "Payed so far:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(87, 104);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(14, 15);
            this.label11.TabIndex = 18;
            this.label11.Text = "$";
            // 
            // textTotalSum
            // 
            this.textTotalSum.DecimalPlaces = 2;
            this.textTotalSum.Enabled = false;
            this.textTotalSum.Location = new System.Drawing.Point(381, 51);
            this.textTotalSum.Maximum = new decimal(new int[] {
            2000000001,
            0,
            0,
            0});
            this.textTotalSum.Name = "textTotalSum";
            this.textTotalSum.Size = new System.Drawing.Size(116, 20);
            this.textTotalSum.TabIndex = 17;
            // 
            // labelWarning
            // 
            this.labelWarning.AutoSize = true;
            this.labelWarning.ForeColor = System.Drawing.Color.DarkRed;
            this.labelWarning.Location = new System.Drawing.Point(237, 27);
            this.labelWarning.Name = "labelWarning";
            this.labelWarning.Size = new System.Drawing.Size(0, 15);
            this.labelWarning.TabIndex = 16;
            // 
            // btnShowPlan
            // 
            this.btnShowPlan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnShowPlan.Location = new System.Drawing.Point(281, 76);
            this.btnShowPlan.Name = "btnShowPlan";
            this.btnShowPlan.Size = new System.Drawing.Size(24, 22);
            this.btnShowPlan.TabIndex = 15;
            this.btnShowPlan.Text = "S";
            this.btnShowPlan.UseVisualStyleBackColor = true;
            this.btnShowPlan.Click += new System.EventHandler(this.btnShowPlan_Click);
            // 
            // btnShowProduct
            // 
            this.btnShowProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnShowProduct.Location = new System.Drawing.Point(281, 49);
            this.btnShowProduct.Name = "btnShowProduct";
            this.btnShowProduct.Size = new System.Drawing.Size(24, 22);
            this.btnShowProduct.TabIndex = 14;
            this.btnShowProduct.Text = "S";
            this.btnShowProduct.UseVisualStyleBackColor = true;
            this.btnShowProduct.Click += new System.EventHandler(this.btnShowProduct_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 15);
            this.label8.TabIndex = 13;
            this.label8.Text = "$ per month:";
            // 
            // comboPlans
            // 
            this.comboPlans.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboPlans.FormattingEnabled = true;
            this.comboPlans.Location = new System.Drawing.Point(76, 76);
            this.comboPlans.Name = "comboPlans";
            this.comboPlans.Size = new System.Drawing.Size(202, 21);
            this.comboPlans.TabIndex = 12;
            this.comboPlans.SelectedIndexChanged += new System.EventHandler(this.comboPlans_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(314, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 15);
            this.label3.TabIndex = 10;
            this.label3.Text = "Total sum:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 15);
            this.label2.TabIndex = 9;
            this.label2.Text = "Product:";
            // 
            // comboProducts
            // 
            this.comboProducts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboProducts.FormattingEnabled = true;
            this.comboProducts.Location = new System.Drawing.Point(76, 49);
            this.comboProducts.Name = "comboProducts";
            this.comboProducts.Size = new System.Drawing.Size(202, 21);
            this.comboProducts.TabIndex = 8;
            this.comboProducts.SelectedIndexChanged += new System.EventHandler(this.comboProducts_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(368, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 15);
            this.label7.TabIndex = 9;
            this.label7.Text = "$";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancel.Location = new System.Drawing.Point(444, 508);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtSave
            // 
            this.txtSave.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtSave.Location = new System.Drawing.Point(321, 508);
            this.txtSave.Name = "txtSave";
            this.txtSave.Size = new System.Drawing.Size(115, 23);
            this.txtSave.TabIndex = 9;
            this.txtSave.Text = "Create subscription";
            this.txtSave.UseVisualStyleBackColor = true;
            this.txtSave.Click += new System.EventHandler(this.txtSave_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnRefresh.Location = new System.Drawing.Point(12, 508);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(102, 23);
            this.btnRefresh.TabIndex = 10;
            this.btnRefresh.Text = "Refresh Customer";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // comboApplicants
            // 
            this.comboApplicants.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboApplicants.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboApplicants.DisplayMember = "Text";
            this.comboApplicants.FormattingEnabled = true;
            this.comboApplicants.Location = new System.Drawing.Point(127, 13);
            this.comboApplicants.Name = "comboApplicants";
            this.comboApplicants.Size = new System.Drawing.Size(391, 21);
            this.comboApplicants.TabIndex = 11;
            this.comboApplicants.ValueMember = "Value";
            this.comboApplicants.SelectedIndexChanged += new System.EventHandler(this.comboApplicants_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(17, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(130, 17);
            this.label12.TabIndex = 12;
            this.label12.Text = "Select Applicant:";
            // 
            // groupAddCharge
            // 
            this.groupAddCharge.Controls.Add(this.txtChargeReason);
            this.groupAddCharge.Controls.Add(this.label15);
            this.groupAddCharge.Controls.Add(this.label14);
            this.groupAddCharge.Controls.Add(this.btnAddCharge);
            this.groupAddCharge.Controls.Add(this.txtChargeAmmount);
            this.groupAddCharge.Controls.Add(this.label13);
            this.groupAddCharge.Location = new System.Drawing.Point(12, 339);
            this.groupAddCharge.Name = "groupAddCharge";
            this.groupAddCharge.Size = new System.Drawing.Size(506, 134);
            this.groupAddCharge.TabIndex = 13;
            this.groupAddCharge.TabStop = false;
            this.groupAddCharge.Text = "Add Charge (for example when the customer stoped paying and there are expenses fo" +
    "r text messages)";
            // 
            // txtChargeReason
            // 
            this.txtChargeReason.Location = new System.Drawing.Point(76, 94);
            this.txtChargeReason.Name = "txtChargeReason";
            this.txtChargeReason.Size = new System.Drawing.Size(328, 20);
            this.txtChargeReason.TabIndex = 26;
            this.txtChargeReason.Text = "EDIT ME  Charges related with text messages and other";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 98);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 15);
            this.label15.TabIndex = 25;
            this.label15.Text = "Invoice Text:";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(13, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(474, 42);
            this.label14.TabIndex = 24;
            this.label14.Text = resources.GetString("label14.Text");
            // 
            // btnAddCharge
            // 
            this.btnAddCharge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCharge.Location = new System.Drawing.Point(410, 94);
            this.btnAddCharge.Name = "btnAddCharge";
            this.btnAddCharge.Size = new System.Drawing.Size(87, 21);
            this.btnAddCharge.TabIndex = 23;
            this.btnAddCharge.Text = "Add Charge";
            this.btnAddCharge.UseVisualStyleBackColor = true;
            this.btnAddCharge.Click += new System.EventHandler(this.btnAddCharge_Click_1);
            // 
            // txtChargeAmmount
            // 
            this.txtChargeAmmount.DecimalPlaces = 2;
            this.txtChargeAmmount.Location = new System.Drawing.Point(76, 68);
            this.txtChargeAmmount.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.txtChargeAmmount.Name = "txtChargeAmmount";
            this.txtChargeAmmount.Size = new System.Drawing.Size(180, 20);
            this.txtChargeAmmount.TabIndex = 22;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(63, 70);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 15);
            this.label13.TabIndex = 21;
            this.label13.Text = "$";
            // 
            // FormStripeCustomerSubscriptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 543);
            this.Controls.Add(this.groupAddCharge);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.comboApplicants);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.txtSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormStripeCustomerSubscriptions";
            this.Text = "Recurring Sales Receipt";
            this.Load += new System.EventHandler(this.FormStripeCustomerSubscriptions_Load);
            ((System.ComponentModel.ISupportInitialize)(this.textStartDay)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textInitialAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textTotalSum)).EndInit();
            this.groupAddCharge.ResumeLayout(false);
            this.groupAddCharge.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtChargeAmmount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown textStartDay;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnShowCustomer;
        private System.Windows.Forms.TextBox txtStripeEmail;
        private System.Windows.Forms.TextBox txtStripeCustomerName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboProducts;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboPlans;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button txtSave;
        private System.Windows.Forms.Button btnShowPlan;
        private System.Windows.Forms.Button btnShowProduct;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelWarning;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox txtPaymentMethods;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.NumericUpDown textTotalSum;
        private System.Windows.Forms.NumericUpDown textInitialAmount;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnHellpInitialAmount;
        private System.Windows.Forms.ComboBox comboApplicants;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupAddCharge;
        private System.Windows.Forms.Button btnAddCharge;
        private System.Windows.Forms.NumericUpDown txtChargeAmmount;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtChargeReason;
        private System.Windows.Forms.ComboBox comboStatus;
        private System.Windows.Forms.Label lblStatus;
    }
}