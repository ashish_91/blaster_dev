﻿using ApplicantsApp.Models.DTOs;
using Newtonsoft.Json;
using Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp
{
    public partial class FormFaxSend : Form
    {
        public ApplicantsNew ApplicantData { get; set; }
        public ApplicantsRepresentatives Representative { get; set; }
        public List<AzureToLocalFileDTO> FilesToSend { get; set; }

        private Source.Progress progress;
        private NLog.Logger logger = NLog.LogManager.GetLogger("email");

        private Models.Representative.RepresentativeInfo representativeInfo;
        private Models.Applicant.Applicant.FullApplicantInfo applicantInfo;

        private void RefreshApplicantDocuments()
        {
            progress.SetProgressText("Refreshing the documents...");

            listDocuments.Items.Clear();
            foreach (var document in FilesToSend)
            {
                listDocuments.Items.Add(document.documentId, document.humanFileName, CheckState.Checked, true);
            }
        }

        private List<Guid> GetSelectedDocuments()
        {
            var selectedDocuments = new List<Guid>();
            foreach (object checkedItem in listDocuments.CheckedItems)
            {
                selectedDocuments.Add((Guid)((DevExpress.XtraEditors.Controls.ListBoxItem)checkedItem).Value);
            }

            return selectedDocuments;
        }

        public FormFaxSend()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnSend_Click(object sender, EventArgs e)
        {
            Guid correlationId = Guid.NewGuid();
            string info = string.Empty;
            string errors = string.Empty;

            List<Guid> selectedDocumentsIds = GetSelectedDocuments();
            if (selectedDocumentsIds.Count == 0)
            {
                MessageBox.Show("In order to send a fax you need to select at least one document!");
                return;
            }

            if (selectedDocumentsIds.Count > 1)
            {
                DialogResult result = MessageBox.Show("Each selected document will be sent as new fax, do you want to continue?", "Warning", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.No || result == DialogResult.Cancel)
                {
                    return;
                }
            }

            progress.ShowProgressBar("Add audit action...");
            try
            {
                foreach (Guid documentId in selectedDocumentsIds)
                {
                    AzureToLocalFileDTO documentInfo = FilesToSend.Single(s => s.documentId == documentId);

                    if (!Path.GetExtension(documentInfo.humanFileName).Equals(".pdf", StringComparison.OrdinalIgnoreCase))
                    {
                        MessageBox.Show("Only pdf files can be faxed in the moment, we are working on making it possible for more document types, but for now - pdf only.");
                        continue;
                    }

                    Db.InsertAction(
                        logger,
                        ApplicantData.Id,
                        Models.ReportActions.SendFax,
                        correlationId,                        
                        documentInfo.documentType.ToString() + ": " + documentInfo.humanFileName
                        );

                    string blobFileName = Guid.NewGuid().ToString() + ".pdf";
                    //upload localFile to azure public container
                    Uri publicAddress = Shared.Source.Azure.StorageManager.UploadFile(
                        localFilePath: documentInfo.localFilePath,
                        containerName: "fax-files",
                        remoteFileName: blobFileName,
                        logger: logger,
                        applicantId: ApplicantData.Id
                        );

                    var аpplicantsActionsFaxDetails = new ApplicantsActionsFaxDetails()
                    {
                        ApplicantId = ApplicantData.Id,
                        CorrelationId = correlationId,
                        FileName = documentInfo.humanFileName,
                        FileType = (int)documentInfo.documentType,
                        To = textTo.Text,
                        Price = 0,
                        Uri = publicAddress.ToString(),
                        BlobFileName = blobFileName
                    };

                    try
                    {
                        progress.SetProgressText($"Sending fax to '{textTo.Text.Trim()}'");

                        string recipient = textTo.Text.Trim();
                        аpplicantsActionsFaxDetails.To = recipient;

                        Twilio.Rest.Fax.V1.FaxResource faxInfo = null;

                        string resp = Shared.Source.TwilioHelper.SendFax(
                            to: recipient,
                            uri: publicAddress.ToString(),
                            logger: logger,
                            info: ref faxInfo
                            );

                        if (resp != null)
                            throw new Exception(resp);

                        info += $"Fax successfully sended to '{recipient}'" + Environment.NewLine;
                        аpplicantsActionsFaxDetails.ErrorMessage = faxInfo?.Status?.ToString();
                        аpplicantsActionsFaxDetails.Result = 1;
                        аpplicantsActionsFaxDetails.Price = faxInfo?.Price ?? 0;

                        if (
                            documentInfo.documentType == Models.Documents.DocumentType.de101
                            || documentInfo.documentType == Models.Documents.DocumentType.de101_csra
                            || documentInfo.documentType == Models.Documents.DocumentType.de101_pas
                            )
                        {
                            //need to update RFI Submit Date
                            Db.ChangeApplicantSubmitDateAndStatus(ApplicantData.Id, DateTime.Now, documentInfo.documentType, logger);
                            MessageBox.Show("Submit Date and status has just been automatically updated!");
                        }

                        if (documentInfo.documentType == Models.Documents.DocumentType.de101)
                        {
                            MessageBox.Show("Nulling Status age. This is not an error, this is okay.");
                            Db.UpdateApplicantLastStatusChangeDate(ApplicantData);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                        errors += $"Sending fax to '{textTo.Text}' failed. {ex.Message.ToUpperInvariant()}" + Environment.NewLine;
                        аpplicantsActionsFaxDetails.ErrorMessage = ex.Message;
                        аpplicantsActionsFaxDetails.Result = -1;
                    }
                    finally
                    {
                        Db.InsertActionFax(logger, аpplicantsActionsFaxDetails);
                    }
                }

            }
            finally
            {
                MessageBox.Show(info + Environment.NewLine + errors);
                progress.HideProgressBar();
            }
        }

        private void FormFaxSend_Load(object sender, EventArgs e)
        {
            progress = new Source.Progress(progressBar1, labelProgress, this, logger);
            RefreshApplicantDocuments();
            representativeInfo = JsonConvert.DeserializeObject<Models.Representative.RepresentativeInfo>(Representative.Data);
            applicantInfo = JsonConvert.DeserializeObject<Models.Applicant.Applicant.FullApplicantInfo>(ApplicantData.Data);
        }
    }
}
