﻿using EnumsNET;
using Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp.Forms
{
    public partial class FormRepresentativesCatalog : Form
    {
        private Source.Progress progress;
        private NLog.Logger logger = NLog.LogManager.GetLogger("repCatalog");

        public FormRepresentativesCatalog()
        {
            InitializeComponent();
        }

        private void FormRepresentativesCatalog_Load(object sender, EventArgs e)
        {
            progress = new Source.Progress(progressBar, labelProgress, this, logger);

            RefreshRepresentatives();
        }

        private void RefreshRepresentatives()
        {
            gridApplicants.Rows.Clear();

            foreach(var rep in Db.GetAllRepresentatives(logger))
            {
                string status = ((Models.Representative.RepresentativeStatus)rep.Status).AsString();
                string type = ((Models.Representative.RepresentativeType)rep.Type).AsString();

                gridApplicants.Rows.Add(
                    rep.Id.ToString(),
                    status,
                    rep.RepresentativeName,
                    type,
                    rep.RepresentativeEmail,
                    rep.CreatedOn.ToString(Source.Resources.USADateTime)
                );
            }

            gridApplicants_SelectionChanged(null, null);
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            Guid? selectedRepresentative = GetSelectedRepresentative();

            menuStripDelete.Enabled = selectedRepresentative != null;
            menuStripEdit.Enabled = selectedRepresentative != null;
            menuStripNew.Enabled = true;
        }

        private Guid? GetSelectedRepresentative()
        {
            if (gridApplicants.SelectedRows.Count > 0)
            {
                return Guid.Parse(gridApplicants.SelectedRows[0].Cells[0].Value.ToString());
            }
            else
            {
                return null;
            }
        }

        private void gridApplicants_SelectionChanged(object sender, EventArgs e)
        {
            Guid? selectedRepresentative = GetSelectedRepresentative();

            toolEditApplicant.Enabled = selectedRepresentative != null;
            toolDeleteApplicant.Enabled = selectedRepresentative != null;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            AddOrEditRepresentative(Models.FormModes.New, null);
        }

        private void AddOrEditRepresentative(Models.FormModes formMode, ApplicantsRepresentatives Representative)
        {
            FormRepresentative representativeForm = new FormRepresentative()
            {
                FormMode = formMode,
                Representative = Representative
            };

            DialogResult res = representativeForm.ShowDialog();

            switch (res)
            {
                case DialogResult.Abort:
                case DialogResult.Cancel:
                    break;

                case DialogResult.OK:
                    RefreshRepresentatives();
                    break;

                default:
                    throw new Exception($"Unknown dialog result: '{res}' occured!");
            }
        }

        private void menuStripNew_Click(object sender, EventArgs e)
        {
            AddOrEditRepresentative(Models.FormModes.New, null);
        }

        private void toolEditApplicant_Click(object sender, EventArgs e)
        {
            EditRepresentative();
        }

        private void EditRepresentative()
        {
            Guid? selectedRepresentative = GetSelectedRepresentative();
            if (!selectedRepresentative.HasValue)
            {
                MessageBox.Show("Please, select an representrative!");
                return;
            }

            ApplicantsRepresentatives rep = Db.LoadRepresentative(selectedRepresentative.Value, logger);

            AddOrEditRepresentative(Models.FormModes.Edit, rep);
        }

        private void menuStripEdit_Click(object sender, EventArgs e)
        {
            EditRepresentative();
        }

        private void MenuStripDelete_Click(object sender, EventArgs e)
        {
            DeleteRepresentative();
        }

        private void ToolDeleteApplicant_Click(object sender, EventArgs e)
        {
            DeleteRepresentative();
        }

        private void DeleteRepresentative()
        {
            Guid? selectedRepresentativeId = GetSelectedRepresentative();
            if (!selectedRepresentativeId.HasValue)
            {
                MessageBox.Show("Please, select an representrative!");
                return;
            }

            DialogResult result = MessageBox.Show("Are you sure you want to delete the selected representative?", "Warning", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.No || result == DialogResult.Cancel)
            {
                return;
            }

            progress.ShowProgressBar("Deleting representative...");
            try
            {
                Db.RemoveRepresentative(selectedRepresentativeId.Value, logger);

                progress.SetProgressText("Add audit data");
                Db.InsertAction(
                    logger,
                    selectedRepresentativeId.Value,
                    Models.ReportActions.RepresentativeDelete,
                    data: selectedRepresentativeId.Value.ToString());

                MessageBox.Show($"The representative was deleted successfully!");
                RefreshRepresentatives();
            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }

}