﻿namespace ApplicantsApp.Forms
{
    partial class FormApplicantInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        ///
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormApplicantInfo));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("RFI");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Misc Docs");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Tasks – Client Facing");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Tasks – Non Client Facing");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Uncategorized documents");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Rejected documents");
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.contextRFI_Click = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newRFICategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.manageRFICategoriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextNoActions = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.noActionsHereToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnStamp = new System.Windows.Forms.Button();
            this.btnConvert = new System.Windows.Forms.Button();
            this.btnSaveNotes = new DevExpress.XtraEditors.SimpleButton();
            this.btnSaveRejectReason = new DevExpress.XtraEditors.SimpleButton();
            this.label5 = new System.Windows.Forms.Label();
            this.textRejectReason = new System.Windows.Forms.TextBox();
            this.btnDeleteDocuments = new DevExpress.XtraEditors.SimpleButton();
            this.btnDownloadDocuments = new DevExpress.XtraEditors.SimpleButton();
            this.btnSendFaxType = new System.Windows.Forms.Button();
            this.btnSendEmailType = new System.Windows.Forms.Button();
            this.comboFaxTypes = new System.Windows.Forms.ComboBox();
            this.comboEmailTypes = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textDocumentNotes = new System.Windows.Forms.TextBox();
            this.treeDocsAndTasks = new System.Windows.Forms.TreeView();
            this.btnGenerate = new DevExpress.XtraEditors.SimpleButton();
            this.comboDocumentGenerator = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.accordionControl1 = new DevExpress.XtraBars.Navigation.AccordionControl();
            this.accordionContentContainer8 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.gridMedical_Parties = new System.Windows.Forms.DataGridView();
            this.colMedical_Party_Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMedical_Party_DoctorName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMedical_Party_Fax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMedical_Party_Phone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMedical_Party_Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.medicalContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.medicalContextRemotePhysicians = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMedicalParties_CreatreNewDOctor = new System.Windows.Forms.Button();
            this.btnMedicalParties_AddDoctor = new System.Windows.Forms.Button();
            this.label65 = new System.Windows.Forms.Label();
            this.comboMedical_Parties = new System.Windows.Forms.ComboBox();
            this.accordionContentContainer9 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.checkMedical_Disclosed_All = new System.Windows.Forms.CheckBox();
            this.groupMedical_Disclosed_Condition = new System.Windows.Forms.GroupBox();
            this.txtMedical_Disclosed_SpecificText = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.checkMedical_Disclosed_Specific = new System.Windows.Forms.CheckBox();
            this.txtMedical_Disclosed_OtherEmail = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.txtMedical_Disclosed_OtherText = new System.Windows.Forms.TextBox();
            this.checkMedical_Disclosed_Other = new System.Windows.Forms.CheckBox();
            this.groupMedical_Disclosed_Period = new System.Windows.Forms.GroupBox();
            this.dateMedical_Disclosed_PeriodTo = new System.Windows.Forms.DateTimePicker();
            this.label70 = new System.Windows.Forms.Label();
            this.dateMedical_Disclosed_PeriodFrom = new System.Windows.Forms.DateTimePicker();
            this.label69 = new System.Windows.Forms.Label();
            this.checkMedical_Disclosed_Period = new System.Windows.Forms.CheckBox();
            this.accordionContentContainer10 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.checkMedical_Purpose_Sell = new System.Windows.Forms.CheckBox();
            this.checkMedical_Purpose_Comunicate = new System.Windows.Forms.CheckBox();
            this.txtMedical_Purpose_OtherText = new System.Windows.Forms.TextBox();
            this.checkMedical_Purpose_Other = new System.Windows.Forms.CheckBox();
            this.checkMedical_Purpose_MyRequest = new System.Windows.Forms.CheckBox();
            this.accordionContentContainer11 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.dateMedical_AuthEnd_date = new System.Windows.Forms.DateTimePicker();
            this.txtMedical_AuthEnd_EventText = new System.Windows.Forms.TextBox();
            this.checkMedical_AuthEnd_Event = new System.Windows.Forms.CheckBox();
            this.checkMedical_AuthEnd_OnDate = new System.Windows.Forms.CheckBox();
            this.accordionContentContainer12 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.txtMedical_Rights_OtherText = new System.Windows.Forms.TextBox();
            this.checkMedical_Rights_Other = new System.Windows.Forms.CheckBox();
            this.checkMedical_Rights_CountOrder = new System.Windows.Forms.CheckBox();
            this.checkMedical_Rights_LegalGuardian = new System.Windows.Forms.CheckBox();
            this.checkMedical_Rights_CantSign = new System.Windows.Forms.CheckBox();
            this.checkMedical_Rights_Parent = new System.Windows.Forms.CheckBox();
            this.label74 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.dateMedical_Rights_AuthorizedRep_SignDate = new System.Windows.Forms.DateTimePicker();
            this.groupMedical_Rights_UnableToSign = new System.Windows.Forms.GroupBox();
            this.txtMedical_Rights_MinorAge = new System.Windows.Forms.TextBox();
            this.txtMedical_Rights_UnableToSignText = new System.Windows.Forms.TextBox();
            this.checkMedical_Rights_UnableToSign = new System.Windows.Forms.CheckBox();
            this.checkMedical_Rights_SubMinor = new System.Windows.Forms.CheckBox();
            this.label73 = new System.Windows.Forms.Label();
            this.dateMedical_Rights_date = new System.Windows.Forms.DateTimePicker();
            this.accordionContentContainer13 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.label76 = new System.Windows.Forms.Label();
            this.dateMedical_AddCon_Time = new System.Windows.Forms.DateTimePicker();
            this.label75 = new System.Windows.Forms.Label();
            this.dateMedical_AddCon_Date = new System.Windows.Forms.DateTimePicker();
            this.checkMedical_AddCon_IDoNot = new System.Windows.Forms.CheckBox();
            this.checkMedical_AddCon_IDo = new System.Windows.Forms.CheckBox();
            this.accordionContentContainer14 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.label77 = new System.Windows.Forms.Label();
            this.dateMedical_HIV_Time = new System.Windows.Forms.DateTimePicker();
            this.label78 = new System.Windows.Forms.Label();
            this.dateMedical_HIV_date = new System.Windows.Forms.DateTimePicker();
            this.checkMedical_HIV_IDoNot = new System.Windows.Forms.CheckBox();
            this.checkMedical_HIV_IDo = new System.Windows.Forms.CheckBox();
            this.accordionContentContainer3 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.label36 = new System.Windows.Forms.Label();
            this.CustomerCurrentLiving_NameOfHospital = new System.Windows.Forms.TextBox();
            this.CustomerCurrentLiving_ExpectedDateOfDischarge = new System.Windows.Forms.DateTimePicker();
            this.CustomerCurrentLiving_HospitalAddress = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.CustomerCurrentLivingOther_Text = new System.Windows.Forms.TextBox();
            this.CustomerCurrentLiving_Other = new System.Windows.Forms.RadioButton();
            this.CustomerCurrentLiving_Nursing = new System.Windows.Forms.RadioButton();
            this.CustomerCurrentLiving_Home = new System.Windows.Forms.RadioButton();
            this.CustomerCurrentLiving_Hospital = new System.Windows.Forms.RadioButton();
            this.label42 = new System.Windows.Forms.Label();
            this.CustomerCurrentLiving_City = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.CustomerCurrentLiving_ZipCode = new System.Windows.Forms.TextBox();
            this.CustomerCurrentLiving_PhoneNumber = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.CustomerCurrentLiving_State = new System.Windows.Forms.TextBox();
            this.accordionContentContainer4 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.AccomodationsForPrintedLetters_Yes = new System.Windows.Forms.RadioButton();
            this.AccomodationsForPrintedLetters_No = new System.Windows.Forms.RadioButton();
            this.AccomodationsForPrintedLetters_Group = new System.Windows.Forms.GroupBox();
            this.AccomodationsForPrintedLetters_OtherText = new System.Windows.Forms.TextBox();
            this.AccomodationsForPrintedLetters_Other = new System.Windows.Forms.CheckBox();
            this.AccomodationsForPrintedLetters_LargePrint = new System.Windows.Forms.CheckBox();
            this.AccomodationsForPrintedLetters_Email = new System.Windows.Forms.CheckBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.AccomodationsForPrintedLetters_WhoIfYes = new System.Windows.Forms.TextBox();
            this.accordionContentContainer5 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.isPregnant_Yes = new System.Windows.Forms.RadioButton();
            this.isPregnant_No = new System.Windows.Forms.RadioButton();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.AdditionalQuestions_5_Yes = new System.Windows.Forms.RadioButton();
            this.AdditionalQuestions_5_No = new System.Windows.Forms.RadioButton();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.AdditionalQuestions_4_Yes = new System.Windows.Forms.RadioButton();
            this.AdditionalQuestions_4_No = new System.Windows.Forms.RadioButton();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.AdditionalQuestions_3_Seizure = new System.Windows.Forms.CheckBox();
            this.AdditionalQuestions_3_Intellectual = new System.Windows.Forms.CheckBox();
            this.AdditionalQuestions_3_CerebralPalsy = new System.Windows.Forms.CheckBox();
            this.AdditionalQuestions_3_Autism = new System.Windows.Forms.CheckBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.AdditionalQuestions_2_Yes = new System.Windows.Forms.RadioButton();
            this.AdditionalQuestions_2_No = new System.Windows.Forms.RadioButton();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.AdditionalQuestions_2_Yes_Date = new System.Windows.Forms.DateTimePicker();
            this.label44 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.AdditionalQuestions_1_Yes = new System.Windows.Forms.RadioButton();
            this.AdditionalQuestions_1_No = new System.Windows.Forms.RadioButton();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label45 = new System.Windows.Forms.Label();
            this.AdditionalQuestions_1_Yes_Month3 = new System.Windows.Forms.TextBox();
            this.AdditionalQuestions_1_Yes_Month2 = new System.Windows.Forms.TextBox();
            this.AdditionalQuestions_1_Yes_Month1 = new System.Windows.Forms.TextBox();
            this.accordionContentContainer6 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.InterviewInterpretter_Yes = new System.Windows.Forms.RadioButton();
            this.InterviewInterpretter_No = new System.Windows.Forms.RadioButton();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.label51 = new System.Windows.Forms.Label();
            this.InterviewInterpretter_Language = new System.Windows.Forms.TextBox();
            this.InterviewInterpretter_HomeAddress = new System.Windows.Forms.TextBox();
            this.InterviewInterpretter_MajorCrossroads = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.Interview_Thursday_Time = new System.Windows.Forms.TextBox();
            this.Interview_Tuesday_Time = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.Interview_Friday_Time = new System.Windows.Forms.TextBox();
            this.Interview_Wednesday_Time = new System.Windows.Forms.TextBox();
            this.Interview_Thursday = new System.Windows.Forms.CheckBox();
            this.label50 = new System.Windows.Forms.Label();
            this.Interview_Monday_Time = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.Interview_Friday = new System.Windows.Forms.CheckBox();
            this.Interview_Tuesday = new System.Windows.Forms.CheckBox();
            this.Interview_Wednesday = new System.Windows.Forms.CheckBox();
            this.label46 = new System.Windows.Forms.Label();
            this.Interview_Monday = new System.Windows.Forms.CheckBox();
            this.accordionContentContainer7 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.PersonCompleteingTheForm_IsParent = new System.Windows.Forms.RadioButton();
            this.PersonCompleteingTheForm_IsSpouse = new System.Windows.Forms.RadioButton();
            this.PersonCompleteingTheForm_IsCustomer = new System.Windows.Forms.RadioButton();
            this.PersonCompleteingTheForm_Phone = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.PersonCompleteingTheForm_Name = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.accordionContentContainer15 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.btnAddNewMedicalRepresentative = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditSelectedMedicalRepresentative = new DevExpress.XtraEditors.SimpleButton();
            this.comboMedicalRepresentative = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnAddNewRepresentative = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditRepresentative = new DevExpress.XtraEditors.SimpleButton();
            this.comboFinancialRepresentative = new System.Windows.Forms.ComboBox();
            this.label64 = new System.Windows.Forms.Label();
            this.labelControl77 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl76 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_MailingAddressCountry = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl69 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_MailingAddressZip = new DevExpress.XtraEditors.TextEdit();
            this.labelControl70 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_MailingAddressState = new DevExpress.XtraEditors.TextEdit();
            this.labelControl71 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_MailingAddressCity = new DevExpress.XtraEditors.TextEdit();
            this.labelControl72 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_MailingAddressLine2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl73 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl74 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_MailingAddressStreet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl75 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_Email = new DevExpress.XtraEditors.TextEdit();
            this.labelControl67 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_Phone = new DevExpress.XtraEditors.TextEdit();
            this.labelControl66 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_AddressCountry = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl59 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_AddressZip = new DevExpress.XtraEditors.TextEdit();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_AddressState = new DevExpress.XtraEditors.TextEdit();
            this.labelControl61 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_AddressCity = new DevExpress.XtraEditors.TextEdit();
            this.labelControl62 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_AddressLine2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl63 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl64 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_AddressStreet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl65 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_MilitaryToYear = new DevExpress.XtraEditors.TextEdit();
            this.applicant_MilitaryFromYear = new DevExpress.XtraEditors.TextEdit();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_MilitaryBranch = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl58 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_IsMilitary = new DevExpress.XtraEditors.CheckEdit();
            this.applicant_SpouseSSN = new DevExpress.XtraEditors.TextEdit();
            this.labelControl68 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_SpouseLastName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_SpouseMiddleName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_SpouseFirstName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_DateOfMarrgiage = new DevExpress.XtraEditors.DateEdit();
            this.label84 = new System.Windows.Forms.Label();
            this.applicant_SpouceDateOfBirth = new DevExpress.XtraEditors.DateEdit();
            this.label83 = new System.Windows.Forms.Label();
            this.applicant_MaritalStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_DateOfBirth = new DevExpress.XtraEditors.DateEdit();
            this.label82 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.applicant_Gender = new DevExpress.XtraEditors.RadioGroup();
            this.applicant_SSN = new DevExpress.XtraEditors.TextEdit();
            this.applicant_LastName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_MiddleName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_FirstName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_LivingAt = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_AlreadyOnAHCCCSProgram = new DevExpress.XtraEditors.CheckEdit();
            this.applicant_IsRegisteredVoter = new DevExpress.XtraEditors.CheckEdit();
            this.applicant_Ethnicity = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_PlaceOfBirth = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.applicant_IsUSCitizen = new DevExpress.XtraEditors.CheckEdit();
            this.accordionContentContainer16 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.poc_IsPOC = new DevExpress.XtraEditors.CheckEdit();
            this.groupPOC = new DevExpress.XtraEditors.GroupControl();
            this.poc_AddressCountry = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.poc_AddressZip = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.poc_AddressState = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.poc_AddressCity = new DevExpress.XtraEditors.TextEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.poc_Email = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.poc_Phone = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.poc_AddressLine2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.poc_AddressStreet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.poc_LastName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.poc_MiddleName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.poc_FirstName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.poc_Relationship = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.accordionContentContainer17 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.ltc_IsLtcAvaliable = new DevExpress.XtraEditors.CheckEdit();
            this.groupLTC = new DevExpress.XtraEditors.GroupControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.ltc_DateEntered = new DevExpress.XtraEditors.DateEdit();
            this.ltc_FacilityType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.ltc_AddressCountry = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.ltc_FacilityPhone = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.ltc_AddressZip = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.ltc_AddressState = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.ltc_AddressCity = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.ltc_AddressLine2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.ltc_AddressStreet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.ltc_FacilityPOCLastName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.ltc_CaficlityPocFirstName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.ltc_FacilityName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.accordionContentContainer18 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.helthAndMedical_IsuesWithAlzheimer = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl17 = new DevExpress.XtraEditors.GroupControl();
            this.helthAndMedical_NotesDDD = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.helthAndMedical_IsuesWithDDD = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl16 = new DevExpress.XtraEditors.GroupControl();
            this.helthAndMedical_NotesOxygen = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.helthAndMedical_IsuesWithOxygen = new DevExpress.XtraEditors.CheckEdit();
            this.helthAndMedical_IsuesWithDressing = new DevExpress.XtraEditors.CheckEdit();
            this.groupAlzheimer = new DevExpress.XtraEditors.GroupControl();
            this.helthAndMedical_AlzheimerDate = new DevExpress.XtraEditors.DateEdit();
            this.helthAndMedical_AlzheimerField = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label66 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.helthAndMedical_AlzheimerLastName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.helthAndMedical_AlzheimerFirstName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl18 = new DevExpress.XtraEditors.GroupControl();
            this.helthAndMedical_NotesOther = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.helthAndMedical_IsuesWithOther = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl14 = new DevExpress.XtraEditors.GroupControl();
            this.helthAndMedical_NotesParalisys = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.helthAndMedical_IsuesWithParalysis = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl13 = new DevExpress.XtraEditors.GroupControl();
            this.helthAndMedical_NotesBehavior = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.helthAndMedical_IsuesWithDisruptive = new DevExpress.XtraEditors.CheckEdit();
            this.helthAndMedical_IsuesWithWandering = new DevExpress.XtraEditors.CheckEdit();
            this.helthAndMedical_IsuesWithSelfInjury = new DevExpress.XtraEditors.CheckEdit();
            this.helthAndMedical_IsuesWithAggression = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl12 = new DevExpress.XtraEditors.GroupControl();
            this.helthAndMedical_NotesOrientation = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.helthAndMedical_IsuesWithOrientation = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl11 = new DevExpress.XtraEditors.GroupControl();
            this.helthAndMedical_IsLegallyBlind = new DevExpress.XtraEditors.CheckEdit();
            this.helthAndMedical_NotesVision = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.helthAndMedical_IsuesWithVision = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl10 = new DevExpress.XtraEditors.GroupControl();
            this.helthAndMedical_NotesIncontinence = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.helthAndMedical_IsuesWithBowel = new DevExpress.XtraEditors.CheckEdit();
            this.helthAndMedical_IsuesWithBladder = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl9 = new DevExpress.XtraEditors.GroupControl();
            this.helthAndMedical_NotesToileting = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.helthAndMedical_IsuesWithToileting = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.helthAndMedical_NotesEating = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.helthAndMedical_IsuesWithEating = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.helthAndMedical_NotesDressing = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.helthAndMedical_NotesBathing = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.helthAndMedical_IsuesWithBathing = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.helthAndMedical_NotesTransferring = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.helthAndMedical_IsuesWithTransferring = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.helthAndMedical_NotesMobility = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.helthAndMedical_IsuesWithMobility = new DevExpress.XtraEditors.CheckEdit();
            this.accordionContentContainer19 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.financial_IsOwningHome = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl24 = new DevExpress.XtraEditors.GroupControl();
            this.financial_vehicles = new System.Windows.Forms.DataGridView();
            this.ColVehicle_Make = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColVehicle_Model = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColVehicle_Year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColVehicle_Milage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupHomeOwned = new DevExpress.XtraEditors.GroupControl();
            this.financial_IsHomeOtherOccupants = new DevExpress.XtraEditors.CheckEdit();
            this.financial_IsHomeReverseMortgage = new DevExpress.XtraEditors.CheckEdit();
            this.financial_IsHomeMortgage = new DevExpress.XtraEditors.CheckEdit();
            this.financial_BurialAmount = new DevExpress.XtraEditors.TextEdit();
            this.label67 = new System.Windows.Forms.Label();
            this.financial_isBural = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl22 = new DevExpress.XtraEditors.GroupControl();
            this.financial_gridIncome = new System.Windows.Forms.DataGridView();
            this.ColIncome_Source = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColSource_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.financial_PolicyNotes = new DevExpress.XtraEditors.MemoEdit();
            this.financial_IsOwningPolicy = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl21 = new DevExpress.XtraEditors.GroupControl();
            this.financial_LifeCash = new DevExpress.XtraEditors.TextEdit();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.financial_LifeInstitutionName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl20 = new DevExpress.XtraEditors.GroupControl();
            this.financial_gridRetirement = new System.Windows.Forms.DataGridView();
            this.ColRetirement_Ret = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColRetirement_Institution = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColRetirement_Balance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupControl19 = new DevExpress.XtraEditors.GroupControl();
            this.financial_gridResourceAccounts = new System.Windows.Forms.DataGridView();
            this.ColResource_BankAccType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColResource_BankAccInstitution = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColResource_BankAccBallance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accordionContentContainer20 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.lookback_OtherNotes = new DevExpress.XtraEditors.MemoEdit();
            this.label79 = new System.Windows.Forms.Label();
            this.lookback_GivenCash = new DevExpress.XtraEditors.CheckEdit();
            this.lookback_TransferredFunds = new DevExpress.XtraEditors.CheckEdit();
            this.lookback_SoldCarlLast5 = new DevExpress.XtraEditors.CheckEdit();
            this.lookback_SoldHomeLast5 = new DevExpress.XtraEditors.CheckEdit();
            this.accordionContentContainer21 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.clientNeeds_OtherNotes = new DevExpress.XtraEditors.MemoEdit();
            this.label80 = new System.Windows.Forms.Label();
            this.clientNeeds_RealEstate = new DevExpress.XtraEditors.CheckEdit();
            this.clientNeeds_DocPrep = new DevExpress.XtraEditors.CheckEdit();
            this.clientNeeds_Placement = new DevExpress.XtraEditors.CheckEdit();
            this.clientNeeds_Veteran = new DevExpress.XtraEditors.CheckEdit();
            this.clientNeeds_ALTCS = new DevExpress.XtraEditors.CheckEdit();
            this.accordionContentContainer22 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.autoEmail_SendWhenStatusIsChanged = new System.Windows.Forms.CheckBox();
            this.autoEmail_SendWhenDocIsRejected = new System.Windows.Forms.CheckBox();
            this.autoEmail_SendWhenDocIsApproved = new System.Windows.Forms.CheckBox();
            this.accordionContentContainer23 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.autoEmail_RFI_Enabled = new System.Windows.Forms.CheckBox();
            this.autoEmail_RFI_group = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.autoEmail_RFI_TopTextContent = new System.Windows.Forms.TextBox();
            this.autoEmail_RFI_AddTopText = new System.Windows.Forms.CheckBox();
            this.autoEmail_RFI_SendOnlyMonToFri = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.autoEmail_RFI_SendIntervalInDays = new System.Windows.Forms.NumericUpDown();
            this.accordionContentContainer24 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.autoEmail_Interim_Enabled = new System.Windows.Forms.CheckBox();
            this.autoEmail_Interim_Group = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.autoEmail_Interim_TopTextText = new System.Windows.Forms.TextBox();
            this.autoEmail_Interim_AddTopText = new System.Windows.Forms.CheckBox();
            this.autoEmail_Interim_MonFriOnly = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.autoEmail_Interim_SendIntervalInDays = new System.Windows.Forms.NumericUpDown();
            this.accordionContentContainer2 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.txtFinWorkerEmail = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtFinWorkerFax = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtFinWorkerPhone = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtFinWorkerAddress = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnViewFin = new System.Windows.Forms.Button();
            this.comboFin = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.accordionContentContainer25 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.txtFinSuperEmail = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtFinSuperFax = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtFinSuperPhone = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtFinSuperAddress = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.btnViewFinSuper_ = new System.Windows.Forms.Button();
            this.comboFinSup = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.accordionContentContainer26 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.btnMedApproveDateSendEmail = new System.Windows.Forms.Button();
            this.checkMedicalCaseWorker_MedAPprovalDate = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.dateMedicalCaseWorker_MedAPprovalDate = new System.Windows.Forms.DateTimePicker();
            this.txtMedEmail = new System.Windows.Forms.TextBox();
            this.lblMedEmail = new System.Windows.Forms.Label();
            this.txtMedFax = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtMedPhone = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtMedAddress = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.btnCiewMed = new System.Windows.Forms.Button();
            this.comboMed = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.accordionContentContainer27 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.autoEmail_LiteRFI_IsEnabled = new System.Windows.Forms.CheckBox();
            this.autoEmail_LiteRFI_Groupbox = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.accordionControlElement3 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement25 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement26 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement28 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement32 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement17 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement18 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement19 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement20 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement21 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement22 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement23 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement24 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement1 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement4 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement5 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement6 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement7 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement8 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement9 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement10 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement11 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement12 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement13 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement14 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement15 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement16 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement27 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement29 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement30 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement31 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.progressBar = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.labelProgress = new DevExpress.XtraEditors.LabelControl();
            this.pictureViewer = new DevExpress.XtraEditors.PictureEdit();
            this.richEditControl = new DevExpress.XtraRichEdit.RichEditControl();
            this.pdfViewer = new PdfiumViewer.PdfViewer();
            this.checkedListBoxControl2 = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.checkedListBoxControl3 = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.checkedListBoxControl4 = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.accordionContentContainer1 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.accordionControlElement2 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.timerLoad = new System.Windows.Forms.Timer(this.components);
            this.contextRFI_CategoryClick = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addDocumentPlaceholderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.editCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextRFI_DocumentClick = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.renamePlaceholderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadDocumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deletePlaceholderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.approveDocumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rejectDocumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextTask_Click = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createTaskToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextOtherDocument_Click = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.renameDocumentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteDocumentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewDocumentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.contextRFI_FileClick = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.viewDocumentToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.renameDocumentToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteDocumentToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.combineSelectedPDFsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextRFI_Click.SuspendLayout();
            this.contextNoActions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboDocumentGenerator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl1)).BeginInit();
            this.accordionControl1.SuspendLayout();
            this.accordionContentContainer8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMedical_Parties)).BeginInit();
            this.medicalContextMenu.SuspendLayout();
            this.accordionContentContainer9.SuspendLayout();
            this.groupMedical_Disclosed_Condition.SuspendLayout();
            this.groupMedical_Disclosed_Period.SuspendLayout();
            this.accordionContentContainer10.SuspendLayout();
            this.accordionContentContainer11.SuspendLayout();
            this.accordionContentContainer12.SuspendLayout();
            this.groupMedical_Rights_UnableToSign.SuspendLayout();
            this.accordionContentContainer13.SuspendLayout();
            this.accordionContentContainer14.SuspendLayout();
            this.accordionContentContainer3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.accordionContentContainer4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.AccomodationsForPrintedLetters_Group.SuspendLayout();
            this.accordionContentContainer5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.accordionContentContainer6.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.accordionContentContainer7.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.accordionContentContainer15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MailingAddressCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MailingAddressZip.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MailingAddressState.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MailingAddressCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MailingAddressLine2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MailingAddressStreet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_Email.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_Phone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_AddressCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_AddressZip.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_AddressState.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_AddressCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_AddressLine2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_AddressStreet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MilitaryToYear.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MilitaryFromYear.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MilitaryBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_IsMilitary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_SpouseSSN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_SpouseLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_SpouseMiddleName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_SpouseFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_DateOfMarrgiage.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_DateOfMarrgiage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_SpouceDateOfBirth.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_SpouceDateOfBirth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MaritalStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_DateOfBirth.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_DateOfBirth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_Gender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_SSN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_LastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MiddleName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_FirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_LivingAt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_AlreadyOnAHCCCSProgram.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_IsRegisteredVoter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_Ethnicity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_PlaceOfBirth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_IsUSCitizen.Properties)).BeginInit();
            this.accordionContentContainer16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.poc_IsPOC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupPOC)).BeginInit();
            this.groupPOC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.poc_AddressCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_AddressZip.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_AddressState.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_AddressCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_Email.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_Phone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_AddressLine2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_AddressStreet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_LastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_MiddleName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_FirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_Relationship.Properties)).BeginInit();
            this.accordionContentContainer17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_IsLtcAvaliable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupLTC)).BeginInit();
            this.groupLTC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_DateEntered.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_DateEntered.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_FacilityType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_AddressCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_FacilityPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_AddressZip.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_AddressState.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_AddressCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_AddressLine2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_AddressStreet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_FacilityPOCLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_CaficlityPocFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_FacilityName.Properties)).BeginInit();
            this.accordionContentContainer18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithAlzheimer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl17)).BeginInit();
            this.groupControl17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesDDD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithDDD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl16)).BeginInit();
            this.groupControl16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesOxygen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithOxygen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithDressing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupAlzheimer)).BeginInit();
            this.groupAlzheimer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_AlzheimerDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_AlzheimerDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_AlzheimerField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_AlzheimerLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_AlzheimerFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl18)).BeginInit();
            this.groupControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesOther.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithOther.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl14)).BeginInit();
            this.groupControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesParalisys.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithParalysis.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl13)).BeginInit();
            this.groupControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesBehavior.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithDisruptive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithWandering.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithSelfInjury.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithAggression.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl12)).BeginInit();
            this.groupControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesOrientation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithOrientation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).BeginInit();
            this.groupControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsLegallyBlind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesVision.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithVision.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).BeginInit();
            this.groupControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesIncontinence.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithBowel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithBladder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).BeginInit();
            this.groupControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesToileting.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithToileting.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesEating.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithEating.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesDressing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesBathing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithBathing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesTransferring.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithTransferring.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesMobility.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithMobility.Properties)).BeginInit();
            this.accordionContentContainer19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.financial_IsOwningHome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl24)).BeginInit();
            this.groupControl24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.financial_vehicles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupHomeOwned)).BeginInit();
            this.groupHomeOwned.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.financial_IsHomeOtherOccupants.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.financial_IsHomeReverseMortgage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.financial_IsHomeMortgage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.financial_BurialAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.financial_isBural.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl22)).BeginInit();
            this.groupControl22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.financial_gridIncome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.financial_PolicyNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.financial_IsOwningPolicy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl21)).BeginInit();
            this.groupControl21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.financial_LifeCash.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.financial_LifeInstitutionName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl20)).BeginInit();
            this.groupControl20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.financial_gridRetirement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl19)).BeginInit();
            this.groupControl19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.financial_gridResourceAccounts)).BeginInit();
            this.accordionContentContainer20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookback_OtherNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookback_GivenCash.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookback_TransferredFunds.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookback_SoldCarlLast5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookback_SoldHomeLast5.Properties)).BeginInit();
            this.accordionContentContainer21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientNeeds_OtherNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientNeeds_RealEstate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientNeeds_DocPrep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientNeeds_Placement.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientNeeds_Veteran.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientNeeds_ALTCS.Properties)).BeginInit();
            this.accordionContentContainer22.SuspendLayout();
            this.accordionContentContainer23.SuspendLayout();
            this.autoEmail_RFI_group.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.autoEmail_RFI_SendIntervalInDays)).BeginInit();
            this.accordionContentContainer24.SuspendLayout();
            this.autoEmail_Interim_Group.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.autoEmail_Interim_SendIntervalInDays)).BeginInit();
            this.accordionContentContainer2.SuspendLayout();
            this.accordionContentContainer25.SuspendLayout();
            this.accordionContentContainer26.SuspendLayout();
            this.accordionContentContainer27.SuspendLayout();
            this.autoEmail_LiteRFI_Groupbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progressBar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureViewer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxControl4)).BeginInit();
            this.contextRFI_CategoryClick.SuspendLayout();
            this.contextRFI_DocumentClick.SuspendLayout();
            this.contextTask_Click.SuspendLayout();
            this.contextOtherDocument_Click.SuspendLayout();
            this.contextRFI_FileClick.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextRFI_Click
            // 
            this.contextRFI_Click.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newRFICategoryToolStripMenuItem,
            this.toolStripMenuItem2,
            this.manageRFICategoriesToolStripMenuItem});
            this.contextRFI_Click.Name = "contextRFI_Click";
            this.contextRFI_Click.Size = new System.Drawing.Size(196, 54);
            // 
            // newRFICategoryToolStripMenuItem
            // 
            this.newRFICategoryToolStripMenuItem.Name = "newRFICategoryToolStripMenuItem";
            this.newRFICategoryToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.newRFICategoryToolStripMenuItem.Text = "New RFI Category";
            this.newRFICategoryToolStripMenuItem.Click += new System.EventHandler(this.newRFICategoryToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(192, 6);
            // 
            // manageRFICategoriesToolStripMenuItem
            // 
            this.manageRFICategoriesToolStripMenuItem.Name = "manageRFICategoriesToolStripMenuItem";
            this.manageRFICategoriesToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.manageRFICategoriesToolStripMenuItem.Text = "Manage RFI Categories";
            this.manageRFICategoriesToolStripMenuItem.Click += new System.EventHandler(this.manageRFICategoriesToolStripMenuItem_Click);
            // 
            // contextNoActions
            // 
            this.contextNoActions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.noActionsHereToolStripMenuItem});
            this.contextNoActions.Name = "contextMenuStrip1";
            this.contextNoActions.Size = new System.Drawing.Size(158, 26);
            // 
            // noActionsHereToolStripMenuItem
            // 
            this.noActionsHereToolStripMenuItem.Enabled = false;
            this.noActionsHereToolStripMenuItem.Name = "noActionsHereToolStripMenuItem";
            this.noActionsHereToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.noActionsHereToolStripMenuItem.Text = "No actions here";
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Location = new System.Drawing.Point(0, 617);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1187, 41);
            this.panelControl1.TabIndex = 1;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.btnStamp);
            this.groupControl1.Controls.Add(this.btnConvert);
            this.groupControl1.Controls.Add(this.btnSaveNotes);
            this.groupControl1.Controls.Add(this.btnSaveRejectReason);
            this.groupControl1.Controls.Add(this.label5);
            this.groupControl1.Controls.Add(this.textRejectReason);
            this.groupControl1.Controls.Add(this.btnDeleteDocuments);
            this.groupControl1.Controls.Add(this.btnDownloadDocuments);
            this.groupControl1.Controls.Add(this.btnSendFaxType);
            this.groupControl1.Controls.Add(this.btnSendEmailType);
            this.groupControl1.Controls.Add(this.comboFaxTypes);
            this.groupControl1.Controls.Add(this.comboEmailTypes);
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.textDocumentNotes);
            this.groupControl1.Controls.Add(this.treeDocsAndTasks);
            this.groupControl1.Controls.Add(this.btnGenerate);
            this.groupControl1.Controls.Add(this.comboDocumentGenerator);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(383, 786);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "Right click for context menu, different for each item!";
            // 
            // btnStamp
            // 
            this.btnStamp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStamp.Location = new System.Drawing.Point(52, 440);
            this.btnStamp.Name = "btnStamp";
            this.btnStamp.Size = new System.Drawing.Size(52, 39);
            this.btnStamp.TabIndex = 24;
            this.btnStamp.Text = "Stamp";
            this.btnStamp.UseVisualStyleBackColor = true;
            this.btnStamp.Visible = false;
            this.btnStamp.Click += new System.EventHandler(this.btnStamp_Click);
            // 
            // btnConvert
            // 
            this.btnConvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnConvert.Location = new System.Drawing.Point(254, 440);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(44, 39);
            this.btnConvert.TabIndex = 23;
            this.btnConvert.Text = "CONV";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Visible = false;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // btnSaveNotes
            // 
            this.btnSaveNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveNotes.Location = new System.Drawing.Point(319, 488);
            this.btnSaveNotes.Name = "btnSaveNotes";
            this.btnSaveNotes.Size = new System.Drawing.Size(54, 16);
            this.btnSaveNotes.TabIndex = 22;
            this.btnSaveNotes.Text = "Save";
            this.btnSaveNotes.Click += new System.EventHandler(this.btnSaveNotes_Click);
            // 
            // btnSaveRejectReason
            // 
            this.btnSaveRejectReason.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveRejectReason.Location = new System.Drawing.Point(319, 610);
            this.btnSaveRejectReason.Name = "btnSaveRejectReason";
            this.btnSaveRejectReason.Size = new System.Drawing.Size(54, 16);
            this.btnSaveRejectReason.TabIndex = 22;
            this.btnSaveRejectReason.Text = "Save";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(5, 610);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Reject reason:";
            // 
            // textRejectReason
            // 
            this.textRejectReason.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textRejectReason.Location = new System.Drawing.Point(5, 626);
            this.textRejectReason.Multiline = true;
            this.textRejectReason.Name = "textRejectReason";
            this.textRejectReason.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textRejectReason.Size = new System.Drawing.Size(368, 52);
            this.textRejectReason.TabIndex = 19;
            // 
            // btnDeleteDocuments
            // 
            this.btnDeleteDocuments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeleteDocuments.Appearance.Options.UseImage = true;
            this.btnDeleteDocuments.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDeleteDocuments.BackgroundImage")));
            this.btnDeleteDocuments.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteDocuments.ImageOptions.Image")));
            this.btnDeleteDocuments.Location = new System.Drawing.Point(146, 439);
            this.btnDeleteDocuments.Name = "btnDeleteDocuments";
            this.btnDeleteDocuments.Size = new System.Drawing.Size(41, 40);
            this.btnDeleteDocuments.TabIndex = 2;
            this.btnDeleteDocuments.ToolTip = "Delete checked documents";
            this.btnDeleteDocuments.Visible = false;
            this.btnDeleteDocuments.Click += new System.EventHandler(this.btnDeleteDocuments_Click);
            // 
            // btnDownloadDocuments
            // 
            this.btnDownloadDocuments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDownloadDocuments.Appearance.Options.UseImage = true;
            this.btnDownloadDocuments.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDownloadDocuments.BackgroundImage")));
            this.btnDownloadDocuments.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDownloadDocuments.ImageOptions.Image")));
            this.btnDownloadDocuments.Location = new System.Drawing.Point(5, 439);
            this.btnDownloadDocuments.Name = "btnDownloadDocuments";
            this.btnDownloadDocuments.Size = new System.Drawing.Size(41, 40);
            this.btnDownloadDocuments.TabIndex = 8;
            this.btnDownloadDocuments.ToolTip = "Download checked documents";
            this.btnDownloadDocuments.Click += new System.EventHandler(this.BtnDownloadDocuments_Click);
            // 
            // btnSendFaxType
            // 
            this.btnSendFaxType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSendFaxType.Location = new System.Drawing.Point(304, 698);
            this.btnSendFaxType.Name = "btnSendFaxType";
            this.btnSendFaxType.Size = new System.Drawing.Size(69, 23);
            this.btnSendFaxType.TabIndex = 18;
            this.btnSendFaxType.Text = "Send";
            this.btnSendFaxType.UseVisualStyleBackColor = true;
            this.btnSendFaxType.Click += new System.EventHandler(this.btnSendFaxType_Click);
            // 
            // btnSendEmailType
            // 
            this.btnSendEmailType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSendEmailType.Location = new System.Drawing.Point(304, 726);
            this.btnSendEmailType.Name = "btnSendEmailType";
            this.btnSendEmailType.Size = new System.Drawing.Size(69, 23);
            this.btnSendEmailType.TabIndex = 18;
            this.btnSendEmailType.Text = "Send";
            this.btnSendEmailType.UseVisualStyleBackColor = true;
            this.btnSendEmailType.Click += new System.EventHandler(this.btnSendEmailType_Click);
            // 
            // comboFaxTypes
            // 
            this.comboFaxTypes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboFaxTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboFaxTypes.FormattingEnabled = true;
            this.comboFaxTypes.Items.AddRange(new object[] {
            "Fax all selected documents"});
            this.comboFaxTypes.Location = new System.Drawing.Point(72, 699);
            this.comboFaxTypes.Name = "comboFaxTypes";
            this.comboFaxTypes.Size = new System.Drawing.Size(226, 21);
            this.comboFaxTypes.TabIndex = 17;
            // 
            // comboEmailTypes
            // 
            this.comboEmailTypes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboEmailTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboEmailTypes.FormattingEnabled = true;
            this.comboEmailTypes.Items.AddRange(new object[] {
            "Request for documents",
            "Blank e-mail with attached documents"});
            this.comboEmailTypes.Location = new System.Drawing.Point(72, 728);
            this.comboEmailTypes.Name = "comboEmailTypes";
            this.comboEmailTypes.Size = new System.Drawing.Size(226, 21);
            this.comboEmailTypes.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 704);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Faxes:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 732);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "e-mailers:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(5, 488);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Notes:";
            // 
            // textDocumentNotes
            // 
            this.textDocumentNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textDocumentNotes.Location = new System.Drawing.Point(5, 504);
            this.textDocumentNotes.Multiline = true;
            this.textDocumentNotes.Name = "textDocumentNotes";
            this.textDocumentNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textDocumentNotes.Size = new System.Drawing.Size(368, 94);
            this.textDocumentNotes.TabIndex = 11;
            // 
            // treeDocsAndTasks
            // 
            this.treeDocsAndTasks.AllowDrop = true;
            this.treeDocsAndTasks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeDocsAndTasks.CheckBoxes = true;
            this.treeDocsAndTasks.HideSelection = false;
            this.treeDocsAndTasks.Indent = 25;
            this.treeDocsAndTasks.ItemHeight = 19;
            this.treeDocsAndTasks.Location = new System.Drawing.Point(5, 23);
            this.treeDocsAndTasks.Name = "treeDocsAndTasks";
            treeNode1.ContextMenuStrip = this.contextRFI_Click;
            treeNode1.Name = "NodeDocuments";
            treeNode1.Text = "RFI";
            treeNode2.ContextMenuStrip = this.contextNoActions;
            treeNode2.Name = "NodeMiscDocuments";
            treeNode2.Text = "Misc Docs";
            treeNode3.Name = "NodeTasksClientFacing";
            treeNode3.Text = "Tasks – Client Facing";
            treeNode4.Name = "NodeTasksNonClientFacing";
            treeNode4.Text = "Tasks – Non Client Facing";
            treeNode5.ContextMenuStrip = this.contextNoActions;
            treeNode5.Name = "NodeUncategorizedDocuments";
            treeNode5.Text = "Uncategorized documents";
            treeNode6.ContextMenuStrip = this.contextNoActions;
            treeNode6.Name = "NodeRejectedDocuments";
            treeNode6.Text = "Rejected documents";
            this.treeDocsAndTasks.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode6});
            this.treeDocsAndTasks.Size = new System.Drawing.Size(368, 410);
            this.treeDocsAndTasks.TabIndex = 4;
            this.treeDocsAndTasks.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.treeDocsAndTasks_ItemDrag);
            this.treeDocsAndTasks.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeDocsAndTasks_AfterSelect);
            this.treeDocsAndTasks.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeDocsAndTasks_NodeMouseClick);
            this.treeDocsAndTasks.DragDrop += new System.Windows.Forms.DragEventHandler(this.treeDocsAndTasks_DragDrop);
            this.treeDocsAndTasks.DragEnter += new System.Windows.Forms.DragEventHandler(this.treeDocsAndTasks_DragEnter);
            this.treeDocsAndTasks.DragOver += new System.Windows.Forms.DragEventHandler(this.treeDocsAndTasks_DragOver);
            this.treeDocsAndTasks.DragLeave += new System.EventHandler(this.treeDocsAndTasks_DragLeave);
            // 
            // btnGenerate
            // 
            this.btnGenerate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGenerate.Location = new System.Drawing.Point(304, 753);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(69, 23);
            this.btnGenerate.TabIndex = 10;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.Click += new System.EventHandler(this.BtnGenerate_Click);
            // 
            // comboDocumentGenerator
            // 
            this.comboDocumentGenerator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboDocumentGenerator.Cursor = System.Windows.Forms.Cursors.Default;
            this.comboDocumentGenerator.Location = new System.Drawing.Point(72, 756);
            this.comboDocumentGenerator.Name = "comboDocumentGenerator";
            this.comboDocumentGenerator.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboDocumentGenerator.Properties.ImmediatePopup = true;
            this.comboDocumentGenerator.Properties.Items.AddRange(new object[] {
            "DE-101",
            "DE-112",
            "Medical",
            "DE-101 PAS",
            "DE-101 CSRA"});
            this.comboDocumentGenerator.Size = new System.Drawing.Size(226, 20);
            this.comboDocumentGenerator.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline);
            this.label1.Location = new System.Drawing.Point(3, 759);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Generators:";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Controls.Add(this.accordionControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(698, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(489, 786);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl4
            // 
            this.panelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl4.Controls.Add(this.simpleButton3);
            this.panelControl4.Controls.Add(this.btnSave);
            this.panelControl4.Controls.Add(this.simpleButton1);
            this.panelControl4.Location = new System.Drawing.Point(5, 745);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(482, 40);
            this.panelControl4.TabIndex = 2;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Location = new System.Drawing.Point(7, 9);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(75, 23);
            this.simpleButton3.TabIndex = 1;
            this.simpleButton3.Text = "Validate";
            this.simpleButton3.Click += new System.EventHandler(this.SimpleButton3_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(319, 9);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(400, 9);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Exit";
            this.simpleButton1.Click += new System.EventHandler(this.SimpleButton1_Click_1);
            // 
            // accordionControl1
            // 
            this.accordionControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.accordionControl1.Controls.Add(this.accordionContentContainer8);
            this.accordionControl1.Controls.Add(this.accordionContentContainer9);
            this.accordionControl1.Controls.Add(this.accordionContentContainer10);
            this.accordionControl1.Controls.Add(this.accordionContentContainer11);
            this.accordionControl1.Controls.Add(this.accordionContentContainer12);
            this.accordionControl1.Controls.Add(this.accordionContentContainer13);
            this.accordionControl1.Controls.Add(this.accordionContentContainer14);
            this.accordionControl1.Controls.Add(this.accordionContentContainer3);
            this.accordionControl1.Controls.Add(this.accordionContentContainer4);
            this.accordionControl1.Controls.Add(this.accordionContentContainer5);
            this.accordionControl1.Controls.Add(this.accordionContentContainer6);
            this.accordionControl1.Controls.Add(this.accordionContentContainer7);
            this.accordionControl1.Controls.Add(this.accordionContentContainer15);
            this.accordionControl1.Controls.Add(this.accordionContentContainer16);
            this.accordionControl1.Controls.Add(this.accordionContentContainer17);
            this.accordionControl1.Controls.Add(this.accordionContentContainer18);
            this.accordionControl1.Controls.Add(this.accordionContentContainer19);
            this.accordionControl1.Controls.Add(this.accordionContentContainer20);
            this.accordionControl1.Controls.Add(this.accordionContentContainer21);
            this.accordionControl1.Controls.Add(this.accordionContentContainer22);
            this.accordionControl1.Controls.Add(this.accordionContentContainer23);
            this.accordionControl1.Controls.Add(this.accordionContentContainer24);
            this.accordionControl1.Controls.Add(this.accordionContentContainer2);
            this.accordionControl1.Controls.Add(this.accordionContentContainer25);
            this.accordionControl1.Controls.Add(this.accordionContentContainer26);
            this.accordionControl1.Controls.Add(this.accordionContentContainer27);
            this.accordionControl1.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement3,
            this.accordionControlElement17,
            this.accordionControlElement1,
            this.accordionControlElement9,
            this.accordionControlElement27});
            this.accordionControl1.Location = new System.Drawing.Point(2, 2);
            this.accordionControl1.Name = "accordionControl1";
            this.accordionControl1.Size = new System.Drawing.Size(485, 737);
            this.accordionControl1.TabIndex = 1;
            this.accordionControl1.Text = "accordionControl1";
            // 
            // accordionContentContainer8
            // 
            this.accordionContentContainer8.Controls.Add(this.gridMedical_Parties);
            this.accordionContentContainer8.Controls.Add(this.btnMedicalParties_CreatreNewDOctor);
            this.accordionContentContainer8.Controls.Add(this.btnMedicalParties_AddDoctor);
            this.accordionContentContainer8.Controls.Add(this.label65);
            this.accordionContentContainer8.Controls.Add(this.comboMedical_Parties);
            this.accordionContentContainer8.Name = "accordionContentContainer8";
            this.accordionContentContainer8.Size = new System.Drawing.Size(468, 210);
            this.accordionContentContainer8.TabIndex = 9;
            // 
            // gridMedical_Parties
            // 
            this.gridMedical_Parties.AllowUserToAddRows = false;
            this.gridMedical_Parties.AllowUserToDeleteRows = false;
            this.gridMedical_Parties.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridMedical_Parties.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colMedical_Party_Id,
            this.colMedical_Party_DoctorName,
            this.colMedical_Party_Fax,
            this.colMedical_Party_Phone,
            this.colMedical_Party_Address});
            this.gridMedical_Parties.ContextMenuStrip = this.medicalContextMenu;
            this.gridMedical_Parties.Location = new System.Drawing.Point(7, 54);
            this.gridMedical_Parties.Name = "gridMedical_Parties";
            this.gridMedical_Parties.ReadOnly = true;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridMedical_Parties.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.gridMedical_Parties.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridMedical_Parties.Size = new System.Drawing.Size(447, 153);
            this.gridMedical_Parties.TabIndex = 4;
            // 
            // colMedical_Party_Id
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.colMedical_Party_Id.DefaultCellStyle = dataGridViewCellStyle1;
            this.colMedical_Party_Id.HeaderText = "Id";
            this.colMedical_Party_Id.Name = "colMedical_Party_Id";
            this.colMedical_Party_Id.ReadOnly = true;
            this.colMedical_Party_Id.Visible = false;
            // 
            // colMedical_Party_DoctorName
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.colMedical_Party_DoctorName.DefaultCellStyle = dataGridViewCellStyle2;
            this.colMedical_Party_DoctorName.HeaderText = "MD or Practice Name";
            this.colMedical_Party_DoctorName.Name = "colMedical_Party_DoctorName";
            this.colMedical_Party_DoctorName.ReadOnly = true;
            this.colMedical_Party_DoctorName.Width = 140;
            // 
            // colMedical_Party_Fax
            // 
            this.colMedical_Party_Fax.HeaderText = "Fax";
            this.colMedical_Party_Fax.Name = "colMedical_Party_Fax";
            this.colMedical_Party_Fax.ReadOnly = true;
            // 
            // colMedical_Party_Phone
            // 
            this.colMedical_Party_Phone.HeaderText = "Phone";
            this.colMedical_Party_Phone.Name = "colMedical_Party_Phone";
            this.colMedical_Party_Phone.ReadOnly = true;
            // 
            // colMedical_Party_Address
            // 
            this.colMedical_Party_Address.HeaderText = "Address";
            this.colMedical_Party_Address.Name = "colMedical_Party_Address";
            this.colMedical_Party_Address.ReadOnly = true;
            // 
            // medicalContextMenu
            // 
            this.medicalContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.medicalContextRemotePhysicians});
            this.medicalContextMenu.Name = "medicalContextMenu";
            this.medicalContextMenu.Size = new System.Drawing.Size(118, 26);
            this.medicalContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenuStrip1_Opening);
            // 
            // medicalContextRemotePhysicians
            // 
            this.medicalContextRemotePhysicians.Name = "medicalContextRemotePhysicians";
            this.medicalContextRemotePhysicians.Size = new System.Drawing.Size(117, 22);
            this.medicalContextRemotePhysicians.Text = "Remove";
            this.medicalContextRemotePhysicians.Click += new System.EventHandler(this.MedicalContextRemotePhysicians_Click);
            // 
            // btnMedicalParties_CreatreNewDOctor
            // 
            this.btnMedicalParties_CreatreNewDOctor.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnMedicalParties_CreatreNewDOctor.Location = new System.Drawing.Point(376, 0);
            this.btnMedicalParties_CreatreNewDOctor.Name = "btnMedicalParties_CreatreNewDOctor";
            this.btnMedicalParties_CreatreNewDOctor.Size = new System.Drawing.Size(78, 21);
            this.btnMedicalParties_CreatreNewDOctor.TabIndex = 3;
            this.btnMedicalParties_CreatreNewDOctor.Text = "Create new";
            this.btnMedicalParties_CreatreNewDOctor.UseVisualStyleBackColor = true;
            this.btnMedicalParties_CreatreNewDOctor.Click += new System.EventHandler(this.BtnMedicalParties_CreatreNewDOctor_Click);
            // 
            // btnMedicalParties_AddDoctor
            // 
            this.btnMedicalParties_AddDoctor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnMedicalParties_AddDoctor.Location = new System.Drawing.Point(376, 24);
            this.btnMedicalParties_AddDoctor.Name = "btnMedicalParties_AddDoctor";
            this.btnMedicalParties_AddDoctor.Size = new System.Drawing.Size(78, 23);
            this.btnMedicalParties_AddDoctor.TabIndex = 2;
            this.btnMedicalParties_AddDoctor.Text = "Add";
            this.btnMedicalParties_AddDoctor.UseVisualStyleBackColor = true;
            this.btnMedicalParties_AddDoctor.Click += new System.EventHandler(this.BtnMedicalParties_AddDoctor_Click);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label65.Location = new System.Drawing.Point(8, 10);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(254, 13);
            this.label65.TabIndex = 8;
            this.label65.Text = "Doctors / physicians (right click on table for actions):";
            // 
            // comboMedical_Parties
            // 
            this.comboMedical_Parties.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboMedical_Parties.FormattingEnabled = true;
            this.comboMedical_Parties.Location = new System.Drawing.Point(7, 26);
            this.comboMedical_Parties.Name = "comboMedical_Parties";
            this.comboMedical_Parties.Size = new System.Drawing.Size(363, 21);
            this.comboMedical_Parties.TabIndex = 1;
            // 
            // accordionContentContainer9
            // 
            this.accordionContentContainer9.Controls.Add(this.checkMedical_Disclosed_All);
            this.accordionContentContainer9.Controls.Add(this.groupMedical_Disclosed_Condition);
            this.accordionContentContainer9.Controls.Add(this.txtMedical_Disclosed_OtherEmail);
            this.accordionContentContainer9.Controls.Add(this.label71);
            this.accordionContentContainer9.Controls.Add(this.txtMedical_Disclosed_OtherText);
            this.accordionContentContainer9.Controls.Add(this.checkMedical_Disclosed_Other);
            this.accordionContentContainer9.Controls.Add(this.groupMedical_Disclosed_Period);
            this.accordionContentContainer9.Name = "accordionContentContainer9";
            this.accordionContentContainer9.Size = new System.Drawing.Size(468, 236);
            this.accordionContentContainer9.TabIndex = 10;
            // 
            // checkMedical_Disclosed_All
            // 
            this.checkMedical_Disclosed_All.AutoSize = true;
            this.checkMedical_Disclosed_All.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_Disclosed_All.Location = new System.Drawing.Point(13, 4);
            this.checkMedical_Disclosed_All.Name = "checkMedical_Disclosed_All";
            this.checkMedical_Disclosed_All.Size = new System.Drawing.Size(151, 17);
            this.checkMedical_Disclosed_All.TabIndex = 5;
            this.checkMedical_Disclosed_All.Text = "All of my health information";
            this.checkMedical_Disclosed_All.UseVisualStyleBackColor = true;
            // 
            // groupMedical_Disclosed_Condition
            // 
            this.groupMedical_Disclosed_Condition.Controls.Add(this.txtMedical_Disclosed_SpecificText);
            this.groupMedical_Disclosed_Condition.Controls.Add(this.label68);
            this.groupMedical_Disclosed_Condition.Controls.Add(this.checkMedical_Disclosed_Specific);
            this.groupMedical_Disclosed_Condition.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupMedical_Disclosed_Condition.Location = new System.Drawing.Point(7, 30);
            this.groupMedical_Disclosed_Condition.Name = "groupMedical_Disclosed_Condition";
            this.groupMedical_Disclosed_Condition.Size = new System.Drawing.Size(458, 56);
            this.groupMedical_Disclosed_Condition.TabIndex = 8;
            this.groupMedical_Disclosed_Condition.TabStop = false;
            // 
            // txtMedical_Disclosed_SpecificText
            // 
            this.txtMedical_Disclosed_SpecificText.Location = new System.Drawing.Point(133, 26);
            this.txtMedical_Disclosed_SpecificText.Name = "txtMedical_Disclosed_SpecificText";
            this.txtMedical_Disclosed_SpecificText.Size = new System.Drawing.Size(319, 20);
            this.txtMedical_Disclosed_SpecificText.TabIndex = 7;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(15, 29);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(116, 13);
            this.label68.TabIndex = 1;
            this.label68.Text = "Treatment or condition:";
            // 
            // checkMedical_Disclosed_Specific
            // 
            this.checkMedical_Disclosed_Specific.AutoSize = true;
            this.checkMedical_Disclosed_Specific.Location = new System.Drawing.Point(6, -1);
            this.checkMedical_Disclosed_Specific.Name = "checkMedical_Disclosed_Specific";
            this.checkMedical_Disclosed_Specific.Size = new System.Drawing.Size(345, 17);
            this.checkMedical_Disclosed_Specific.TabIndex = 6;
            this.checkMedical_Disclosed_Specific.Text = "My health information relating to the following treatment or condition:";
            this.checkMedical_Disclosed_Specific.UseVisualStyleBackColor = true;
            this.checkMedical_Disclosed_Specific.CheckedChanged += new System.EventHandler(this.CheckMedical_Disclosed_Specific_CheckedChanged);
            // 
            // txtMedical_Disclosed_OtherEmail
            // 
            this.txtMedical_Disclosed_OtherEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMedical_Disclosed_OtherEmail.Location = new System.Drawing.Point(115, 204);
            this.txtMedical_Disclosed_OtherEmail.Name = "txtMedical_Disclosed_OtherEmail";
            this.txtMedical_Disclosed_OtherEmail.Size = new System.Drawing.Size(350, 20);
            this.txtMedical_Disclosed_OtherEmail.TabIndex = 13;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label71.Location = new System.Drawing.Point(11, 208);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(101, 13);
            this.label71.TabIndex = 12;
            this.label71.Text = "Disclosed to (email):";
            // 
            // txtMedical_Disclosed_OtherText
            // 
            this.txtMedical_Disclosed_OtherText.Location = new System.Drawing.Point(75, 169);
            this.txtMedical_Disclosed_OtherText.Name = "txtMedical_Disclosed_OtherText";
            this.txtMedical_Disclosed_OtherText.Size = new System.Drawing.Size(390, 21);
            this.txtMedical_Disclosed_OtherText.TabIndex = 12;
            // 
            // checkMedical_Disclosed_Other
            // 
            this.checkMedical_Disclosed_Other.AutoSize = true;
            this.checkMedical_Disclosed_Other.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_Disclosed_Other.Location = new System.Drawing.Point(14, 172);
            this.checkMedical_Disclosed_Other.Name = "checkMedical_Disclosed_Other";
            this.checkMedical_Disclosed_Other.Size = new System.Drawing.Size(55, 17);
            this.checkMedical_Disclosed_Other.TabIndex = 11;
            this.checkMedical_Disclosed_Other.Text = "Other:";
            this.checkMedical_Disclosed_Other.UseVisualStyleBackColor = true;
            this.checkMedical_Disclosed_Other.CheckedChanged += new System.EventHandler(this.CheckMedical_Disclosed_Other_CheckedChanged);
            // 
            // groupMedical_Disclosed_Period
            // 
            this.groupMedical_Disclosed_Period.Controls.Add(this.dateMedical_Disclosed_PeriodTo);
            this.groupMedical_Disclosed_Period.Controls.Add(this.label70);
            this.groupMedical_Disclosed_Period.Controls.Add(this.dateMedical_Disclosed_PeriodFrom);
            this.groupMedical_Disclosed_Period.Controls.Add(this.label69);
            this.groupMedical_Disclosed_Period.Controls.Add(this.checkMedical_Disclosed_Period);
            this.groupMedical_Disclosed_Period.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupMedical_Disclosed_Period.Location = new System.Drawing.Point(7, 92);
            this.groupMedical_Disclosed_Period.Name = "groupMedical_Disclosed_Period";
            this.groupMedical_Disclosed_Period.Size = new System.Drawing.Size(458, 61);
            this.groupMedical_Disclosed_Period.TabIndex = 9;
            this.groupMedical_Disclosed_Period.TabStop = false;
            // 
            // dateMedical_Disclosed_PeriodTo
            // 
            this.dateMedical_Disclosed_PeriodTo.CustomFormat = "MM/dd/yyyy";
            this.dateMedical_Disclosed_PeriodTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateMedical_Disclosed_PeriodTo.Location = new System.Drawing.Point(220, 25);
            this.dateMedical_Disclosed_PeriodTo.Name = "dateMedical_Disclosed_PeriodTo";
            this.dateMedical_Disclosed_PeriodTo.Size = new System.Drawing.Size(107, 20);
            this.dateMedical_Disclosed_PeriodTo.TabIndex = 10;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(194, 28);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(23, 13);
            this.label70.TabIndex = 3;
            this.label70.Text = "To:";
            // 
            // dateMedical_Disclosed_PeriodFrom
            // 
            this.dateMedical_Disclosed_PeriodFrom.CustomFormat = "MM/dd/yyyy";
            this.dateMedical_Disclosed_PeriodFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateMedical_Disclosed_PeriodFrom.Location = new System.Drawing.Point(50, 25);
            this.dateMedical_Disclosed_PeriodFrom.Name = "dateMedical_Disclosed_PeriodFrom";
            this.dateMedical_Disclosed_PeriodFrom.Size = new System.Drawing.Size(107, 20);
            this.dateMedical_Disclosed_PeriodFrom.TabIndex = 9;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(15, 28);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(33, 13);
            this.label69.TabIndex = 1;
            this.label69.Text = "From:";
            // 
            // checkMedical_Disclosed_Period
            // 
            this.checkMedical_Disclosed_Period.AutoSize = true;
            this.checkMedical_Disclosed_Period.Location = new System.Drawing.Point(6, -1);
            this.checkMedical_Disclosed_Period.Name = "checkMedical_Disclosed_Period";
            this.checkMedical_Disclosed_Period.Size = new System.Drawing.Size(220, 17);
            this.checkMedical_Disclosed_Period.TabIndex = 8;
            this.checkMedical_Disclosed_Period.Text = "My health information covering the period";
            this.checkMedical_Disclosed_Period.UseVisualStyleBackColor = true;
            this.checkMedical_Disclosed_Period.CheckedChanged += new System.EventHandler(this.CheckMedical_Disclosed_Period_CheckedChanged);
            // 
            // accordionContentContainer10
            // 
            this.accordionContentContainer10.Controls.Add(this.checkMedical_Purpose_Sell);
            this.accordionContentContainer10.Controls.Add(this.checkMedical_Purpose_Comunicate);
            this.accordionContentContainer10.Controls.Add(this.txtMedical_Purpose_OtherText);
            this.accordionContentContainer10.Controls.Add(this.checkMedical_Purpose_Other);
            this.accordionContentContainer10.Controls.Add(this.checkMedical_Purpose_MyRequest);
            this.accordionContentContainer10.Name = "accordionContentContainer10";
            this.accordionContentContainer10.Size = new System.Drawing.Size(468, 107);
            this.accordionContentContainer10.TabIndex = 11;
            // 
            // checkMedical_Purpose_Sell
            // 
            this.checkMedical_Purpose_Sell.AutoSize = true;
            this.checkMedical_Purpose_Sell.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_Purpose_Sell.Location = new System.Drawing.Point(10, 80);
            this.checkMedical_Purpose_Sell.Name = "checkMedical_Purpose_Sell";
            this.checkMedical_Purpose_Sell.Size = new System.Drawing.Size(350, 17);
            this.checkMedical_Purpose_Sell.TabIndex = 11;
            this.checkMedical_Purpose_Sell.Text = "To authorize the using or disclosing party to sell my health information";
            this.checkMedical_Purpose_Sell.UseVisualStyleBackColor = true;
            // 
            // checkMedical_Purpose_Comunicate
            // 
            this.checkMedical_Purpose_Comunicate.AutoSize = true;
            this.checkMedical_Purpose_Comunicate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_Purpose_Comunicate.Location = new System.Drawing.Point(10, 56);
            this.checkMedical_Purpose_Comunicate.Name = "checkMedical_Purpose_Comunicate";
            this.checkMedical_Purpose_Comunicate.Size = new System.Drawing.Size(297, 17);
            this.checkMedical_Purpose_Comunicate.TabIndex = 10;
            this.checkMedical_Purpose_Comunicate.Text = "Authorize to communicate with me for marketing purposes";
            this.checkMedical_Purpose_Comunicate.UseVisualStyleBackColor = true;
            // 
            // txtMedical_Purpose_OtherText
            // 
            this.txtMedical_Purpose_OtherText.Location = new System.Drawing.Point(71, 28);
            this.txtMedical_Purpose_OtherText.Name = "txtMedical_Purpose_OtherText";
            this.txtMedical_Purpose_OtherText.Size = new System.Drawing.Size(362, 21);
            this.txtMedical_Purpose_OtherText.TabIndex = 9;
            // 
            // checkMedical_Purpose_Other
            // 
            this.checkMedical_Purpose_Other.AutoSize = true;
            this.checkMedical_Purpose_Other.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_Purpose_Other.Location = new System.Drawing.Point(10, 31);
            this.checkMedical_Purpose_Other.Name = "checkMedical_Purpose_Other";
            this.checkMedical_Purpose_Other.Size = new System.Drawing.Size(55, 17);
            this.checkMedical_Purpose_Other.TabIndex = 8;
            this.checkMedical_Purpose_Other.Text = "Other:";
            this.checkMedical_Purpose_Other.UseVisualStyleBackColor = true;
            this.checkMedical_Purpose_Other.CheckedChanged += new System.EventHandler(this.CheckMedical_Purpose_Other_CheckedChanged);
            // 
            // checkMedical_Purpose_MyRequest
            // 
            this.checkMedical_Purpose_MyRequest.AutoSize = true;
            this.checkMedical_Purpose_MyRequest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_Purpose_MyRequest.Location = new System.Drawing.Point(10, 8);
            this.checkMedical_Purpose_MyRequest.Name = "checkMedical_Purpose_MyRequest";
            this.checkMedical_Purpose_MyRequest.Size = new System.Drawing.Size(90, 17);
            this.checkMedical_Purpose_MyRequest.TabIndex = 7;
            this.checkMedical_Purpose_MyRequest.Text = "At my request";
            this.checkMedical_Purpose_MyRequest.UseVisualStyleBackColor = true;
            // 
            // accordionContentContainer11
            // 
            this.accordionContentContainer11.Controls.Add(this.dateMedical_AuthEnd_date);
            this.accordionContentContainer11.Controls.Add(this.txtMedical_AuthEnd_EventText);
            this.accordionContentContainer11.Controls.Add(this.checkMedical_AuthEnd_Event);
            this.accordionContentContainer11.Controls.Add(this.checkMedical_AuthEnd_OnDate);
            this.accordionContentContainer11.Name = "accordionContentContainer11";
            this.accordionContentContainer11.Size = new System.Drawing.Size(468, 66);
            this.accordionContentContainer11.TabIndex = 12;
            // 
            // dateMedical_AuthEnd_date
            // 
            this.dateMedical_AuthEnd_date.CustomFormat = "MM/dd/yyyy";
            this.dateMedical_AuthEnd_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateMedical_AuthEnd_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateMedical_AuthEnd_date.Location = new System.Drawing.Point(74, 2);
            this.dateMedical_AuthEnd_date.Name = "dateMedical_AuthEnd_date";
            this.dateMedical_AuthEnd_date.Size = new System.Drawing.Size(107, 20);
            this.dateMedical_AuthEnd_date.TabIndex = 11;
            // 
            // txtMedical_AuthEnd_EventText
            // 
            this.txtMedical_AuthEnd_EventText.Location = new System.Drawing.Point(188, 33);
            this.txtMedical_AuthEnd_EventText.Name = "txtMedical_AuthEnd_EventText";
            this.txtMedical_AuthEnd_EventText.Size = new System.Drawing.Size(241, 21);
            this.txtMedical_AuthEnd_EventText.TabIndex = 20;
            // 
            // checkMedical_AuthEnd_Event
            // 
            this.checkMedical_AuthEnd_Event.AutoSize = true;
            this.checkMedical_AuthEnd_Event.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_AuthEnd_Event.Location = new System.Drawing.Point(6, 36);
            this.checkMedical_AuthEnd_Event.Name = "checkMedical_AuthEnd_Event";
            this.checkMedical_AuthEnd_Event.Size = new System.Drawing.Size(185, 17);
            this.checkMedical_AuthEnd_Event.TabIndex = 15;
            this.checkMedical_AuthEnd_Event.Text = "When the following event occurs:";
            this.checkMedical_AuthEnd_Event.UseVisualStyleBackColor = true;
            this.checkMedical_AuthEnd_Event.CheckedChanged += new System.EventHandler(this.CheckMedical_AuthEnd_Event_CheckedChanged);
            // 
            // checkMedical_AuthEnd_OnDate
            // 
            this.checkMedical_AuthEnd_OnDate.AutoSize = true;
            this.checkMedical_AuthEnd_OnDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_AuthEnd_OnDate.Location = new System.Drawing.Point(6, 5);
            this.checkMedical_AuthEnd_OnDate.Name = "checkMedical_AuthEnd_OnDate";
            this.checkMedical_AuthEnd_OnDate.Size = new System.Drawing.Size(67, 17);
            this.checkMedical_AuthEnd_OnDate.TabIndex = 8;
            this.checkMedical_AuthEnd_OnDate.Text = "On date:";
            this.checkMedical_AuthEnd_OnDate.UseVisualStyleBackColor = true;
            this.checkMedical_AuthEnd_OnDate.CheckedChanged += new System.EventHandler(this.CheckMedical_AuthEnd_OnDate_CheckedChanged);
            // 
            // accordionContentContainer12
            // 
            this.accordionContentContainer12.Controls.Add(this.txtMedical_Rights_OtherText);
            this.accordionContentContainer12.Controls.Add(this.checkMedical_Rights_Other);
            this.accordionContentContainer12.Controls.Add(this.checkMedical_Rights_CountOrder);
            this.accordionContentContainer12.Controls.Add(this.checkMedical_Rights_LegalGuardian);
            this.accordionContentContainer12.Controls.Add(this.checkMedical_Rights_CantSign);
            this.accordionContentContainer12.Controls.Add(this.checkMedical_Rights_Parent);
            this.accordionContentContainer12.Controls.Add(this.label74);
            this.accordionContentContainer12.Controls.Add(this.label72);
            this.accordionContentContainer12.Controls.Add(this.dateMedical_Rights_AuthorizedRep_SignDate);
            this.accordionContentContainer12.Controls.Add(this.groupMedical_Rights_UnableToSign);
            this.accordionContentContainer12.Controls.Add(this.label73);
            this.accordionContentContainer12.Controls.Add(this.dateMedical_Rights_date);
            this.accordionContentContainer12.Name = "accordionContentContainer12";
            this.accordionContentContainer12.Size = new System.Drawing.Size(468, 229);
            this.accordionContentContainer12.TabIndex = 13;
            // 
            // txtMedical_Rights_OtherText
            // 
            this.txtMedical_Rights_OtherText.Location = new System.Drawing.Point(74, 192);
            this.txtMedical_Rights_OtherText.Name = "txtMedical_Rights_OtherText";
            this.txtMedical_Rights_OtherText.Size = new System.Drawing.Size(363, 21);
            this.txtMedical_Rights_OtherText.TabIndex = 34;
            // 
            // checkMedical_Rights_Other
            // 
            this.checkMedical_Rights_Other.AutoSize = true;
            this.checkMedical_Rights_Other.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_Rights_Other.Location = new System.Drawing.Point(11, 195);
            this.checkMedical_Rights_Other.Name = "checkMedical_Rights_Other";
            this.checkMedical_Rights_Other.Size = new System.Drawing.Size(55, 17);
            this.checkMedical_Rights_Other.TabIndex = 33;
            this.checkMedical_Rights_Other.Text = "Other:";
            this.checkMedical_Rights_Other.UseVisualStyleBackColor = true;
            this.checkMedical_Rights_Other.CheckedChanged += new System.EventHandler(this.CheckMedical_Rights_Other_CheckedChanged);
            // 
            // checkMedical_Rights_CountOrder
            // 
            this.checkMedical_Rights_CountOrder.AutoSize = true;
            this.checkMedical_Rights_CountOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_Rights_CountOrder.Location = new System.Drawing.Point(172, 172);
            this.checkMedical_Rights_CountOrder.Name = "checkMedical_Rights_CountOrder";
            this.checkMedical_Rights_CountOrder.Size = new System.Drawing.Size(80, 17);
            this.checkMedical_Rights_CountOrder.TabIndex = 32;
            this.checkMedical_Rights_CountOrder.Text = "Court Order";
            this.checkMedical_Rights_CountOrder.UseVisualStyleBackColor = true;
            // 
            // checkMedical_Rights_LegalGuardian
            // 
            this.checkMedical_Rights_LegalGuardian.AutoSize = true;
            this.checkMedical_Rights_LegalGuardian.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_Rights_LegalGuardian.Location = new System.Drawing.Point(74, 172);
            this.checkMedical_Rights_LegalGuardian.Name = "checkMedical_Rights_LegalGuardian";
            this.checkMedical_Rights_LegalGuardian.Size = new System.Drawing.Size(98, 17);
            this.checkMedical_Rights_LegalGuardian.TabIndex = 31;
            this.checkMedical_Rights_LegalGuardian.Text = "Legal Guardian";
            this.checkMedical_Rights_LegalGuardian.UseVisualStyleBackColor = true;
            // 
            // checkMedical_Rights_CantSign
            // 
            this.checkMedical_Rights_CantSign.AutoSize = true;
            this.checkMedical_Rights_CantSign.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_Rights_CantSign.Location = new System.Drawing.Point(13, 25);
            this.checkMedical_Rights_CantSign.Name = "checkMedical_Rights_CantSign";
            this.checkMedical_Rights_CantSign.Size = new System.Drawing.Size(187, 17);
            this.checkMedical_Rights_CantSign.TabIndex = 22;
            this.checkMedical_Rights_CantSign.Text = "Patient is a minor or unable to sign";
            this.checkMedical_Rights_CantSign.UseVisualStyleBackColor = true;
            this.checkMedical_Rights_CantSign.CheckedChanged += new System.EventHandler(this.CheckMedical_Rights_CantSign_CheckedChanged);
            // 
            // checkMedical_Rights_Parent
            // 
            this.checkMedical_Rights_Parent.AutoSize = true;
            this.checkMedical_Rights_Parent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_Rights_Parent.Location = new System.Drawing.Point(11, 172);
            this.checkMedical_Rights_Parent.Name = "checkMedical_Rights_Parent";
            this.checkMedical_Rights_Parent.Size = new System.Drawing.Size(57, 17);
            this.checkMedical_Rights_Parent.TabIndex = 30;
            this.checkMedical_Rights_Parent.Text = "Parent";
            this.checkMedical_Rights_Parent.UseVisualStyleBackColor = true;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label74.Location = new System.Drawing.Point(7, 153);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(282, 13);
            this.label74.TabIndex = 25;
            this.label74.Text = "Authority of representative to sign on behalf of the patient: ";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label72.Location = new System.Drawing.Point(5, 125);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(197, 13);
            this.label72.TabIndex = 24;
            this.label72.Text = "Date of Authorized Representative Sign:";
            // 
            // dateMedical_Rights_AuthorizedRep_SignDate
            // 
            this.dateMedical_Rights_AuthorizedRep_SignDate.CustomFormat = "MM/dd/yyyy";
            this.dateMedical_Rights_AuthorizedRep_SignDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateMedical_Rights_AuthorizedRep_SignDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateMedical_Rights_AuthorizedRep_SignDate.Location = new System.Drawing.Point(204, 121);
            this.dateMedical_Rights_AuthorizedRep_SignDate.Name = "dateMedical_Rights_AuthorizedRep_SignDate";
            this.dateMedical_Rights_AuthorizedRep_SignDate.Size = new System.Drawing.Size(107, 20);
            this.dateMedical_Rights_AuthorizedRep_SignDate.TabIndex = 27;
            // 
            // groupMedical_Rights_UnableToSign
            // 
            this.groupMedical_Rights_UnableToSign.Controls.Add(this.txtMedical_Rights_MinorAge);
            this.groupMedical_Rights_UnableToSign.Controls.Add(this.txtMedical_Rights_UnableToSignText);
            this.groupMedical_Rights_UnableToSign.Controls.Add(this.checkMedical_Rights_UnableToSign);
            this.groupMedical_Rights_UnableToSign.Controls.Add(this.checkMedical_Rights_SubMinor);
            this.groupMedical_Rights_UnableToSign.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupMedical_Rights_UnableToSign.Location = new System.Drawing.Point(8, 26);
            this.groupMedical_Rights_UnableToSign.Name = "groupMedical_Rights_UnableToSign";
            this.groupMedical_Rights_UnableToSign.Size = new System.Drawing.Size(429, 85);
            this.groupMedical_Rights_UnableToSign.TabIndex = 22;
            this.groupMedical_Rights_UnableToSign.TabStop = false;
            // 
            // txtMedical_Rights_MinorAge
            // 
            this.txtMedical_Rights_MinorAge.Location = new System.Drawing.Point(210, 20);
            this.txtMedical_Rights_MinorAge.Name = "txtMedical_Rights_MinorAge";
            this.txtMedical_Rights_MinorAge.Size = new System.Drawing.Size(47, 20);
            this.txtMedical_Rights_MinorAge.TabIndex = 24;
            // 
            // txtMedical_Rights_UnableToSignText
            // 
            this.txtMedical_Rights_UnableToSignText.Location = new System.Drawing.Point(210, 49);
            this.txtMedical_Rights_UnableToSignText.Name = "txtMedical_Rights_UnableToSignText";
            this.txtMedical_Rights_UnableToSignText.Size = new System.Drawing.Size(213, 20);
            this.txtMedical_Rights_UnableToSignText.TabIndex = 26;
            // 
            // checkMedical_Rights_UnableToSign
            // 
            this.checkMedical_Rights_UnableToSign.AutoSize = true;
            this.checkMedical_Rights_UnableToSign.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_Rights_UnableToSign.Location = new System.Drawing.Point(15, 52);
            this.checkMedical_Rights_UnableToSign.Name = "checkMedical_Rights_UnableToSign";
            this.checkMedical_Rights_UnableToSign.Size = new System.Drawing.Size(185, 17);
            this.checkMedical_Rights_UnableToSign.TabIndex = 25;
            this.checkMedical_Rights_UnableToSign.Text = "Patient is unable to sign because:";
            this.checkMedical_Rights_UnableToSign.UseVisualStyleBackColor = true;
            this.checkMedical_Rights_UnableToSign.CheckedChanged += new System.EventHandler(this.CheckMedical_Rights_UnableToSign_CheckedChanged);
            // 
            // checkMedical_Rights_SubMinor
            // 
            this.checkMedical_Rights_SubMinor.AutoSize = true;
            this.checkMedical_Rights_SubMinor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_Rights_SubMinor.Location = new System.Drawing.Point(15, 24);
            this.checkMedical_Rights_SubMinor.Name = "checkMedical_Rights_SubMinor";
            this.checkMedical_Rights_SubMinor.Size = new System.Drawing.Size(159, 17);
            this.checkMedical_Rights_SubMinor.TabIndex = 23;
            this.checkMedical_Rights_SubMinor.Text = "Patient is a minor, his age is:";
            this.checkMedical_Rights_SubMinor.UseVisualStyleBackColor = true;
            this.checkMedical_Rights_SubMinor.CheckedChanged += new System.EventHandler(this.CheckMedical_Rights_SubMinor_CheckedChanged);
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label73.Location = new System.Drawing.Point(4, 3);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(113, 13);
            this.label73.TabIndex = 21;
            this.label73.Text = "Patient signature date:";
            // 
            // dateMedical_Rights_date
            // 
            this.dateMedical_Rights_date.CustomFormat = "MM/dd/yyyy";
            this.dateMedical_Rights_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateMedical_Rights_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateMedical_Rights_date.Location = new System.Drawing.Point(120, 0);
            this.dateMedical_Rights_date.Name = "dateMedical_Rights_date";
            this.dateMedical_Rights_date.Size = new System.Drawing.Size(107, 20);
            this.dateMedical_Rights_date.TabIndex = 20;
            // 
            // accordionContentContainer13
            // 
            this.accordionContentContainer13.Controls.Add(this.label76);
            this.accordionContentContainer13.Controls.Add(this.dateMedical_AddCon_Time);
            this.accordionContentContainer13.Controls.Add(this.label75);
            this.accordionContentContainer13.Controls.Add(this.dateMedical_AddCon_Date);
            this.accordionContentContainer13.Controls.Add(this.checkMedical_AddCon_IDoNot);
            this.accordionContentContainer13.Controls.Add(this.checkMedical_AddCon_IDo);
            this.accordionContentContainer13.Name = "accordionContentContainer13";
            this.accordionContentContainer13.Size = new System.Drawing.Size(468, 105);
            this.accordionContentContainer13.TabIndex = 14;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label76.Location = new System.Drawing.Point(130, 54);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(33, 13);
            this.label76.TabIndex = 21;
            this.label76.Text = "Time:";
            // 
            // dateMedical_AddCon_Time
            // 
            this.dateMedical_AddCon_Time.CustomFormat = "hh:mm:ss";
            this.dateMedical_AddCon_Time.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateMedical_AddCon_Time.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateMedical_AddCon_Time.Location = new System.Drawing.Point(133, 70);
            this.dateMedical_AddCon_Time.Name = "dateMedical_AddCon_Time";
            this.dateMedical_AddCon_Time.Size = new System.Drawing.Size(107, 20);
            this.dateMedical_AddCon_Time.TabIndex = 20;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label75.Location = new System.Drawing.Point(6, 54);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(33, 13);
            this.label75.TabIndex = 19;
            this.label75.Text = "Date:";
            // 
            // dateMedical_AddCon_Date
            // 
            this.dateMedical_AddCon_Date.CustomFormat = "MM/dd/yyyy";
            this.dateMedical_AddCon_Date.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateMedical_AddCon_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateMedical_AddCon_Date.Location = new System.Drawing.Point(9, 70);
            this.dateMedical_AddCon_Date.Name = "dateMedical_AddCon_Date";
            this.dateMedical_AddCon_Date.Size = new System.Drawing.Size(107, 20);
            this.dateMedical_AddCon_Date.TabIndex = 18;
            // 
            // checkMedical_AddCon_IDoNot
            // 
            this.checkMedical_AddCon_IDoNot.AutoSize = true;
            this.checkMedical_AddCon_IDoNot.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_AddCon_IDoNot.Location = new System.Drawing.Point(6, 25);
            this.checkMedical_AddCon_IDoNot.Name = "checkMedical_AddCon_IDoNot";
            this.checkMedical_AddCon_IDoNot.Size = new System.Drawing.Size(290, 17);
            this.checkMedical_AddCon_IDoNot.TabIndex = 17;
            this.checkMedical_AddCon_IDoNot.Text = "I do not consent to have the above information released";
            this.checkMedical_AddCon_IDoNot.UseVisualStyleBackColor = true;
            // 
            // checkMedical_AddCon_IDo
            // 
            this.checkMedical_AddCon_IDo.AutoSize = true;
            this.checkMedical_AddCon_IDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_AddCon_IDo.Location = new System.Drawing.Point(6, 4);
            this.checkMedical_AddCon_IDo.Name = "checkMedical_AddCon_IDo";
            this.checkMedical_AddCon_IDo.Size = new System.Drawing.Size(257, 17);
            this.checkMedical_AddCon_IDo.TabIndex = 16;
            this.checkMedical_AddCon_IDo.Text = "I consent to have the above information released";
            this.checkMedical_AddCon_IDo.UseVisualStyleBackColor = true;
            // 
            // accordionContentContainer14
            // 
            this.accordionContentContainer14.Controls.Add(this.label77);
            this.accordionContentContainer14.Controls.Add(this.dateMedical_HIV_Time);
            this.accordionContentContainer14.Controls.Add(this.label78);
            this.accordionContentContainer14.Controls.Add(this.dateMedical_HIV_date);
            this.accordionContentContainer14.Controls.Add(this.checkMedical_HIV_IDoNot);
            this.accordionContentContainer14.Controls.Add(this.checkMedical_HIV_IDo);
            this.accordionContentContainer14.Name = "accordionContentContainer14";
            this.accordionContentContainer14.Size = new System.Drawing.Size(468, 100);
            this.accordionContentContainer14.TabIndex = 15;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label77.Location = new System.Drawing.Point(128, 54);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(33, 13);
            this.label77.TabIndex = 21;
            this.label77.Text = "Time:";
            // 
            // dateMedical_HIV_Time
            // 
            this.dateMedical_HIV_Time.CustomFormat = "hh:mm:ss";
            this.dateMedical_HIV_Time.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateMedical_HIV_Time.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateMedical_HIV_Time.Location = new System.Drawing.Point(131, 70);
            this.dateMedical_HIV_Time.Name = "dateMedical_HIV_Time";
            this.dateMedical_HIV_Time.Size = new System.Drawing.Size(107, 20);
            this.dateMedical_HIV_Time.TabIndex = 20;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label78.Location = new System.Drawing.Point(4, 54);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(33, 13);
            this.label78.TabIndex = 19;
            this.label78.Text = "Date:";
            // 
            // dateMedical_HIV_date
            // 
            this.dateMedical_HIV_date.CustomFormat = "MM/dd/yyyy";
            this.dateMedical_HIV_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateMedical_HIV_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateMedical_HIV_date.Location = new System.Drawing.Point(7, 70);
            this.dateMedical_HIV_date.Name = "dateMedical_HIV_date";
            this.dateMedical_HIV_date.Size = new System.Drawing.Size(107, 20);
            this.dateMedical_HIV_date.TabIndex = 18;
            // 
            // checkMedical_HIV_IDoNot
            // 
            this.checkMedical_HIV_IDoNot.AutoSize = true;
            this.checkMedical_HIV_IDoNot.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_HIV_IDoNot.Location = new System.Drawing.Point(4, 25);
            this.checkMedical_HIV_IDoNot.Name = "checkMedical_HIV_IDoNot";
            this.checkMedical_HIV_IDoNot.Size = new System.Drawing.Size(290, 17);
            this.checkMedical_HIV_IDoNot.TabIndex = 17;
            this.checkMedical_HIV_IDoNot.Text = "I do not consent to have the above information released";
            this.checkMedical_HIV_IDoNot.UseVisualStyleBackColor = true;
            // 
            // checkMedical_HIV_IDo
            // 
            this.checkMedical_HIV_IDo.AutoSize = true;
            this.checkMedical_HIV_IDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkMedical_HIV_IDo.Location = new System.Drawing.Point(4, 4);
            this.checkMedical_HIV_IDo.Name = "checkMedical_HIV_IDo";
            this.checkMedical_HIV_IDo.Size = new System.Drawing.Size(257, 17);
            this.checkMedical_HIV_IDo.TabIndex = 16;
            this.checkMedical_HIV_IDo.Text = "I consent to have the above information released";
            this.checkMedical_HIV_IDo.UseVisualStyleBackColor = true;
            // 
            // accordionContentContainer3
            // 
            this.accordionContentContainer3.Controls.Add(this.label36);
            this.accordionContentContainer3.Controls.Add(this.CustomerCurrentLiving_NameOfHospital);
            this.accordionContentContainer3.Controls.Add(this.CustomerCurrentLiving_ExpectedDateOfDischarge);
            this.accordionContentContainer3.Controls.Add(this.CustomerCurrentLiving_HospitalAddress);
            this.accordionContentContainer3.Controls.Add(this.label35);
            this.accordionContentContainer3.Controls.Add(this.groupBox5);
            this.accordionContentContainer3.Controls.Add(this.label42);
            this.accordionContentContainer3.Controls.Add(this.CustomerCurrentLiving_City);
            this.accordionContentContainer3.Controls.Add(this.label37);
            this.accordionContentContainer3.Controls.Add(this.label39);
            this.accordionContentContainer3.Controls.Add(this.CustomerCurrentLiving_ZipCode);
            this.accordionContentContainer3.Controls.Add(this.CustomerCurrentLiving_PhoneNumber);
            this.accordionContentContainer3.Controls.Add(this.label40);
            this.accordionContentContainer3.Controls.Add(this.label41);
            this.accordionContentContainer3.Controls.Add(this.CustomerCurrentLiving_State);
            this.accordionContentContainer3.Name = "accordionContentContainer3";
            this.accordionContentContainer3.Size = new System.Drawing.Size(0, 188);
            this.accordionContentContainer3.TabIndex = 4;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label36.Location = new System.Drawing.Point(274, 48);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(100, 13);
            this.label36.TabIndex = 18;
            this.label36.Text = "Name of the facility:";
            // 
            // CustomerCurrentLiving_NameOfHospital
            // 
            this.CustomerCurrentLiving_NameOfHospital.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CustomerCurrentLiving_NameOfHospital.Location = new System.Drawing.Point(277, 65);
            this.CustomerCurrentLiving_NameOfHospital.Name = "CustomerCurrentLiving_NameOfHospital";
            this.CustomerCurrentLiving_NameOfHospital.Size = new System.Drawing.Size(174, 20);
            this.CustomerCurrentLiving_NameOfHospital.TabIndex = 11;
            this.CustomerCurrentLiving_NameOfHospital.Tag = "";
            // 
            // CustomerCurrentLiving_ExpectedDateOfDischarge
            // 
            this.CustomerCurrentLiving_ExpectedDateOfDischarge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CustomerCurrentLiving_ExpectedDateOfDischarge.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.CustomerCurrentLiving_ExpectedDateOfDischarge.Location = new System.Drawing.Point(276, 18);
            this.CustomerCurrentLiving_ExpectedDateOfDischarge.Name = "CustomerCurrentLiving_ExpectedDateOfDischarge";
            this.CustomerCurrentLiving_ExpectedDateOfDischarge.Size = new System.Drawing.Size(175, 20);
            this.CustomerCurrentLiving_ExpectedDateOfDischarge.TabIndex = 10;
            this.CustomerCurrentLiving_ExpectedDateOfDischarge.Value = new System.DateTime(2019, 5, 9, 22, 9, 3, 0);
            // 
            // CustomerCurrentLiving_HospitalAddress
            // 
            this.CustomerCurrentLiving_HospitalAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CustomerCurrentLiving_HospitalAddress.Location = new System.Drawing.Point(2, 111);
            this.CustomerCurrentLiving_HospitalAddress.Name = "CustomerCurrentLiving_HospitalAddress";
            this.CustomerCurrentLiving_HospitalAddress.Size = new System.Drawing.Size(325, 20);
            this.CustomerCurrentLiving_HospitalAddress.TabIndex = 13;
            this.CustomerCurrentLiving_HospitalAddress.Tag = "";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label35.Location = new System.Drawing.Point(273, 1);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(144, 13);
            this.label35.TabIndex = 14;
            this.label35.Text = "Expected Date of Discharge:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.CustomerCurrentLivingOther_Text);
            this.groupBox5.Controls.Add(this.CustomerCurrentLiving_Other);
            this.groupBox5.Controls.Add(this.CustomerCurrentLiving_Nursing);
            this.groupBox5.Controls.Add(this.CustomerCurrentLiving_Home);
            this.groupBox5.Controls.Add(this.CustomerCurrentLiving_Hospital);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox5.Location = new System.Drawing.Point(4, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(255, 83);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Customer currently residing";
            // 
            // CustomerCurrentLivingOther_Text
            // 
            this.CustomerCurrentLivingOther_Text.Enabled = false;
            this.CustomerCurrentLivingOther_Text.Location = new System.Drawing.Point(67, 47);
            this.CustomerCurrentLivingOther_Text.Name = "CustomerCurrentLivingOther_Text";
            this.CustomerCurrentLivingOther_Text.Size = new System.Drawing.Size(172, 20);
            this.CustomerCurrentLivingOther_Text.TabIndex = 4;
            // 
            // CustomerCurrentLiving_Other
            // 
            this.CustomerCurrentLiving_Other.AutoSize = true;
            this.CustomerCurrentLiving_Other.Location = new System.Drawing.Point(7, 50);
            this.CustomerCurrentLiving_Other.Name = "CustomerCurrentLiving_Other";
            this.CustomerCurrentLiving_Other.Size = new System.Drawing.Size(54, 17);
            this.CustomerCurrentLiving_Other.TabIndex = 3;
            this.CustomerCurrentLiving_Other.Text = "Other:";
            this.CustomerCurrentLiving_Other.UseVisualStyleBackColor = true;
            this.CustomerCurrentLiving_Other.CheckedChanged += new System.EventHandler(this.CustomerCurrentLiving_Other_CheckedChanged);
            // 
            // CustomerCurrentLiving_Nursing
            // 
            this.CustomerCurrentLiving_Nursing.AutoSize = true;
            this.CustomerCurrentLiving_Nursing.Location = new System.Drawing.Point(79, 20);
            this.CustomerCurrentLiving_Nursing.Name = "CustomerCurrentLiving_Nursing";
            this.CustomerCurrentLiving_Nursing.Size = new System.Drawing.Size(96, 17);
            this.CustomerCurrentLiving_Nursing.TabIndex = 1;
            this.CustomerCurrentLiving_Nursing.Text = "Nursing Facility";
            this.CustomerCurrentLiving_Nursing.UseVisualStyleBackColor = true;
            // 
            // CustomerCurrentLiving_Home
            // 
            this.CustomerCurrentLiving_Home.AutoSize = true;
            this.CustomerCurrentLiving_Home.Checked = true;
            this.CustomerCurrentLiving_Home.Location = new System.Drawing.Point(180, 20);
            this.CustomerCurrentLiving_Home.Name = "CustomerCurrentLiving_Home";
            this.CustomerCurrentLiving_Home.Size = new System.Drawing.Size(66, 17);
            this.CustomerCurrentLiving_Home.TabIndex = 2;
            this.CustomerCurrentLiving_Home.TabStop = true;
            this.CustomerCurrentLiving_Home.Text = "At Home";
            this.CustomerCurrentLiving_Home.UseVisualStyleBackColor = true;
            // 
            // CustomerCurrentLiving_Hospital
            // 
            this.CustomerCurrentLiving_Hospital.AutoSize = true;
            this.CustomerCurrentLiving_Hospital.Location = new System.Drawing.Point(7, 20);
            this.CustomerCurrentLiving_Hospital.Name = "CustomerCurrentLiving_Hospital";
            this.CustomerCurrentLiving_Hospital.Size = new System.Drawing.Size(63, 17);
            this.CustomerCurrentLiving_Hospital.TabIndex = 0;
            this.CustomerCurrentLiving_Hospital.Text = "Hospital";
            this.CustomerCurrentLiving_Hospital.UseVisualStyleBackColor = true;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label42.Location = new System.Drawing.Point(-1, 94);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(254, 13);
            this.label42.TabIndex = 23;
            this.label42.Text = "Hospital, Assisted Living, or Nursing Facility Address:";
            // 
            // CustomerCurrentLiving_City
            // 
            this.CustomerCurrentLiving_City.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CustomerCurrentLiving_City.Location = new System.Drawing.Point(333, 111);
            this.CustomerCurrentLiving_City.Name = "CustomerCurrentLiving_City";
            this.CustomerCurrentLiving_City.Size = new System.Drawing.Size(118, 20);
            this.CustomerCurrentLiving_City.TabIndex = 15;
            this.CustomerCurrentLiving_City.Tag = "";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label37.Location = new System.Drawing.Point(330, 94);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(27, 13);
            this.label37.TabIndex = 16;
            this.label37.Text = "City:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label39.Location = new System.Drawing.Point(1, 140);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(35, 13);
            this.label39.TabIndex = 19;
            this.label39.Text = "State:";
            // 
            // CustomerCurrentLiving_ZipCode
            // 
            this.CustomerCurrentLiving_ZipCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CustomerCurrentLiving_ZipCode.Location = new System.Drawing.Point(109, 157);
            this.CustomerCurrentLiving_ZipCode.Name = "CustomerCurrentLiving_ZipCode";
            this.CustomerCurrentLiving_ZipCode.Size = new System.Drawing.Size(49, 20);
            this.CustomerCurrentLiving_ZipCode.TabIndex = 20;
            this.CustomerCurrentLiving_ZipCode.Tag = "";
            // 
            // CustomerCurrentLiving_PhoneNumber
            // 
            this.CustomerCurrentLiving_PhoneNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CustomerCurrentLiving_PhoneNumber.Location = new System.Drawing.Point(163, 157);
            this.CustomerCurrentLiving_PhoneNumber.Name = "CustomerCurrentLiving_PhoneNumber";
            this.CustomerCurrentLiving_PhoneNumber.Size = new System.Drawing.Size(107, 20);
            this.CustomerCurrentLiving_PhoneNumber.TabIndex = 22;
            this.CustomerCurrentLiving_PhoneNumber.Tag = "";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label40.Location = new System.Drawing.Point(160, 140);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(81, 13);
            this.label40.TabIndex = 12;
            this.label40.Text = "Phone Number:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label41.Location = new System.Drawing.Point(106, 140);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(52, 13);
            this.label41.TabIndex = 21;
            this.label41.Text = "Zip code:";
            // 
            // CustomerCurrentLiving_State
            // 
            this.CustomerCurrentLiving_State.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CustomerCurrentLiving_State.Location = new System.Drawing.Point(2, 157);
            this.CustomerCurrentLiving_State.Name = "CustomerCurrentLiving_State";
            this.CustomerCurrentLiving_State.Size = new System.Drawing.Size(102, 20);
            this.CustomerCurrentLiving_State.TabIndex = 17;
            this.CustomerCurrentLiving_State.Tag = "";
            // 
            // accordionContentContainer4
            // 
            this.accordionContentContainer4.Controls.Add(this.groupBox7);
            this.accordionContentContainer4.Name = "accordionContentContainer4";
            this.accordionContentContainer4.Size = new System.Drawing.Size(0, 186);
            this.accordionContentContainer4.TabIndex = 5;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.AccomodationsForPrintedLetters_Yes);
            this.groupBox7.Controls.Add(this.AccomodationsForPrintedLetters_No);
            this.groupBox7.Controls.Add(this.AccomodationsForPrintedLetters_Group);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox7.Location = new System.Drawing.Point(4, 0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(447, 178);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Does the customer, authorized representative, or legal guardian have a visual imp" +
    "airment that requires an alternative format for printed letters?";
            // 
            // AccomodationsForPrintedLetters_Yes
            // 
            this.AccomodationsForPrintedLetters_Yes.AutoSize = true;
            this.AccomodationsForPrintedLetters_Yes.Location = new System.Drawing.Point(55, 35);
            this.AccomodationsForPrintedLetters_Yes.Name = "AccomodationsForPrintedLetters_Yes";
            this.AccomodationsForPrintedLetters_Yes.Size = new System.Drawing.Size(43, 17);
            this.AccomodationsForPrintedLetters_Yes.TabIndex = 2;
            this.AccomodationsForPrintedLetters_Yes.Text = "Yes";
            this.AccomodationsForPrintedLetters_Yes.UseVisualStyleBackColor = true;
            this.AccomodationsForPrintedLetters_Yes.CheckedChanged += new System.EventHandler(this.AccomodationsForPrintedLetters_Yes_CheckedChanged);
            // 
            // AccomodationsForPrintedLetters_No
            // 
            this.AccomodationsForPrintedLetters_No.AutoSize = true;
            this.AccomodationsForPrintedLetters_No.Checked = true;
            this.AccomodationsForPrintedLetters_No.Location = new System.Drawing.Point(7, 35);
            this.AccomodationsForPrintedLetters_No.Name = "AccomodationsForPrintedLetters_No";
            this.AccomodationsForPrintedLetters_No.Size = new System.Drawing.Size(39, 17);
            this.AccomodationsForPrintedLetters_No.TabIndex = 1;
            this.AccomodationsForPrintedLetters_No.TabStop = true;
            this.AccomodationsForPrintedLetters_No.Text = "No";
            this.AccomodationsForPrintedLetters_No.UseVisualStyleBackColor = true;
            // 
            // AccomodationsForPrintedLetters_Group
            // 
            this.AccomodationsForPrintedLetters_Group.Controls.Add(this.AccomodationsForPrintedLetters_OtherText);
            this.AccomodationsForPrintedLetters_Group.Controls.Add(this.AccomodationsForPrintedLetters_Other);
            this.AccomodationsForPrintedLetters_Group.Controls.Add(this.AccomodationsForPrintedLetters_LargePrint);
            this.AccomodationsForPrintedLetters_Group.Controls.Add(this.AccomodationsForPrintedLetters_Email);
            this.AccomodationsForPrintedLetters_Group.Controls.Add(this.label43);
            this.AccomodationsForPrintedLetters_Group.Controls.Add(this.label38);
            this.AccomodationsForPrintedLetters_Group.Controls.Add(this.AccomodationsForPrintedLetters_WhoIfYes);
            this.AccomodationsForPrintedLetters_Group.Enabled = false;
            this.AccomodationsForPrintedLetters_Group.Location = new System.Drawing.Point(50, 37);
            this.AccomodationsForPrintedLetters_Group.Name = "AccomodationsForPrintedLetters_Group";
            this.AccomodationsForPrintedLetters_Group.Size = new System.Drawing.Size(391, 135);
            this.AccomodationsForPrintedLetters_Group.TabIndex = 3;
            this.AccomodationsForPrintedLetters_Group.TabStop = false;
            this.AccomodationsForPrintedLetters_Group.Text = "Yes";
            // 
            // AccomodationsForPrintedLetters_OtherText
            // 
            this.AccomodationsForPrintedLetters_OtherText.Enabled = false;
            this.AccomodationsForPrintedLetters_OtherText.Location = new System.Drawing.Point(65, 107);
            this.AccomodationsForPrintedLetters_OtherText.Name = "AccomodationsForPrintedLetters_OtherText";
            this.AccomodationsForPrintedLetters_OtherText.Size = new System.Drawing.Size(320, 20);
            this.AccomodationsForPrintedLetters_OtherText.TabIndex = 8;
            // 
            // AccomodationsForPrintedLetters_Other
            // 
            this.AccomodationsForPrintedLetters_Other.AutoSize = true;
            this.AccomodationsForPrintedLetters_Other.Location = new System.Drawing.Point(14, 110);
            this.AccomodationsForPrintedLetters_Other.Name = "AccomodationsForPrintedLetters_Other";
            this.AccomodationsForPrintedLetters_Other.Size = new System.Drawing.Size(55, 17);
            this.AccomodationsForPrintedLetters_Other.TabIndex = 7;
            this.AccomodationsForPrintedLetters_Other.Text = "Other:";
            this.AccomodationsForPrintedLetters_Other.UseVisualStyleBackColor = true;
            this.AccomodationsForPrintedLetters_Other.CheckedChanged += new System.EventHandler(this.AccomodationsForPrintedLetters_Other_CheckedChanged);
            // 
            // AccomodationsForPrintedLetters_LargePrint
            // 
            this.AccomodationsForPrintedLetters_LargePrint.AutoSize = true;
            this.AccomodationsForPrintedLetters_LargePrint.Location = new System.Drawing.Point(14, 87);
            this.AccomodationsForPrintedLetters_LargePrint.Name = "AccomodationsForPrintedLetters_LargePrint";
            this.AccomodationsForPrintedLetters_LargePrint.Size = new System.Drawing.Size(356, 17);
            this.AccomodationsForPrintedLetters_LargePrint.TabIndex = 6;
            this.AccomodationsForPrintedLetters_LargePrint.Text = "Large print: letters sent by U.S. mail will be provided Arial 24 point font.";
            this.AccomodationsForPrintedLetters_LargePrint.UseVisualStyleBackColor = true;
            // 
            // AccomodationsForPrintedLetters_Email
            // 
            this.AccomodationsForPrintedLetters_Email.AutoSize = true;
            this.AccomodationsForPrintedLetters_Email.Location = new System.Drawing.Point(14, 64);
            this.AccomodationsForPrintedLetters_Email.Name = "AccomodationsForPrintedLetters_Email";
            this.AccomodationsForPrintedLetters_Email.Size = new System.Drawing.Size(195, 17);
            this.AccomodationsForPrintedLetters_Email.TabIndex = 5;
            this.AccomodationsForPrintedLetters_Email.Text = "Readable PDF sent by secure email";
            this.AccomodationsForPrintedLetters_Email.UseVisualStyleBackColor = true;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(11, 47);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(220, 13);
            this.label43.TabIndex = 10;
            this.label43.Text = "What kind of alternative format do you need?";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(10, 20);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(164, 13);
            this.label38.TabIndex = 8;
            this.label38.Text = "Who needs the accommodation: ";
            // 
            // AccomodationsForPrintedLetters_WhoIfYes
            // 
            this.AccomodationsForPrintedLetters_WhoIfYes.Location = new System.Drawing.Point(175, 17);
            this.AccomodationsForPrintedLetters_WhoIfYes.Name = "AccomodationsForPrintedLetters_WhoIfYes";
            this.AccomodationsForPrintedLetters_WhoIfYes.Size = new System.Drawing.Size(210, 20);
            this.AccomodationsForPrintedLetters_WhoIfYes.TabIndex = 4;
            // 
            // accordionContentContainer5
            // 
            this.accordionContentContainer5.Controls.Add(this.groupBox1);
            this.accordionContentContainer5.Controls.Add(this.groupBox16);
            this.accordionContentContainer5.Controls.Add(this.groupBox15);
            this.accordionContentContainer5.Controls.Add(this.groupBox14);
            this.accordionContentContainer5.Controls.Add(this.groupBox12);
            this.accordionContentContainer5.Controls.Add(this.groupBox10);
            this.accordionContentContainer5.Name = "accordionContentContainer5";
            this.accordionContentContainer5.Size = new System.Drawing.Size(0, 413);
            this.accordionContentContainer5.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.isPregnant_Yes);
            this.groupBox1.Controls.Add(this.isPregnant_No);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(8, 359);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(447, 49);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Is the customer pregnant or had a pregnancy end in the last 5 months?";
            // 
            // isPregnant_Yes
            // 
            this.isPregnant_Yes.AutoSize = true;
            this.isPregnant_Yes.Location = new System.Drawing.Point(68, 19);
            this.isPregnant_Yes.Name = "isPregnant_Yes";
            this.isPregnant_Yes.Size = new System.Drawing.Size(43, 17);
            this.isPregnant_Yes.TabIndex = 1;
            this.isPregnant_Yes.Text = "Yes";
            this.isPregnant_Yes.UseVisualStyleBackColor = true;
            // 
            // isPregnant_No
            // 
            this.isPregnant_No.AutoSize = true;
            this.isPregnant_No.Checked = true;
            this.isPregnant_No.Location = new System.Drawing.Point(20, 19);
            this.isPregnant_No.Name = "isPregnant_No";
            this.isPregnant_No.Size = new System.Drawing.Size(39, 17);
            this.isPregnant_No.TabIndex = 0;
            this.isPregnant_No.TabStop = true;
            this.isPregnant_No.Text = "No";
            this.isPregnant_No.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.AdditionalQuestions_5_Yes);
            this.groupBox16.Controls.Add(this.AdditionalQuestions_5_No);
            this.groupBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox16.Location = new System.Drawing.Point(8, 304);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(447, 49);
            this.groupBox16.TabIndex = 10;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Is the customer a trustor, trustee, or beneficiary of any type of trust? ";
            // 
            // AdditionalQuestions_5_Yes
            // 
            this.AdditionalQuestions_5_Yes.AutoSize = true;
            this.AdditionalQuestions_5_Yes.Location = new System.Drawing.Point(68, 19);
            this.AdditionalQuestions_5_Yes.Name = "AdditionalQuestions_5_Yes";
            this.AdditionalQuestions_5_Yes.Size = new System.Drawing.Size(43, 17);
            this.AdditionalQuestions_5_Yes.TabIndex = 1;
            this.AdditionalQuestions_5_Yes.Text = "Yes";
            this.AdditionalQuestions_5_Yes.UseVisualStyleBackColor = true;
            // 
            // AdditionalQuestions_5_No
            // 
            this.AdditionalQuestions_5_No.AutoSize = true;
            this.AdditionalQuestions_5_No.Checked = true;
            this.AdditionalQuestions_5_No.Location = new System.Drawing.Point(20, 19);
            this.AdditionalQuestions_5_No.Name = "AdditionalQuestions_5_No";
            this.AdditionalQuestions_5_No.Size = new System.Drawing.Size(39, 17);
            this.AdditionalQuestions_5_No.TabIndex = 0;
            this.AdditionalQuestions_5_No.TabStop = true;
            this.AdditionalQuestions_5_No.Text = "No";
            this.AdditionalQuestions_5_No.UseVisualStyleBackColor = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.AdditionalQuestions_4_Yes);
            this.groupBox15.Controls.Add(this.AdditionalQuestions_4_No);
            this.groupBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox15.Location = new System.Drawing.Point(8, 249);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(447, 49);
            this.groupBox15.TabIndex = 9;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "If the customer is under age of 6, has he been diagnosed with Developmental Delay" +
    "?";
            // 
            // AdditionalQuestions_4_Yes
            // 
            this.AdditionalQuestions_4_Yes.AutoSize = true;
            this.AdditionalQuestions_4_Yes.Location = new System.Drawing.Point(68, 19);
            this.AdditionalQuestions_4_Yes.Name = "AdditionalQuestions_4_Yes";
            this.AdditionalQuestions_4_Yes.Size = new System.Drawing.Size(43, 17);
            this.AdditionalQuestions_4_Yes.TabIndex = 1;
            this.AdditionalQuestions_4_Yes.Text = "Yes";
            this.AdditionalQuestions_4_Yes.UseVisualStyleBackColor = true;
            // 
            // AdditionalQuestions_4_No
            // 
            this.AdditionalQuestions_4_No.AutoSize = true;
            this.AdditionalQuestions_4_No.Checked = true;
            this.AdditionalQuestions_4_No.Location = new System.Drawing.Point(20, 19);
            this.AdditionalQuestions_4_No.Name = "AdditionalQuestions_4_No";
            this.AdditionalQuestions_4_No.Size = new System.Drawing.Size(39, 17);
            this.AdditionalQuestions_4_No.TabIndex = 0;
            this.AdditionalQuestions_4_No.TabStop = true;
            this.AdditionalQuestions_4_No.Text = "No";
            this.AdditionalQuestions_4_No.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.AdditionalQuestions_3_Seizure);
            this.groupBox14.Controls.Add(this.AdditionalQuestions_3_Intellectual);
            this.groupBox14.Controls.Add(this.AdditionalQuestions_3_CerebralPalsy);
            this.groupBox14.Controls.Add(this.AdditionalQuestions_3_Autism);
            this.groupBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox14.Location = new System.Drawing.Point(7, 173);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(447, 69);
            this.groupBox14.TabIndex = 8;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Prior to the age of 18 was the customer diagnosed with any of the following condi" +
    "tions?";
            // 
            // AdditionalQuestions_3_Seizure
            // 
            this.AdditionalQuestions_3_Seizure.AutoSize = true;
            this.AdditionalQuestions_3_Seizure.Location = new System.Drawing.Point(234, 43);
            this.AdditionalQuestions_3_Seizure.Name = "AdditionalQuestions_3_Seizure";
            this.AdditionalQuestions_3_Seizure.Size = new System.Drawing.Size(103, 17);
            this.AdditionalQuestions_3_Seizure.TabIndex = 3;
            this.AdditionalQuestions_3_Seizure.Text = "Seizure Disorder";
            this.AdditionalQuestions_3_Seizure.UseVisualStyleBackColor = true;
            // 
            // AdditionalQuestions_3_Intellectual
            // 
            this.AdditionalQuestions_3_Intellectual.AutoSize = true;
            this.AdditionalQuestions_3_Intellectual.Location = new System.Drawing.Point(21, 43);
            this.AdditionalQuestions_3_Intellectual.Name = "AdditionalQuestions_3_Intellectual";
            this.AdditionalQuestions_3_Intellectual.Size = new System.Drawing.Size(170, 17);
            this.AdditionalQuestions_3_Intellectual.TabIndex = 2;
            this.AdditionalQuestions_3_Intellectual.Text = "Intellectual/Cognitive Disability";
            this.AdditionalQuestions_3_Intellectual.UseVisualStyleBackColor = true;
            // 
            // AdditionalQuestions_3_CerebralPalsy
            // 
            this.AdditionalQuestions_3_CerebralPalsy.AutoSize = true;
            this.AdditionalQuestions_3_CerebralPalsy.Location = new System.Drawing.Point(234, 20);
            this.AdditionalQuestions_3_CerebralPalsy.Name = "AdditionalQuestions_3_CerebralPalsy";
            this.AdditionalQuestions_3_CerebralPalsy.Size = new System.Drawing.Size(96, 17);
            this.AdditionalQuestions_3_CerebralPalsy.TabIndex = 1;
            this.AdditionalQuestions_3_CerebralPalsy.Text = "Cerebral Palsy ";
            this.AdditionalQuestions_3_CerebralPalsy.UseVisualStyleBackColor = true;
            // 
            // AdditionalQuestions_3_Autism
            // 
            this.AdditionalQuestions_3_Autism.AutoSize = true;
            this.AdditionalQuestions_3_Autism.Location = new System.Drawing.Point(21, 20);
            this.AdditionalQuestions_3_Autism.Name = "AdditionalQuestions_3_Autism";
            this.AdditionalQuestions_3_Autism.Size = new System.Drawing.Size(57, 17);
            this.AdditionalQuestions_3_Autism.TabIndex = 0;
            this.AdditionalQuestions_3_Autism.Text = "Autism";
            this.AdditionalQuestions_3_Autism.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.AdditionalQuestions_2_Yes);
            this.groupBox12.Controls.Add(this.AdditionalQuestions_2_No);
            this.groupBox12.Controls.Add(this.groupBox13);
            this.groupBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox12.Location = new System.Drawing.Point(7, 88);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(447, 79);
            this.groupBox12.TabIndex = 7;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Is the customer receiving services from the DES Division of Developmental Disabil" +
    "ities? ";
            // 
            // AdditionalQuestions_2_Yes
            // 
            this.AdditionalQuestions_2_Yes.AutoSize = true;
            this.AdditionalQuestions_2_Yes.Location = new System.Drawing.Point(69, 20);
            this.AdditionalQuestions_2_Yes.Name = "AdditionalQuestions_2_Yes";
            this.AdditionalQuestions_2_Yes.Size = new System.Drawing.Size(43, 17);
            this.AdditionalQuestions_2_Yes.TabIndex = 1;
            this.AdditionalQuestions_2_Yes.Text = "Yes";
            this.AdditionalQuestions_2_Yes.UseVisualStyleBackColor = true;
            this.AdditionalQuestions_2_Yes.CheckedChanged += new System.EventHandler(this.AdditionalQuestions_2_Yes_CheckedChanged);
            // 
            // AdditionalQuestions_2_No
            // 
            this.AdditionalQuestions_2_No.AutoSize = true;
            this.AdditionalQuestions_2_No.Checked = true;
            this.AdditionalQuestions_2_No.Location = new System.Drawing.Point(21, 20);
            this.AdditionalQuestions_2_No.Name = "AdditionalQuestions_2_No";
            this.AdditionalQuestions_2_No.Size = new System.Drawing.Size(39, 17);
            this.AdditionalQuestions_2_No.TabIndex = 0;
            this.AdditionalQuestions_2_No.TabStop = true;
            this.AdditionalQuestions_2_No.Text = "No";
            this.AdditionalQuestions_2_No.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.AdditionalQuestions_2_Yes_Date);
            this.groupBox13.Controls.Add(this.label44);
            this.groupBox13.Location = new System.Drawing.Point(65, 23);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(376, 50);
            this.groupBox13.TabIndex = 2;
            this.groupBox13.TabStop = false;
            // 
            // AdditionalQuestions_2_Yes_Date
            // 
            this.AdditionalQuestions_2_Yes_Date.Enabled = false;
            this.AdditionalQuestions_2_Yes_Date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.AdditionalQuestions_2_Yes_Date.Location = new System.Drawing.Point(136, 17);
            this.AdditionalQuestions_2_Yes_Date.Name = "AdditionalQuestions_2_Yes_Date";
            this.AdditionalQuestions_2_Yes_Date.Size = new System.Drawing.Size(136, 20);
            this.AdditionalQuestions_2_Yes_Date.TabIndex = 0;
            this.AdditionalQuestions_2_Yes_Date.Value = new System.DateTime(2019, 5, 9, 22, 9, 3, 0);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(22, 20);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(108, 13);
            this.label44.TabIndex = 8;
            this.label44.Text = "Date services began:";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.AdditionalQuestions_1_Yes);
            this.groupBox10.Controls.Add(this.AdditionalQuestions_1_No);
            this.groupBox10.Controls.Add(this.groupBox11);
            this.groupBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox10.Location = new System.Drawing.Point(7, 3);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(447, 79);
            this.groupBox10.TabIndex = 6;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "   Does the customer need help paying for medical bills:";
            // 
            // AdditionalQuestions_1_Yes
            // 
            this.AdditionalQuestions_1_Yes.AutoSize = true;
            this.AdditionalQuestions_1_Yes.Location = new System.Drawing.Point(69, 20);
            this.AdditionalQuestions_1_Yes.Name = "AdditionalQuestions_1_Yes";
            this.AdditionalQuestions_1_Yes.Size = new System.Drawing.Size(43, 17);
            this.AdditionalQuestions_1_Yes.TabIndex = 2;
            this.AdditionalQuestions_1_Yes.Text = "Yes";
            this.AdditionalQuestions_1_Yes.UseVisualStyleBackColor = true;
            this.AdditionalQuestions_1_Yes.CheckedChanged += new System.EventHandler(this.AdditionalQuestions_1_Yes_CheckedChanged);
            // 
            // AdditionalQuestions_1_No
            // 
            this.AdditionalQuestions_1_No.AutoSize = true;
            this.AdditionalQuestions_1_No.Checked = true;
            this.AdditionalQuestions_1_No.Location = new System.Drawing.Point(21, 20);
            this.AdditionalQuestions_1_No.Name = "AdditionalQuestions_1_No";
            this.AdditionalQuestions_1_No.Size = new System.Drawing.Size(39, 17);
            this.AdditionalQuestions_1_No.TabIndex = 1;
            this.AdditionalQuestions_1_No.TabStop = true;
            this.AdditionalQuestions_1_No.Text = "No";
            this.AdditionalQuestions_1_No.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label45);
            this.groupBox11.Controls.Add(this.AdditionalQuestions_1_Yes_Month3);
            this.groupBox11.Controls.Add(this.AdditionalQuestions_1_Yes_Month2);
            this.groupBox11.Controls.Add(this.AdditionalQuestions_1_Yes_Month1);
            this.groupBox11.Location = new System.Drawing.Point(65, 23);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(376, 50);
            this.groupBox11.TabIndex = 3;
            this.groupBox11.TabStop = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(20, 20);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(76, 13);
            this.label45.TabIndex = 8;
            this.label45.Text = "What months?";
            // 
            // AdditionalQuestions_1_Yes_Month3
            // 
            this.AdditionalQuestions_1_Yes_Month3.Enabled = false;
            this.AdditionalQuestions_1_Yes_Month3.Location = new System.Drawing.Point(284, 17);
            this.AdditionalQuestions_1_Yes_Month3.Name = "AdditionalQuestions_1_Yes_Month3";
            this.AdditionalQuestions_1_Yes_Month3.Size = new System.Drawing.Size(82, 20);
            this.AdditionalQuestions_1_Yes_Month3.TabIndex = 2;
            // 
            // AdditionalQuestions_1_Yes_Month2
            // 
            this.AdditionalQuestions_1_Yes_Month2.Enabled = false;
            this.AdditionalQuestions_1_Yes_Month2.Location = new System.Drawing.Point(190, 17);
            this.AdditionalQuestions_1_Yes_Month2.Name = "AdditionalQuestions_1_Yes_Month2";
            this.AdditionalQuestions_1_Yes_Month2.Size = new System.Drawing.Size(82, 20);
            this.AdditionalQuestions_1_Yes_Month2.TabIndex = 1;
            // 
            // AdditionalQuestions_1_Yes_Month1
            // 
            this.AdditionalQuestions_1_Yes_Month1.Enabled = false;
            this.AdditionalQuestions_1_Yes_Month1.Location = new System.Drawing.Point(96, 17);
            this.AdditionalQuestions_1_Yes_Month1.Name = "AdditionalQuestions_1_Yes_Month1";
            this.AdditionalQuestions_1_Yes_Month1.Size = new System.Drawing.Size(82, 20);
            this.AdditionalQuestions_1_Yes_Month1.TabIndex = 0;
            // 
            // accordionContentContainer6
            // 
            this.accordionContentContainer6.Controls.Add(this.groupBox19);
            this.accordionContentContainer6.Controls.Add(this.Interview_Thursday_Time);
            this.accordionContentContainer6.Controls.Add(this.Interview_Tuesday_Time);
            this.accordionContentContainer6.Controls.Add(this.label49);
            this.accordionContentContainer6.Controls.Add(this.label47);
            this.accordionContentContainer6.Controls.Add(this.Interview_Friday_Time);
            this.accordionContentContainer6.Controls.Add(this.Interview_Wednesday_Time);
            this.accordionContentContainer6.Controls.Add(this.Interview_Thursday);
            this.accordionContentContainer6.Controls.Add(this.label50);
            this.accordionContentContainer6.Controls.Add(this.Interview_Monday_Time);
            this.accordionContentContainer6.Controls.Add(this.label48);
            this.accordionContentContainer6.Controls.Add(this.Interview_Friday);
            this.accordionContentContainer6.Controls.Add(this.Interview_Tuesday);
            this.accordionContentContainer6.Controls.Add(this.Interview_Wednesday);
            this.accordionContentContainer6.Controls.Add(this.label46);
            this.accordionContentContainer6.Controls.Add(this.Interview_Monday);
            this.accordionContentContainer6.Name = "accordionContentContainer6";
            this.accordionContentContainer6.Size = new System.Drawing.Size(0, 162);
            this.accordionContentContainer6.TabIndex = 7;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.InterviewInterpretter_Yes);
            this.groupBox19.Controls.Add(this.InterviewInterpretter_No);
            this.groupBox19.Controls.Add(this.groupBox20);
            this.groupBox19.Controls.Add(this.InterviewInterpretter_MajorCrossroads);
            this.groupBox19.Controls.Add(this.label52);
            this.groupBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox19.Location = new System.Drawing.Point(7, 79);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(447, 79);
            this.groupBox19.TabIndex = 30;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Does the person completing the interview need an interpreter?";
            // 
            // InterviewInterpretter_Yes
            // 
            this.InterviewInterpretter_Yes.AutoSize = true;
            this.InterviewInterpretter_Yes.Location = new System.Drawing.Point(69, 20);
            this.InterviewInterpretter_Yes.Name = "InterviewInterpretter_Yes";
            this.InterviewInterpretter_Yes.Size = new System.Drawing.Size(43, 17);
            this.InterviewInterpretter_Yes.TabIndex = 1;
            this.InterviewInterpretter_Yes.Text = "Yes";
            this.InterviewInterpretter_Yes.UseVisualStyleBackColor = true;
            this.InterviewInterpretter_Yes.CheckedChanged += new System.EventHandler(this.InterviewInterpretter_Yes_CheckedChanged);
            // 
            // InterviewInterpretter_No
            // 
            this.InterviewInterpretter_No.AutoSize = true;
            this.InterviewInterpretter_No.Checked = true;
            this.InterviewInterpretter_No.Location = new System.Drawing.Point(21, 20);
            this.InterviewInterpretter_No.Name = "InterviewInterpretter_No";
            this.InterviewInterpretter_No.Size = new System.Drawing.Size(39, 17);
            this.InterviewInterpretter_No.TabIndex = 0;
            this.InterviewInterpretter_No.TabStop = true;
            this.InterviewInterpretter_No.Text = "No";
            this.InterviewInterpretter_No.UseVisualStyleBackColor = true;
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.label51);
            this.groupBox20.Controls.Add(this.InterviewInterpretter_Language);
            this.groupBox20.Controls.Add(this.InterviewInterpretter_HomeAddress);
            this.groupBox20.Location = new System.Drawing.Point(65, 23);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(376, 50);
            this.groupBox20.TabIndex = 2;
            this.groupBox20.TabStop = false;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(20, 20);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(86, 13);
            this.label51.TabIndex = 8;
            this.label51.Text = "What language?";
            // 
            // InterviewInterpretter_Language
            // 
            this.InterviewInterpretter_Language.Enabled = false;
            this.InterviewInterpretter_Language.Location = new System.Drawing.Point(110, 17);
            this.InterviewInterpretter_Language.Name = "InterviewInterpretter_Language";
            this.InterviewInterpretter_Language.Size = new System.Drawing.Size(256, 20);
            this.InterviewInterpretter_Language.TabIndex = 0;
            // 
            // InterviewInterpretter_HomeAddress
            // 
            this.InterviewInterpretter_HomeAddress.AccessibleDescription = "Address or location for home visit";
            this.InterviewInterpretter_HomeAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.InterviewInterpretter_HomeAddress.Location = new System.Drawing.Point(110, 17);
            this.InterviewInterpretter_HomeAddress.Name = "InterviewInterpretter_HomeAddress";
            this.InterviewInterpretter_HomeAddress.Size = new System.Drawing.Size(26, 20);
            this.InterviewInterpretter_HomeAddress.TabIndex = 31;
            this.InterviewInterpretter_HomeAddress.Tag = "1";
            this.InterviewInterpretter_HomeAddress.Visible = false;
            // 
            // InterviewInterpretter_MajorCrossroads
            // 
            this.InterviewInterpretter_MajorCrossroads.AccessibleDescription = "Major crossroads";
            this.InterviewInterpretter_MajorCrossroads.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.InterviewInterpretter_MajorCrossroads.Location = new System.Drawing.Point(259, 48);
            this.InterviewInterpretter_MajorCrossroads.Name = "InterviewInterpretter_MajorCrossroads";
            this.InterviewInterpretter_MajorCrossroads.Size = new System.Drawing.Size(110, 20);
            this.InterviewInterpretter_MajorCrossroads.TabIndex = 32;
            this.InterviewInterpretter_MajorCrossroads.Tag = "1";
            this.InterviewInterpretter_MajorCrossroads.Visible = false;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label52.Location = new System.Drawing.Point(-3, -17);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(165, 13);
            this.label52.TabIndex = 26;
            this.label52.Text = "Address or location for home visit:";
            this.label52.Visible = false;
            // 
            // Interview_Thursday_Time
            // 
            this.Interview_Thursday_Time.Enabled = false;
            this.Interview_Thursday_Time.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Interview_Thursday_Time.Location = new System.Drawing.Point(349, 26);
            this.Interview_Thursday_Time.Name = "Interview_Thursday_Time";
            this.Interview_Thursday_Time.Size = new System.Drawing.Size(107, 20);
            this.Interview_Thursday_Time.TabIndex = 25;
            // 
            // Interview_Tuesday_Time
            // 
            this.Interview_Tuesday_Time.Enabled = false;
            this.Interview_Tuesday_Time.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Interview_Tuesday_Time.Location = new System.Drawing.Point(349, 0);
            this.Interview_Tuesday_Time.Name = "Interview_Tuesday_Time";
            this.Interview_Tuesday_Time.Size = new System.Drawing.Size(107, 20);
            this.Interview_Tuesday_Time.TabIndex = 21;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label49.Location = new System.Drawing.Point(315, 29);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(33, 13);
            this.label49.TabIndex = 14;
            this.label49.Text = "Time:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label47.Location = new System.Drawing.Point(315, 3);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(33, 13);
            this.label47.TabIndex = 15;
            this.label47.Text = "Time:";
            // 
            // Interview_Friday_Time
            // 
            this.Interview_Friday_Time.Enabled = false;
            this.Interview_Friday_Time.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Interview_Friday_Time.Location = new System.Drawing.Point(119, 53);
            this.Interview_Friday_Time.Name = "Interview_Friday_Time";
            this.Interview_Friday_Time.Size = new System.Drawing.Size(107, 20);
            this.Interview_Friday_Time.TabIndex = 28;
            // 
            // Interview_Wednesday_Time
            // 
            this.Interview_Wednesday_Time.Enabled = false;
            this.Interview_Wednesday_Time.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Interview_Wednesday_Time.Location = new System.Drawing.Point(119, 27);
            this.Interview_Wednesday_Time.Name = "Interview_Wednesday_Time";
            this.Interview_Wednesday_Time.Size = new System.Drawing.Size(107, 20);
            this.Interview_Wednesday_Time.TabIndex = 23;
            // 
            // Interview_Thursday
            // 
            this.Interview_Thursday.AutoSize = true;
            this.Interview_Thursday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Interview_Thursday.Location = new System.Drawing.Point(252, 28);
            this.Interview_Thursday.Name = "Interview_Thursday";
            this.Interview_Thursday.Size = new System.Drawing.Size(70, 17);
            this.Interview_Thursday.TabIndex = 24;
            this.Interview_Thursday.Text = "Thursday";
            this.Interview_Thursday.UseVisualStyleBackColor = true;
            this.Interview_Thursday.CheckedChanged += new System.EventHandler(this.Interview_Thursday_CheckedChanged);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label50.Location = new System.Drawing.Point(85, 56);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(33, 13);
            this.label50.TabIndex = 17;
            this.label50.Text = "Time:";
            // 
            // Interview_Monday_Time
            // 
            this.Interview_Monday_Time.Enabled = false;
            this.Interview_Monday_Time.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Interview_Monday_Time.Location = new System.Drawing.Point(119, 1);
            this.Interview_Monday_Time.Name = "Interview_Monday_Time";
            this.Interview_Monday_Time.Size = new System.Drawing.Size(107, 20);
            this.Interview_Monday_Time.TabIndex = 18;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label48.Location = new System.Drawing.Point(85, 30);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(33, 13);
            this.label48.TabIndex = 19;
            this.label48.Text = "Time:";
            // 
            // Interview_Friday
            // 
            this.Interview_Friday.AutoSize = true;
            this.Interview_Friday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Interview_Friday.Location = new System.Drawing.Point(10, 55);
            this.Interview_Friday.Name = "Interview_Friday";
            this.Interview_Friday.Size = new System.Drawing.Size(54, 17);
            this.Interview_Friday.TabIndex = 27;
            this.Interview_Friday.Text = "Friday";
            this.Interview_Friday.UseVisualStyleBackColor = true;
            this.Interview_Friday.CheckedChanged += new System.EventHandler(this.Interview_Friday_CheckedChanged);
            // 
            // Interview_Tuesday
            // 
            this.Interview_Tuesday.AutoSize = true;
            this.Interview_Tuesday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Interview_Tuesday.Location = new System.Drawing.Point(252, 2);
            this.Interview_Tuesday.Name = "Interview_Tuesday";
            this.Interview_Tuesday.Size = new System.Drawing.Size(67, 17);
            this.Interview_Tuesday.TabIndex = 20;
            this.Interview_Tuesday.Text = "Tuesday";
            this.Interview_Tuesday.UseVisualStyleBackColor = true;
            this.Interview_Tuesday.CheckedChanged += new System.EventHandler(this.Interview_Tuesday_CheckedChanged);
            // 
            // Interview_Wednesday
            // 
            this.Interview_Wednesday.AutoSize = true;
            this.Interview_Wednesday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Interview_Wednesday.Location = new System.Drawing.Point(10, 29);
            this.Interview_Wednesday.Name = "Interview_Wednesday";
            this.Interview_Wednesday.Size = new System.Drawing.Size(83, 17);
            this.Interview_Wednesday.TabIndex = 22;
            this.Interview_Wednesday.Text = "Wednesday";
            this.Interview_Wednesday.UseVisualStyleBackColor = true;
            this.Interview_Wednesday.CheckedChanged += new System.EventHandler(this.Interview_Wednesday_CheckedChanged);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label46.Location = new System.Drawing.Point(85, 4);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(33, 13);
            this.label46.TabIndex = 16;
            this.label46.Text = "Time:";
            // 
            // Interview_Monday
            // 
            this.Interview_Monday.AutoSize = true;
            this.Interview_Monday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Interview_Monday.Location = new System.Drawing.Point(10, 3);
            this.Interview_Monday.Name = "Interview_Monday";
            this.Interview_Monday.Size = new System.Drawing.Size(64, 17);
            this.Interview_Monday.TabIndex = 13;
            this.Interview_Monday.Text = "Monday";
            this.Interview_Monday.UseVisualStyleBackColor = true;
            this.Interview_Monday.CheckedChanged += new System.EventHandler(this.Interview_Monday_CheckedChanged);
            // 
            // accordionContentContainer7
            // 
            this.accordionContentContainer7.Controls.Add(this.groupBox22);
            this.accordionContentContainer7.Controls.Add(this.PersonCompleteingTheForm_Phone);
            this.accordionContentContainer7.Controls.Add(this.label55);
            this.accordionContentContainer7.Controls.Add(this.PersonCompleteingTheForm_Name);
            this.accordionContentContainer7.Controls.Add(this.label54);
            this.accordionContentContainer7.Name = "accordionContentContainer7";
            this.accordionContentContainer7.Size = new System.Drawing.Size(0, 102);
            this.accordionContentContainer7.TabIndex = 8;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.PersonCompleteingTheForm_IsParent);
            this.groupBox22.Controls.Add(this.PersonCompleteingTheForm_IsSpouse);
            this.groupBox22.Controls.Add(this.PersonCompleteingTheForm_IsCustomer);
            this.groupBox22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox22.Location = new System.Drawing.Point(7, 44);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(452, 51);
            this.groupBox22.TabIndex = 13;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "The person completing this form is the:";
            // 
            // PersonCompleteingTheForm_IsParent
            // 
            this.PersonCompleteingTheForm_IsParent.AutoSize = true;
            this.PersonCompleteingTheForm_IsParent.Location = new System.Drawing.Point(277, 20);
            this.PersonCompleteingTheForm_IsParent.Name = "PersonCompleteingTheForm_IsParent";
            this.PersonCompleteingTheForm_IsParent.Size = new System.Drawing.Size(132, 17);
            this.PersonCompleteingTheForm_IsParent.TabIndex = 2;
            this.PersonCompleteingTheForm_IsParent.Text = "Parent of the customer";
            this.PersonCompleteingTheForm_IsParent.UseVisualStyleBackColor = true;
            // 
            // PersonCompleteingTheForm_IsSpouse
            // 
            this.PersonCompleteingTheForm_IsSpouse.AutoSize = true;
            this.PersonCompleteingTheForm_IsSpouse.Location = new System.Drawing.Point(110, 20);
            this.PersonCompleteingTheForm_IsSpouse.Name = "PersonCompleteingTheForm_IsSpouse";
            this.PersonCompleteingTheForm_IsSpouse.Size = new System.Drawing.Size(137, 17);
            this.PersonCompleteingTheForm_IsSpouse.TabIndex = 1;
            this.PersonCompleteingTheForm_IsSpouse.Text = "Spouse of the customer";
            this.PersonCompleteingTheForm_IsSpouse.UseVisualStyleBackColor = true;
            // 
            // PersonCompleteingTheForm_IsCustomer
            // 
            this.PersonCompleteingTheForm_IsCustomer.AutoSize = true;
            this.PersonCompleteingTheForm_IsCustomer.Checked = true;
            this.PersonCompleteingTheForm_IsCustomer.Location = new System.Drawing.Point(12, 20);
            this.PersonCompleteingTheForm_IsCustomer.Name = "PersonCompleteingTheForm_IsCustomer";
            this.PersonCompleteingTheForm_IsCustomer.Size = new System.Drawing.Size(69, 17);
            this.PersonCompleteingTheForm_IsCustomer.TabIndex = 0;
            this.PersonCompleteingTheForm_IsCustomer.TabStop = true;
            this.PersonCompleteingTheForm_IsCustomer.Text = "Customer";
            this.PersonCompleteingTheForm_IsCustomer.UseVisualStyleBackColor = true;
            // 
            // PersonCompleteingTheForm_Phone
            // 
            this.PersonCompleteingTheForm_Phone.AccessibleDescription = "Phone of person completeing form";
            this.PersonCompleteingTheForm_Phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PersonCompleteingTheForm_Phone.Location = new System.Drawing.Point(301, 17);
            this.PersonCompleteingTheForm_Phone.Name = "PersonCompleteingTheForm_Phone";
            this.PersonCompleteingTheForm_Phone.Size = new System.Drawing.Size(158, 20);
            this.PersonCompleteingTheForm_Phone.TabIndex = 12;
            this.PersonCompleteingTheForm_Phone.Tag = "1";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label55.Location = new System.Drawing.Point(298, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(81, 13);
            this.label55.TabIndex = 14;
            this.label55.Text = "Phone Number:";
            // 
            // PersonCompleteingTheForm_Name
            // 
            this.PersonCompleteingTheForm_Name.AccessibleDescription = "Name of person completeing form";
            this.PersonCompleteingTheForm_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PersonCompleteingTheForm_Name.Location = new System.Drawing.Point(7, 17);
            this.PersonCompleteingTheForm_Name.Name = "PersonCompleteingTheForm_Name";
            this.PersonCompleteingTheForm_Name.Size = new System.Drawing.Size(288, 20);
            this.PersonCompleteingTheForm_Name.TabIndex = 11;
            this.PersonCompleteingTheForm_Name.Tag = "1";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label54.Location = new System.Drawing.Point(4, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(167, 13);
            this.label54.TabIndex = 15;
            this.label54.Text = "Name of Person Completing Form:";
            // 
            // accordionContentContainer15
            // 
            this.accordionContentContainer15.Controls.Add(this.btnAddNewMedicalRepresentative);
            this.accordionContentContainer15.Controls.Add(this.btnEditSelectedMedicalRepresentative);
            this.accordionContentContainer15.Controls.Add(this.comboMedicalRepresentative);
            this.accordionContentContainer15.Controls.Add(this.label13);
            this.accordionContentContainer15.Controls.Add(this.btnAddNewRepresentative);
            this.accordionContentContainer15.Controls.Add(this.btnEditRepresentative);
            this.accordionContentContainer15.Controls.Add(this.comboFinancialRepresentative);
            this.accordionContentContainer15.Controls.Add(this.label64);
            this.accordionContentContainer15.Controls.Add(this.labelControl77);
            this.accordionContentContainer15.Controls.Add(this.labelControl76);
            this.accordionContentContainer15.Controls.Add(this.applicant_MailingAddressCountry);
            this.accordionContentContainer15.Controls.Add(this.labelControl69);
            this.accordionContentContainer15.Controls.Add(this.applicant_MailingAddressZip);
            this.accordionContentContainer15.Controls.Add(this.labelControl70);
            this.accordionContentContainer15.Controls.Add(this.applicant_MailingAddressState);
            this.accordionContentContainer15.Controls.Add(this.labelControl71);
            this.accordionContentContainer15.Controls.Add(this.applicant_MailingAddressCity);
            this.accordionContentContainer15.Controls.Add(this.labelControl72);
            this.accordionContentContainer15.Controls.Add(this.applicant_MailingAddressLine2);
            this.accordionContentContainer15.Controls.Add(this.labelControl73);
            this.accordionContentContainer15.Controls.Add(this.labelControl74);
            this.accordionContentContainer15.Controls.Add(this.applicant_MailingAddressStreet);
            this.accordionContentContainer15.Controls.Add(this.labelControl75);
            this.accordionContentContainer15.Controls.Add(this.applicant_Email);
            this.accordionContentContainer15.Controls.Add(this.labelControl67);
            this.accordionContentContainer15.Controls.Add(this.applicant_Phone);
            this.accordionContentContainer15.Controls.Add(this.labelControl66);
            this.accordionContentContainer15.Controls.Add(this.applicant_AddressCountry);
            this.accordionContentContainer15.Controls.Add(this.labelControl59);
            this.accordionContentContainer15.Controls.Add(this.applicant_AddressZip);
            this.accordionContentContainer15.Controls.Add(this.labelControl60);
            this.accordionContentContainer15.Controls.Add(this.applicant_AddressState);
            this.accordionContentContainer15.Controls.Add(this.labelControl61);
            this.accordionContentContainer15.Controls.Add(this.applicant_AddressCity);
            this.accordionContentContainer15.Controls.Add(this.labelControl62);
            this.accordionContentContainer15.Controls.Add(this.applicant_AddressLine2);
            this.accordionContentContainer15.Controls.Add(this.labelControl63);
            this.accordionContentContainer15.Controls.Add(this.labelControl64);
            this.accordionContentContainer15.Controls.Add(this.applicant_AddressStreet);
            this.accordionContentContainer15.Controls.Add(this.labelControl65);
            this.accordionContentContainer15.Controls.Add(this.applicant_MilitaryToYear);
            this.accordionContentContainer15.Controls.Add(this.applicant_MilitaryFromYear);
            this.accordionContentContainer15.Controls.Add(this.labelControl56);
            this.accordionContentContainer15.Controls.Add(this.applicant_MilitaryBranch);
            this.accordionContentContainer15.Controls.Add(this.labelControl58);
            this.accordionContentContainer15.Controls.Add(this.labelControl57);
            this.accordionContentContainer15.Controls.Add(this.labelControl55);
            this.accordionContentContainer15.Controls.Add(this.applicant_IsMilitary);
            this.accordionContentContainer15.Controls.Add(this.applicant_SpouseSSN);
            this.accordionContentContainer15.Controls.Add(this.labelControl68);
            this.accordionContentContainer15.Controls.Add(this.applicant_SpouseLastName);
            this.accordionContentContainer15.Controls.Add(this.labelControl52);
            this.accordionContentContainer15.Controls.Add(this.applicant_SpouseMiddleName);
            this.accordionContentContainer15.Controls.Add(this.labelControl53);
            this.accordionContentContainer15.Controls.Add(this.applicant_SpouseFirstName);
            this.accordionContentContainer15.Controls.Add(this.labelControl54);
            this.accordionContentContainer15.Controls.Add(this.applicant_DateOfMarrgiage);
            this.accordionContentContainer15.Controls.Add(this.label84);
            this.accordionContentContainer15.Controls.Add(this.applicant_SpouceDateOfBirth);
            this.accordionContentContainer15.Controls.Add(this.label83);
            this.accordionContentContainer15.Controls.Add(this.applicant_MaritalStatus);
            this.accordionContentContainer15.Controls.Add(this.labelControl51);
            this.accordionContentContainer15.Controls.Add(this.applicant_DateOfBirth);
            this.accordionContentContainer15.Controls.Add(this.label82);
            this.accordionContentContainer15.Controls.Add(this.label81);
            this.accordionContentContainer15.Controls.Add(this.applicant_Gender);
            this.accordionContentContainer15.Controls.Add(this.applicant_SSN);
            this.accordionContentContainer15.Controls.Add(this.applicant_LastName);
            this.accordionContentContainer15.Controls.Add(this.labelControl48);
            this.accordionContentContainer15.Controls.Add(this.applicant_MiddleName);
            this.accordionContentContainer15.Controls.Add(this.labelControl49);
            this.accordionContentContainer15.Controls.Add(this.applicant_FirstName);
            this.accordionContentContainer15.Controls.Add(this.labelControl50);
            this.accordionContentContainer15.Controls.Add(this.applicant_LivingAt);
            this.accordionContentContainer15.Controls.Add(this.labelControl3);
            this.accordionContentContainer15.Controls.Add(this.applicant_AlreadyOnAHCCCSProgram);
            this.accordionContentContainer15.Controls.Add(this.applicant_IsRegisteredVoter);
            this.accordionContentContainer15.Controls.Add(this.applicant_Ethnicity);
            this.accordionContentContainer15.Controls.Add(this.labelControl2);
            this.accordionContentContainer15.Controls.Add(this.applicant_PlaceOfBirth);
            this.accordionContentContainer15.Controls.Add(this.labelControl1);
            this.accordionContentContainer15.Controls.Add(this.applicant_IsUSCitizen);
            this.accordionContentContainer15.Name = "accordionContentContainer15";
            this.accordionContentContainer15.Size = new System.Drawing.Size(0, 810);
            this.accordionContentContainer15.TabIndex = 16;
            // 
            // btnAddNewMedicalRepresentative
            // 
            this.btnAddNewMedicalRepresentative.Location = new System.Drawing.Point(369, 389);
            this.btnAddNewMedicalRepresentative.Name = "btnAddNewMedicalRepresentative";
            this.btnAddNewMedicalRepresentative.Size = new System.Drawing.Size(40, 21);
            this.btnAddNewMedicalRepresentative.TabIndex = 89;
            this.btnAddNewMedicalRepresentative.Text = "New";
            this.btnAddNewMedicalRepresentative.Click += new System.EventHandler(this.btnAddNewMedicalRepresentative_Click);
            // 
            // btnEditSelectedMedicalRepresentative
            // 
            this.btnEditSelectedMedicalRepresentative.Location = new System.Drawing.Point(415, 389);
            this.btnEditSelectedMedicalRepresentative.Name = "btnEditSelectedMedicalRepresentative";
            this.btnEditSelectedMedicalRepresentative.Size = new System.Drawing.Size(43, 21);
            this.btnEditSelectedMedicalRepresentative.TabIndex = 90;
            this.btnEditSelectedMedicalRepresentative.Text = "Edit";
            this.btnEditSelectedMedicalRepresentative.Click += new System.EventHandler(this.btnEditSelectedMedicalRepresentative_Click);
            // 
            // comboMedicalRepresentative
            // 
            this.comboMedicalRepresentative.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMedicalRepresentative.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboMedicalRepresentative.FormattingEnabled = true;
            this.comboMedicalRepresentative.Location = new System.Drawing.Point(10, 389);
            this.comboMedicalRepresentative.Name = "comboMedicalRepresentative";
            this.comboMedicalRepresentative.Size = new System.Drawing.Size(353, 21);
            this.comboMedicalRepresentative.TabIndex = 88;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(7, 372);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(145, 13);
            this.label13.TabIndex = 91;
            this.label13.Text = "Medical Representative:";
            // 
            // btnAddNewRepresentative
            // 
            this.btnAddNewRepresentative.Location = new System.Drawing.Point(369, 343);
            this.btnAddNewRepresentative.Name = "btnAddNewRepresentative";
            this.btnAddNewRepresentative.Size = new System.Drawing.Size(40, 21);
            this.btnAddNewRepresentative.TabIndex = 25;
            this.btnAddNewRepresentative.Text = "New";
            this.btnAddNewRepresentative.Click += new System.EventHandler(this.BtnAddNewRepresentative_Click);
            // 
            // btnEditRepresentative
            // 
            this.btnEditRepresentative.Location = new System.Drawing.Point(415, 343);
            this.btnEditRepresentative.Name = "btnEditRepresentative";
            this.btnEditRepresentative.Size = new System.Drawing.Size(43, 21);
            this.btnEditRepresentative.TabIndex = 26;
            this.btnEditRepresentative.Text = "Edit";
            this.btnEditRepresentative.Click += new System.EventHandler(this.BtnEditRepresentative_Click);
            // 
            // comboFinancialRepresentative
            // 
            this.comboFinancialRepresentative.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboFinancialRepresentative.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboFinancialRepresentative.FormattingEnabled = true;
            this.comboFinancialRepresentative.Location = new System.Drawing.Point(10, 343);
            this.comboFinancialRepresentative.Name = "comboFinancialRepresentative";
            this.comboFinancialRepresentative.Size = new System.Drawing.Size(353, 21);
            this.comboFinancialRepresentative.TabIndex = 24;
            this.comboFinancialRepresentative.SelectedIndexChanged += new System.EventHandler(this.ComboRepresentatives_SelectedIndexChanged);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label64.Location = new System.Drawing.Point(7, 326);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(152, 13);
            this.label64.TabIndex = 87;
            this.label64.Text = "Financial Representative:";
            // 
            // labelControl77
            // 
            this.labelControl77.Location = new System.Drawing.Point(155, 91);
            this.labelControl77.Name = "labelControl77";
            this.labelControl77.Size = new System.Drawing.Size(23, 13);
            this.labelControl77.TabIndex = 86;
            this.labelControl77.Text = "SSN:";
            // 
            // labelControl76
            // 
            this.labelControl76.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl76.Location = new System.Drawing.Point(176, 649);
            this.labelControl76.Name = "labelControl76";
            this.labelControl76.Size = new System.Drawing.Size(150, 13);
            this.labelControl76.TabIndex = 85;
            this.labelControl76.Text = "(if different from main address)";
            // 
            // applicant_MailingAddressCountry
            // 
            this.applicant_MailingAddressCountry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.applicant_MailingAddressCountry.Location = new System.Drawing.Point(284, 777);
            this.applicant_MailingAddressCountry.Name = "applicant_MailingAddressCountry";
            this.applicant_MailingAddressCountry.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.applicant_MailingAddressCountry.Properties.Items.AddRange(new object[] {
            "American Indian or Alaska Native",
            "Asian",
            "Black or African American",
            "etc..."});
            this.applicant_MailingAddressCountry.Size = new System.Drawing.Size(175, 20);
            this.applicant_MailingAddressCountry.TabIndex = 40;
            // 
            // labelControl69
            // 
            this.labelControl69.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl69.Location = new System.Drawing.Point(284, 759);
            this.labelControl69.Name = "labelControl69";
            this.labelControl69.Size = new System.Drawing.Size(43, 13);
            this.labelControl69.TabIndex = 83;
            this.labelControl69.Text = "Country:";
            // 
            // applicant_MailingAddressZip
            // 
            this.applicant_MailingAddressZip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.applicant_MailingAddressZip.Location = new System.Drawing.Point(224, 777);
            this.applicant_MailingAddressZip.Name = "applicant_MailingAddressZip";
            this.applicant_MailingAddressZip.Size = new System.Drawing.Size(54, 20);
            this.applicant_MailingAddressZip.TabIndex = 39;
            // 
            // labelControl70
            // 
            this.labelControl70.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl70.Location = new System.Drawing.Point(224, 759);
            this.labelControl70.Name = "labelControl70";
            this.labelControl70.Size = new System.Drawing.Size(20, 13);
            this.labelControl70.TabIndex = 81;
            this.labelControl70.Text = "ZIP:";
            // 
            // applicant_MailingAddressState
            // 
            this.applicant_MailingAddressState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.applicant_MailingAddressState.Location = new System.Drawing.Point(137, 777);
            this.applicant_MailingAddressState.Name = "applicant_MailingAddressState";
            this.applicant_MailingAddressState.Size = new System.Drawing.Size(81, 20);
            this.applicant_MailingAddressState.TabIndex = 38;
            // 
            // labelControl71
            // 
            this.labelControl71.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl71.Location = new System.Drawing.Point(137, 759);
            this.labelControl71.Name = "labelControl71";
            this.labelControl71.Size = new System.Drawing.Size(30, 13);
            this.labelControl71.TabIndex = 79;
            this.labelControl71.Text = "State:";
            // 
            // applicant_MailingAddressCity
            // 
            this.applicant_MailingAddressCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.applicant_MailingAddressCity.Location = new System.Drawing.Point(12, 777);
            this.applicant_MailingAddressCity.Name = "applicant_MailingAddressCity";
            this.applicant_MailingAddressCity.Size = new System.Drawing.Size(119, 20);
            this.applicant_MailingAddressCity.TabIndex = 37;
            // 
            // labelControl72
            // 
            this.labelControl72.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl72.Location = new System.Drawing.Point(12, 759);
            this.labelControl72.Name = "labelControl72";
            this.labelControl72.Size = new System.Drawing.Size(23, 13);
            this.labelControl72.TabIndex = 77;
            this.labelControl72.Text = "City:";
            // 
            // applicant_MailingAddressLine2
            // 
            this.applicant_MailingAddressLine2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.applicant_MailingAddressLine2.Location = new System.Drawing.Point(12, 730);
            this.applicant_MailingAddressLine2.Name = "applicant_MailingAddressLine2";
            this.applicant_MailingAddressLine2.Size = new System.Drawing.Size(446, 20);
            this.applicant_MailingAddressLine2.TabIndex = 36;
            // 
            // labelControl73
            // 
            this.labelControl73.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl73.Location = new System.Drawing.Point(12, 714);
            this.labelControl73.Name = "labelControl73";
            this.labelControl73.Size = new System.Drawing.Size(74, 13);
            this.labelControl73.TabIndex = 75;
            this.labelControl73.Text = "Address Line 2:";
            // 
            // labelControl74
            // 
            this.labelControl74.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl74.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl74.Appearance.Options.UseFont = true;
            this.labelControl74.Location = new System.Drawing.Point(12, 648);
            this.labelControl74.Name = "labelControl74";
            this.labelControl74.Size = new System.Drawing.Size(158, 14);
            this.labelControl74.TabIndex = 74;
            this.labelControl74.Text = "Applicant Mailing Address";
            // 
            // applicant_MailingAddressStreet
            // 
            this.applicant_MailingAddressStreet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.applicant_MailingAddressStreet.Location = new System.Drawing.Point(12, 688);
            this.applicant_MailingAddressStreet.Name = "applicant_MailingAddressStreet";
            this.applicant_MailingAddressStreet.Size = new System.Drawing.Size(447, 20);
            this.applicant_MailingAddressStreet.TabIndex = 35;
            // 
            // labelControl75
            // 
            this.labelControl75.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl75.Location = new System.Drawing.Point(12, 672);
            this.labelControl75.Name = "labelControl75";
            this.labelControl75.Size = new System.Drawing.Size(76, 13);
            this.labelControl75.TabIndex = 72;
            this.labelControl75.Text = "Street Address:";
            // 
            // applicant_Email
            // 
            this.applicant_Email.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.applicant_Email.Location = new System.Drawing.Point(137, 611);
            this.applicant_Email.Name = "applicant_Email";
            this.applicant_Email.Size = new System.Drawing.Size(141, 20);
            this.applicant_Email.TabIndex = 34;
            // 
            // labelControl67
            // 
            this.labelControl67.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl67.Location = new System.Drawing.Point(137, 593);
            this.labelControl67.Name = "labelControl67";
            this.labelControl67.Size = new System.Drawing.Size(69, 13);
            this.labelControl67.TabIndex = 70;
            this.labelControl67.Text = "Email address:";
            // 
            // applicant_Phone
            // 
            this.applicant_Phone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.applicant_Phone.Location = new System.Drawing.Point(12, 611);
            this.applicant_Phone.Name = "applicant_Phone";
            this.applicant_Phone.Size = new System.Drawing.Size(119, 20);
            this.applicant_Phone.TabIndex = 33;
            // 
            // labelControl66
            // 
            this.labelControl66.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl66.Location = new System.Drawing.Point(12, 593);
            this.labelControl66.Name = "labelControl66";
            this.labelControl66.Size = new System.Drawing.Size(34, 13);
            this.labelControl66.TabIndex = 68;
            this.labelControl66.Text = "Phone:";
            // 
            // applicant_AddressCountry
            // 
            this.applicant_AddressCountry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.applicant_AddressCountry.Location = new System.Drawing.Point(284, 560);
            this.applicant_AddressCountry.Name = "applicant_AddressCountry";
            this.applicant_AddressCountry.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.applicant_AddressCountry.Properties.Items.AddRange(new object[] {
            "American Indian or Alaska Native",
            "Asian",
            "Black or African American",
            "etc..."});
            this.applicant_AddressCountry.Size = new System.Drawing.Size(175, 20);
            this.applicant_AddressCountry.TabIndex = 32;
            // 
            // labelControl59
            // 
            this.labelControl59.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl59.Location = new System.Drawing.Point(284, 542);
            this.labelControl59.Name = "labelControl59";
            this.labelControl59.Size = new System.Drawing.Size(43, 13);
            this.labelControl59.TabIndex = 66;
            this.labelControl59.Text = "Country:";
            // 
            // applicant_AddressZip
            // 
            this.applicant_AddressZip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.applicant_AddressZip.Location = new System.Drawing.Point(224, 560);
            this.applicant_AddressZip.Name = "applicant_AddressZip";
            this.applicant_AddressZip.Size = new System.Drawing.Size(54, 20);
            this.applicant_AddressZip.TabIndex = 31;
            // 
            // labelControl60
            // 
            this.labelControl60.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl60.Location = new System.Drawing.Point(224, 542);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(20, 13);
            this.labelControl60.TabIndex = 64;
            this.labelControl60.Text = "ZIP:";
            // 
            // applicant_AddressState
            // 
            this.applicant_AddressState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.applicant_AddressState.Location = new System.Drawing.Point(137, 560);
            this.applicant_AddressState.Name = "applicant_AddressState";
            this.applicant_AddressState.Size = new System.Drawing.Size(81, 20);
            this.applicant_AddressState.TabIndex = 30;
            // 
            // labelControl61
            // 
            this.labelControl61.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl61.Location = new System.Drawing.Point(137, 542);
            this.labelControl61.Name = "labelControl61";
            this.labelControl61.Size = new System.Drawing.Size(30, 13);
            this.labelControl61.TabIndex = 62;
            this.labelControl61.Text = "State:";
            // 
            // applicant_AddressCity
            // 
            this.applicant_AddressCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.applicant_AddressCity.Location = new System.Drawing.Point(12, 560);
            this.applicant_AddressCity.Name = "applicant_AddressCity";
            this.applicant_AddressCity.Size = new System.Drawing.Size(119, 20);
            this.applicant_AddressCity.TabIndex = 29;
            // 
            // labelControl62
            // 
            this.labelControl62.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl62.Location = new System.Drawing.Point(12, 542);
            this.labelControl62.Name = "labelControl62";
            this.labelControl62.Size = new System.Drawing.Size(23, 13);
            this.labelControl62.TabIndex = 60;
            this.labelControl62.Text = "City:";
            // 
            // applicant_AddressLine2
            // 
            this.applicant_AddressLine2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.applicant_AddressLine2.Location = new System.Drawing.Point(12, 513);
            this.applicant_AddressLine2.Name = "applicant_AddressLine2";
            this.applicant_AddressLine2.Size = new System.Drawing.Size(446, 20);
            this.applicant_AddressLine2.TabIndex = 28;
            // 
            // labelControl63
            // 
            this.labelControl63.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl63.Location = new System.Drawing.Point(12, 497);
            this.labelControl63.Name = "labelControl63";
            this.labelControl63.Size = new System.Drawing.Size(74, 13);
            this.labelControl63.TabIndex = 58;
            this.labelControl63.Text = "Address Line 2:";
            // 
            // labelControl64
            // 
            this.labelControl64.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl64.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl64.Appearance.Options.UseFont = true;
            this.labelControl64.Location = new System.Drawing.Point(12, 431);
            this.labelControl64.Name = "labelControl64";
            this.labelControl64.Size = new System.Drawing.Size(111, 14);
            this.labelControl64.TabIndex = 57;
            this.labelControl64.Text = "Applicant Address";
            // 
            // applicant_AddressStreet
            // 
            this.applicant_AddressStreet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.applicant_AddressStreet.Location = new System.Drawing.Point(12, 471);
            this.applicant_AddressStreet.Name = "applicant_AddressStreet";
            this.applicant_AddressStreet.Size = new System.Drawing.Size(447, 20);
            this.applicant_AddressStreet.TabIndex = 27;
            // 
            // labelControl65
            // 
            this.labelControl65.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl65.Location = new System.Drawing.Point(12, 455);
            this.labelControl65.Name = "labelControl65";
            this.labelControl65.Size = new System.Drawing.Size(76, 13);
            this.labelControl65.TabIndex = 55;
            this.labelControl65.Text = "Street Address:";
            // 
            // applicant_MilitaryToYear
            // 
            this.applicant_MilitaryToYear.Location = new System.Drawing.Point(399, 300);
            this.applicant_MilitaryToYear.Name = "applicant_MilitaryToYear";
            this.applicant_MilitaryToYear.Properties.Mask.EditMask = "\\d{0,6}";
            this.applicant_MilitaryToYear.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.applicant_MilitaryToYear.Size = new System.Drawing.Size(59, 20);
            this.applicant_MilitaryToYear.TabIndex = 23;
            // 
            // applicant_MilitaryFromYear
            // 
            this.applicant_MilitaryFromYear.Location = new System.Drawing.Point(306, 300);
            this.applicant_MilitaryFromYear.Name = "applicant_MilitaryFromYear";
            this.applicant_MilitaryFromYear.Properties.Mask.EditMask = "\\d{0,6}";
            this.applicant_MilitaryFromYear.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.applicant_MilitaryFromYear.Size = new System.Drawing.Size(57, 20);
            this.applicant_MilitaryFromYear.TabIndex = 22;
            // 
            // labelControl56
            // 
            this.labelControl56.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl56.Appearance.Options.UseFont = true;
            this.labelControl56.Location = new System.Drawing.Point(276, 282);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(173, 13);
            this.labelControl56.TabIndex = 53;
            this.labelControl56.Text = "Military service start and end years:";
            // 
            // applicant_MilitaryBranch
            // 
            this.applicant_MilitaryBranch.Location = new System.Drawing.Point(129, 300);
            this.applicant_MilitaryBranch.Name = "applicant_MilitaryBranch";
            this.applicant_MilitaryBranch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.applicant_MilitaryBranch.Properties.Items.AddRange(new object[] {
            "Army",
            "Navy",
            "Marine Corps",
            "Air Force",
            "Coast Guard"});
            this.applicant_MilitaryBranch.Size = new System.Drawing.Size(134, 20);
            this.applicant_MilitaryBranch.TabIndex = 21;
            // 
            // labelControl58
            // 
            this.labelControl58.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl58.Appearance.Options.UseFont = true;
            this.labelControl58.Location = new System.Drawing.Point(380, 302);
            this.labelControl58.Name = "labelControl58";
            this.labelControl58.Size = new System.Drawing.Size(16, 13);
            this.labelControl58.TabIndex = 51;
            this.labelControl58.Text = "To:";
            // 
            // labelControl57
            // 
            this.labelControl57.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl57.Appearance.Options.UseFont = true;
            this.labelControl57.Location = new System.Drawing.Point(277, 302);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(28, 13);
            this.labelControl57.TabIndex = 51;
            this.labelControl57.Text = "From:";
            // 
            // labelControl55
            // 
            this.labelControl55.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl55.Appearance.Options.UseFont = true;
            this.labelControl55.Location = new System.Drawing.Point(129, 283);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(37, 13);
            this.labelControl55.TabIndex = 51;
            this.labelControl55.Text = "Branch:";
            // 
            // applicant_IsMilitary
            // 
            this.applicant_IsMilitary.Location = new System.Drawing.Point(11, 280);
            this.applicant_IsMilitary.Name = "applicant_IsMilitary";
            this.applicant_IsMilitary.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.applicant_IsMilitary.Properties.Appearance.Options.UseFont = true;
            this.applicant_IsMilitary.Properties.Caption = "Military Service";
            this.applicant_IsMilitary.Size = new System.Drawing.Size(112, 19);
            this.applicant_IsMilitary.TabIndex = 20;
            this.applicant_IsMilitary.CheckedChanged += new System.EventHandler(this.Applicant_IsMilitary_CheckedChanged);
            // 
            // applicant_SpouseSSN
            // 
            this.applicant_SpouseSSN.Enabled = false;
            this.applicant_SpouseSSN.Location = new System.Drawing.Point(367, 242);
            this.applicant_SpouseSSN.Name = "applicant_SpouseSSN";
            this.applicant_SpouseSSN.Size = new System.Drawing.Size(91, 20);
            this.applicant_SpouseSSN.TabIndex = 19;
            // 
            // labelControl68
            // 
            this.labelControl68.Location = new System.Drawing.Point(368, 226);
            this.labelControl68.Name = "labelControl68";
            this.labelControl68.Size = new System.Drawing.Size(61, 13);
            this.labelControl68.TabIndex = 48;
            this.labelControl68.Text = "Spouse SSN:";
            // 
            // applicant_SpouseLastName
            // 
            this.applicant_SpouseLastName.Enabled = false;
            this.applicant_SpouseLastName.Location = new System.Drawing.Point(269, 242);
            this.applicant_SpouseLastName.Name = "applicant_SpouseLastName";
            this.applicant_SpouseLastName.Size = new System.Drawing.Size(94, 20);
            this.applicant_SpouseLastName.TabIndex = 18;
            // 
            // labelControl52
            // 
            this.labelControl52.Location = new System.Drawing.Point(270, 226);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(91, 13);
            this.labelControl52.TabIndex = 48;
            this.labelControl52.Text = "Spouse Last name:";
            // 
            // applicant_SpouseMiddleName
            // 
            this.applicant_SpouseMiddleName.Enabled = false;
            this.applicant_SpouseMiddleName.Location = new System.Drawing.Point(152, 242);
            this.applicant_SpouseMiddleName.Name = "applicant_SpouseMiddleName";
            this.applicant_SpouseMiddleName.Size = new System.Drawing.Size(111, 20);
            this.applicant_SpouseMiddleName.TabIndex = 17;
            // 
            // labelControl53
            // 
            this.labelControl53.Location = new System.Drawing.Point(152, 226);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(101, 13);
            this.labelControl53.TabIndex = 46;
            this.labelControl53.Text = "Spouse Middle name:";
            // 
            // applicant_SpouseFirstName
            // 
            this.applicant_SpouseFirstName.Enabled = false;
            this.applicant_SpouseFirstName.Location = new System.Drawing.Point(11, 242);
            this.applicant_SpouseFirstName.Name = "applicant_SpouseFirstName";
            this.applicant_SpouseFirstName.Size = new System.Drawing.Size(135, 20);
            this.applicant_SpouseFirstName.TabIndex = 16;
            // 
            // labelControl54
            // 
            this.labelControl54.Location = new System.Drawing.Point(11, 226);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(92, 13);
            this.labelControl54.TabIndex = 44;
            this.labelControl54.Text = "Spouse First name:";
            // 
            // applicant_DateOfMarrgiage
            // 
            this.applicant_DateOfMarrgiage.EditValue = new System.DateTime(2019, 7, 1, 0, 0, 0, 0);
            this.applicant_DateOfMarrgiage.Enabled = false;
            this.applicant_DateOfMarrgiage.Location = new System.Drawing.Point(269, 197);
            this.applicant_DateOfMarrgiage.Name = "applicant_DateOfMarrgiage";
            this.applicant_DateOfMarrgiage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.applicant_DateOfMarrgiage.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.applicant_DateOfMarrgiage.Size = new System.Drawing.Size(190, 20);
            this.applicant_DateOfMarrgiage.TabIndex = 15;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(266, 181);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(195, 13);
            this.label84.TabIndex = 42;
            this.label84.Text = "Date of Marriage, Divorce or Widowed:";
            // 
            // applicant_SpouceDateOfBirth
            // 
            this.applicant_SpouceDateOfBirth.EditValue = new System.DateTime(2019, 7, 1, 0, 0, 0, 0);
            this.applicant_SpouceDateOfBirth.Enabled = false;
            this.applicant_SpouceDateOfBirth.Location = new System.Drawing.Point(152, 198);
            this.applicant_SpouceDateOfBirth.Name = "applicant_SpouceDateOfBirth";
            this.applicant_SpouceDateOfBirth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.applicant_SpouceDateOfBirth.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.applicant_SpouceDateOfBirth.Size = new System.Drawing.Size(111, 20);
            this.applicant_SpouceDateOfBirth.TabIndex = 14;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(149, 181);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(109, 13);
            this.label83.TabIndex = 40;
            this.label83.Text = "Spouse date of birth:";
            // 
            // applicant_MaritalStatus
            // 
            this.applicant_MaritalStatus.EditValue = "Never Married";
            this.applicant_MaritalStatus.Location = new System.Drawing.Point(12, 198);
            this.applicant_MaritalStatus.Name = "applicant_MaritalStatus";
            this.applicant_MaritalStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.applicant_MaritalStatus.Properties.Items.AddRange(new object[] {
            "Married",
            "Divorced",
            "Widowed",
            "Never Married"});
            this.applicant_MaritalStatus.Size = new System.Drawing.Size(134, 20);
            this.applicant_MaritalStatus.TabIndex = 13;
            this.applicant_MaritalStatus.SelectedIndexChanged += new System.EventHandler(this.Applicant_MaritalStatus_SelectedIndexChanged);
            // 
            // labelControl51
            // 
            this.labelControl51.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl51.Appearance.Options.UseFont = true;
            this.labelControl51.Location = new System.Drawing.Point(12, 181);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(83, 13);
            this.labelControl51.TabIndex = 38;
            this.labelControl51.Text = "Marital Status:";
            // 
            // applicant_DateOfBirth
            // 
            this.applicant_DateOfBirth.EditValue = new System.DateTime(2019, 7, 1, 0, 0, 0, 0);
            this.applicant_DateOfBirth.Location = new System.Drawing.Point(151, 70);
            this.applicant_DateOfBirth.Name = "applicant_DateOfBirth";
            this.applicant_DateOfBirth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.applicant_DateOfBirth.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.applicant_DateOfBirth.Size = new System.Drawing.Size(131, 20);
            this.applicant_DateOfBirth.TabIndex = 5;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(152, 54);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(72, 13);
            this.label82.TabIndex = 36;
            this.label82.Text = "Date of birth:";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(10, 52);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(46, 13);
            this.label81.TabIndex = 35;
            this.label81.Text = "Gender:";
            // 
            // applicant_Gender
            // 
            this.applicant_Gender.EditValue = ((short)(1));
            this.applicant_Gender.Location = new System.Drawing.Point(12, 68);
            this.applicant_Gender.Name = "applicant_Gender";
            this.applicant_Gender.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(1)), "Male"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(2)), "Female")});
            this.applicant_Gender.Size = new System.Drawing.Size(133, 57);
            this.applicant_Gender.TabIndex = 4;
            // 
            // applicant_SSN
            // 
            this.applicant_SSN.Location = new System.Drawing.Point(151, 105);
            this.applicant_SSN.Name = "applicant_SSN";
            this.applicant_SSN.Size = new System.Drawing.Size(129, 20);
            this.applicant_SSN.TabIndex = 6;
            // 
            // applicant_LastName
            // 
            this.applicant_LastName.Location = new System.Drawing.Point(316, 25);
            this.applicant_LastName.Name = "applicant_LastName";
            this.applicant_LastName.Size = new System.Drawing.Size(129, 20);
            this.applicant_LastName.TabIndex = 3;
            // 
            // labelControl48
            // 
            this.labelControl48.Location = new System.Drawing.Point(317, 7);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(53, 13);
            this.labelControl48.TabIndex = 32;
            this.labelControl48.Text = "Last name:";
            // 
            // applicant_MiddleName
            // 
            this.applicant_MiddleName.Location = new System.Drawing.Point(151, 25);
            this.applicant_MiddleName.Name = "applicant_MiddleName";
            this.applicant_MiddleName.Size = new System.Drawing.Size(159, 20);
            this.applicant_MiddleName.TabIndex = 2;
            // 
            // labelControl49
            // 
            this.labelControl49.Location = new System.Drawing.Point(151, 7);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(63, 13);
            this.labelControl49.TabIndex = 30;
            this.labelControl49.Text = "Middle name:";
            // 
            // applicant_FirstName
            // 
            this.applicant_FirstName.Location = new System.Drawing.Point(12, 25);
            this.applicant_FirstName.Name = "applicant_FirstName";
            this.applicant_FirstName.Size = new System.Drawing.Size(133, 20);
            this.applicant_FirstName.TabIndex = 1;
            // 
            // labelControl50
            // 
            this.labelControl50.Location = new System.Drawing.Point(12, 7);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(54, 13);
            this.labelControl50.TabIndex = 28;
            this.labelControl50.Text = "First name:";
            // 
            // applicant_LivingAt
            // 
            this.applicant_LivingAt.Location = new System.Drawing.Point(11, 150);
            this.applicant_LivingAt.Name = "applicant_LivingAt";
            this.applicant_LivingAt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.applicant_LivingAt.Properties.Items.AddRange(new object[] {
            "Own home",
            "With family/others",
            "At long term care facility"});
            this.applicant_LivingAt.Size = new System.Drawing.Size(134, 20);
            this.applicant_LivingAt.TabIndex = 10;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(11, 133);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(44, 13);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Living at:";
            // 
            // applicant_AlreadyOnAHCCCSProgram
            // 
            this.applicant_AlreadyOnAHCCCSProgram.Location = new System.Drawing.Point(288, 88);
            this.applicant_AlreadyOnAHCCCSProgram.Name = "applicant_AlreadyOnAHCCCSProgram";
            this.applicant_AlreadyOnAHCCCSProgram.Properties.Caption = "Already on AHCCCS programs?";
            this.applicant_AlreadyOnAHCCCSProgram.Size = new System.Drawing.Size(171, 19);
            this.applicant_AlreadyOnAHCCCSProgram.TabIndex = 8;
            // 
            // applicant_IsRegisteredVoter
            // 
            this.applicant_IsRegisteredVoter.Location = new System.Drawing.Point(289, 109);
            this.applicant_IsRegisteredVoter.Name = "applicant_IsRegisteredVoter";
            this.applicant_IsRegisteredVoter.Properties.Caption = "Registered voter";
            this.applicant_IsRegisteredVoter.Size = new System.Drawing.Size(112, 19);
            this.applicant_IsRegisteredVoter.TabIndex = 9;
            // 
            // applicant_Ethnicity
            // 
            this.applicant_Ethnicity.Location = new System.Drawing.Point(289, 150);
            this.applicant_Ethnicity.Name = "applicant_Ethnicity";
            this.applicant_Ethnicity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.applicant_Ethnicity.Properties.Items.AddRange(new object[] {
            "American Indian or Alaska Native",
            "Asian",
            "Black or African American",
            "Hispanic or Latino",
            "Native Hawaiian or Other Pacific Islander",
            "White",
            "Other",
            "Declined to answer"});
            this.applicant_Ethnicity.Size = new System.Drawing.Size(169, 20);
            this.applicant_Ethnicity.TabIndex = 12;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(289, 133);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(45, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Ethnicity:";
            // 
            // applicant_PlaceOfBirth
            // 
            this.applicant_PlaceOfBirth.Location = new System.Drawing.Point(151, 150);
            this.applicant_PlaceOfBirth.Name = "applicant_PlaceOfBirth";
            this.applicant_PlaceOfBirth.Size = new System.Drawing.Size(131, 20);
            this.applicant_PlaceOfBirth.TabIndex = 11;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(151, 133);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(67, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Place of birth:";
            // 
            // applicant_IsUSCitizen
            // 
            this.applicant_IsUSCitizen.Location = new System.Drawing.Point(288, 67);
            this.applicant_IsUSCitizen.Name = "applicant_IsUSCitizen";
            this.applicant_IsUSCitizen.Properties.Caption = "US Citizen";
            this.applicant_IsUSCitizen.Size = new System.Drawing.Size(75, 19);
            this.applicant_IsUSCitizen.TabIndex = 7;
            // 
            // accordionContentContainer16
            // 
            this.accordionContentContainer16.Controls.Add(this.poc_IsPOC);
            this.accordionContentContainer16.Controls.Add(this.groupPOC);
            this.accordionContentContainer16.Name = "accordionContentContainer16";
            this.accordionContentContainer16.Size = new System.Drawing.Size(0, 294);
            this.accordionContentContainer16.TabIndex = 17;
            // 
            // poc_IsPOC
            // 
            this.poc_IsPOC.Location = new System.Drawing.Point(12, 2);
            this.poc_IsPOC.Name = "poc_IsPOC";
            this.poc_IsPOC.Properties.Caption = "Is there a point of contact?";
            this.poc_IsPOC.Size = new System.Drawing.Size(152, 19);
            this.poc_IsPOC.TabIndex = 1;
            this.poc_IsPOC.CheckedChanged += new System.EventHandler(this.Poc_IsPOC_CheckedChanged);
            // 
            // groupPOC
            // 
            this.groupPOC.Controls.Add(this.poc_AddressCountry);
            this.groupPOC.Controls.Add(this.labelControl11);
            this.groupPOC.Controls.Add(this.poc_AddressZip);
            this.groupPOC.Controls.Add(this.labelControl12);
            this.groupPOC.Controls.Add(this.poc_AddressState);
            this.groupPOC.Controls.Add(this.labelControl13);
            this.groupPOC.Controls.Add(this.poc_AddressCity);
            this.groupPOC.Controls.Add(this.labelControl28);
            this.groupPOC.Controls.Add(this.poc_Email);
            this.groupPOC.Controls.Add(this.labelControl15);
            this.groupPOC.Controls.Add(this.poc_Phone);
            this.groupPOC.Controls.Add(this.labelControl14);
            this.groupPOC.Controls.Add(this.poc_AddressLine2);
            this.groupPOC.Controls.Add(this.labelControl10);
            this.groupPOC.Controls.Add(this.labelControl9);
            this.groupPOC.Controls.Add(this.poc_AddressStreet);
            this.groupPOC.Controls.Add(this.labelControl8);
            this.groupPOC.Controls.Add(this.poc_LastName);
            this.groupPOC.Controls.Add(this.labelControl7);
            this.groupPOC.Controls.Add(this.poc_MiddleName);
            this.groupPOC.Controls.Add(this.labelControl6);
            this.groupPOC.Controls.Add(this.poc_FirstName);
            this.groupPOC.Controls.Add(this.labelControl5);
            this.groupPOC.Controls.Add(this.poc_Relationship);
            this.groupPOC.Controls.Add(this.labelControl4);
            this.groupPOC.Enabled = false;
            this.groupPOC.Location = new System.Drawing.Point(7, 3);
            this.groupPOC.Name = "groupPOC";
            this.groupPOC.Size = new System.Drawing.Size(458, 281);
            this.groupPOC.TabIndex = 20;
            // 
            // poc_AddressCountry
            // 
            this.poc_AddressCountry.Location = new System.Drawing.Point(283, 245);
            this.poc_AddressCountry.Name = "poc_AddressCountry";
            this.poc_AddressCountry.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.poc_AddressCountry.Properties.Items.AddRange(new object[] {
            "American Indian or Alaska Native",
            "Asian",
            "Black or African American",
            "etc..."});
            this.poc_AddressCountry.Size = new System.Drawing.Size(161, 20);
            this.poc_AddressCountry.TabIndex = 13;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(283, 227);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(43, 13);
            this.labelControl11.TabIndex = 49;
            this.labelControl11.Text = "Country:";
            // 
            // poc_AddressZip
            // 
            this.poc_AddressZip.Location = new System.Drawing.Point(223, 245);
            this.poc_AddressZip.Name = "poc_AddressZip";
            this.poc_AddressZip.Size = new System.Drawing.Size(54, 20);
            this.poc_AddressZip.TabIndex = 12;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(223, 227);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(20, 13);
            this.labelControl12.TabIndex = 47;
            this.labelControl12.Text = "ZIP:";
            // 
            // poc_AddressState
            // 
            this.poc_AddressState.Location = new System.Drawing.Point(136, 245);
            this.poc_AddressState.Name = "poc_AddressState";
            this.poc_AddressState.Size = new System.Drawing.Size(81, 20);
            this.poc_AddressState.TabIndex = 11;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(136, 227);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(30, 13);
            this.labelControl13.TabIndex = 45;
            this.labelControl13.Text = "State:";
            // 
            // poc_AddressCity
            // 
            this.poc_AddressCity.Location = new System.Drawing.Point(11, 245);
            this.poc_AddressCity.Name = "poc_AddressCity";
            this.poc_AddressCity.Size = new System.Drawing.Size(119, 20);
            this.poc_AddressCity.TabIndex = 10;
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(11, 227);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(23, 13);
            this.labelControl28.TabIndex = 43;
            this.labelControl28.Text = "City:";
            // 
            // poc_Email
            // 
            this.poc_Email.Location = new System.Drawing.Point(315, 41);
            this.poc_Email.Name = "poc_Email";
            this.poc_Email.Size = new System.Drawing.Size(129, 20);
            this.poc_Email.TabIndex = 4;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(315, 23);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(28, 13);
            this.labelControl15.TabIndex = 41;
            this.labelControl15.Text = "Email:";
            // 
            // poc_Phone
            // 
            this.poc_Phone.Location = new System.Drawing.Point(163, 41);
            this.poc_Phone.Name = "poc_Phone";
            this.poc_Phone.Size = new System.Drawing.Size(146, 20);
            this.poc_Phone.TabIndex = 3;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(163, 23);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(34, 13);
            this.labelControl14.TabIndex = 39;
            this.labelControl14.Text = "Phone:";
            // 
            // poc_AddressLine2
            // 
            this.poc_AddressLine2.Location = new System.Drawing.Point(11, 198);
            this.poc_AddressLine2.Name = "poc_AddressLine2";
            this.poc_AddressLine2.Size = new System.Drawing.Size(433, 20);
            this.poc_AddressLine2.TabIndex = 9;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(11, 182);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(74, 13);
            this.labelControl10.TabIndex = 31;
            this.labelControl10.Text = "Address Line 2:";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(11, 116);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(78, 14);
            this.labelControl9.TabIndex = 30;
            this.labelControl9.Text = "POC Address";
            // 
            // poc_AddressStreet
            // 
            this.poc_AddressStreet.Location = new System.Drawing.Point(11, 156);
            this.poc_AddressStreet.Name = "poc_AddressStreet";
            this.poc_AddressStreet.Size = new System.Drawing.Size(433, 20);
            this.poc_AddressStreet.TabIndex = 8;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(11, 140);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(76, 13);
            this.labelControl8.TabIndex = 28;
            this.labelControl8.Text = "Street Address:";
            // 
            // poc_LastName
            // 
            this.poc_LastName.Location = new System.Drawing.Point(315, 87);
            this.poc_LastName.Name = "poc_LastName";
            this.poc_LastName.Size = new System.Drawing.Size(129, 20);
            this.poc_LastName.TabIndex = 7;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(316, 69);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(53, 13);
            this.labelControl7.TabIndex = 26;
            this.labelControl7.Text = "Last name:";
            // 
            // poc_MiddleName
            // 
            this.poc_MiddleName.Location = new System.Drawing.Point(163, 87);
            this.poc_MiddleName.Name = "poc_MiddleName";
            this.poc_MiddleName.Size = new System.Drawing.Size(146, 20);
            this.poc_MiddleName.TabIndex = 6;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(163, 69);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(63, 13);
            this.labelControl6.TabIndex = 24;
            this.labelControl6.Text = "Middle name:";
            // 
            // poc_FirstName
            // 
            this.poc_FirstName.Location = new System.Drawing.Point(11, 87);
            this.poc_FirstName.Name = "poc_FirstName";
            this.poc_FirstName.Size = new System.Drawing.Size(146, 20);
            this.poc_FirstName.TabIndex = 5;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(11, 69);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(54, 13);
            this.labelControl5.TabIndex = 22;
            this.labelControl5.Text = "First name:";
            // 
            // poc_Relationship
            // 
            this.poc_Relationship.Location = new System.Drawing.Point(11, 41);
            this.poc_Relationship.Name = "poc_Relationship";
            this.poc_Relationship.Size = new System.Drawing.Size(146, 20);
            this.poc_Relationship.TabIndex = 2;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(11, 23);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(62, 13);
            this.labelControl4.TabIndex = 20;
            this.labelControl4.Text = "Relationship:";
            // 
            // accordionContentContainer17
            // 
            this.accordionContentContainer17.Controls.Add(this.ltc_IsLtcAvaliable);
            this.accordionContentContainer17.Controls.Add(this.groupLTC);
            this.accordionContentContainer17.Name = "accordionContentContainer17";
            this.accordionContentContainer17.Size = new System.Drawing.Size(0, 342);
            this.accordionContentContainer17.TabIndex = 18;
            // 
            // ltc_IsLtcAvaliable
            // 
            this.ltc_IsLtcAvaliable.Location = new System.Drawing.Point(9, 4);
            this.ltc_IsLtcAvaliable.Name = "ltc_IsLtcAvaliable";
            this.ltc_IsLtcAvaliable.Properties.Caption = "Is there a LTC community?";
            this.ltc_IsLtcAvaliable.Size = new System.Drawing.Size(147, 19);
            this.ltc_IsLtcAvaliable.TabIndex = 1;
            this.ltc_IsLtcAvaliable.CheckedChanged += new System.EventHandler(this.Ltc_IsLtcAvaliable_CheckedChanged);
            // 
            // groupLTC
            // 
            this.groupLTC.Controls.Add(this.labelControl29);
            this.groupLTC.Controls.Add(this.ltc_DateEntered);
            this.groupLTC.Controls.Add(this.ltc_FacilityType);
            this.groupLTC.Controls.Add(this.labelControl25);
            this.groupLTC.Controls.Add(this.ltc_AddressCountry);
            this.groupLTC.Controls.Add(this.labelControl16);
            this.groupLTC.Controls.Add(this.ltc_FacilityPhone);
            this.groupLTC.Controls.Add(this.labelControl17);
            this.groupLTC.Controls.Add(this.ltc_AddressZip);
            this.groupLTC.Controls.Add(this.labelControl18);
            this.groupLTC.Controls.Add(this.ltc_AddressState);
            this.groupLTC.Controls.Add(this.labelControl19);
            this.groupLTC.Controls.Add(this.ltc_AddressCity);
            this.groupLTC.Controls.Add(this.labelControl20);
            this.groupLTC.Controls.Add(this.ltc_AddressLine2);
            this.groupLTC.Controls.Add(this.labelControl21);
            this.groupLTC.Controls.Add(this.labelControl22);
            this.groupLTC.Controls.Add(this.ltc_AddressStreet);
            this.groupLTC.Controls.Add(this.labelControl23);
            this.groupLTC.Controls.Add(this.ltc_FacilityPOCLastName);
            this.groupLTC.Controls.Add(this.labelControl24);
            this.groupLTC.Controls.Add(this.ltc_CaficlityPocFirstName);
            this.groupLTC.Controls.Add(this.labelControl26);
            this.groupLTC.Controls.Add(this.ltc_FacilityName);
            this.groupLTC.Controls.Add(this.labelControl27);
            this.groupLTC.Enabled = false;
            this.groupLTC.Location = new System.Drawing.Point(4, 3);
            this.groupLTC.Name = "groupLTC";
            this.groupLTC.Size = new System.Drawing.Size(458, 328);
            this.groupLTC.TabIndex = 21;
            // 
            // labelControl29
            // 
            this.labelControl29.Location = new System.Drawing.Point(195, 279);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(68, 13);
            this.labelControl29.TabIndex = 46;
            this.labelControl29.Text = "Date entered:";
            // 
            // ltc_DateEntered
            // 
            this.ltc_DateEntered.EditValue = new System.DateTime(2019, 7, 1, 0, 0, 0, 0);
            this.ltc_DateEntered.Location = new System.Drawing.Point(195, 297);
            this.ltc_DateEntered.Name = "ltc_DateEntered";
            this.ltc_DateEntered.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ltc_DateEntered.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ltc_DateEntered.Size = new System.Drawing.Size(151, 20);
            this.ltc_DateEntered.TabIndex = 13;
            // 
            // ltc_FacilityType
            // 
            this.ltc_FacilityType.Location = new System.Drawing.Point(11, 297);
            this.ltc_FacilityType.Name = "ltc_FacilityType";
            this.ltc_FacilityType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ltc_FacilityType.Properties.Items.AddRange(new object[] {
            "Group",
            "Independent",
            "Assisted",
            "Memory",
            "Skilled",
            "Other"});
            this.ltc_FacilityType.Size = new System.Drawing.Size(161, 20);
            this.ltc_FacilityType.TabIndex = 12;
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(11, 279);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(64, 13);
            this.labelControl25.TabIndex = 43;
            this.labelControl25.Text = "Facility Type:";
            // 
            // ltc_AddressCountry
            // 
            this.ltc_AddressCountry.Location = new System.Drawing.Point(283, 246);
            this.ltc_AddressCountry.Name = "ltc_AddressCountry";
            this.ltc_AddressCountry.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ltc_AddressCountry.Properties.Items.AddRange(new object[] {
            "American Indian or Alaska Native",
            "Asian",
            "Black or African American",
            "etc..."});
            this.ltc_AddressCountry.Size = new System.Drawing.Size(161, 20);
            this.ltc_AddressCountry.TabIndex = 11;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(283, 228);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(43, 13);
            this.labelControl16.TabIndex = 41;
            this.labelControl16.Text = "Country:";
            // 
            // ltc_FacilityPhone
            // 
            this.ltc_FacilityPhone.Location = new System.Drawing.Point(315, 41);
            this.ltc_FacilityPhone.Name = "ltc_FacilityPhone";
            this.ltc_FacilityPhone.Size = new System.Drawing.Size(129, 20);
            this.ltc_FacilityPhone.TabIndex = 3;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(315, 24);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(70, 13);
            this.labelControl17.TabIndex = 39;
            this.labelControl17.Text = "Facility Phone:";
            // 
            // ltc_AddressZip
            // 
            this.ltc_AddressZip.Location = new System.Drawing.Point(223, 246);
            this.ltc_AddressZip.Name = "ltc_AddressZip";
            this.ltc_AddressZip.Size = new System.Drawing.Size(54, 20);
            this.ltc_AddressZip.TabIndex = 10;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(223, 228);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(20, 13);
            this.labelControl18.TabIndex = 37;
            this.labelControl18.Text = "ZIP:";
            // 
            // ltc_AddressState
            // 
            this.ltc_AddressState.Location = new System.Drawing.Point(136, 246);
            this.ltc_AddressState.Name = "ltc_AddressState";
            this.ltc_AddressState.Size = new System.Drawing.Size(81, 20);
            this.ltc_AddressState.TabIndex = 9;
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(136, 228);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(30, 13);
            this.labelControl19.TabIndex = 35;
            this.labelControl19.Text = "State:";
            // 
            // ltc_AddressCity
            // 
            this.ltc_AddressCity.Location = new System.Drawing.Point(11, 246);
            this.ltc_AddressCity.Name = "ltc_AddressCity";
            this.ltc_AddressCity.Size = new System.Drawing.Size(119, 20);
            this.ltc_AddressCity.TabIndex = 8;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(11, 228);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(23, 13);
            this.labelControl20.TabIndex = 33;
            this.labelControl20.Text = "City:";
            // 
            // ltc_AddressLine2
            // 
            this.ltc_AddressLine2.Location = new System.Drawing.Point(11, 198);
            this.ltc_AddressLine2.Name = "ltc_AddressLine2";
            this.ltc_AddressLine2.Size = new System.Drawing.Size(433, 20);
            this.ltc_AddressLine2.TabIndex = 7;
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(11, 182);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(74, 13);
            this.labelControl21.TabIndex = 31;
            this.labelControl21.Text = "Address Line 2:";
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl22.Appearance.Options.UseFont = true;
            this.labelControl22.Location = new System.Drawing.Point(11, 116);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(94, 14);
            this.labelControl22.TabIndex = 30;
            this.labelControl22.Text = "Facility Address";
            // 
            // ltc_AddressStreet
            // 
            this.ltc_AddressStreet.Location = new System.Drawing.Point(11, 156);
            this.ltc_AddressStreet.Name = "ltc_AddressStreet";
            this.ltc_AddressStreet.Size = new System.Drawing.Size(433, 20);
            this.ltc_AddressStreet.TabIndex = 6;
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(11, 140);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(76, 13);
            this.labelControl23.TabIndex = 28;
            this.labelControl23.Text = "Street Address:";
            // 
            // ltc_FacilityPOCLastName
            // 
            this.ltc_FacilityPOCLastName.Location = new System.Drawing.Point(238, 87);
            this.ltc_FacilityPOCLastName.Name = "ltc_FacilityPOCLastName";
            this.ltc_FacilityPOCLastName.Size = new System.Drawing.Size(206, 20);
            this.ltc_FacilityPOCLastName.TabIndex = 5;
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(238, 69);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(168, 13);
            this.labelControl24.TabIndex = 26;
            this.labelControl24.Text = "Facility point of contact Last name:";
            // 
            // ltc_CaficlityPocFirstName
            // 
            this.ltc_CaficlityPocFirstName.Location = new System.Drawing.Point(11, 87);
            this.ltc_CaficlityPocFirstName.Name = "ltc_CaficlityPocFirstName";
            this.ltc_CaficlityPocFirstName.Size = new System.Drawing.Size(209, 20);
            this.ltc_CaficlityPocFirstName.TabIndex = 4;
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(11, 69);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(169, 13);
            this.labelControl26.TabIndex = 22;
            this.labelControl26.Text = "Facility point of contact First name:";
            // 
            // ltc_FacilityName
            // 
            this.ltc_FacilityName.Location = new System.Drawing.Point(11, 41);
            this.ltc_FacilityName.Name = "ltc_FacilityName";
            this.ltc_FacilityName.Size = new System.Drawing.Size(298, 20);
            this.ltc_FacilityName.TabIndex = 2;
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(11, 23);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(67, 13);
            this.labelControl27.TabIndex = 20;
            this.labelControl27.Text = "Facility Name:";
            // 
            // accordionContentContainer18
            // 
            this.accordionContentContainer18.Controls.Add(this.helthAndMedical_IsuesWithAlzheimer);
            this.accordionContentContainer18.Controls.Add(this.groupControl17);
            this.accordionContentContainer18.Controls.Add(this.groupControl16);
            this.accordionContentContainer18.Controls.Add(this.helthAndMedical_IsuesWithDressing);
            this.accordionContentContainer18.Controls.Add(this.groupAlzheimer);
            this.accordionContentContainer18.Controls.Add(this.groupControl18);
            this.accordionContentContainer18.Controls.Add(this.groupControl14);
            this.accordionContentContainer18.Controls.Add(this.groupControl13);
            this.accordionContentContainer18.Controls.Add(this.groupControl12);
            this.accordionContentContainer18.Controls.Add(this.groupControl11);
            this.accordionContentContainer18.Controls.Add(this.groupControl10);
            this.accordionContentContainer18.Controls.Add(this.groupControl9);
            this.accordionContentContainer18.Controls.Add(this.groupControl8);
            this.accordionContentContainer18.Controls.Add(this.groupControl7);
            this.accordionContentContainer18.Controls.Add(this.groupControl6);
            this.accordionContentContainer18.Controls.Add(this.groupControl5);
            this.accordionContentContainer18.Controls.Add(this.groupControl4);
            this.accordionContentContainer18.Name = "accordionContentContainer18";
            this.accordionContentContainer18.Size = new System.Drawing.Size(0, 1773);
            this.accordionContentContainer18.TabIndex = 19;
            // 
            // helthAndMedical_IsuesWithAlzheimer
            // 
            this.helthAndMedical_IsuesWithAlzheimer.Location = new System.Drawing.Point(8, 1306);
            this.helthAndMedical_IsuesWithAlzheimer.Name = "helthAndMedical_IsuesWithAlzheimer";
            this.helthAndMedical_IsuesWithAlzheimer.Properties.Caption = "ALZHEIMER’S DISEASE OR DEMENTIA";
            this.helthAndMedical_IsuesWithAlzheimer.Size = new System.Drawing.Size(216, 19);
            this.helthAndMedical_IsuesWithAlzheimer.TabIndex = 26;
            this.helthAndMedical_IsuesWithAlzheimer.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithAlzheimer_CheckedChanged);
            // 
            // groupControl17
            // 
            this.groupControl17.Controls.Add(this.helthAndMedical_NotesDDD);
            this.groupControl17.Controls.Add(this.labelControl44);
            this.groupControl17.Controls.Add(this.helthAndMedical_IsuesWithDDD);
            this.groupControl17.Location = new System.Drawing.Point(4, 1547);
            this.groupControl17.Name = "groupControl17";
            this.groupControl17.Size = new System.Drawing.Size(461, 108);
            this.groupControl17.TabIndex = 14;
            // 
            // helthAndMedical_NotesDDD
            // 
            this.helthAndMedical_NotesDDD.Enabled = false;
            this.helthAndMedical_NotesDDD.Location = new System.Drawing.Point(13, 47);
            this.helthAndMedical_NotesDDD.Name = "helthAndMedical_NotesDDD";
            this.helthAndMedical_NotesDDD.Size = new System.Drawing.Size(437, 49);
            this.helthAndMedical_NotesDDD.TabIndex = 34;
            // 
            // labelControl44
            // 
            this.labelControl44.Location = new System.Drawing.Point(13, 27);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(32, 13);
            this.labelControl44.TabIndex = 1;
            this.labelControl44.Text = "Notes:";
            // 
            // helthAndMedical_IsuesWithDDD
            // 
            this.helthAndMedical_IsuesWithDDD.Location = new System.Drawing.Point(5, 6);
            this.helthAndMedical_IsuesWithDDD.Name = "helthAndMedical_IsuesWithDDD";
            this.helthAndMedical_IsuesWithDDD.Properties.Caption = "Diagnosed with DDD applicable issues before 18";
            this.helthAndMedical_IsuesWithDDD.Size = new System.Drawing.Size(330, 19);
            this.helthAndMedical_IsuesWithDDD.TabIndex = 33;
            this.helthAndMedical_IsuesWithDDD.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithDDD_CheckedChanged);
            // 
            // groupControl16
            // 
            this.groupControl16.Controls.Add(this.helthAndMedical_NotesOxygen);
            this.groupControl16.Controls.Add(this.labelControl43);
            this.groupControl16.Controls.Add(this.helthAndMedical_IsuesWithOxygen);
            this.groupControl16.Location = new System.Drawing.Point(4, 1434);
            this.groupControl16.Name = "groupControl16";
            this.groupControl16.Size = new System.Drawing.Size(461, 108);
            this.groupControl16.TabIndex = 13;
            // 
            // helthAndMedical_NotesOxygen
            // 
            this.helthAndMedical_NotesOxygen.Enabled = false;
            this.helthAndMedical_NotesOxygen.Location = new System.Drawing.Point(13, 45);
            this.helthAndMedical_NotesOxygen.Name = "helthAndMedical_NotesOxygen";
            this.helthAndMedical_NotesOxygen.Size = new System.Drawing.Size(437, 49);
            this.helthAndMedical_NotesOxygen.TabIndex = 32;
            // 
            // labelControl43
            // 
            this.labelControl43.Location = new System.Drawing.Point(13, 25);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(228, 13);
            this.labelControl43.TabIndex = 1;
            this.labelControl43.Text = "OXYGEN TREATMENTS frequency and duration:";
            // 
            // helthAndMedical_IsuesWithOxygen
            // 
            this.helthAndMedical_IsuesWithOxygen.Location = new System.Drawing.Point(5, 3);
            this.helthAndMedical_IsuesWithOxygen.Name = "helthAndMedical_IsuesWithOxygen";
            this.helthAndMedical_IsuesWithOxygen.Properties.Caption = "OXYGEN TREATMENTS";
            this.helthAndMedical_IsuesWithOxygen.Size = new System.Drawing.Size(330, 19);
            this.helthAndMedical_IsuesWithOxygen.TabIndex = 30;
            this.helthAndMedical_IsuesWithOxygen.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithOxygen_CheckedChanged);
            // 
            // helthAndMedical_IsuesWithDressing
            // 
            this.helthAndMedical_IsuesWithDressing.Location = new System.Drawing.Point(9, 346);
            this.helthAndMedical_IsuesWithDressing.Name = "helthAndMedical_IsuesWithDressing";
            this.helthAndMedical_IsuesWithDressing.Properties.Caption = "Issues with DRESSING";
            this.helthAndMedical_IsuesWithDressing.Size = new System.Drawing.Size(177, 19);
            this.helthAndMedical_IsuesWithDressing.TabIndex = 6;
            this.helthAndMedical_IsuesWithDressing.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithDressing_CheckedChanged);
            // 
            // groupAlzheimer
            // 
            this.groupAlzheimer.Controls.Add(this.helthAndMedical_AlzheimerDate);
            this.groupAlzheimer.Controls.Add(this.helthAndMedical_AlzheimerField);
            this.groupAlzheimer.Controls.Add(this.label66);
            this.groupAlzheimer.Controls.Add(this.label63);
            this.groupAlzheimer.Controls.Add(this.helthAndMedical_AlzheimerLastName);
            this.groupAlzheimer.Controls.Add(this.labelControl41);
            this.groupAlzheimer.Controls.Add(this.helthAndMedical_AlzheimerFirstName);
            this.groupAlzheimer.Controls.Add(this.labelControl42);
            this.groupAlzheimer.Enabled = false;
            this.groupAlzheimer.Location = new System.Drawing.Point(4, 1304);
            this.groupAlzheimer.Name = "groupAlzheimer";
            this.groupAlzheimer.Size = new System.Drawing.Size(461, 125);
            this.groupAlzheimer.TabIndex = 12;
            // 
            // helthAndMedical_AlzheimerDate
            // 
            this.helthAndMedical_AlzheimerDate.EditValue = new System.DateTime(2019, 7, 1, 0, 0, 0, 0);
            this.helthAndMedical_AlzheimerDate.Location = new System.Drawing.Point(188, 94);
            this.helthAndMedical_AlzheimerDate.Name = "helthAndMedical_AlzheimerDate";
            this.helthAndMedical_AlzheimerDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.helthAndMedical_AlzheimerDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.helthAndMedical_AlzheimerDate.Size = new System.Drawing.Size(257, 20);
            this.helthAndMedical_AlzheimerDate.TabIndex = 30;
            // 
            // helthAndMedical_AlzheimerField
            // 
            this.helthAndMedical_AlzheimerField.Location = new System.Drawing.Point(12, 94);
            this.helthAndMedical_AlzheimerField.Name = "helthAndMedical_AlzheimerField";
            this.helthAndMedical_AlzheimerField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.helthAndMedical_AlzheimerField.Properties.Items.AddRange(new object[] {
            "General Practitioner",
            "Neurologist"});
            this.helthAndMedical_AlzheimerField.Size = new System.Drawing.Size(170, 20);
            this.helthAndMedical_AlzheimerField.TabIndex = 29;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(185, 78);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(248, 13);
            this.label66.TabIndex = 32;
            this.label66.Text = "Date of alzheimer’s disease or dementia diagnosis:";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(10, 78);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(78, 13);
            this.label63.TabIndex = 32;
            this.label63.Text = "Physician field:";
            // 
            // helthAndMedical_AlzheimerLastName
            // 
            this.helthAndMedical_AlzheimerLastName.Location = new System.Drawing.Point(227, 45);
            this.helthAndMedical_AlzheimerLastName.Name = "helthAndMedical_AlzheimerLastName";
            this.helthAndMedical_AlzheimerLastName.Size = new System.Drawing.Size(218, 20);
            this.helthAndMedical_AlzheimerLastName.TabIndex = 28;
            // 
            // labelControl41
            // 
            this.labelControl41.Location = new System.Drawing.Point(227, 27);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(194, 13);
            this.labelControl41.TabIndex = 30;
            this.labelControl41.Text = "Name of diagnosing physician Last Name";
            // 
            // helthAndMedical_AlzheimerFirstName
            // 
            this.helthAndMedical_AlzheimerFirstName.Location = new System.Drawing.Point(12, 45);
            this.helthAndMedical_AlzheimerFirstName.Name = "helthAndMedical_AlzheimerFirstName";
            this.helthAndMedical_AlzheimerFirstName.Size = new System.Drawing.Size(209, 20);
            this.helthAndMedical_AlzheimerFirstName.TabIndex = 27;
            // 
            // labelControl42
            // 
            this.labelControl42.Location = new System.Drawing.Point(12, 27);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(195, 13);
            this.labelControl42.TabIndex = 28;
            this.labelControl42.Text = "Name of diagnosing physician First Name";
            // 
            // groupControl18
            // 
            this.groupControl18.Controls.Add(this.helthAndMedical_NotesOther);
            this.groupControl18.Controls.Add(this.labelControl45);
            this.groupControl18.Controls.Add(this.helthAndMedical_IsuesWithOther);
            this.groupControl18.Location = new System.Drawing.Point(4, 1658);
            this.groupControl18.Name = "groupControl18";
            this.groupControl18.Size = new System.Drawing.Size(461, 108);
            this.groupControl18.TabIndex = 11;
            // 
            // helthAndMedical_NotesOther
            // 
            this.helthAndMedical_NotesOther.Enabled = false;
            this.helthAndMedical_NotesOther.Location = new System.Drawing.Point(13, 45);
            this.helthAndMedical_NotesOther.Name = "helthAndMedical_NotesOther";
            this.helthAndMedical_NotesOther.Size = new System.Drawing.Size(437, 49);
            this.helthAndMedical_NotesOther.TabIndex = 36;
            // 
            // labelControl45
            // 
            this.labelControl45.Location = new System.Drawing.Point(13, 25);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(127, 13);
            this.labelControl45.TabIndex = 1;
            this.labelControl45.Text = "OTHER DIAGNOSIS notes:";
            // 
            // helthAndMedical_IsuesWithOther
            // 
            this.helthAndMedical_IsuesWithOther.Location = new System.Drawing.Point(5, 3);
            this.helthAndMedical_IsuesWithOther.Name = "helthAndMedical_IsuesWithOther";
            this.helthAndMedical_IsuesWithOther.Properties.Caption = "OTHER DIGNOSIS";
            this.helthAndMedical_IsuesWithOther.Size = new System.Drawing.Size(330, 19);
            this.helthAndMedical_IsuesWithOther.TabIndex = 35;
            this.helthAndMedical_IsuesWithOther.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithOther_CheckedChanged);
            // 
            // groupControl14
            // 
            this.groupControl14.Controls.Add(this.helthAndMedical_NotesParalisys);
            this.groupControl14.Controls.Add(this.labelControl40);
            this.groupControl14.Controls.Add(this.helthAndMedical_IsuesWithParalysis);
            this.groupControl14.Location = new System.Drawing.Point(4, 1191);
            this.groupControl14.Name = "groupControl14";
            this.groupControl14.Size = new System.Drawing.Size(461, 108);
            this.groupControl14.TabIndex = 11;
            // 
            // helthAndMedical_NotesParalisys
            // 
            this.helthAndMedical_NotesParalisys.Enabled = false;
            this.helthAndMedical_NotesParalisys.Location = new System.Drawing.Point(13, 45);
            this.helthAndMedical_NotesParalisys.Name = "helthAndMedical_NotesParalisys";
            this.helthAndMedical_NotesParalisys.Size = new System.Drawing.Size(437, 49);
            this.helthAndMedical_NotesParalisys.TabIndex = 25;
            // 
            // labelControl40
            // 
            this.labelControl40.Location = new System.Drawing.Point(13, 25);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(88, 13);
            this.labelControl40.TabIndex = 1;
            this.labelControl40.Text = "PARALYSIS notes:";
            // 
            // helthAndMedical_IsuesWithParalysis
            // 
            this.helthAndMedical_IsuesWithParalysis.Location = new System.Drawing.Point(5, 3);
            this.helthAndMedical_IsuesWithParalysis.Name = "helthAndMedical_IsuesWithParalysis";
            this.helthAndMedical_IsuesWithParalysis.Properties.Caption = "PARALYSIS";
            this.helthAndMedical_IsuesWithParalysis.Size = new System.Drawing.Size(330, 19);
            this.helthAndMedical_IsuesWithParalysis.TabIndex = 24;
            this.helthAndMedical_IsuesWithParalysis.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithParalysis_CheckedChanged);
            // 
            // groupControl13
            // 
            this.groupControl13.Controls.Add(this.helthAndMedical_NotesBehavior);
            this.groupControl13.Controls.Add(this.labelControl39);
            this.groupControl13.Controls.Add(this.helthAndMedical_IsuesWithDisruptive);
            this.groupControl13.Controls.Add(this.helthAndMedical_IsuesWithWandering);
            this.groupControl13.Controls.Add(this.helthAndMedical_IsuesWithSelfInjury);
            this.groupControl13.Controls.Add(this.helthAndMedical_IsuesWithAggression);
            this.groupControl13.Location = new System.Drawing.Point(4, 1054);
            this.groupControl13.Name = "groupControl13";
            this.groupControl13.Size = new System.Drawing.Size(461, 131);
            this.groupControl13.TabIndex = 10;
            this.groupControl13.Text = "ISSUES WITH BEHAVIOR";
            // 
            // helthAndMedical_NotesBehavior
            // 
            this.helthAndMedical_NotesBehavior.Enabled = false;
            this.helthAndMedical_NotesBehavior.Location = new System.Drawing.Point(12, 68);
            this.helthAndMedical_NotesBehavior.Name = "helthAndMedical_NotesBehavior";
            this.helthAndMedical_NotesBehavior.Size = new System.Drawing.Size(437, 49);
            this.helthAndMedical_NotesBehavior.TabIndex = 23;
            // 
            // labelControl39
            // 
            this.labelControl39.Location = new System.Drawing.Point(13, 49);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(153, 13);
            this.labelControl39.TabIndex = 1;
            this.labelControl39.Text = "ISSUES WITH BEHAVIOR notes:";
            // 
            // helthAndMedical_IsuesWithDisruptive
            // 
            this.helthAndMedical_IsuesWithDisruptive.Location = new System.Drawing.Point(261, 24);
            this.helthAndMedical_IsuesWithDisruptive.Name = "helthAndMedical_IsuesWithDisruptive";
            this.helthAndMedical_IsuesWithDisruptive.Properties.Caption = "Disruptive";
            this.helthAndMedical_IsuesWithDisruptive.Size = new System.Drawing.Size(74, 19);
            this.helthAndMedical_IsuesWithDisruptive.TabIndex = 22;
            this.helthAndMedical_IsuesWithDisruptive.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithAggression_CheckedChanged);
            // 
            // helthAndMedical_IsuesWithWandering
            // 
            this.helthAndMedical_IsuesWithWandering.Location = new System.Drawing.Point(177, 24);
            this.helthAndMedical_IsuesWithWandering.Name = "helthAndMedical_IsuesWithWandering";
            this.helthAndMedical_IsuesWithWandering.Properties.Caption = "Wandering";
            this.helthAndMedical_IsuesWithWandering.Size = new System.Drawing.Size(74, 19);
            this.helthAndMedical_IsuesWithWandering.TabIndex = 21;
            this.helthAndMedical_IsuesWithWandering.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithAggression_CheckedChanged);
            // 
            // helthAndMedical_IsuesWithSelfInjury
            // 
            this.helthAndMedical_IsuesWithSelfInjury.Location = new System.Drawing.Point(97, 24);
            this.helthAndMedical_IsuesWithSelfInjury.Name = "helthAndMedical_IsuesWithSelfInjury";
            this.helthAndMedical_IsuesWithSelfInjury.Properties.Caption = "Self injury";
            this.helthAndMedical_IsuesWithSelfInjury.Size = new System.Drawing.Size(74, 19);
            this.helthAndMedical_IsuesWithSelfInjury.TabIndex = 20;
            this.helthAndMedical_IsuesWithSelfInjury.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithAggression_CheckedChanged);
            // 
            // helthAndMedical_IsuesWithAggression
            // 
            this.helthAndMedical_IsuesWithAggression.Location = new System.Drawing.Point(12, 24);
            this.helthAndMedical_IsuesWithAggression.Name = "helthAndMedical_IsuesWithAggression";
            this.helthAndMedical_IsuesWithAggression.Properties.Caption = "Aggression";
            this.helthAndMedical_IsuesWithAggression.Size = new System.Drawing.Size(74, 19);
            this.helthAndMedical_IsuesWithAggression.TabIndex = 19;
            this.helthAndMedical_IsuesWithAggression.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithAggression_CheckedChanged);
            // 
            // groupControl12
            // 
            this.groupControl12.Controls.Add(this.helthAndMedical_NotesOrientation);
            this.groupControl12.Controls.Add(this.labelControl38);
            this.groupControl12.Controls.Add(this.helthAndMedical_IsuesWithOrientation);
            this.groupControl12.Location = new System.Drawing.Point(4, 940);
            this.groupControl12.Name = "groupControl12";
            this.groupControl12.Size = new System.Drawing.Size(461, 108);
            this.groupControl12.TabIndex = 9;
            // 
            // helthAndMedical_NotesOrientation
            // 
            this.helthAndMedical_NotesOrientation.Enabled = false;
            this.helthAndMedical_NotesOrientation.Location = new System.Drawing.Point(13, 45);
            this.helthAndMedical_NotesOrientation.Name = "helthAndMedical_NotesOrientation";
            this.helthAndMedical_NotesOrientation.Size = new System.Drawing.Size(437, 49);
            this.helthAndMedical_NotesOrientation.TabIndex = 18;
            // 
            // labelControl38
            // 
            this.labelControl38.Location = new System.Drawing.Point(13, 25);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(206, 13);
            this.labelControl38.TabIndex = 1;
            this.labelControl38.Text = "ORIENTATION TO TIME AND PLACE notes:";
            // 
            // helthAndMedical_IsuesWithOrientation
            // 
            this.helthAndMedical_IsuesWithOrientation.Location = new System.Drawing.Point(5, 0);
            this.helthAndMedical_IsuesWithOrientation.Name = "helthAndMedical_IsuesWithOrientation";
            this.helthAndMedical_IsuesWithOrientation.Properties.Caption = "ORIENTATION TO TIME AND PLACE";
            this.helthAndMedical_IsuesWithOrientation.Size = new System.Drawing.Size(330, 19);
            this.helthAndMedical_IsuesWithOrientation.TabIndex = 17;
            this.helthAndMedical_IsuesWithOrientation.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithOrientation_CheckedChanged);
            // 
            // groupControl11
            // 
            this.groupControl11.Controls.Add(this.helthAndMedical_IsLegallyBlind);
            this.groupControl11.Controls.Add(this.helthAndMedical_NotesVision);
            this.groupControl11.Controls.Add(this.labelControl37);
            this.groupControl11.Controls.Add(this.helthAndMedical_IsuesWithVision);
            this.groupControl11.Location = new System.Drawing.Point(3, 802);
            this.groupControl11.Name = "groupControl11";
            this.groupControl11.Size = new System.Drawing.Size(461, 131);
            this.groupControl11.TabIndex = 8;
            // 
            // helthAndMedical_IsLegallyBlind
            // 
            this.helthAndMedical_IsLegallyBlind.Location = new System.Drawing.Point(14, 25);
            this.helthAndMedical_IsLegallyBlind.Name = "helthAndMedical_IsLegallyBlind";
            this.helthAndMedical_IsLegallyBlind.Properties.Caption = "Legally blind";
            this.helthAndMedical_IsLegallyBlind.Size = new System.Drawing.Size(88, 19);
            this.helthAndMedical_IsLegallyBlind.TabIndex = 15;
            // 
            // helthAndMedical_NotesVision
            // 
            this.helthAndMedical_NotesVision.Enabled = false;
            this.helthAndMedical_NotesVision.Location = new System.Drawing.Point(13, 71);
            this.helthAndMedical_NotesVision.Name = "helthAndMedical_NotesVision";
            this.helthAndMedical_NotesVision.Size = new System.Drawing.Size(437, 49);
            this.helthAndMedical_NotesVision.TabIndex = 16;
            // 
            // labelControl37
            // 
            this.labelControl37.Location = new System.Drawing.Point(13, 52);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(69, 13);
            this.labelControl37.TabIndex = 1;
            this.labelControl37.Text = "VISION notes:";
            // 
            // helthAndMedical_IsuesWithVision
            // 
            this.helthAndMedical_IsuesWithVision.Location = new System.Drawing.Point(5, 0);
            this.helthAndMedical_IsuesWithVision.Name = "helthAndMedical_IsuesWithVision";
            this.helthAndMedical_IsuesWithVision.Properties.Caption = "Issues with VISION";
            this.helthAndMedical_IsuesWithVision.Size = new System.Drawing.Size(120, 19);
            this.helthAndMedical_IsuesWithVision.TabIndex = 0;
            this.helthAndMedical_IsuesWithVision.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithVision_CheckedChanged);
            // 
            // groupControl10
            // 
            this.groupControl10.Controls.Add(this.helthAndMedical_NotesIncontinence);
            this.groupControl10.Controls.Add(this.labelControl36);
            this.groupControl10.Controls.Add(this.helthAndMedical_IsuesWithBowel);
            this.groupControl10.Controls.Add(this.helthAndMedical_IsuesWithBladder);
            this.groupControl10.Location = new System.Drawing.Point(4, 687);
            this.groupControl10.Name = "groupControl10";
            this.groupControl10.Size = new System.Drawing.Size(461, 108);
            this.groupControl10.TabIndex = 7;
            this.groupControl10.Text = "INCONTINENCE";
            // 
            // helthAndMedical_NotesIncontinence
            // 
            this.helthAndMedical_NotesIncontinence.Enabled = false;
            this.helthAndMedical_NotesIncontinence.Location = new System.Drawing.Point(13, 45);
            this.helthAndMedical_NotesIncontinence.Name = "helthAndMedical_NotesIncontinence";
            this.helthAndMedical_NotesIncontinence.Size = new System.Drawing.Size(437, 49);
            this.helthAndMedical_NotesIncontinence.TabIndex = 14;
            // 
            // labelControl36
            // 
            this.labelControl36.Location = new System.Drawing.Point(13, 25);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(110, 13);
            this.labelControl36.TabIndex = 1;
            this.labelControl36.Text = "INCONTINENCE notes:";
            // 
            // helthAndMedical_IsuesWithBowel
            // 
            this.helthAndMedical_IsuesWithBowel.Location = new System.Drawing.Point(160, 0);
            this.helthAndMedical_IsuesWithBowel.Name = "helthAndMedical_IsuesWithBowel";
            this.helthAndMedical_IsuesWithBowel.Properties.Caption = "Bowel";
            this.helthAndMedical_IsuesWithBowel.Size = new System.Drawing.Size(74, 19);
            this.helthAndMedical_IsuesWithBowel.TabIndex = 13;
            this.helthAndMedical_IsuesWithBowel.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithBladder_CheckedChanged);
            // 
            // helthAndMedical_IsuesWithBladder
            // 
            this.helthAndMedical_IsuesWithBladder.Location = new System.Drawing.Point(97, 0);
            this.helthAndMedical_IsuesWithBladder.Name = "helthAndMedical_IsuesWithBladder";
            this.helthAndMedical_IsuesWithBladder.Properties.Caption = "Bladder";
            this.helthAndMedical_IsuesWithBladder.Size = new System.Drawing.Size(74, 19);
            this.helthAndMedical_IsuesWithBladder.TabIndex = 12;
            this.helthAndMedical_IsuesWithBladder.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithBladder_CheckedChanged);
            // 
            // groupControl9
            // 
            this.groupControl9.Controls.Add(this.helthAndMedical_NotesToileting);
            this.groupControl9.Controls.Add(this.labelControl35);
            this.groupControl9.Controls.Add(this.helthAndMedical_IsuesWithToileting);
            this.groupControl9.Location = new System.Drawing.Point(4, 573);
            this.groupControl9.Name = "groupControl9";
            this.groupControl9.Size = new System.Drawing.Size(461, 108);
            this.groupControl9.TabIndex = 6;
            // 
            // helthAndMedical_NotesToileting
            // 
            this.helthAndMedical_NotesToileting.Enabled = false;
            this.helthAndMedical_NotesToileting.Location = new System.Drawing.Point(13, 45);
            this.helthAndMedical_NotesToileting.Name = "helthAndMedical_NotesToileting";
            this.helthAndMedical_NotesToileting.Size = new System.Drawing.Size(437, 49);
            this.helthAndMedical_NotesToileting.TabIndex = 11;
            // 
            // labelControl35
            // 
            this.labelControl35.Location = new System.Drawing.Point(13, 25);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(87, 13);
            this.labelControl35.TabIndex = 1;
            this.labelControl35.Text = "TOILETING notes:";
            // 
            // helthAndMedical_IsuesWithToileting
            // 
            this.helthAndMedical_IsuesWithToileting.Location = new System.Drawing.Point(5, 0);
            this.helthAndMedical_IsuesWithToileting.Name = "helthAndMedical_IsuesWithToileting";
            this.helthAndMedical_IsuesWithToileting.Properties.Caption = "Issues with TOILETING";
            this.helthAndMedical_IsuesWithToileting.Size = new System.Drawing.Size(330, 19);
            this.helthAndMedical_IsuesWithToileting.TabIndex = 10;
            this.helthAndMedical_IsuesWithToileting.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithToileting_CheckedChanged);
            // 
            // groupControl8
            // 
            this.groupControl8.Controls.Add(this.helthAndMedical_NotesEating);
            this.groupControl8.Controls.Add(this.labelControl34);
            this.groupControl8.Controls.Add(this.helthAndMedical_IsuesWithEating);
            this.groupControl8.Location = new System.Drawing.Point(4, 459);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(461, 108);
            this.groupControl8.TabIndex = 5;
            // 
            // helthAndMedical_NotesEating
            // 
            this.helthAndMedical_NotesEating.Enabled = false;
            this.helthAndMedical_NotesEating.Location = new System.Drawing.Point(13, 45);
            this.helthAndMedical_NotesEating.Name = "helthAndMedical_NotesEating";
            this.helthAndMedical_NotesEating.Size = new System.Drawing.Size(437, 49);
            this.helthAndMedical_NotesEating.TabIndex = 9;
            // 
            // labelControl34
            // 
            this.labelControl34.Location = new System.Drawing.Point(13, 25);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(175, 13);
            this.labelControl34.TabIndex = 1;
            this.labelControl34.Text = "EATING/MEAL PREPARATION notes:";
            // 
            // helthAndMedical_IsuesWithEating
            // 
            this.helthAndMedical_IsuesWithEating.Location = new System.Drawing.Point(5, 0);
            this.helthAndMedical_IsuesWithEating.Name = "helthAndMedical_IsuesWithEating";
            this.helthAndMedical_IsuesWithEating.Properties.Caption = "Issues with EATING/MEAL PREPARATION";
            this.helthAndMedical_IsuesWithEating.Size = new System.Drawing.Size(330, 19);
            this.helthAndMedical_IsuesWithEating.TabIndex = 8;
            this.helthAndMedical_IsuesWithEating.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithEating_CheckedChanged);
            // 
            // groupControl7
            // 
            this.groupControl7.Controls.Add(this.helthAndMedical_NotesDressing);
            this.groupControl7.Controls.Add(this.labelControl33);
            this.groupControl7.Enabled = false;
            this.groupControl7.Location = new System.Drawing.Point(4, 345);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(461, 108);
            this.groupControl7.TabIndex = 4;
            // 
            // helthAndMedical_NotesDressing
            // 
            this.helthAndMedical_NotesDressing.Enabled = false;
            this.helthAndMedical_NotesDressing.Location = new System.Drawing.Point(13, 45);
            this.helthAndMedical_NotesDressing.Name = "helthAndMedical_NotesDressing";
            this.helthAndMedical_NotesDressing.Size = new System.Drawing.Size(437, 49);
            this.helthAndMedical_NotesDressing.TabIndex = 7;
            // 
            // labelControl33
            // 
            this.labelControl33.Location = new System.Drawing.Point(13, 25);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(84, 13);
            this.labelControl33.TabIndex = 1;
            this.labelControl33.Text = "DRESSING notes:";
            // 
            // groupControl6
            // 
            this.groupControl6.Controls.Add(this.helthAndMedical_NotesBathing);
            this.groupControl6.Controls.Add(this.labelControl32);
            this.groupControl6.Controls.Add(this.helthAndMedical_IsuesWithBathing);
            this.groupControl6.Location = new System.Drawing.Point(4, 231);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(461, 108);
            this.groupControl6.TabIndex = 3;
            // 
            // helthAndMedical_NotesBathing
            // 
            this.helthAndMedical_NotesBathing.Enabled = false;
            this.helthAndMedical_NotesBathing.Location = new System.Drawing.Point(13, 45);
            this.helthAndMedical_NotesBathing.Name = "helthAndMedical_NotesBathing";
            this.helthAndMedical_NotesBathing.Size = new System.Drawing.Size(437, 49);
            this.helthAndMedical_NotesBathing.TabIndex = 5;
            // 
            // labelControl32
            // 
            this.labelControl32.Location = new System.Drawing.Point(13, 25);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(78, 13);
            this.labelControl32.TabIndex = 1;
            this.labelControl32.Text = "BATHING notes:";
            // 
            // helthAndMedical_IsuesWithBathing
            // 
            this.helthAndMedical_IsuesWithBathing.Location = new System.Drawing.Point(5, 0);
            this.helthAndMedical_IsuesWithBathing.Name = "helthAndMedical_IsuesWithBathing";
            this.helthAndMedical_IsuesWithBathing.Properties.Caption = "Issues with BATHING";
            this.helthAndMedical_IsuesWithBathing.Size = new System.Drawing.Size(177, 19);
            this.helthAndMedical_IsuesWithBathing.TabIndex = 4;
            this.helthAndMedical_IsuesWithBathing.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithBathing_CheckedChanged);
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.helthAndMedical_NotesTransferring);
            this.groupControl5.Controls.Add(this.labelControl31);
            this.groupControl5.Controls.Add(this.helthAndMedical_IsuesWithTransferring);
            this.groupControl5.Location = new System.Drawing.Point(4, 117);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(461, 108);
            this.groupControl5.TabIndex = 2;
            // 
            // helthAndMedical_NotesTransferring
            // 
            this.helthAndMedical_NotesTransferring.Enabled = false;
            this.helthAndMedical_NotesTransferring.Location = new System.Drawing.Point(13, 45);
            this.helthAndMedical_NotesTransferring.Name = "helthAndMedical_NotesTransferring";
            this.helthAndMedical_NotesTransferring.Size = new System.Drawing.Size(437, 49);
            this.helthAndMedical_NotesTransferring.TabIndex = 3;
            // 
            // labelControl31
            // 
            this.labelControl31.Location = new System.Drawing.Point(13, 25);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(111, 13);
            this.labelControl31.TabIndex = 1;
            this.labelControl31.Text = "TRANSFERRING notes:";
            // 
            // helthAndMedical_IsuesWithTransferring
            // 
            this.helthAndMedical_IsuesWithTransferring.Location = new System.Drawing.Point(5, 0);
            this.helthAndMedical_IsuesWithTransferring.Name = "helthAndMedical_IsuesWithTransferring";
            this.helthAndMedical_IsuesWithTransferring.Properties.Caption = "Issues with TRANSFERRING";
            this.helthAndMedical_IsuesWithTransferring.Size = new System.Drawing.Size(177, 19);
            this.helthAndMedical_IsuesWithTransferring.TabIndex = 2;
            this.helthAndMedical_IsuesWithTransferring.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithTransferring_CheckedChanged);
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.helthAndMedical_NotesMobility);
            this.groupControl4.Controls.Add(this.labelControl30);
            this.groupControl4.Controls.Add(this.helthAndMedical_IsuesWithMobility);
            this.groupControl4.Location = new System.Drawing.Point(4, 3);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(461, 108);
            this.groupControl4.TabIndex = 1;
            // 
            // helthAndMedical_NotesMobility
            // 
            this.helthAndMedical_NotesMobility.Enabled = false;
            this.helthAndMedical_NotesMobility.Location = new System.Drawing.Point(13, 45);
            this.helthAndMedical_NotesMobility.Name = "helthAndMedical_NotesMobility";
            this.helthAndMedical_NotesMobility.Size = new System.Drawing.Size(437, 49);
            this.helthAndMedical_NotesMobility.TabIndex = 1;
            // 
            // labelControl30
            // 
            this.labelControl30.Location = new System.Drawing.Point(13, 25);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(81, 13);
            this.labelControl30.TabIndex = 1;
            this.labelControl30.Text = "MOBILITY notes:";
            // 
            // helthAndMedical_IsuesWithMobility
            // 
            this.helthAndMedical_IsuesWithMobility.Location = new System.Drawing.Point(5, 0);
            this.helthAndMedical_IsuesWithMobility.Name = "helthAndMedical_IsuesWithMobility";
            this.helthAndMedical_IsuesWithMobility.Properties.Caption = "Issues with MOBILITY";
            this.helthAndMedical_IsuesWithMobility.Size = new System.Drawing.Size(177, 19);
            this.helthAndMedical_IsuesWithMobility.TabIndex = 0;
            this.helthAndMedical_IsuesWithMobility.CheckedChanged += new System.EventHandler(this.HelthAndMedical_IsuesWithMobility_CheckedChanged_1);
            // 
            // accordionContentContainer19
            // 
            this.accordionContentContainer19.Controls.Add(this.financial_IsOwningHome);
            this.accordionContentContainer19.Controls.Add(this.groupControl24);
            this.accordionContentContainer19.Controls.Add(this.groupHomeOwned);
            this.accordionContentContainer19.Controls.Add(this.financial_BurialAmount);
            this.accordionContentContainer19.Controls.Add(this.label67);
            this.accordionContentContainer19.Controls.Add(this.financial_isBural);
            this.accordionContentContainer19.Controls.Add(this.groupControl22);
            this.accordionContentContainer19.Controls.Add(this.financial_PolicyNotes);
            this.accordionContentContainer19.Controls.Add(this.financial_IsOwningPolicy);
            this.accordionContentContainer19.Controls.Add(this.groupControl21);
            this.accordionContentContainer19.Controls.Add(this.groupControl20);
            this.accordionContentContainer19.Controls.Add(this.groupControl19);
            this.accordionContentContainer19.Name = "accordionContentContainer19";
            this.accordionContentContainer19.Size = new System.Drawing.Size(0, 835);
            this.accordionContentContainer19.TabIndex = 20;
            // 
            // financial_IsOwningHome
            // 
            this.financial_IsOwningHome.Location = new System.Drawing.Point(17, 634);
            this.financial_IsOwningHome.Name = "financial_IsOwningHome";
            this.financial_IsOwningHome.Properties.Caption = "Home Owned";
            this.financial_IsOwningHome.Size = new System.Drawing.Size(93, 19);
            this.financial_IsOwningHome.TabIndex = 9;
            this.financial_IsOwningHome.CheckedChanged += new System.EventHandler(this.Financial_IsOwningHome_CheckedChanged);
            // 
            // groupControl24
            // 
            this.groupControl24.Controls.Add(this.financial_vehicles);
            this.groupControl24.Location = new System.Drawing.Point(9, 697);
            this.groupControl24.Name = "groupControl24";
            this.groupControl24.Size = new System.Drawing.Size(457, 129);
            this.groupControl24.TabIndex = 10;
            this.groupControl24.Text = "Vehicles";
            // 
            // financial_vehicles
            // 
            this.financial_vehicles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.financial_vehicles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColVehicle_Make,
            this.ColVehicle_Model,
            this.ColVehicle_Year,
            this.ColVehicle_Milage});
            this.financial_vehicles.Location = new System.Drawing.Point(6, 24);
            this.financial_vehicles.Name = "financial_vehicles";
            this.financial_vehicles.Size = new System.Drawing.Size(440, 95);
            this.financial_vehicles.TabIndex = 13;
            // 
            // ColVehicle_Make
            // 
            this.ColVehicle_Make.HeaderText = "Make";
            this.ColVehicle_Make.Name = "ColVehicle_Make";
            // 
            // ColVehicle_Model
            // 
            this.ColVehicle_Model.HeaderText = "Model";
            this.ColVehicle_Model.Name = "ColVehicle_Model";
            // 
            // ColVehicle_Year
            // 
            this.ColVehicle_Year.HeaderText = "Year";
            this.ColVehicle_Year.Name = "ColVehicle_Year";
            this.ColVehicle_Year.Width = 70;
            // 
            // ColVehicle_Milage
            // 
            this.ColVehicle_Milage.HeaderText = "Milage";
            this.ColVehicle_Milage.Name = "ColVehicle_Milage";
            this.ColVehicle_Milage.Width = 80;
            // 
            // groupHomeOwned
            // 
            this.groupHomeOwned.Controls.Add(this.financial_IsHomeOtherOccupants);
            this.groupHomeOwned.Controls.Add(this.financial_IsHomeReverseMortgage);
            this.groupHomeOwned.Controls.Add(this.financial_IsHomeMortgage);
            this.groupHomeOwned.Enabled = false;
            this.groupHomeOwned.Location = new System.Drawing.Point(9, 634);
            this.groupHomeOwned.Name = "groupHomeOwned";
            this.groupHomeOwned.Size = new System.Drawing.Size(456, 57);
            this.groupHomeOwned.TabIndex = 9;
            // 
            // financial_IsHomeOtherOccupants
            // 
            this.financial_IsHomeOtherOccupants.Location = new System.Drawing.Point(244, 25);
            this.financial_IsHomeOtherOccupants.Name = "financial_IsHomeOtherOccupants";
            this.financial_IsHomeOtherOccupants.Properties.Caption = "Other Occupants";
            this.financial_IsHomeOtherOccupants.Size = new System.Drawing.Size(129, 19);
            this.financial_IsHomeOtherOccupants.TabIndex = 12;
            // 
            // financial_IsHomeReverseMortgage
            // 
            this.financial_IsHomeReverseMortgage.Location = new System.Drawing.Point(109, 25);
            this.financial_IsHomeReverseMortgage.Name = "financial_IsHomeReverseMortgage";
            this.financial_IsHomeReverseMortgage.Properties.Caption = "Reverse Mortgage";
            this.financial_IsHomeReverseMortgage.Size = new System.Drawing.Size(129, 19);
            this.financial_IsHomeReverseMortgage.TabIndex = 11;
            // 
            // financial_IsHomeMortgage
            // 
            this.financial_IsHomeMortgage.Location = new System.Drawing.Point(12, 25);
            this.financial_IsHomeMortgage.Name = "financial_IsHomeMortgage";
            this.financial_IsHomeMortgage.Properties.Caption = "Mortgage";
            this.financial_IsHomeMortgage.Size = new System.Drawing.Size(75, 19);
            this.financial_IsHomeMortgage.TabIndex = 10;
            // 
            // financial_BurialAmount
            // 
            this.financial_BurialAmount.Location = new System.Drawing.Point(213, 594);
            this.financial_BurialAmount.Name = "financial_BurialAmount";
            this.financial_BurialAmount.Size = new System.Drawing.Size(100, 20);
            this.financial_BurialAmount.TabIndex = 8;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(162, 598);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(48, 13);
            this.label67.TabIndex = 7;
            this.label67.Text = "Amount:";
            // 
            // financial_isBural
            // 
            this.financial_isBural.Location = new System.Drawing.Point(21, 595);
            this.financial_isBural.Name = "financial_isBural";
            this.financial_isBural.Properties.Caption = "BURIAL ARRANGEMENT";
            this.financial_isBural.Size = new System.Drawing.Size(135, 19);
            this.financial_isBural.TabIndex = 7;
            this.financial_isBural.CheckedChanged += new System.EventHandler(this.Financial_isBural_CheckedChanged);
            // 
            // groupControl22
            // 
            this.groupControl22.Controls.Add(this.financial_gridIncome);
            this.groupControl22.Location = new System.Drawing.Point(8, 447);
            this.groupControl22.Name = "groupControl22";
            this.groupControl22.Size = new System.Drawing.Size(457, 129);
            this.groupControl22.TabIndex = 5;
            this.groupControl22.Text = "Income, Total Monthly Income: $0";
            // 
            // financial_gridIncome
            // 
            this.financial_gridIncome.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.financial_gridIncome.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColIncome_Source,
            this.ColSource_Amount});
            this.financial_gridIncome.Location = new System.Drawing.Point(6, 24);
            this.financial_gridIncome.Name = "financial_gridIncome";
            this.financial_gridIncome.Size = new System.Drawing.Size(440, 95);
            this.financial_gridIncome.TabIndex = 6;
            // 
            // ColIncome_Source
            // 
            this.ColIncome_Source.HeaderText = "Source";
            this.ColIncome_Source.Name = "ColIncome_Source";
            this.ColIncome_Source.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColIncome_Source.Width = 200;
            // 
            // ColSource_Amount
            // 
            this.ColSource_Amount.HeaderText = "Amount Monthly";
            this.ColSource_Amount.Name = "ColSource_Amount";
            this.ColSource_Amount.Width = 120;
            // 
            // financial_PolicyNotes
            // 
            this.financial_PolicyNotes.EditValue = "explain";
            this.financial_PolicyNotes.Enabled = false;
            this.financial_PolicyNotes.Location = new System.Drawing.Point(7, 386);
            this.financial_PolicyNotes.Name = "financial_PolicyNotes";
            this.financial_PolicyNotes.Size = new System.Drawing.Size(458, 55);
            this.financial_PolicyNotes.TabIndex = 5;
            // 
            // financial_IsOwningPolicy
            // 
            this.financial_IsOwningPolicy.Location = new System.Drawing.Point(5, 363);
            this.financial_IsOwningPolicy.Name = "financial_IsOwningPolicy";
            this.financial_IsOwningPolicy.Properties.Caption = "Does the applicant own a policy on someone other than themselves?";
            this.financial_IsOwningPolicy.Size = new System.Drawing.Size(460, 19);
            this.financial_IsOwningPolicy.TabIndex = 4;
            this.financial_IsOwningPolicy.CheckedChanged += new System.EventHandler(this.Financial_IsOwningPolicy_CheckedChanged);
            // 
            // groupControl21
            // 
            this.groupControl21.Controls.Add(this.financial_LifeCash);
            this.groupControl21.Controls.Add(this.labelControl46);
            this.groupControl21.Controls.Add(this.financial_LifeInstitutionName);
            this.groupControl21.Controls.Add(this.labelControl47);
            this.groupControl21.Location = new System.Drawing.Point(4, 276);
            this.groupControl21.Name = "groupControl21";
            this.groupControl21.Size = new System.Drawing.Size(461, 80);
            this.groupControl21.TabIndex = 2;
            this.groupControl21.Text = "Life Insurance";
            // 
            // financial_LifeCash
            // 
            this.financial_LifeCash.Location = new System.Drawing.Point(241, 44);
            this.financial_LifeCash.Name = "financial_LifeCash";
            this.financial_LifeCash.Size = new System.Drawing.Size(206, 20);
            this.financial_LifeCash.TabIndex = 3;
            // 
            // labelControl46
            // 
            this.labelControl46.Location = new System.Drawing.Point(241, 26);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(108, 13);
            this.labelControl46.TabIndex = 30;
            this.labelControl46.Text = "Cash Surrender Value:";
            // 
            // financial_LifeInstitutionName
            // 
            this.financial_LifeInstitutionName.Location = new System.Drawing.Point(14, 44);
            this.financial_LifeInstitutionName.Name = "financial_LifeInstitutionName";
            this.financial_LifeInstitutionName.Size = new System.Drawing.Size(209, 20);
            this.financial_LifeInstitutionName.TabIndex = 2;
            // 
            // labelControl47
            // 
            this.labelControl47.Location = new System.Drawing.Point(14, 26);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(82, 13);
            this.labelControl47.TabIndex = 28;
            this.labelControl47.Text = "Institution name:";
            // 
            // groupControl20
            // 
            this.groupControl20.Controls.Add(this.financial_gridRetirement);
            this.groupControl20.Location = new System.Drawing.Point(4, 140);
            this.groupControl20.Name = "groupControl20";
            this.groupControl20.Size = new System.Drawing.Size(461, 129);
            this.groupControl20.TabIndex = 1;
            this.groupControl20.Text = "Retirement (IRA 401k, Pension)";
            // 
            // financial_gridRetirement
            // 
            this.financial_gridRetirement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.financial_gridRetirement.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColRetirement_Ret,
            this.ColRetirement_Institution,
            this.ColRetirement_Balance});
            this.financial_gridRetirement.Location = new System.Drawing.Point(6, 24);
            this.financial_gridRetirement.Name = "financial_gridRetirement";
            this.financial_gridRetirement.Size = new System.Drawing.Size(450, 95);
            this.financial_gridRetirement.TabIndex = 1;
            // 
            // ColRetirement_Ret
            // 
            this.ColRetirement_Ret.HeaderText = "Retirement";
            this.ColRetirement_Ret.Items.AddRange(new object[] {
            "IRA",
            "401k",
            "Pension",
            "Other"});
            this.ColRetirement_Ret.Name = "ColRetirement_Ret";
            this.ColRetirement_Ret.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColRetirement_Ret.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColRetirement_Ret.Width = 120;
            // 
            // ColRetirement_Institution
            // 
            this.ColRetirement_Institution.HeaderText = "Institution";
            this.ColRetirement_Institution.Name = "ColRetirement_Institution";
            this.ColRetirement_Institution.Width = 150;
            // 
            // ColRetirement_Balance
            // 
            this.ColRetirement_Balance.HeaderText = "Ballance";
            this.ColRetirement_Balance.Name = "ColRetirement_Balance";
            // 
            // groupControl19
            // 
            this.groupControl19.Controls.Add(this.financial_gridResourceAccounts);
            this.groupControl19.Location = new System.Drawing.Point(4, 3);
            this.groupControl19.Name = "groupControl19";
            this.groupControl19.Size = new System.Drawing.Size(461, 131);
            this.groupControl19.TabIndex = 0;
            this.groupControl19.Text = "COUNTABLE RESOURCES";
            // 
            // financial_gridResourceAccounts
            // 
            this.financial_gridResourceAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.financial_gridResourceAccounts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColResource_BankAccType,
            this.ColResource_BankAccInstitution,
            this.ColResource_BankAccBallance});
            this.financial_gridResourceAccounts.Location = new System.Drawing.Point(6, 24);
            this.financial_gridResourceAccounts.Name = "financial_gridResourceAccounts";
            this.financial_gridResourceAccounts.Size = new System.Drawing.Size(450, 95);
            this.financial_gridResourceAccounts.TabIndex = 0;
            // 
            // ColResource_BankAccType
            // 
            this.ColResource_BankAccType.HeaderText = "Account Type";
            this.ColResource_BankAccType.Items.AddRange(new object[] {
            "Checking",
            "Savings",
            "Other"});
            this.ColResource_BankAccType.Name = "ColResource_BankAccType";
            this.ColResource_BankAccType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColResource_BankAccType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColResource_BankAccType.Width = 120;
            // 
            // ColResource_BankAccInstitution
            // 
            this.ColResource_BankAccInstitution.HeaderText = "Bank Institution";
            this.ColResource_BankAccInstitution.Name = "ColResource_BankAccInstitution";
            this.ColResource_BankAccInstitution.Width = 150;
            // 
            // ColResource_BankAccBallance
            // 
            this.ColResource_BankAccBallance.HeaderText = "Ballance";
            this.ColResource_BankAccBallance.Name = "ColResource_BankAccBallance";
            // 
            // accordionContentContainer20
            // 
            this.accordionContentContainer20.Controls.Add(this.lookback_OtherNotes);
            this.accordionContentContainer20.Controls.Add(this.label79);
            this.accordionContentContainer20.Controls.Add(this.lookback_GivenCash);
            this.accordionContentContainer20.Controls.Add(this.lookback_TransferredFunds);
            this.accordionContentContainer20.Controls.Add(this.lookback_SoldCarlLast5);
            this.accordionContentContainer20.Controls.Add(this.lookback_SoldHomeLast5);
            this.accordionContentContainer20.Name = "accordionContentContainer20";
            this.accordionContentContainer20.Size = new System.Drawing.Size(0, 197);
            this.accordionContentContainer20.TabIndex = 22;
            // 
            // lookback_OtherNotes
            // 
            this.lookback_OtherNotes.Location = new System.Drawing.Point(14, 131);
            this.lookback_OtherNotes.Name = "lookback_OtherNotes";
            this.lookback_OtherNotes.Size = new System.Drawing.Size(438, 57);
            this.lookback_OtherNotes.TabIndex = 4;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(14, 114);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(335, 13);
            this.label79.TabIndex = 1;
            this.label79.Text = "Other major financial commitments or decision in the past five years:";
            // 
            // lookback_GivenCash
            // 
            this.lookback_GivenCash.Location = new System.Drawing.Point(14, 88);
            this.lookback_GivenCash.Name = "lookback_GivenCash";
            this.lookback_GivenCash.Properties.Caption = "Given a cash gift in the last five years? (not including small gifts for birthday" +
    "s or holidays)";
            this.lookback_GivenCash.Size = new System.Drawing.Size(451, 19);
            this.lookback_GivenCash.TabIndex = 3;
            // 
            // lookback_TransferredFunds
            // 
            this.lookback_TransferredFunds.Location = new System.Drawing.Point(14, 63);
            this.lookback_TransferredFunds.Name = "lookback_TransferredFunds";
            this.lookback_TransferredFunds.Properties.Caption = "Transferred any funds out of your name in the last five years";
            this.lookback_TransferredFunds.Size = new System.Drawing.Size(451, 19);
            this.lookback_TransferredFunds.TabIndex = 2;
            // 
            // lookback_SoldCarlLast5
            // 
            this.lookback_SoldCarlLast5.Location = new System.Drawing.Point(14, 38);
            this.lookback_SoldCarlLast5.Name = "lookback_SoldCarlLast5";
            this.lookback_SoldCarlLast5.Properties.Caption = "Sold or transferred ownership of a car within the last five years";
            this.lookback_SoldCarlLast5.Size = new System.Drawing.Size(451, 19);
            this.lookback_SoldCarlLast5.TabIndex = 1;
            // 
            // lookback_SoldHomeLast5
            // 
            this.lookback_SoldHomeLast5.Location = new System.Drawing.Point(14, 13);
            this.lookback_SoldHomeLast5.Name = "lookback_SoldHomeLast5";
            this.lookback_SoldHomeLast5.Properties.Caption = "Sold or transferred ownership of a home within the last five years";
            this.lookback_SoldHomeLast5.Size = new System.Drawing.Size(451, 19);
            this.lookback_SoldHomeLast5.TabIndex = 0;
            // 
            // accordionContentContainer21
            // 
            this.accordionContentContainer21.Controls.Add(this.clientNeeds_OtherNotes);
            this.accordionContentContainer21.Controls.Add(this.label80);
            this.accordionContentContainer21.Controls.Add(this.clientNeeds_RealEstate);
            this.accordionContentContainer21.Controls.Add(this.clientNeeds_DocPrep);
            this.accordionContentContainer21.Controls.Add(this.clientNeeds_Placement);
            this.accordionContentContainer21.Controls.Add(this.clientNeeds_Veteran);
            this.accordionContentContainer21.Controls.Add(this.clientNeeds_ALTCS);
            this.accordionContentContainer21.Name = "accordionContentContainer21";
            this.accordionContentContainer21.Size = new System.Drawing.Size(0, 226);
            this.accordionContentContainer21.TabIndex = 23;
            // 
            // clientNeeds_OtherNotes
            // 
            this.clientNeeds_OtherNotes.Location = new System.Drawing.Point(14, 151);
            this.clientNeeds_OtherNotes.Name = "clientNeeds_OtherNotes";
            this.clientNeeds_OtherNotes.Size = new System.Drawing.Size(438, 57);
            this.clientNeeds_OtherNotes.TabIndex = 5;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(14, 134);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(89, 13);
            this.label80.TabIndex = 3;
            this.label80.Text = "Additional Notes:";
            // 
            // clientNeeds_RealEstate
            // 
            this.clientNeeds_RealEstate.Location = new System.Drawing.Point(24, 103);
            this.clientNeeds_RealEstate.Name = "clientNeeds_RealEstate";
            this.clientNeeds_RealEstate.Properties.Caption = "Real estate";
            this.clientNeeds_RealEstate.Size = new System.Drawing.Size(201, 19);
            this.clientNeeds_RealEstate.TabIndex = 4;
            // 
            // clientNeeds_DocPrep
            // 
            this.clientNeeds_DocPrep.Location = new System.Drawing.Point(24, 78);
            this.clientNeeds_DocPrep.Name = "clientNeeds_DocPrep";
            this.clientNeeds_DocPrep.Properties.Caption = "Document Preparation";
            this.clientNeeds_DocPrep.Size = new System.Drawing.Size(201, 19);
            this.clientNeeds_DocPrep.TabIndex = 3;
            // 
            // clientNeeds_Placement
            // 
            this.clientNeeds_Placement.Location = new System.Drawing.Point(24, 53);
            this.clientNeeds_Placement.Name = "clientNeeds_Placement";
            this.clientNeeds_Placement.Properties.Caption = "Placement";
            this.clientNeeds_Placement.Size = new System.Drawing.Size(201, 19);
            this.clientNeeds_Placement.TabIndex = 2;
            // 
            // clientNeeds_Veteran
            // 
            this.clientNeeds_Veteran.Location = new System.Drawing.Point(24, 28);
            this.clientNeeds_Veteran.Name = "clientNeeds_Veteran";
            this.clientNeeds_Veteran.Properties.Caption = "Veterans benefits";
            this.clientNeeds_Veteran.Size = new System.Drawing.Size(201, 19);
            this.clientNeeds_Veteran.TabIndex = 1;
            // 
            // clientNeeds_ALTCS
            // 
            this.clientNeeds_ALTCS.Location = new System.Drawing.Point(24, 3);
            this.clientNeeds_ALTCS.Name = "clientNeeds_ALTCS";
            this.clientNeeds_ALTCS.Properties.Caption = "ALTCS application";
            this.clientNeeds_ALTCS.Size = new System.Drawing.Size(201, 19);
            this.clientNeeds_ALTCS.TabIndex = 0;
            // 
            // accordionContentContainer22
            // 
            this.accordionContentContainer22.Controls.Add(this.autoEmail_SendWhenStatusIsChanged);
            this.accordionContentContainer22.Controls.Add(this.autoEmail_SendWhenDocIsRejected);
            this.accordionContentContainer22.Controls.Add(this.autoEmail_SendWhenDocIsApproved);
            this.accordionContentContainer22.Name = "accordionContentContainer22";
            this.accordionContentContainer22.Size = new System.Drawing.Size(468, 76);
            this.accordionContentContainer22.TabIndex = 27;
            // 
            // autoEmail_SendWhenStatusIsChanged
            // 
            this.autoEmail_SendWhenStatusIsChanged.AutoSize = true;
            this.autoEmail_SendWhenStatusIsChanged.Location = new System.Drawing.Point(7, 52);
            this.autoEmail_SendWhenStatusIsChanged.Name = "autoEmail_SendWhenStatusIsChanged";
            this.autoEmail_SendWhenStatusIsChanged.Size = new System.Drawing.Size(275, 17);
            this.autoEmail_SendWhenStatusIsChanged.TabIndex = 0;
            this.autoEmail_SendWhenStatusIsChanged.Text = "Automatically Send email when STATUS is CHANGED";
            this.autoEmail_SendWhenStatusIsChanged.UseVisualStyleBackColor = true;
            // 
            // autoEmail_SendWhenDocIsRejected
            // 
            this.autoEmail_SendWhenDocIsRejected.AutoSize = true;
            this.autoEmail_SendWhenDocIsRejected.Location = new System.Drawing.Point(7, 29);
            this.autoEmail_SendWhenDocIsRejected.Name = "autoEmail_SendWhenDocIsRejected";
            this.autoEmail_SendWhenDocIsRejected.Size = new System.Drawing.Size(295, 17);
            this.autoEmail_SendWhenDocIsRejected.TabIndex = 0;
            this.autoEmail_SendWhenDocIsRejected.Text = "Automatically Send email when DOCUMENT is REJECTED";
            this.autoEmail_SendWhenDocIsRejected.UseVisualStyleBackColor = true;
            // 
            // autoEmail_SendWhenDocIsApproved
            // 
            this.autoEmail_SendWhenDocIsApproved.AutoSize = true;
            this.autoEmail_SendWhenDocIsApproved.Location = new System.Drawing.Point(7, 6);
            this.autoEmail_SendWhenDocIsApproved.Name = "autoEmail_SendWhenDocIsApproved";
            this.autoEmail_SendWhenDocIsApproved.Size = new System.Drawing.Size(298, 17);
            this.autoEmail_SendWhenDocIsApproved.TabIndex = 0;
            this.autoEmail_SendWhenDocIsApproved.Text = "Automatically Send email when DOCUMENT is APPROVED";
            this.autoEmail_SendWhenDocIsApproved.UseVisualStyleBackColor = true;
            // 
            // accordionContentContainer23
            // 
            this.accordionContentContainer23.Controls.Add(this.autoEmail_RFI_Enabled);
            this.accordionContentContainer23.Controls.Add(this.autoEmail_RFI_group);
            this.accordionContentContainer23.Name = "accordionContentContainer23";
            this.accordionContentContainer23.Size = new System.Drawing.Size(468, 190);
            this.accordionContentContainer23.TabIndex = 28;
            // 
            // autoEmail_RFI_Enabled
            // 
            this.autoEmail_RFI_Enabled.AutoSize = true;
            this.autoEmail_RFI_Enabled.Location = new System.Drawing.Point(14, 8);
            this.autoEmail_RFI_Enabled.Name = "autoEmail_RFI_Enabled";
            this.autoEmail_RFI_Enabled.Size = new System.Drawing.Size(64, 17);
            this.autoEmail_RFI_Enabled.TabIndex = 1;
            this.autoEmail_RFI_Enabled.Text = "Enabled";
            this.autoEmail_RFI_Enabled.UseVisualStyleBackColor = true;
            this.autoEmail_RFI_Enabled.CheckedChanged += new System.EventHandler(this.autoEmail_RFI_Enabled_CheckedChanged);
            // 
            // autoEmail_RFI_group
            // 
            this.autoEmail_RFI_group.Controls.Add(this.label8);
            this.autoEmail_RFI_group.Controls.Add(this.autoEmail_RFI_TopTextContent);
            this.autoEmail_RFI_group.Controls.Add(this.autoEmail_RFI_AddTopText);
            this.autoEmail_RFI_group.Controls.Add(this.autoEmail_RFI_SendOnlyMonToFri);
            this.autoEmail_RFI_group.Controls.Add(this.label6);
            this.autoEmail_RFI_group.Controls.Add(this.autoEmail_RFI_SendIntervalInDays);
            this.autoEmail_RFI_group.Location = new System.Drawing.Point(9, 8);
            this.autoEmail_RFI_group.Name = "autoEmail_RFI_group";
            this.autoEmail_RFI_group.Size = new System.Drawing.Size(446, 171);
            this.autoEmail_RFI_group.TabIndex = 0;
            this.autoEmail_RFI_group.TabStop = false;
            this.autoEmail_RFI_group.Text = "groupBox1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Send every";
            // 
            // autoEmail_RFI_TopTextContent
            // 
            this.autoEmail_RFI_TopTextContent.Location = new System.Drawing.Point(36, 81);
            this.autoEmail_RFI_TopTextContent.Multiline = true;
            this.autoEmail_RFI_TopTextContent.Name = "autoEmail_RFI_TopTextContent";
            this.autoEmail_RFI_TopTextContent.Size = new System.Drawing.Size(390, 72);
            this.autoEmail_RFI_TopTextContent.TabIndex = 7;
            // 
            // autoEmail_RFI_AddTopText
            // 
            this.autoEmail_RFI_AddTopText.AutoSize = true;
            this.autoEmail_RFI_AddTopText.Location = new System.Drawing.Point(18, 57);
            this.autoEmail_RFI_AddTopText.Name = "autoEmail_RFI_AddTopText";
            this.autoEmail_RFI_AddTopText.Size = new System.Drawing.Size(228, 17);
            this.autoEmail_RFI_AddTopText.TabIndex = 6;
            this.autoEmail_RFI_AddTopText.Text = "Include the following top text in the email:";
            this.autoEmail_RFI_AddTopText.UseVisualStyleBackColor = true;
            this.autoEmail_RFI_AddTopText.CheckedChanged += new System.EventHandler(this.autoEmail_RFI_Enabled_CheckedChanged);
            // 
            // autoEmail_RFI_SendOnlyMonToFri
            // 
            this.autoEmail_RFI_SendOnlyMonToFri.AutoSize = true;
            this.autoEmail_RFI_SendOnlyMonToFri.Location = new System.Drawing.Point(175, 25);
            this.autoEmail_RFI_SendOnlyMonToFri.Name = "autoEmail_RFI_SendOnlyMonToFri";
            this.autoEmail_RFI_SendOnlyMonToFri.Size = new System.Drawing.Size(127, 17);
            this.autoEmail_RFI_SendOnlyMonToFri.TabIndex = 3;
            this.autoEmail_RFI_SendOnlyMonToFri.Text = "only Monday - Friday";
            this.autoEmail_RFI_SendOnlyMonToFri.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(141, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "days";
            // 
            // autoEmail_RFI_SendIntervalInDays
            // 
            this.autoEmail_RFI_SendIntervalInDays.Location = new System.Drawing.Point(83, 23);
            this.autoEmail_RFI_SendIntervalInDays.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.autoEmail_RFI_SendIntervalInDays.Name = "autoEmail_RFI_SendIntervalInDays";
            this.autoEmail_RFI_SendIntervalInDays.Size = new System.Drawing.Size(55, 21);
            this.autoEmail_RFI_SendIntervalInDays.TabIndex = 1;
            this.autoEmail_RFI_SendIntervalInDays.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // accordionContentContainer24
            // 
            this.accordionContentContainer24.Controls.Add(this.autoEmail_Interim_Enabled);
            this.accordionContentContainer24.Controls.Add(this.autoEmail_Interim_Group);
            this.accordionContentContainer24.Name = "accordionContentContainer24";
            this.accordionContentContainer24.Size = new System.Drawing.Size(468, 198);
            this.accordionContentContainer24.TabIndex = 29;
            // 
            // autoEmail_Interim_Enabled
            // 
            this.autoEmail_Interim_Enabled.AutoSize = true;
            this.autoEmail_Interim_Enabled.Location = new System.Drawing.Point(16, 11);
            this.autoEmail_Interim_Enabled.Name = "autoEmail_Interim_Enabled";
            this.autoEmail_Interim_Enabled.Size = new System.Drawing.Size(64, 17);
            this.autoEmail_Interim_Enabled.TabIndex = 3;
            this.autoEmail_Interim_Enabled.Text = "Enabled";
            this.autoEmail_Interim_Enabled.UseVisualStyleBackColor = true;
            this.autoEmail_Interim_Enabled.CheckedChanged += new System.EventHandler(this.autoEmail_RFI_Enabled_CheckedChanged);
            // 
            // autoEmail_Interim_Group
            // 
            this.autoEmail_Interim_Group.Controls.Add(this.label9);
            this.autoEmail_Interim_Group.Controls.Add(this.autoEmail_Interim_TopTextText);
            this.autoEmail_Interim_Group.Controls.Add(this.autoEmail_Interim_AddTopText);
            this.autoEmail_Interim_Group.Controls.Add(this.autoEmail_Interim_MonFriOnly);
            this.autoEmail_Interim_Group.Controls.Add(this.label7);
            this.autoEmail_Interim_Group.Controls.Add(this.autoEmail_Interim_SendIntervalInDays);
            this.autoEmail_Interim_Group.Location = new System.Drawing.Point(11, 11);
            this.autoEmail_Interim_Group.Name = "autoEmail_Interim_Group";
            this.autoEmail_Interim_Group.Size = new System.Drawing.Size(446, 177);
            this.autoEmail_Interim_Group.TabIndex = 4;
            this.autoEmail_Interim_Group.TabStop = false;
            this.autoEmail_Interim_Group.Text = "groupBox2";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(31, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Send every";
            // 
            // autoEmail_Interim_TopTextText
            // 
            this.autoEmail_Interim_TopTextText.Location = new System.Drawing.Point(38, 80);
            this.autoEmail_Interim_TopTextText.Multiline = true;
            this.autoEmail_Interim_TopTextText.Name = "autoEmail_Interim_TopTextText";
            this.autoEmail_Interim_TopTextText.Size = new System.Drawing.Size(390, 72);
            this.autoEmail_Interim_TopTextText.TabIndex = 7;
            // 
            // autoEmail_Interim_AddTopText
            // 
            this.autoEmail_Interim_AddTopText.AutoSize = true;
            this.autoEmail_Interim_AddTopText.Location = new System.Drawing.Point(20, 56);
            this.autoEmail_Interim_AddTopText.Name = "autoEmail_Interim_AddTopText";
            this.autoEmail_Interim_AddTopText.Size = new System.Drawing.Size(228, 17);
            this.autoEmail_Interim_AddTopText.TabIndex = 6;
            this.autoEmail_Interim_AddTopText.Text = "Include the following top text in the email:";
            this.autoEmail_Interim_AddTopText.UseVisualStyleBackColor = true;
            this.autoEmail_Interim_AddTopText.CheckedChanged += new System.EventHandler(this.autoEmail_RFI_Enabled_CheckedChanged);
            // 
            // autoEmail_Interim_MonFriOnly
            // 
            this.autoEmail_Interim_MonFriOnly.AutoSize = true;
            this.autoEmail_Interim_MonFriOnly.Location = new System.Drawing.Point(194, 24);
            this.autoEmail_Interim_MonFriOnly.Name = "autoEmail_Interim_MonFriOnly";
            this.autoEmail_Interim_MonFriOnly.Size = new System.Drawing.Size(127, 17);
            this.autoEmail_Interim_MonFriOnly.TabIndex = 3;
            this.autoEmail_Interim_MonFriOnly.Text = "only Monday - Friday";
            this.autoEmail_Interim_MonFriOnly.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(160, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "days";
            // 
            // autoEmail_Interim_SendIntervalInDays
            // 
            this.autoEmail_Interim_SendIntervalInDays.Location = new System.Drawing.Point(99, 23);
            this.autoEmail_Interim_SendIntervalInDays.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.autoEmail_Interim_SendIntervalInDays.Name = "autoEmail_Interim_SendIntervalInDays";
            this.autoEmail_Interim_SendIntervalInDays.Size = new System.Drawing.Size(55, 21);
            this.autoEmail_Interim_SendIntervalInDays.TabIndex = 1;
            this.autoEmail_Interim_SendIntervalInDays.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // accordionContentContainer2
            // 
            this.accordionContentContainer2.Controls.Add(this.txtFinWorkerEmail);
            this.accordionContentContainer2.Controls.Add(this.label20);
            this.accordionContentContainer2.Controls.Add(this.txtFinWorkerFax);
            this.accordionContentContainer2.Controls.Add(this.label19);
            this.accordionContentContainer2.Controls.Add(this.txtFinWorkerPhone);
            this.accordionContentContainer2.Controls.Add(this.label18);
            this.accordionContentContainer2.Controls.Add(this.txtFinWorkerAddress);
            this.accordionContentContainer2.Controls.Add(this.label10);
            this.accordionContentContainer2.Controls.Add(this.btnViewFin);
            this.accordionContentContainer2.Controls.Add(this.comboFin);
            this.accordionContentContainer2.Controls.Add(this.label11);
            this.accordionContentContainer2.Name = "accordionContentContainer2";
            this.accordionContentContainer2.Size = new System.Drawing.Size(468, 94);
            this.accordionContentContainer2.TabIndex = 38;
            // 
            // txtFinWorkerEmail
            // 
            this.txtFinWorkerEmail.Location = new System.Drawing.Point(59, 62);
            this.txtFinWorkerEmail.Name = "txtFinWorkerEmail";
            this.txtFinWorkerEmail.ReadOnly = true;
            this.txtFinWorkerEmail.Size = new System.Drawing.Size(157, 21);
            this.txtFinWorkerEmail.TabIndex = 4;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(23, 66);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 13);
            this.label20.TabIndex = 13;
            this.label20.Text = "Email:";
            // 
            // txtFinWorkerFax
            // 
            this.txtFinWorkerFax.Location = new System.Drawing.Point(262, 62);
            this.txtFinWorkerFax.Name = "txtFinWorkerFax";
            this.txtFinWorkerFax.ReadOnly = true;
            this.txtFinWorkerFax.Size = new System.Drawing.Size(168, 21);
            this.txtFinWorkerFax.TabIndex = 5;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(233, 65);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 13);
            this.label19.TabIndex = 11;
            this.label19.Text = "Fax:";
            // 
            // txtFinWorkerPhone
            // 
            this.txtFinWorkerPhone.Location = new System.Drawing.Point(262, 35);
            this.txtFinWorkerPhone.Name = "txtFinWorkerPhone";
            this.txtFinWorkerPhone.ReadOnly = true;
            this.txtFinWorkerPhone.Size = new System.Drawing.Size(168, 21);
            this.txtFinWorkerPhone.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(222, 38);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 13);
            this.label18.TabIndex = 9;
            this.label18.Text = "Phone:";
            // 
            // txtFinWorkerAddress
            // 
            this.txtFinWorkerAddress.Location = new System.Drawing.Point(59, 35);
            this.txtFinWorkerAddress.Name = "txtFinWorkerAddress";
            this.txtFinWorkerAddress.ReadOnly = true;
            this.txtFinWorkerAddress.Size = new System.Drawing.Size(157, 21);
            this.txtFinWorkerAddress.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Address:";
            // 
            // btnViewFin
            // 
            this.btnViewFin.Location = new System.Drawing.Point(434, 2);
            this.btnViewFin.Name = "btnViewFin";
            this.btnViewFin.Size = new System.Drawing.Size(28, 23);
            this.btnViewFin.TabIndex = 1;
            this.btnViewFin.Text = "...";
            this.btnViewFin.UseVisualStyleBackColor = true;
            this.btnViewFin.Click += new System.EventHandler(this.btnViewFin_Click);
            // 
            // comboFin
            // 
            this.comboFin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboFin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboFin.FormattingEnabled = true;
            this.comboFin.Location = new System.Drawing.Point(153, 3);
            this.comboFin.Name = "comboFin";
            this.comboFin.Size = new System.Drawing.Size(277, 21);
            this.comboFin.TabIndex = 0;
            this.comboFin.SelectedIndexChanged += new System.EventHandler(this.comboFin_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(57, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Financial Worker:";
            // 
            // accordionContentContainer25
            // 
            this.accordionContentContainer25.Controls.Add(this.txtFinSuperEmail);
            this.accordionContentContainer25.Controls.Add(this.label21);
            this.accordionContentContainer25.Controls.Add(this.txtFinSuperFax);
            this.accordionContentContainer25.Controls.Add(this.label22);
            this.accordionContentContainer25.Controls.Add(this.txtFinSuperPhone);
            this.accordionContentContainer25.Controls.Add(this.label23);
            this.accordionContentContainer25.Controls.Add(this.txtFinSuperAddress);
            this.accordionContentContainer25.Controls.Add(this.label24);
            this.accordionContentContainer25.Controls.Add(this.btnViewFinSuper_);
            this.accordionContentContainer25.Controls.Add(this.comboFinSup);
            this.accordionContentContainer25.Controls.Add(this.label25);
            this.accordionContentContainer25.Name = "accordionContentContainer25";
            this.accordionContentContainer25.Size = new System.Drawing.Size(468, 92);
            this.accordionContentContainer25.TabIndex = 40;
            // 
            // txtFinSuperEmail
            // 
            this.txtFinSuperEmail.Location = new System.Drawing.Point(59, 61);
            this.txtFinSuperEmail.Name = "txtFinSuperEmail";
            this.txtFinSuperEmail.ReadOnly = true;
            this.txtFinSuperEmail.Size = new System.Drawing.Size(157, 21);
            this.txtFinSuperEmail.TabIndex = 18;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(23, 65);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(35, 13);
            this.label21.TabIndex = 24;
            this.label21.Text = "Email:";
            // 
            // txtFinSuperFax
            // 
            this.txtFinSuperFax.Location = new System.Drawing.Point(262, 61);
            this.txtFinSuperFax.Name = "txtFinSuperFax";
            this.txtFinSuperFax.ReadOnly = true;
            this.txtFinSuperFax.Size = new System.Drawing.Size(168, 21);
            this.txtFinSuperFax.TabIndex = 20;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(233, 64);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(29, 13);
            this.label22.TabIndex = 23;
            this.label22.Text = "Fax:";
            // 
            // txtFinSuperPhone
            // 
            this.txtFinSuperPhone.Location = new System.Drawing.Point(262, 34);
            this.txtFinSuperPhone.Name = "txtFinSuperPhone";
            this.txtFinSuperPhone.ReadOnly = true;
            this.txtFinSuperPhone.Size = new System.Drawing.Size(168, 21);
            this.txtFinSuperPhone.TabIndex = 17;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(222, 37);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 13);
            this.label23.TabIndex = 22;
            this.label23.Text = "Phone:";
            // 
            // txtFinSuperAddress
            // 
            this.txtFinSuperAddress.Location = new System.Drawing.Point(59, 34);
            this.txtFinSuperAddress.Name = "txtFinSuperAddress";
            this.txtFinSuperAddress.ReadOnly = true;
            this.txtFinSuperAddress.Size = new System.Drawing.Size(157, 21);
            this.txtFinSuperAddress.TabIndex = 16;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(9, 37);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(50, 13);
            this.label24.TabIndex = 21;
            this.label24.Text = "Address:";
            // 
            // btnViewFinSuper_
            // 
            this.btnViewFinSuper_.Location = new System.Drawing.Point(434, -1);
            this.btnViewFinSuper_.Name = "btnViewFinSuper_";
            this.btnViewFinSuper_.Size = new System.Drawing.Size(28, 23);
            this.btnViewFinSuper_.TabIndex = 15;
            this.btnViewFinSuper_.Text = "...";
            this.btnViewFinSuper_.UseVisualStyleBackColor = true;
            this.btnViewFinSuper_.Click += new System.EventHandler(this.btnViewFinSuper__Click);
            // 
            // comboFinSup
            // 
            this.comboFinSup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboFinSup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboFinSup.FormattingEnabled = true;
            this.comboFinSup.Location = new System.Drawing.Point(153, 0);
            this.comboFinSup.Name = "comboFinSup";
            this.comboFinSup.Size = new System.Drawing.Size(277, 21);
            this.comboFinSup.TabIndex = 14;
            this.comboFinSup.SelectedIndexChanged += new System.EventHandler(this.comboFinSup_SelectedIndexChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(28, 4);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(119, 13);
            this.label25.TabIndex = 19;
            this.label25.Text = "Financial worker\'s supr:";
            // 
            // accordionContentContainer26
            // 
            this.accordionContentContainer26.Controls.Add(this.btnMedApproveDateSendEmail);
            this.accordionContentContainer26.Controls.Add(this.checkMedicalCaseWorker_MedAPprovalDate);
            this.accordionContentContainer26.Controls.Add(this.label29);
            this.accordionContentContainer26.Controls.Add(this.dateMedicalCaseWorker_MedAPprovalDate);
            this.accordionContentContainer26.Controls.Add(this.txtMedEmail);
            this.accordionContentContainer26.Controls.Add(this.lblMedEmail);
            this.accordionContentContainer26.Controls.Add(this.txtMedFax);
            this.accordionContentContainer26.Controls.Add(this.label26);
            this.accordionContentContainer26.Controls.Add(this.txtMedPhone);
            this.accordionContentContainer26.Controls.Add(this.label27);
            this.accordionContentContainer26.Controls.Add(this.txtMedAddress);
            this.accordionContentContainer26.Controls.Add(this.label28);
            this.accordionContentContainer26.Controls.Add(this.btnCiewMed);
            this.accordionContentContainer26.Controls.Add(this.comboMed);
            this.accordionContentContainer26.Controls.Add(this.label14);
            this.accordionContentContainer26.Name = "accordionContentContainer26";
            this.accordionContentContainer26.Size = new System.Drawing.Size(468, 155);
            this.accordionContentContainer26.TabIndex = 41;
            // 
            // btnMedApproveDateSendEmail
            // 
            this.btnMedApproveDateSendEmail.Location = new System.Drawing.Point(223, 117);
            this.btnMedApproveDateSendEmail.Name = "btnMedApproveDateSendEmail";
            this.btnMedApproveDateSendEmail.Size = new System.Drawing.Size(75, 23);
            this.btnMedApproveDateSendEmail.TabIndex = 36;
            this.btnMedApproveDateSendEmail.Text = "Send Email";
            this.btnMedApproveDateSendEmail.UseVisualStyleBackColor = true;
            this.btnMedApproveDateSendEmail.Click += new System.EventHandler(this.btnMedApproveDateSendEmail_Click);
            // 
            // checkMedicalCaseWorker_MedAPprovalDate
            // 
            this.checkMedicalCaseWorker_MedAPprovalDate.AutoSize = true;
            this.checkMedicalCaseWorker_MedAPprovalDate.Location = new System.Drawing.Point(60, 98);
            this.checkMedicalCaseWorker_MedAPprovalDate.Name = "checkMedicalCaseWorker_MedAPprovalDate";
            this.checkMedicalCaseWorker_MedAPprovalDate.Size = new System.Drawing.Size(120, 17);
            this.checkMedicalCaseWorker_MedAPprovalDate.TabIndex = 35;
            this.checkMedicalCaseWorker_MedAPprovalDate.Text = "Med approval date:";
            this.checkMedicalCaseWorker_MedAPprovalDate.UseVisualStyleBackColor = true;
            this.checkMedicalCaseWorker_MedAPprovalDate.CheckedChanged += new System.EventHandler(this.checkMedicalCaseWorker_MedAPprovalDate_CheckedChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(57, 109);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(0, 13);
            this.label29.TabIndex = 34;
            // 
            // dateMedicalCaseWorker_MedAPprovalDate
            // 
            this.dateMedicalCaseWorker_MedAPprovalDate.Enabled = false;
            this.dateMedicalCaseWorker_MedAPprovalDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateMedicalCaseWorker_MedAPprovalDate.Location = new System.Drawing.Point(60, 118);
            this.dateMedicalCaseWorker_MedAPprovalDate.Name = "dateMedicalCaseWorker_MedAPprovalDate";
            this.dateMedicalCaseWorker_MedAPprovalDate.Size = new System.Drawing.Size(157, 21);
            this.dateMedicalCaseWorker_MedAPprovalDate.TabIndex = 33;
            // 
            // txtMedEmail
            // 
            this.txtMedEmail.Location = new System.Drawing.Point(60, 64);
            this.txtMedEmail.Name = "txtMedEmail";
            this.txtMedEmail.ReadOnly = true;
            this.txtMedEmail.Size = new System.Drawing.Size(157, 21);
            this.txtMedEmail.TabIndex = 27;
            // 
            // lblMedEmail
            // 
            this.lblMedEmail.AutoSize = true;
            this.lblMedEmail.Location = new System.Drawing.Point(24, 68);
            this.lblMedEmail.Name = "lblMedEmail";
            this.lblMedEmail.Size = new System.Drawing.Size(35, 13);
            this.lblMedEmail.TabIndex = 32;
            this.lblMedEmail.Text = "Email:";
            // 
            // txtMedFax
            // 
            this.txtMedFax.Location = new System.Drawing.Point(263, 64);
            this.txtMedFax.Name = "txtMedFax";
            this.txtMedFax.ReadOnly = true;
            this.txtMedFax.Size = new System.Drawing.Size(168, 21);
            this.txtMedFax.TabIndex = 28;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(234, 67);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(29, 13);
            this.label26.TabIndex = 31;
            this.label26.Text = "Fax:";
            // 
            // txtMedPhone
            // 
            this.txtMedPhone.Location = new System.Drawing.Point(263, 37);
            this.txtMedPhone.Name = "txtMedPhone";
            this.txtMedPhone.ReadOnly = true;
            this.txtMedPhone.Size = new System.Drawing.Size(168, 21);
            this.txtMedPhone.TabIndex = 26;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(223, 40);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(41, 13);
            this.label27.TabIndex = 30;
            this.label27.Text = "Phone:";
            // 
            // txtMedAddress
            // 
            this.txtMedAddress.Location = new System.Drawing.Point(60, 37);
            this.txtMedAddress.Name = "txtMedAddress";
            this.txtMedAddress.ReadOnly = true;
            this.txtMedAddress.Size = new System.Drawing.Size(157, 21);
            this.txtMedAddress.TabIndex = 25;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(10, 40);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(50, 13);
            this.label28.TabIndex = 29;
            this.label28.Text = "Address:";
            // 
            // btnCiewMed
            // 
            this.btnCiewMed.Location = new System.Drawing.Point(434, 4);
            this.btnCiewMed.Name = "btnCiewMed";
            this.btnCiewMed.Size = new System.Drawing.Size(28, 23);
            this.btnCiewMed.TabIndex = 8;
            this.btnCiewMed.Text = "...";
            this.btnCiewMed.UseVisualStyleBackColor = true;
            this.btnCiewMed.Click += new System.EventHandler(this.btnCiewMed_Click);
            // 
            // comboMed
            // 
            this.comboMed.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboMed.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboMed.FormattingEnabled = true;
            this.comboMed.Location = new System.Drawing.Point(153, 5);
            this.comboMed.Name = "comboMed";
            this.comboMed.Size = new System.Drawing.Size(277, 21);
            this.comboMed.TabIndex = 7;
            this.comboMed.SelectedIndexChanged += new System.EventHandler(this.comboMed_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(41, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(106, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Medical Caseworker:";
            // 
            // accordionContentContainer27
            // 
            this.accordionContentContainer27.Controls.Add(this.autoEmail_LiteRFI_IsEnabled);
            this.accordionContentContainer27.Controls.Add(this.autoEmail_LiteRFI_Groupbox);
            this.accordionContentContainer27.Name = "accordionContentContainer27";
            this.accordionContentContainer27.Size = new System.Drawing.Size(468, 140);
            this.accordionContentContainer27.TabIndex = 43;
            // 
            // autoEmail_LiteRFI_IsEnabled
            // 
            this.autoEmail_LiteRFI_IsEnabled.AutoSize = true;
            this.autoEmail_LiteRFI_IsEnabled.Location = new System.Drawing.Point(12, 4);
            this.autoEmail_LiteRFI_IsEnabled.Name = "autoEmail_LiteRFI_IsEnabled";
            this.autoEmail_LiteRFI_IsEnabled.Size = new System.Drawing.Size(197, 17);
            this.autoEmail_LiteRFI_IsEnabled.TabIndex = 0;
            this.autoEmail_LiteRFI_IsEnabled.Text = "Enabled (add more options. maybe)";
            this.autoEmail_LiteRFI_IsEnabled.UseVisualStyleBackColor = true;
            this.autoEmail_LiteRFI_IsEnabled.Click += new System.EventHandler(this.autoEmail_LiteRFI_IsEnabled_Click);
            // 
            // autoEmail_LiteRFI_Groupbox
            // 
            this.autoEmail_LiteRFI_Groupbox.Controls.Add(this.label31);
            this.autoEmail_LiteRFI_Groupbox.Controls.Add(this.label30);
            this.autoEmail_LiteRFI_Groupbox.Location = new System.Drawing.Point(5, 4);
            this.autoEmail_LiteRFI_Groupbox.Name = "autoEmail_LiteRFI_Groupbox";
            this.autoEmail_LiteRFI_Groupbox.Size = new System.Drawing.Size(457, 133);
            this.autoEmail_LiteRFI_Groupbox.TabIndex = 0;
            this.autoEmail_LiteRFI_Groupbox.TabStop = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(19, 45);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(82, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "Skips weekends";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(19, 23);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(177, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = "Sent RFI Summary on every 4 days";
            // 
            // accordionControlElement3
            // 
            this.accordionControlElement3.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement25,
            this.accordionControlElement26,
            this.accordionControlElement28,
            this.accordionControlElement32});
            this.accordionControlElement3.Expanded = true;
            this.accordionControlElement3.Name = "accordionControlElement3";
            this.accordionControlElement3.Text = "Email sdheduler";
            // 
            // accordionControlElement25
            // 
            this.accordionControlElement25.ContentContainer = this.accordionContentContainer22;
            this.accordionControlElement25.Name = "accordionControlElement25";
            this.accordionControlElement25.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement25.Text = "Automatic email actions";
            // 
            // accordionControlElement26
            // 
            this.accordionControlElement26.ContentContainer = this.accordionContentContainer23;
            this.accordionControlElement26.Name = "accordionControlElement26";
            this.accordionControlElement26.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement26.Text = "Automatic RFI emails";
            // 
            // accordionControlElement28
            // 
            this.accordionControlElement28.ContentContainer = this.accordionContentContainer24;
            this.accordionControlElement28.Name = "accordionControlElement28";
            this.accordionControlElement28.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement28.Text = "Interim status emails";
            // 
            // accordionControlElement32
            // 
            this.accordionControlElement32.ContentContainer = this.accordionContentContainer27;
            this.accordionControlElement32.Expanded = true;
            this.accordionControlElement32.Name = "accordionControlElement32";
            this.accordionControlElement32.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement32.Text = "The Little RFI";
            // 
            // accordionControlElement17
            // 
            this.accordionControlElement17.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement18,
            this.accordionControlElement19,
            this.accordionControlElement20,
            this.accordionControlElement21,
            this.accordionControlElement22,
            this.accordionControlElement23,
            this.accordionControlElement24});
            this.accordionControlElement17.Name = "accordionControlElement17";
            this.accordionControlElement17.Text = "Gravity Form Elements";
            // 
            // accordionControlElement18
            // 
            this.accordionControlElement18.ContentContainer = this.accordionContentContainer15;
            this.accordionControlElement18.Expanded = true;
            this.accordionControlElement18.Name = "accordionControlElement18";
            this.accordionControlElement18.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement18.Text = "Personal Information of applicant";
            // 
            // accordionControlElement19
            // 
            this.accordionControlElement19.ContentContainer = this.accordionContentContainer16;
            this.accordionControlElement19.Name = "accordionControlElement19";
            this.accordionControlElement19.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement19.Text = "Point of contact";
            // 
            // accordionControlElement20
            // 
            this.accordionControlElement20.ContentContainer = this.accordionContentContainer17;
            this.accordionControlElement20.Name = "accordionControlElement20";
            this.accordionControlElement20.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement20.Text = "LTC community";
            // 
            // accordionControlElement21
            // 
            this.accordionControlElement21.ContentContainer = this.accordionContentContainer18;
            this.accordionControlElement21.Name = "accordionControlElement21";
            this.accordionControlElement21.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement21.Text = "Health and Medical Information";
            // 
            // accordionControlElement22
            // 
            this.accordionControlElement22.ContentContainer = this.accordionContentContainer19;
            this.accordionControlElement22.Name = "accordionControlElement22";
            this.accordionControlElement22.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement22.Text = "ALTCS Application Financial Checklist";
            // 
            // accordionControlElement23
            // 
            this.accordionControlElement23.ContentContainer = this.accordionContentContainer20;
            this.accordionControlElement23.Name = "accordionControlElement23";
            this.accordionControlElement23.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement23.Text = "Lookback Period";
            // 
            // accordionControlElement24
            // 
            this.accordionControlElement24.ContentContainer = this.accordionContentContainer21;
            this.accordionControlElement24.Name = "accordionControlElement24";
            this.accordionControlElement24.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement24.Text = "Client needs";
            // 
            // accordionControlElement1
            // 
            this.accordionControlElement1.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement4,
            this.accordionControlElement5,
            this.accordionControlElement6,
            this.accordionControlElement7,
            this.accordionControlElement8});
            this.accordionControlElement1.HeaderTemplate.AddRange(new DevExpress.XtraBars.Navigation.HeaderElementInfo[] {
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.Text),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.Image),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.HeaderControl),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.ContextButtons)});
            this.accordionControlElement1.Name = "accordionControlElement1";
            this.accordionControlElement1.Text = "DE related stuff, can be refactored";
            // 
            // accordionControlElement4
            // 
            this.accordionControlElement4.ContentContainer = this.accordionContentContainer3;
            this.accordionControlElement4.Expanded = true;
            this.accordionControlElement4.Name = "accordionControlElement4";
            this.accordionControlElement4.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement4.Text = "Customer’s Current Living Arrangement";
            // 
            // accordionControlElement5
            // 
            this.accordionControlElement5.ContentContainer = this.accordionContentContainer4;
            this.accordionControlElement5.Name = "accordionControlElement5";
            this.accordionControlElement5.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement5.Text = "Accommodations for Printed Letters";
            // 
            // accordionControlElement6
            // 
            this.accordionControlElement6.ContentContainer = this.accordionContentContainer5;
            this.accordionControlElement6.Name = "accordionControlElement6";
            this.accordionControlElement6.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement6.Text = "Additional Questions";
            this.accordionControlElement6.Click += new System.EventHandler(this.AccordionControlElement6_Click);
            // 
            // accordionControlElement7
            // 
            this.accordionControlElement7.ContentContainer = this.accordionContentContainer6;
            this.accordionControlElement7.Expanded = true;
            this.accordionControlElement7.Name = "accordionControlElement7";
            this.accordionControlElement7.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement7.Text = "Interview Information -  best days and times to complete it";
            // 
            // accordionControlElement8
            // 
            this.accordionControlElement8.ContentContainer = this.accordionContentContainer7;
            this.accordionControlElement8.Name = "accordionControlElement8";
            this.accordionControlElement8.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement8.Text = "Person Completing the Form";
            // 
            // accordionControlElement9
            // 
            this.accordionControlElement9.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement10,
            this.accordionControlElement11,
            this.accordionControlElement12,
            this.accordionControlElement13,
            this.accordionControlElement14,
            this.accordionControlElement15,
            this.accordionControlElement16});
            this.accordionControlElement9.Expanded = true;
            this.accordionControlElement9.Name = "accordionControlElement9";
            this.accordionControlElement9.Text = "The medical file stuff";
            // 
            // accordionControlElement10
            // 
            this.accordionControlElement10.ContentContainer = this.accordionContentContainer8;
            this.accordionControlElement10.Name = "accordionControlElement10";
            this.accordionControlElement10.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement10.Text = "Authorized Parties";
            // 
            // accordionControlElement11
            // 
            this.accordionControlElement11.ContentContainer = this.accordionContentContainer9;
            this.accordionControlElement11.Name = "accordionControlElement11";
            this.accordionControlElement11.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement11.Text = "Disclosed information";
            // 
            // accordionControlElement12
            // 
            this.accordionControlElement12.ContentContainer = this.accordionContentContainer10;
            this.accordionControlElement12.Name = "accordionControlElement12";
            this.accordionControlElement12.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement12.Text = "Purpose of the authorization";
            // 
            // accordionControlElement13
            // 
            this.accordionControlElement13.ContentContainer = this.accordionContentContainer11;
            this.accordionControlElement13.Name = "accordionControlElement13";
            this.accordionControlElement13.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement13.Text = "End of this authorization";
            // 
            // accordionControlElement14
            // 
            this.accordionControlElement14.ContentContainer = this.accordionContentContainer12;
            this.accordionControlElement14.Name = "accordionControlElement14";
            this.accordionControlElement14.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement14.Text = "Patient Rights";
            // 
            // accordionControlElement15
            // 
            this.accordionControlElement15.ContentContainer = this.accordionContentContainer13;
            this.accordionControlElement15.Name = "accordionControlElement15";
            this.accordionControlElement15.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement15.Text = "Additional Consent for Certain Conditions:";
            // 
            // accordionControlElement16
            // 
            this.accordionControlElement16.ContentContainer = this.accordionContentContainer14;
            this.accordionControlElement16.Name = "accordionControlElement16";
            this.accordionControlElement16.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement16.Text = "Additional Consent for HIV/AIDS";
            // 
            // accordionControlElement27
            // 
            this.accordionControlElement27.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement29,
            this.accordionControlElement30,
            this.accordionControlElement31});
            this.accordionControlElement27.Expanded = true;
            this.accordionControlElement27.Name = "accordionControlElement27";
            this.accordionControlElement27.Text = "Workers";
            // 
            // accordionControlElement29
            // 
            this.accordionControlElement29.ContentContainer = this.accordionContentContainer2;
            this.accordionControlElement29.Name = "accordionControlElement29";
            this.accordionControlElement29.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement29.Text = "Financial Case Worker";
            // 
            // accordionControlElement30
            // 
            this.accordionControlElement30.ContentContainer = this.accordionContentContainer25;
            this.accordionControlElement30.Name = "accordionControlElement30";
            this.accordionControlElement30.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement30.Text = "Financial case worker\'s supervisor";
            // 
            // accordionControlElement31
            // 
            this.accordionControlElement31.ContentContainer = this.accordionContentContainer26;
            this.accordionControlElement31.Expanded = true;
            this.accordionControlElement31.Name = "accordionControlElement31";
            this.accordionControlElement31.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement31.Text = "Medical Caseworker";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.progressBar);
            this.panelControl3.Controls.Add(this.labelProgress);
            this.panelControl3.Controls.Add(this.pictureViewer);
            this.panelControl3.Controls.Add(this.richEditControl);
            this.panelControl3.Controls.Add(this.pdfViewer);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(383, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(315, 786);
            this.panelControl3.TabIndex = 4;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.EditValue = "sadasdads";
            this.progressBar.Location = new System.Drawing.Point(6, 763);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(349, 18);
            this.progressBar.TabIndex = 0;
            this.progressBar.Visible = false;
            // 
            // labelProgress
            // 
            this.labelProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelProgress.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelProgress.Appearance.Options.UseFont = true;
            this.labelProgress.Location = new System.Drawing.Point(10, 766);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(90, 14);
            this.labelProgress.TabIndex = 0;
            this.labelProgress.Text = "labelControl78";
            this.labelProgress.Visible = false;
            // 
            // pictureViewer
            // 
            this.pictureViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureViewer.Location = new System.Drawing.Point(2, 2);
            this.pictureViewer.Name = "pictureViewer";
            this.pictureViewer.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureViewer.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureViewer.Size = new System.Drawing.Size(311, 779);
            this.pictureViewer.TabIndex = 3;
            // 
            // richEditControl
            // 
            this.richEditControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richEditControl.Location = new System.Drawing.Point(2, 2);
            this.richEditControl.Name = "richEditControl";
            this.richEditControl.Size = new System.Drawing.Size(311, 782);
            this.richEditControl.TabIndex = 2;
            // 
            // pdfViewer
            // 
            this.pdfViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pdfViewer.Location = new System.Drawing.Point(5, 5);
            this.pdfViewer.Name = "pdfViewer";
            this.pdfViewer.Size = new System.Drawing.Size(304, 776);
            this.pdfViewer.TabIndex = 1;
            // 
            // checkedListBoxControl2
            // 
            this.checkedListBoxControl2.Location = new System.Drawing.Point(5, 23);
            this.checkedListBoxControl2.Name = "checkedListBoxControl2";
            this.checkedListBoxControl2.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.checkedListBoxControl2.Size = new System.Drawing.Size(185, 220);
            this.checkedListBoxControl2.TabIndex = 0;
            // 
            // checkedListBoxControl3
            // 
            this.checkedListBoxControl3.Location = new System.Drawing.Point(5, 23);
            this.checkedListBoxControl3.Name = "checkedListBoxControl3";
            this.checkedListBoxControl3.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.checkedListBoxControl3.Size = new System.Drawing.Size(185, 220);
            this.checkedListBoxControl3.TabIndex = 0;
            // 
            // checkedListBoxControl4
            // 
            this.checkedListBoxControl4.Location = new System.Drawing.Point(5, 23);
            this.checkedListBoxControl4.Name = "checkedListBoxControl4";
            this.checkedListBoxControl4.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.checkedListBoxControl4.Size = new System.Drawing.Size(185, 220);
            this.checkedListBoxControl4.TabIndex = 0;
            // 
            // accordionContentContainer1
            // 
            this.accordionContentContainer1.Name = "accordionContentContainer1";
            this.accordionContentContainer1.Size = new System.Drawing.Size(468, 392);
            this.accordionContentContainer1.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(183, 298);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(27, 13);
            this.label15.TabIndex = 29;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(298, 298);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 13);
            this.label16.TabIndex = 32;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(401, 298);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 13);
            this.label17.TabIndex = 34;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(4, 299);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(136, 13);
            this.label12.TabIndex = 39;
            // 
            // accordionControlElement2
            // 
            this.accordionControlElement2.ContentContainer = this.accordionContentContainer1;
            this.accordionControlElement2.Expanded = true;
            this.accordionControlElement2.Name = "accordionControlElement2";
            this.accordionControlElement2.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement2.Text = "Customer Information";
            // 
            // timerLoad
            // 
            this.timerLoad.Enabled = true;
            this.timerLoad.Tick += new System.EventHandler(this.TimerLoad_Tick);
            // 
            // contextRFI_CategoryClick
            // 
            this.contextRFI_CategoryClick.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addDocumentPlaceholderToolStripMenuItem,
            this.toolStripMenuItem1,
            this.editCategoryToolStripMenuItem,
            this.deleteCategoryToolStripMenuItem});
            this.contextRFI_CategoryClick.Name = "contextRFI_CategoryClick";
            this.contextRFI_CategoryClick.Size = new System.Drawing.Size(220, 76);
            // 
            // addDocumentPlaceholderToolStripMenuItem
            // 
            this.addDocumentPlaceholderToolStripMenuItem.Name = "addDocumentPlaceholderToolStripMenuItem";
            this.addDocumentPlaceholderToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.addDocumentPlaceholderToolStripMenuItem.Text = "Add document placeholder";
            this.addDocumentPlaceholderToolStripMenuItem.Click += new System.EventHandler(this.AddDocumentPlaceholderToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(216, 6);
            // 
            // editCategoryToolStripMenuItem
            // 
            this.editCategoryToolStripMenuItem.Name = "editCategoryToolStripMenuItem";
            this.editCategoryToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.editCategoryToolStripMenuItem.Text = "Edit category";
            this.editCategoryToolStripMenuItem.Click += new System.EventHandler(this.editCategoryToolStripMenuItem_Click);
            // 
            // deleteCategoryToolStripMenuItem
            // 
            this.deleteCategoryToolStripMenuItem.Name = "deleteCategoryToolStripMenuItem";
            this.deleteCategoryToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.deleteCategoryToolStripMenuItem.Text = "Delete category";
            this.deleteCategoryToolStripMenuItem.Click += new System.EventHandler(this.deleteCategoryToolStripMenuItem_Click);
            // 
            // contextRFI_DocumentClick
            // 
            this.contextRFI_DocumentClick.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.renamePlaceholderToolStripMenuItem,
            this.uploadDocumentToolStripMenuItem,
            this.deletePlaceholderToolStripMenuItem,
            this.toolStripMenuItem4,
            this.approveDocumentToolStripMenuItem,
            this.rejectDocumentToolStripMenuItem});
            this.contextRFI_DocumentClick.Name = "contextRFI_DocumentClick";
            this.contextRFI_DocumentClick.Size = new System.Drawing.Size(272, 120);
            this.contextRFI_DocumentClick.Opening += new System.ComponentModel.CancelEventHandler(this.contextRFI_DocumentClick_Opening);
            // 
            // renamePlaceholderToolStripMenuItem
            // 
            this.renamePlaceholderToolStripMenuItem.Name = "renamePlaceholderToolStripMenuItem";
            this.renamePlaceholderToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.renamePlaceholderToolStripMenuItem.Text = "Rename placeholder";
            this.renamePlaceholderToolStripMenuItem.Click += new System.EventHandler(this.renamePlaceholderToolStripMenuItem_Click);
            // 
            // uploadDocumentToolStripMenuItem
            // 
            this.uploadDocumentToolStripMenuItem.Name = "uploadDocumentToolStripMenuItem";
            this.uploadDocumentToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.uploadDocumentToolStripMenuItem.Text = "Upload document to this placeholder";
            this.uploadDocumentToolStripMenuItem.Click += new System.EventHandler(this.uploadDocumentToolStripMenuItem_Click);
            // 
            // deletePlaceholderToolStripMenuItem
            // 
            this.deletePlaceholderToolStripMenuItem.Name = "deletePlaceholderToolStripMenuItem";
            this.deletePlaceholderToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.deletePlaceholderToolStripMenuItem.Text = "Delete placeholder";
            this.deletePlaceholderToolStripMenuItem.Click += new System.EventHandler(this.deletePlaceholderToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(268, 6);
            // 
            // approveDocumentToolStripMenuItem
            // 
            this.approveDocumentToolStripMenuItem.Name = "approveDocumentToolStripMenuItem";
            this.approveDocumentToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.approveDocumentToolStripMenuItem.Text = "Approve document";
            this.approveDocumentToolStripMenuItem.Click += new System.EventHandler(this.approveDocumentToolStripMenuItem_Click);
            // 
            // rejectDocumentToolStripMenuItem
            // 
            this.rejectDocumentToolStripMenuItem.Name = "rejectDocumentToolStripMenuItem";
            this.rejectDocumentToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.rejectDocumentToolStripMenuItem.Text = "Reject document";
            this.rejectDocumentToolStripMenuItem.Click += new System.EventHandler(this.rejectDocumentToolStripMenuItem_Click);
            // 
            // contextTask_Click
            // 
            this.contextTask_Click.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createTaskToolStripMenuItem});
            this.contextTask_Click.Name = "contextTask_Click";
            this.contextTask_Click.Size = new System.Drawing.Size(133, 26);
            // 
            // createTaskToolStripMenuItem
            // 
            this.createTaskToolStripMenuItem.Name = "createTaskToolStripMenuItem";
            this.createTaskToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.createTaskToolStripMenuItem.Text = "Create task";
            // 
            // contextOtherDocument_Click
            // 
            this.contextOtherDocument_Click.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.renameDocumentToolStripMenuItem1,
            this.deleteDocumentToolStripMenuItem1,
            this.viewDocumentToolStripMenuItem1});
            this.contextOtherDocument_Click.Name = "contextOtherDocument_Click";
            this.contextOtherDocument_Click.Size = new System.Drawing.Size(177, 70);
            // 
            // renameDocumentToolStripMenuItem1
            // 
            this.renameDocumentToolStripMenuItem1.Name = "renameDocumentToolStripMenuItem1";
            this.renameDocumentToolStripMenuItem1.Size = new System.Drawing.Size(176, 22);
            this.renameDocumentToolStripMenuItem1.Text = "Rename Document";
            this.renameDocumentToolStripMenuItem1.Click += new System.EventHandler(this.renameDocumentToolStripMenuItem1_Click);
            // 
            // deleteDocumentToolStripMenuItem1
            // 
            this.deleteDocumentToolStripMenuItem1.Name = "deleteDocumentToolStripMenuItem1";
            this.deleteDocumentToolStripMenuItem1.Size = new System.Drawing.Size(176, 22);
            this.deleteDocumentToolStripMenuItem1.Text = "DeleteDocument";
            this.deleteDocumentToolStripMenuItem1.Click += new System.EventHandler(this.deleteDocumentToolStripMenuItem1_Click);
            // 
            // viewDocumentToolStripMenuItem1
            // 
            this.viewDocumentToolStripMenuItem1.Name = "viewDocumentToolStripMenuItem1";
            this.viewDocumentToolStripMenuItem1.Size = new System.Drawing.Size(176, 22);
            this.viewDocumentToolStripMenuItem1.Text = "View Document";
            this.viewDocumentToolStripMenuItem1.Click += new System.EventHandler(this.viewDocumentToolStripMenuItem1_Click);
            // 
            // contextRFI_FileClick
            // 
            this.contextRFI_FileClick.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewDocumentToolStripMenuItem2,
            this.renameDocumentToolStripMenuItem2,
            this.deleteDocumentToolStripMenuItem2,
            this.toolStripMenuItem3,
            this.combineSelectedPDFsToolStripMenuItem});
            this.contextRFI_FileClick.Name = "contextRFI_FileClick";
            this.contextRFI_FileClick.Size = new System.Drawing.Size(200, 98);
            // 
            // viewDocumentToolStripMenuItem2
            // 
            this.viewDocumentToolStripMenuItem2.Name = "viewDocumentToolStripMenuItem2";
            this.viewDocumentToolStripMenuItem2.Size = new System.Drawing.Size(199, 22);
            this.viewDocumentToolStripMenuItem2.Text = "View Document";
            this.viewDocumentToolStripMenuItem2.Click += new System.EventHandler(this.viewDocumentToolStripMenuItem2_Click);
            // 
            // renameDocumentToolStripMenuItem2
            // 
            this.renameDocumentToolStripMenuItem2.Name = "renameDocumentToolStripMenuItem2";
            this.renameDocumentToolStripMenuItem2.Size = new System.Drawing.Size(199, 22);
            this.renameDocumentToolStripMenuItem2.Text = "Rename Document";
            this.renameDocumentToolStripMenuItem2.Click += new System.EventHandler(this.renameDocumentToolStripMenuItem2_Click);
            // 
            // deleteDocumentToolStripMenuItem2
            // 
            this.deleteDocumentToolStripMenuItem2.Name = "deleteDocumentToolStripMenuItem2";
            this.deleteDocumentToolStripMenuItem2.Size = new System.Drawing.Size(199, 22);
            this.deleteDocumentToolStripMenuItem2.Text = "Delete Document";
            this.deleteDocumentToolStripMenuItem2.Click += new System.EventHandler(this.deleteDocumentToolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(196, 6);
            // 
            // combineSelectedPDFsToolStripMenuItem
            // 
            this.combineSelectedPDFsToolStripMenuItem.Name = "combineSelectedPDFsToolStripMenuItem";
            this.combineSelectedPDFsToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.combineSelectedPDFsToolStripMenuItem.Text = "Combine Selected PDFs";
            this.combineSelectedPDFsToolStripMenuItem.Click += new System.EventHandler(this.combineSelectedPDFsToolStripMenuItem_Click);
            // 
            // FormApplicantInfo
            // 
            this.AllowDrop = true;
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1187, 786);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "FormApplicantInfo";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormApplicantInfo_Load);
            this.contextRFI_Click.ResumeLayout(false);
            this.contextNoActions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboDocumentGenerator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl1)).EndInit();
            this.accordionControl1.ResumeLayout(false);
            this.accordionContentContainer8.ResumeLayout(false);
            this.accordionContentContainer8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMedical_Parties)).EndInit();
            this.medicalContextMenu.ResumeLayout(false);
            this.accordionContentContainer9.ResumeLayout(false);
            this.accordionContentContainer9.PerformLayout();
            this.groupMedical_Disclosed_Condition.ResumeLayout(false);
            this.groupMedical_Disclosed_Condition.PerformLayout();
            this.groupMedical_Disclosed_Period.ResumeLayout(false);
            this.groupMedical_Disclosed_Period.PerformLayout();
            this.accordionContentContainer10.ResumeLayout(false);
            this.accordionContentContainer10.PerformLayout();
            this.accordionContentContainer11.ResumeLayout(false);
            this.accordionContentContainer11.PerformLayout();
            this.accordionContentContainer12.ResumeLayout(false);
            this.accordionContentContainer12.PerformLayout();
            this.groupMedical_Rights_UnableToSign.ResumeLayout(false);
            this.groupMedical_Rights_UnableToSign.PerformLayout();
            this.accordionContentContainer13.ResumeLayout(false);
            this.accordionContentContainer13.PerformLayout();
            this.accordionContentContainer14.ResumeLayout(false);
            this.accordionContentContainer14.PerformLayout();
            this.accordionContentContainer3.ResumeLayout(false);
            this.accordionContentContainer3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.accordionContentContainer4.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.AccomodationsForPrintedLetters_Group.ResumeLayout(false);
            this.AccomodationsForPrintedLetters_Group.PerformLayout();
            this.accordionContentContainer5.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.accordionContentContainer6.ResumeLayout(false);
            this.accordionContentContainer6.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.accordionContentContainer7.ResumeLayout(false);
            this.accordionContentContainer7.PerformLayout();
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.accordionContentContainer15.ResumeLayout(false);
            this.accordionContentContainer15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MailingAddressCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MailingAddressZip.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MailingAddressState.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MailingAddressCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MailingAddressLine2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MailingAddressStreet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_Email.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_Phone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_AddressCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_AddressZip.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_AddressState.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_AddressCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_AddressLine2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_AddressStreet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MilitaryToYear.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MilitaryFromYear.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MilitaryBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_IsMilitary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_SpouseSSN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_SpouseLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_SpouseMiddleName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_SpouseFirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_DateOfMarrgiage.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_DateOfMarrgiage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_SpouceDateOfBirth.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_SpouceDateOfBirth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MaritalStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_DateOfBirth.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_DateOfBirth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_Gender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_SSN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_LastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_MiddleName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_FirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_LivingAt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_AlreadyOnAHCCCSProgram.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_IsRegisteredVoter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_Ethnicity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_PlaceOfBirth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicant_IsUSCitizen.Properties)).EndInit();
            this.accordionContentContainer16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.poc_IsPOC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupPOC)).EndInit();
            this.groupPOC.ResumeLayout(false);
            this.groupPOC.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.poc_AddressCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_AddressZip.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_AddressState.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_AddressCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_Email.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_Phone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_AddressLine2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_AddressStreet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_LastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_MiddleName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_FirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.poc_Relationship.Properties)).EndInit();
            this.accordionContentContainer17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ltc_IsLtcAvaliable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupLTC)).EndInit();
            this.groupLTC.ResumeLayout(false);
            this.groupLTC.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_DateEntered.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_DateEntered.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_FacilityType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_AddressCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_FacilityPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_AddressZip.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_AddressState.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_AddressCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_AddressLine2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_AddressStreet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_FacilityPOCLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_CaficlityPocFirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ltc_FacilityName.Properties)).EndInit();
            this.accordionContentContainer18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithAlzheimer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl17)).EndInit();
            this.groupControl17.ResumeLayout(false);
            this.groupControl17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesDDD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithDDD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl16)).EndInit();
            this.groupControl16.ResumeLayout(false);
            this.groupControl16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesOxygen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithOxygen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithDressing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupAlzheimer)).EndInit();
            this.groupAlzheimer.ResumeLayout(false);
            this.groupAlzheimer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_AlzheimerDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_AlzheimerDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_AlzheimerField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_AlzheimerLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_AlzheimerFirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl18)).EndInit();
            this.groupControl18.ResumeLayout(false);
            this.groupControl18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesOther.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithOther.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl14)).EndInit();
            this.groupControl14.ResumeLayout(false);
            this.groupControl14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesParalisys.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithParalysis.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl13)).EndInit();
            this.groupControl13.ResumeLayout(false);
            this.groupControl13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesBehavior.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithDisruptive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithWandering.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithSelfInjury.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithAggression.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl12)).EndInit();
            this.groupControl12.ResumeLayout(false);
            this.groupControl12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesOrientation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithOrientation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).EndInit();
            this.groupControl11.ResumeLayout(false);
            this.groupControl11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsLegallyBlind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesVision.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithVision.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).EndInit();
            this.groupControl10.ResumeLayout(false);
            this.groupControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesIncontinence.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithBowel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithBladder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).EndInit();
            this.groupControl9.ResumeLayout(false);
            this.groupControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesToileting.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithToileting.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            this.groupControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesEating.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithEating.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            this.groupControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesDressing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            this.groupControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesBathing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithBathing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesTransferring.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithTransferring.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_NotesMobility.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helthAndMedical_IsuesWithMobility.Properties)).EndInit();
            this.accordionContentContainer19.ResumeLayout(false);
            this.accordionContentContainer19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.financial_IsOwningHome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl24)).EndInit();
            this.groupControl24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.financial_vehicles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupHomeOwned)).EndInit();
            this.groupHomeOwned.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.financial_IsHomeOtherOccupants.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.financial_IsHomeReverseMortgage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.financial_IsHomeMortgage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.financial_BurialAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.financial_isBural.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl22)).EndInit();
            this.groupControl22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.financial_gridIncome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.financial_PolicyNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.financial_IsOwningPolicy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl21)).EndInit();
            this.groupControl21.ResumeLayout(false);
            this.groupControl21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.financial_LifeCash.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.financial_LifeInstitutionName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl20)).EndInit();
            this.groupControl20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.financial_gridRetirement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl19)).EndInit();
            this.groupControl19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.financial_gridResourceAccounts)).EndInit();
            this.accordionContentContainer20.ResumeLayout(false);
            this.accordionContentContainer20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookback_OtherNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookback_GivenCash.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookback_TransferredFunds.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookback_SoldCarlLast5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookback_SoldHomeLast5.Properties)).EndInit();
            this.accordionContentContainer21.ResumeLayout(false);
            this.accordionContentContainer21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientNeeds_OtherNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientNeeds_RealEstate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientNeeds_DocPrep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientNeeds_Placement.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientNeeds_Veteran.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientNeeds_ALTCS.Properties)).EndInit();
            this.accordionContentContainer22.ResumeLayout(false);
            this.accordionContentContainer22.PerformLayout();
            this.accordionContentContainer23.ResumeLayout(false);
            this.accordionContentContainer23.PerformLayout();
            this.autoEmail_RFI_group.ResumeLayout(false);
            this.autoEmail_RFI_group.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.autoEmail_RFI_SendIntervalInDays)).EndInit();
            this.accordionContentContainer24.ResumeLayout(false);
            this.accordionContentContainer24.PerformLayout();
            this.autoEmail_Interim_Group.ResumeLayout(false);
            this.autoEmail_Interim_Group.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.autoEmail_Interim_SendIntervalInDays)).EndInit();
            this.accordionContentContainer2.ResumeLayout(false);
            this.accordionContentContainer2.PerformLayout();
            this.accordionContentContainer25.ResumeLayout(false);
            this.accordionContentContainer25.PerformLayout();
            this.accordionContentContainer26.ResumeLayout(false);
            this.accordionContentContainer26.PerformLayout();
            this.accordionContentContainer27.ResumeLayout(false);
            this.accordionContentContainer27.PerformLayout();
            this.autoEmail_LiteRFI_Groupbox.ResumeLayout(false);
            this.autoEmail_LiteRFI_Groupbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progressBar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureViewer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxControl4)).EndInit();
            this.contextRFI_CategoryClick.ResumeLayout(false);
            this.contextRFI_DocumentClick.ResumeLayout(false);
            this.contextTask_Click.ResumeLayout(false);
            this.contextOtherDocument_Click.ResumeLayout(false);
            this.contextRFI_FileClick.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.CheckedListBoxControl checkedListBoxControl2;
        private DevExpress.XtraEditors.CheckedListBoxControl checkedListBoxControl3;
        private DevExpress.XtraEditors.CheckedListBoxControl checkedListBoxControl4;
        private DevExpress.XtraBars.Navigation.AccordionControl accordionControl1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement1;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer3;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement4;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox CustomerCurrentLiving_NameOfHospital;
        private System.Windows.Forms.DateTimePicker CustomerCurrentLiving_ExpectedDateOfDischarge;
        private System.Windows.Forms.TextBox CustomerCurrentLiving_HospitalAddress;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox CustomerCurrentLivingOther_Text;
        private System.Windows.Forms.RadioButton CustomerCurrentLiving_Other;
        private System.Windows.Forms.RadioButton CustomerCurrentLiving_Nursing;
        private System.Windows.Forms.RadioButton CustomerCurrentLiving_Home;
        private System.Windows.Forms.RadioButton CustomerCurrentLiving_Hospital;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox CustomerCurrentLiving_City;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox CustomerCurrentLiving_ZipCode;
        private System.Windows.Forms.TextBox CustomerCurrentLiving_PhoneNumber;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox CustomerCurrentLiving_State;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer4;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement5;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton AccomodationsForPrintedLetters_Yes;
        private System.Windows.Forms.RadioButton AccomodationsForPrintedLetters_No;
        private System.Windows.Forms.GroupBox AccomodationsForPrintedLetters_Group;
        private System.Windows.Forms.TextBox AccomodationsForPrintedLetters_OtherText;
        private System.Windows.Forms.CheckBox AccomodationsForPrintedLetters_Other;
        private System.Windows.Forms.CheckBox AccomodationsForPrintedLetters_LargePrint;
        private System.Windows.Forms.CheckBox AccomodationsForPrintedLetters_Email;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox AccomodationsForPrintedLetters_WhoIfYes;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement6;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer5;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.RadioButton AdditionalQuestions_5_Yes;
        private System.Windows.Forms.RadioButton AdditionalQuestions_5_No;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.RadioButton AdditionalQuestions_4_Yes;
        private System.Windows.Forms.RadioButton AdditionalQuestions_4_No;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.CheckBox AdditionalQuestions_3_Seizure;
        private System.Windows.Forms.CheckBox AdditionalQuestions_3_Intellectual;
        private System.Windows.Forms.CheckBox AdditionalQuestions_3_CerebralPalsy;
        private System.Windows.Forms.CheckBox AdditionalQuestions_3_Autism;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.RadioButton AdditionalQuestions_2_Yes;
        private System.Windows.Forms.RadioButton AdditionalQuestions_2_No;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.DateTimePicker AdditionalQuestions_2_Yes_Date;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.RadioButton AdditionalQuestions_1_Yes;
        private System.Windows.Forms.RadioButton AdditionalQuestions_1_No;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox AdditionalQuestions_1_Yes_Month3;
        private System.Windows.Forms.TextBox AdditionalQuestions_1_Yes_Month2;
        private System.Windows.Forms.TextBox AdditionalQuestions_1_Yes_Month1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement7;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer6;
        private System.Windows.Forms.TextBox InterviewInterpretter_MajorCrossroads;
        private System.Windows.Forms.TextBox InterviewInterpretter_HomeAddress;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.RadioButton InterviewInterpretter_Yes;
        private System.Windows.Forms.RadioButton InterviewInterpretter_No;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox InterviewInterpretter_Language;
        private System.Windows.Forms.TextBox Interview_Thursday_Time;
        private System.Windows.Forms.TextBox Interview_Tuesday_Time;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox Interview_Friday_Time;
        private System.Windows.Forms.TextBox Interview_Wednesday_Time;
        private System.Windows.Forms.CheckBox Interview_Thursday;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox Interview_Monday_Time;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.CheckBox Interview_Friday;
        private System.Windows.Forms.CheckBox Interview_Tuesday;
        private System.Windows.Forms.CheckBox Interview_Wednesday;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.CheckBox Interview_Monday;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement8;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer7;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.RadioButton PersonCompleteingTheForm_IsParent;
        private System.Windows.Forms.RadioButton PersonCompleteingTheForm_IsSpouse;
        private System.Windows.Forms.RadioButton PersonCompleteingTheForm_IsCustomer;
        private System.Windows.Forms.TextBox PersonCompleteingTheForm_Phone;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox PersonCompleteingTheForm_Name;
        private System.Windows.Forms.Label label54;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement9;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement10;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer8;
        private System.Windows.Forms.DataGridView gridMedical_Parties;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMedical_Party_Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMedical_Party_DoctorName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMedical_Party_Fax;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMedical_Party_Phone;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMedical_Party_Address;
        private System.Windows.Forms.Button btnMedicalParties_CreatreNewDOctor;
        private System.Windows.Forms.Button btnMedicalParties_AddDoctor;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.ComboBox comboMedical_Parties;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement11;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer9;
        private System.Windows.Forms.CheckBox checkMedical_Disclosed_All;
        private System.Windows.Forms.GroupBox groupMedical_Disclosed_Condition;
        private System.Windows.Forms.TextBox txtMedical_Disclosed_SpecificText;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.CheckBox checkMedical_Disclosed_Specific;
        private System.Windows.Forms.TextBox txtMedical_Disclosed_OtherEmail;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox txtMedical_Disclosed_OtherText;
        private System.Windows.Forms.CheckBox checkMedical_Disclosed_Other;
        private System.Windows.Forms.GroupBox groupMedical_Disclosed_Period;
        private System.Windows.Forms.DateTimePicker dateMedical_Disclosed_PeriodTo;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.DateTimePicker dateMedical_Disclosed_PeriodFrom;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.CheckBox checkMedical_Disclosed_Period;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement12;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer10;
        private System.Windows.Forms.CheckBox checkMedical_Purpose_Sell;
        private System.Windows.Forms.CheckBox checkMedical_Purpose_Comunicate;
        private System.Windows.Forms.TextBox txtMedical_Purpose_OtherText;
        private System.Windows.Forms.CheckBox checkMedical_Purpose_Other;
        private System.Windows.Forms.CheckBox checkMedical_Purpose_MyRequest;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement13;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer11;
        private System.Windows.Forms.DateTimePicker dateMedical_AuthEnd_date;
        private System.Windows.Forms.TextBox txtMedical_AuthEnd_EventText;
        private System.Windows.Forms.CheckBox checkMedical_AuthEnd_Event;
        private System.Windows.Forms.CheckBox checkMedical_AuthEnd_OnDate;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement14;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer12;
        private System.Windows.Forms.TextBox txtMedical_Rights_OtherText;
        private System.Windows.Forms.CheckBox checkMedical_Rights_Other;
        private System.Windows.Forms.CheckBox checkMedical_Rights_CountOrder;
        private System.Windows.Forms.CheckBox checkMedical_Rights_LegalGuardian;
        private System.Windows.Forms.CheckBox checkMedical_Rights_CantSign;
        private System.Windows.Forms.CheckBox checkMedical_Rights_Parent;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.DateTimePicker dateMedical_Rights_AuthorizedRep_SignDate;
        private System.Windows.Forms.GroupBox groupMedical_Rights_UnableToSign;
        private System.Windows.Forms.TextBox txtMedical_Rights_MinorAge;
        private System.Windows.Forms.TextBox txtMedical_Rights_UnableToSignText;
        private System.Windows.Forms.CheckBox checkMedical_Rights_UnableToSign;
        private System.Windows.Forms.CheckBox checkMedical_Rights_SubMinor;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.DateTimePicker dateMedical_Rights_date;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement15;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer13;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.DateTimePicker dateMedical_AddCon_Time;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.DateTimePicker dateMedical_AddCon_Date;
        private System.Windows.Forms.CheckBox checkMedical_AddCon_IDoNot;
        private System.Windows.Forms.CheckBox checkMedical_AddCon_IDo;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement16;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer14;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.DateTimePicker dateMedical_HIV_Time;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.DateTimePicker dateMedical_HIV_date;
        private System.Windows.Forms.CheckBox checkMedical_HIV_IDoNot;
        private System.Windows.Forms.CheckBox checkMedical_HIV_IDo;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer15;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement17;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement18;
        private DevExpress.XtraEditors.CheckEdit applicant_IsUSCitizen;
        private DevExpress.XtraEditors.TextEdit applicant_PlaceOfBirth;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit applicant_Ethnicity;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CheckEdit applicant_AlreadyOnAHCCCSProgram;
        private DevExpress.XtraEditors.CheckEdit applicant_IsRegisteredVoter;
        private DevExpress.XtraEditors.ComboBoxEdit applicant_LivingAt;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer16;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement19;
        private DevExpress.XtraEditors.CheckEdit poc_IsPOC;
        private DevExpress.XtraEditors.GroupControl groupPOC;
        private DevExpress.XtraEditors.TextEdit poc_LastName;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit poc_MiddleName;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit poc_FirstName;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit poc_Relationship;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit poc_AddressStreet;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit poc_AddressLine2;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit poc_Email;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit poc_Phone;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer17;
        private DevExpress.XtraEditors.GroupControl groupLTC;
        private DevExpress.XtraEditors.TextEdit ltc_FacilityPhone;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit ltc_AddressZip;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit ltc_AddressState;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit ltc_AddressCity;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit ltc_AddressLine2;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.TextEdit ltc_AddressStreet;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.TextEdit ltc_FacilityPOCLastName;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit ltc_CaficlityPocFirstName;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.TextEdit ltc_FacilityName;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.CheckEdit ltc_IsLtcAvaliable;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement20;
        private DevExpress.XtraEditors.ComboBoxEdit ltc_AddressCountry;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.ComboBoxEdit poc_AddressCountry;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit poc_AddressZip;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit poc_AddressState;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit poc_AddressCity;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.ComboBoxEdit ltc_FacilityType;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.DateEdit ltc_DateEntered;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer18;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement21;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithMobility;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.MemoEdit helthAndMedical_NotesMobility;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.MemoEdit helthAndMedical_NotesBathing;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithBathing;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.MemoEdit helthAndMedical_NotesTransferring;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithTransferring;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraEditors.MemoEdit helthAndMedical_NotesDressing;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithDressing;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private DevExpress.XtraEditors.MemoEdit helthAndMedical_NotesEating;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithEating;
        private DevExpress.XtraEditors.GroupControl groupControl9;
        private DevExpress.XtraEditors.MemoEdit helthAndMedical_NotesToileting;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithToileting;
        private DevExpress.XtraEditors.GroupControl groupControl10;
        private DevExpress.XtraEditors.MemoEdit helthAndMedical_NotesIncontinence;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithBowel;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithBladder;
        private DevExpress.XtraEditors.GroupControl groupControl11;
        private DevExpress.XtraEditors.MemoEdit helthAndMedical_NotesVision;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithVision;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsLegallyBlind;
        private DevExpress.XtraEditors.GroupControl groupControl12;
        private DevExpress.XtraEditors.MemoEdit helthAndMedical_NotesOrientation;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithOrientation;
        private DevExpress.XtraEditors.GroupControl groupControl13;
        private DevExpress.XtraEditors.MemoEdit helthAndMedical_NotesBehavior;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithSelfInjury;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithAggression;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithDisruptive;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithWandering;
        private DevExpress.XtraEditors.GroupControl groupControl14;
        private DevExpress.XtraEditors.MemoEdit helthAndMedical_NotesParalisys;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithParalysis;
        private DevExpress.XtraEditors.GroupControl groupAlzheimer;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithAlzheimer;
        private DevExpress.XtraEditors.TextEdit helthAndMedical_AlzheimerLastName;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.TextEdit helthAndMedical_AlzheimerFirstName;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.ComboBoxEdit helthAndMedical_AlzheimerField;
        private System.Windows.Forms.Label label63;
        private DevExpress.XtraEditors.DateEdit helthAndMedical_AlzheimerDate;
        private System.Windows.Forms.Label label66;
        private DevExpress.XtraEditors.GroupControl groupControl16;
        private DevExpress.XtraEditors.MemoEdit helthAndMedical_NotesOxygen;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithOxygen;
        private DevExpress.XtraEditors.GroupControl groupControl17;
        private DevExpress.XtraEditors.MemoEdit helthAndMedical_NotesDDD;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithDDD;
        private DevExpress.XtraEditors.GroupControl groupControl18;
        private DevExpress.XtraEditors.MemoEdit helthAndMedical_NotesOther;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.CheckEdit helthAndMedical_IsuesWithOther;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer19;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement22;
        private DevExpress.XtraEditors.GroupControl groupControl19;
        private System.Windows.Forms.DataGridView financial_gridResourceAccounts;
        private DevExpress.XtraEditors.GroupControl groupControl20;
        private System.Windows.Forms.DataGridView financial_gridRetirement;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColRetirement_Ret;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRetirement_Institution;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRetirement_Balance;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColResource_BankAccType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColResource_BankAccInstitution;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColResource_BankAccBallance;
        private DevExpress.XtraEditors.GroupControl groupControl21;
        private DevExpress.XtraEditors.TextEdit financial_LifeCash;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.TextEdit financial_LifeInstitutionName;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.MemoEdit financial_PolicyNotes;
        private DevExpress.XtraEditors.CheckEdit financial_IsOwningPolicy;
        private DevExpress.XtraEditors.GroupControl groupControl22;
        private System.Windows.Forms.DataGridView financial_gridIncome;
        private DevExpress.XtraEditors.TextEdit financial_BurialAmount;
        private System.Windows.Forms.Label label67;
        private DevExpress.XtraEditors.CheckEdit financial_isBural;
        private DevExpress.XtraEditors.GroupControl groupHomeOwned;
        private DevExpress.XtraEditors.CheckEdit financial_IsOwningHome;
        private DevExpress.XtraEditors.CheckEdit financial_IsHomeOtherOccupants;
        private DevExpress.XtraEditors.CheckEdit financial_IsHomeReverseMortgage;
        private DevExpress.XtraEditors.CheckEdit financial_IsHomeMortgage;
        private DevExpress.XtraEditors.GroupControl groupControl24;
        private System.Windows.Forms.DataGridView financial_vehicles;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColVehicle_Make;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColVehicle_Model;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColVehicle_Year;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColVehicle_Milage;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColIncome_Source;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColSource_Amount;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer20;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement23;
        private DevExpress.XtraEditors.MemoEdit lookback_OtherNotes;
        private System.Windows.Forms.Label label79;
        private DevExpress.XtraEditors.CheckEdit lookback_GivenCash;
        private DevExpress.XtraEditors.CheckEdit lookback_TransferredFunds;
        private DevExpress.XtraEditors.CheckEdit lookback_SoldCarlLast5;
        private DevExpress.XtraEditors.CheckEdit lookback_SoldHomeLast5;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer21;
        private DevExpress.XtraEditors.MemoEdit clientNeeds_OtherNotes;
        private System.Windows.Forms.Label label80;
        private DevExpress.XtraEditors.CheckEdit clientNeeds_RealEstate;
        private DevExpress.XtraEditors.CheckEdit clientNeeds_DocPrep;
        private DevExpress.XtraEditors.CheckEdit clientNeeds_Placement;
        private DevExpress.XtraEditors.CheckEdit clientNeeds_Veteran;
        private DevExpress.XtraEditors.CheckEdit clientNeeds_ALTCS;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement24;
        private DevExpress.XtraEditors.TextEdit applicant_LastName;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.TextEdit applicant_MiddleName;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.TextEdit applicant_FirstName;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.DateEdit applicant_DateOfBirth;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label81;
        private DevExpress.XtraEditors.RadioGroup applicant_Gender;
        private DevExpress.XtraEditors.ComboBoxEdit applicant_MaritalStatus;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.DateEdit applicant_SpouceDateOfBirth;
        private System.Windows.Forms.Label label83;
        private DevExpress.XtraEditors.DateEdit applicant_DateOfMarrgiage;
        private System.Windows.Forms.Label label84;
        private DevExpress.XtraEditors.TextEdit applicant_SpouseLastName;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.TextEdit applicant_SpouseMiddleName;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.TextEdit applicant_SpouseFirstName;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private DevExpress.XtraEditors.CheckEdit applicant_IsMilitary;
        private DevExpress.XtraEditors.LabelControl labelControl56;
        private DevExpress.XtraEditors.ComboBoxEdit applicant_MilitaryBranch;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private DevExpress.XtraEditors.TextEdit applicant_MilitaryToYear;
        private DevExpress.XtraEditors.TextEdit applicant_MilitaryFromYear;
        private DevExpress.XtraEditors.LabelControl labelControl58;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.TextEdit applicant_Phone;
        private DevExpress.XtraEditors.LabelControl labelControl66;
        private DevExpress.XtraEditors.ComboBoxEdit applicant_AddressCountry;
        private DevExpress.XtraEditors.LabelControl labelControl59;
        private DevExpress.XtraEditors.TextEdit applicant_AddressZip;
        private DevExpress.XtraEditors.LabelControl labelControl60;
        private DevExpress.XtraEditors.TextEdit applicant_AddressState;
        private DevExpress.XtraEditors.LabelControl labelControl61;
        private DevExpress.XtraEditors.TextEdit applicant_AddressCity;
        private DevExpress.XtraEditors.LabelControl labelControl62;
        private DevExpress.XtraEditors.TextEdit applicant_AddressLine2;
        private DevExpress.XtraEditors.LabelControl labelControl63;
        private DevExpress.XtraEditors.LabelControl labelControl64;
        private DevExpress.XtraEditors.TextEdit applicant_AddressStreet;
        private DevExpress.XtraEditors.LabelControl labelControl65;
        private DevExpress.XtraEditors.LabelControl labelControl76;
        private DevExpress.XtraEditors.ComboBoxEdit applicant_MailingAddressCountry;
        private DevExpress.XtraEditors.LabelControl labelControl69;
        private DevExpress.XtraEditors.TextEdit applicant_MailingAddressZip;
        private DevExpress.XtraEditors.LabelControl labelControl70;
        private DevExpress.XtraEditors.TextEdit applicant_MailingAddressState;
        private DevExpress.XtraEditors.LabelControl labelControl71;
        private DevExpress.XtraEditors.TextEdit applicant_MailingAddressCity;
        private DevExpress.XtraEditors.LabelControl labelControl72;
        private DevExpress.XtraEditors.TextEdit applicant_MailingAddressLine2;
        private DevExpress.XtraEditors.LabelControl labelControl73;
        private DevExpress.XtraEditors.LabelControl labelControl74;
        private DevExpress.XtraEditors.TextEdit applicant_MailingAddressStreet;
        private DevExpress.XtraEditors.LabelControl labelControl75;
        private DevExpress.XtraEditors.TextEdit applicant_Email;
        private DevExpress.XtraEditors.LabelControl labelControl67;
        private DevExpress.XtraEditors.TextEdit applicant_SpouseSSN;
        private DevExpress.XtraEditors.LabelControl labelControl68;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer1;
        private System.Windows.Forms.Label label15;        
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;        
        private System.Windows.Forms.Label label12;        
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement2;
        private DevExpress.XtraEditors.LabelControl labelControl77;
        private DevExpress.XtraEditors.TextEdit applicant_SSN;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.MarqueeProgressBarControl progressBar;
        private DevExpress.XtraEditors.LabelControl labelProgress;
        private DevExpress.XtraEditors.SimpleButton btnDeleteDocuments;        
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timerLoad;
        private DevExpress.XtraEditors.SimpleButton btnGenerate;
        private DevExpress.XtraEditors.ComboBoxEdit comboDocumentGenerator;
        private DevExpress.XtraEditors.SimpleButton btnDownloadDocuments;
        private System.Windows.Forms.ComboBox comboFinancialRepresentative;
        private System.Windows.Forms.Label label64;
        private DevExpress.XtraEditors.SimpleButton btnAddNewRepresentative;
        private DevExpress.XtraEditors.SimpleButton btnEditRepresentative;
        private PdfiumViewer.PdfViewer pdfViewer;
        private DevExpress.XtraRichEdit.RichEditControl richEditControl;
        private DevExpress.XtraEditors.PictureEdit pictureViewer;
        private System.Windows.Forms.ContextMenuStrip medicalContextMenu;
        private System.Windows.Forms.ToolStripMenuItem medicalContextRemotePhysicians;
        private System.Windows.Forms.TreeView treeDocsAndTasks;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textDocumentNotes;
        private System.Windows.Forms.ContextMenuStrip contextRFI_Click;
        private System.Windows.Forms.ToolStripMenuItem newRFICategoryToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextRFI_CategoryClick;
        private System.Windows.Forms.ToolStripMenuItem addDocumentPlaceholderToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem editCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem manageRFICategoriesToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextRFI_DocumentClick;
        private System.Windows.Forms.ToolStripMenuItem uploadDocumentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deletePlaceholderToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem rejectDocumentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem approveDocumentToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextNoActions;
        private System.Windows.Forms.ToolStripMenuItem noActionsHereToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextTask_Click;
        private System.Windows.Forms.ToolStripMenuItem createTaskToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextOtherDocument_Click;
        private System.Windows.Forms.ToolStripMenuItem renameDocumentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deleteDocumentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem viewDocumentToolStripMenuItem1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboFaxTypes;
        private System.Windows.Forms.ComboBox comboEmailTypes;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSendFaxType;
        private System.Windows.Forms.Button btnSendEmailType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textRejectReason;
        private DevExpress.XtraEditors.SimpleButton btnSaveNotes;
        private DevExpress.XtraEditors.SimpleButton btnSaveRejectReason;
        private System.Windows.Forms.ToolStripMenuItem renamePlaceholderToolStripMenuItem;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer22;
        private System.Windows.Forms.CheckBox autoEmail_SendWhenDocIsApproved;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement3;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement25;
        private System.Windows.Forms.CheckBox autoEmail_SendWhenStatusIsChanged;
        private System.Windows.Forms.CheckBox autoEmail_SendWhenDocIsRejected;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer23;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement26;
        private System.Windows.Forms.CheckBox autoEmail_RFI_Enabled;
        private System.Windows.Forms.GroupBox autoEmail_RFI_group;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown autoEmail_RFI_SendIntervalInDays;
        private System.Windows.Forms.CheckBox autoEmail_RFI_SendOnlyMonToFri;
        private System.Windows.Forms.TextBox autoEmail_RFI_TopTextContent;
        private System.Windows.Forms.CheckBox autoEmail_RFI_AddTopText;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer24;
        private System.Windows.Forms.CheckBox autoEmail_Interim_Enabled;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement28;
        private System.Windows.Forms.GroupBox autoEmail_Interim_Group;
        private System.Windows.Forms.TextBox autoEmail_Interim_TopTextText;
        private System.Windows.Forms.CheckBox autoEmail_Interim_AddTopText;
        private System.Windows.Forms.CheckBox autoEmail_Interim_MonFriOnly;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown autoEmail_Interim_SendIntervalInDays;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.ContextMenuStrip contextRFI_FileClick;
        private System.Windows.Forms.ToolStripMenuItem viewDocumentToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem renameDocumentToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem deleteDocumentToolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem combineSelectedPDFsToolStripMenuItem;
        private System.Windows.Forms.Button btnStamp;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton isPregnant_Yes;
        private System.Windows.Forms.RadioButton isPregnant_No;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer2;
        private System.Windows.Forms.Button btnViewFin;
        private System.Windows.Forms.ComboBox comboFin;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement27;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement29;
        private System.Windows.Forms.TextBox txtFinWorkerAddress;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtFinWorkerPhone;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtFinWorkerFax;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtFinWorkerEmail;
        private System.Windows.Forms.Label label20;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer25;
        private System.Windows.Forms.TextBox txtFinSuperEmail;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtFinSuperFax;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtFinSuperPhone;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtFinSuperAddress;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button btnViewFinSuper_;
        private System.Windows.Forms.ComboBox comboFinSup;
        private System.Windows.Forms.Label label25;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement30;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer26;
        private System.Windows.Forms.TextBox txtMedEmail;
        private System.Windows.Forms.Label lblMedEmail;
        private System.Windows.Forms.TextBox txtMedFax;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtMedPhone;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtMedAddress;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button btnCiewMed;
        private System.Windows.Forms.ComboBox comboMed;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement31;
        private DevExpress.XtraEditors.SimpleButton btnAddNewMedicalRepresentative;
        private DevExpress.XtraEditors.SimpleButton btnEditSelectedMedicalRepresentative;
        private System.Windows.Forms.ComboBox comboMedicalRepresentative;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.DateTimePicker dateMedicalCaseWorker_MedAPprovalDate;
        private System.Windows.Forms.CheckBox checkMedicalCaseWorker_MedAPprovalDate;
        private System.Windows.Forms.Button btnMedApproveDateSendEmail;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer27;
        private System.Windows.Forms.CheckBox autoEmail_LiteRFI_IsEnabled;
        private System.Windows.Forms.GroupBox autoEmail_LiteRFI_Groupbox;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
    }
}