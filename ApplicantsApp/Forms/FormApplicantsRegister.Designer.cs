﻿namespace ApplicantsApp
{
    partial class FormApplicantsRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormApplicantsRegister));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression1 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression2 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression3 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression4 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            this.colIsExceptionMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPasapproved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.changeStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incomeOnlyTrustToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.noneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unsentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.receivedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editTransferToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeDateOfTransferToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeDueDateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.caseWorkerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeFInancialInterviewDateToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.changeFinancialCaseWorkerNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeFinancialCaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.medicalCaseWorkerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.phoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eMailToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.changeBilledToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeCSRAAmountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeApproxRestartDateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeMovedDateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.handleExceptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePASApprovedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnExit = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.progressBarText = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applicantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.sendEmailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendFaxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.auditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.representativesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolEditApplicant = new System.Windows.Forms.ToolStripButton();
            this.stripButtonFinancialReview = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolDeleteApplicant = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolAudit = new System.Windows.Forms.ToolStripButton();
            this.toolRep = new System.Windows.Forms.ToolStripButton();
            this.toolDoctorsRegister = new System.Windows.Forms.ToolStripButton();
            this.toolStripCategoriesRegister = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripStripeRegister = new System.Windows.Forms.ToolStripButton();
            this.toolStripStripeInvoicesRegister = new System.Windows.Forms.ToolStripButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.toolStripRFI = new System.Windows.Forms.ToolStripButton();
            this.btnManualRefresh = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calendarColumn1 = new ApplicantsApp.Source.Components.CalendarColumn();
            this.gridApplicants = new DevExpress.XtraGrid.GridControl();
            this.entityServerModeSource1 = new DevExpress.Data.Linq.EntityServerModeSource();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransferSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colincomeOnlyTrust = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colrepFirstName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransferDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldocsCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusDaysAge = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coluploadedAndReviewIsPendingDocumentsCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colapprovedDocumentsCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colrejectedDocumentsCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcreatedOn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colupdatedOn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colupdatedFromUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMoveDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinancialInterviewDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinancialInfo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMedicalInfo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExceptionMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstatusName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmasterStatusName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBilled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCSRAAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApproxRestartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCSRASubmitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPASSubmitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRFISubmitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.contextMenuStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridApplicants)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.entityServerModeSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // colIsExceptionMessage
            // 
            this.colIsExceptionMessage.FieldName = "IsExceptionMessage";
            this.colIsExceptionMessage.MaxWidth = 20;
            this.colIsExceptionMessage.Name = "colIsExceptionMessage";
            this.colIsExceptionMessage.Visible = true;
            this.colIsExceptionMessage.VisibleIndex = 3;
            this.colIsExceptionMessage.Width = 20;
            // 
            // colPasapproved
            // 
            this.colPasapproved.Caption = "PAS";
            this.colPasapproved.FieldName = "Pasapproved";
            this.colPasapproved.MaxWidth = 27;
            this.colPasapproved.MinWidth = 27;
            this.colPasapproved.Name = "colPasapproved";
            this.colPasapproved.Visible = true;
            this.colPasapproved.VisibleIndex = 12;
            this.colPasapproved.Width = 27;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeStatusToolStripMenuItem,
            this.incomeOnlyTrustToolStripMenuItem,
            this.editTransferToolStripMenuItem,
            this.changeDateOfTransferToolStripMenuItem,
            this.changeDueDateToolStripMenuItem,
            this.caseWorkerToolStripMenuItem,
            this.medicalCaseWorkerToolStripMenuItem,
            this.changeBilledToolStripMenuItem,
            this.changeCSRAAmountToolStripMenuItem,
            this.changeApproxRestartDateToolStripMenuItem,
            this.changeMovedDateToolStripMenuItem,
            this.handleExceptionToolStripMenuItem,
            this.changePASApprovedToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(233, 290);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // changeStatusToolStripMenuItem
            // 
            this.changeStatusToolStripMenuItem.Name = "changeStatusToolStripMenuItem";
            this.changeStatusToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.changeStatusToolStripMenuItem.Text = "Change status";
            // 
            // incomeOnlyTrustToolStripMenuItem
            // 
            this.incomeOnlyTrustToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.noneToolStripMenuItem,
            this.unsentToolStripMenuItem,
            this.sentToolStripMenuItem,
            this.receivedToolStripMenuItem});
            this.incomeOnlyTrustToolStripMenuItem.Name = "incomeOnlyTrustToolStripMenuItem";
            this.incomeOnlyTrustToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.incomeOnlyTrustToolStripMenuItem.Text = "Income Only Trust";
            // 
            // noneToolStripMenuItem
            // 
            this.noneToolStripMenuItem.Name = "noneToolStripMenuItem";
            this.noneToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.noneToolStripMenuItem.Tag = "0";
            this.noneToolStripMenuItem.Text = "none";
            this.noneToolStripMenuItem.Click += new System.EventHandler(this.noneToolStripMenuItem_Click_1);
            // 
            // unsentToolStripMenuItem
            // 
            this.unsentToolStripMenuItem.Name = "unsentToolStripMenuItem";
            this.unsentToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.unsentToolStripMenuItem.Tag = "1";
            this.unsentToolStripMenuItem.Text = "Unsent";
            this.unsentToolStripMenuItem.Click += new System.EventHandler(this.noneToolStripMenuItem_Click_1);
            // 
            // sentToolStripMenuItem
            // 
            this.sentToolStripMenuItem.Name = "sentToolStripMenuItem";
            this.sentToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.sentToolStripMenuItem.Tag = "2";
            this.sentToolStripMenuItem.Text = "Sent";
            this.sentToolStripMenuItem.Click += new System.EventHandler(this.noneToolStripMenuItem_Click_1);
            // 
            // receivedToolStripMenuItem
            // 
            this.receivedToolStripMenuItem.Name = "receivedToolStripMenuItem";
            this.receivedToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.receivedToolStripMenuItem.Tag = "3";
            this.receivedToolStripMenuItem.Text = "Received";
            this.receivedToolStripMenuItem.Click += new System.EventHandler(this.noneToolStripMenuItem_Click_1);
            // 
            // editTransferToolStripMenuItem
            // 
            this.editTransferToolStripMenuItem.Name = "editTransferToolStripMenuItem";
            this.editTransferToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.editTransferToolStripMenuItem.Text = "Edit Transfer...";
            this.editTransferToolStripMenuItem.Click += new System.EventHandler(this.editTransferToolStripMenuItem_Click);
            // 
            // changeDateOfTransferToolStripMenuItem
            // 
            this.changeDateOfTransferToolStripMenuItem.Name = "changeDateOfTransferToolStripMenuItem";
            this.changeDateOfTransferToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.changeDateOfTransferToolStripMenuItem.Text = "Change Date of Transfer...";
            this.changeDateOfTransferToolStripMenuItem.Click += new System.EventHandler(this.changeDateOfTransferToolStripMenuItem_Click);
            // 
            // changeDueDateToolStripMenuItem
            // 
            this.changeDueDateToolStripMenuItem.Name = "changeDueDateToolStripMenuItem";
            this.changeDueDateToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.changeDueDateToolStripMenuItem.Text = "Change Due date...";
            this.changeDueDateToolStripMenuItem.Click += new System.EventHandler(this.changeDueDateToolStripMenuItem_Click);
            // 
            // caseWorkerToolStripMenuItem
            // 
            this.caseWorkerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeFInancialInterviewDateToolStripMenuItem1,
            this.toolStripMenuItem1,
            this.changeFinancialCaseWorkerNameToolStripMenuItem,
            this.changeFinancialCaseToolStripMenuItem,
            this.emailToolStripMenuItem});
            this.caseWorkerToolStripMenuItem.Name = "caseWorkerToolStripMenuItem";
            this.caseWorkerToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.caseWorkerToolStripMenuItem.Text = "Financial Case Worker";
            // 
            // changeFInancialInterviewDateToolStripMenuItem1
            // 
            this.changeFInancialInterviewDateToolStripMenuItem1.Name = "changeFInancialInterviewDateToolStripMenuItem1";
            this.changeFInancialInterviewDateToolStripMenuItem1.Size = new System.Drawing.Size(164, 22);
            this.changeFInancialInterviewDateToolStripMenuItem1.Text = "Interview Date...";
            this.changeFInancialInterviewDateToolStripMenuItem1.Click += new System.EventHandler(this.changeFInancialInterviewDateToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(161, 6);
            // 
            // changeFinancialCaseWorkerNameToolStripMenuItem
            // 
            this.changeFinancialCaseWorkerNameToolStripMenuItem.Name = "changeFinancialCaseWorkerNameToolStripMenuItem";
            this.changeFinancialCaseWorkerNameToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.changeFinancialCaseWorkerNameToolStripMenuItem.Text = "Name...";
            // 
            // changeFinancialCaseToolStripMenuItem
            // 
            this.changeFinancialCaseToolStripMenuItem.Name = "changeFinancialCaseToolStripMenuItem";
            this.changeFinancialCaseToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.changeFinancialCaseToolStripMenuItem.Text = "Phone Number...";
            // 
            // emailToolStripMenuItem
            // 
            this.emailToolStripMenuItem.Name = "emailToolStripMenuItem";
            this.emailToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.emailToolStripMenuItem.Text = "Email...";
            // 
            // medicalCaseWorkerToolStripMenuItem
            // 
            this.medicalCaseWorkerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nameToolStripMenuItem,
            this.phoneToolStripMenuItem,
            this.eMailToolStripMenuItem1});
            this.medicalCaseWorkerToolStripMenuItem.Name = "medicalCaseWorkerToolStripMenuItem";
            this.medicalCaseWorkerToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.medicalCaseWorkerToolStripMenuItem.Text = "Medical Case Worker";
            // 
            // nameToolStripMenuItem
            // 
            this.nameToolStripMenuItem.Name = "nameToolStripMenuItem";
            this.nameToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.nameToolStripMenuItem.Text = "Name...";
            // 
            // phoneToolStripMenuItem
            // 
            this.phoneToolStripMenuItem.Name = "phoneToolStripMenuItem";
            this.phoneToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.phoneToolStripMenuItem.Text = "Phone Number...";
            // 
            // eMailToolStripMenuItem1
            // 
            this.eMailToolStripMenuItem1.Name = "eMailToolStripMenuItem1";
            this.eMailToolStripMenuItem1.Size = new System.Drawing.Size(164, 22);
            this.eMailToolStripMenuItem1.Text = "eMail...";
            // 
            // changeBilledToolStripMenuItem
            // 
            this.changeBilledToolStripMenuItem.Name = "changeBilledToolStripMenuItem";
            this.changeBilledToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.changeBilledToolStripMenuItem.Text = "Change Billed...";
            this.changeBilledToolStripMenuItem.Click += new System.EventHandler(this.changeBilledToolStripMenuItem_Click);
            // 
            // changeCSRAAmountToolStripMenuItem
            // 
            this.changeCSRAAmountToolStripMenuItem.Name = "changeCSRAAmountToolStripMenuItem";
            this.changeCSRAAmountToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.changeCSRAAmountToolStripMenuItem.Text = "Change CSRA Amount...";
            this.changeCSRAAmountToolStripMenuItem.Click += new System.EventHandler(this.changeCSRAAmountToolStripMenuItem_Click);
            // 
            // changeApproxRestartDateToolStripMenuItem
            // 
            this.changeApproxRestartDateToolStripMenuItem.Name = "changeApproxRestartDateToolStripMenuItem";
            this.changeApproxRestartDateToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.changeApproxRestartDateToolStripMenuItem.Text = "Change Approx Restart Date...";
            this.changeApproxRestartDateToolStripMenuItem.Click += new System.EventHandler(this.changeApproxRestartDateToolStripMenuItem_Click);
            // 
            // changeMovedDateToolStripMenuItem
            // 
            this.changeMovedDateToolStripMenuItem.Name = "changeMovedDateToolStripMenuItem";
            this.changeMovedDateToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.changeMovedDateToolStripMenuItem.Text = "Change Moved Date...";
            this.changeMovedDateToolStripMenuItem.Click += new System.EventHandler(this.changeMovedDateToolStripMenuItem_Click);
            // 
            // handleExceptionToolStripMenuItem
            // 
            this.handleExceptionToolStripMenuItem.Name = "handleExceptionToolStripMenuItem";
            this.handleExceptionToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.handleExceptionToolStripMenuItem.Text = "Set Exception...";
            this.handleExceptionToolStripMenuItem.Click += new System.EventHandler(this.handleExceptionToolStripMenuItem_Click);
            // 
            // changePASApprovedToolStripMenuItem
            // 
            this.changePASApprovedToolStripMenuItem.Name = "changePASApprovedToolStripMenuItem";
            this.changePASApprovedToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.changePASApprovedToolStripMenuItem.Text = "Change PAS Approved...";
            this.changePASApprovedToolStripMenuItem.Click += new System.EventHandler(this.changePASApprovedToolStripMenuItem_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Location = new System.Drawing.Point(1296, 761);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 1;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(12, 761);
            this.progressBar.MarqueeAnimationSpeed = 30;
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(956, 23);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.TabIndex = 3;
            this.progressBar.Visible = false;
            // 
            // progressBarText
            // 
            this.progressBarText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarText.AutoSize = true;
            this.progressBarText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.progressBarText.Location = new System.Drawing.Point(711, 767);
            this.progressBarText.Name = "progressBarText";
            this.progressBarText.Size = new System.Drawing.Size(0, 13);
            this.progressBarText.TabIndex = 4;
            this.progressBarText.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.applicantToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1382, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // applicantToolStripMenuItem
            // 
            this.applicantToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.editToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.toolStripSeparator6,
            this.closeToolStripMenuItem,
            this.openToolStripMenuItem,
            this.toolStripSeparator7,
            this.sendEmailToolStripMenuItem,
            this.sendFaxToolStripMenuItem});
            this.applicantToolStripMenuItem.Name = "applicantToolStripMenuItem";
            this.applicantToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.applicantToolStripMenuItem.Text = "Applicant";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Enabled = false;
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(104, 6);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.closeToolStripMenuItem.Text = "Close";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.openToolStripMenuItem.Text = "Open";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(104, 6);
            // 
            // sendEmailToolStripMenuItem
            // 
            this.sendEmailToolStripMenuItem.Name = "sendEmailToolStripMenuItem";
            this.sendEmailToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            // 
            // sendFaxToolStripMenuItem
            // 
            this.sendFaxToolStripMenuItem.Name = "sendFaxToolStripMenuItem";
            this.sendFaxToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.auditToolStripMenuItem,
            this.representativesToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // auditToolStripMenuItem
            // 
            this.auditToolStripMenuItem.Name = "auditToolStripMenuItem";
            this.auditToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.auditToolStripMenuItem.Text = "Audit";
            this.auditToolStripMenuItem.Click += new System.EventHandler(this.auditToolStripMenuItem_Click);
            // 
            // representativesToolStripMenuItem
            // 
            this.representativesToolStripMenuItem.Name = "representativesToolStripMenuItem";
            this.representativesToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.representativesToolStripMenuItem.Text = "Representatives";
            this.representativesToolStripMenuItem.Click += new System.EventHandler(this.representativesToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolEditApplicant,
            this.stripButtonFinancialReview,
            this.toolStripButton2,
            this.toolDeleteApplicant,
            this.toolStripSeparator3,
            this.toolAudit,
            this.toolRep,
            this.toolDoctorsRegister,
            this.toolStripCategoriesRegister,
            this.toolStripSeparator1,
            this.toolStripStripeRegister,
            this.toolStripStripeInvoicesRegister});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1382, 39);
            this.toolStrip1.TabIndex = 8;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.ToolStrip1_ItemClicked);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.ToolTipText = "New Applicant";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolEditApplicant
            // 
            this.toolEditApplicant.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolEditApplicant.Image = ((System.Drawing.Image)(resources.GetObject("toolEditApplicant.Image")));
            this.toolEditApplicant.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolEditApplicant.Name = "toolEditApplicant";
            this.toolEditApplicant.Size = new System.Drawing.Size(36, 36);
            this.toolEditApplicant.ToolTipText = "Edit Applicant";
            this.toolEditApplicant.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // stripButtonFinancialReview
            // 
            this.stripButtonFinancialReview.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.stripButtonFinancialReview.Enabled = false;
            this.stripButtonFinancialReview.Image = ((System.Drawing.Image)(resources.GetObject("stripButtonFinancialReview.Image")));
            this.stripButtonFinancialReview.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stripButtonFinancialReview.Name = "stripButtonFinancialReview";
            this.stripButtonFinancialReview.Size = new System.Drawing.Size(36, 36);
            this.stripButtonFinancialReview.Text = "toolStripButton2";
            this.stripButtonFinancialReview.ToolTipText = "Financial Review, active if status is \'Financial Review\'";
            this.stripButtonFinancialReview.Visible = false;
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton2.Text = "RFI";
            this.toolStripButton2.ToolTipText = "Opens the RFI Form";
            this.toolStripButton2.Visible = false;
            this.toolStripButton2.Click += new System.EventHandler(this.ToolStripButton2_Click_1);
            // 
            // toolDeleteApplicant
            // 
            this.toolDeleteApplicant.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolDeleteApplicant.Enabled = false;
            this.toolDeleteApplicant.Image = ((System.Drawing.Image)(resources.GetObject("toolDeleteApplicant.Image")));
            this.toolDeleteApplicant.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolDeleteApplicant.Name = "toolDeleteApplicant";
            this.toolDeleteApplicant.Size = new System.Drawing.Size(36, 36);
            this.toolDeleteApplicant.Text = "toolStripButton3";
            this.toolDeleteApplicant.ToolTipText = "Delete applicant";
            this.toolDeleteApplicant.Click += new System.EventHandler(this.ToolDeleteApplicant_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // toolAudit
            // 
            this.toolAudit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolAudit.Image = ((System.Drawing.Image)(resources.GetObject("toolAudit.Image")));
            this.toolAudit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolAudit.Name = "toolAudit";
            this.toolAudit.Size = new System.Drawing.Size(36, 36);
            this.toolAudit.Text = "toolStripButton3";
            this.toolAudit.ToolTipText = "Open Audit";
            this.toolAudit.Click += new System.EventHandler(this.toolAudit_Click);
            // 
            // toolRep
            // 
            this.toolRep.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolRep.Image = ((System.Drawing.Image)(resources.GetObject("toolRep.Image")));
            this.toolRep.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolRep.Name = "toolRep";
            this.toolRep.Size = new System.Drawing.Size(36, 36);
            this.toolRep.Text = "Representatives Register";
            this.toolRep.Click += new System.EventHandler(this.toolRep_Click);
            // 
            // toolDoctorsRegister
            // 
            this.toolDoctorsRegister.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolDoctorsRegister.Image = ((System.Drawing.Image)(resources.GetObject("toolDoctorsRegister.Image")));
            this.toolDoctorsRegister.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolDoctorsRegister.Name = "toolDoctorsRegister";
            this.toolDoctorsRegister.Size = new System.Drawing.Size(36, 36);
            this.toolDoctorsRegister.Text = "Physicians, Doctors, Med and Financial workers register";
            this.toolDoctorsRegister.Click += new System.EventHandler(this.ToolDoctorsRegister_Click);
            // 
            // toolStripCategoriesRegister
            // 
            this.toolStripCategoriesRegister.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripCategoriesRegister.Image = ((System.Drawing.Image)(resources.GetObject("toolStripCategoriesRegister.Image")));
            this.toolStripCategoriesRegister.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripCategoriesRegister.Name = "toolStripCategoriesRegister";
            this.toolStripCategoriesRegister.Size = new System.Drawing.Size(36, 36);
            this.toolStripCategoriesRegister.Text = "The Categories Register";
            this.toolStripCategoriesRegister.Click += new System.EventHandler(this.toolStripCategoriesRegister_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripStripeRegister
            // 
            this.toolStripStripeRegister.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripStripeRegister.Image = ((System.Drawing.Image)(resources.GetObject("toolStripStripeRegister.Image")));
            this.toolStripStripeRegister.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripStripeRegister.Name = "toolStripStripeRegister";
            this.toolStripStripeRegister.Size = new System.Drawing.Size(36, 36);
            this.toolStripStripeRegister.Text = "The Stripe Subscriptions Register";
            this.toolStripStripeRegister.ToolTipText = "Stripe Subscriptions Register";
            this.toolStripStripeRegister.Click += new System.EventHandler(this.toolStripStripeRegister_Click);
            // 
            // toolStripStripeInvoicesRegister
            // 
            this.toolStripStripeInvoicesRegister.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripStripeInvoicesRegister.Image = ((System.Drawing.Image)(resources.GetObject("toolStripStripeInvoicesRegister.Image")));
            this.toolStripStripeInvoicesRegister.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripStripeInvoicesRegister.Name = "toolStripStripeInvoicesRegister";
            this.toolStripStripeInvoicesRegister.Size = new System.Drawing.Size(36, 36);
            this.toolStripStripeInvoicesRegister.Text = "Stripe Invoice Register";
            this.toolStripStripeInvoicesRegister.ToolTipText = "Stripe Invoice Register";
            this.toolStripStripeInvoicesRegister.Click += new System.EventHandler(this.toolStripStripeInvoicesRegister_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "iconfinder_new10_216291.png");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(438, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "https://www.flaticon.com";
            this.label2.Visible = false;
            // 
            // toolStripRFI
            // 
            this.toolStripRFI.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripRFI.Image = ((System.Drawing.Image)(resources.GetObject("toolStripRFI.Image")));
            this.toolStripRFI.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripRFI.Name = "toolStripRFI";
            this.toolStripRFI.Size = new System.Drawing.Size(36, 36);
            this.toolStripRFI.Text = "toolStripButton2";
            this.toolStripRFI.Click += new System.EventHandler(this.ToolStripRFI_Click);
            // 
            // btnManualRefresh
            // 
            this.btnManualRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnManualRefresh.Location = new System.Drawing.Point(1215, 761);
            this.btnManualRefresh.Name = "btnManualRefresh";
            this.btnManualRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnManualRefresh.TabIndex = 1;
            this.btnManualRefresh.Text = "Refresh";
            this.btnManualRefresh.UseVisualStyleBackColor = true;
            this.btnManualRefresh.Click += new System.EventHandler(this.BtnManualRefresh_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Applicant Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 250;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Required Docs";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 110;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Created on";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 120;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Created By";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 135;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Updated on";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 120;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Updated by";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 135;
            // 
            // calendarColumn1
            // 
            this.calendarColumn1.HeaderText = "Due date";
            this.calendarColumn1.Name = "calendarColumn1";
            this.calendarColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.calendarColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // gridApplicants
            // 
            this.gridApplicants.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridApplicants.ContextMenuStrip = this.contextMenuStrip1;
            this.gridApplicants.DataSource = this.entityServerModeSource1;
            this.gridApplicants.Location = new System.Drawing.Point(4, 66);
            this.gridApplicants.MainView = this.gridView1;
            this.gridApplicants.Name = "gridApplicants";
            this.gridApplicants.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1});
            this.gridApplicants.Size = new System.Drawing.Size(1366, 689);
            this.gridApplicants.TabIndex = 14;
            this.gridApplicants.ToolTipController = this.toolTipController1;
            this.gridApplicants.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // entityServerModeSource1
            // 
            this.entityServerModeSource1.DefaultSorting = "updatedOn DESC";
            this.entityServerModeSource1.ElementType = typeof(Shared.viewApplicants);
            this.entityServerModeSource1.KeyExpression = "id";
            // 
            // gridView1
            // 
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.Gainsboro;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colname,
            this.colTransferSum,
            this.colincomeOnlyTrust,
            this.colrepFirstName,
            this.colTransferDate,
            this.coldocsCount,
            this.colStatusDaysAge,
            this.coluploadedAndReviewIsPendingDocumentsCount,
            this.colapprovedDocumentsCount,
            this.colIsExceptionMessage,
            this.colrejectedDocumentsCount,
            this.colcreatedOn,
            this.colupdatedOn,
            this.colPasapproved,
            this.colupdatedFromUser,
            this.colMoveDate,
            this.colFinancialInterviewDate,
            this.colFinancialInfo,
            this.colMedicalInfo,
            this.colExceptionMessage,
            this.coldueDate,
            this.colstatus,
            this.colstatusName,
            this.colmasterStatusName,
            this.colBilled,
            this.colCSRAAmount,
            this.colApproxRestartDate,
            this.colCSRASubmitDate,
            this.colPASSubmitDate,
            this.colRFISubmitDate});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            gridFormatRule1.Column = this.colIsExceptionMessage;
            gridFormatRule1.Name = "Format Is Exception RED if 1";
            formatConditionRuleExpression1.Appearance.BackColor = System.Drawing.Color.Red;
            formatConditionRuleExpression1.Appearance.BackColor2 = System.Drawing.Color.Red;
            formatConditionRuleExpression1.Appearance.BorderColor = System.Drawing.Color.Red;
            formatConditionRuleExpression1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleExpression1.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression1.Appearance.Options.UseBorderColor = true;
            formatConditionRuleExpression1.Appearance.Options.UseForeColor = true;
            formatConditionRuleExpression1.Expression = "[IsExceptionMessage] = 1";
            gridFormatRule1.Rule = formatConditionRuleExpression1;
            gridFormatRule2.Column = this.colIsExceptionMessage;
            gridFormatRule2.Name = "Format Is Exception GREEN if 0";
            formatConditionRuleExpression2.Appearance.BackColor = System.Drawing.Color.Lime;
            formatConditionRuleExpression2.Appearance.BackColor2 = System.Drawing.Color.Lime;
            formatConditionRuleExpression2.Appearance.BorderColor = System.Drawing.Color.Lime;
            formatConditionRuleExpression2.Appearance.ForeColor = System.Drawing.Color.Lime;
            formatConditionRuleExpression2.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression2.Appearance.Options.UseBorderColor = true;
            formatConditionRuleExpression2.Appearance.Options.UseForeColor = true;
            formatConditionRuleExpression2.Expression = "[IsExceptionMessage] = 0";
            gridFormatRule2.Rule = formatConditionRuleExpression2;
            gridFormatRule3.Column = this.colPasapproved;
            gridFormatRule3.Name = "PAS Approve GREEN if 1";
            formatConditionRuleExpression3.Appearance.BackColor = System.Drawing.Color.Lime;
            formatConditionRuleExpression3.Appearance.BackColor2 = System.Drawing.Color.Lime;
            formatConditionRuleExpression3.Appearance.BorderColor = System.Drawing.Color.Lime;
            formatConditionRuleExpression3.Appearance.ForeColor = System.Drawing.Color.Lime;
            formatConditionRuleExpression3.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression3.Appearance.Options.UseBorderColor = true;
            formatConditionRuleExpression3.Appearance.Options.UseForeColor = true;
            formatConditionRuleExpression3.Expression = "[Pasapproved] = 1";
            gridFormatRule3.Rule = formatConditionRuleExpression3;
            gridFormatRule4.Column = this.colPasapproved;
            gridFormatRule4.Name = "PAS Approve RED if 0";
            formatConditionRuleExpression4.Appearance.BackColor = System.Drawing.Color.Red;
            formatConditionRuleExpression4.Appearance.BackColor2 = System.Drawing.Color.Red;
            formatConditionRuleExpression4.Appearance.BorderColor = System.Drawing.Color.Red;
            formatConditionRuleExpression4.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleExpression4.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression4.Appearance.Options.UseBorderColor = true;
            formatConditionRuleExpression4.Appearance.Options.UseForeColor = true;
            formatConditionRuleExpression4.Expression = "[Pasapproved] = 0";
            gridFormatRule4.Rule = formatConditionRuleExpression4;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.FormatRules.Add(gridFormatRule2);
            this.gridView1.FormatRules.Add(gridFormatRule3);
            this.gridView1.FormatRules.Add(gridFormatRule4);
            this.gridView1.GridControl = this.gridApplicants;
            this.gridView1.GroupCount = 2;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.Behavior = DevExpress.XtraEditors.FindPanelBehavior.Filter;
            this.gridView1.OptionsFind.Condition = DevExpress.Data.Filtering.FilterCondition.Contains;
            this.gridView1.OptionsFind.FindDelay = 500;
            this.gridView1.OptionsFind.FindFilterColumns = "name";
            this.gridView1.OptionsFind.FindMode = DevExpress.XtraEditors.FindMode.Always;
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.BestFitMode = DevExpress.XtraGrid.Views.Grid.GridBestFitMode.Fast;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colmasterStatusName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colstatusName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colid
            // 
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            this.colid.OptionsColumn.ReadOnly = true;
            // 
            // colname
            // 
            this.colname.Caption = "Applicant Name";
            this.colname.FieldName = "name";
            this.colname.Name = "colname";
            this.colname.OptionsColumn.ReadOnly = true;
            this.colname.Visible = true;
            this.colname.VisibleIndex = 0;
            this.colname.Width = 104;
            // 
            // colTransferSum
            // 
            this.colTransferSum.Caption = "Transfers";
            this.colTransferSum.DisplayFormat.FormatString = "C2";
            this.colTransferSum.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTransferSum.FieldName = "transfers";
            this.colTransferSum.Name = "colTransferSum";
            this.colTransferSum.Visible = true;
            this.colTransferSum.VisibleIndex = 4;
            this.colTransferSum.Width = 34;
            // 
            // colincomeOnlyTrust
            // 
            this.colincomeOnlyTrust.Caption = "Income Only Trust";
            this.colincomeOnlyTrust.FieldName = "incomeOnlyTrust";
            this.colincomeOnlyTrust.Name = "colincomeOnlyTrust";
            this.colincomeOnlyTrust.Visible = true;
            this.colincomeOnlyTrust.VisibleIndex = 5;
            this.colincomeOnlyTrust.Width = 40;
            // 
            // colrepFirstName
            // 
            this.colrepFirstName.Caption = "Auth Rep";
            this.colrepFirstName.FieldName = "repFirstName";
            this.colrepFirstName.Name = "colrepFirstName";
            this.colrepFirstName.Visible = true;
            this.colrepFirstName.VisibleIndex = 1;
            this.colrepFirstName.Width = 58;
            // 
            // colTransferDate
            // 
            this.colTransferDate.Caption = "Date of transfer";
            this.colTransferDate.DisplayFormat.FormatString = "d";
            this.colTransferDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTransferDate.FieldName = "transferDate";
            this.colTransferDate.Name = "colTransferDate";
            this.colTransferDate.Visible = true;
            this.colTransferDate.VisibleIndex = 6;
            this.colTransferDate.Width = 55;
            // 
            // coldocsCount
            // 
            this.coldocsCount.Caption = "Required docs";
            this.coldocsCount.FieldName = "docsCount";
            this.coldocsCount.Name = "coldocsCount";
            this.coldocsCount.OptionsColumn.ReadOnly = true;
            this.coldocsCount.Visible = true;
            this.coldocsCount.VisibleIndex = 7;
            this.coldocsCount.Width = 49;
            // 
            // colStatusDaysAge
            // 
            this.colStatusDaysAge.Caption = "Status Age in Days";
            this.colStatusDaysAge.FieldName = "StatusDaysAge";
            this.colStatusDaysAge.Name = "colStatusDaysAge";
            this.colStatusDaysAge.Visible = true;
            this.colStatusDaysAge.VisibleIndex = 2;
            this.colStatusDaysAge.Width = 102;
            // 
            // coluploadedAndReviewIsPendingDocumentsCount
            // 
            this.coluploadedAndReviewIsPendingDocumentsCount.Caption = "Pending Docs";
            this.coluploadedAndReviewIsPendingDocumentsCount.FieldName = "uploadedAndReviewIsPendingDocumentsCount";
            this.coluploadedAndReviewIsPendingDocumentsCount.Name = "coluploadedAndReviewIsPendingDocumentsCount";
            this.coluploadedAndReviewIsPendingDocumentsCount.Visible = true;
            this.coluploadedAndReviewIsPendingDocumentsCount.VisibleIndex = 8;
            this.coluploadedAndReviewIsPendingDocumentsCount.Width = 47;
            // 
            // colapprovedDocumentsCount
            // 
            this.colapprovedDocumentsCount.Caption = "Approved docs";
            this.colapprovedDocumentsCount.FieldName = "approvedDocumentsCount";
            this.colapprovedDocumentsCount.Name = "colapprovedDocumentsCount";
            this.colapprovedDocumentsCount.Visible = true;
            this.colapprovedDocumentsCount.VisibleIndex = 9;
            this.colapprovedDocumentsCount.Width = 49;
            // 
            // colrejectedDocumentsCount
            // 
            this.colrejectedDocumentsCount.Caption = "Rejected docs";
            this.colrejectedDocumentsCount.FieldName = "rejectedDocumentsCount";
            this.colrejectedDocumentsCount.Name = "colrejectedDocumentsCount";
            this.colrejectedDocumentsCount.Visible = true;
            this.colrejectedDocumentsCount.VisibleIndex = 10;
            this.colrejectedDocumentsCount.Width = 49;
            // 
            // colcreatedOn
            // 
            this.colcreatedOn.Caption = "Created on";
            this.colcreatedOn.FieldName = "createdOn";
            this.colcreatedOn.Name = "colcreatedOn";
            this.colcreatedOn.OptionsColumn.ReadOnly = true;
            this.colcreatedOn.Visible = true;
            this.colcreatedOn.VisibleIndex = 11;
            this.colcreatedOn.Width = 56;
            // 
            // colupdatedOn
            // 
            this.colupdatedOn.Caption = "Updated on";
            this.colupdatedOn.FieldName = "updatedOn";
            this.colupdatedOn.Name = "colupdatedOn";
            this.colupdatedOn.OptionsColumn.ReadOnly = true;
            this.colupdatedOn.Visible = true;
            this.colupdatedOn.VisibleIndex = 13;
            this.colupdatedOn.Width = 46;
            // 
            // colupdatedFromUser
            // 
            this.colupdatedFromUser.Caption = "Updated by";
            this.colupdatedFromUser.FieldName = "updatedFromUser";
            this.colupdatedFromUser.Name = "colupdatedFromUser";
            this.colupdatedFromUser.OptionsColumn.ReadOnly = true;
            this.colupdatedFromUser.Width = 96;
            // 
            // colMoveDate
            // 
            this.colMoveDate.FieldName = "MoveDate";
            this.colMoveDate.Name = "colMoveDate";
            this.colMoveDate.Visible = true;
            this.colMoveDate.VisibleIndex = 18;
            this.colMoveDate.Width = 41;
            // 
            // colFinancialInterviewDate
            // 
            this.colFinancialInterviewDate.Caption = "Fin. Interview Date";
            this.colFinancialInterviewDate.FieldName = "FinancialInterviewDate";
            this.colFinancialInterviewDate.Name = "colFinancialInterviewDate";
            this.colFinancialInterviewDate.Visible = true;
            this.colFinancialInterviewDate.VisibleIndex = 19;
            this.colFinancialInterviewDate.Width = 61;
            // 
            // colFinancialInfo
            // 
            this.colFinancialInfo.Caption = "Financial Worker Info";
            this.colFinancialInfo.FieldName = "FinancialInfo";
            this.colFinancialInfo.Name = "colFinancialInfo";
            this.colFinancialInfo.Visible = true;
            this.colFinancialInfo.VisibleIndex = 23;
            this.colFinancialInfo.Width = 41;
            // 
            // colMedicalInfo
            // 
            this.colMedicalInfo.Caption = "Medical Worker Info";
            this.colMedicalInfo.FieldName = "MedicalInfo";
            this.colMedicalInfo.Name = "colMedicalInfo";
            this.colMedicalInfo.Visible = true;
            this.colMedicalInfo.VisibleIndex = 22;
            this.colMedicalInfo.Width = 41;
            // 
            // colExceptionMessage
            // 
            this.colExceptionMessage.FieldName = "ExceptionMessage";
            this.colExceptionMessage.Name = "colExceptionMessage";
            // 
            // coldueDate
            // 
            this.coldueDate.Caption = "Due date";
            this.coldueDate.FieldName = "dueDate";
            this.coldueDate.Name = "coldueDate";
            this.coldueDate.OptionsColumn.ReadOnly = true;
            this.coldueDate.Visible = true;
            this.coldueDate.VisibleIndex = 20;
            this.coldueDate.Width = 62;
            // 
            // colstatus
            // 
            this.colstatus.FieldName = "status";
            this.colstatus.Name = "colstatus";
            this.colstatus.OptionsColumn.ReadOnly = true;
            // 
            // colstatusName
            // 
            this.colstatusName.Caption = "Status";
            this.colstatusName.FieldName = "statusName";
            this.colstatusName.Name = "colstatusName";
            this.colstatusName.OptionsColumn.ReadOnly = true;
            this.colstatusName.Visible = true;
            this.colstatusName.VisibleIndex = 7;
            // 
            // colmasterStatusName
            // 
            this.colmasterStatusName.Caption = " ";
            this.colmasterStatusName.FieldName = "masterStatusName";
            this.colmasterStatusName.Name = "colmasterStatusName";
            this.colmasterStatusName.OptionsColumn.ReadOnly = true;
            this.colmasterStatusName.Visible = true;
            this.colmasterStatusName.VisibleIndex = 7;
            // 
            // colBilled
            // 
            this.colBilled.Caption = "Billed";
            this.colBilled.FieldName = "Billed";
            this.colBilled.Name = "colBilled";
            this.colBilled.Visible = true;
            this.colBilled.VisibleIndex = 21;
            this.colBilled.Width = 41;
            // 
            // colCSRAAmount
            // 
            this.colCSRAAmount.Caption = "CSRA Amount";
            this.colCSRAAmount.DisplayFormat.FormatString = "C2";
            this.colCSRAAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colCSRAAmount.FieldName = "CSRAAmount";
            this.colCSRAAmount.Name = "colCSRAAmount";
            this.colCSRAAmount.Visible = true;
            this.colCSRAAmount.VisibleIndex = 17;
            this.colCSRAAmount.Width = 41;
            // 
            // colApproxRestartDate
            // 
            this.colApproxRestartDate.FieldName = "ApproxRestartDate";
            this.colApproxRestartDate.Name = "colApproxRestartDate";
            this.colApproxRestartDate.Visible = true;
            this.colApproxRestartDate.VisibleIndex = 24;
            this.colApproxRestartDate.Width = 161;
            // 
            // colCSRASubmitDate
            // 
            this.colCSRASubmitDate.FieldName = "CSRASubmitDate";
            this.colCSRASubmitDate.Name = "colCSRASubmitDate";
            this.colCSRASubmitDate.Visible = true;
            this.colCSRASubmitDate.VisibleIndex = 16;
            this.colCSRASubmitDate.Width = 41;
            // 
            // colPASSubmitDate
            // 
            this.colPASSubmitDate.FieldName = "PASSubmitDate";
            this.colPASSubmitDate.Name = "colPASSubmitDate";
            this.colPASSubmitDate.Visible = true;
            this.colPASSubmitDate.VisibleIndex = 15;
            this.colPASSubmitDate.Width = 41;
            // 
            // colRFISubmitDate
            // 
            this.colRFISubmitDate.FieldName = "RFISubmitDate";
            this.colRFISubmitDate.Name = "colRFISubmitDate";
            this.colRFISubmitDate.Visible = true;
            this.colRFISubmitDate.VisibleIndex = 14;
            this.colRFISubmitDate.Width = 41;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // toolTipController1
            // 
            this.toolTipController1.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.toolTipController1_GetActiveObjectInfo);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(352, 211);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "Stripe Import";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormApplicantsRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1382, 793);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.gridApplicants);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.progressBarText);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnManualRefresh);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormApplicantsRegister";
            this.Text = "Applicants Register";
            this.Load += new System.EventHandler(this.FormApplicantsRegister_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridApplicants)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.entityServerModeSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label progressBarText;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applicantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem auditToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolEditApplicant;
        private System.Windows.Forms.ToolStripButton toolDeleteApplicant;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolStripButton toolAudit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem sendEmailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendFaxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem representativesToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolRep;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripButton toolDoctorsRegister;
        private System.Windows.Forms.ToolStripButton stripButtonFinancialReview;
        private System.Windows.Forms.ToolStripButton toolStripRFI;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Button btnManualRefresh;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem changeDueDateToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripCategoriesRegister;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private Source.Components.CalendarColumn calendarColumn1;
        private DevExpress.XtraGrid.GridControl gridApplicants;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colname;
        private DevExpress.XtraGrid.Columns.GridColumn colcreatedOn;
        private DevExpress.XtraGrid.Columns.GridColumn colstatus;
        private DevExpress.XtraGrid.Columns.GridColumn colstatusName;
        private DevExpress.XtraGrid.Columns.GridColumn colupdatedOn;
        private DevExpress.XtraGrid.Columns.GridColumn coldueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colupdatedFromUser;
        private DevExpress.XtraGrid.Columns.GridColumn colmasterStatusName;
        private DevExpress.XtraGrid.Columns.GridColumn coldocsCount;
        private System.Windows.Forms.ToolStripMenuItem changeStatusToolStripMenuItem;
        private DevExpress.XtraGrid.Columns.GridColumn colTransferSum;
        private DevExpress.XtraGrid.Columns.GridColumn colTransferDate;
        private System.Windows.Forms.ToolStripMenuItem editTransferToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeDateOfTransferToolStripMenuItem;
        private DevExpress.Data.Linq.EntityServerModeSource entityServerModeSource1;
        private DevExpress.XtraGrid.Columns.GridColumn coluploadedAndReviewIsPendingDocumentsCount;
        private DevExpress.XtraGrid.Columns.GridColumn colapprovedDocumentsCount;
        private DevExpress.XtraGrid.Columns.GridColumn colrejectedDocumentsCount;
        private DevExpress.XtraGrid.Columns.GridColumn colFinancialInterviewDate;
        private DevExpress.XtraGrid.Columns.GridColumn colincomeOnlyTrust;
        private System.Windows.Forms.ToolStripMenuItem incomeOnlyTrustToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem noneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unsentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem receivedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem caseWorkerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeFInancialInterviewDateToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem changeFinancialCaseWorkerNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeFinancialCaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emailToolStripMenuItem;
        private DevExpress.XtraGrid.Columns.GridColumn colBilled;
        private DevExpress.XtraGrid.Columns.GridColumn colCSRAAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colFinancialInfo;
        private DevExpress.XtraGrid.Columns.GridColumn colMedicalInfo;
        private System.Windows.Forms.ToolStripMenuItem medicalCaseWorkerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem phoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eMailToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem changeBilledToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeCSRAAmountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeApproxRestartDateToolStripMenuItem;
        private DevExpress.XtraGrid.Columns.GridColumn colApproxRestartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colMoveDate;
        private System.Windows.Forms.ToolStripMenuItem changeMovedDateToolStripMenuItem;
        private DevExpress.XtraGrid.Columns.GridColumn colRFISubmitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsExceptionMessage;
        private DevExpress.XtraGrid.Columns.GridColumn colCSRASubmitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPASSubmitDate;
        private System.Windows.Forms.ToolStripMenuItem handleExceptionToolStripMenuItem;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraGrid.Columns.GridColumn colExceptionMessage;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripButton toolStripStripeRegister;
        private DevExpress.XtraGrid.Columns.GridColumn colPasapproved;
        private System.Windows.Forms.ToolStripMenuItem changePASApprovedToolStripMenuItem;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusDaysAge;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripStripeInvoicesRegister;
        private DevExpress.XtraGrid.Columns.GridColumn colrepFirstName;
    }
}