﻿using ApplicantsApp.Models.Applicant.Applicant.Enums;
using ApplicantsApp.Source;
using Newtonsoft.Json;
using Shared;
using System;
using System.Windows.Forms;

namespace ApplicantsApp.Forms
{
    public partial class FormRepresentative : Form
    {
        public bool NeedRefresh { get; private set; } = false;
        public Models.FormModes FormMode { get; set; }
        public ApplicantsRepresentatives Representative { get; set; }

        private NLog.Logger logger = NLog.LogManager.GetLogger("representative");
        private Source.Progress progress;

        public FormRepresentative()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (NeedRefresh)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                DialogResult = DialogResult.Abort;
            }
        }

        private void FormRepresentative_Load(object sender, EventArgs e)
        {
            progress = new Progress(progressBar, progressBarText, this, logger);

            if (FormMode == Models.FormModes.New)
            {
                Representative = new ApplicantsRepresentatives();
                Representative.Id = Guid.NewGuid();
            }

            if (FormMode == Models.FormModes.Edit)
            {
                FillFormFromModel(JsonConvert.DeserializeObject<Models.Representative.RepresentativeInfo>(Representative.Data));
            }

            RespectMode();
        }

        private void RespectMode()
        {
            switch (FormMode)
            {
                case Models.FormModes.New:
                    Text = $"Representative      [ {FormMode.ToString().ToUpperInvariant()} ]";
                    break;

                case Models.FormModes.Edit:
                    Text = $"Representative  '{Representative.RepresentativeName}'    [ {FormMode.ToString().ToUpperInvariant()} ]";
                    break;

                default: throw new Exception($"Unsupported form mode {FormMode}");
            }

            btnSave.Visible = FormMode != Models.FormModes.View;
        }

        private void FillFormFromModel(Models.Representative.RepresentativeInfo repInfo)
        {
            AuthorizedRepresentativeCity.Text = repInfo.address.city;
            AuthorizedRepresentativeState.Text = repInfo.address.state;
            AuthorizedRepresentativeMailingAddress.Text = repInfo.address.steerAddress;
            AuthorizedRepresentativeZipCode.Text = repInfo.address.zip;

            AuthorizedRepresentativeNames.Text = repInfo.name;
            AuthorizedRepresentativeRelationship.Text = repInfo.relationship;

            switch (repInfo.writtenLanguage)
            {
                case "English":
                    RepresentativeWritten_English.Checked = true;
                    break;

                case "Spanish":
                    RepresentativeWritten_Spanish.Checked = true;
                    break;

                default:
                    RepresentativeWritten_Other.Checked = true;
                    RepresentativeWritten_OtherText.Text = repInfo.writtenLanguage;
                    break;
            }

            switch (repInfo.spokenLanguage)
            {
                case "English":
                    RepresentativeSpokoen_English.Checked = true;
                    break;

                case "Spanish":
                    RepresentativeSpokoen_English.Checked = true;
                    break;

                default:
                    RepresentativeSpokoen_English.Checked = true;
                    RepresentativeSpokoen_English.Text = repInfo.spokenLanguage;
                    break;
            }

            AuthorizedRepresentativeEmailAddress.Text = repInfo.email;
            RepresentativeEmailAddress.Text = repInfo.heaPlusEmailNotifyAddress;
            RepresentativeText_Number.Text = repInfo.heaPlusTextNotifyNumber;
            RepresentativeEmail_Yes.Checked = repInfo.isheaPlusEmailNotify;
            RepresentativeText_Yes.Checked = repInfo.isheaPlusTextNotify;
            AuthorizedRepresentativePhoneNumber.Text = repInfo.phoneNumber;
            RepresentativeOtherPhone.Text = repInfo.otherPhoneNumber;
            WitnessName.Text = repInfo.witnessName;

            RepresentativePhone_Home.Checked = repInfo.phoneNumberType == RepresentativePhoneNumberType.Home;
            RepresentativePhone_Cell.Checked = repInfo.phoneNumberType == RepresentativePhoneNumberType.Cell;
            RepresentativePhone_Work.Checked = repInfo.phoneNumberType == RepresentativePhoneNumberType.Work;
            RepresentativePhone_Message.Checked = repInfo.phoneNumberType == RepresentativePhoneNumberType.Message;
            RepresentativePhone_Other.Checked = repInfo.phoneNumberType == RepresentativePhoneNumberType.Other;
            RepresentativePhone_OtherText.Text = repInfo.otherPhoneNumberTypeOther;

            RepresentativeOtherPhone_Home.Checked = repInfo.otherphoneNumberType == RepresentativePhoneNumberType.Home;
            RepresentativeOtherPhone_Cell.Checked = repInfo.otherphoneNumberType == RepresentativePhoneNumberType.Cell;
            RepresentativeOtherPhone_Work.Checked = repInfo.otherphoneNumberType == RepresentativePhoneNumberType.Work;
            RepresentativeOtherPhone_Message.Checked = repInfo.otherphoneNumberType == RepresentativePhoneNumberType.Message;
            RepresentativeOtherPhone_Other.Checked = repInfo.otherphoneNumberType == RepresentativePhoneNumberType.Other;
            RepresentativeOtherPhone_OtherText.Text = repInfo.otherPhoneNumberTypeOther;


            LegalGuardianCity.Text = repInfo.guardian.address.city;            
            LegalGuardianState.Text = repInfo.guardian.address.state;
            LegalGuardianState.Text = repInfo.guardian.address.steerAddress;
            LegalGuardianZipCode.Text = repInfo.guardian.address.zip =

            LegalGuardianEmailAddress.Text = repInfo.guardian.emailAddress;
            LegalGuardianRelationShip.Text = repInfo.guardian.guardianRelationship;
            IsRepresentativeYourLegalGuardian_Yes.Checked = repInfo.guardian.isRepresentativeAGuardian;
            LegalGuardianNames.Text = repInfo.guardian.nameOfGuardian;
            LegalGuardianPhoneNumber.Text = repInfo.guardian.phoneNumber;
        }

        private Models.Representative.RepresentativeInfo FillRepresentativeModelFromForm()
        {
            Models.Representative.RepresentativeInfo representativeInfo = new Models.Representative.RepresentativeInfo()
            {
                address = new Models.Applicant.Applicant.Common.Address()
                {
                    addressLine2 = null,
                    city = AuthorizedRepresentativeCity.Text,
                    country = "United States",
                    state = AuthorizedRepresentativeState.Text,
                    steerAddress = AuthorizedRepresentativeMailingAddress.Text,
                    zip = AuthorizedRepresentativeZipCode.Text
                },
                name = AuthorizedRepresentativeNames.Text,
                relationship = AuthorizedRepresentativeRelationship.Text,
                writtenLanguage =
                    RepresentativeWritten_English.Checked ? "English" :
                        RepresentativeWritten_Spanish.Checked ? "Spanish" :
                            RepresentativeWritten_Other.Checked ? RepresentativeWritten_OtherText.Text : null,
                spokenLanguage =
                    RepresentativeSpokoen_English.Checked ? "English" :
                            RepresentativeSpokoen_Spanish.Checked ? "Spanish" :
                                RepresentativeSpokoen_Other.Checked ? RepresentativeSpokoen_OtherText.Text : null,

                email = AuthorizedRepresentativeEmailAddress.Text,
                heaPlusEmailNotifyAddress = RepresentativeEmailAddress.Text,
                heaPlusTextNotifyNumber = RepresentativeText_Number.Text,
                isheaPlusEmailNotify = RepresentativeEmail_Yes.Checked,
                isheaPlusTextNotify = RepresentativeText_Yes.Checked,
                phoneNumber = AuthorizedRepresentativePhoneNumber.Text,
                otherPhoneNumber = RepresentativeOtherPhone.Text,
                witnessName = WitnessName.Text,

                phoneNumberType = RepresentativePhone_Home.Checked ? RepresentativePhoneNumberType.Home :
                    RepresentativePhone_Cell.Checked ? RepresentativePhoneNumberType.Cell :
                        RepresentativePhone_Work.Checked ? RepresentativePhoneNumberType.Work :
                            RepresentativePhone_Message.Checked ? RepresentativePhoneNumberType.Message :
                                RepresentativePhone_Other.Checked ? RepresentativePhoneNumberType.Other : RepresentativePhoneNumberType.None,

                otherphoneNumberType = RepresentativeOtherPhone_Home.Checked ? RepresentativePhoneNumberType.Home :
                    RepresentativeOtherPhone_Cell.Checked ? RepresentativePhoneNumberType.Cell :
                        RepresentativeOtherPhone_Work.Checked ? RepresentativePhoneNumberType.Work :
                            RepresentativeOtherPhone_Message.Checked ? RepresentativePhoneNumberType.Message :
                                RepresentativeOtherPhone_Other.Checked ? RepresentativePhoneNumberType.Other : RepresentativePhoneNumberType.None,

                phoneNumberTypeOther = RepresentativePhone_OtherText.Text,
                otherPhoneNumberTypeOther = RepresentativeOtherPhone_OtherText.Text,

                guardian = new Models.Representative.Guardian()
                {
                    address = new Models.Applicant.Applicant.Common.Address()
                    {
                        addressLine2 = null,
                        city = LegalGuardianCity.Text,
                        country = "United States", 
                        state = LegalGuardianState.Text,
                        steerAddress = LegalGuardianState.Text,
                        zip = LegalGuardianZipCode.Text
                    },
                    emailAddress = LegalGuardianEmailAddress.Text,
                    guardianRelationship = LegalGuardianRelationShip.Text,
                    isRepresentativeAGuardian = IsRepresentativeYourLegalGuardian_Yes.Checked,
                    nameOfGuardian = LegalGuardianNames.Text,
                    phoneNumber = LegalGuardianPhoneNumber.Text
                }

            };

            return representativeInfo;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!IsFormValid())
                return;

            //string serializedApplicantDataXml = FormSerialisation.FormSerialisor.Serialize(groupAuthorizedRepresentative);
            string repData = JsonConvert.SerializeObject(FillRepresentativeModelFromForm());

            progress.ShowProgressBar("Saving representative");
            try
            {
                Representative.Status = (int)Models.Representative.RepresentativeStatus.Enabled;
                Representative.Data = repData;

                Representative.RepresentativeEmail = AuthorizedRepresentativeEmailAddress.Text;
                Representative.RepresentativeName = AuthorizedRepresentativeNames.Text;
                Representative.Type = (int)Models.Representative.RepresentativeType.RepresentativeEmployee;

                switch (FormMode)
                {
                    case Models.FormModes.New:

                        //the id is already generated in FormLoad, we need it when uploading to azure
                        Representative.CreatedByUserId = Db.GetUserId();
                        Representative.CreatedOn = DateTime.Now;

                        Db.InsertRepresentative(Representative, logger);
                        break;

                    case Models.FormModes.Edit:
                        Db.UpdateRepresentative(Representative, logger);
                        break;

                    default:
                        throw new Exception($"Unexpected form mode: {FormMode}");
                }

                progress.SetProgressText("Add audit data");
                Db.InsertAction(
                    logger,
                    Representative.Id,
                    FormMode == Models.FormModes.Edit ? Models.ReportActions.RepresentativeUpdate : Models.ReportActions.RepresentativeCreate,
                    data: JsonConvert.SerializeObject(Representative));
            }
            finally
            {
                progress.HideProgressBar();
            }

            MessageBox.Show("The representative was stored successfully!");
            if (FormMode == Models.FormModes.New)
            {
                FormMode = Models.FormModes.Edit;
                Representative = Db.LoadRepresentative(Representative.Id, logger);

                RespectMode();
            }

            NeedRefresh = true;
        }

        private bool IsFormValid()
        {
            if (!Mop.ValidateControl(
                        RepresentativeSpokoen_Other.Checked && string.IsNullOrWhiteSpace(RepresentativeSpokoen_OtherText.Text),
                        "Representative Other spoken language when 'Other' is checked",
                        RepresentativeSpokoen_OtherText)

                || !Mop.ValidateControl(
                        RepresentativeWritten_Other.Checked && string.IsNullOrWhiteSpace(RepresentativeWritten_OtherText.Text),
                        "Representative Other writen language when 'Other' is checked",
                        RepresentativeWritten_OtherText)

                || !Mop.ValidateControl(
                        RepresentativeEmail_Yes.Checked && string.IsNullOrWhiteSpace(RepresentativeEmailAddress.Text),
                        "Representative email address when 'Email' is checked",
                        RepresentativeEmailAddress)

                || !Mop.ValidateControl(
                        RepresentativeText_Yes.Checked && string.IsNullOrWhiteSpace(RepresentativeText_Number.Text),
                        "Representative number when 'Text' is checked",
                        RepresentativeText_Number)

                || !Mop.ValidateControl(
                        RepresentativePhone_Other.Checked && string.IsNullOrWhiteSpace(RepresentativePhone_OtherText.Text),
                        "Representative phone type when 'Other' is checked",
                        RepresentativePhone_OtherText)

                || !Mop.ValidateControl(
                        RepresentativeOtherPhone_Other.Checked && string.IsNullOrWhiteSpace(RepresentativeOtherPhone_OtherText.Text),
                        "Representative 'Other phone' type when 'Other' is checked",
                        RepresentativeOtherPhone_OtherText)
                        )
            {
                return false;
            }

            return Mop.ValidateChildControls(groupAuthorizedRepresentative);
        }

        private void RepresentativeSpokoen_Other_CheckedChanged(object sender, EventArgs e)
        {
            RepresentativeSpokoen_OtherText.Enabled = RepresentativeSpokoen_Other.Checked;
        }

        private void RepresentativeWritten_Other_CheckedChanged(object sender, EventArgs e)
        {
            RepresentativeWritten_OtherText.Enabled = RepresentativeWritten_Other.Checked;
        }

        private void RepresentativeEmail_Yes_CheckedChanged(object sender, EventArgs e)
        {
            RepresentativeEmailAddress.Enabled = RepresentativeEmail_Yes.Checked;
        }

        private void RepresentativeText_Yes_CheckedChanged(object sender, EventArgs e)
        {
            RepresentativeText_Number.Enabled = RepresentativeText_Yes.Checked;
        }

        private void RepresentativePhone_Other_CheckedChanged(object sender, EventArgs e)
        {
            RepresentativePhone_OtherText.Enabled = RepresentativePhone_Other.Checked;
        }

        private void RepresentativeOtherPhone_Other_CheckedChanged(object sender, EventArgs e)
        {
            RepresentativeOtherPhone_OtherText.Enabled = RepresentativeOtherPhone_Other.Checked;
        }
    }
}
