﻿using ApplicantsApp.Models;
using ApplicantsApp.Models.DTOs;
using ApplicantsApp.Source;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp.Forms
{
    public partial class FormStripeInvoices : Form
    {
        private NLog.Logger logger = NLog.LogManager.GetLogger("stripe-invoices");

        private Progress progress;

        public FormStripeInvoices()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            LoadInvoices();
            LoadApplicants();
        }

        private void FormStripeInvoices_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            progress = new Progress(progressBar, progressBarText, this, logger);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void LoadApplicants()
        {
            comboApplicants.Items.Clear();

            List<ApplicantNewDTO> resp = Db.LoadApplicantsNew(0, logger); //TODO: load from db or constant
            resp.ForEach(f => comboApplicants.Items.Add(new ComboboxItem
            {
                Text = f.Name + "  " + f.StripeCustomerId,
                Value = f.Id,
                MetaString1 = f.StripeCustomerId
            }));
        }

        private void LoadInvoices()
        {
            dataGridView1.Rows.Clear();

            progress.ShowProgressBar("Loading data...");
            try
            {
                using (var db = new Shared.DbModel())
                {
                    var query = db.Applicants_StripeInvoiceRegister.Where(w => true);

                    if (comboApplicants.SelectedItem != null)
                    {
                        string customerStripeId = ((ComboboxItem)comboApplicants.SelectedItem).MetaString1;

                        if (!string.IsNullOrWhiteSpace(customerStripeId)) {
                            query = query.Where(w => w.CustomerId == customerStripeId);
                        } else
                        {
                            //if you select customer which does not have strupeId there should not be any invoices, rghjt?
                            query = query.Where(w => w.CustomerId == "shoud not exist");
                        }
                    }

                    if (radioPayedOnly.Checked)
                    {
                        query = query.Where(w => w.Paid);
                    } 
                    else if (radioNotPayedOnly.Checked)
                    {
                        query = query.Where(w => !w.Paid);
                    }

                    foreach (Shared.Applicants_StripeInvoiceRegister subscriptionInfo in query)
                    {
                        dataGridView1.Rows.Add(
                            subscriptionInfo.InvoiceId,
                            subscriptionInfo.ChargeId,
                            subscriptionInfo.PaymentId,
                            subscriptionInfo.AmountDue,
                            subscriptionInfo.AmountPaid,
                            subscriptionInfo.AttemptCount,
                            subscriptionInfo.Attempted,
                            subscriptionInfo.CustomerId,
                            subscriptionInfo.Paid ? "Yes" : "No",
                            subscriptionInfo.Status,
                            subscriptionInfo.SubscriptionId,
                            subscriptionInfo.PlanId
                            );
                    }
                }

            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This can take a minute or two. \nThe message you will see in the end can be structured or formatted a lot better, if you preffer to.");

            progress.ShowProgressBar("Refreshing subscriptions, invoices, refunds...");
            try
            {
                using (var webClient = new HttpClient())
                {
                    webClient.Timeout = TimeSpan.FromMinutes(5);
                    string resp = webClient.GetStringAsync("http://jubotho-001-site1.itempurl.com/api/api/stripe/HandleInvoices?skipEmailingForNotPayedInvoices=true").Result;
                    MessageBox.Show(resp.Replace("\\r\\n", Environment.NewLine));

                    LoadInvoices();
                }
            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            comboApplicants.SelectedItem = null;
            radioPayedAll.Checked = true;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadInvoices();
        }
    }
}
