﻿using ApplicantsApp.Source;
using EnumsNET;
using Newtonsoft.Json;
using Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp.Forms
{
    public partial class FormDoctor : Form
    {
        public bool NeedRefresh { get; private set; } = false;
        public Models.FormModes FormMode { get; set; }
        public ApplicantsDoctors Doctor { get; set; }

        private NLog.Logger logger = NLog.LogManager.GetLogger("worker");
        private Source.Progress progress;

        public FormDoctor()
        {
            InitializeComponent();
        }

        private void FormDoctor_Load(object sender, EventArgs e)
        {
            comboTypes.SelectedIndex = 0;
            progress = new Progress(progressBar, progressBarText, this, logger);

            if (FormMode == Models.FormModes.New)
            {
                Doctor = new ApplicantsDoctors();
                Doctor.Id = Guid.NewGuid();
            }

            if (FormMode == Models.FormModes.Edit)
            {
                txtAddress.Text = Doctor.Address;
                txtDoctorName.Text = Doctor.Name;
                txtFax.Text = Doctor.OfficeFax;
                txtPhone.Text = Doctor.OfficeTelephoneNumber;
                txtEmail.Text = Doctor.Email;
                comboTypes.SelectedIndex = Shared.Models.Enums.Helper.GetComboIndexByWorkerType(Enums.Parse<Shared.Models.Enums.WorkerTypes>(Doctor.Type, true));
            }

            RespectMode();
        }

        private void RespectMode()
        {
            switch (FormMode)
            {
                case Models.FormModes.New:
                    Text = $"Worker [ {FormMode.ToString().ToUpperInvariant()} ]";
                    break;

                case Models.FormModes.Edit:
                    Text = $"Worker  '{Doctor.Name}'    [ {FormMode.ToString().ToUpperInvariant()} ]";
                    break;

                default: throw new Exception($"Unsupported form mode {FormMode}");
            }

            btnSave.Visible = FormMode != Models.FormModes.View;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (!IsFormValid())
                return;

            progress.ShowProgressBar("Saving worker");
            try
            {
                Doctor.Address = txtAddress.Text;
                Doctor.Name = txtDoctorName.Text;
                Doctor.OfficeFax = txtFax.Text;
                Doctor.OfficeTelephoneNumber = txtPhone.Text;
                Doctor.Email = txtEmail.Text;
                Doctor.Type = Shared.Models.Enums.Helper.GetWorkerTypeFromComboIndex(comboTypes.SelectedIndex).ToString();

                switch (FormMode)
                {
                    case Models.FormModes.New:
                        Db.CreateDoctor(Doctor, logger);
                        break;

                    case Models.FormModes.Edit:
                        Db.UpdateDoctor(Doctor, logger);
                        break;

                    default:
                        throw new Exception($"Unexpected form mode: {FormMode}");
                }

                progress.SetProgressText("Add audit data");
                Db.InsertAction(
                    logger,
                    Doctor.Id,
                    FormMode == Models.FormModes.Edit ? Models.ReportActions.PhysycianUpdate : Models.ReportActions.PhysycianCreate,
                    correlationId: Doctor.Id,
                    data: JsonConvert.SerializeObject(Doctor));
            }
            finally
            {
                progress.HideProgressBar();
            }

            MessageBox.Show("The worker was stored successfully!");
            if (FormMode == Models.FormModes.New)
            {
                FormMode = Models.FormModes.Edit;
                Doctor = Db.GetDoctor(Doctor.Id, logger);

                RespectMode();
            }

            NeedRefresh = true;
        }

        private bool IsFormValid()
        {
            if (!Mop.ValidateControl(
                        string.IsNullOrWhiteSpace(txtDoctorName.Text),
                        "MD or Practice Name",
                        txtDoctorName))
            {
                return false;
            }

            return true;
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormDoctor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (NeedRefresh)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                DialogResult = DialogResult.Abort;
            }
        }
    }
}
