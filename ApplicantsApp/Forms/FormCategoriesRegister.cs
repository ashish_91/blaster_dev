﻿using Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp.Forms
{
    public partial class FormCategoriesRegister : Form
    {
        private Source.Progress progress;
        private NLog.Logger logger = NLog.LogManager.GetLogger("categoriesRegister");

        public FormCategoriesRegister()
        {
            InitializeComponent();
        }

        private void btnHelpPriority_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Set the order in which the categories should be listed in the email and in the category tree.");
        }

        private void FormCategoriesRegister_Load(object sender, EventArgs e)
        {
            progress = new Source.Progress(progressBar, labelProgress, this, logger);
            RefreshCategories();
        }

        private void RefreshCategories()
        {
            progress.ShowProgressBar("Refreshing categories...");
            try
            {
                gridCategories.Rows.Clear();

                foreach (var rep in Db.GetAllDocumentCategories(logger))
                {
                    gridCategories.Rows.Add(
                        rep.Id,
                        rep.Category,
                        rep.Priority,
                        rep.Description                        
                    );
                }
            }
            finally
            {
                progress.HideProgressBar();
            }

            gridCategories_SelectionChanged(null, null);
        }

        private void gridCategories_SelectionChanged(object sender, EventArgs e)
        {
            int? selectedCategory = GetSelected();

            toolBtnEdit.Enabled = selectedCategory != null;
            toolBtnDelete.Enabled = selectedCategory != null;
            toolBtnAdd.Enabled = true;
        }

        private void menuStripAdd_Opening(object sender, CancelEventArgs e)
        {
            int? selectedCategory = GetSelected();

            menuStripDelete.Enabled = selectedCategory != null;
            menuStripEdit.Enabled = selectedCategory != null;
            menuStripAdd.Enabled = true;
        }

        private int? GetSelected()
        {
            if (gridCategories.SelectedRows.Count > 0)
            {
                return (int)gridCategories.SelectedRows[0].Cells[0].Value;
            }
            else
            {
                return null;
            }
        }

        private void toolBtnAdd_Click(object sender, EventArgs e)
        {
            AddOrEdit(Models.FormModes.New, null);
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddOrEdit(Models.FormModes.New, null);
        }

        private void AddOrEdit(Models.FormModes formMode, Applicants_DocumentCategories category)
        {
            using (var categoryForm = new FormCategoryDetails
            {
                FormMode = formMode,
                Category = category
            })
            {
                DialogResult res = categoryForm.ShowDialog();

                switch (res)
                {
                    case DialogResult.Abort:
                    case DialogResult.Cancel:
                        break;

                    case DialogResult.OK:
                        RefreshCategories();
                        break;

                    default:
                        throw new Exception($"Unknown dialog result: '{res}' occured!");
                }
            }
        }

        private void menuStripEdit_Click(object sender, EventArgs e)
        {
            Edit();
        }

        private void toolBtnEdit_Click(object sender, EventArgs e)
        {
            Edit();
        }

        private void Edit()
        {
            int? selectedId = GetSelected();
            if (!selectedId.HasValue)
            {
                MessageBox.Show("Select a category!");
                return;
            }

            var item = Db.GetCategory(selectedId.Value, logger);

            AddOrEdit(Models.FormModes.Edit, item);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormCategoriesRegister_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void toolBtnDelete_Click(object sender, EventArgs e)
        {
            Delete();
        }

        private void menuStripDelete_Click(object sender, EventArgs e)
        {
            Delete();
        }

        private void Delete()
        {
            int? selectedId = GetSelected();
            if (!selectedId.HasValue)
            {
                MessageBox.Show("Select a category!");
                return;
            }

            DialogResult result = MessageBox.Show("Are you sure you want to delete the selected category? This will also delete all documents related to that category!", "Warning", MessageBoxButtons.YesNo);
            if (result == DialogResult.No || result == DialogResult.Cancel)
            {
                return;
            }

            Shared.Source.DB.Documents.DeleteCategory(selectedId.Value, logger);
        }
    }
}
