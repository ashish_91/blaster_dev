﻿using EnumsNET;
using Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp.Forms
{
    public partial class FormDoctorsRegister : Form
    {
        private Source.Progress progress;
        private NLog.Logger logger = NLog.LogManager.GetLogger("docCatalog");

        public FormDoctorsRegister()
        {
            InitializeComponent();
        }

        private void FormDoctorsRegister_Load(object sender, EventArgs e)
        {
            progress = new Source.Progress(progressBar, labelProgress, this, logger);
            RefreshDoctors();
        }

        private void RefreshDoctors()
        {
            progress.ShowProgressBar("Refreshing workers...");
            try
            {
                gridDoctors.Rows.Clear();

                foreach (var rep in Db.GetAllDoctors(logger))
                {
                    gridDoctors.Rows.Add(
                        rep.Id.ToString(),
                        rep.Name,
                        Shared.Models.Enums.Helper.GetWorkerTypeNiceNameFromEnum(Enums.Parse<Shared.Models.Enums.WorkerTypes>(rep.Type, true)) ,
                        rep.Email,
                        rep.OfficeTelephoneNumber,
                        rep.OfficeFax,
                        rep.Address,
                        rep.CreationDate.ToString(Source.Resources.USADateTime)
                    );
                }
            }
            finally
            {
                progress.HideProgressBar();
            }

            GridDoctors_SelectionChanged(null, null);
        }

        private void ContextMenuDoctors_Opening(object sender, CancelEventArgs e)
        {
            Guid? selectedDoctor = GetSelectedDoctor();

            menuStripDelete.Enabled = selectedDoctor != null;
            menuStripEdit.Enabled = selectedDoctor != null;
            menuStripAdd.Enabled = true;
        }

        private void GridDoctors_SelectionChanged(object sender, EventArgs e)
        {
            Guid? selectedDoctor = GetSelectedDoctor();

            toolBtnEdit.Enabled = selectedDoctor != null;
            toolBtnDelete.Enabled = false;
            toolBtnAdd.Enabled = true;
        }

        private Guid? GetSelectedDoctor()
        {
            if (gridDoctors.SelectedRows.Count > 0)
            {
                return Guid.Parse(gridDoctors.SelectedRows[0].Cells[0].Value.ToString());
            }
            else
            {
                return null;
            }
        }

        private void ToolBtnAdd_Click(object sender, EventArgs e)
        {
            AddOrEditDoctor(Models.FormModes.New, null);
        }

        private void AddToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddOrEditDoctor(Models.FormModes.New, null);
        }

        private void AddOrEditDoctor(Models.FormModes formMode, ApplicantsDoctors doctor)
        {
            using (var doctorForm = new FormDoctor
            {
                FormMode = formMode,
                Doctor = doctor
            })
            {
                DialogResult res = doctorForm.ShowDialog();

                switch (res)
                {
                    case DialogResult.Abort:
                    case DialogResult.Cancel:
                        break;

                    case DialogResult.OK:
                        RefreshDoctors();
                        break;

                    default:
                        throw new Exception($"Unknown dialog result: '{res}' occured!");
                }
            }
        }

        private void MenuStripEdit_Click(object sender, EventArgs e)
        {
            EditDoctor();
        }

        private void ToolBtnEdit_Click(object sender, EventArgs e)
        {
            EditDoctor();
        }

        private void EditDoctor()
        {            
            Guid? selectedDoctor = GetSelectedDoctor();
            if (!selectedDoctor.HasValue)
            {
                MessageBox.Show("Select a worker!");
                return;
            }

            ApplicantsDoctors doctor = Db.GetDoctor(selectedDoctor.Value, logger);

            AddOrEditDoctor(Models.FormModes.Edit, doctor);
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormDoctorsRegister_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void ToolBtnDelete_Click(object sender, EventArgs e)
        {

        }
    }
}