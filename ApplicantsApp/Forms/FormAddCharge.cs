﻿using ApplicantsApp.Models;
using ApplicantsApp.Models.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp.Forms
{
    public partial class FormAddCharge : Form
    {
        private NLog.Logger logger = NLog.LogManager.GetLogger("stripe");

        //private const string StripeApiKey = "rk_test_uVvYJyPPghQAvmzjWnAbfCxj00hcbOhJy6";
        private const string StripeApiKey = "sk_live_7hMRqrf9iejUj7HahNxxZMhu00kVzfhdq2";
        private const string BaseCustomersAddress = "https://dashboard.stripe.com/test/customers/";

        public FormAddCharge()
        {
            InitializeComponent();
        }

        private void LoadApplicants()
        {
            comboApplicants.Items.Clear();

            List<ApplicantNewDTO> resp = Db.LoadApplicantsNew(0, logger); //TODO: load from db or constant
            resp.ForEach(f => comboApplicants.Items.Add(new ComboboxItem
            {
                Text = f.Name + "  " + f.StripeCustomerId,
                Value = f.Id,
                MetaString1 = f.StripeCustomerId
            }));
        }

        private void FormAddCharge_Load(object sender, EventArgs e)
        {
            LoadApplicants();
        }
    }

}
