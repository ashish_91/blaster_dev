﻿namespace ApplicantsApp.Forms
{
    partial class FormRepresentative
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupAuthorizedRepresentative = new System.Windows.Forms.GroupBox();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.label61 = new System.Windows.Forms.Label();
            this.RepresentativeOtherPhone = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.RepresentativeOtherPhone_OtherText = new System.Windows.Forms.TextBox();
            this.RepresentativeOtherPhone_Other = new System.Windows.Forms.RadioButton();
            this.RepresentativeOtherPhone_Message = new System.Windows.Forms.RadioButton();
            this.RepresentativeOtherPhone_Work = new System.Windows.Forms.RadioButton();
            this.RepresentativeOtherPhone_Cell = new System.Windows.Forms.RadioButton();
            this.RepresentativeOtherPhone_Home = new System.Windows.Forms.RadioButton();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.label60 = new System.Windows.Forms.Label();
            this.AuthorizedRepresentativePhoneNumber = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.RepresentativePhone_OtherText = new System.Windows.Forms.TextBox();
            this.RepresentativePhone_Other = new System.Windows.Forms.RadioButton();
            this.RepresentativePhone_Message = new System.Windows.Forms.RadioButton();
            this.RepresentativePhone_Work = new System.Windows.Forms.RadioButton();
            this.RepresentativePhone_Cell = new System.Windows.Forms.RadioButton();
            this.RepresentativePhone_Home = new System.Windows.Forms.RadioButton();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.RepresentativeText_Number = new System.Windows.Forms.TextBox();
            this.RepresentativeText_Yes = new System.Windows.Forms.CheckBox();
            this.RepresentativeEmailAddress = new System.Windows.Forms.TextBox();
            this.RepresentativeEmail_Yes = new System.Windows.Forms.CheckBox();
            this.label57 = new System.Windows.Forms.Label();
            this.WitnessName = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.RepresentativeWritten_OtherText = new System.Windows.Forms.TextBox();
            this.RepresentativeWritten_Other = new System.Windows.Forms.RadioButton();
            this.RepresentativeWritten_Spanish = new System.Windows.Forms.RadioButton();
            this.RepresentativeWritten_English = new System.Windows.Forms.RadioButton();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.RepresentativeSpokoen_OtherText = new System.Windows.Forms.TextBox();
            this.RepresentativeSpokoen_Other = new System.Windows.Forms.RadioButton();
            this.RepresentativeSpokoen_Spanish = new System.Windows.Forms.RadioButton();
            this.RepresentativeSpokoen_English = new System.Windows.Forms.RadioButton();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.IsRepresentativeYourLegalGuardian_No = new System.Windows.Forms.RadioButton();
            this.IsRepresentativeYourLegalGuardian_Yes = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LegalGuardianRelationShip = new System.Windows.Forms.TextBox();
            this.LegalGuardianMailingAddress = new System.Windows.Forms.TextBox();
            this.AuthorizedRepresentativeMailingAddress = new System.Windows.Forms.TextBox();
            this.AuthorizedRepresentativeRelationship = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.LegalGuardianZipCode = new System.Windows.Forms.TextBox();
            this.AuthorizedRepresentativeZipCode = new System.Windows.Forms.TextBox();
            this.LegalGuardianNames = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.LegalGuardianState = new System.Windows.Forms.TextBox();
            this.AuthorizedRepresentativeState = new System.Windows.Forms.TextBox();
            this.AuthorizedRepresentativeNames = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.LegalGuardianPhoneNumber = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.LegalGuardianEmailAddress = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.AuthorizedRepresentativeEmailAddress = new System.Windows.Forms.TextBox();
            this.LegalGuardianCity = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.AuthorizedRepresentativeCity = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.progressBarText = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.groupAuthorizedRepresentative.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupAuthorizedRepresentative
            // 
            this.groupAuthorizedRepresentative.Controls.Add(this.groupBox28);
            this.groupAuthorizedRepresentative.Controls.Add(this.groupBox27);
            this.groupAuthorizedRepresentative.Controls.Add(this.groupBox26);
            this.groupAuthorizedRepresentative.Controls.Add(this.WitnessName);
            this.groupAuthorizedRepresentative.Controls.Add(this.label56);
            this.groupAuthorizedRepresentative.Controls.Add(this.groupBox25);
            this.groupAuthorizedRepresentative.Controls.Add(this.groupBox24);
            this.groupAuthorizedRepresentative.Controls.Add(this.groupBox23);
            this.groupAuthorizedRepresentative.Controls.Add(this.groupBox1);
            this.groupAuthorizedRepresentative.Controls.Add(this.LegalGuardianRelationShip);
            this.groupAuthorizedRepresentative.Controls.Add(this.LegalGuardianMailingAddress);
            this.groupAuthorizedRepresentative.Controls.Add(this.AuthorizedRepresentativeMailingAddress);
            this.groupAuthorizedRepresentative.Controls.Add(this.AuthorizedRepresentativeRelationship);
            this.groupAuthorizedRepresentative.Controls.Add(this.label34);
            this.groupAuthorizedRepresentative.Controls.Add(this.label28);
            this.groupAuthorizedRepresentative.Controls.Add(this.label22);
            this.groupAuthorizedRepresentative.Controls.Add(this.label20);
            this.groupAuthorizedRepresentative.Controls.Add(this.LegalGuardianZipCode);
            this.groupAuthorizedRepresentative.Controls.Add(this.AuthorizedRepresentativeZipCode);
            this.groupAuthorizedRepresentative.Controls.Add(this.LegalGuardianNames);
            this.groupAuthorizedRepresentative.Controls.Add(this.label21);
            this.groupAuthorizedRepresentative.Controls.Add(this.LegalGuardianState);
            this.groupAuthorizedRepresentative.Controls.Add(this.AuthorizedRepresentativeState);
            this.groupAuthorizedRepresentative.Controls.Add(this.AuthorizedRepresentativeNames);
            this.groupAuthorizedRepresentative.Controls.Add(this.label33);
            this.groupAuthorizedRepresentative.Controls.Add(this.label4);
            this.groupAuthorizedRepresentative.Controls.Add(this.label32);
            this.groupAuthorizedRepresentative.Controls.Add(this.label27);
            this.groupAuthorizedRepresentative.Controls.Add(this.LegalGuardianPhoneNumber);
            this.groupAuthorizedRepresentative.Controls.Add(this.label31);
            this.groupAuthorizedRepresentative.Controls.Add(this.label30);
            this.groupAuthorizedRepresentative.Controls.Add(this.label26);
            this.groupAuthorizedRepresentative.Controls.Add(this.LegalGuardianEmailAddress);
            this.groupAuthorizedRepresentative.Controls.Add(this.label24);
            this.groupAuthorizedRepresentative.Controls.Add(this.label29);
            this.groupAuthorizedRepresentative.Controls.Add(this.AuthorizedRepresentativeEmailAddress);
            this.groupAuthorizedRepresentative.Controls.Add(this.LegalGuardianCity);
            this.groupAuthorizedRepresentative.Controls.Add(this.label25);
            this.groupAuthorizedRepresentative.Controls.Add(this.AuthorizedRepresentativeCity);
            this.groupAuthorizedRepresentative.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupAuthorizedRepresentative.Location = new System.Drawing.Point(12, 12);
            this.groupAuthorizedRepresentative.Name = "groupAuthorizedRepresentative";
            this.groupAuthorizedRepresentative.Size = new System.Drawing.Size(943, 408);
            this.groupAuthorizedRepresentative.TabIndex = 5;
            this.groupAuthorizedRepresentative.TabStop = false;
            this.groupAuthorizedRepresentative.Text = "Representative and Legal Guardian/Conservator";
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.label61);
            this.groupBox28.Controls.Add(this.RepresentativeOtherPhone);
            this.groupBox28.Controls.Add(this.label62);
            this.groupBox28.Controls.Add(this.RepresentativeOtherPhone_OtherText);
            this.groupBox28.Controls.Add(this.RepresentativeOtherPhone_Other);
            this.groupBox28.Controls.Add(this.RepresentativeOtherPhone_Message);
            this.groupBox28.Controls.Add(this.RepresentativeOtherPhone_Work);
            this.groupBox28.Controls.Add(this.RepresentativeOtherPhone_Cell);
            this.groupBox28.Controls.Add(this.RepresentativeOtherPhone_Home);
            this.groupBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox28.Location = new System.Drawing.Point(699, 111);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(219, 127);
            this.groupBox28.TabIndex = 16;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Representative’s Other Phone Number:";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label61.Location = new System.Drawing.Point(4, 52);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(78, 13);
            this.label61.TabIndex = 4;
            this.label61.Text = "This number is:";
            // 
            // RepresentativeOtherPhone
            // 
            this.RepresentativeOtherPhone.AccessibleDescription = "Representative’s Other Phone Number:";
            this.RepresentativeOtherPhone.Location = new System.Drawing.Point(54, 17);
            this.RepresentativeOtherPhone.Name = "RepresentativeOtherPhone";
            this.RepresentativeOtherPhone.Size = new System.Drawing.Size(159, 20);
            this.RepresentativeOtherPhone.TabIndex = 0;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(7, 20);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(47, 13);
            this.label62.TabIndex = 2;
            this.label62.Text = "Number:";
            // 
            // RepresentativeOtherPhone_OtherText
            // 
            this.RepresentativeOtherPhone_OtherText.Enabled = false;
            this.RepresentativeOtherPhone_OtherText.Location = new System.Drawing.Point(134, 92);
            this.RepresentativeOtherPhone_OtherText.Name = "RepresentativeOtherPhone_OtherText";
            this.RepresentativeOtherPhone_OtherText.Size = new System.Drawing.Size(79, 20);
            this.RepresentativeOtherPhone_OtherText.TabIndex = 6;
            // 
            // RepresentativeOtherPhone_Other
            // 
            this.RepresentativeOtherPhone_Other.AutoSize = true;
            this.RepresentativeOtherPhone_Other.Location = new System.Drawing.Point(82, 94);
            this.RepresentativeOtherPhone_Other.Name = "RepresentativeOtherPhone_Other";
            this.RepresentativeOtherPhone_Other.Size = new System.Drawing.Size(54, 17);
            this.RepresentativeOtherPhone_Other.TabIndex = 5;
            this.RepresentativeOtherPhone_Other.Text = "Other:";
            this.RepresentativeOtherPhone_Other.UseVisualStyleBackColor = true;
            this.RepresentativeOtherPhone_Other.CheckedChanged += new System.EventHandler(this.RepresentativeOtherPhone_Other_CheckedChanged);
            // 
            // RepresentativeOtherPhone_Message
            // 
            this.RepresentativeOtherPhone_Message.AutoSize = true;
            this.RepresentativeOtherPhone_Message.Location = new System.Drawing.Point(7, 94);
            this.RepresentativeOtherPhone_Message.Name = "RepresentativeOtherPhone_Message";
            this.RepresentativeOtherPhone_Message.Size = new System.Drawing.Size(68, 17);
            this.RepresentativeOtherPhone_Message.TabIndex = 4;
            this.RepresentativeOtherPhone_Message.Text = "Message";
            this.RepresentativeOtherPhone_Message.UseVisualStyleBackColor = true;
            // 
            // RepresentativeOtherPhone_Work
            // 
            this.RepresentativeOtherPhone_Work.AutoSize = true;
            this.RepresentativeOtherPhone_Work.Location = new System.Drawing.Point(134, 71);
            this.RepresentativeOtherPhone_Work.Name = "RepresentativeOtherPhone_Work";
            this.RepresentativeOtherPhone_Work.Size = new System.Drawing.Size(51, 17);
            this.RepresentativeOtherPhone_Work.TabIndex = 3;
            this.RepresentativeOtherPhone_Work.Text = "Work";
            this.RepresentativeOtherPhone_Work.UseVisualStyleBackColor = true;
            // 
            // RepresentativeOtherPhone_Cell
            // 
            this.RepresentativeOtherPhone_Cell.AutoSize = true;
            this.RepresentativeOtherPhone_Cell.Location = new System.Drawing.Point(82, 71);
            this.RepresentativeOtherPhone_Cell.Name = "RepresentativeOtherPhone_Cell";
            this.RepresentativeOtherPhone_Cell.Size = new System.Drawing.Size(42, 17);
            this.RepresentativeOtherPhone_Cell.TabIndex = 2;
            this.RepresentativeOtherPhone_Cell.Text = "Cell";
            this.RepresentativeOtherPhone_Cell.UseVisualStyleBackColor = true;
            // 
            // RepresentativeOtherPhone_Home
            // 
            this.RepresentativeOtherPhone_Home.AutoSize = true;
            this.RepresentativeOtherPhone_Home.Checked = true;
            this.RepresentativeOtherPhone_Home.Location = new System.Drawing.Point(7, 71);
            this.RepresentativeOtherPhone_Home.Name = "RepresentativeOtherPhone_Home";
            this.RepresentativeOtherPhone_Home.Size = new System.Drawing.Size(53, 17);
            this.RepresentativeOtherPhone_Home.TabIndex = 1;
            this.RepresentativeOtherPhone_Home.TabStop = true;
            this.RepresentativeOtherPhone_Home.Text = "Home";
            this.RepresentativeOtherPhone_Home.UseVisualStyleBackColor = true;
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.label60);
            this.groupBox27.Controls.Add(this.AuthorizedRepresentativePhoneNumber);
            this.groupBox27.Controls.Add(this.label23);
            this.groupBox27.Controls.Add(this.RepresentativePhone_OtherText);
            this.groupBox27.Controls.Add(this.RepresentativePhone_Other);
            this.groupBox27.Controls.Add(this.RepresentativePhone_Message);
            this.groupBox27.Controls.Add(this.RepresentativePhone_Work);
            this.groupBox27.Controls.Add(this.RepresentativePhone_Cell);
            this.groupBox27.Controls.Add(this.RepresentativePhone_Home);
            this.groupBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox27.Location = new System.Drawing.Point(474, 111);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(219, 127);
            this.groupBox27.TabIndex = 15;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Representative’s Phone Number: ";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label60.Location = new System.Drawing.Point(5, 52);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(78, 13);
            this.label60.TabIndex = 4;
            this.label60.Text = "This number is:";
            // 
            // AuthorizedRepresentativePhoneNumber
            // 
            this.AuthorizedRepresentativePhoneNumber.AccessibleDescription = "Representative’s Phone Number: ";
            this.AuthorizedRepresentativePhoneNumber.Location = new System.Drawing.Point(54, 17);
            this.AuthorizedRepresentativePhoneNumber.Name = "AuthorizedRepresentativePhoneNumber";
            this.AuthorizedRepresentativePhoneNumber.Size = new System.Drawing.Size(159, 20);
            this.AuthorizedRepresentativePhoneNumber.TabIndex = 0;
            this.AuthorizedRepresentativePhoneNumber.Tag = "1";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 20);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(47, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "Number:";
            // 
            // RepresentativePhone_OtherText
            // 
            this.RepresentativePhone_OtherText.Enabled = false;
            this.RepresentativePhone_OtherText.Location = new System.Drawing.Point(134, 92);
            this.RepresentativePhone_OtherText.Name = "RepresentativePhone_OtherText";
            this.RepresentativePhone_OtherText.Size = new System.Drawing.Size(79, 20);
            this.RepresentativePhone_OtherText.TabIndex = 6;
            // 
            // RepresentativePhone_Other
            // 
            this.RepresentativePhone_Other.AutoSize = true;
            this.RepresentativePhone_Other.Location = new System.Drawing.Point(82, 94);
            this.RepresentativePhone_Other.Name = "RepresentativePhone_Other";
            this.RepresentativePhone_Other.Size = new System.Drawing.Size(54, 17);
            this.RepresentativePhone_Other.TabIndex = 5;
            this.RepresentativePhone_Other.Text = "Other:";
            this.RepresentativePhone_Other.UseVisualStyleBackColor = true;
            this.RepresentativePhone_Other.CheckedChanged += new System.EventHandler(this.RepresentativePhone_Other_CheckedChanged);
            // 
            // RepresentativePhone_Message
            // 
            this.RepresentativePhone_Message.AutoSize = true;
            this.RepresentativePhone_Message.Location = new System.Drawing.Point(7, 94);
            this.RepresentativePhone_Message.Name = "RepresentativePhone_Message";
            this.RepresentativePhone_Message.Size = new System.Drawing.Size(68, 17);
            this.RepresentativePhone_Message.TabIndex = 4;
            this.RepresentativePhone_Message.Text = "Message";
            this.RepresentativePhone_Message.UseVisualStyleBackColor = true;
            // 
            // RepresentativePhone_Work
            // 
            this.RepresentativePhone_Work.AutoSize = true;
            this.RepresentativePhone_Work.Location = new System.Drawing.Point(134, 71);
            this.RepresentativePhone_Work.Name = "RepresentativePhone_Work";
            this.RepresentativePhone_Work.Size = new System.Drawing.Size(51, 17);
            this.RepresentativePhone_Work.TabIndex = 3;
            this.RepresentativePhone_Work.Text = "Work";
            this.RepresentativePhone_Work.UseVisualStyleBackColor = true;
            // 
            // RepresentativePhone_Cell
            // 
            this.RepresentativePhone_Cell.AutoSize = true;
            this.RepresentativePhone_Cell.Location = new System.Drawing.Point(82, 71);
            this.RepresentativePhone_Cell.Name = "RepresentativePhone_Cell";
            this.RepresentativePhone_Cell.Size = new System.Drawing.Size(42, 17);
            this.RepresentativePhone_Cell.TabIndex = 2;
            this.RepresentativePhone_Cell.Text = "Cell";
            this.RepresentativePhone_Cell.UseVisualStyleBackColor = true;
            // 
            // RepresentativePhone_Home
            // 
            this.RepresentativePhone_Home.AutoSize = true;
            this.RepresentativePhone_Home.Checked = true;
            this.RepresentativePhone_Home.Location = new System.Drawing.Point(7, 71);
            this.RepresentativePhone_Home.Name = "RepresentativePhone_Home";
            this.RepresentativePhone_Home.Size = new System.Drawing.Size(53, 17);
            this.RepresentativePhone_Home.TabIndex = 1;
            this.RepresentativePhone_Home.TabStop = true;
            this.RepresentativePhone_Home.Text = "Home";
            this.RepresentativePhone_Home.UseVisualStyleBackColor = true;
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.label59);
            this.groupBox26.Controls.Add(this.label58);
            this.groupBox26.Controls.Add(this.RepresentativeText_Number);
            this.groupBox26.Controls.Add(this.RepresentativeText_Yes);
            this.groupBox26.Controls.Add(this.RepresentativeEmailAddress);
            this.groupBox26.Controls.Add(this.RepresentativeEmail_Yes);
            this.groupBox26.Controls.Add(this.label57);
            this.groupBox26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox26.Location = new System.Drawing.Point(10, 155);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(447, 89);
            this.groupBox26.TabIndex = 9;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Get an alert that a letter is ready for viewing in HEAplus by:";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(71, 34);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(75, 13);
            this.label59.TabIndex = 6;
            this.label59.Text = "Email address:";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(71, 61);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(79, 13);
            this.label58.TabIndex = 5;
            this.label58.Text = "Number to text:";
            // 
            // RepresentativeText_Number
            // 
            this.RepresentativeText_Number.Enabled = false;
            this.RepresentativeText_Number.Location = new System.Drawing.Point(152, 58);
            this.RepresentativeText_Number.Name = "RepresentativeText_Number";
            this.RepresentativeText_Number.Size = new System.Drawing.Size(283, 20);
            this.RepresentativeText_Number.TabIndex = 3;
            // 
            // RepresentativeText_Yes
            // 
            this.RepresentativeText_Yes.AutoSize = true;
            this.RepresentativeText_Yes.Location = new System.Drawing.Point(14, 60);
            this.RepresentativeText_Yes.Name = "RepresentativeText_Yes";
            this.RepresentativeText_Yes.Size = new System.Drawing.Size(47, 17);
            this.RepresentativeText_Yes.TabIndex = 2;
            this.RepresentativeText_Yes.Text = "Text";
            this.RepresentativeText_Yes.UseVisualStyleBackColor = true;
            this.RepresentativeText_Yes.CheckedChanged += new System.EventHandler(this.RepresentativeText_Yes_CheckedChanged);
            // 
            // RepresentativeEmailAddress
            // 
            this.RepresentativeEmailAddress.Enabled = false;
            this.RepresentativeEmailAddress.Location = new System.Drawing.Point(152, 32);
            this.RepresentativeEmailAddress.Name = "RepresentativeEmailAddress";
            this.RepresentativeEmailAddress.Size = new System.Drawing.Size(283, 20);
            this.RepresentativeEmailAddress.TabIndex = 1;
            // 
            // RepresentativeEmail_Yes
            // 
            this.RepresentativeEmail_Yes.AutoSize = true;
            this.RepresentativeEmail_Yes.Location = new System.Drawing.Point(14, 34);
            this.RepresentativeEmail_Yes.Name = "RepresentativeEmail_Yes";
            this.RepresentativeEmail_Yes.Size = new System.Drawing.Size(54, 17);
            this.RepresentativeEmail_Yes.TabIndex = 0;
            this.RepresentativeEmail_Yes.Text = "Email:";
            this.RepresentativeEmail_Yes.UseVisualStyleBackColor = true;
            this.RepresentativeEmail_Yes.CheckedChanged += new System.EventHandler(this.RepresentativeEmail_Yes_CheckedChanged);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label57.Location = new System.Drawing.Point(5, 14);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(363, 13);
            this.label57.TabIndex = 0;
            this.label57.Text = "(Email and text alerts are not available for ALTCS applications)";
            // 
            // WitnessName
            // 
            this.WitnessName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.WitnessName.Location = new System.Drawing.Point(10, 374);
            this.WitnessName.Name = "WitnessName";
            this.WitnessName.Size = new System.Drawing.Size(241, 20);
            this.WitnessName.TabIndex = 8;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label56.Location = new System.Drawing.Point(8, 359);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(242, 13);
            this.label56.TabIndex = 14;
            this.label56.Text = "Name of witness (only need if signed with a mark):";
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.RepresentativeWritten_OtherText);
            this.groupBox25.Controls.Add(this.RepresentativeWritten_Other);
            this.groupBox25.Controls.Add(this.RepresentativeWritten_Spanish);
            this.groupBox25.Controls.Add(this.RepresentativeWritten_English);
            this.groupBox25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox25.Location = new System.Drawing.Point(11, 64);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(199, 85);
            this.groupBox25.TabIndex = 7;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "What is the representative’s preferred WRITTEN Language?";
            // 
            // RepresentativeWritten_OtherText
            // 
            this.RepresentativeWritten_OtherText.Enabled = false;
            this.RepresentativeWritten_OtherText.Location = new System.Drawing.Point(62, 54);
            this.RepresentativeWritten_OtherText.Name = "RepresentativeWritten_OtherText";
            this.RepresentativeWritten_OtherText.Size = new System.Drawing.Size(128, 20);
            this.RepresentativeWritten_OtherText.TabIndex = 1;
            // 
            // RepresentativeWritten_Other
            // 
            this.RepresentativeWritten_Other.AutoSize = true;
            this.RepresentativeWritten_Other.Location = new System.Drawing.Point(7, 55);
            this.RepresentativeWritten_Other.Name = "RepresentativeWritten_Other";
            this.RepresentativeWritten_Other.Size = new System.Drawing.Size(54, 17);
            this.RepresentativeWritten_Other.TabIndex = 2;
            this.RepresentativeWritten_Other.Text = "Other:";
            this.RepresentativeWritten_Other.UseVisualStyleBackColor = true;
            this.RepresentativeWritten_Other.CheckedChanged += new System.EventHandler(this.RepresentativeWritten_Other_CheckedChanged);
            // 
            // RepresentativeWritten_Spanish
            // 
            this.RepresentativeWritten_Spanish.AutoSize = true;
            this.RepresentativeWritten_Spanish.Location = new System.Drawing.Point(77, 32);
            this.RepresentativeWritten_Spanish.Name = "RepresentativeWritten_Spanish";
            this.RepresentativeWritten_Spanish.Size = new System.Drawing.Size(63, 17);
            this.RepresentativeWritten_Spanish.TabIndex = 1;
            this.RepresentativeWritten_Spanish.Text = "Spanish";
            this.RepresentativeWritten_Spanish.UseVisualStyleBackColor = true;
            // 
            // RepresentativeWritten_English
            // 
            this.RepresentativeWritten_English.AutoSize = true;
            this.RepresentativeWritten_English.Checked = true;
            this.RepresentativeWritten_English.Location = new System.Drawing.Point(7, 32);
            this.RepresentativeWritten_English.Name = "RepresentativeWritten_English";
            this.RepresentativeWritten_English.Size = new System.Drawing.Size(59, 17);
            this.RepresentativeWritten_English.TabIndex = 0;
            this.RepresentativeWritten_English.TabStop = true;
            this.RepresentativeWritten_English.Text = "English";
            this.RepresentativeWritten_English.UseVisualStyleBackColor = true;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.RepresentativeSpokoen_OtherText);
            this.groupBox24.Controls.Add(this.RepresentativeSpokoen_Other);
            this.groupBox24.Controls.Add(this.RepresentativeSpokoen_Spanish);
            this.groupBox24.Controls.Add(this.RepresentativeSpokoen_English);
            this.groupBox24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox24.Location = new System.Drawing.Point(215, 64);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(240, 84);
            this.groupBox24.TabIndex = 6;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "What is the representative’s preferred SPOKEN Language?";
            // 
            // RepresentativeSpokoen_OtherText
            // 
            this.RepresentativeSpokoen_OtherText.Enabled = false;
            this.RepresentativeSpokoen_OtherText.Location = new System.Drawing.Point(66, 52);
            this.RepresentativeSpokoen_OtherText.Name = "RepresentativeSpokoen_OtherText";
            this.RepresentativeSpokoen_OtherText.Size = new System.Drawing.Size(157, 20);
            this.RepresentativeSpokoen_OtherText.TabIndex = 3;
            // 
            // RepresentativeSpokoen_Other
            // 
            this.RepresentativeSpokoen_Other.AutoSize = true;
            this.RepresentativeSpokoen_Other.Location = new System.Drawing.Point(7, 53);
            this.RepresentativeSpokoen_Other.Name = "RepresentativeSpokoen_Other";
            this.RepresentativeSpokoen_Other.Size = new System.Drawing.Size(54, 17);
            this.RepresentativeSpokoen_Other.TabIndex = 2;
            this.RepresentativeSpokoen_Other.Text = "Other:";
            this.RepresentativeSpokoen_Other.UseVisualStyleBackColor = true;
            this.RepresentativeSpokoen_Other.CheckedChanged += new System.EventHandler(this.RepresentativeSpokoen_Other_CheckedChanged);
            // 
            // RepresentativeSpokoen_Spanish
            // 
            this.RepresentativeSpokoen_Spanish.AutoSize = true;
            this.RepresentativeSpokoen_Spanish.Location = new System.Drawing.Point(79, 32);
            this.RepresentativeSpokoen_Spanish.Name = "RepresentativeSpokoen_Spanish";
            this.RepresentativeSpokoen_Spanish.Size = new System.Drawing.Size(63, 17);
            this.RepresentativeSpokoen_Spanish.TabIndex = 1;
            this.RepresentativeSpokoen_Spanish.Text = "Spanish";
            this.RepresentativeSpokoen_Spanish.UseVisualStyleBackColor = true;
            // 
            // RepresentativeSpokoen_English
            // 
            this.RepresentativeSpokoen_English.AutoSize = true;
            this.RepresentativeSpokoen_English.Checked = true;
            this.RepresentativeSpokoen_English.Location = new System.Drawing.Point(7, 32);
            this.RepresentativeSpokoen_English.Name = "RepresentativeSpokoen_English";
            this.RepresentativeSpokoen_English.Size = new System.Drawing.Size(59, 17);
            this.RepresentativeSpokoen_English.TabIndex = 0;
            this.RepresentativeSpokoen_English.TabStop = true;
            this.RepresentativeSpokoen_English.Text = "English";
            this.RepresentativeSpokoen_English.UseVisualStyleBackColor = true;
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.IsRepresentativeYourLegalGuardian_No);
            this.groupBox23.Controls.Add(this.IsRepresentativeYourLegalGuardian_Yes);
            this.groupBox23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox23.Location = new System.Drawing.Point(10, 300);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(200, 49);
            this.groupBox23.TabIndex = 5;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Is representative your legal guardian?";
            // 
            // IsRepresentativeYourLegalGuardian_No
            // 
            this.IsRepresentativeYourLegalGuardian_No.AutoSize = true;
            this.IsRepresentativeYourLegalGuardian_No.Checked = true;
            this.IsRepresentativeYourLegalGuardian_No.Location = new System.Drawing.Point(59, 20);
            this.IsRepresentativeYourLegalGuardian_No.Name = "IsRepresentativeYourLegalGuardian_No";
            this.IsRepresentativeYourLegalGuardian_No.Size = new System.Drawing.Size(39, 17);
            this.IsRepresentativeYourLegalGuardian_No.TabIndex = 1;
            this.IsRepresentativeYourLegalGuardian_No.TabStop = true;
            this.IsRepresentativeYourLegalGuardian_No.Text = "No";
            this.IsRepresentativeYourLegalGuardian_No.UseVisualStyleBackColor = true;
            // 
            // IsRepresentativeYourLegalGuardian_Yes
            // 
            this.IsRepresentativeYourLegalGuardian_Yes.AutoSize = true;
            this.IsRepresentativeYourLegalGuardian_Yes.Location = new System.Drawing.Point(7, 20);
            this.IsRepresentativeYourLegalGuardian_Yes.Name = "IsRepresentativeYourLegalGuardian_Yes";
            this.IsRepresentativeYourLegalGuardian_Yes.Size = new System.Drawing.Size(43, 17);
            this.IsRepresentativeYourLegalGuardian_Yes.TabIndex = 0;
            this.IsRepresentativeYourLegalGuardian_Yes.Text = "Yes";
            this.IsRepresentativeYourLegalGuardian_Yes.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(10, 241);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(909, 11);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // LegalGuardianRelationShip
            // 
            this.LegalGuardianRelationShip.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LegalGuardianRelationShip.Location = new System.Drawing.Point(282, 273);
            this.LegalGuardianRelationShip.Name = "LegalGuardianRelationShip";
            this.LegalGuardianRelationShip.Size = new System.Drawing.Size(174, 20);
            this.LegalGuardianRelationShip.TabIndex = 4;
            this.LegalGuardianRelationShip.Tag = "";
            // 
            // LegalGuardianMailingAddress
            // 
            this.LegalGuardianMailingAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LegalGuardianMailingAddress.Location = new System.Drawing.Point(474, 273);
            this.LegalGuardianMailingAddress.Name = "LegalGuardianMailingAddress";
            this.LegalGuardianMailingAddress.Size = new System.Drawing.Size(325, 20);
            this.LegalGuardianMailingAddress.TabIndex = 17;
            this.LegalGuardianMailingAddress.Tag = "";
            // 
            // AuthorizedRepresentativeMailingAddress
            // 
            this.AuthorizedRepresentativeMailingAddress.AccessibleDescription = "Authorized Representative’s Mailing Address:";
            this.AuthorizedRepresentativeMailingAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AuthorizedRepresentativeMailingAddress.Location = new System.Drawing.Point(474, 38);
            this.AuthorizedRepresentativeMailingAddress.Name = "AuthorizedRepresentativeMailingAddress";
            this.AuthorizedRepresentativeMailingAddress.Size = new System.Drawing.Size(325, 20);
            this.AuthorizedRepresentativeMailingAddress.TabIndex = 10;
            this.AuthorizedRepresentativeMailingAddress.Tag = "1";
            // 
            // AuthorizedRepresentativeRelationship
            // 
            this.AuthorizedRepresentativeRelationship.AccessibleDescription = "Relationship to Customer:";
            this.AuthorizedRepresentativeRelationship.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AuthorizedRepresentativeRelationship.Location = new System.Drawing.Point(283, 38);
            this.AuthorizedRepresentativeRelationship.Name = "AuthorizedRepresentativeRelationship";
            this.AuthorizedRepresentativeRelationship.Size = new System.Drawing.Size(172, 20);
            this.AuthorizedRepresentativeRelationship.TabIndex = 2;
            this.AuthorizedRepresentativeRelationship.Tag = "1";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label34.Location = new System.Drawing.Point(471, 256);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(237, 13);
            this.label34.TabIndex = 8;
            this.label34.Text = "Legal Guardian’s/Conservator’s Mailing Address:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label28.Location = new System.Drawing.Point(473, 22);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(220, 13);
            this.label28.TabIndex = 8;
            this.label28.Text = "Authorized Representative’s Mailing Address:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(279, 259);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(124, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "Relationship to Customer";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(280, 24);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(127, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "Relationship to Customer:";
            // 
            // LegalGuardianZipCode
            // 
            this.LegalGuardianZipCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LegalGuardianZipCode.Location = new System.Drawing.Point(581, 319);
            this.LegalGuardianZipCode.Name = "LegalGuardianZipCode";
            this.LegalGuardianZipCode.Size = new System.Drawing.Size(49, 20);
            this.LegalGuardianZipCode.TabIndex = 20;
            this.LegalGuardianZipCode.Tag = "";
            // 
            // AuthorizedRepresentativeZipCode
            // 
            this.AuthorizedRepresentativeZipCode.AccessibleDescription = "Authorized Representative’s Zip code";
            this.AuthorizedRepresentativeZipCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AuthorizedRepresentativeZipCode.Location = new System.Drawing.Point(583, 82);
            this.AuthorizedRepresentativeZipCode.Name = "AuthorizedRepresentativeZipCode";
            this.AuthorizedRepresentativeZipCode.Size = new System.Drawing.Size(49, 20);
            this.AuthorizedRepresentativeZipCode.TabIndex = 13;
            this.AuthorizedRepresentativeZipCode.Tag = "1";
            // 
            // LegalGuardianNames
            // 
            this.LegalGuardianNames.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LegalGuardianNames.Location = new System.Drawing.Point(10, 273);
            this.LegalGuardianNames.Name = "LegalGuardianNames";
            this.LegalGuardianNames.Size = new System.Drawing.Size(264, 20);
            this.LegalGuardianNames.TabIndex = 3;
            this.LegalGuardianNames.Tag = "";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(8, 259);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(257, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "Name of the Customer’s Legal Guardian/Conservator";
            // 
            // LegalGuardianState
            // 
            this.LegalGuardianState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LegalGuardianState.Location = new System.Drawing.Point(474, 319);
            this.LegalGuardianState.Name = "LegalGuardianState";
            this.LegalGuardianState.Size = new System.Drawing.Size(102, 20);
            this.LegalGuardianState.TabIndex = 19;
            this.LegalGuardianState.Tag = "";
            // 
            // AuthorizedRepresentativeState
            // 
            this.AuthorizedRepresentativeState.AccessibleDescription = "Authorized Representative’s Mailing State";
            this.AuthorizedRepresentativeState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AuthorizedRepresentativeState.Location = new System.Drawing.Point(474, 82);
            this.AuthorizedRepresentativeState.Name = "AuthorizedRepresentativeState";
            this.AuthorizedRepresentativeState.Size = new System.Drawing.Size(100, 20);
            this.AuthorizedRepresentativeState.TabIndex = 12;
            this.AuthorizedRepresentativeState.Tag = "1";
            // 
            // AuthorizedRepresentativeNames
            // 
            this.AuthorizedRepresentativeNames.AccessibleDescription = "Name of the Customer’s Authorized Representative:";
            this.AuthorizedRepresentativeNames.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AuthorizedRepresentativeNames.Location = new System.Drawing.Point(11, 38);
            this.AuthorizedRepresentativeNames.Name = "AuthorizedRepresentativeNames";
            this.AuthorizedRepresentativeNames.Size = new System.Drawing.Size(264, 20);
            this.AuthorizedRepresentativeNames.TabIndex = 1;
            this.AuthorizedRepresentativeNames.Tag = "1";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label33.Location = new System.Drawing.Point(578, 302);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(52, 13);
            this.label33.TabIndex = 6;
            this.label33.Text = "Zip code:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(9, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(251, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Name of the Customer’s Authorized Representative:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label32.Location = new System.Drawing.Point(632, 302);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(81, 13);
            this.label32.TabIndex = 2;
            this.label32.Text = "Phone Number:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label27.Location = new System.Drawing.Point(580, 68);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(52, 13);
            this.label27.TabIndex = 6;
            this.label27.Text = "Zip code:";
            // 
            // LegalGuardianPhoneNumber
            // 
            this.LegalGuardianPhoneNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LegalGuardianPhoneNumber.Location = new System.Drawing.Point(635, 319);
            this.LegalGuardianPhoneNumber.Name = "LegalGuardianPhoneNumber";
            this.LegalGuardianPhoneNumber.Size = new System.Drawing.Size(107, 20);
            this.LegalGuardianPhoneNumber.TabIndex = 21;
            this.LegalGuardianPhoneNumber.Tag = "";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label31.Location = new System.Drawing.Point(473, 302);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(35, 13);
            this.label31.TabIndex = 6;
            this.label31.Text = "State:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label30.Location = new System.Drawing.Point(744, 302);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(80, 13);
            this.label30.TabIndex = 2;
            this.label30.Text = "E-Mail Address:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label26.Location = new System.Drawing.Point(471, 68);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(35, 13);
            this.label26.TabIndex = 6;
            this.label26.Text = "State:";
            // 
            // LegalGuardianEmailAddress
            // 
            this.LegalGuardianEmailAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LegalGuardianEmailAddress.Location = new System.Drawing.Point(747, 319);
            this.LegalGuardianEmailAddress.Name = "LegalGuardianEmailAddress";
            this.LegalGuardianEmailAddress.Size = new System.Drawing.Size(173, 20);
            this.LegalGuardianEmailAddress.TabIndex = 22;
            this.LegalGuardianEmailAddress.Tag = "";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(636, 68);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(80, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "E-Mail Address:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label29.Location = new System.Drawing.Point(802, 256);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(27, 13);
            this.label29.TabIndex = 4;
            this.label29.Text = "City:";
            // 
            // AuthorizedRepresentativeEmailAddress
            // 
            this.AuthorizedRepresentativeEmailAddress.AccessibleDescription = "Authorized Representative’s Address";
            this.AuthorizedRepresentativeEmailAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AuthorizedRepresentativeEmailAddress.Location = new System.Drawing.Point(639, 82);
            this.AuthorizedRepresentativeEmailAddress.Name = "AuthorizedRepresentativeEmailAddress";
            this.AuthorizedRepresentativeEmailAddress.Size = new System.Drawing.Size(160, 20);
            this.AuthorizedRepresentativeEmailAddress.TabIndex = 14;
            this.AuthorizedRepresentativeEmailAddress.Tag = "1";
            // 
            // LegalGuardianCity
            // 
            this.LegalGuardianCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LegalGuardianCity.Location = new System.Drawing.Point(805, 273);
            this.LegalGuardianCity.Name = "LegalGuardianCity";
            this.LegalGuardianCity.Size = new System.Drawing.Size(115, 20);
            this.LegalGuardianCity.TabIndex = 18;
            this.LegalGuardianCity.Tag = "";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(804, 22);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(27, 13);
            this.label25.TabIndex = 4;
            this.label25.Text = "City:";
            // 
            // AuthorizedRepresentativeCity
            // 
            this.AuthorizedRepresentativeCity.AccessibleDescription = "Authorized Representative’s Mailing City";
            this.AuthorizedRepresentativeCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AuthorizedRepresentativeCity.Location = new System.Drawing.Point(807, 38);
            this.AuthorizedRepresentativeCity.Name = "AuthorizedRepresentativeCity";
            this.AuthorizedRepresentativeCity.Size = new System.Drawing.Size(112, 20);
            this.AuthorizedRepresentativeCity.TabIndex = 11;
            this.AuthorizedRepresentativeCity.Tag = "1";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnClose.Location = new System.Drawing.Point(841, 427);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(114, 25);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(720, 427);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(114, 25);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // progressBarText
            // 
            this.progressBarText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarText.AutoSize = true;
            this.progressBarText.Location = new System.Drawing.Point(26, 435);
            this.progressBarText.Name = "progressBarText";
            this.progressBarText.Size = new System.Drawing.Size(52, 13);
            this.progressBarText.TabIndex = 19;
            this.progressBarText.Text = "_           _";
            this.progressBarText.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(12, 429);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(702, 23);
            this.progressBar.TabIndex = 18;
            this.progressBar.Visible = false;
            // 
            // FormRepresentative
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(967, 458);
            this.Controls.Add(this.progressBarText);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.groupAuthorizedRepresentative);
            this.Name = "FormRepresentative";
            this.Text = "Representative";
            this.Load += new System.EventHandler(this.FormRepresentative_Load);
            this.groupAuthorizedRepresentative.ResumeLayout(false);
            this.groupAuthorizedRepresentative.PerformLayout();
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupAuthorizedRepresentative;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox RepresentativeOtherPhone;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox RepresentativeOtherPhone_OtherText;
        private System.Windows.Forms.RadioButton RepresentativeOtherPhone_Other;
        private System.Windows.Forms.RadioButton RepresentativeOtherPhone_Message;
        private System.Windows.Forms.RadioButton RepresentativeOtherPhone_Work;
        private System.Windows.Forms.RadioButton RepresentativeOtherPhone_Cell;
        private System.Windows.Forms.RadioButton RepresentativeOtherPhone_Home;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox AuthorizedRepresentativePhoneNumber;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox RepresentativePhone_OtherText;
        private System.Windows.Forms.RadioButton RepresentativePhone_Other;
        private System.Windows.Forms.RadioButton RepresentativePhone_Message;
        private System.Windows.Forms.RadioButton RepresentativePhone_Work;
        private System.Windows.Forms.RadioButton RepresentativePhone_Cell;
        private System.Windows.Forms.RadioButton RepresentativePhone_Home;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox RepresentativeText_Number;
        private System.Windows.Forms.CheckBox RepresentativeText_Yes;
        private System.Windows.Forms.TextBox RepresentativeEmailAddress;
        private System.Windows.Forms.CheckBox RepresentativeEmail_Yes;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox WitnessName;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.TextBox RepresentativeWritten_OtherText;
        private System.Windows.Forms.RadioButton RepresentativeWritten_Other;
        private System.Windows.Forms.RadioButton RepresentativeWritten_Spanish;
        private System.Windows.Forms.RadioButton RepresentativeWritten_English;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.TextBox RepresentativeSpokoen_OtherText;
        private System.Windows.Forms.RadioButton RepresentativeSpokoen_Other;
        private System.Windows.Forms.RadioButton RepresentativeSpokoen_Spanish;
        private System.Windows.Forms.RadioButton RepresentativeSpokoen_English;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.RadioButton IsRepresentativeYourLegalGuardian_No;
        private System.Windows.Forms.RadioButton IsRepresentativeYourLegalGuardian_Yes;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox LegalGuardianRelationShip;
        private System.Windows.Forms.TextBox LegalGuardianMailingAddress;
        private System.Windows.Forms.TextBox AuthorizedRepresentativeMailingAddress;
        private System.Windows.Forms.TextBox AuthorizedRepresentativeRelationship;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox LegalGuardianZipCode;
        private System.Windows.Forms.TextBox AuthorizedRepresentativeZipCode;
        private System.Windows.Forms.TextBox LegalGuardianNames;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox LegalGuardianState;
        private System.Windows.Forms.TextBox AuthorizedRepresentativeState;
        private System.Windows.Forms.TextBox AuthorizedRepresentativeNames;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox LegalGuardianPhoneNumber;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox LegalGuardianEmailAddress;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox AuthorizedRepresentativeEmailAddress;
        private System.Windows.Forms.TextBox LegalGuardianCity;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox AuthorizedRepresentativeCity;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label progressBarText;
        private System.Windows.Forms.ProgressBar progressBar;
    }
}