﻿using ApplicantsApp.UserControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IdGen;

namespace ApplicantsApp.Forms
{
    public partial class FormRFI : Form
    {
        private static Random rand = new Random();
        private static IdGenerator generator = new IdGenerator(rand.Next(1000));

        public FormRFI()
        {
            InitializeComponent();
        }

        private void AddNewTask_Click(object sender, EventArgs e)
        {
            long newTaskId = generator.CreateId();
            AddTask(newTaskId);
        }

        private void AddTask(long taskId)
        {
            var task = new RFIGroupTemplate(taskId)
            {
                Width = 515,
                Height = 190,
            };
            flowPanel.Controls.Add(task);

            GroupBox gr = GetGroupBox(taskId, task);

            ((Button)gr.Controls["btnEditTask_" + taskId]).Click += new EventHandler(ButtonEditTask_Click);
            ((Button)gr.Controls["btnDeleteTask_" + taskId]).Click += new EventHandler(ButtonDeleteTask_Click);

            ((Button)gr.Controls["btnDeleteFile_" + taskId]).Click += new EventHandler(ButtonDeleteFile_Click);
            ((Button)gr.Controls["btnAddFile_" + taskId]).Click += new EventHandler(ButtonAddFile_Click);
            ((Button)gr.Controls["btnViewFile_" + taskId]).Click += new EventHandler(ButtonViewFile_Click);

            ((TextBox)gr.Controls["textTaskNotes_" + taskId]).TextChanged += new EventHandler(TaskNotes_TextChange);
            ((TextBox)gr.Controls["textTaskName_" + taskId]).TextChanged += new EventHandler(TaskName_TextChange);

            ((CheckBox)gr.Controls["checkTaskIsDone_" + taskId]).CheckedChanged += new EventHandler(TaskIsDone_Changed);

            
        }

        private GroupBox GetGroupBox(long taskId, RFIGroupTemplate task)
        {
            return (GroupBox)task.Controls["groupTaskTemplate_" + taskId];
        }

        private void SetPendingChanges(bool areThereChanges, long taskId, GroupBox gr)
        {
            CheckBox taskSelector = (CheckBox)gr.Controls["checkTaskSelected_" + taskId];
            if (!areThereChanges)
            {
                taskSelector.Text = "Select Task";
            } else
            {
                taskSelector.Text = "Select Task    (Pending changes)";
            }
        }

        private void ButtonEditTask_Click(object sender, EventArgs e)
        {
            long taskId = (long)((Control)sender).Tag;
            SetPendingChanges(false, taskId, (GroupBox)((Control)sender).Parent);
        }

        private void ButtonViewFile_Click(object sender, EventArgs e)
        {
            long taskId = (long)((Control)sender).Tag;
        }

        private void ButtonDeleteTask_Click(object sender, EventArgs e)
        {
            long taskId = (long)((Control)sender).Tag;
        }

        private void TaskIsDone_Changed(object sender, EventArgs e)
        {
            long taskId = (long)((Control)sender).Tag;
        }

        private void ButtonAddFile_Click(object sender, EventArgs e)
        {
            long taskId = (long)((Control)sender).Tag;
        }

        private void ButtonDeleteFile_Click(object sender, EventArgs e)
        {
            long taskId = (long)((Control)sender).Tag;
        }

        private void TaskNotes_TextChange(object sender, EventArgs e)
        {
            long taskId = (long)((Control)sender).Tag;
            SetPendingChanges(true, taskId, (GroupBox)((Control)sender).Parent);
        }

        private void TaskName_TextChange(object sender, EventArgs e)
        {
            long taskId = (long)((Control)sender).Tag;
            SetPendingChanges(true, taskId, (GroupBox)((Control)sender).Parent);
        }
    }
}
