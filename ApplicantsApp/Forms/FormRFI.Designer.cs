﻿namespace ApplicantsApp.Forms
{
    partial class FormRFI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.groupTaskTemplate = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.textTaskNotes = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkedListAttachedFiles = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnDeleteFile = new System.Windows.Forms.Button();
            this.btnAddFile = new System.Windows.Forms.Button();
            this.checkTaskIsDone = new System.Windows.Forms.CheckBox();
            this.btnDeleteTask = new System.Windows.Forms.Button();
            this.btnEditTask = new System.Windows.Forms.Button();
            this.btnViewFile = new System.Windows.Forms.Button();
            this.AddNewTask = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.comboTask = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textTaskName = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupTaskTemplate.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowPanel
            // 
            this.flowPanel.AutoScroll = true;
            this.flowPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowPanel.Location = new System.Drawing.Point(12, 12);
            this.flowPanel.Name = "flowPanel";
            this.flowPanel.Size = new System.Drawing.Size(540, 550);
            this.flowPanel.TabIndex = 0;
            this.flowPanel.WrapContents = false;
            // 
            // groupTaskTemplate
            // 
            this.groupTaskTemplate.Controls.Add(this.textTaskName);
            this.groupTaskTemplate.Controls.Add(this.label4);
            this.groupTaskTemplate.Controls.Add(this.label3);
            this.groupTaskTemplate.Controls.Add(this.comboTask);
            this.groupTaskTemplate.Controls.Add(this.btnViewFile);
            this.groupTaskTemplate.Controls.Add(this.checkTaskIsDone);
            this.groupTaskTemplate.Controls.Add(this.btnAddFile);
            this.groupTaskTemplate.Controls.Add(this.btnEditTask);
            this.groupTaskTemplate.Controls.Add(this.btnDeleteTask);
            this.groupTaskTemplate.Controls.Add(this.btnDeleteFile);
            this.groupTaskTemplate.Controls.Add(this.label2);
            this.groupTaskTemplate.Controls.Add(this.checkedListAttachedFiles);
            this.groupTaskTemplate.Controls.Add(this.label1);
            this.groupTaskTemplate.Controls.Add(this.textTaskNotes);
            this.groupTaskTemplate.Controls.Add(this.checkBox1);
            this.groupTaskTemplate.Location = new System.Drawing.Point(3, 24);
            this.groupTaskTemplate.Name = "groupTaskTemplate";
            this.groupTaskTemplate.Size = new System.Drawing.Size(503, 184);
            this.groupTaskTemplate.TabIndex = 1;
            this.groupTaskTemplate.TabStop = false;
            this.groupTaskTemplate.Visible = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(7, 0);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(83, 17);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "Select Task";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // textTaskNotes
            // 
            this.textTaskNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textTaskNotes.Location = new System.Drawing.Point(210, 82);
            this.textTaskNotes.Multiline = true;
            this.textTaskNotes.Name = "textTaskNotes";
            this.textTaskNotes.Size = new System.Drawing.Size(283, 64);
            this.textTaskNotes.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(207, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Task notes:";
            // 
            // checkedListAttachedFiles
            // 
            this.checkedListAttachedFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkedListAttachedFiles.FormattingEnabled = true;
            this.checkedListAttachedFiles.Items.AddRange(new object[] {
            "File1.jpg",
            "SomethingScanned.pdf",
            "some-other-file.pdf",
            "something-else.pdf",
            "scheme.xml"});
            this.checkedListAttachedFiles.Location = new System.Drawing.Point(6, 82);
            this.checkedListAttachedFiles.Name = "checkedListAttachedFiles";
            this.checkedListAttachedFiles.Size = new System.Drawing.Size(198, 64);
            this.checkedListAttachedFiles.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Attached files:";
            // 
            // btnDeleteFile
            // 
            this.btnDeleteFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeleteFile.Location = new System.Drawing.Point(7, 152);
            this.btnDeleteFile.Name = "btnDeleteFile";
            this.btnDeleteFile.Size = new System.Drawing.Size(47, 23);
            this.btnDeleteFile.TabIndex = 5;
            this.btnDeleteFile.Text = "Delete";
            this.btnDeleteFile.UseVisualStyleBackColor = true;
            // 
            // btnAddFile
            // 
            this.btnAddFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddFile.Location = new System.Drawing.Point(60, 152);
            this.btnAddFile.Name = "btnAddFile";
            this.btnAddFile.Size = new System.Drawing.Size(47, 23);
            this.btnAddFile.TabIndex = 6;
            this.btnAddFile.Text = "Add";
            this.btnAddFile.UseVisualStyleBackColor = true;
            // 
            // checkTaskIsDone
            // 
            this.checkTaskIsDone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkTaskIsDone.AutoSize = true;
            this.checkTaskIsDone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkTaskIsDone.Location = new System.Drawing.Point(210, 155);
            this.checkTaskIsDone.Name = "checkTaskIsDone";
            this.checkTaskIsDone.Size = new System.Drawing.Size(87, 17);
            this.checkTaskIsDone.TabIndex = 7;
            this.checkTaskIsDone.Text = "Task is done";
            this.checkTaskIsDone.UseVisualStyleBackColor = true;
            // 
            // btnDeleteTask
            // 
            this.btnDeleteTask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeleteTask.Location = new System.Drawing.Point(420, 151);
            this.btnDeleteTask.Name = "btnDeleteTask";
            this.btnDeleteTask.Size = new System.Drawing.Size(74, 23);
            this.btnDeleteTask.TabIndex = 5;
            this.btnDeleteTask.Text = "Delete task";
            this.btnDeleteTask.UseVisualStyleBackColor = true;
            // 
            // btnEditTask
            // 
            this.btnEditTask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEditTask.Location = new System.Drawing.Point(340, 151);
            this.btnEditTask.Name = "btnEditTask";
            this.btnEditTask.Size = new System.Drawing.Size(74, 23);
            this.btnEditTask.TabIndex = 5;
            this.btnEditTask.Text = "Edit task";
            this.btnEditTask.UseVisualStyleBackColor = true;
            // 
            // btnViewFile
            // 
            this.btnViewFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnViewFile.Location = new System.Drawing.Point(113, 152);
            this.btnViewFile.Name = "btnViewFile";
            this.btnViewFile.Size = new System.Drawing.Size(47, 23);
            this.btnViewFile.TabIndex = 8;
            this.btnViewFile.Text = "View";
            this.btnViewFile.UseVisualStyleBackColor = true;
            // 
            // AddNewTask
            // 
            this.AddNewTask.Location = new System.Drawing.Point(360, 571);
            this.AddNewTask.Name = "AddNewTask";
            this.AddNewTask.Size = new System.Drawing.Size(98, 23);
            this.AddNewTask.TabIndex = 2;
            this.AddNewTask.Text = "Add new task";
            this.AddNewTask.UseVisualStyleBackColor = true;
            this.AddNewTask.Click += new System.EventHandler(this.AddNewTask_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(464, 571);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(88, 23);
            this.button6.TabIndex = 3;
            this.button6.Text = "Cancel";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // comboTask
            // 
            this.comboTask.FormattingEnabled = true;
            this.comboTask.Location = new System.Drawing.Point(8, 36);
            this.comboTask.Name = "comboTask";
            this.comboTask.Size = new System.Drawing.Size(196, 21);
            this.comboTask.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Category:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(207, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Task name:";
            // 
            // textTaskName
            // 
            this.textTaskName.Location = new System.Drawing.Point(210, 36);
            this.textTaskName.Name = "textTaskName";
            this.textTaskName.Size = new System.Drawing.Size(283, 20);
            this.textTaskName.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 571);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Edit task categories";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // FormRFI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 603);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.AddNewTask);
            this.Controls.Add(this.flowPanel);
            this.Controls.Add(this.groupTaskTemplate);
            this.Name = "FormRFI";
            this.Text = "RFI";
            this.groupTaskTemplate.ResumeLayout(false);
            this.groupTaskTemplate.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowPanel;
        private System.Windows.Forms.GroupBox groupTaskTemplate;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textTaskNotes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox checkedListAttachedFiles;
        private System.Windows.Forms.Button btnAddFile;
        private System.Windows.Forms.Button btnDeleteFile;
        private System.Windows.Forms.CheckBox checkTaskIsDone;
        private System.Windows.Forms.Button btnEditTask;
        private System.Windows.Forms.Button btnDeleteTask;
        private System.Windows.Forms.Button btnViewFile;
        private System.Windows.Forms.Button AddNewTask;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox textTaskName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboTask;
        private System.Windows.Forms.Button button1;
    }
}