﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

            if (txtGmailAccount.Text.Length < 5)
            {
                MessageBox.Show("The user name is too short!");
                if (txtGmailAccount.CanFocus)
                    txtGmailAccount.Focus();

                return;
            }

            if (txtPassword.Text.Length < 4)
            {
                MessageBox.Show("The password is too short!");
                if (txtPassword.CanFocus)
                    txtPassword.Focus();

                return;
            }

            try
            {
                Db.Login(txtGmailAccount.Text, txtPassword.Text, logger);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show(ex.Message);
                return;
            }

            //if (chkRememberMe.Checked)
            //    WriteSessionId();

            HandleMainForm();
        }

        public void HandleMainForm()
        {
            this.Visible = false;

            FormApplicantsRegister main = new FormApplicantsRegister();            

            var res = main.ShowDialog();
            //if (res == DialogResult.Cancel)
                Close();

            if (res == DialogResult.Abort)
            {
                //if (System.IO.File.Exists("cookies"))
                //    System.IO.File.Delete("cookies");

                txtGmailAccount.Text = "";
                txtPassword.Text = "";
                this.Visible = true;
            }
        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnLogin_Click(null, null);
            }
        }
    }
}