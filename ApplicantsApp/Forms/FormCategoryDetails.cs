﻿using ApplicantsApp.Source;
using Newtonsoft.Json;
using Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp.Forms
{
    public partial class FormCategoryDetails : Form
    {
        public bool NeedRefresh { get; private set; } = false;
        public Models.FormModes FormMode { get; set; }
        public Applicants_DocumentCategories Category { get; set; }

        private NLog.Logger logger = NLog.LogManager.GetLogger("formCategory");
        private Source.Progress progress;

        public FormCategoryDetails()
        {
            InitializeComponent();
        }

        private void FormCategoryDetails_Load(object sender, EventArgs e)
        {
            progress = new Progress(progressBar, progressBarText, this, logger);

            if (FormMode == Models.FormModes.New)
            {
                Category = new Applicants_DocumentCategories();
            }

            if (FormMode == Models.FormModes.Edit)
            {
                categoryName.Text = Category.Category;
                categoryDesc.Text = Category.Description;
                categoryPriority.Value = Category.Priority; 
            }

            RespectMode();
        }

        private void RespectMode()
        {
            switch (FormMode)
            {
                case Models.FormModes.New:
                    Text = $"Category [ {FormMode.ToString().ToUpperInvariant()} ]";
                    break;

                case Models.FormModes.Edit:
                    Text = $"Category  '{Category.Category}'    [ {FormMode.ToString().ToUpperInvariant()} ]";
                    break;

                default: throw new Exception($"Unsupported form mode {FormMode}");
            }

            btnSave.Visible = FormMode != Models.FormModes.View;
        }

        private bool IsFormValid()
        {
            if (!Mop.ValidateControl(
                        string.IsNullOrWhiteSpace(categoryName.Text.Trim()),
                        "Category Name",
                        categoryName)

                || !Mop.ValidateControl(
                        string.IsNullOrWhiteSpace(categoryDesc.Text.Trim()),
                        "Category Description",
                        categoryDesc))
            {
                return false;
            }
            
            if (!Db.IsCategoryNameUnique(categoryName.Text.Trim(), Category.Id, logger))
            {
                MessageBox.Show("This category name already exists!");
                return false;
            }

            return true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!IsFormValid())
                return;

            progress.ShowProgressBar("Saving Category");
            try
            {
                Category.Category = categoryName.Text;
                Category.Description = categoryDesc.Text;
                Category.Priority = (int)categoryPriority.Value;

                switch (FormMode)
                {
                    case Models.FormModes.New:
                        Category = Db.CreateCategory(Category, logger);
                        break;

                    case Models.FormModes.Edit:
                        Db.UpdateCategory(Category, logger);
                        break;

                    default:
                        throw new Exception($"Unexpected form mode: {FormMode}");
                }

                progress.SetProgressText("Add audit data");
                Db.InsertAction(
                    logger,
                    Guid.Empty,
                    FormMode == Models.FormModes.Edit ? Models.ReportActions.CategoryEdit : Models.ReportActions.CategoryCreate,
                    correlationId: null,
                    data: JsonConvert.SerializeObject(Category));
            }
            finally
            {
                progress.HideProgressBar();
            }

            MessageBox.Show("The category was stored successfully!");
            if (FormMode == Models.FormModes.New)
            {
                FormMode = Models.FormModes.Edit;
                Category = Db.GetCategory(Category.Id, logger);

                RespectMode();
            }

            NeedRefresh = true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormCategoryDetails_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (NeedRefresh)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                DialogResult = DialogResult.Abort;
            }
        }
    }
}