﻿using ApplicantsApp.Forms;
using ApplicantsApp.Models;
using ApplicantsApp.Source;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using Shared;
using Shared.Source;
using System.Linq;

namespace ApplicantsApp
{
    public partial class FormApplicantsRegister : Form
    {
        public NLog.Logger logger = NLog.LogManager.GetLogger("main");

        List<Applicants_NomStatuses> statuses;

        private Source.Progress progress;

        public FormApplicantsRegister()
        {
            InitializeComponent();
            //entityServerModeSource1.QueryableSource = new ApplicantsApp.DbModel().viewApplicants;
            // This line of code is generated by Data Source Configuration Wizard
            //entityServerModeSource2.QueryableSource = new ApplicantsApp.DbModel().viewApplicants;
            // This line of code is generated by Data Source Configuration Wizard
            entityServerModeSource1.QueryableSource = new DbModel().viewApplicants;
            RefreshApplicants();
            FitColumns();
        }

        private void FitColumns()
        {
            //GridView gridView = gridApplicants.FocusedView as GridView;
            //gridView.BestFitColumns();

            gridView1.OptionsView.ColumnAutoWidth = false;
            gridView1.BestFitColumns();
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo viewInfo = gridView1.GetViewInfo() as DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo;
            if (viewInfo.ViewRects.ColumnTotalWidth < viewInfo.ViewRects.ColumnPanelWidth)
                gridView1.OptionsView.ColumnAutoWidth = true;
        }

        //private void LoadApplicants(Models.ApplicantStatusType applicantStatusType)        

        private void HandleApplicant(Models.FormModes formMode, ApplicantsNew applicantData)
        {
            var applicantInfo = new FormApplicantInfo()
            {
                FormMode = formMode,
                ApplicantData = applicantData
            };

            DialogResult res = applicantInfo.ShowDialog();
            RefreshApplicants();
        }

        private void RefreshApplicants()
        {
            entityServerModeSource1.QueryableSource = new DbModel().viewApplicants;
            entityServerModeSource1.Reload();
            FitColumns();
        }

        private void btnStartNewApp_Click(object sender, EventArgs e)
        {
            HandleApplicant(FormModes.New, null);
        }

        private Guid? GetSelectedApplicantId()
        {
            GridView gridView = (gridApplicants.FocusedView as GridView);
            if (gridView == null) return null;

            viewApplicants selectedRowData = (viewApplicants)gridView.GetRow(gridView.FocusedRowHandle);
            if (selectedRowData == null) return null;

            return selectedRowData.id;
        }

        private viewApplicants GetSelectedApplicantObject()
        {
            GridView gridView = (gridApplicants.FocusedView as GridView);
            if (gridView == null) return null;

            viewApplicants selectedRowData = (viewApplicants)gridView.GetRow(gridView.FocusedRowHandle);
            if (selectedRowData == null) return null;

            return selectedRowData;
        }

        private void EditApplicant()
        {
            Guid? applicantId = GetSelectedApplicantId();
            if (applicantId is null) return;

            ApplicantsNew applicantData;

            progress.ShowProgressBar("Loading applicant data");
            try
            {
                applicantData = Db.LoadApplicant(applicantId.Value, logger);

                Db.InsertAction(logger, applicantData.Id, Models.ReportActions.ApplicantRead, applicantData.Id, JsonConvert.SerializeObject(applicantData));
            }
            finally
            {
                progress.HideProgressBar();
            }

            HandleApplicant(Models.FormModes.Edit, applicantData);
        }

        private void FormApplicantsRegister_Load(object sender, EventArgs e)
        {
            RefreshStatuses();

            Text = $"Hello, {Db.user.RealName}, this is the Applicant Register, v{Application.ProductVersion}";

            toolAudit.Enabled = Db.user.IsAdmin == 1;
            auditToolStripMenuItem.Enabled = Db.user.IsAdmin == 1;

            progress = new Progress(progressBar, progressBarText, this, logger);
        }

        private void RefreshStatuses()
        {
            statuses = Db.GetAllStatuses(logger);
            foreach (var staus in statuses)
            {
                var item = new ToolStripMenuItem()
                {
                    Text = staus.StatusName,
                    Tag = staus.StatusId,
                };

                item.Click += new EventHandler(changeDStatusToolStripMenuItem_Click);

                changeStatusToolStripMenuItem.DropDownItems.Add(item);
            }
        }

        private void gridApplicants_DoubleClick(object sender, EventArgs e)
        {
            EditApplicant();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private bool ChangeApplicantStatus(int applicantStatusTypeId)
        {
            Guid? applicantId = GetSelectedApplicantId();
            if (applicantId is null) return false;

            progress.ShowProgressBar("Changing status");
            try
            {
                Db.ApplicantChangeStatus(applicantId.Value, applicantStatusTypeId, logger);
                RefreshApplicants();

                //if the applicant changes his status - the auto emails for PAS stops, if any
                Db.DeleteAllScheduledEmails(applicantId.Value, logger, Models.Communication.EmailTypes.PasAssssorAutoRequest);
            }
            finally
            {
                progress.HideProgressBar();
            }

            return true;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            HandleApplicant(Models.FormModes.New, null);
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            EditApplicant();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HandleApplicant(Models.FormModes.New, null);
        }

        private void toolAudit_Click(object sender, EventArgs e)
        {
            ShowAuditForm();
        }

        private void auditToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowAuditForm();
        }

        private void ShowAuditForm()
        {
            var audit = new FormAudit();
            audit.ShowDialog();
        }

        private void representativesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadRepresentrativesCatalog();
        }

        private void toolRep_Click(object sender, EventArgs e)
        {
            LoadRepresentrativesCatalog();
        }

        private void LoadRepresentrativesCatalog()
        {
            using (var formCatalog = new FormRepresentativesCatalog())
            {
                DialogResult res = formCatalog.ShowDialog();
            }
        }

        private void ToolDoctorsRegister_Click(object sender, EventArgs e)
        {
            using (var doctorsRegister = new FormDoctorsRegister())
            {
                doctorsRegister.ShowDialog();
            }
        }

        private void SimpleButton1_Click(object sender, EventArgs e)
        {

        }

        private void ToolDeleteApplicant_Click(object sender, EventArgs e)
        {
            //
        }

        private void ToolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void StripStatusChange_Scheduled_Click(object sender, EventArgs e)
        {
            Guid? applicantId = GetSelectedApplicantId();
            if (applicantId is null) return;

            ApplicantsNew applicantData;

            progress.ShowProgressBar("Loading applicant data");
            try
            {
                applicantData = Db.LoadApplicant(applicantId.Value, logger);
            }
            finally
            {
                progress.HideProgressBar();
            }

            FormScheduleUser formSchedule = new FormScheduleUser()
            {
                ApplicantData = applicantData,
                FormMode = Models.FormModes.Edit
            };

            if (formSchedule.ShowDialog() == DialogResult.OK)
            {
                //TODO: change to scheduled
                ChangeApplicantStatus(5); //TODO: change to enum or something
            }
        }

        private void ToolStripDropDownButton1_Click(object sender, EventArgs e)
        {

        }

        private void ToolStripRFI_Click(object sender, EventArgs e)
        {

        }

        private void ToolStripButton2_Click_1(object sender, EventArgs e)
        {
            var formRfi = new FormRFI();
            formRfi.ShowDialog();
        }

        private void BtnManualRefresh_Click(object sender, EventArgs e)
        {
            RefreshApplicants();
        }

        private void changeDStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int statusId = (int)((ToolStripMenuItem)sender).Tag;

            ChangeApplicantStatus(statusId);
        }

        private void changeDueDateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            viewApplicants applicantObject = GetSelectedApplicantObject();
            if (applicantObject is null) return;

            using (var dueDatePickForm = new FormDatePicker()
            {
                CurrentDate = applicantObject.dueDate ?? DateTime.Now,
                LabelCaption = "Pick Due Date"
            })
            {
                if (dueDatePickForm.ShowDialog() == DialogResult.OK)
                {
                    progress.ShowProgressBar("Setting due date...");
                    try
                    {
                        Db.ChangeApplicantDueDate(
                            applicantObject.id,
                            dueDatePickForm.CurrentDate,
                            logger);

                        Db.InsertAction(
                            logger,
                            applicantObject.id,
                            Models.ReportActions.ApplicantChangeDueDate,
                            null,
                            dueDatePickForm.CurrentDate.ToString(Source.Resources.USADateTime)
                        );

                        applicantObject.dueDate = dueDatePickForm.CurrentDate;
                        //entityServerModeSource6.Reload();
                    }
                    finally
                    {
                        progress.HideProgressBar();
                    }
                }
            }
        }

        private void toolStripCategoriesRegister_Click(object sender, EventArgs e)
        {
            using (var form = new FormCategoriesRegister())
            {
                form.ShowDialog();
            }
        }

        private void editTransferToolStripMenuItem_Click(object sender, EventArgs e)
        {
            viewApplicants applicantObject = GetSelectedApplicantObject();
            if (applicantObject is null) return;

            using (var formTransferSumPick = new FormDecimalPicker()
            {
                Value = applicantObject.transfers ?? 0,
                LabelCaption = "Enter Transfer sum:",
                Text = "Transfer Sum",
                FormMode = FormModes.Edit
            })
            {
                if (formTransferSumPick.ShowDialog() == DialogResult.OK)
                {
                    progress.ShowProgressBar("Setting transfer sum...");
                    try
                    {
                        Db.ChangeApplicantTransferSum(
                            applicantObject.id,
                            formTransferSumPick.Value,
                            logger);

                        applicantObject.transfers = formTransferSumPick.Value;
                    }
                    finally
                    {
                        progress.HideProgressBar();
                    }
                }
            }
        }

        private void changeDateOfTransferToolStripMenuItem_Click(object sender, EventArgs e)
        {
            viewApplicants applicantObject = GetSelectedApplicantObject();
            if (applicantObject is null) return;

            using (var formTransferDateChange = new FormDatePicker()
            {
                CurrentDate = applicantObject.transferDate ?? DateTime.Now,
                LabelCaption = "Enter Transfer date:",
                Text = "Transfer Date"
            })
            {
                if (formTransferDateChange.ShowDialog() == DialogResult.OK)
                {
                    progress.ShowProgressBar("Setting transfer date...");
                    try
                    {
                        Db.ChangeApplicantTransferDate(
                            applicantObject.id,
                            formTransferDateChange.CurrentDate,
                            logger);

                        applicantObject.transferDate = formTransferDateChange.CurrentDate;
                    }
                    finally
                    {
                        progress.HideProgressBar();
                    }
                }
            }
        }

        private void changeFinancialInterviewDateToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void noneToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            viewApplicants applicantObject = GetSelectedApplicantObject();
            if (applicantObject is null) return;

            string text;
            int tagId = int.Parse((sender as ToolStripMenuItem).Tag.ToString());
            switch (tagId)
            {
                case 0:
                    text = null;
                    break;

                case 1:
                    text = "Unsent";
                    break;

                case 2:
                    text = "Sent";
                    break;

                case 3:
                    text = "Received";
                    break;

                default: throw new Exception("Income Only Trust tag unknown entered " + tagId);
            }


            progress.ShowProgressBar("Setting Income Only Trust...");
            try
            {
                Db.ChangeIncomeOnlyTrust(
                    applicantObject.id,
                    text,
                    logger);

                applicantObject.incomeOnlyTrust = text;
            }
            finally
            {
                progress.HideProgressBar();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            throw new Exception("Sup, dude?");
        }

        private void changeFInancialInterviewDateToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            viewApplicants applicantObject = GetSelectedApplicantObject();
            if (applicantObject is null) return;

            using (var formSomeDateChange = new FormDatePicker()
            {
                CurrentDate = applicantObject.transferDate ?? DateTime.Now,
                LabelCaption = "Enter Financial Interview Date:",
                Text = "Financial Interview Date"
            })
            {
                if (formSomeDateChange.ShowDialog() == DialogResult.OK)
                {
                    progress.ShowProgressBar("Setting Financial Interview Date...");
                    try
                    {
                        Db.ChangeFinancialInterviewDate(
                            applicantObject.id,
                            formSomeDateChange.CurrentDate,
                            logger);

                        applicantObject.FinancialInterviewDate = formSomeDateChange.CurrentDate;
                    }
                    finally
                    {
                        progress.HideProgressBar();
                    }
                }
            }
        }

        private void changeBilledToolStripMenuItem_Click(object sender, EventArgs e)
        {
            viewApplicants applicantObject = GetSelectedApplicantObject();
            if (applicantObject is null) return;

            using (var formSomeDateChange = new FormBoolPicker()
            {
                value = applicantObject.Billed ?? false,
                checkboxCaption = "Billed",
                Text = "Billed",
                FormMode = FormModes.Edit,
            })
            {
                if (formSomeDateChange.ShowDialog() == DialogResult.OK)
                {
                    progress.ShowProgressBar("Setting Billed...");
                    try
                    {
                        Db.ChangeBilled(
                            applicantObject.id,
                            formSomeDateChange.value,
                            logger);

                        applicantObject.Billed = formSomeDateChange.value;
                    }
                    finally
                    {
                        progress.HideProgressBar();
                    }
                }
            }
        }

        private void changeCSRAAmountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            viewApplicants applicantObject = GetSelectedApplicantObject();
            if (applicantObject is null) return;

            using (var formSomeDateChange = new FormDecimalPicker()
            {
                Value = applicantObject.CSRAAmount ?? 0,
                LabelCaption = "CSRA Amount",
                Text = "Pick CSRA Amount",
                FormMode = FormModes.Edit,
            })
            {
                if (formSomeDateChange.ShowDialog() == DialogResult.OK)
                {
                    progress.ShowProgressBar("Setting CSRA Amount...");
                    try
                    {
                        Db.ChangeCSRAAmount(
                            applicantObject.id,
                            formSomeDateChange.Value,
                            logger);

                        applicantObject.CSRAAmount = formSomeDateChange.Value;
                    }
                    finally
                    {
                        progress.HideProgressBar();
                    }
                }
            }
        }

        private void changeApproxRestartDateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            viewApplicants applicantObject = GetSelectedApplicantObject();
            if (applicantObject is null) return;

            using (var formSomeDateChange = new FormDatePicker()
            {
                CurrentDate = DateTime.Now,
                LabelCaption = "Approximate Restart date",
                Text = "Pick Approximate Restart date",
            })
            {
                if (formSomeDateChange.ShowDialog() == DialogResult.OK)
                {
                    progress.ShowProgressBar("Setting Approximate Restart date...");
                    try
                    {
                        Db.ChangeApplicantRestartDate(
                            applicantObject.id,
                            formSomeDateChange.CurrentDate,
                            logger);

                        applicantObject.ApproxRestartDate = formSomeDateChange.CurrentDate;
                    }
                    finally
                    {
                        progress.HideProgressBar();
                    }
                }
            }
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void changeMovedDateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            viewApplicants applicantObject = GetSelectedApplicantObject();
            if (applicantObject is null) return;

            using (var formSomeDateChange = new FormDatePicker()
            {
                CurrentDate = DateTime.Now,
                LabelCaption = "Move Date",
                Text = "Move Date",
            })
            {
                if (formSomeDateChange.ShowDialog() == DialogResult.OK)
                {
                    progress.ShowProgressBar("Setting Move date...");
                    try
                    {
                        Db.ChangeApplicantMoveDate(
                            applicantObject.id,
                            formSomeDateChange.CurrentDate,
                            logger);

                        applicantObject.MoveDate = formSomeDateChange.CurrentDate;
                    }
                    finally
                    {
                        progress.HideProgressBar();
                    }
                }
            }
        }

        private void handleExceptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            viewApplicants applicantObject = GetSelectedApplicantObject();
            if (applicantObject is null) return;

            progress.ShowProgressBar("Setting Exception...");
            try
            {
                Guid applicantId = applicantObject.id;

                using (var formtextChange = new FormTextPicker()
                {
                    ValueCaption = "Exception, leave empty to unset",
                    Text = "Applicant Exception",
                    value = applicantObject.ExceptionMessage,
                    FormMode = FormModes.Edit
                })
                {
                    if (formtextChange.ShowDialog() == DialogResult.OK)
                    {
                        Db.ChangeApplicantException(
                            applicantId,
                            formtextChange.value,
                            logger);

                        RefreshApplicants();
                    }
                }
            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void toolTipController1_GetActiveObjectInfo(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.Info == null && e.SelectedControl == gridApplicants)
            {
                GridView view = gridApplicants.FocusedView as GridView;
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(e.ControlMousePosition);
                if (info.InRowCell && info.Column.Name == "colIsExceptionMessage")
                {
                    string text = view.GetRowCellDisplayText(info.RowHandle, "ExceptionMessage");
                    string cellKey = info.RowHandle.ToString() + " - " + info.Column.ToString();
                    e.Info = new DevExpress.Utils.ToolTipControlInfo(cellKey, text);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            return;

            //convert workers to new scehem
            List<Models.Applicant.Applicant.CaseWorker> financialWorkers = new List<Models.Applicant.Applicant.CaseWorker>();
            using (var db = new Shared.DbModel())
            {
                foreach (var applicant in db.ApplicantsNew.ToList())
                {
                    //var applicantInfo = JsonConvert.DeserializeObject<Models.Applicant.Applicant.FullApplicantInfo>(applicant.Data);                    
                    //string financialWorker = applicantInfo.caseWorker?.Name?.Trim();

                    //if (!string.IsNullOrWhiteSpace(financialWorker))
                    //{
                    //    var dbWorker = db.ApplicantsDoctors.SingleOrDefault(a => a.Name.Equals(applicantInfo.caseWorker.Name.Trim()));
                    //    Guid workerId;

                    //    if (dbWorker is null)
                    //    {
                    //        workerId = Guid.NewGuid();
                    //        db.ApplicantsDoctors.Add(new ApplicantsDoctors()
                    //        {
                    //            Id = workerId,
                    //            Address = null,
                    //            CreatedByUserId = ApplicantsApp.Db.user.Id,
                    //            CreationDate = DateTime.Now,
                    //            Email = applicantInfo.caseWorker.Email,
                    //            Name = applicantInfo.caseWorker.Name,
                    //            OfficeFax = applicantInfo.caseWorker.Fax,
                    //            OfficeTelephoneNumber = applicantInfo.caseWorker.Phone,
                    //            Type = "financialWorker"
                    //        });

                    //        db.SaveChanges();
                    //    } else
                    //    {
                    //        workerId = dbWorker.Id;
                    //    }

                        
                    //    applicantInfo.financialCaseWorkerId = workerId;
                        
                    //    applicant.Data = JsonConvert.SerializeObject(applicantInfo);
                    //    db.SaveChanges();

                    //}
                }
            }


            return;
            //Shared.Source.Stripe.DeleteAllCustomers("rk_test_uVvYJyPPghQAvmzjWnAbfCxj00hcbOhJy6");

            //return;
            using (var db = new Shared.DbModel())
            {
                foreach (var applicant in db.ApplicantsNew)
                {
                    string stripeAppilcantId = Shared.Source.Stripe.CreateCustomer(
                        "rk_test_uVvYJyPPghQAvmzjWnAbfCxj00hcbOhJy6",
                        JsonConvert.DeserializeObject<Models.Applicant.Applicant.FullApplicantInfo>(applicant.Data),
                        applicant.Id
                        );

                    applicant.StripeCustomerId = stripeAppilcantId;
                }

                db.SaveChanges();
            }

        }

        private void toolStripStripeRegister_Click(object sender, EventArgs e)
        {
            var formStripeRegister = new FormStripeSubscriptionsRegister();
            formStripeRegister.ShowDialog();
        }

        private void changePASApprovedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            viewApplicants applicantObject = GetSelectedApplicantObject();
            if (applicantObject is null) return;

            using (var formSomeDateChange = new FormBoolPicker()
            {
                value = applicantObject.Pasapproved == 1 ? true : false,
                checkboxCaption = "PAS Approved",
                Text = "PAS Approved",
                FormMode = FormModes.Edit,
            })
            {
                if (formSomeDateChange.ShowDialog() == DialogResult.OK)
                {
                    progress.ShowProgressBar("Setting PAS Approved...");
                    try
                    {
                        Db.ChangePASApproved(
                            applicantObject.id,
                            formSomeDateChange.value,
                            logger);

                        applicantObject.Pasapproved = formSomeDateChange.value ? 1 : 0;
                    }
                    finally
                    {
                        progress.HideProgressBar();
                    }
                }
            }
        }

        private void toolStripStripeInvoicesRegister_Click(object sender, EventArgs e)
        {
            var formStripeRegister = new FormStripeInvoices();
            formStripeRegister.ShowDialog();
        }
    }
}