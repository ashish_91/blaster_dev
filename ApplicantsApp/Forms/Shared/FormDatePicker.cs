﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp.Forms
{
    public partial class FormDatePicker : Form
    {
        public DateTime CurrentDate { get; set; }
        public string LabelCaption { get; set; }

        public FormDatePicker()
        {
            InitializeComponent();
        }

        private void FormDueDate_Load(object sender, EventArgs e)
        {
            if (CurrentDate.Date.Equals(new DateTime(2000, 1, 1)))
                CurrentDate = DateTime.Now;

            monthCalendar1.SetDate(CurrentDate);

            labelCaption.Text = LabelCaption;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            CurrentDate = monthCalendar1.SelectionRange.Start;
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
