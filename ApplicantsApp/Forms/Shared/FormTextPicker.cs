﻿using ApplicantsApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp.Forms
{
    public partial class FormTextPicker : Form
    {
        public FormModes FormMode { get; set; }
        public string value { get; set; }
        public string ValueCaption { get; set; }

        public FormTextPicker()
        {
            InitializeComponent();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void FormEditSingleValue_Load(object sender, EventArgs e)
        {
            switch(FormMode)
            {
                case FormModes.Edit:
                    btnSave.Text = "Save";
                    break;

                case FormModes.New:
                    btnSave.Text = "Add";
                    break;


                case FormModes.View:
                    btnSave.Text = "I should be invisible";
                    btnSave.Visible = false;
                    break;
            }

            textValue.Text = value;
            labelCaption.Text = ValueCaption;
        }

        private void TextValue_TextChanged(object sender, EventArgs e)
        {
            value = textValue.Text;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
