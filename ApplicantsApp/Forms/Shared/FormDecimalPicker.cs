﻿using ApplicantsApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp.Forms
{
    public partial class FormDecimalPicker : Form
    {
        public FormModes FormMode { get; set; }
        public decimal Value { get; set; }
        public string LabelCaption { get; set; }

        public FormDecimalPicker()
        {
            InitializeComponent();
        }

        private void FormDecimalPicker_Load(object sender, EventArgs e)
        {
            switch (FormMode)
            {
                case FormModes.Edit:
                    btnSave.Text = "Save";
                    break;

                case FormModes.New:
                    btnSave.Text = "Save";
                    break;


                case FormModes.View:
                    btnSave.Text = "I should be invisible";
                    btnSave.Visible = false;
                    break;
            }

            labelCaption.Text = LabelCaption;
            numericUpDown1.Value = Value;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Value = numericUpDown1.Value;
            DialogResult = DialogResult.OK;
        }
    }
}
