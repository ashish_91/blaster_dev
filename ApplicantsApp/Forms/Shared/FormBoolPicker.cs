﻿using ApplicantsApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp.Forms
{
    public partial class FormBoolPicker : Form
    {
        public FormModes FormMode { get; set; }
        public bool value { get; set; }
        public string checkboxCaption { get; set; }

        public FormBoolPicker()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void FormBoolPicker_Load(object sender, EventArgs e)
        {
            switch (FormMode)
            {
                case FormModes.Edit:
                    btnSave.Text = "Save";
                    break;

                case FormModes.New:
                    btnSave.Text = "Add";
                    break;


                case FormModes.View:
                    btnSave.Text = "I should be invisible";
                    btnSave.Visible = false;
                    break;
            }

            checkBox.Checked = value;
            checkBox.Text = checkboxCaption;
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            value = checkBox.Checked;
        }
    }
}
