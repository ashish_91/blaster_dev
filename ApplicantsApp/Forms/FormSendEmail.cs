﻿using Newtonsoft.Json;
using Shared;
using Shared.Source.MailGeneration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml.XPath;

namespace ApplicantsApp
{
    public partial class FormSendEmail : Form
    {
        public ApplicantsNew ApplicantData { get; set; }
        public ApplicantsRepresentatives Representative { get; set; }
        public List<Models.DTOs.AzureToLocalFileDTO> FilesToSend { get; set; }
        public string Body { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }

        public Models.Communication.EmailTypes emailType;

        public int FromSpecialAccount { get; set; } = 0;

        private Source.Progress progress;        
        private NLog.Logger logger = NLog.LogManager.GetLogger("email");

        private Models.Representative.RepresentativeInfo representativeInfo;
        private Models.Applicant.Applicant.FullApplicantInfo applicantInfo;

        public FormSendEmail()
        {
            InitializeComponent();
        }

        private void RefreshApplicantDocuments()
        {
            if ( (FilesToSend?.Count ?? 0) <= 0)
                return;

            progress.SetProgressText("Refreshing the documents...");

            listDocuments.Items.Clear();
            foreach (var document in FilesToSend)
            {
                listDocuments.Items.Add(document.documentId, document.humanFileName, CheckState.Checked, true);
            }
        }

        private List<Guid> GetSelectedDocuments()
        {
            var selectedDocuments = new List<Guid>();
            foreach (object checkedItem in listDocuments.CheckedItems)
            {
                selectedDocuments.Add((Guid)((DevExpress.XtraEditors.Controls.ListBoxItem)checkedItem).Value);
            }

            return selectedDocuments;
        }

        private void BtnSend_Click(object sender, EventArgs e)
        {
            foreach(string email in txtReceivers.Lines)
            {
                if (email.Trim().EndsWith("SENIORPLANNING.ORG", StringComparison.OrdinalIgnoreCase))
                {
                    DialogResult result = MessageBox.Show($"WARNING!!! This is being sent to {email}!!! Are you sure you want to continue?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == DialogResult.No)
                    {
                        return;
                    }
                }
            }

            Models.ReportActions action = Shared.Source.Mop.GetActionFromEmailType(emailType);

            Guid correlationId = Guid.NewGuid();
            string info = string.Empty;
            string errors = string.Empty;

            progress.ShowProgressBar("Sending the emails...");
            try
            {
                List<Guid> selectedDocumentsIds = GetSelectedDocuments();

                MailGenerator.SendEmail(
                    action, 
                    correlationId, 
                    ref info, 
                    ref errors, 
                    selectedDocumentsIds, 
                    FilesToSend, 
                    txtReceivers.Lines,
                    txtBody.Text,
                    txtSubject.Text,
                    logger,
                    ApplicantData.Id,
                    FromSpecialAccount,
                    Representative,
                    Properties.Resources.ResourceManager.GetObject("emailFooter").ToString(),
                    Db.GetUserId()
                    );

            }
            finally
            {
                MessageBox.Show(info + Environment.NewLine + errors);
                progress.HideProgressBar();
            }

        }

        private void FormSendEmail_Load(object sender, EventArgs e)
        {
            progress = new Source.Progress(progressBar1, labelProgress, this, logger);
            
            RefreshApplicantDocuments();

            representativeInfo = JsonConvert.DeserializeObject<Models.Representative.RepresentativeInfo>(Representative.Data);
            applicantInfo = JsonConvert.DeserializeObject<Models.Applicant.Applicant.FullApplicantInfo>(ApplicantData.Data);

            if (string.IsNullOrWhiteSpace(Subject))
            {
                txtSubject.Text = "The subject of the email";
            } else
            {
                txtSubject.Text = Subject;
            }

            if (string.IsNullOrWhiteSpace(To))
            {
                HashSet<string> emails = new HashSet<string>();

                emails.Add(applicantInfo.personalInfo?.detailedPersonalInfo?.email);
                emails.Add(representativeInfo.email);
                emails.Add(representativeInfo.guardian?.emailAddress);
                emails.Add(representativeInfo.heaPlusEmailNotifyAddress);

                emails.Where(w => !string.IsNullOrWhiteSpace(w)).ToList().ForEach(f => txtReceivers.AppendText(f + Environment.NewLine));
            } else
            {
                txtReceivers.AppendText(To);
            }

            if (string.IsNullOrWhiteSpace(Body))
            {
                txtBody.AppendText("This is the body of the email" + Environment.NewLine + Environment.NewLine);
                txtBody.AppendText("It is multiline. This is plain text. Can be HTML if required." + Environment.NewLine + Environment.NewLine);
            } else
            {
                txtBody.Text = Body;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
