﻿using ApplicantsApp.Models;
using ApplicantsApp.Models.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp
{
    public partial class FormAudit : Form
    {
        private NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private Source.Progress progress;

        public FormAudit()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void AddColumn(DataGridView grid, string text, bool visible = true, DataGridViewAutoSizeColumnMode sizeMode = DataGridViewAutoSizeColumnMode.AllCells)
        {
            grid.Columns.Add(new DataGridViewTextBoxColumn()
            {
                AutoSizeMode = sizeMode,
                HeaderText = text,
                ReadOnly = true,
                Resizable = DataGridViewTriState.True,
                Visible = visible,
                Name = Guid.NewGuid().ToString().Replace("-", "")
            });
        }

        private void LoadEmailsReport()
        {
            AddColumn(gridReport, "Id", false);
            AddColumn(gridReport, "ApplicantId", false); //1
            AddColumn(gridReport, "Recipient");
            AddColumn(gridReport, "Result");
            AddColumn(gridReport, "Error message", sizeMode: DataGridViewAutoSizeColumnMode.NotSet);
            AddColumn(gridReport, "Responsible User");
            AddColumn(gridReport, "Applicant Name");
            AddColumn(gridReport, "Event Date");                        
            AddColumn(gridReport, "eMail Subject", sizeMode: DataGridViewAutoSizeColumnMode.NotSet);
            AddColumn(gridReport, "eMail Body", sizeMode: DataGridViewAutoSizeColumnMode.NotSet);

            Guid? applicantId = null;

            if (comboApplicants.SelectedItem != null)
            {
                applicantId = (Guid)((ComboboxItem)comboApplicants.SelectedItem).Value;
            }

            Db.LoadSentEmails(applicantId, logger).ForEach(a =>
                gridReport.Rows.Add(
                    a.Id.ToString(),
                    a.ApplicantId.ToString(),  //1
                    a.To,
                    a.Result == 1 ? "OK" : "ERROR",
                    a.ErrorMessage,
                    a.ResponsibleUser,
                    a.ApplicantName,
                    a.Created.ToString(Source.Resources.USADateTime),                    
                    a.Subject,
                    a.Body));
        }

        private void LoadFaxReport()
        {
            AddColumn(gridReport, "Id", false);
            AddColumn(gridReport, "ApplicantId", false); //1
            AddColumn(gridReport, "Recipient");
            AddColumn(gridReport, "Result");
            AddColumn(gridReport, "Error message", sizeMode: DataGridViewAutoSizeColumnMode.NotSet);
            AddColumn(gridReport, "Responsible User");
            AddColumn(gridReport, "Applicant Name");
            AddColumn(gridReport, "Event Date");
            AddColumn(gridReport, "Faxed File (double click to open)");  //8   !!!! CHECK IT, It's index is in use!

            Guid? applicantId = null;

            if (comboApplicants.SelectedItem != null)
            {
                applicantId = (Guid)((ComboboxItem)comboApplicants.SelectedItem).Value;
            }

            Db.LoadSentFaxes(applicantId, logger).ForEach(a =>
                gridReport.Rows.Add(
                    a.Id.ToString(),
                    a.ApplicantId.ToString(),  //1
                    a.To,
                    a.Result == 1 ? "OK" : "ERROR",
                    a.ErrorMessage,
                    a.ResponsibleUser,
                    a.ApplicantName,
                    a.Created.ToString(Source.Resources.USADateTime),
                    a.FileName //8 as index
                    ));
        }

        private void LoadAllActions()
        {
            AddColumn(gridReport, "Id", false);
            AddColumn(gridReport, "Responsible User");
            AddColumn(gridReport, "Applicant Name");
            AddColumn(gridReport, "Event Date");
            AddColumn(gridReport, "What Happened");
            AddColumn(gridReport, "Data");

            Guid? applicantId = null;

            if (comboApplicants.SelectedItem != null)
            {
                applicantId = (Guid)((ComboboxItem)comboApplicants.SelectedItem).Value;
            }

            Db.LoadActions(applicantId, logger).ForEach(a =>
                gridReport.Rows.Add(
                    a.Id.ToString(),
                    a.responsibleUserName,
                    a.applicantName,
                    a.Created.ToString(Source.Resources.USADateTime),
                    ((ReportActions)a.ActionId).ToString(), 
                    a.Data
                ));
        }

        private void btnLoadReport_Click(object sender, EventArgs e)
        {
            progress.ShowProgressBar("Loading report data...");
            try
            {
                gridReport.Rows.Clear();
                gridReport.Columns.Clear();

                switch ((ReportTypes)(int)((ComboboxItem)comboReportTypes.SelectedItem).Value)
                {
                    case ReportTypes.AllActions:
                        LoadAllActions();
                        break;

                    case ReportTypes.SentEmails:
                        LoadEmailsReport();
                        break;

                    case ReportTypes.SentFaxes:
                        LoadFaxReport();
                        break;
                }
            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void FormAudit_Load(object sender, EventArgs e)
        {
            progress = new Source.Progress(progressBar1, labelProgress, this, logger);

            gridReport.Rows.Clear();
            gridReport.Columns.Clear();
            Application.DoEvents();

            comboReportTypes.Items.Add(new ComboboxItem { Text = "All Actions", Value = 1 });
            comboReportTypes.Items.Add(new ComboboxItem { Text = "Send eMails", Value = 2 });
            comboReportTypes.Items.Add(new ComboboxItem { Text = "Send faxes", Value = 3 });

            comboReportTypes.SelectedIndex = 0;

            LoadApplicants();
        }

        private void LoadApplicants()
        {
            comboApplicants.Items.Clear();

            List<ApplicantNewDTO> resp = Db.LoadApplicantsNew(0, logger); //TODO: load from db or constant
            resp.ForEach(f => comboApplicants.Items.Add(new ComboboxItem{ Text = f.Name, Value = f.Id }));
        }

        private Guid GetApplicantId(int applicantIdCellIndex = 1)
        {
            var selectedCell = gridReport.SelectedCells[0];
            return Guid.Parse(gridReport.Rows[selectedCell.RowIndex].Cells[1].Value.ToString());
        }

        private void gridReport_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ReportTypes reportType = (ReportTypes)(int)((ComboboxItem)comboReportTypes.SelectedItem).Value;

            progress.ShowProgressBar("Loading...");
            try
            {
                if (gridReport.SelectedCells.Count != 1) return;
                var selectedCell = gridReport.SelectedCells[0];

                switch (reportType)
                {
                    case ReportTypes.SentEmails:
                        

                        switch(selectedCell.ColumnIndex)
                        {
                            case 8:  //DE-101
                                //LoadAndShowFileByName(selectedCell, "de101");
                                break;

                            case 9:  //DE-112
                                //LoadAndShowFileByName(selectedCell, "de112");
                                break;

                            case 10:  //scanned
                                //LoadAndShowFileByName(selectedCell, "scanned");
                                break;
                        }
                        
                        break;

                    case ReportTypes.SentFaxes:
                        
                        switch (selectedCell.ColumnIndex)
                        {
                            case 8:  //whatever was faxed
                                LoadAndShowFileByName(selectedCell, "de101");
                                break;                            
                        }

                        break;
                }
            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void LoadAndShowFileByName(DataGridViewCell selectedCell, string prefix)
        {
            Guid applicantId = GetApplicantId();
            string fileName = gridReport.Rows[selectedCell.RowIndex].Cells[selectedCell.ColumnIndex].Value.ToString();

            string localFileName = Shared.Source.Mop.GetTempFileName(prefix, extension: "pdf");

            Shared.Source.Azure.StorageManager.DownloadToFile(
                connectionString: Shared.Source.Azure.AzureConnStr,
                containerName: "applicants",
                remoteFileName: Path.Combine(applicantId.ToString(), fileName),
                filePath: localFileName
                );

            System.Diagnostics.Process.Start(localFileName);
        }
    }
}