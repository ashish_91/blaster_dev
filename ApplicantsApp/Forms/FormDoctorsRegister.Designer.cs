﻿namespace ApplicantsApp.Forms
{
    partial class FormDoctorsRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDoctorsRegister));
            this.gridDoctors = new System.Windows.Forms.DataGridView();
            this.menuStripAdd = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolBtnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolBtnEdit = new System.Windows.Forms.ToolStripButton();
            this.toolBtnDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.labelProgress = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.btnExit = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDocId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDocDoctorName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDocType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDocEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDocTelephone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDocFax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDocAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridDoctors)).BeginInit();
            this.menuStripAdd.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridDoctors
            // 
            this.gridDoctors.AllowUserToAddRows = false;
            this.gridDoctors.AllowUserToDeleteRows = false;
            this.gridDoctors.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridDoctors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDoctors.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColDocId,
            this.ColDocDoctorName,
            this.ColDocType,
            this.ColDocEmail,
            this.ColDocTelephone,
            this.ColDocFax,
            this.ColDocAddress});
            this.gridDoctors.ContextMenuStrip = this.menuStripAdd;
            this.gridDoctors.Location = new System.Drawing.Point(12, 42);
            this.gridDoctors.Name = "gridDoctors";
            this.gridDoctors.ReadOnly = true;
            this.gridDoctors.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridDoctors.Size = new System.Drawing.Size(1025, 392);
            this.gridDoctors.TabIndex = 0;
            this.gridDoctors.SelectionChanged += new System.EventHandler(this.GridDoctors_SelectionChanged);
            // 
            // menuStripAdd
            // 
            this.menuStripAdd.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.menuStripEdit,
            this.menuStripDelete});
            this.menuStripAdd.Name = "menuStripAdd";
            this.menuStripAdd.Size = new System.Drawing.Size(108, 70);
            this.menuStripAdd.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenuDoctors_Opening);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.addToolStripMenuItem.Text = "Add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.AddToolStripMenuItem_Click);
            // 
            // menuStripEdit
            // 
            this.menuStripEdit.Name = "menuStripEdit";
            this.menuStripEdit.Size = new System.Drawing.Size(107, 22);
            this.menuStripEdit.Text = "Edit";
            this.menuStripEdit.Click += new System.EventHandler(this.MenuStripEdit_Click);
            // 
            // menuStripDelete
            // 
            this.menuStripDelete.Name = "menuStripDelete";
            this.menuStripDelete.Size = new System.Drawing.Size(107, 22);
            this.menuStripDelete.Text = "Delete";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolBtnAdd,
            this.toolBtnEdit,
            this.toolBtnDelete,
            this.toolStripSeparator3});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1049, 39);
            this.toolStrip1.TabIndex = 13;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolBtnAdd
            // 
            this.toolBtnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnAdd.Image")));
            this.toolBtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnAdd.Name = "toolBtnAdd";
            this.toolBtnAdd.Size = new System.Drawing.Size(36, 36);
            this.toolBtnAdd.Text = "toolStripButton1";
            this.toolBtnAdd.ToolTipText = "New Doctor or Psysician";
            this.toolBtnAdd.Click += new System.EventHandler(this.ToolBtnAdd_Click);
            // 
            // toolBtnEdit
            // 
            this.toolBtnEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnEdit.Enabled = false;
            this.toolBtnEdit.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnEdit.Image")));
            this.toolBtnEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnEdit.Name = "toolBtnEdit";
            this.toolBtnEdit.Size = new System.Drawing.Size(36, 36);
            this.toolBtnEdit.ToolTipText = "Edit Doctor or Psysician";
            this.toolBtnEdit.Click += new System.EventHandler(this.ToolBtnEdit_Click);
            // 
            // toolBtnDelete
            // 
            this.toolBtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnDelete.Enabled = false;
            this.toolBtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnDelete.Image")));
            this.toolBtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnDelete.Name = "toolBtnDelete";
            this.toolBtnDelete.Size = new System.Drawing.Size(36, 36);
            this.toolBtnDelete.Text = "toolStripButton3";
            this.toolBtnDelete.ToolTipText = "Delete Doctor or Psysician";
            this.toolBtnDelete.Click += new System.EventHandler(this.ToolBtnDelete_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // labelProgress
            // 
            this.labelProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelProgress.AutoSize = true;
            this.labelProgress.Location = new System.Drawing.Point(21, 445);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(49, 13);
            this.labelProgress.TabIndex = 20;
            this.labelProgress.Text = "              ";
            this.labelProgress.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(12, 440);
            this.progressBar.MarqueeAnimationSpeed = 30;
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(944, 23);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.TabIndex = 19;
            this.progressBar.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Location = new System.Drawing.Point(962, 440);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 18;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 200;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.FillWeight = 115F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Type";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 115;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Email";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 180;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Telephone";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 125;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Fax";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 125;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Address";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 350;
            // 
            // ColDocId
            // 
            this.ColDocId.HeaderText = "Id";
            this.ColDocId.Name = "ColDocId";
            this.ColDocId.ReadOnly = true;
            this.ColDocId.Visible = false;
            // 
            // ColDocDoctorName
            // 
            this.ColDocDoctorName.HeaderText = "Name";
            this.ColDocDoctorName.Name = "ColDocDoctorName";
            this.ColDocDoctorName.ReadOnly = true;
            this.ColDocDoctorName.Width = 200;
            // 
            // ColDocType
            // 
            this.ColDocType.FillWeight = 115F;
            this.ColDocType.HeaderText = "Type";
            this.ColDocType.Name = "ColDocType";
            this.ColDocType.ReadOnly = true;
            this.ColDocType.Width = 115;
            // 
            // ColDocEmail
            // 
            this.ColDocEmail.HeaderText = "Email";
            this.ColDocEmail.Name = "ColDocEmail";
            this.ColDocEmail.ReadOnly = true;
            this.ColDocEmail.Width = 180;
            // 
            // ColDocTelephone
            // 
            this.ColDocTelephone.HeaderText = "Telephone";
            this.ColDocTelephone.Name = "ColDocTelephone";
            this.ColDocTelephone.ReadOnly = true;
            // 
            // ColDocFax
            // 
            this.ColDocFax.HeaderText = "Fax";
            this.ColDocFax.Name = "ColDocFax";
            this.ColDocFax.ReadOnly = true;
            // 
            // ColDocAddress
            // 
            this.ColDocAddress.HeaderText = "Address";
            this.ColDocAddress.Name = "ColDocAddress";
            this.ColDocAddress.ReadOnly = true;
            this.ColDocAddress.Width = 250;
            // 
            // FormDoctorsRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1049, 475);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.gridDoctors);
            this.Name = "FormDoctorsRegister";
            this.Text = "Workers Register";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormDoctorsRegister_FormClosing);
            this.Load += new System.EventHandler(this.FormDoctorsRegister_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridDoctors)).EndInit();
            this.menuStripAdd.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gridDoctors;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolBtnAdd;
        private System.Windows.Forms.ToolStripButton toolBtnEdit;
        private System.Windows.Forms.ToolStripButton toolBtnDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.Label labelProgress;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ContextMenuStrip menuStripAdd;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuStripEdit;
        private System.Windows.Forms.ToolStripMenuItem menuStripDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDocId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDocDoctorName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDocType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDocEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDocTelephone;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDocFax;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDocAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
    }
}