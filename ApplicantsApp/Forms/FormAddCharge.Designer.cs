﻿namespace ApplicantsApp.Forms
{
    partial class FormAddCharge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label12 = new System.Windows.Forms.Label();
            this.comboApplicants = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPaymentMethods = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtStripeEmail = new System.Windows.Forms.TextBox();
            this.txtStripeCustomerName = new System.Windows.Forms.TextBox();
            this.btnShowCustomer = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(12, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 13);
            this.label12.TabIndex = 15;
            this.label12.Text = "Select Applicant:";
            // 
            // comboApplicants
            // 
            this.comboApplicants.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboApplicants.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboApplicants.DisplayMember = "Text";
            this.comboApplicants.FormattingEnabled = true;
            this.comboApplicants.Location = new System.Drawing.Point(122, 6);
            this.comboApplicants.Name = "comboApplicants";
            this.comboApplicants.Size = new System.Drawing.Size(391, 21);
            this.comboApplicants.TabIndex = 14;
            this.comboApplicants.ValueMember = "Value";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.groupBox1.Controls.Add(this.txtPaymentMethods);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtStripeEmail);
            this.groupBox1.Controls.Add(this.txtStripeCustomerName);
            this.groupBox1.Controls.Add(this.btnShowCustomer);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(7, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(506, 124);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Stripe Customer Info";
            // 
            // txtPaymentMethods
            // 
            this.txtPaymentMethods.Enabled = false;
            this.txtPaymentMethods.Location = new System.Drawing.Point(24, 91);
            this.txtPaymentMethods.Name = "txtPaymentMethods";
            this.txtPaymentMethods.ReadOnly = true;
            this.txtPaymentMethods.Size = new System.Drawing.Size(473, 20);
            this.txtPaymentMethods.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Payment methods:";
            // 
            // txtStripeEmail
            // 
            this.txtStripeEmail.Enabled = false;
            this.txtStripeEmail.Location = new System.Drawing.Point(281, 41);
            this.txtStripeEmail.Name = "txtStripeEmail";
            this.txtStripeEmail.ReadOnly = true;
            this.txtStripeEmail.Size = new System.Drawing.Size(189, 20);
            this.txtStripeEmail.TabIndex = 10;
            // 
            // txtStripeCustomerName
            // 
            this.txtStripeCustomerName.Enabled = false;
            this.txtStripeCustomerName.Location = new System.Drawing.Point(24, 41);
            this.txtStripeCustomerName.Name = "txtStripeCustomerName";
            this.txtStripeCustomerName.ReadOnly = true;
            this.txtStripeCustomerName.Size = new System.Drawing.Size(251, 20);
            this.txtStripeCustomerName.TabIndex = 9;
            // 
            // btnShowCustomer
            // 
            this.btnShowCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnShowCustomer.Location = new System.Drawing.Point(473, 40);
            this.btnShowCustomer.Name = "btnShowCustomer";
            this.btnShowCustomer.Size = new System.Drawing.Size(24, 22);
            this.btnShowCustomer.TabIndex = 8;
            this.btnShowCustomer.Text = "S";
            this.btnShowCustomer.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(278, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Stripe recipient e-mail:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Applicant/Stripe Customer Name:";
            // 
            // FormAddCharge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 450);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.comboApplicants);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAddCharge";
            this.Text = "FormAddCharge";
            this.Load += new System.EventHandler(this.FormAddCharge_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboApplicants;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtPaymentMethods;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtStripeEmail;
        private System.Windows.Forms.TextBox txtStripeCustomerName;
        private System.Windows.Forms.Button btnShowCustomer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
    }
}