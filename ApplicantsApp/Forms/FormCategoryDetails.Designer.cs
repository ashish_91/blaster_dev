﻿namespace ApplicantsApp.Forms
{
    partial class FormCategoryDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnHelpPriority = new System.Windows.Forms.Button();
            this.categoryPriority = new System.Windows.Forms.NumericUpDown();
            this.progressBarText = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.categoryDesc = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.categoryName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.categoryPriority)).BeginInit();
            this.SuspendLayout();
            // 
            // btnHelpPriority
            // 
            this.btnHelpPriority.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHelpPriority.Location = new System.Drawing.Point(452, 4);
            this.btnHelpPriority.Name = "btnHelpPriority";
            this.btnHelpPriority.Size = new System.Drawing.Size(18, 20);
            this.btnHelpPriority.TabIndex = 46;
            this.btnHelpPriority.Text = "?";
            this.btnHelpPriority.UseVisualStyleBackColor = true;
            // 
            // categoryPriority
            // 
            this.categoryPriority.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.categoryPriority.Location = new System.Drawing.Point(372, 26);
            this.categoryPriority.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.categoryPriority.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.categoryPriority.Name = "categoryPriority";
            this.categoryPriority.Size = new System.Drawing.Size(98, 20);
            this.categoryPriority.TabIndex = 45;
            this.categoryPriority.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // progressBarText
            // 
            this.progressBarText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.progressBarText.AutoSize = true;
            this.progressBarText.Location = new System.Drawing.Point(26, 234);
            this.progressBarText.Name = "progressBarText";
            this.progressBarText.Size = new System.Drawing.Size(52, 13);
            this.progressBarText.TabIndex = 44;
            this.progressBarText.Text = "_           _";
            this.progressBarText.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(12, 228);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(261, 23);
            this.progressBar.TabIndex = 43;
            this.progressBar.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnClose.Location = new System.Drawing.Point(389, 227);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(81, 25);
            this.btnClose.TabIndex = 42;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(301, 227);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(81, 25);
            this.btnSave.TabIndex = 41;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // categoryDesc
            // 
            this.categoryDesc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.categoryDesc.Location = new System.Drawing.Point(12, 79);
            this.categoryDesc.Multiline = true;
            this.categoryDesc.Name = "categoryDesc";
            this.categoryDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.categoryDesc.Size = new System.Drawing.Size(458, 138);
            this.categoryDesc.TabIndex = 40;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(349, 13);
            this.label3.TabIndex = 39;
            this.label3.Text = "Default Category description, can be later custumized for each applicant:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(369, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 38;
            this.label2.Text = "Category priority:";
            // 
            // categoryName
            // 
            this.categoryName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.categoryName.Location = new System.Drawing.Point(12, 27);
            this.categoryName.Name = "categoryName";
            this.categoryName.Size = new System.Drawing.Size(341, 20);
            this.categoryName.TabIndex = 37;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "Category name:";
            // 
            // FormCategoryDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 263);
            this.Controls.Add(this.btnHelpPriority);
            this.Controls.Add(this.categoryPriority);
            this.Controls.Add(this.progressBarText);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.categoryDesc);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.categoryName);
            this.Controls.Add(this.label1);
            this.Name = "FormCategoryDetails";
            this.Text = "Category";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormCategoryDetails_FormClosing);
            this.Load += new System.EventHandler(this.FormCategoryDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.categoryPriority)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnHelpPriority;
        private System.Windows.Forms.NumericUpDown categoryPriority;
        private System.Windows.Forms.Label progressBarText;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox categoryDesc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox categoryName;
        private System.Windows.Forms.Label label1;
    }
}