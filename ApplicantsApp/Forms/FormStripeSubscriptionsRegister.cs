﻿using ApplicantsApp.Source;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp.Forms
{
    public partial class FormStripeSubscriptionsRegister : Form
    {        
        private NLog.Logger logger = NLog.LogManager.GetLogger("stripe-register");

        private Progress progress;

        public FormStripeSubscriptionsRegister()
        {
            InitializeComponent();
        }

        private void FormStripeSubscriptionsRegister_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            progress = new Progress(progressBar, progressBarText, this, logger);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            LoadSubscriptions();
        }

        private void LoadSubscriptions()
        {
            dataGridView1.Rows.Clear();

            progress.ShowProgressBar("Loading data...");
            try
            {
                using (var db = new Shared.DbModel())
                {
                    foreach (Shared.viewApplicantsStripeInfo subscriptionInfo in db.viewApplicantsStripeInfo.Where(w => w.Id != null))
                    {
                        dataGridView1.Rows.Add(
                            subscriptionInfo.Applicant_ApplicantId,
                            subscriptionInfo.applicantName,
                            subscriptionInfo.StripeCustomerId,
                            subscriptionInfo.ProductName,
                            subscriptionInfo.DayOfMonthToInvoice,
                            subscriptionInfo.MonthlySum,
                            subscriptionInfo.TotalPayedSoFar,
                            subscriptionInfo.TotalTargetSum == Shared.Source.Stripe.TOTAL_INFINITY_TO_PAY_SUM ? "infinity" : subscriptionInfo.TotalTargetSum.ToString(),
                            subscriptionInfo.Created?.ToString("MM/dd/yyyy"),
                            subscriptionInfo.StripePlanId,
                            subscriptionInfo.StripeProductId,
                            subscriptionInfo.SubscriptionId,
                            subscriptionInfo.Id
                            );
                    }
                }
            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void btnAddNewSubscription_Click(object sender, EventArgs e)
        {            
            var formStripeCustomerSubscriptions = new FormStripeCustomerSubscriptions()
            {
                FormMode = Models.FormModes.New
            };

            formStripeCustomerSubscriptions.ShowDialog();

            LoadSubscriptions();
        }

        private void btnViewSubscription_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedCells.Count != 1)
            {
                MessageBox.Show("Please select a single subscription!");
                return;
            }

            var formStripeCustomerSubscriptions = new FormStripeCustomerSubscriptions()
            {
                FormMode = Models.FormModes.View,
                CustomersSubscriptionId = (Guid)dataGridView1.Rows[dataGridView1.SelectedCells[0].RowIndex].Cells[12].Value,
            };

            formStripeCustomerSubscriptions.ShowDialog();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This can take a minute or two. \nThe message you will see in the end can be structured or formatted a lot better, if you preffer to.");

            progress.ShowProgressBar("Refreshing subscriptions, invoices, refunds...");
            try
            {
                using (var webClient = new HttpClient())
                {
                    webClient.Timeout = TimeSpan.FromMinutes(45);                    
                   // webClient.Timeout = TimeSpan.FromMinutes(5);                    
                    //string resp = webClient.GetStringAsync("http://jubotho-001-site1.itempurl.com/api/api/stripe/HandleInvoices?skipEmailingForNotPayedInvoices=true").Result;
                    string resp = webClient.GetStringAsync("http://localhost:50359/api/stripe/HandleInvoices?skipEmailingForNotPayedInvoices=true").Result;
                    MessageBox.Show(resp.Replace("\\r\\n", Environment.NewLine));

                    LoadSubscriptions();
                }
            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void btnEditSubscription_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedCells.Count != 1)
            {
                MessageBox.Show("Please select a single subscription!");
                return;
            }

            var formStripeCustomerSubscriptions = new FormStripeCustomerSubscriptions()
            {
                FormMode = Models.FormModes.Edit,
                CustomersSubscriptionId = (Guid)dataGridView1.Rows[dataGridView1.SelectedCells[0].RowIndex].Cells[12].Value,
            };

            formStripeCustomerSubscriptions.ShowDialog();
            LoadSubscriptions();
        }

        private void btnJustRefresh_Click(object sender, EventArgs e)
        {
            LoadSubscriptions();
        }
    }
}