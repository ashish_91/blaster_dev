﻿namespace ApplicantsApp.Forms
{
    partial class FormStripeRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridStripeCustomers = new DevExpress.XtraGrid.GridControl();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addPlanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.entityServerModeSource3 = new DevExpress.Data.Linq.EntityServerModeSource();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colapplicantName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colappllicantStatusId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStripeCustomerId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApplicant_ApplicantId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApplicantId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDayOfMonthToInvoice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMonthlySum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalTargetSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStripePlanId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStripeProductId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coluniqueRowId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalPayedSoFar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.addManualInvoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.gridStripeCustomers)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.entityServerModeSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridStripeCustomers
            // 
            this.gridStripeCustomers.ContextMenuStrip = this.contextMenuStrip1;
            this.gridStripeCustomers.DataSource = this.entityServerModeSource3;
            this.gridStripeCustomers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridStripeCustomers.Location = new System.Drawing.Point(0, 0);
            this.gridStripeCustomers.MainView = this.gridView1;
            this.gridStripeCustomers.Name = "gridStripeCustomers";
            this.gridStripeCustomers.Size = new System.Drawing.Size(1221, 547);
            this.gridStripeCustomers.TabIndex = 0;
            this.gridStripeCustomers.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addPlanToolStripMenuItem,
            this.addManualInvoiceToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(190, 70);
            // 
            // addPlanToolStripMenuItem
            // 
            this.addPlanToolStripMenuItem.Name = "addPlanToolStripMenuItem";
            this.addPlanToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.addPlanToolStripMenuItem.Text = "Add Subscription...";
            this.addPlanToolStripMenuItem.Click += new System.EventHandler(this.addPlanToolStripMenuItem_Click);
            // 
            // entityServerModeSource3
            // 
            this.entityServerModeSource3.ElementType = typeof(Shared.viewApplicantsStripeInfo);
            this.entityServerModeSource3.KeyExpression = "uniqueRowId";
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colapplicantName,
            this.colappllicantStatusId,
            this.colStripeCustomerId,
            this.colApplicant_ApplicantId,
            this.colPlanId,
            this.colApplicantId,
            this.colDayOfMonthToInvoice,
            this.colMonthlySum,
            this.colTotalTargetSum,
            this.colProductName,
            this.colIsActive,
            this.colCreated,
            this.colCreatedBy,
            this.colStripePlanId,
            this.colStripeProductId,
            this.coluniqueRowId,
            this.colTotalPayedSoFar});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gridView1.GridControl = this.gridStripeCustomers;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.Behavior = DevExpress.XtraEditors.FindPanelBehavior.Filter;
            this.gridView1.OptionsFind.Condition = DevExpress.Data.Filtering.FilterCondition.Contains;
            this.gridView1.OptionsFind.FindDelay = 500;
            this.gridView1.OptionsFind.FindFilterColumns = "applicantName";
            this.gridView1.OptionsFind.FindMode = DevExpress.XtraEditors.FindMode.Always;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.BestFitMode = DevExpress.XtraGrid.Views.Grid.GridBestFitMode.Fast;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colapplicantName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colapplicantName
            // 
            this.colapplicantName.FieldName = "applicantName";
            this.colapplicantName.Name = "colapplicantName";
            this.colapplicantName.Visible = true;
            this.colapplicantName.VisibleIndex = 1;
            // 
            // colappllicantStatusId
            // 
            this.colappllicantStatusId.FieldName = "appllicantStatusId";
            this.colappllicantStatusId.Name = "colappllicantStatusId";
            // 
            // colStripeCustomerId
            // 
            this.colStripeCustomerId.FieldName = "StripeCustomerId";
            this.colStripeCustomerId.Name = "colStripeCustomerId";
            // 
            // colApplicant_ApplicantId
            // 
            this.colApplicant_ApplicantId.FieldName = "Applicant_ApplicantId";
            this.colApplicant_ApplicantId.Name = "colApplicant_ApplicantId";
            // 
            // colPlanId
            // 
            this.colPlanId.FieldName = "PlanId";
            this.colPlanId.Name = "colPlanId";
            // 
            // colApplicantId
            // 
            this.colApplicantId.FieldName = "ApplicantId";
            this.colApplicantId.Name = "colApplicantId";
            // 
            // colDayOfMonthToInvoice
            // 
            this.colDayOfMonthToInvoice.FieldName = "DayOfMonthToInvoice";
            this.colDayOfMonthToInvoice.Name = "colDayOfMonthToInvoice";
            this.colDayOfMonthToInvoice.Visible = true;
            this.colDayOfMonthToInvoice.VisibleIndex = 2;
            this.colDayOfMonthToInvoice.Width = 135;
            // 
            // colMonthlySum
            // 
            this.colMonthlySum.FieldName = "MonthlySum";
            this.colMonthlySum.Name = "colMonthlySum";
            this.colMonthlySum.Visible = true;
            this.colMonthlySum.VisibleIndex = 0;
            this.colMonthlySum.Width = 78;
            // 
            // colTotalTargetSum
            // 
            this.colTotalTargetSum.FieldName = "TotalTargetSum";
            this.colTotalTargetSum.Name = "colTotalTargetSum";
            this.colTotalTargetSum.Visible = true;
            this.colTotalTargetSum.VisibleIndex = 3;
            this.colTotalTargetSum.Width = 98;
            // 
            // colProductName
            // 
            this.colProductName.FieldName = "ProductName";
            this.colProductName.Name = "colProductName";
            this.colProductName.Visible = true;
            this.colProductName.VisibleIndex = 4;
            this.colProductName.Width = 786;
            // 
            // colIsActive
            // 
            this.colIsActive.FieldName = "IsActive";
            this.colIsActive.Name = "colIsActive";
            // 
            // colCreated
            // 
            this.colCreated.FieldName = "Created";
            this.colCreated.Name = "colCreated";
            // 
            // colCreatedBy
            // 
            this.colCreatedBy.FieldName = "CreatedBy";
            this.colCreatedBy.Name = "colCreatedBy";
            // 
            // colStripePlanId
            // 
            this.colStripePlanId.FieldName = "StripePlanId";
            this.colStripePlanId.Name = "colStripePlanId";
            // 
            // colStripeProductId
            // 
            this.colStripeProductId.FieldName = "StripeProductId";
            this.colStripeProductId.Name = "colStripeProductId";
            // 
            // coluniqueRowId
            // 
            this.coluniqueRowId.FieldName = "uniqueRowId";
            this.coluniqueRowId.Name = "coluniqueRowId";
            // 
            // colTotalPayedSoFar
            // 
            this.colTotalPayedSoFar.FieldName = "TotalPayedSoFar";
            this.colTotalPayedSoFar.Name = "colTotalPayedSoFar";
            this.colTotalPayedSoFar.Visible = true;
            this.colTotalPayedSoFar.VisibleIndex = 1;
            this.colTotalPayedSoFar.Width = 106;
            // 
            // addManualInvoiceToolStripMenuItem
            // 
            this.addManualInvoiceToolStripMenuItem.Name = "addManualInvoiceToolStripMenuItem";
            this.addManualInvoiceToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.addManualInvoiceToolStripMenuItem.Text = "Add Manual Invoice...";
            this.addManualInvoiceToolStripMenuItem.Click += new System.EventHandler(this.addManualInvoiceToolStripMenuItem_Click);
            // 
            // FormStripeRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1221, 547);
            this.Controls.Add(this.gridStripeCustomers);
            this.Name = "FormStripeRegister";
            this.Text = "FormStripeRegister";
            ((System.ComponentModel.ISupportInitialize)(this.gridStripeCustomers)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.entityServerModeSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridStripeCustomers;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colapplicantName;
        private DevExpress.XtraGrid.Columns.GridColumn colappllicantStatusId;
        private DevExpress.XtraGrid.Columns.GridColumn colStripeCustomerId;
        private DevExpress.XtraGrid.Columns.GridColumn colApplicant_ApplicantId;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanId;
        private DevExpress.XtraGrid.Columns.GridColumn colApplicantId;
        private DevExpress.XtraGrid.Columns.GridColumn colDayOfMonthToInvoice;
        private DevExpress.XtraGrid.Columns.GridColumn colMonthlySum;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalTargetSum;
        private DevExpress.XtraGrid.Columns.GridColumn colProductName;
        private DevExpress.XtraGrid.Columns.GridColumn colIsActive;
        private DevExpress.XtraGrid.Columns.GridColumn colCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colStripePlanId;
        private DevExpress.XtraGrid.Columns.GridColumn colStripeProductId;
        private DevExpress.XtraGrid.Columns.GridColumn coluniqueRowId;
        private DevExpress.Data.Linq.EntityServerModeSource entityServerModeSource3;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addPlanToolStripMenuItem;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalPayedSoFar;
        private System.Windows.Forms.ToolStripMenuItem addManualInvoiceToolStripMenuItem;
    }
}