﻿namespace ApplicantsApp.Forms
{
    partial class FormRepresentativesCatalog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRepresentativesCatalog));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolEditApplicant = new System.Windows.Forms.ToolStripButton();
            this.toolDeleteApplicant = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.btnExit = new System.Windows.Forms.Button();
            this.gridApplicants = new System.Windows.Forms.DataGridView();
            this.GridRepAppId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridAppStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridRepName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridAppType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridAppRepEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridAppCreated = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuApplicants = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStripNew = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.labelProgress = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridApplicants)).BeginInit();
            this.contextMenuApplicants.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolEditApplicant,
            this.toolDeleteApplicant,
            this.toolStripSeparator3});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(992, 39);
            this.toolStrip1.TabIndex = 12;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.ToolTipText = "New Applicant";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolEditApplicant
            // 
            this.toolEditApplicant.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolEditApplicant.Enabled = false;
            this.toolEditApplicant.Image = ((System.Drawing.Image)(resources.GetObject("toolEditApplicant.Image")));
            this.toolEditApplicant.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolEditApplicant.Name = "toolEditApplicant";
            this.toolEditApplicant.Size = new System.Drawing.Size(36, 36);
            this.toolEditApplicant.ToolTipText = "Edit Applicant";
            this.toolEditApplicant.Click += new System.EventHandler(this.toolEditApplicant_Click);
            // 
            // toolDeleteApplicant
            // 
            this.toolDeleteApplicant.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolDeleteApplicant.Enabled = false;
            this.toolDeleteApplicant.Image = ((System.Drawing.Image)(resources.GetObject("toolDeleteApplicant.Image")));
            this.toolDeleteApplicant.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolDeleteApplicant.Name = "toolDeleteApplicant";
            this.toolDeleteApplicant.Size = new System.Drawing.Size(36, 36);
            this.toolDeleteApplicant.Text = "toolStripButton3";
            this.toolDeleteApplicant.ToolTipText = "Delete applicant";
            this.toolDeleteApplicant.Click += new System.EventHandler(this.ToolDeleteApplicant_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(12, 465);
            this.progressBar.MarqueeAnimationSpeed = 30;
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(531, 23);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.TabIndex = 11;
            this.progressBar.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Location = new System.Drawing.Point(906, 465);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 10;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // gridApplicants
            // 
            this.gridApplicants.AllowUserToAddRows = false;
            this.gridApplicants.AllowUserToDeleteRows = false;
            this.gridApplicants.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridApplicants.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridApplicants.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GridRepAppId,
            this.GridAppStatus,
            this.GridRepName,
            this.GridAppType,
            this.GridAppRepEmail,
            this.GridAppCreated});
            this.gridApplicants.ContextMenuStrip = this.contextMenuApplicants;
            this.gridApplicants.Location = new System.Drawing.Point(12, 41);
            this.gridApplicants.MultiSelect = false;
            this.gridApplicants.Name = "gridApplicants";
            this.gridApplicants.ReadOnly = true;
            this.gridApplicants.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridApplicants.Size = new System.Drawing.Size(968, 415);
            this.gridApplicants.TabIndex = 9;
            this.gridApplicants.SelectionChanged += new System.EventHandler(this.gridApplicants_SelectionChanged);
            // 
            // GridRepAppId
            // 
            this.GridRepAppId.HeaderText = "Id";
            this.GridRepAppId.Name = "GridRepAppId";
            this.GridRepAppId.ReadOnly = true;
            this.GridRepAppId.Visible = false;
            // 
            // GridAppStatus
            // 
            this.GridAppStatus.HeaderText = "Status";
            this.GridAppStatus.Name = "GridAppStatus";
            this.GridAppStatus.ReadOnly = true;
            // 
            // GridRepName
            // 
            this.GridRepName.HeaderText = "Representative Name";
            this.GridRepName.Name = "GridRepName";
            this.GridRepName.ReadOnly = true;
            this.GridRepName.Width = 250;
            // 
            // GridAppType
            // 
            this.GridAppType.HeaderText = "Representative Type";
            this.GridAppType.Name = "GridAppType";
            this.GridAppType.ReadOnly = true;
            this.GridAppType.Width = 150;
            // 
            // GridAppRepEmail
            // 
            this.GridAppRepEmail.HeaderText = "eMail address";
            this.GridAppRepEmail.Name = "GridAppRepEmail";
            this.GridAppRepEmail.ReadOnly = true;
            this.GridAppRepEmail.Width = 150;
            // 
            // GridAppCreated
            // 
            this.GridAppCreated.HeaderText = "Created on";
            this.GridAppCreated.Name = "GridAppCreated";
            this.GridAppCreated.ReadOnly = true;
            this.GridAppCreated.Width = 120;
            // 
            // contextMenuApplicants
            // 
            this.contextMenuApplicants.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuStripNew,
            this.menuStripEdit,
            this.menuStripDelete});
            this.contextMenuApplicants.Name = "contextMenuStrip1";
            this.contextMenuApplicants.Size = new System.Drawing.Size(188, 70);
            this.contextMenuApplicants.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // menuStripNew
            // 
            this.menuStripNew.Name = "menuStripNew";
            this.menuStripNew.Size = new System.Drawing.Size(187, 22);
            this.menuStripNew.Text = "New Representative";
            this.menuStripNew.Click += new System.EventHandler(this.menuStripNew_Click);
            // 
            // menuStripEdit
            // 
            this.menuStripEdit.Name = "menuStripEdit";
            this.menuStripEdit.Size = new System.Drawing.Size(187, 22);
            this.menuStripEdit.Text = "Edit Representative";
            this.menuStripEdit.Click += new System.EventHandler(this.menuStripEdit_Click);
            // 
            // menuStripDelete
            // 
            this.menuStripDelete.Name = "menuStripDelete";
            this.menuStripDelete.Size = new System.Drawing.Size(187, 22);
            this.menuStripDelete.Text = "Delete Representative";
            this.menuStripDelete.Click += new System.EventHandler(this.MenuStripDelete_Click);
            // 
            // labelProgress
            // 
            this.labelProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelProgress.AutoSize = true;
            this.labelProgress.Location = new System.Drawing.Point(24, 471);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(70, 13);
            this.labelProgress.TabIndex = 17;
            this.labelProgress.Text = "                     ";
            this.labelProgress.Visible = false;
            // 
            // FormRepresentativesCatalog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 497);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.gridApplicants);
            this.Name = "FormRepresentativesCatalog";
            this.Text = "RepresentativesCatalog";
            this.Load += new System.EventHandler(this.FormRepresentativesCatalog_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridApplicants)).EndInit();
            this.contextMenuApplicants.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolEditApplicant;
        private System.Windows.Forms.ToolStripButton toolDeleteApplicant;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.DataGridView gridApplicants;
        private System.Windows.Forms.Label labelProgress;
        private System.Windows.Forms.ContextMenuStrip contextMenuApplicants;
        private System.Windows.Forms.ToolStripMenuItem menuStripNew;
        private System.Windows.Forms.ToolStripMenuItem menuStripEdit;
        private System.Windows.Forms.ToolStripMenuItem menuStripDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridRepAppId;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridAppStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridRepName;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridAppType;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridAppRepEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridAppCreated;
    }
}