﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp.Forms
{
    public partial class FormEditValue : Form
    {
        public string Caption { get; set; }
        public string Value { get; set; }
        public string FormCaption { get; set; }
        public string TaskNotes { get; set; }

        public FormEditValue()
        {
            InitializeComponent();            
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            Value = textName.Text;
            TaskNotes = textTaskNotes.Text;
            DialogResult = DialogResult.OK;
        }

        private void FormEditValue_Load(object sender, EventArgs e)
        {
            labelCaption.Text = Caption;
            textName.Text = Value;
            textTaskNotes.Text = TaskNotes;
            this.Text = FormCaption;
        }
    }
}
