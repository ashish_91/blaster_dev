﻿using ApplicantsApp.Models;
using ApplicantsApp.Models.DTOs;
using Shared;
using Stripe;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Shared.Models;
using Shared.Enums;

namespace ApplicantsApp.Forms
{
    public partial class FormStripeCustomerSubscriptions : Form
    {
        private NLog.Logger logger = NLog.LogManager.GetLogger("stripe");

        //private const string StripeApiKey = "rk_test_uVvYJyPPghQAvmzjWnAbfCxj00hcbOhJy6";
        private const string StripeApiKey = "sk_live_7hMRqrf9iejUj7HahNxxZMhu00kVzfhdq2";
        private const string BaseCustomersAddress = "https://dashboard.stripe.com/test/customers/";

        private List<Stripe.Product> allProducts;
        private Stripe.Customer stripeCustomer;

        public Guid CustomersSubscriptionId { get; set; }

        private Applicants_CustomersSubscriptions subscriptionInfo;

        public FormModes FormMode { get; set; }

        public FormStripeCustomerSubscriptions()
        {
            InitializeComponent();
        }

        private void FormStripeCustomerSubscriptions_Load(object sender, EventArgs e)
        {

        }

        private void RefreshStripeProducts()
        {
            allProducts = Shared.Source.Stripe.GetProducts(StripeApiKey);

            comboProducts.Items.Clear();
            foreach (var product in allProducts)
            {
                comboProducts.Items.Add(new ComboboxItem
                {
                    Text = product.Name,
                    Value = product
                });
            }

        }
        private void LoadStatus()
        {
            comboStatus.Items.Clear();
            foreach(var status in Enum.GetValues(typeof(Shared.Enums.subscriptionStatus))){
                comboStatus.Items.Add(new ComboboxItem
                {
                    Text=status.ToString(),
                    Value=(int)status
                });
            }
            comboStatus.SelectedIndex=2;
        }

        private void comboProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            Stripe.Product product = (Stripe.Product)((ComboboxItem)comboProducts.SelectedItem).Value;
            textTotalSum.Value = GetTotalToPayFromProduct(product);

            List<Stripe.Plan> plans = Shared.Source.Stripe.GetPlans(StripeApiKey, product.Id);

            comboPlans.Items.Clear();
            foreach (var plan in plans)
            {
                comboPlans.Items.Add(new ComboboxItem
                {
                    Text = plan.Nickname,
                    Value = plan
                });
            }

            IsActiveSubscription();
        }

        private static decimal GetTotalToPayFromProduct(Product product)
        {
            return decimal.Parse(product.Metadata["total_amount"]);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnShowCustomer_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(BaseCustomersAddress + stripeCustomer.Id);
        }

        private void EnableForm(bool enable)
        {
            comboApplicants.Enabled = enable;
            textStartDay.Enabled = enable;
            comboProducts.Enabled = enable;
            comboPlans.Enabled = enable;
            textInitialAmount.Enabled = enable;

            btnRefresh.Enabled = enable;
            txtSave.Enabled = enable;
            groupAddCharge.Enabled = enable;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            RefreshStripeProducts();
            LoadApplicants();
            LoadStatus();

            switch (FormMode)
            {
                case FormModes.New:
                    //comboApplicants.SelectedIndex = 0;
                    EnableForm(true);
                    groupAddCharge.Enabled = false;
                    break;

                case FormModes.View:
                case FormModes.Edit:

                    //Load subscrtiption Data
                    subscriptionInfo = Db.GetCustomersSubscriptionById(logger, CustomersSubscriptionId);


                    //set applicant
                    foreach (var item in comboApplicants.Items)
                    {
                        if (((Guid)((ComboboxItem)item).Value).Equals(subscriptionInfo.ApplicantId))
                        {
                            comboApplicants.SelectedItem = item;
                            break;
                        }
                    }
                    System.Windows.Forms.Application.DoEvents();

                    //set product
                    foreach (var item in comboProducts.Items)
                    {
                        if (((Stripe.Product)((ComboboxItem)item).Value).Id.Equals(subscriptionInfo.StripeProductId))
                        {
                            comboProducts.SelectedItem = item;
                            break;
                        }
                    }
                    System.Windows.Forms.Application.DoEvents();

                    //set plan
                    foreach (var item in comboPlans.Items)
                    {
                        if ((((Stripe.Plan)((ComboboxItem)item).Value).Id).Equals(subscriptionInfo.StripePlanId))
                        {
                            comboPlans.SelectedItem = item;
                            break;
                        }
                    }
                    System.Windows.Forms.Application.DoEvents();

                    //set total payed so far
                    textInitialAmount.Value = subscriptionInfo.TotalPayedSoFar;
                    System.Windows.Forms.Application.DoEvents();

                    ///Addition by Ashish 16-09-2020
                    comboStatus.SelectedIndex = subscriptionInfo.SubscriptionStatus;

                    EnableForm(false);
                    groupAddCharge.Enabled = true;

                    break;


                default:
                    throw new NotImplementedException($"For mode {FormMode} is not implemented!");
            }

            if (FormMode == FormModes.Edit)
            {
                textInitialAmount.Enabled = true;
                txtSave.Enabled = true;
                txtSave.Text = "Apply";
            }
        }

        private void LoadApplicants()
        {
            comboApplicants.Items.Clear();

            List<ApplicantNewDTO> resp = Db.LoadApplicantsNew(0, logger); //TODO: load from db or constant
            resp.ForEach(f => comboApplicants.Items.Add(new ComboboxItem
            {
                Text = f.Name + "  " + f.StripeCustomerId,
                Value = f.Id,
                MetaString1 = f.StripeCustomerId
            }));
        }

        private string GetStripeIdOfSelectedApplicant()
        {
            if (comboApplicants.SelectedItem == null) throw new Exception("Please select an applicant!");

            string customerStripeId = ((ComboboxItem)comboApplicants.SelectedItem).MetaString1;
            if (string.IsNullOrWhiteSpace(customerStripeId)) throw new Exception("The selected applicant is not imported to Stripe, because only the customers from like a December are importing to stripe automatically. It is not a problem to import all applicants, or only selected ones, if you need this, let me know.");

            return customerStripeId;
        }

        private Guid GetApplicantIdOfSelectedApplicant()
        {
            if (comboApplicants.SelectedItem == null) throw new Exception("Please select an applicant!");

            return (Guid)(((ComboboxItem)comboApplicants.SelectedItem).Value);
        }

        private void RefreshStripeCustomer()
        {
            stripeCustomer = Shared.Source.Stripe.GetCustomer(StripeApiKey, GetStripeIdOfSelectedApplicant());

            txtStripeCustomerName.Text = stripeCustomer.Name;
            txtStripeEmail.Text = stripeCustomer.Email;

            txtPaymentMethods.Text = stripeCustomer.Sources.Count() > 0 ? "" : "none";

            foreach (var source in stripeCustomer.Sources)
            {
                if (source is Stripe.Card)
                {
                    var card = source as Stripe.Card;
                    txtPaymentMethods.Text += $"{card.Brand} ****{card.Last4}; ";
                }
                else if (source is Stripe.Source)
                {
                    var source1 = source as Stripe.Source;
                    if (source1.Card != null)
                    {
                        txtPaymentMethods.Text += $"{source1.Card.Brand} ****{source1.Card.Last4}; ";
                    }
                    else
                    {
                        //MessageBox.Show($"Payment method '{source1.Type}' not implemented ");
                        txtPaymentMethods.Text += $"{source1.Type} ";
                    }
                }
                else
                {
                    MessageBox.Show($"Payment method '{source.GetType()}' not implemented, this is not a problem.");
                    txtPaymentMethods.Text += $"Payment type: {source.GetType()} ";
                }
            }
        }

        private void txtSave_Click(object sender, EventArgs e)
        {
            int subscriptionStatus = (int)((ComboboxItem)comboStatus.SelectedItem).Value;
            if (FormMode == FormModes.New)
            {
                bool isSubscription = IsActiveSubscription();
                if (isSubscription)
                {
                    MessageBox.Show("This customer is already subscribed to this product and plan!");
                    return;
                }

                if (stripeCustomer.Sources.Count() == 0)
                {
                    MessageBox.Show("This customer does not have valid payment methods. The customer will be open in STRIPE web interface where you can add payment method.\n\n After that press the 'Refresh customer' button.");
                    System.Diagnostics.Process.Start(BaseCustomersAddress + stripeCustomer.Id);
                    return;
                }

                Stripe.Plan plan = (Stripe.Plan)((ComboboxItem)comboPlans.SelectedItem).Value;
                int invoiceDayOfMonth = (int)textStartDay.Value;
                Stripe.Subscription subscription = Shared.Source.Stripe.CreateSubscription(StripeApiKey, GetStripeIdOfSelectedApplicant(), plan.Id, invoiceDayOfMonth);                
                try
                {
                    using (var db = new Shared.DbModel())
                    {
                        Db.AddSubscription(
                            invoiceDayOfMonth,
                            db,
                            GetApplicantIdOfSelectedApplicant(),
                            subscription,
                            (Stripe.Product)((ComboboxItem)comboProducts.SelectedItem).Value,
                            textTotalSum.Value,
                            textInitialAmount.Value,
                            subscriptionStatus
                            );

                        db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("The subscription was created successfully IN STRIPE, but was not stored in our database because: " + ex.Message);
                    MessageBox.Show("This error will be emailed to the supporting team!");
                    throw;
                }

                MessageBox.Show("The subscription was created successfully");
                EnableForm(false);
            }
            else if (FormMode == FormModes.Edit)
            {
                MessageBox.Show("Only the total payed so far will be saved, if changed. Subscriptions (product and plan) should not (and can't) be edited.");
                Db.UpdateCustomersTotalPayedSoFar(logger, CustomersSubscriptionId, textInitialAmount.Value,subscriptionStatus);

                MessageBox.Show("The 'Total payed so far' was edited successfully!");

            }
            else
            {
                MessageBox.Show("How did you get here?");
            }
        }

        private void comboPlans_SelectedIndexChanged(object sender, EventArgs e)
        {
            IsActiveSubscription();
        }

        private bool IsActiveSubscription()
        {
            Stripe.Product product = (Stripe.Product)((ComboboxItem)comboProducts.SelectedItem).Value;

            int subsciptionsCount = Shared.Source.Stripe.GetSubscriptionsCountOfCustomerByProduct(StripeApiKey, GetStripeIdOfSelectedApplicant(), product.Id);

            if (subsciptionsCount > 1)
            {
                throw new Exception($"Customer with id: '{GetStripeIdOfSelectedApplicant()}' and name: '{stripeCustomer.Name}' has '{subsciptionsCount} subscriptions to the same plan! This will lead to double+ charing per month! This is a serious issue!'");
            }
            else
            if (subsciptionsCount == 1)
            {
                labelWarning.Text = "Customer is subscribed for that product";
                return true;
            }
            else
            {
                labelWarning.Text = "";
                return false;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshStripeCustomer();
        }

        private void btnShowPlan_Click(object sender, EventArgs e)
        {
            if (comboPlans.SelectedItem == null)
                return;

            System.Diagnostics.Process.Start("https://dashboard.stripe.com/plans/" + ((Stripe.Plan)((ComboboxItem)comboPlans.SelectedItem).Value).Id);
        }

        private void btnShowProduct_Click(object sender, EventArgs e)
        {
            if (comboProducts.SelectedItem == null)
                return;

            System.Diagnostics.Process.Start("https://dashboard.stripe.com/subscriptions/products/" + ((Stripe.Product)((ComboboxItem)comboProducts.SelectedItem).Value).Id);
        }

        private void btnHellpInitialAmount_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                @"This is how much the applicant has already payed. The default is 0.
If the applicant already payed some invoices from other systems then you can enter their combined value here.");
        }

        private void comboApplicants_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshStripeCustomer();
        }

        private void btnAddCharge_Click_1(object sender, EventArgs e)
        {
            if (txtChargeAmmount.Value <= 0m)
            {
                MessageBox.Show("Please enter a charge bigger than $0!");
                return;
            }

            string customerId = GetStripeIdOfSelectedApplicant();

            if (!IsActiveSubscription())
            {
                MessageBox.Show("This customer does not have subscription for this product");
                return;
            }

            Stripe.Product product = (Stripe.Product)((ComboboxItem)comboProducts.SelectedItem).Value;
            Stripe.Subscription subscription = Shared.Source.Stripe.GetSubscriptionsOfCustomerByProduct(StripeApiKey, customerId, product.Id).Single();
            Stripe.Plan plan = (Stripe.Plan)((ComboboxItem)comboPlans.SelectedItem).Value;

            bool isInfinity = decimal.Parse(product.Metadata["total_amount"]) == Shared.Source.Stripe.TOTAL_INFINITY_TO_PAY_SUM;            

            DialogResult result = MessageBox.Show($@"You are about to charge with
${txtChargeAmmount.Value:N2} the customer: {txtStripeCustomerName.Text}
on product: {product.Name}
and plan: {plan.Nickname}

Are you sure you want to continue?",
                                                  "Question",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);
            if (result != DialogResult.Yes)
            {
                return;
            }

            //Create manual invoice
            //Update to be payed total
            PaymentIntent paymentIntent = Shared.Source.Stripe.CreateManualPaymentIntent(
                apiKey: StripeApiKey,
                ammount: txtChargeAmmount.Value,
                customerId: customerId,
                desc: txtChargeReason.Text,
                subscriptionId: subscription.Id,
                planId: plan.Id,
                productId: product.Id
            );

            if (!isInfinity)
            {
                //need to adjust the total to pay ONLY when there is finite ammount to be payed
                try
                {
                    var applicant = Db.IncrementCustomerToPayTotal(logger, CustomersSubscriptionId, txtChargeAmmount.Value);
                    textTotalSum.Value = applicant.TotalTargetSum;
                }
                catch
                {
                    MessageBox.Show("There was an error while updating the TotalTargetSum. The CHARGE is created successfully, but not the TotalTargetSum. Please contact Svetoslav Mitov in order to resolve the issue. Thank you.");
                    throw;
                }
            }



            MessageBox.Show(@"
The charge (payment intent) was created successfully!

You don't need to do anything else! 

Now it will be open in stripe for review.");
            System.Diagnostics.Process.Start($"https://dashboard.stripe.com/payments/{paymentIntent.Id}");
        }
    }
}