﻿namespace ApplicantsApp
{
    partial class FormAudit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridReport = new System.Windows.Forms.DataGridView();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnLoadReport = new System.Windows.Forms.Button();
            this.comboReportTypes = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboApplicants = new System.Windows.Forms.ComboBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.labelProgress = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridResponsibleUserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridApplicantName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridHappenedOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridActionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridReport)).BeginInit();
            this.SuspendLayout();
            // 
            // gridReport
            // 
            this.gridReport.AllowUserToAddRows = false;
            this.gridReport.AllowUserToDeleteRows = false;
            this.gridReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GridId,
            this.GridResponsibleUserName,
            this.GridApplicantName,
            this.GridHappenedOn,
            this.GridActionName,
            this.GridData});
            this.gridReport.Location = new System.Drawing.Point(12, 131);
            this.gridReport.Name = "gridReport";
            this.gridReport.ReadOnly = true;
            this.gridReport.Size = new System.Drawing.Size(1154, 382);
            this.gridReport.TabIndex = 0;
            this.gridReport.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridReport_CellDoubleClick);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(1091, 519);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Location = new System.Drawing.Point(12, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1154, 71);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filters";
            // 
            // btnLoadReport
            // 
            this.btnLoadReport.Location = new System.Drawing.Point(532, 27);
            this.btnLoadReport.Name = "btnLoadReport";
            this.btnLoadReport.Size = new System.Drawing.Size(75, 25);
            this.btnLoadReport.TabIndex = 1;
            this.btnLoadReport.Text = "Load";
            this.btnLoadReport.UseVisualStyleBackColor = true;
            this.btnLoadReport.Click += new System.EventHandler(this.btnLoadReport_Click);
            // 
            // comboReportTypes
            // 
            this.comboReportTypes.DisplayMember = "Text";
            this.comboReportTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboReportTypes.FormattingEnabled = true;
            this.comboReportTypes.Location = new System.Drawing.Point(12, 28);
            this.comboReportTypes.Name = "comboReportTypes";
            this.comboReportTypes.Size = new System.Drawing.Size(206, 21);
            this.comboReportTypes.TabIndex = 3;
            this.comboReportTypes.ValueMember = "Value";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Report type:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(221, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Applicant name:";
            // 
            // comboApplicants
            // 
            this.comboApplicants.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboApplicants.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboApplicants.DisplayMember = "Text";
            this.comboApplicants.FormattingEnabled = true;
            this.comboApplicants.Items.AddRange(new object[] {
            "All applicant actions",
            "Send eMails",
            "Send Faxes"});
            this.comboApplicants.Location = new System.Drawing.Point(224, 28);
            this.comboApplicants.Name = "comboApplicants";
            this.comboApplicants.Size = new System.Drawing.Size(302, 21);
            this.comboApplicants.TabIndex = 4;
            this.comboApplicants.ValueMember = "Value";
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(12, 520);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(848, 23);
            this.progressBar1.TabIndex = 7;
            // 
            // labelProgress
            // 
            this.labelProgress.AutoSize = true;
            this.labelProgress.Location = new System.Drawing.Point(15, 469);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(35, 13);
            this.labelProgress.TabIndex = 8;
            this.labelProgress.Text = "label3";
            this.labelProgress.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Responsible User";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 135;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.FillWeight = 150F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Applicant Name";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 160;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Event Date";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 125;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "What Happened";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 110;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Data";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 500;
            // 
            // GridId
            // 
            this.GridId.HeaderText = "Id";
            this.GridId.Name = "GridId";
            this.GridId.ReadOnly = true;
            this.GridId.Visible = false;
            // 
            // GridResponsibleUserName
            // 
            this.GridResponsibleUserName.HeaderText = "Responsible User";
            this.GridResponsibleUserName.Name = "GridResponsibleUserName";
            this.GridResponsibleUserName.ReadOnly = true;
            this.GridResponsibleUserName.Width = 135;
            // 
            // GridApplicantName
            // 
            this.GridApplicantName.FillWeight = 150F;
            this.GridApplicantName.HeaderText = "Applicant Name";
            this.GridApplicantName.Name = "GridApplicantName";
            this.GridApplicantName.ReadOnly = true;
            this.GridApplicantName.Width = 160;
            // 
            // GridHappenedOn
            // 
            this.GridHappenedOn.HeaderText = "Event Date";
            this.GridHappenedOn.Name = "GridHappenedOn";
            this.GridHappenedOn.ReadOnly = true;
            this.GridHappenedOn.Width = 125;
            // 
            // GridActionName
            // 
            this.GridActionName.HeaderText = "What Happened";
            this.GridActionName.Name = "GridActionName";
            this.GridActionName.ReadOnly = true;
            this.GridActionName.Width = 110;
            // 
            // GridData
            // 
            this.GridData.HeaderText = "Data";
            this.GridData.Name = "GridData";
            this.GridData.ReadOnly = true;
            this.GridData.Width = 500;
            // 
            // FormAudit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1178, 546);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.comboApplicants);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnLoadReport);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboReportTypes);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.gridReport);
            this.Name = "FormAudit";
            this.Text = "Audit";
            this.Load += new System.EventHandler(this.FormAudit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridReport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gridReport;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnLoadReport;
        private System.Windows.Forms.ComboBox comboReportTypes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboApplicants;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label labelProgress;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridId;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridResponsibleUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridApplicantName;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridHappenedOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridActionName;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridData;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
    }
}