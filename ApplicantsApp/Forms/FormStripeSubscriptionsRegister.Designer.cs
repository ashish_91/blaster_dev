﻿namespace ApplicantsApp.Forms
{
    partial class FormStripeSubscriptionsRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormStripeSubscriptionsRegister));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.progressBarText = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.btnClose = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnViewSubscription = new System.Windows.Forms.ToolStripButton();
            this.btnAddNewSubscription = new System.Windows.Forms.ToolStripButton();
            this.btnEditSubscription = new System.Windows.Forms.ToolStripButton();
            this.btnDeleteSubscription = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColApplicantId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColApplicantName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColStripeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDayOfMonthInvoice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColMontlySum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColTotalPayedSoFar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColTargetSum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCreatedOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColStripePlanId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColStripeProductId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColSubscriptionId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnJustRefresh = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColApplicantId,
            this.ColApplicantName,
            this.ColStripeId,
            this.ColProductName,
            this.ColDayOfMonthInvoice,
            this.ColMontlySum,
            this.ColTotalPayedSoFar,
            this.ColTargetSum,
            this.ColCreatedOn,
            this.ColStripePlanId,
            this.ColStripeProductId,
            this.ColSubscriptionId,
            this.ColID});
            this.dataGridView1.Location = new System.Drawing.Point(12, 42);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(1371, 480);
            this.dataGridView1.TabIndex = 0;
            // 
            // progressBarText
            // 
            this.progressBarText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.progressBarText.AutoSize = true;
            this.progressBarText.Location = new System.Drawing.Point(21, 538);
            this.progressBarText.Name = "progressBarText";
            this.progressBarText.Size = new System.Drawing.Size(52, 13);
            this.progressBarText.TabIndex = 22;
            this.progressBarText.Text = "_           _";
            this.progressBarText.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(12, 534);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(955, 23);
            this.progressBar.TabIndex = 21;
            this.progressBar.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnClose.Location = new System.Drawing.Point(1269, 533);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(114, 25);
            this.btnClose.TabIndex = 20;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnViewSubscription,
            this.btnAddNewSubscription,
            this.btnEditSubscription,
            this.btnDeleteSubscription,
            this.toolStripSeparator3});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1395, 39);
            this.toolStrip1.TabIndex = 23;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnViewSubscription
            // 
            this.btnViewSubscription.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnViewSubscription.Image = ((System.Drawing.Image)(resources.GetObject("btnViewSubscription.Image")));
            this.btnViewSubscription.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnViewSubscription.Name = "btnViewSubscription";
            this.btnViewSubscription.Size = new System.Drawing.Size(36, 36);
            this.btnViewSubscription.Text = "View Subscription";
            this.btnViewSubscription.Click += new System.EventHandler(this.btnViewSubscription_Click);
            // 
            // btnAddNewSubscription
            // 
            this.btnAddNewSubscription.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddNewSubscription.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNewSubscription.Image")));
            this.btnAddNewSubscription.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddNewSubscription.Name = "btnAddNewSubscription";
            this.btnAddNewSubscription.Size = new System.Drawing.Size(36, 36);
            this.btnAddNewSubscription.Text = "Add New Subscription";
            this.btnAddNewSubscription.ToolTipText = "New Applicant";
            this.btnAddNewSubscription.Click += new System.EventHandler(this.btnAddNewSubscription_Click);
            // 
            // btnEditSubscription
            // 
            this.btnEditSubscription.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEditSubscription.Image = ((System.Drawing.Image)(resources.GetObject("btnEditSubscription.Image")));
            this.btnEditSubscription.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditSubscription.Name = "btnEditSubscription";
            this.btnEditSubscription.Size = new System.Drawing.Size(36, 36);
            this.btnEditSubscription.Text = "Edit Subscription";
            this.btnEditSubscription.ToolTipText = "Edit Applicant";
            this.btnEditSubscription.Click += new System.EventHandler(this.btnEditSubscription_Click);
            // 
            // btnDeleteSubscription
            // 
            this.btnDeleteSubscription.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDeleteSubscription.Enabled = false;
            this.btnDeleteSubscription.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteSubscription.Image")));
            this.btnDeleteSubscription.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDeleteSubscription.Name = "btnDeleteSubscription";
            this.btnDeleteSubscription.Size = new System.Drawing.Size(36, 36);
            this.btnDeleteSubscription.Text = "Delete Subscription";
            this.btnDeleteSubscription.ToolTipText = "Delete applicant";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnRefresh.ForeColor = System.Drawing.Color.Maroon;
            this.btnRefresh.Location = new System.Drawing.Point(973, 533);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(170, 25);
            this.btnRefresh.TabIndex = 20;
            this.btnRefresh.Text = "STRIPE Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ApplicantId";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Applicant Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 180;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Stripe Id";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Product Name";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Day Of Month";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Montly $";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Total $";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 120;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Target $";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "Created On";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "Plan Id";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "Product Id";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "Subscription Id";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "Id";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.Visible = false;
            // 
            // ColApplicantId
            // 
            this.ColApplicantId.HeaderText = "ApplicantId";
            this.ColApplicantId.Name = "ColApplicantId";
            this.ColApplicantId.ReadOnly = true;
            this.ColApplicantId.Visible = false;
            this.ColApplicantId.Width = 150;
            // 
            // ColApplicantName
            // 
            this.ColApplicantName.HeaderText = "Applicant Name";
            this.ColApplicantName.Name = "ColApplicantName";
            this.ColApplicantName.ReadOnly = true;
            this.ColApplicantName.Width = 180;
            // 
            // ColStripeId
            // 
            this.ColStripeId.HeaderText = "Stripe Id";
            this.ColStripeId.Name = "ColStripeId";
            this.ColStripeId.ReadOnly = true;
            // 
            // ColProductName
            // 
            this.ColProductName.HeaderText = "Product Name";
            this.ColProductName.Name = "ColProductName";
            this.ColProductName.ReadOnly = true;
            // 
            // ColDayOfMonthInvoice
            // 
            this.ColDayOfMonthInvoice.HeaderText = "Day Of Month";
            this.ColDayOfMonthInvoice.Name = "ColDayOfMonthInvoice";
            this.ColDayOfMonthInvoice.ReadOnly = true;
            // 
            // ColMontlySum
            // 
            this.ColMontlySum.HeaderText = "Montly $";
            this.ColMontlySum.Name = "ColMontlySum";
            this.ColMontlySum.ReadOnly = true;
            // 
            // ColTotalPayedSoFar
            // 
            this.ColTotalPayedSoFar.HeaderText = "Total $";
            this.ColTotalPayedSoFar.Name = "ColTotalPayedSoFar";
            this.ColTotalPayedSoFar.ReadOnly = true;
            this.ColTotalPayedSoFar.Width = 120;
            // 
            // ColTargetSum
            // 
            this.ColTargetSum.HeaderText = "Target $";
            this.ColTargetSum.Name = "ColTargetSum";
            this.ColTargetSum.ReadOnly = true;
            // 
            // ColCreatedOn
            // 
            this.ColCreatedOn.HeaderText = "Created On";
            this.ColCreatedOn.Name = "ColCreatedOn";
            this.ColCreatedOn.ReadOnly = true;
            // 
            // ColStripePlanId
            // 
            this.ColStripePlanId.HeaderText = "Plan Id";
            this.ColStripePlanId.Name = "ColStripePlanId";
            this.ColStripePlanId.ReadOnly = true;
            // 
            // ColStripeProductId
            // 
            this.ColStripeProductId.HeaderText = "Product Id";
            this.ColStripeProductId.Name = "ColStripeProductId";
            this.ColStripeProductId.ReadOnly = true;
            // 
            // ColSubscriptionId
            // 
            this.ColSubscriptionId.HeaderText = "Subscription Id";
            this.ColSubscriptionId.Name = "ColSubscriptionId";
            this.ColSubscriptionId.ReadOnly = true;
            // 
            // ColID
            // 
            this.ColID.HeaderText = "Id";
            this.ColID.Name = "ColID";
            this.ColID.ReadOnly = true;
            this.ColID.Visible = false;
            // 
            // btnJustRefresh
            // 
            this.btnJustRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnJustRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnJustRefresh.Location = new System.Drawing.Point(1149, 533);
            this.btnJustRefresh.Name = "btnJustRefresh";
            this.btnJustRefresh.Size = new System.Drawing.Size(114, 25);
            this.btnJustRefresh.TabIndex = 20;
            this.btnJustRefresh.Text = "Just DB Refresh";
            this.btnJustRefresh.UseVisualStyleBackColor = true;
            this.btnJustRefresh.Click += new System.EventHandler(this.btnJustRefresh_Click);
            // 
            // FormStripeSubscriptionsRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1395, 565);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.progressBarText);
            this.Controls.Add(this.btnJustRefresh);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.dataGridView1);
            this.Name = "FormStripeSubscriptionsRegister";
            this.Text = "Stripe Subscriptions Register";
            this.Load += new System.EventHandler(this.FormStripeSubscriptionsRegister_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label progressBarText;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnAddNewSubscription;
        private System.Windows.Forms.ToolStripButton btnEditSubscription;
        private System.Windows.Forms.ToolStripButton btnDeleteSubscription;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btnViewSubscription;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColApplicantId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColApplicantName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColStripeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDayOfMonthInvoice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColMontlySum;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColTotalPayedSoFar;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColTargetSum;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCreatedOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColStripePlanId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColStripeProductId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColSubscriptionId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.Button btnJustRefresh;
    }
}