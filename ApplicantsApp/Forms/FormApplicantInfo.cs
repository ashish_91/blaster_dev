﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using DevExpress.XtraBars;
using ApplicantsApp.Source;
using ApplicantsApp.Models.Applicant.Applicant;
using ApplicantsApp.Models.Applicant.Applicant.Enums;
using Newtonsoft.Json;
using ApplicantsApp.Models;
using System.IO;
using ApplicantsApp.Models.Representative;
using ApplicantsApp.Models.DTOs;
using EnumsNET;
using Newtonsoft.Json.Linq;
using Shared;
using ApplicantsApp.Models.Form;
using Shared.Source.MailGeneration;

namespace ApplicantsApp.Forms
{
    public partial class FormApplicantInfo : XtraForm
    {
        private NLog.Logger logger = NLog.LogManager.GetLogger("applicantInfo");

        public FormModes FormMode { get; set; }
        public ApplicantsNew ApplicantData { get; set; }
        public bool NeedRefresh { get; private set; } = false;

        private List<ApplicantsRepresentatives> representatives;
        private List<ApplicantsDoctors> doctors;
        private FullApplicantInfo applicantModel;
        private List<Countries> countries;
        private List<ApplicantsDocuments> applicantDocuments;
        private Dictionary<Guid, string> documentsTemporatyPaths = new Dictionary<Guid, string>();
        private List<Applicants_DocumentCategories> documentCategories;
        private List<Models.Documents.CustomCategoryDescription> customCategoryDescriptions = new List<Models.Documents.CustomCategoryDescription>();

        private ProgressExpress progress;
        private bool documentsLoaded = false;

        public FormApplicantInfo()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            //richEditControl1.LoadDocument(@"C:\Intel\gp\projects\New Jacob\projects\Jacob Gottlieb\ApplicantsApp\Documents\HIPPA Medical Record Auth 06-10-2019_original.docx", DevExpress.XtraRichEdit.DocumentFormat.Doc);
            //pdfViewer2.LoadDocument(@"C:\Intel\gp\projects\New Jacob\projects\Jacob Gottlieb\ApplicantsApp\Documents\Wireframes(1).pdf"); 
        }

        private void AccordionControlElement6_Click(object sender, EventArgs e)
        {

        }

        private void SimpleButton1_Click(object sender, EventArgs e)
        {

        }

        public FullApplicantInfo FillApplicantModelFromForm()
        {
            //Some validations
            decimal financialBurialAmount = 0;
            if (!string.IsNullOrWhiteSpace(financial_BurialAmount.Text))
            {
                if (!decimal.TryParse(financial_BurialAmount.Text, out financialBurialAmount))
                {
                    if (financial_BurialAmount.CanFocus)
                        financial_BurialAmount.Focus();

                    throw new Exception("The entered numeric value in 'Burial Arrangement' is not valid!");
                }
            }

            var appInfo = new FullApplicantInfo();

            appInfo.autoRFIEmailSettings = new Models.Applicant.Applicant.AutoEmails.AutoRFIEmailSettings()
            {
                enabled = autoEmail_RFI_Enabled.Checked,
                sendIntervalInDays = (int)autoEmail_RFI_SendIntervalInDays.Value,
                addCustomMessage = autoEmail_RFI_AddTopText.Checked,
                onlyMondayToFriday = autoEmail_RFI_SendOnlyMonToFri.Checked,
                customTopTextMessage = autoEmail_RFI_TopTextContent.Text
            };

            appInfo.autoInterimEmailSettings = new Models.Applicant.Applicant.AutoEmails.AutoInterimEmailSettings()
            {
                addCustomMessage = autoEmail_Interim_AddTopText.Checked,
                customTopTextMessage = autoEmail_Interim_TopTextText.Text,
                enabled = autoEmail_Interim_Enabled.Checked,
                onlyMondayToFriday = autoEmail_Interim_MonFriOnly.Checked,
                sendIntervalInDays = (int)autoEmail_Interim_SendIntervalInDays.Value
            };

            appInfo.autoEmailEvents = new Models.Applicant.Applicant.AutoEmails.AutoEmailEvents()
            {
                autoSendMailOnDocumentApproved = autoEmail_SendWhenDocIsApproved.Checked,
                autoSendMailOnDocumenRejected = autoEmail_SendWhenDocIsRejected.Checked,
                autoSendMailOnStatusChanged = autoEmail_SendWhenStatusIsChanged.Checked,
            };

            appInfo.autoLiteRFIEmailSettings = new Models.Applicant.Applicant.AutoEmails.AutoSmaillRFIEmailSettings()
            {
                enabled = autoEmail_LiteRFI_IsEnabled.Checked,
            };

            appInfo.financialCaseWorkerId = Mop.GetComboValue(comboFin);
            appInfo.financialCaseWorkerSupervisorId = Mop.GetComboValue(comboFinSup);
            appInfo.medicalCaseWorkerId = Mop.GetComboValue(comboMed);

            appInfo.medApprovalDate =
                checkMedicalCaseWorker_MedAPprovalDate.Checked ?
                (DateTime?)dateMedicalCaseWorker_MedAPprovalDate.Value.Date :
                null;

            appInfo.personalInfo = new PersonalInfo();

            appInfo.personalInfo.detailedPersonalInfo = new DetailedPersonalInfo()
            {
                firstName = applicant_FirstName.Text,
                middleName = applicant_MiddleName.Text,
                lastName = applicant_LastName.Text,
                dateOfBirth = applicant_DateOfBirth.DateTime,
                ethnicity = applicant_Ethnicity.Text,
                gender = (short?)applicant_Gender.EditValue ?? (short)1,
                isOnAHCCSProgram = applicant_AlreadyOnAHCCCSProgram.Checked,
                isRegisteredVoter = applicant_IsRegisteredVoter.Checked,
                isUSCitizen = applicant_IsUSCitizen.Checked,
                livingAt = applicant_LivingAt.Text,
                placeOfBirth = applicant_PlaceOfBirth.Text,
                SSN = applicant_SSN.Text,
                email = applicant_Email.Text,
                phone = applicant_Phone.Text,
            };

            appInfo.personalInfo.applicantAddress = new Models.Applicant.Applicant.Common.Address()
            {
                addressLine2 = applicant_AddressLine2.Text,
                city = applicant_AddressCity.Text,
                country = applicant_AddressCountry.Text,
                state = applicant_AddressState.Text,
                steerAddress = applicant_AddressStreet.Text,
                zip = applicant_AddressZip.Text,
            };

            appInfo.personalInfo.applicantMailingAddress = new Models.Applicant.Applicant.Common.Address()
            {
                addressLine2 = applicant_MailingAddressLine2.Text,
                zip = applicant_MailingAddressZip.Text,
                city = applicant_MailingAddressCity.Text,
                country = applicant_MailingAddressCountry.Text,
                state = applicant_MailingAddressState.Text,
                steerAddress = applicant_MailingAddressStreet.Text
            };

            appInfo.personalInfo.martialStatus = new MartialStatus()
            {
                daveOfMarriageDivorceOrWidowed = applicant_DateOfMarrgiage.DateTime,
                martialStatus = applicant_MaritalStatus.Text,
                spouseDateOfBirth = applicant_SpouceDateOfBirth.DateTime,
                spouseFirstName = applicant_SpouseFirstName.Text,
                spouseLastName = applicant_SpouseLastName.Text,
                spouseMiddleName = applicant_SpouseMiddleName.Text,
                spouseSSN = applicant_SpouseSSN.Text
            };

            appInfo.personalInfo.militaryService = new MilitaryService()
            {
                IsMilitaryService = applicant_IsMilitary.Checked,
                Branch = applicant_MilitaryBranch.Text,
                FromYear = applicant_MilitaryFromYear.Text,
                ToYear = applicant_MilitaryToYear.Text
            };

            appInfo.pointOfContact = new PointOfContact()
            {
                isPOCAvaliable = poc_IsPOC.Checked,
                address = new Models.Applicant.Applicant.Common.Address()
                {
                    addressLine2 = poc_AddressLine2.Text,
                    city = poc_AddressCity.Text,
                    country = poc_AddressCountry.Text,
                    state = poc_AddressState.Text,
                    steerAddress = poc_AddressStreet.Text,
                    zip = poc_AddressZip.Text
                },
                email = poc_Email.Text,
                firstName = poc_FirstName.Text,
                lastName = poc_LastName.Text,
                middleName = poc_MiddleName.Text,
                phone = poc_Phone.Text,
                relationship = poc_Relationship.Text
            };

            appInfo.ltcCommunity = new LTCCommunity()
            {
                address = new Models.Applicant.Applicant.Common.Address()
                {
                    addressLine2 = ltc_AddressLine2.Text,
                    city = ltc_AddressCity.Text,
                    country = ltc_AddressCountry.Text,
                    state = ltc_AddressState.Text,
                    steerAddress = ltc_AddressStreet.Text,
                    zip = ltc_AddressZip.Text
                },
                dateEntered = ltc_DateEntered.DateTime,
                facilityName = ltc_FacilityName.Text,
                facilityPhone = ltc_FacilityPhone.Text,
                facilityType = ltc_FacilityType.Text,
                IsLtcCommunity = ltc_IsLtcAvaliable.Checked,
                POCFirstName = ltc_CaficlityPocFirstName.Text,
                POCLastName = ltc_FacilityPOCLastName.Text
            };

            appInfo.healthAndMedInfo = new HealthAndMedInfo()
            {
                bathingNotes = helthAndMedical_NotesBathing.Text,
                behaviorNotes = helthAndMedical_NotesBehavior.Text,
                DDDNotes = helthAndMedical_NotesDDD.Text,
                dressingNotes = helthAndMedical_NotesDressing.Text,
                eatingOrMealNotes = helthAndMedical_NotesEating.Text,
                incontinenceNotes = helthAndMedical_NotesIncontinence.Text,

                isAggression = helthAndMedical_IsuesWithAggression.Checked,
                isBathingIssues = helthAndMedical_IsuesWithBathing.Checked,
                isDDD = helthAndMedical_IsuesWithDDD.Checked,
                isDisruptive = helthAndMedical_IsuesWithDisruptive.Checked,
                isDressingIssues = helthAndMedical_IsuesWithDressing.Checked,
                isEatingOrMealIssues = helthAndMedical_IsuesWithEating.Checked,
                isIncontinenceBladder = helthAndMedical_IsuesWithBladder.Checked,
                isIncontinenceBowel = helthAndMedical_IsuesWithBowel.Checked,
                isLegallyBlind = helthAndMedical_IsLegallyBlind.Checked,
                isMobilityissues = helthAndMedical_IsuesWithMobility.Checked,
                isOrientationIssues = helthAndMedical_IsuesWithOrientation.Checked,
                isOther = helthAndMedical_IsuesWithOther.Checked,
                isOxygen = helthAndMedical_IsuesWithOxygen.Checked,
                isParalysisIssues = helthAndMedical_IsuesWithParalysis.Checked,
                isSelfInjury = helthAndMedical_IsuesWithSelfInjury.Checked,
                isToiletingIssues = helthAndMedical_IsuesWithToileting.Checked,
                isTransferringIssues = helthAndMedical_IsuesWithTransferring.Checked,
                isVisionIssues = helthAndMedical_IsuesWithVision.Checked,
                isWandering = helthAndMedical_IsuesWithWandering.Checked,

                mobilityNotes = helthAndMedical_NotesMobility.Text,
                orientationNotes = helthAndMedical_NotesOrientation.Text,
                otherNotes = helthAndMedical_NotesOther.Text,
                oxygenNotes = helthAndMedical_NotesOxygen.Text,
                paralisysNotes = helthAndMedical_NotesParalisys.Text,
                toiletingNotes = helthAndMedical_IsuesWithToileting.Text,
                transferringNotes = helthAndMedical_NotesTransferring.Text,
                visionNotes = helthAndMedical_NotesVision.Text,

                alzheimer = new Alzheimer()
                {
                    diagnosingFirstName = helthAndMedical_AlzheimerFirstName.Text,
                    diagnosingLastName = helthAndMedical_AlzheimerLastName.Text,
                    diagnosisDate = helthAndMedical_AlzheimerDate.DateTime,
                    IsAlzheimer = helthAndMedical_IsuesWithAlzheimer.Checked,
                    physicianField = helthAndMedical_AlzheimerField.Text
                }
            };

            appInfo.finnancialInfo = new FinnancialInfo()
            {
                burialArrangementAmmount = financialBurialAmount,
                homeOwned = new HomeOwned()
                {
                    isHomeOwned = financial_IsOwningHome.Checked,
                    mortgage = financial_IsHomeMortgage.Checked,
                    otherOccupants = financial_IsHomeOtherOccupants.Checked,
                    reverseMortgage = financial_IsHomeReverseMortgage.Checked
                },
                IsBurialArrangement = financial_isBural.Checked,
                isOwnPolicy = financial_IsOwningPolicy.Checked,
                lifeInsuranceInstitution = financial_LifeInstitutionName.Text,
                liveInsuranceCash = financial_LifeCash.Text,
                ownPolicyNotes = financial_PolicyNotes.Text,

                countableResources = new List<CountableResource>(),
                monthlyIncomes = new List<MonthlyIncome>(),
                retirements = new List<Retirement>()
            };

            appInfo.lookBackPeriod = new LookBackPeriod()
            {
                isCarSold = lookback_SoldCarlLast5.Checked,
                isFundsTransferred = lookback_TransferredFunds.Checked,
                isGiftGiven = lookback_GivenCash.Checked,
                isHomeSold = lookback_SoldHomeLast5.Checked,
                otherMajorFinancialCommitments = lookback_OtherNotes.Text
            };

            appInfo.clientNeeds = new ClientNeeds()
            {
                isALTCSApplicant = clientNeeds_ALTCS.Checked,
                isDocumentPreparation = clientNeeds_DocPrep.Checked,
                isPlacement = clientNeeds_Placement.Checked,
                isRealEstate = clientNeeds_RealEstate.Checked,
                isVeteranBenefits = clientNeeds_Veteran.Checked,
                otherNotes = clientNeeds_OtherNotes.Text
            };

            appInfo.representativeId = GetSelectedRepresentativeId();
            appInfo.medicalRepresentativeId = Mop.GetComboValue(comboMedicalRepresentative);

            //THE DE STUFF

            appInfo.currentLivingArrangement = new Models.Applicant.Applicant.de.CurrentLivingArrangement()
            {
                currentLivingType = CustomerCurrentLiving_Hospital.Checked ? CurrentLiving.Hospital :
                        CustomerCurrentLiving_Nursing.Checked ? CurrentLiving.Nursing :
                            CustomerCurrentLiving_Home.Checked ? CurrentLiving.Home :
                                CustomerCurrentLiving_Other.Checked ? CurrentLiving.Other : CurrentLiving.None,

                currentLivingText = CustomerCurrentLivingOther_Text.Text,

                expectedDateOfDIscharge = CustomerCurrentLiving_ExpectedDateOfDischarge.Value,
                facilityName = CustomerCurrentLiving_NameOfHospital.Text,
                phoneNumber = CustomerCurrentLiving_PhoneNumber.Text,

                address = new Models.Applicant.Applicant.Common.Address()
                {
                    addressLine2 = null,
                    city = CustomerCurrentLiving_City.Text,
                    country = "United States",
                    state = CustomerCurrentLiving_State.Text,
                    steerAddress = CustomerCurrentLiving_HospitalAddress.Text,
                    zip = CustomerCurrentLiving_ZipCode.Text
                }
            };

            appInfo.accommodationForPrintedLetters = new Models.Applicant.Applicant.de.AccommodationForPrintedLetters()
            {
                isAlternativeFormatPrinterLetters = AccomodationsForPrintedLetters_Yes.Checked,
                isLargePrint = AccomodationsForPrintedLetters_LargePrint.Checked,
                isReadablePDF = AccomodationsForPrintedLetters_Email.Checked,
                isOther = AccomodationsForPrintedLetters_Other.Checked,
                otherText = AccomodationsForPrintedLetters_OtherText.Text,
                whoNeedsThis = AccomodationsForPrintedLetters_WhoIfYes.Text
            };

            appInfo.additionalQuestions = new Models.Applicant.Applicant.de.AdditionalQuestions()
            {
                isHelpNeededForPayingMedicalBills = AdditionalQuestions_1_Yes.Checked,
                isBef18Autism = AdditionalQuestions_3_Autism.Checked,
                helpPayingMedicalBillsMonths = new List<string>(new string[] { AdditionalQuestions_1_Yes_Month1.Text, AdditionalQuestions_1_Yes_Month2.Text, AdditionalQuestions_1_Yes_Month3.Text }),
                isBef18Cerebral = AdditionalQuestions_3_CerebralPalsy.Checked,
                isBef18IntelectualDisability = AdditionalQuestions_3_Intellectual.Checked,
                isBef18SeizureDisorder = AdditionalQuestions_3_Seizure.Checked,
                isDevelopmentDelayBefore6 = AdditionalQuestions_4_Yes.Checked,
                isReceiveServiceFromDES = AdditionalQuestions_2_Yes.Checked,
                isTrustor = AdditionalQuestions_5_Yes.Checked,
                serviceBegan = AdditionalQuestions_2_Yes_Date.Value,
                isPregnant = isPregnant_Yes.Checked,
            };

            appInfo.interviewInfo = new Models.Applicant.Applicant.de.InterviewInfo()
            {
                fridayTime = Interview_Friday_Time.Text,
                interpreterLanguage = InterviewInterpretter_Language.Text,
                isFriday = Interview_Friday.Checked,
                isInterpreterNeeded = InterviewInterpretter_Yes.Checked,
                isMonday = Interview_Monday.Checked,
                isThursday = Interview_Thursday.Checked,
                isTuesday = Interview_Tuesday.Checked,
                isWednesday = Interview_Wednesday.Checked,
                mondayTime = Interview_Monday_Time.Text,
                thursdayTime = Interview_Thursday_Time.Text,
                tuesdayTime = Interview_Tuesday_Time.Text,
                wednesdayTime = Interview_Wednesday_Time.Text,
                interpreterHomeAddress = InterviewInterpretter_HomeAddress.Text,
                interpreterMajorCrossRoads = InterviewInterpretter_MajorCrossroads.Text
            };

            appInfo.personCompletingDeForm = new Models.Applicant.Applicant.de.PersonCompletingDeForm()
            {
                name = PersonCompleteingTheForm_Name.Text,
                phone = PersonCompleteingTheForm_Phone.Text,
                whoIsCompletingTheForm =
                        PersonCompleteingTheForm_IsCustomer.Checked ? CompletingFormPersonType.Customer :
                            PersonCompleteingTheForm_IsSpouse.Checked ? CompletingFormPersonType.Spouse :
                                PersonCompleteingTheForm_IsParent.Checked ? CompletingFormPersonType.Parent : CompletingFormPersonType.None
            };

            //THE MEDICAL STUFF  
            appInfo.disclosedInformation = new Models.Applicant.Applicant.Medical.DisclosedInformation()
            {
                disclosedToEmail = txtMedical_Disclosed_OtherEmail.Text,
                isAllInfo = checkMedical_Disclosed_All.Checked,
                isCertainCondition = checkMedical_Disclosed_Specific.Checked,
                isForPeriod = checkMedical_Disclosed_Period.Checked,
                isOther = checkMedical_Disclosed_Other.Checked,
                otherText = txtMedical_Disclosed_OtherText.Text,
                periodFrom = dateMedical_Disclosed_PeriodFrom.Value,
                periodTo = dateMedical_Disclosed_PeriodTo.Value,
                theCondition = txtMedical_Disclosed_SpecificText.Text
            };

            appInfo.authorizationPurpose = new Models.Applicant.Applicant.Medical.AuthorizationPurpose()
            {
                authToCommunicate = checkMedical_Purpose_Comunicate.Checked,
                authToSell = checkMedical_Purpose_Sell.Checked,
                isAtMyRequest = checkMedical_Purpose_MyRequest.Checked,
                isOther = checkMedical_Purpose_Other.Checked,
                otherText = txtMedical_Purpose_OtherText.Text
            };

            appInfo.endOfAuth = new Models.Applicant.Applicant.Medical.EndOfAuth()
            {
                endOn = dateMedical_AuthEnd_date.Value,
                isOnDate = checkMedical_AuthEnd_OnDate.Checked,
                isOnEvent = checkMedical_AuthEnd_Event.Checked,
                theEvent = txtMedical_AuthEnd_EventText.Text
            };

            appInfo.patientRights = new Models.Applicant.Applicant.Medical.PatientRights()
            {
                authRepSignDate = dateMedical_Rights_AuthorizedRep_SignDate.Value,
                isPatientMinor = checkMedical_Rights_SubMinor.Checked,
                isPatientMinorOrUnableToSign = checkMedical_Rights_UnableToSign.Checked,
                isRepCourtOrder = checkMedical_Rights_CountOrder.Checked,
                isRepGuardian = checkMedical_Rights_LegalGuardian.Checked,
                isRepOther = checkMedical_Rights_Other.Checked,
                isRepParent = checkMedical_Rights_Parent.Checked,
                isUnableToSign = checkMedical_Rights_CantSign.Checked,
                patientMinorAge = txtMedical_Rights_MinorAge.Text,
                patientSignatureDate = dateMedical_Rights_date.Value,
                repAuthOtherText = txtMedical_Rights_OtherText.Text,
                unableToSignBecause = txtMedical_Rights_UnableToSignText.Text
            };

            appInfo.additionalConsent = new Models.Applicant.Applicant.Medical.AdditionalConsentForConditions()
            {
                allowRelease = checkMedical_AddCon_IDo.Checked,
                notAllowRelease = checkMedical_AddCon_IDoNot.Checked,
                date = dateMedical_AddCon_Date.Value.Date + dateMedical_AddCon_Time.Value.TimeOfDay,
            };

            appInfo.additionalConsentForHIV = new Models.Applicant.Applicant.Medical.AdditionalCOnsentForHIV()
            {
                allowRelease = checkMedical_HIV_IDo.Checked,
                date = dateMedical_HIV_date.Value.Date + dateMedical_HIV_date.Value.TimeOfDay,
                notAllowRelease = checkMedical_HIV_IDoNot.Checked
            };

            //Countable Resource
            appInfo.finnancialInfo.countableResources = new List<CountableResource>();
            foreach (DataGridViewRow row in financial_gridResourceAccounts.Rows)
            {
                string val = row.Cells[2]?.Value?.ToString();
                string institution = row.Cells[1]?.Value?.ToString();
                string accountType = row.Cells[0]?.Value?.ToString();

                if (val is null && institution is null && accountType is null)
                    continue;

                decimal sum;
                if (!decimal.TryParse(val, out sum))
                {
                    if (financial_gridResourceAccounts.CanFocus)
                        financial_gridResourceAccounts.Focus();

                    throw new Exception($"The entered Ballance in 'COUNTABLE RESOURCES' - '{val}' is not a valid number");
                }

                if (string.IsNullOrWhiteSpace(institution))
                {
                    if (financial_gridResourceAccounts.CanFocus)
                        financial_gridResourceAccounts.Focus();

                    throw new Exception($"Please enter 'Institution' in 'COUNTABLE RESOURCES' ");
                }

                if (string.IsNullOrWhiteSpace(accountType))
                {
                    if (financial_gridResourceAccounts.CanFocus)
                        financial_gridResourceAccounts.Focus();

                    throw new Exception($"Please select 'Account Type' in 'COUNTABLE RESOURCES' ");
                }

                appInfo.finnancialInfo.countableResources.Add(new CountableResource()
                {
                    ballance = sum,
                    institution = institution,
                    type = accountType
                });
            }

            //Income
            appInfo.finnancialInfo.monthlyIncomes = new List<MonthlyIncome>();
            foreach (DataGridViewRow row in financial_gridIncome.Rows)
            {
                if (row.Cells[0].Value == null)
                    continue;

                string val = row.Cells[1]?.Value?.ToString();
                decimal sum;
                if (!decimal.TryParse(val, out sum))
                {
                    if (financial_gridIncome.CanFocus)
                        financial_gridIncome.Focus();

                    throw new Exception($"The entered value in 'INCOME' - '{val}' is not a valid number");
                }

                appInfo.finnancialInfo.monthlyIncomes.Add(new MonthlyIncome()
                {
                    source = row.Cells[0].Value.ToString(),
                    ammount = sum
                });
            }

            //Retirement
            appInfo.finnancialInfo.retirements = new List<Retirement>();
            foreach (DataGridViewRow row in financial_gridRetirement.Rows)
            {
                if (row.Cells[0].Value == null)
                    continue;

                string val = row.Cells[2].Value.ToString();
                decimal sum;
                if (!decimal.TryParse(val, out sum))
                {
                    if (financial_gridRetirement.CanFocus)
                        financial_gridRetirement.Focus();

                    throw new Exception($"The entered value in 'Retirement (IRA 401k, Pension)' - '{val}' is not a valid number");
                }

                appInfo.finnancialInfo.retirements.Add(new Retirement()
                {
                    ballance = sum,
                    institution = row.Cells[1].Value.ToString(),
                    retirement = row.Cells[0].Value.ToString(),
                });
            }

            //vehicles
            appInfo.finnancialInfo.vehicles = new List<Vehicle>();
            foreach (DataGridViewRow row in financial_vehicles.Rows)
            {
                if (row.Cells[0].Value == null)
                    continue;

                appInfo.finnancialInfo.vehicles.Add(new Vehicle()
                {
                    make = row.Cells[0].Value.ToString(),
                    model = row.Cells[1].Value.ToString(),
                    year = row.Cells[2].Value.ToString(),
                    milage = row.Cells[3].Value.ToString()
                });
            }

            //medical
            appInfo.authorizedPartiesIds = new List<Guid>();
            foreach (DataGridViewRow row in gridMedical_Parties.Rows)
            {
                if (row.Cells[0].Value == null)
                    continue;

                appInfo.authorizedPartiesIds.Add(Guid.Parse(row.Cells[0].Value.ToString()));
            }

            return appInfo;
        }



        public void FillFormFromModel(FullApplicantInfo appInfo)
        {
            ///PersonalInfo
            ///

            //Auto Emails
            autoEmail_RFI_Enabled.Checked = appInfo.autoRFIEmailSettings?.enabled ?? false;
            autoEmail_RFI_SendIntervalInDays.Value = appInfo.autoRFIEmailSettings?.sendIntervalInDays ?? 1;
            autoEmail_RFI_AddTopText.Checked = appInfo.autoRFIEmailSettings?.addCustomMessage ?? false;
            autoEmail_RFI_SendOnlyMonToFri.Checked = appInfo.autoRFIEmailSettings?.onlyMondayToFriday ?? false;
            autoEmail_RFI_TopTextContent.Text = appInfo.autoRFIEmailSettings?.customTopTextMessage;

            autoEmail_Interim_AddTopText.Checked = appInfo.autoInterimEmailSettings?.addCustomMessage ?? false;
            autoEmail_Interim_TopTextText.Text = appInfo.autoInterimEmailSettings?.customTopTextMessage;
            autoEmail_Interim_Enabled.Checked = appInfo.autoInterimEmailSettings?.enabled ?? false;
            autoEmail_Interim_MonFriOnly.Checked = appInfo.autoInterimEmailSettings?.onlyMondayToFriday ?? false;
            autoEmail_Interim_SendIntervalInDays.Value = appInfo.autoInterimEmailSettings?.sendIntervalInDays ?? 1;

            autoEmail_SendWhenDocIsApproved.Checked = appInfo.autoEmailEvents?.autoSendMailOnDocumentApproved ?? false;
            autoEmail_SendWhenDocIsRejected.Checked = appInfo.autoEmailEvents?.autoSendMailOnDocumenRejected ?? false;
            autoEmail_SendWhenStatusIsChanged.Checked = appInfo.autoEmailEvents?.autoSendMailOnStatusChanged ?? false;

            autoEmail_LiteRFI_IsEnabled.Checked = appInfo.autoLiteRFIEmailSettings?.enabled ?? false;

            HandleAutoEmailEvents();

            //Case Worker
            Mop.SetComboItemByValue(comboFin, appInfo.financialCaseWorkerId);
            Mop.SetComboItemByValue(comboFinSup, appInfo.financialCaseWorkerSupervisorId);
            Mop.SetComboItemByValue(comboMed, appInfo.medicalCaseWorkerId);

            checkMedicalCaseWorker_MedAPprovalDate.Checked = appInfo.medApprovalDate.HasValue;
            if (appInfo.medApprovalDate.HasValue)
            {
                dateMedicalCaseWorker_MedAPprovalDate.Value = appInfo.medApprovalDate.Value;
            }
            else
            {
                dateMedicalCaseWorker_MedAPprovalDate.Value = DateTime.Now;
            }

            //DetailedPersonalInfo
            applicant_FirstName.Text = appInfo.personalInfo.detailedPersonalInfo.firstName;
            applicant_MiddleName.Text = appInfo.personalInfo.detailedPersonalInfo.middleName;
            applicant_LastName.Text = appInfo.personalInfo.detailedPersonalInfo.lastName;
            applicant_DateOfBirth.DateTime = appInfo.personalInfo.detailedPersonalInfo.dateOfBirth;
            applicant_Ethnicity.Text = appInfo.personalInfo.detailedPersonalInfo.ethnicity;
            applicant_Gender.EditValue = appInfo.personalInfo.detailedPersonalInfo.gender;
            applicant_AlreadyOnAHCCCSProgram.Checked = appInfo.personalInfo.detailedPersonalInfo.isOnAHCCSProgram;
            applicant_IsRegisteredVoter.Checked = appInfo.personalInfo.detailedPersonalInfo.isRegisteredVoter;
            applicant_IsUSCitizen.Checked = appInfo.personalInfo.detailedPersonalInfo.isUSCitizen;
            applicant_LivingAt.Text = appInfo.personalInfo.detailedPersonalInfo.livingAt;
            applicant_PlaceOfBirth.Text = appInfo.personalInfo.detailedPersonalInfo.placeOfBirth;
            applicant_SSN.Text = appInfo.personalInfo.detailedPersonalInfo.SSN;
            applicant_Email.Text = appInfo.personalInfo.detailedPersonalInfo.email;
            applicant_Phone.Text = appInfo.personalInfo.detailedPersonalInfo.phone;

            //applicantAddress
            applicant_AddressLine2.Text = appInfo.personalInfo.applicantAddress.addressLine2;
            applicant_AddressCity.Text = appInfo.personalInfo.applicantAddress.city;
            applicant_AddressCountry.Text = appInfo.personalInfo.applicantAddress.country;
            applicant_AddressState.Text = appInfo.personalInfo.applicantAddress.state;
            applicant_AddressStreet.Text = appInfo.personalInfo.applicantAddress.steerAddress;
            applicant_AddressZip.Text = appInfo.personalInfo.applicantAddress.zip;

            //applicantMailingAddress
            applicant_MailingAddressLine2.Text = appInfo.personalInfo.applicantMailingAddress.addressLine2;
            applicant_MailingAddressZip.Text = appInfo.personalInfo.applicantMailingAddress.zip;
            applicant_MailingAddressCity.Text = appInfo.personalInfo.applicantMailingAddress.city;
            applicant_MailingAddressCountry.Text = appInfo.personalInfo.applicantMailingAddress.country;
            applicant_MailingAddressState.Text = appInfo.personalInfo.applicantMailingAddress.state;
            applicant_MailingAddressStreet.Text = appInfo.personalInfo.applicantMailingAddress.steerAddress;

            //MartialStatus
            applicant_DateOfMarrgiage.DateTime = appInfo.personalInfo.martialStatus?.daveOfMarriageDivorceOrWidowed ?? new DateTime(1900, 1, 1);
            applicant_MaritalStatus.Text = appInfo.personalInfo.martialStatus?.martialStatus;
            applicant_SpouceDateOfBirth.DateTime = appInfo.personalInfo.martialStatus?.spouseDateOfBirth ?? new DateTime(1900, 1, 1);
            applicant_SpouseFirstName.Text = appInfo.personalInfo.martialStatus?.spouseFirstName;
            applicant_SpouseLastName.Text = appInfo.personalInfo.martialStatus?.spouseLastName;
            applicant_SpouseMiddleName.Text = appInfo.personalInfo.martialStatus?.spouseMiddleName;
            applicant_SpouseSSN.Text = appInfo.personalInfo.martialStatus?.spouseSSN;

            //militaryService
            applicant_IsMilitary.Checked = appInfo.personalInfo.militaryService?.IsMilitaryService ?? false;
            applicant_MilitaryBranch.Text = appInfo.personalInfo.militaryService?.Branch;
            applicant_MilitaryFromYear.Text = appInfo.personalInfo.militaryService?.FromYear.ToString();
            applicant_MilitaryToYear.Text = appInfo.personalInfo.militaryService?.ToYear.ToString();

            ///pointOfContact
            ///
            poc_IsPOC.Checked = appInfo.pointOfContact?.isPOCAvaliable ?? false;
            poc_Email.Text = appInfo.pointOfContact?.email;
            poc_FirstName.Text = appInfo.pointOfContact?.firstName;
            poc_LastName.Text = appInfo.pointOfContact?.lastName;
            poc_MiddleName.Text = appInfo.pointOfContact?.middleName;
            poc_Phone.Text = appInfo.pointOfContact?.phone;
            poc_Relationship.Text = appInfo.pointOfContact?.relationship;

            poc_AddressLine2.Text = appInfo.pointOfContact?.address.addressLine2;
            poc_AddressCity.Text = appInfo.pointOfContact?.address.city;
            poc_AddressCountry.Text = appInfo.pointOfContact?.address.country;
            poc_AddressState.Text = appInfo.pointOfContact?.address.state;
            poc_AddressStreet.Text = appInfo.pointOfContact?.address.steerAddress;
            poc_AddressZip.Text = appInfo.pointOfContact?.address.zip;

            ///LTCCommunity
            ///


            ltc_AddressLine2.Text = appInfo.ltcCommunity?.address.addressLine2;
            ltc_AddressCity.Text = appInfo.ltcCommunity?.address.city;
            ltc_AddressCountry.Text = appInfo.ltcCommunity?.address.country;
            ltc_AddressState.Text = appInfo.ltcCommunity?.address.state;
            ltc_AddressStreet.Text = appInfo.ltcCommunity?.address.steerAddress;
            ltc_AddressZip.Text = appInfo.ltcCommunity?.address.zip;

            ltc_DateEntered.DateTime = appInfo.ltcCommunity?.dateEntered ?? new DateTime(1900, 1, 1);
            ltc_FacilityName.Text = appInfo.ltcCommunity?.facilityName;
            ltc_FacilityPhone.Text = appInfo.ltcCommunity?.facilityPhone;
            ltc_FacilityType.Text = appInfo.ltcCommunity?.facilityType;
            ltc_IsLtcAvaliable.Checked = appInfo.ltcCommunity?.IsLtcCommunity ?? false;
            ltc_CaficlityPocFirstName.Text = appInfo.ltcCommunity?.POCFirstName;
            ltc_FacilityPOCLastName.Text = appInfo.ltcCommunity?.POCLastName;

            ///HealthAndMedInfo
            ///

            helthAndMedical_NotesBathing.Text = appInfo.healthAndMedInfo?.bathingNotes;
            helthAndMedical_NotesBehavior.Text = appInfo.healthAndMedInfo?.behaviorNotes;
            helthAndMedical_NotesDDD.Text = appInfo.healthAndMedInfo?.DDDNotes;
            helthAndMedical_NotesDressing.Text = appInfo.healthAndMedInfo?.dressingNotes;
            helthAndMedical_NotesEating.Text = appInfo.healthAndMedInfo?.eatingOrMealNotes;
            helthAndMedical_NotesIncontinence.Text = appInfo.healthAndMedInfo?.incontinenceNotes;

            helthAndMedical_IsuesWithAggression.Checked = appInfo.healthAndMedInfo?.isAggression ?? false;
            helthAndMedical_IsuesWithBathing.Checked = appInfo.healthAndMedInfo?.isBathingIssues ?? false;
            helthAndMedical_IsuesWithDDD.Checked = appInfo.healthAndMedInfo?.isDDD ?? false;
            helthAndMedical_IsuesWithDisruptive.Checked = appInfo.healthAndMedInfo?.isDisruptive ?? false;
            helthAndMedical_IsuesWithDressing.Checked = appInfo.healthAndMedInfo?.isDressingIssues ?? false;
            helthAndMedical_IsuesWithEating.Checked = appInfo.healthAndMedInfo?.isEatingOrMealIssues ?? false;
            helthAndMedical_IsuesWithBladder.Checked = appInfo.healthAndMedInfo?.isIncontinenceBladder ?? false;
            helthAndMedical_IsuesWithBowel.Checked = appInfo.healthAndMedInfo?.isIncontinenceBowel ?? false;
            helthAndMedical_IsLegallyBlind.Checked = appInfo.healthAndMedInfo?.isLegallyBlind ?? false;
            helthAndMedical_IsuesWithMobility.Checked = appInfo.healthAndMedInfo?.isMobilityissues ?? false;
            helthAndMedical_IsuesWithOrientation.Checked = appInfo.healthAndMedInfo?.isOrientationIssues ?? false;
            helthAndMedical_IsuesWithOther.Checked = appInfo.healthAndMedInfo?.isOther ?? false;
            helthAndMedical_IsuesWithOxygen.Checked = appInfo.healthAndMedInfo?.isOxygen ?? false;
            helthAndMedical_IsuesWithParalysis.Checked = appInfo.healthAndMedInfo?.isParalysisIssues ?? false;
            helthAndMedical_IsuesWithSelfInjury.Checked = appInfo.healthAndMedInfo?.isSelfInjury ?? false;
            helthAndMedical_IsuesWithToileting.Checked = appInfo.healthAndMedInfo?.isToiletingIssues ?? false;
            helthAndMedical_IsuesWithTransferring.Checked = appInfo.healthAndMedInfo?.isTransferringIssues ?? false;
            helthAndMedical_IsuesWithVision.Checked = appInfo.healthAndMedInfo?.isVisionIssues ?? false;
            helthAndMedical_IsuesWithWandering.Checked = appInfo.healthAndMedInfo?.isWandering ?? false;

            helthAndMedical_NotesMobility.Text = appInfo.healthAndMedInfo?.mobilityNotes;
            helthAndMedical_NotesOrientation.Text = appInfo.healthAndMedInfo?.orientationNotes;
            helthAndMedical_NotesOther.Text = appInfo.healthAndMedInfo?.otherNotes;
            helthAndMedical_NotesOxygen.Text = appInfo.healthAndMedInfo?.oxygenNotes;
            helthAndMedical_NotesParalisys.Text = appInfo.healthAndMedInfo?.paralisysNotes;
            helthAndMedical_IsuesWithToileting.Text = appInfo.healthAndMedInfo?.toiletingNotes;
            helthAndMedical_NotesTransferring.Text = appInfo.healthAndMedInfo?.transferringNotes;
            helthAndMedical_NotesVision.Text = appInfo.healthAndMedInfo?.visionNotes;

            //Alzheimer
            helthAndMedical_AlzheimerFirstName.Text = appInfo.healthAndMedInfo?.alzheimer.diagnosingFirstName;
            helthAndMedical_AlzheimerLastName.Text = appInfo.healthAndMedInfo?.alzheimer.diagnosingLastName;
            helthAndMedical_AlzheimerDate.DateTime = appInfo.healthAndMedInfo?.alzheimer.diagnosisDate ?? new DateTime(1900, 1, 1);
            helthAndMedical_IsuesWithAlzheimer.Checked = appInfo.healthAndMedInfo?.alzheimer.IsAlzheimer ?? false;
            helthAndMedical_AlzheimerField.Text = appInfo.healthAndMedInfo?.alzheimer.physicianField;

            ///finnancialInfo
            ///
            financial_BurialAmount.Text = appInfo.finnancialInfo?.burialArrangementAmmount.ToString();

            financial_IsOwningHome.Checked = appInfo.finnancialInfo?.homeOwned.isHomeOwned ?? false;
            financial_IsHomeMortgage.Checked = appInfo.finnancialInfo?.homeOwned.mortgage ?? false;
            financial_IsHomeOtherOccupants.Checked = appInfo.finnancialInfo?.homeOwned.otherOccupants ?? false;
            financial_IsHomeReverseMortgage.Checked = appInfo.finnancialInfo?.homeOwned.reverseMortgage ?? false;

            financial_isBural.Checked = appInfo.finnancialInfo?.IsBurialArrangement ?? false;
            financial_IsOwningPolicy.Checked = appInfo.finnancialInfo?.isOwnPolicy ?? false;
            financial_LifeInstitutionName.Text = appInfo.finnancialInfo?.lifeInsuranceInstitution;
            financial_LifeCash.Text = appInfo.finnancialInfo?.liveInsuranceCash;
            financial_PolicyNotes.Text = appInfo.finnancialInfo?.ownPolicyNotes;

            ///LookBackPeriod
            ///

            lookback_SoldCarlLast5.Checked = appInfo.lookBackPeriod?.isCarSold ?? false;
            lookback_TransferredFunds.Checked = appInfo.lookBackPeriod?.isFundsTransferred ?? false;
            lookback_GivenCash.Checked = appInfo.lookBackPeriod?.isGiftGiven ?? false;
            lookback_SoldHomeLast5.Checked = appInfo.lookBackPeriod?.isHomeSold ?? false;
            lookback_OtherNotes.Text = appInfo.lookBackPeriod?.otherMajorFinancialCommitments;

            ///ClientNeeds
            ///
            clientNeeds_ALTCS.Checked = appInfo.clientNeeds?.isALTCSApplicant ?? false;
            clientNeeds_DocPrep.Checked = appInfo.clientNeeds?.isDocumentPreparation ?? false;
            clientNeeds_Placement.Checked = appInfo.clientNeeds?.isPlacement ?? false;
            clientNeeds_RealEstate.Checked = appInfo.clientNeeds?.isRealEstate ?? false;
            clientNeeds_Veteran.Checked = appInfo.clientNeeds?.isVeteranBenefits ?? false;
            clientNeeds_OtherNotes.Text = appInfo.clientNeeds?.otherNotes;

            //////////////
            ///DE
            ///DE CurrentLivingArrangement
            ///
            CustomerCurrentLiving_Hospital.Checked = appInfo.currentLivingArrangement?.currentLivingType == CurrentLiving.Hospital;
            CustomerCurrentLiving_Nursing.Checked = appInfo.currentLivingArrangement?.currentLivingType == CurrentLiving.Nursing;
            CustomerCurrentLiving_Home.Checked = appInfo.currentLivingArrangement?.currentLivingType == CurrentLiving.Home;
            CustomerCurrentLiving_Other.Checked = appInfo.currentLivingArrangement?.currentLivingType == CurrentLiving.Other;
            CustomerCurrentLivingOther_Text.Text = appInfo.currentLivingArrangement?.currentLivingText;

            CustomerCurrentLiving_ExpectedDateOfDischarge.Value = appInfo.currentLivingArrangement?.expectedDateOfDIscharge ?? DateTime.Now;
            CustomerCurrentLiving_NameOfHospital.Text = appInfo.currentLivingArrangement?.facilityName;
            CustomerCurrentLiving_PhoneNumber.Text = appInfo.currentLivingArrangement?.phoneNumber;

            CustomerCurrentLiving_City.Text = appInfo.currentLivingArrangement?.address.city;
            CustomerCurrentLiving_State.Text = appInfo.currentLivingArrangement?.address.state;
            CustomerCurrentLiving_HospitalAddress.Text = appInfo.currentLivingArrangement?.address.steerAddress;
            CustomerCurrentLiving_ZipCode.Text = appInfo.currentLivingArrangement?.address.zip;

            //AccommodationForPrintedLetters
            AccomodationsForPrintedLetters_Yes.Checked = appInfo.accommodationForPrintedLetters?.isAlternativeFormatPrinterLetters ?? false;
            AccomodationsForPrintedLetters_LargePrint.Checked = appInfo.accommodationForPrintedLetters?.isLargePrint ?? false;
            AccomodationsForPrintedLetters_Email.Checked = appInfo.accommodationForPrintedLetters?.isReadablePDF ?? false;
            AccomodationsForPrintedLetters_Other.Checked = appInfo.accommodationForPrintedLetters?.isOther ?? false;
            AccomodationsForPrintedLetters_OtherText.Text = appInfo.accommodationForPrintedLetters?.otherText;
            AccomodationsForPrintedLetters_WhoIfYes.Text = appInfo.accommodationForPrintedLetters?.whoNeedsThis;

            //AdditionalQuestions
            AdditionalQuestions_1_Yes.Checked = appInfo.additionalQuestions?.isHelpNeededForPayingMedicalBills ?? false;
            AdditionalQuestions_3_Autism.Checked = appInfo.additionalQuestions?.isBef18Autism ?? false;
            AdditionalQuestions_3_CerebralPalsy.Checked = appInfo.additionalQuestions?.isBef18Cerebral ?? false;
            AdditionalQuestions_3_Intellectual.Checked = appInfo.additionalQuestions?.isBef18IntelectualDisability ?? false;
            AdditionalQuestions_3_Seizure.Checked = appInfo.additionalQuestions?.isBef18SeizureDisorder ?? false;
            AdditionalQuestions_4_Yes.Checked = appInfo.additionalQuestions?.isDevelopmentDelayBefore6 ?? false;
            AdditionalQuestions_2_Yes.Checked = appInfo.additionalQuestions?.isReceiveServiceFromDES ?? false;
            AdditionalQuestions_5_Yes.Checked = appInfo.additionalQuestions?.isTrustor ?? false;
            AdditionalQuestions_2_Yes_Date.Value = appInfo.additionalQuestions?.serviceBegan ?? DateTime.Now;
            isPregnant_Yes.Checked = appInfo.additionalQuestions?.isPregnant ?? false;

            AdditionalQuestions_1_Yes_Month1.Text =
                appInfo.additionalQuestions?.helpPayingMedicalBillsMonths?.Count() > 0 ?
                    appInfo.additionalQuestions?.helpPayingMedicalBillsMonths[0] :
                    string.Empty;

            AdditionalQuestions_1_Yes_Month2.Text =
                appInfo.additionalQuestions?.helpPayingMedicalBillsMonths?.Count() > 1 ?
                    appInfo.additionalQuestions?.helpPayingMedicalBillsMonths[1] :
                    string.Empty;

            AdditionalQuestions_1_Yes_Month3.Text =
                appInfo.additionalQuestions?.helpPayingMedicalBillsMonths?.Count() > 2 ?
                    appInfo.additionalQuestions?.helpPayingMedicalBillsMonths[2] :
                    string.Empty;

            //Interview Info
            Interview_Friday_Time.Text = appInfo.interviewInfo?.fridayTime;
            InterviewInterpretter_Language.Text = appInfo.interviewInfo?.interpreterLanguage;
            Interview_Friday.Checked = appInfo.interviewInfo?.isFriday ?? false;
            InterviewInterpretter_Yes.Checked = appInfo.interviewInfo?.isInterpreterNeeded ?? false;
            Interview_Monday.Checked = appInfo.interviewInfo?.isMonday ?? false;
            Interview_Thursday.Checked = appInfo.interviewInfo?.isThursday ?? false;
            Interview_Tuesday.Checked = appInfo.interviewInfo?.isTuesday ?? false;
            Interview_Wednesday.Checked = appInfo.interviewInfo?.isWednesday ?? false;
            Interview_Monday_Time.Text = appInfo.interviewInfo?.mondayTime;
            Interview_Thursday_Time.Text = appInfo.interviewInfo?.thursdayTime;
            Interview_Tuesday_Time.Text = appInfo.interviewInfo?.tuesdayTime;
            Interview_Wednesday_Time.Text = appInfo.interviewInfo?.wednesdayTime;
            InterviewInterpretter_HomeAddress.Text = appInfo.interviewInfo?.interpreterHomeAddress;
            InterviewInterpretter_MajorCrossroads.Text = appInfo.interviewInfo?.interpreterMajorCrossRoads;

            //PersonCompletingDeForm
            PersonCompleteingTheForm_Name.Text = appInfo.personCompletingDeForm?.name;
            PersonCompleteingTheForm_Phone.Text = appInfo.personCompletingDeForm?.phone;
            PersonCompleteingTheForm_IsCustomer.Checked = appInfo.personCompletingDeForm?.whoIsCompletingTheForm == CompletingFormPersonType.Customer;
            PersonCompleteingTheForm_IsParent.Checked = appInfo.personCompletingDeForm?.whoIsCompletingTheForm == CompletingFormPersonType.Parent;
            PersonCompleteingTheForm_IsSpouse.Checked = appInfo.personCompletingDeForm?.whoIsCompletingTheForm == CompletingFormPersonType.Spouse;


            ///////////////
            ///THE MEDICAL STUFF  
            //
            //DisclosedInformation
            txtMedical_Disclosed_OtherEmail.Text = appInfo.disclosedInformation?.disclosedToEmail;
            checkMedical_Disclosed_All.Checked = appInfo.disclosedInformation?.isAllInfo ?? false;
            checkMedical_Disclosed_Specific.Checked = appInfo.disclosedInformation?.isCertainCondition ?? false;
            checkMedical_Disclosed_Period.Checked = appInfo.disclosedInformation?.isForPeriod ?? false;
            checkMedical_Disclosed_Other.Checked = appInfo.disclosedInformation?.isOther ?? false;
            txtMedical_Disclosed_OtherText.Text = appInfo.disclosedInformation?.otherText;
            dateMedical_Disclosed_PeriodFrom.Value = (appInfo.disclosedInformation?.periodFrom == DateTime.MinValue ? DateTime.Now : appInfo.disclosedInformation?.periodFrom) ?? DateTime.Now;
            dateMedical_Disclosed_PeriodTo.Value = (appInfo.disclosedInformation?.periodTo == DateTime.MinValue ? DateTime.Now : appInfo.disclosedInformation?.periodTo) ?? DateTime.Now;
            txtMedical_Disclosed_SpecificText.Text = appInfo.disclosedInformation?.theCondition;

            //AuthorizationPurpose
            checkMedical_Purpose_Comunicate.Checked = appInfo.authorizationPurpose?.authToCommunicate ?? false;
            checkMedical_Purpose_Sell.Checked = appInfo.authorizationPurpose?.authToSell ?? false;
            checkMedical_Purpose_MyRequest.Checked = appInfo.authorizationPurpose?.isAtMyRequest ?? false;
            checkMedical_Purpose_Other.Checked = appInfo.authorizationPurpose?.isOther ?? false;
            txtMedical_Purpose_OtherText.Text = appInfo.authorizationPurpose?.otherText;

            //EndOfAuth
            dateMedical_AuthEnd_date.Value = appInfo.endOfAuth?.endOn ?? DateTime.Now;
            checkMedical_AuthEnd_OnDate.Checked = appInfo.endOfAuth?.isOnDate ?? false;
            checkMedical_AuthEnd_Event.Checked = appInfo.endOfAuth?.isOnEvent ?? false;
            txtMedical_AuthEnd_EventText.Text = appInfo.endOfAuth?.theEvent;

            //PatientRights
            dateMedical_Rights_AuthorizedRep_SignDate.Value = appInfo.patientRights?.authRepSignDate ?? DateTime.Now;
            checkMedical_Rights_SubMinor.Checked = appInfo.patientRights?.isPatientMinor ?? false;
            checkMedical_Rights_UnableToSign.Checked = appInfo.patientRights?.isPatientMinorOrUnableToSign ?? false;
            checkMedical_Rights_CountOrder.Checked = appInfo.patientRights?.isRepCourtOrder ?? false;
            checkMedical_Rights_LegalGuardian.Checked = appInfo.patientRights?.isRepGuardian ?? false;
            checkMedical_Rights_Other.Checked = appInfo.patientRights?.isRepOther ?? false;
            checkMedical_Rights_Parent.Checked = appInfo.patientRights?.isRepParent ?? false;
            checkMedical_Rights_CantSign.Checked = appInfo.patientRights?.isUnableToSign ?? false;
            txtMedical_Rights_MinorAge.Text = appInfo.patientRights?.patientMinorAge;
            dateMedical_Rights_date.Value = appInfo.patientRights?.patientSignatureDate ?? DateTime.Now;
            txtMedical_Rights_OtherText.Text = appInfo.patientRights?.repAuthOtherText;
            txtMedical_Rights_UnableToSignText.Text = appInfo.patientRights?.unableToSignBecause;

            //AdditionalConsentForConditions
            checkMedical_AddCon_IDo.Checked = appInfo.additionalConsent?.allowRelease ?? false;
            checkMedical_AddCon_IDoNot.Checked = appInfo.additionalConsent?.notAllowRelease ?? false;
            dateMedical_AddCon_Time.Value = appInfo.additionalConsent?.date ?? DateTime.Now;
            dateMedical_AddCon_Date.Value = appInfo.additionalConsent?.date ?? DateTime.Now;

            //AdditionalCOnsentForHIV
            checkMedical_HIV_IDo.Checked = appInfo.additionalConsentForHIV?.allowRelease ?? false;
            dateMedical_HIV_date.Value = appInfo.additionalConsentForHIV?.date ?? DateTime.Now;
            dateMedical_HIV_date.Value = appInfo.additionalConsentForHIV?.date ?? DateTime.Now;

            //Accounts
            financial_gridResourceAccounts.Rows.Clear();
            foreach (var row in appInfo.finnancialInfo?.countableResources ?? new List<CountableResource>())
            {
                financial_gridResourceAccounts.Rows.Add(row.type, row.institution, row.ballance.ToString());
            }

            //Income
            financial_gridIncome.Rows.Clear();
            foreach (var row in appInfo.finnancialInfo?.monthlyIncomes ?? new List<MonthlyIncome>())
            {
                financial_gridIncome.Rows.Add(row.source, row.ammount);
            }

            //Retirement
            financial_gridRetirement.Rows.Clear();
            foreach (var row in appInfo.finnancialInfo?.retirements ?? new List<Retirement>())
            {
                financial_gridRetirement.Rows.Add(row.retirement, row.institution, row.ballance.ToString());
            }

            //vehicles
            financial_vehicles.Rows.Clear();
            foreach (var row in appInfo.finnancialInfo?.vehicles ?? new List<Vehicle>())
            {
                financial_vehicles.Rows.Add(row.make, row.model, row.year, row.milage);
            }

            //medical
            gridMedical_Parties.Rows.Clear();
            foreach (Guid doctorId in appInfo.authorizedPartiesIds ?? new List<Guid>())
            {
                AddDoctorInfoToDoctorsGrid(doctorId);
            }
        }

        private void HandleAutoEmailEvents()
        {
            autoEmail_RFI_group.Enabled = autoEmail_RFI_Enabled.Checked;
            autoEmail_Interim_Group.Enabled = autoEmail_Interim_Enabled.Checked;

            autoEmail_RFI_TopTextContent.Enabled =
                autoEmail_RFI_Enabled.Checked &&
                autoEmail_RFI_AddTopText.Checked;

            autoEmail_Interim_TopTextText.Enabled =
                autoEmail_Interim_Enabled.Checked &&
                autoEmail_Interim_AddTopText.Checked;

            autoEmail_LiteRFI_Groupbox.Enabled = autoEmail_LiteRFI_IsEnabled.Checked;
        }

        private void RefreshRepresentatives()
        {
            progress.SetProgressText("Loading representatives info...");
            comboFinancialRepresentative.Items.Clear();
            comboMedicalRepresentative.Items.Clear();

            representatives = Db.GetAllRepresentatives(logger);
            representatives.ForEach(f =>
            {
                comboFinancialRepresentative.Items.Add(new ComboboxItem
                {
                    Text = f.RepresentativeName,
                    Value = f.Id
                });

                comboMedicalRepresentative.Items.Add(new ComboboxItem
                {
                    Text = f.RepresentativeName,
                    Value = f.Id
                });

            });

        }

        private void RefreshCategories(bool refreshApplicantDocuments, string selectedNode)
        {
            progress.SetProgressText("Loading categories...");
            documentCategories = Db.GetAllDocumentCategories(logger);
            if (refreshApplicantDocuments)
                RefreshApplicantDocuments(selectedNode);
        }

        private void RefreshDoctors()
        {
            progress.SetProgressText("Loading physycians info...");

            comboMedical_Parties.Items.Clear();
            comboMed.Items.Clear();
            comboFinSup.Items.Clear();
            comboFin.Items.Clear();

            doctors = Db.GetAllDoctors(logger);

            doctors.Where(w => w.Type == Shared.Models.Enums.WorkerTypes.doctor.ToString())
                .ToList()
                .ForEach(f => comboMedical_Parties.Items.Add(new ComboboxItem { Text = f.Name, Value = f.Id }));

            doctors.Where(w => w.Type == Shared.Models.Enums.WorkerTypes.medicalWorker.ToString())
                .ToList()
                .ForEach(f => comboMed.Items.Add(new ComboboxItem { Text = f.Name, Value = f.Id }));

            doctors.Where(w => w.Type == Shared.Models.Enums.WorkerTypes.financialWorker.ToString())
                .ToList()
                .ForEach(f => comboFin.Items.Add(new ComboboxItem { Text = f.Name, Value = f.Id }));

            doctors.Where(w => w.Type == Shared.Models.Enums.WorkerTypes.financialSupervisor.ToString())
                .ToList()
                .ForEach(f => comboFinSup.Items.Add(new ComboboxItem { Text = f.Name, Value = f.Id }));

            //Case Worker
            Mop.SetComboItemByValue(comboFin, applicantModel?.financialCaseWorkerId);
            Mop.SetComboItemByValue(comboFinSup, applicantModel?.financialCaseWorkerSupervisorId);
            Mop.SetComboItemByValue(comboMed, applicantModel?.medicalCaseWorkerId);
        }

        private TreeNode nodeRFI;
        private TreeNode nodeMiscDocuments;
        private TreeNode nodeUncategorizedDocuments;
        private TreeNode nodeRejectedDocuments;
        private TreeNode nodeTasksClientFacing;
        private TreeNode nodeTasksNonClientFacing;


        private void RefreshApplicantDocuments(string selectThisNode)
        {
            progress.SetProgressText("Refreshing the documents...");

            applicantDocuments = Db.GetApplicantDocuments(ApplicantData.Id, logger, Models.Documents.DocumentStatus.active);

            nodeRFI.Nodes.Clear();
            nodeMiscDocuments.Nodes.Clear();
            nodeUncategorizedDocuments.Nodes.Clear();
            nodeRejectedDocuments.Nodes.Clear();
            nodeTasksClientFacing.Nodes.Clear();
            nodeTasksNonClientFacing.Nodes.Clear();

            nodeRFI.Tag = new TreeNodeTagInfo(NodeTypes.rootRFI);
            nodeMiscDocuments.Tag = new TreeNodeTagInfo(NodeTypes.rootMiscDocuments);
            nodeUncategorizedDocuments.Tag = new TreeNodeTagInfo(NodeTypes.rootUncategorizedDocuments);
            nodeRejectedDocuments.Tag = new TreeNodeTagInfo(NodeTypes.rootRejectedOcuments);
            nodeTasksClientFacing.Tag = new TreeNodeTagInfo(NodeTypes.rootTasksClientFacing);
            nodeTasksNonClientFacing.Tag = new TreeNodeTagInfo(NodeTypes.rootTasksNonClientFacing);

            foreach (var dc in documentCategories)
            {
                var node = nodeRFI.Nodes.Add(dc.Id.ToString(), dc.Category);
                node.ContextMenuStrip = contextRFI_CategoryClick;
                node.Tag = new TreeNodeTagInfo(NodeTypes.categoryRFI, dc.Id);
            }

            //add all root documents (the placeholders)
            foreach (var document in applicantDocuments.Where(w => w.DocumentParentId == null))
            {
                switch ((Models.Documents.DocumentTreeType)document.DocumentTypeId)
                {
                    case Models.Documents.DocumentTreeType.uncategorizedDocument:
                        var uncategorisedDocument = nodeUncategorizedDocuments.Nodes.Add(document.Id.ToString(), document.DocumentName);
                        uncategorisedDocument.ContextMenuStrip = contextOtherDocument_Click;
                        uncategorisedDocument.Tag = new TreeNodeTagInfo(NodeTypes.documentUncategorized, document.Id);
                        break;

                    case Models.Documents.DocumentTreeType.clientRFI:
                        int rfiCategoryId = document.DocumentCategoryId ?? throw new Exception("It's very odd a RFI document not to have a category! Please report this! Thank you");
                        Models.Documents.DocumentExtraStatus documentExtraStatus = (Models.Documents.DocumentExtraStatus)(document.DocumentExtraStatus ?? throw new Exception("It's very odd a RFI document not to have extra status! Please report this! Thank you"));

                        var selectedNodes = nodeRFI.Nodes.Descendants().ToList()
                            .Where(n => ((TreeNodeTagInfo)n.Tag).nodeType == NodeTypes.categoryRFI)
                            .Where(n => ((TreeNodeTagInfo)n.Tag).Id_int == rfiCategoryId);

                        if (selectedNodes.Count() > 1) throw new Exception("Multiple categories with the same tag?");
                        if (selectedNodes.Count() == 0)
                        {
                            throw new Exception($"A category with id {rfiCategoryId} can't be found");
                        }

                        //should be only one
                        TreeNode node = selectedNodes.Single();
                        TreeNode documentNode = node.Nodes.Add(document.Id.ToString(), document.DocumentName);
                        documentNode.ContextMenuStrip = contextRFI_DocumentClick;
                        documentNode.Tag = new TreeNodeTagInfo(NodeTypes.documentRFI, document.Id);
                        node.ExpandAll();

                        switch (documentExtraStatus)
                        {
                            case Models.Documents.DocumentExtraStatus.justPlaceHolder:
                                documentNode.ForeColor = Color.Red;
                                break;

                            case Models.Documents.DocumentExtraStatus.fileIsUploadedAndAccepted:
                                documentNode.ForeColor = Color.Green;
                                break;

                            case Models.Documents.DocumentExtraStatus.fileIsUploadedAndReviewIsPending:
                                documentNode.ForeColor = Color.Navy;
                                break;
                        }

                        break;

                    case Models.Documents.DocumentTreeType.miscDocument:
                        TreeNode miscDocument = nodeMiscDocuments.Nodes.Add(document.Id.ToString(), document.DocumentName);
                        miscDocument.ContextMenuStrip = contextOtherDocument_Click;
                        miscDocument.Tag = new TreeNodeTagInfo(NodeTypes.documentMisc, document.Id);
                        break;

                    case Models.Documents.DocumentTreeType.rejectedDocument:
                        TreeNode rejectedDoc = nodeRejectedDocuments.Nodes.Add(document.Id.ToString(), document.DocumentName);
                        rejectedDoc.ForeColor = Color.Red;
                        rejectedDoc.ContextMenuStrip = contextOtherDocument_Click;
                        rejectedDoc.Tag = new TreeNodeTagInfo(NodeTypes.documentRejected, document.Id);

                        break;
                }
            }

            //add the real documents to their placeholders (parents), which are already in the tree
            foreach (var document in applicantDocuments.Where(w => w.DocumentParentId != null))
            {
                var parentNode = treeDocsAndTasks.Nodes.Descendants().Single(s => ((TreeNodeTagInfo)s.Tag).id_guid.Equals(document.DocumentParentId));
                TreeNode node = parentNode.Nodes.Add(document.Id.ToString(), document.DocumentName);
                node.Tag = new TreeNodeTagInfo(NodeTypes.documentLeaf, document.Id);
                node.ContextMenuStrip = contextRFI_FileClick;
            }

            nodeRFI.Expand();
            nodeMiscDocuments.Expand();
            nodeUncategorizedDocuments.Expand();
            nodeRejectedDocuments.Expand();

            Application.DoEvents();
            if (!string.IsNullOrWhiteSpace(selectThisNode))
            {
                var node = treeDocsAndTasks.Nodes.Descendants().SingleOrDefault(s => s.Name == selectThisNode.ToString());
                treeDocsAndTasks.SelectedNode = node;
            }

            documentsLoaded = true;

            treeDocsAndTasks.Focus();
        }

        private void FormApplicantInfo_Load(object sender, EventArgs e)
        {
            progress = new ProgressExpress(progressBar, labelProgress, this, logger);

            nodeRFI = treeDocsAndTasks.Nodes["NodeDocuments"];
            nodeMiscDocuments = treeDocsAndTasks.Nodes["NodeMiscDocuments"];
            nodeUncategorizedDocuments = treeDocsAndTasks.Nodes["NodeUncategorizedDocuments"];
            nodeRejectedDocuments = treeDocsAndTasks.Nodes["NodeRejectedDocuments"];
            nodeTasksClientFacing = treeDocsAndTasks.Nodes["NodeTasksClientFacing"];
            nodeTasksNonClientFacing = treeDocsAndTasks.Nodes["NodeTasksNonClientFacing"];

            comboEmailTypes.SelectedIndex = 0;
            comboFaxTypes.SelectedIndex = 0;
        }

        private void HandleGravityFIelds()
        {
            Applicant_MaritalStatus_SelectedIndexChanged(null, null);
            Poc_IsPOC_CheckedChanged(null, null);
            Applicant_IsMilitary_CheckedChanged(null, null);
            Ltc_IsLtcAvaliable_CheckedChanged(null, null);

            HelthAndMedical_IsuesWithMobility_CheckedChanged_1(null, null);
            HelthAndMedical_IsuesWithTransferring_CheckedChanged(null, null);
            HelthAndMedical_IsuesWithBathing_CheckedChanged(null, null);
            HelthAndMedical_IsuesWithOther_CheckedChanged(null, null);
            HelthAndMedical_IsuesWithDDD_CheckedChanged(null, null);
            HelthAndMedical_IsuesWithOxygen_CheckedChanged(null, null);
            HelthAndMedical_IsuesWithAlzheimer_CheckedChanged(null, null);
            HelthAndMedical_IsuesWithParalysis_CheckedChanged(null, null);
            HelthAndMedical_IsuesWithAggression_CheckedChanged(null, null);
            HelthAndMedical_IsuesWithOrientation_CheckedChanged(null, null);
            HelthAndMedical_IsuesWithVision_CheckedChanged(null, null);
            HelthAndMedical_IsuesWithBladder_CheckedChanged(null, null);
            HelthAndMedical_IsuesWithToileting_CheckedChanged(null, null);
            HelthAndMedical_IsuesWithEating_CheckedChanged(null, null);
            HelthAndMedical_IsuesWithDressing_CheckedChanged(null, null);
            Financial_IsOwningPolicy_CheckedChanged(null, null);
            Financial_isBural_CheckedChanged(null, null);
            Financial_IsOwningHome_CheckedChanged(null, null);

            //DE events
            CustomerCurrentLiving_Other_CheckedChanged(null, null);
            AccomodationsForPrintedLetters_Yes_CheckedChanged(null, null);
            AccomodationsForPrintedLetters_Other_CheckedChanged(null, null);
            AdditionalQuestions_1_Yes_CheckedChanged(null, null);
            InterviewInterpretter_Yes_CheckedChanged(null, null);

            Interview_Monday_CheckedChanged(null, null);
            Interview_Wednesday_CheckedChanged(null, null);
            Interview_Friday_CheckedChanged(null, null);
            Interview_Tuesday_CheckedChanged(null, null);
            Interview_Thursday_CheckedChanged(null, null);
        }

        private void HandleCountries()
        {
            progress.SetProgressText("Loading countries...");
            countries = Db.GetCountries(logger);

            poc_AddressCountry.Properties.Items.Clear();
            poc_AddressCountry.Properties.Items.AddRange(countries.Select(s => s.CountryName).ToList());

            ltc_AddressCountry.Properties.Items.Clear();
            ltc_AddressCountry.Properties.Items.AddRange(countries.Select(s => s.CountryName).ToList());

            applicant_AddressCountry.Properties.Items.Clear();
            applicant_AddressCountry.Properties.Items.AddRange(countries.Select(s => s.CountryName).ToList());

            applicant_MailingAddressCountry.Properties.Items.Clear();
            applicant_MailingAddressCountry.Properties.Items.AddRange(countries.Select(s => s.CountryName).ToList());
        }

        private void AddDoctorInfoToDoctorsGrid(Guid doctorId)
        {
            ApplicantsDoctors doctor = doctors.Single(s => s.Id.Equals(doctorId));
            gridMedical_Parties.Rows.Add(doctor.Id, doctor.Name, doctor.OfficeFax, doctor.OfficeTelephoneNumber, doctor.Address);
        }

        private void RespectMode()
        {
            switch (FormMode)
            {
                case Models.FormModes.New:
                    Text = $"Applicant      [ {FormMode.ToString().ToUpperInvariant()} ]";
                    break;

                case Models.FormModes.Edit:
                    Text = $"Applicant  '{ApplicantData.Name}'    [ {FormMode.ToString().ToUpperInvariant()} ]";
                    break;

                default: throw new Exception($"Unsupported form mode {FormMode}");
            }
        }

        private void EnableMedicalFields()
        {
            CheckMedical_Disclosed_Period_CheckedChanged(null, null);
            CheckMedical_Disclosed_Other_CheckedChanged(null, null);
            CheckMedical_Purpose_Other_CheckedChanged(null, null);
            CheckMedical_AuthEnd_OnDate_CheckedChanged(null, null);
            CheckMedical_AuthEnd_Event_CheckedChanged(null, null);
            CheckMedical_Rights_SubMinor_CheckedChanged(null, null);
            CheckMedical_Rights_UnableToSign_CheckedChanged(null, null);
            CheckMedical_Rights_Other_CheckedChanged(null, null);
            CheckMedical_Disclosed_Specific_CheckedChanged(null, null);
        }

        private void CheckMedical_Disclosed_Specific_CheckedChanged(object sender, EventArgs e)
        {
            txtMedical_Disclosed_SpecificText.Enabled = checkMedical_Disclosed_Specific.Checked;
        }

        private void CheckMedical_Disclosed_Period_CheckedChanged(object sender, EventArgs e)
        {
            dateMedical_Disclosed_PeriodFrom.Enabled = checkMedical_Disclosed_Period.Checked;
            dateMedical_Disclosed_PeriodTo.Enabled = checkMedical_Disclosed_Period.Checked;
        }

        private void CheckMedical_Disclosed_Other_CheckedChanged(object sender, EventArgs e)
        {
            txtMedical_Disclosed_OtherText.Enabled = checkMedical_Disclosed_Other.Checked;
        }

        private void CheckMedical_Purpose_Other_CheckedChanged(object sender, EventArgs e)
        {
            txtMedical_Purpose_OtherText.Enabled = checkMedical_Purpose_Other.Checked;
        }

        private void CheckMedical_AuthEnd_OnDate_CheckedChanged(object sender, EventArgs e)
        {
            dateMedical_AuthEnd_date.Enabled = checkMedical_AuthEnd_OnDate.Checked;
        }

        private void CheckMedical_AuthEnd_Event_CheckedChanged(object sender, EventArgs e)
        {
            txtMedical_AuthEnd_EventText.Enabled = checkMedical_AuthEnd_Event.Checked;
        }

        private void CheckMedical_Rights_CantSign_CheckedChanged(object sender, EventArgs e)
        {
            //TODO:
        }

        private void CheckMedical_Rights_SubMinor_CheckedChanged(object sender, EventArgs e)
        {
            txtMedical_Rights_MinorAge.Enabled = checkMedical_Rights_SubMinor.Checked && checkMedical_Rights_CantSign.Checked;
        }

        private void CheckMedical_Rights_UnableToSign_CheckedChanged(object sender, EventArgs e)
        {
            txtMedical_Rights_UnableToSignText.Enabled = checkMedical_Rights_UnableToSign.Checked && checkMedical_Rights_CantSign.Checked;
        }

        private void CheckMedical_Rights_Other_CheckedChanged(object sender, EventArgs e)
        {
            txtMedical_Rights_OtherText.Enabled = checkMedical_Rights_Other.Checked;
        }

        #region Validators
        private bool IsDEFormValid()
        {
            if (
                !Mop.ValidateControl(
                        CustomerCurrentLiving_Other.Checked && string.IsNullOrWhiteSpace(CustomerCurrentLivingOther_Text.Text),
                        "Customer currently residing type text when 'Other' is checked",
                        CustomerCurrentLivingOther_Text)

                || !Mop.ValidateControl(
                        AccomodationsForPrintedLetters_Yes.Checked && string.IsNullOrWhiteSpace(AccomodationsForPrintedLetters_WhoIfYes.Text),
                        "'Who needs the accommodation' when 'Need alternative format for printed letters is answered YES",
                        AccomodationsForPrintedLetters_WhoIfYes)

                || !Mop.ValidateControl(
                        AccomodationsForPrintedLetters_Other.Enabled && AccomodationsForPrintedLetters_Other.Checked && string.IsNullOrWhiteSpace(AccomodationsForPrintedLetters_OtherText.Text),
                        "the selected field",
                        AccomodationsForPrintedLetters_OtherText)

                || !Mop.ValidateControl(
                        AdditionalQuestions_1_Yes.Enabled && AdditionalQuestions_1_Yes.Checked && string.IsNullOrWhiteSpace(AdditionalQuestions_1_Yes_Month1.Text),
                        "Paying medical bills for at least one month when answering YES",
                        AdditionalQuestions_1_Yes_Month1)

                || !Mop.ValidateControl(
                        Interview_Monday.Checked && string.IsNullOrWhiteSpace(Interview_Monday_Time.Text),
                        "'Interview time' for Monday when selecting it",
                        Interview_Monday_Time)

                || !Mop.ValidateControl(
                        Interview_Tuesday.Checked && string.IsNullOrWhiteSpace(Interview_Tuesday_Time.Text),
                        "'Interview time' for Tuesday when selecting it",
                        Interview_Tuesday_Time)

                || !Mop.ValidateControl(
                        Interview_Wednesday.Checked && string.IsNullOrWhiteSpace(Interview_Wednesday_Time.Text),
                        "'Interview time' for Wednesday when selecting it",
                        Interview_Wednesday_Time)

                || !Mop.ValidateControl(
                        Interview_Thursday.Checked && string.IsNullOrWhiteSpace(Interview_Thursday.Text),
                        "Interview time' for Thursday when selecting it",
                        Interview_Thursday)

                || !Mop.ValidateControl(
                        Interview_Friday.Checked && string.IsNullOrWhiteSpace(Interview_Friday_Time.Text),
                        "'Interview time' for Friday when selecting it",
                        Interview_Friday_Time)

                || !Mop.ValidateControl(
                        InterviewInterpretter_Yes.Checked && string.IsNullOrWhiteSpace(InterviewInterpretter_Language.Text),
                        "'Interpreter language' when asking for one",
                        InterviewInterpretter_Language)

                )
            {
                return false;
            }

            return Mop.ValidateChildControls(panelControl2);
        }

        private bool IsMedicalFormValid()
        {
            if (FormMode == FormModes.New)
            {
                MessageBox.Show("You need to Save the applicant first, this is the first tab.");
                return false;
            }

            if (checkMedical_Disclosed_Period.Checked && dateMedical_Disclosed_PeriodFrom.Value > dateMedical_Disclosed_PeriodTo.Value)
            {
                MessageBox.Show("Invalid period for 'health information covering the period'");
                return false;
            }

            //Patient is a minor or unable to sign
            if (checkMedical_Rights_CantSign.Checked)
            {
                if (!checkMedical_Rights_SubMinor.Checked && !checkMedical_Rights_UnableToSign.Checked)
                {
                    MessageBox.Show("Filling why patient is unable to sign is mandatory");
                    if (checkMedical_Rights_SubMinor.CanFocus) checkMedical_Rights_SubMinor.Focus();

                    if (!Mop.ValidateControl(
                        checkMedical_Rights_UnableToSign.Checked && string.IsNullOrWhiteSpace(txtMedical_Rights_UnableToSignText.Text),
                        "Unable to sign reason when the option is ticked",
                        txtMedical_Disclosed_SpecificText)) return false;
                }
            }

            if (

                //Disclosed information
                //
                !Mop.ValidateControl(
                        !checkMedical_Disclosed_All.Checked
                        && !checkMedical_Disclosed_Specific.Checked
                        && !checkMedical_Disclosed_Period.Checked
                        && !checkMedical_Disclosed_Other.Checked,
                        "one of the 'Disclosed information' ticks",
                        checkMedical_Disclosed_All)

                || !Mop.ValidateControl(
                        checkMedical_Disclosed_Specific.Checked && string.IsNullOrWhiteSpace(txtMedical_Disclosed_SpecificText.Text),
                        "Treatment or condition when the option is ticked",
                        txtMedical_Disclosed_SpecificText)

                || !Mop.ValidateControl(
                        checkMedical_Disclosed_Other.Checked && string.IsNullOrWhiteSpace(txtMedical_Disclosed_OtherText.Text),
                        "Other text when the option is ticked",
                        txtMedical_Disclosed_OtherText)

                || !Mop.ValidateControl(
                        string.IsNullOrWhiteSpace(txtMedical_Disclosed_OtherEmail.Text),
                        "Discloser email",
                        txtMedical_Disclosed_OtherEmail)

                || !Mop.ValidateControl(
                        string.IsNullOrWhiteSpace(txtMedical_Disclosed_OtherEmail.Text),
                        "Discloser email",
                        txtMedical_Disclosed_OtherEmail)

                //Purpose of the authorization
                //
                || !Mop.ValidateControl(
                        !checkMedical_Purpose_MyRequest.Checked
                        && !checkMedical_Purpose_Other.Checked
                        && !checkMedical_Purpose_Comunicate.Checked
                        && !checkMedical_Purpose_Sell.Checked,
                        "one of the 'Purpose of the authorization' ticks",
                        checkMedical_Purpose_MyRequest)

                || !Mop.ValidateControl(
                        checkMedical_Purpose_Other.Checked && string.IsNullOrWhiteSpace(txtMedical_Purpose_OtherText.Text),
                        "Other text when the option is ticked",
                        txtMedical_Purpose_OtherText)

                //End of this authorization:
                //
                || !Mop.ValidateControl(
                        !checkMedical_AuthEnd_OnDate.Checked
                        && !checkMedical_AuthEnd_Event.Checked,
                        "one of the 'End of this authorization' ticks",
                        checkMedical_AuthEnd_OnDate)

                || !Mop.ValidateControl(
                        checkMedical_AuthEnd_Event.Checked && string.IsNullOrWhiteSpace(txtMedical_AuthEnd_EventText.Text),
                        "End of this authorization Event Text when the option is ticked",
                        txtMedical_AuthEnd_EventText)

                || !Mop.ValidateControl(
                        !checkMedical_Rights_Parent.Checked
                        && !checkMedical_Rights_LegalGuardian.Checked
                        && !checkMedical_Rights_CountOrder.Checked
                        && !checkMedical_Rights_Other.Checked,
                        "one of the 'Authority of representative to sign on behalf of the patient' ticks",
                        checkMedical_Rights_Parent)

                || !Mop.ValidateControl(
                        checkMedical_Rights_Other.Checked && string.IsNullOrWhiteSpace(txtMedical_Rights_OtherText.Text),
                        "Authority of representative to sign on behalf of the patient Other Text ",
                        checkMedical_Rights_Other)

                //Additional Consent for Certain Conditions
                //
                || !Mop.ValidateControl(
                        !checkMedical_AddCon_IDo.Checked
                        && !checkMedical_AddCon_IDoNot.Checked,
                        "one of the 'Additional Consent for Certain Conditions' ticks",
                        checkMedical_Purpose_MyRequest)

                //Additional Consent for HIV/AIDS
                //
                || !Mop.ValidateControl(
                        !checkMedical_HIV_IDo.Checked
                        && !checkMedical_HIV_IDoNot.Checked,
                        "one of the 'Additional Consent for HIV/AIDS' ticks",
                        checkMedical_Purpose_MyRequest)
                )
            {
                return false;
            }

            //return Mop.ValidateChildControls(panelDE);
            return true;
        }

        #endregion

        private Guid? GetSelectedRepresentativeId()
        {
            if (comboFinancialRepresentative.SelectedIndex == -1)
                return null;

            return (Guid)((ComboboxItem)comboFinancialRepresentative.SelectedItem).Value;
        }

        private ApplicantsRepresentatives GetSelectedRepresentativeOrDie()
        {
            Guid? representativeId = GetSelectedRepresentativeId() ?? throw new Exception("Please select a representative for this applicant!");

            return representatives.Single(s => s.Id.Equals(representativeId));
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (
                string.IsNullOrWhiteSpace(applicant_FirstName.Text.Trim())
                || string.IsNullOrWhiteSpace(applicant_LastName.Text.Trim())
                || comboFinancialRepresentative.SelectedIndex < 0
                )
            {
                MessageBox.Show("To save an applicant you must enter at least it's first and last names and select a representative");
                return;
            }

            progress.ShowProgressBar("Saving applicant data");
            try
            {
                var applicantModelLocal = FillApplicantModelFromForm();

                //detect auto email changes
                //applicantModel contains the state before applying the new changes
                DetectAndHandleAutoEmailCHanges(applicantModelLocal);
                DetectAndHandleMedicalCaseWorkerChanges(applicantModelLocal, ApplicantData.Id);

                applicantModel = FillApplicantModelFromForm();
                string serializedData = JsonConvert.SerializeObject(applicantModel);

                ApplicantData.Name = applicantModel.personalInfo.detailedPersonalInfo.GetFullName();
                ApplicantData.Data = serializedData;
                ApplicantData.RepresentativeId = GetSelectedRepresentativeId();
                ApplicantData.MedicalRepresentativeId = Mop.GetComboValue(comboMedicalRepresentative);

                ApplicantsDoctors finWorker = doctors.SingleOrDefault(s => s.Id.Equals(Mop.GetComboValue(comboFin)));
                ApplicantsDoctors medWorker = doctors.SingleOrDefault(s => s.Id.Equals(Mop.GetComboValue(comboMed)));

                ApplicantData.FinancialWorkerEmail = finWorker?.Email;
                ApplicantData.FinancialWorkerName = finWorker?.Name;
                ApplicantData.FinancialWorkerPhoneNumber = finWorker?.OfficeTelephoneNumber;

                ApplicantData.MedicalWorkerEmail = medWorker?.Email;
                ApplicantData.MedicalWorkerName = medWorker?.Name;
                ApplicantData.MedicalWorkerPhoneNumber = medWorker?.OfficeTelephoneNumber;

                switch (FormMode)
                {
                    case FormModes.New:

                        //the id is already generated in FormLoad, we need it when uploading to azure
                        ApplicantData.CreatedByUserId = Db.GetUserId();
                        ApplicantData.CreatedOn = DateTime.Now;
                        ApplicantData.Status = 3; //TODO: somehow load from database or constant

                        Db.InsertApplicant(ApplicantData, logger);
                        break;

                    case FormModes.Edit:
                        Db.UpdateApplicant(ApplicantData, logger);
                        break;

                    default:
                        throw new Exception($"Unexpected form mode: {FormMode}");
                }

                progress.SetProgressText("Add audit data");
                Db.InsertAction(
                    logger,
                    ApplicantData.Id,
                    FormMode == FormModes.Edit ? ReportActions.ApplicantUpdate : ReportActions.ApplicantCreate,
                    correlationId: ApplicantData.Id,
                    data: JsonConvert.SerializeObject(ApplicantData));
            }
            finally
            {
                progress.HideProgressBar();
            }

            MessageBox.Show("The applicant data and files were stored successfully!");
            if (FormMode == FormModes.New)
            {
                FormMode = FormModes.Edit;
                ApplicantData = Db.LoadApplicant(ApplicantData.Id, logger);

                RespectMode();
            }

            RefreshCustomCategoryDescriptions();

            NeedRefresh = true;
        }

        private void DetectAndHandleMedicalCaseWorkerChanges(FullApplicantInfo applicantModelLocal, Guid applicantId)
        {
            if (applicantModel?.medicalCaseWorkerId != Mop.GetComboValue(comboMed))
            {
                Db.DeleteAllScheduledEmails(applicantId, logger, Models.Communication.EmailTypes.PasAssssorAutoRequest);
            }

        }

        private void DetectAndHandleAutoEmailCHanges(FullApplicantInfo applicantModelLocal)
        {
            bool RFIDiff =
                   ((applicantModel?.autoRFIEmailSettings?.addCustomMessage ?? false) != autoEmail_RFI_AddTopText.Checked)
                || (applicantModel?.autoRFIEmailSettings?.customTopTextMessage != autoEmail_RFI_TopTextContent.Text)
                || ((applicantModel?.autoRFIEmailSettings?.enabled ?? false) != autoEmail_RFI_Enabled.Checked)
                || ((applicantModel?.autoRFIEmailSettings?.onlyMondayToFriday ?? false) != autoEmail_RFI_SendOnlyMonToFri.Checked)
                || ((applicantModel?.autoRFIEmailSettings?.sendIntervalInDays ?? -1) != autoEmail_RFI_SendIntervalInDays.Value)
                || ((applicantModel?.pointOfContact?.email) != poc_Email.Text)
                ;

            bool interimDiff =
                   ((applicantModel?.autoInterimEmailSettings?.addCustomMessage ?? false) != autoEmail_Interim_AddTopText.Checked)
                || (applicantModel?.autoInterimEmailSettings?.customTopTextMessage != autoEmail_Interim_TopTextText.Text)
                || ((applicantModel?.autoInterimEmailSettings?.enabled ?? false) != autoEmail_Interim_Enabled.Checked)
                || ((applicantModel?.autoInterimEmailSettings?.onlyMondayToFriday ?? false) != autoEmail_Interim_MonFriOnly.Checked)
                || ((applicantModel?.autoInterimEmailSettings?.sendIntervalInDays ?? -1) != autoEmail_Interim_SendIntervalInDays.Value)
                || ((applicantModel?.pointOfContact?.email) != poc_Email.Text)
                ;

            bool smallRFIDiff =
                (applicantModel?.autoLiteRFIEmailSettings?.enabled ?? false) != autoEmail_LiteRFI_IsEnabled.Checked
                || applicantModel?.pointOfContact?.email != poc_Email.Text;

            if (smallRFIDiff)
            {                
                if (!autoEmail_LiteRFI_IsEnabled.Checked && applicantModelLocal.autoLiteRFIEmailSettings != null)
                {
                    applicantModelLocal.autoLiteRFIEmailSettings.pausedUntilMoreDocumentsAreRequired = false;
                }
            }

            EmailProcessor.ResheduleAllAutomaticEmails(RFIDiff, interimDiff, smallRFIDiff, ApplicantData.Id, logger, applicantModelLocal);
        }



        private void RefreshCustomCategoryDescriptions()
        {
            if (!string.IsNullOrWhiteSpace(ApplicantData.CustomCategoryDescriptions))
            {
                customCategoryDescriptions = JsonConvert.DeserializeObject<List<Models.Documents.CustomCategoryDescription>>(ApplicantData.CustomCategoryDescriptions);
            }
        }

        private void SimpleButton1_Click_1(object sender, EventArgs e)
        {
            if (NeedRefresh)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                DialogResult = DialogResult.Abort;
            }
        }

        private void Applicant_MaritalStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            applicant_SpouceDateOfBirth.Enabled = applicant_MaritalStatus.SelectedIndex != 3;
            applicant_DateOfMarrgiage.Enabled = applicant_MaritalStatus.SelectedIndex != 3;
            applicant_SpouseFirstName.Enabled = applicant_MaritalStatus.SelectedIndex != 3;
            applicant_SpouseMiddleName.Properties.Enabled = applicant_MaritalStatus.SelectedIndex != 3;
            applicant_SpouseLastName.Enabled = applicant_MaritalStatus.SelectedIndex != 3;
            applicant_SpouseSSN.Enabled = applicant_MaritalStatus.SelectedIndex != 3;
        }

        private void Applicant_IsMilitary_CheckedChanged(object sender, EventArgs e)
        {
            applicant_MilitaryBranch.Enabled = applicant_IsMilitary.Checked;
            applicant_MilitaryFromYear.Enabled = applicant_IsMilitary.Checked;
            applicant_MilitaryToYear.Enabled = applicant_IsMilitary.Checked;
        }

        private void Poc_IsPOC_CheckedChanged(object sender, EventArgs e)
        {
            groupPOC.Enabled = poc_IsPOC.Checked;
        }

        private void Ltc_IsLtcAvaliable_CheckedChanged(object sender, EventArgs e)
        {
            groupLTC.Enabled = ltc_IsLtcAvaliable.Checked;
        }

        private void HelthAndMedical_IsuesWithTransferring_CheckedChanged(object sender, EventArgs e)
        {
            helthAndMedical_NotesTransferring.Enabled = helthAndMedical_IsuesWithTransferring.Checked;
        }

        private void HelthAndMedical_IsuesWithBathing_CheckedChanged(object sender, EventArgs e)
        {
            helthAndMedical_NotesBathing.Enabled = helthAndMedical_IsuesWithBathing.Checked;
        }

        private void HelthAndMedical_IsuesWithDressing_CheckedChanged(object sender, EventArgs e)
        {
            helthAndMedical_NotesDressing.Enabled = helthAndMedical_IsuesWithDressing.Checked;
        }

        private void HelthAndMedical_IsuesWithEating_CheckedChanged(object sender, EventArgs e)
        {
            helthAndMedical_NotesEating.Enabled = helthAndMedical_IsuesWithEating.Checked;
        }

        private void HelthAndMedical_IsuesWithToileting_CheckedChanged(object sender, EventArgs e)
        {
            helthAndMedical_NotesToileting.Enabled = helthAndMedical_IsuesWithToileting.Checked;
        }

        private void HelthAndMedical_IsuesWithBladder_CheckedChanged(object sender, EventArgs e)
        {
            helthAndMedical_NotesIncontinence.Enabled =
                helthAndMedical_IsuesWithBladder.Checked ||
                helthAndMedical_IsuesWithBowel.Checked;
        }

        private void HelthAndMedical_IsuesWithVision_CheckedChanged(object sender, EventArgs e)
        {
            helthAndMedical_NotesVision.Enabled = helthAndMedical_IsuesWithVision.Checked;
            helthAndMedical_IsLegallyBlind.Enabled = helthAndMedical_IsuesWithVision.Checked;
        }

        private void HelthAndMedical_IsuesWithOrientation_CheckedChanged(object sender, EventArgs e)
        {
            helthAndMedical_NotesOrientation.Enabled = helthAndMedical_IsuesWithOrientation.Checked;
        }

        private void HelthAndMedical_IsuesWithAggression_CheckedChanged(object sender, EventArgs e)
        {
            helthAndMedical_NotesBehavior.Enabled =
                helthAndMedical_IsuesWithAggression.Checked
                || helthAndMedical_IsuesWithSelfInjury.Checked
                || helthAndMedical_IsuesWithWandering.Checked
                || helthAndMedical_IsuesWithDisruptive.Checked;
        }

        private void HelthAndMedical_IsuesWithParalysis_CheckedChanged(object sender, EventArgs e)
        {
            helthAndMedical_NotesParalisys.Enabled = helthAndMedical_IsuesWithParalysis.Checked;
        }

        private void HelthAndMedical_IsuesWithAlzheimer_CheckedChanged(object sender, EventArgs e)
        {
            groupAlzheimer.Enabled = helthAndMedical_IsuesWithAlzheimer.Checked;
        }

        private void HelthAndMedical_IsuesWithOxygen_CheckedChanged(object sender, EventArgs e)
        {
            helthAndMedical_NotesOxygen.Enabled = helthAndMedical_IsuesWithOxygen.Checked;
        }

        private void HelthAndMedical_IsuesWithDDD_CheckedChanged(object sender, EventArgs e)
        {
            helthAndMedical_NotesDDD.Enabled = helthAndMedical_IsuesWithDDD.Checked;
        }

        private void HelthAndMedical_IsuesWithOther_CheckedChanged(object sender, EventArgs e)
        {
            helthAndMedical_NotesOther.Enabled = helthAndMedical_IsuesWithOther.Checked;
        }

        private void Financial_IsOwningPolicy_CheckedChanged(object sender, EventArgs e)
        {
            financial_PolicyNotes.Enabled = financial_IsOwningPolicy.Checked;
        }

        private void Financial_isBural_CheckedChanged(object sender, EventArgs e)
        {
            financial_BurialAmount.Enabled = financial_isBural.Checked;
        }

        private void Financial_IsOwningHome_CheckedChanged(object sender, EventArgs e)
        {
            groupHomeOwned.Enabled = financial_IsOwningHome.Checked;
        }

        private void HelthAndMedical_IsuesWithMobility_CheckedChanged_1(object sender, EventArgs e)
        {
            helthAndMedical_NotesMobility.Enabled = helthAndMedical_IsuesWithMobility.Checked;
        }

        private void EnsureEditModeOrDie()
        {
            if (FormMode == FormModes.New)
            {
                if (btnSave.CanFocus)
                    btnSave.Focus();

                throw new Exception("You need to save the applicant to perform this action!");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="docType"></param>
        /// <param name="documentId"></param>
        /// <param name="documentExtraStatus"></param>
        /// <param name="RFI_existingDocument">If Passed this means uploading a document to placeholder</param>
        private void UploadDocumentToAzureAndDb(
            string fileName,
            Models.Documents.DocumentType docType,
            Guid? documentId,
            Models.Documents.DocumentExtraStatus? documentExtraStatus,
            ApplicantsDocuments RFI_existingDocument,
            bool uploadToParent
            )
        {
            int maxVersion = 0;
            if (docType == Models.Documents.DocumentType.unspecified)
                throw new Exception("Assert failed, trying to upload 'unspecified' document type!");

            string fileNameOnly = Path.GetFileName(fileName);
            progress.SetProgressText($"Saving {fileName} to db...");

            //for special docuemnt types set to old version
            if (docType != Models.Documents.DocumentType.other)
            {
                var specialDocuments = Db.GetApplicantDocuments(
                    ApplicantData.Id,
                    logger,
                    Models.Documents.DocumentStatus.active,
                    docType);

                if (specialDocuments.Count > 1)
                {
                    MessageBox.Show($"Somehow there are more than one active documents of type '{docType.ToString()}'");
                }

                foreach (var specialDocument in specialDocuments)
                {
                    if (specialDocument.Version > maxVersion)
                        maxVersion = specialDocument.Version;

                    Db.ChangeApplicantDocumentStatus(specialDocument.Id, Models.Documents.DocumentStatus.oldVersion, logger);
                }
            }

            ReportActions reportAction;
            ApplicantsDocuments document;
            if (RFI_existingDocument == null)
            {
                reportAction = ReportActions.DocumentCreate;
                //Totally new document
                document = Db.AddApplicantDocument(new ApplicantsDocuments
                {
                    ApplicantId = ApplicantData.Id,
                    DocumentName = fileNameOnly,
                    DocumentType = docType.ToString(),
                    Id = documentId.Value,
                    Version = maxVersion + 1,
                    DocumentCategoryId = null, //Or real category if we want to place it in RFI cartegory directory
                    DocumentExtraStatus = null, //Or real extra status, if the document required validation
                    DocumentTypeId = (int)Models.Documents.DocumentTreeType.miscDocument,
                }, logger);
            }
            else
            {
                //Uploading document to a placeHolder
                if (!uploadToParent)
                {
                    document = RFI_existingDocument;

                    if (document.DocumentExtraStatus != (int)documentExtraStatus.Value)
                    {
                        Db.ChangeApplicantDocumentExtraStatus(document.Id, documentExtraStatus.Value, logger);
                    }
                }
                else
                {
                    document = Db.AddApplicantDocument(new ApplicantsDocuments
                    {
                        ApplicantId = ApplicantData.Id,
                        DocumentName = fileNameOnly,
                        DocumentType = docType.ToString(),
                        Id = Guid.NewGuid(),
                        Version = maxVersion + 1,
                        DocumentCategoryId = null,
                        DocumentExtraStatus = null,
                        DocumentTypeId = (int)Models.Documents.DocumentTreeType.clientRFI,
                        DocumentParentId = RFI_existingDocument.Id,
                    }, logger);

                    if (RFI_existingDocument.DocumentExtraStatus != (int)Models.Documents.DocumentExtraStatus.fileIsUploadedAndReviewIsPending)
                    {
                        Db.ChangeApplicantDocumentExtraStatus(RFI_existingDocument.Id, Models.Documents.DocumentExtraStatus.fileIsUploadedAndReviewIsPending, logger);
                    }
                }

                reportAction = ReportActions.RFIUploadDocumentToPlaceHolder;
            }

            try
            {
                progress.SetProgressText($"Uploading {fileNameOnly} to AZURE CLOUD...");
                Shared.Source.Azure.StorageManager.UploadFile(
                    localFilePath: fileName,
                    applicantId: ApplicantData.Id,
                    remoteFileName: document.Id.ToString(),
                    logger);

                progress.SetProgressText($"Adding AddDocument action");
                Db.InsertAction(
                    logger,
                    ApplicantData.Id,
                    reportAction,
                    document.Id,
                    data: JsonConvert.SerializeObject(new
                    {
                        localFilePath = fileName,
                        applicantId = ApplicantData.Id,
                        documentInfo = document,
                    })
                );
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show($"Error while uploading the file to azure -  '{ex.Message}'");
                Db.ChangeApplicantDocumentStatus(document.Id, Models.Documents.DocumentStatus.deleted, logger);
            }
        }

        private void TimerLoad_Tick(object sender, EventArgs e)
        {
            timerLoad.Enabled = false;

            InitForm();
        }

        private void InitForm()
        {
            progress.ShowProgressBar("Loading applicant...");
            try
            {
                RefreshRepresentatives();
                RefreshDoctors();
                HandleCountries();
                RefreshCategories(false, null);

                if (FormMode == FormModes.New)
                {
                    ApplicantData = new ApplicantsNew();
                    ApplicantData.Id = Guid.NewGuid();
                }

                if (FormMode == FormModes.Edit)
                {
                    progress.SetProgressText("Loading applicant data...");
                    applicantModel = JsonConvert.DeserializeObject<FullApplicantInfo>(ApplicantData.Data);
                    FillFormFromModel(applicantModel);

                    RefreshApplicantDocuments(null);

                    Mop.SetComboItemByValue(comboFinancialRepresentative, ApplicantData.RepresentativeId);
                    Mop.SetComboItemByValue(comboMedicalRepresentative, ApplicantData.MedicalRepresentativeId);
                }

                progress.SetProgressText("Set form active components...");

                RespectMode();
                EnableMedicalFields();
                HandleGravityFIelds();
                RefreshCustomCategoryDescriptions();
            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void BtnGenerate_Click(object sender, EventArgs e)
        {
            if (comboDocumentGenerator.SelectedIndex < 0)
            {
                MessageBox.Show("Please select the target file type in the Generator File type DropDown");
                if (comboDocumentGenerator.CanFocus)
                    comboDocumentGenerator.Focus();

                return;
            }

            string targetFileType = comboDocumentGenerator.Text;

            //Get representative
            RepresentativeInfo representativeInfo = GetRepresentativeObjectOrDie();

            progress.ShowProgressBar("Start generating file...");
            try
            {
                //Get full app info
                FullApplicantInfo appInfo = FillApplicantModelFromForm();

                string fileToUpload;
                Models.Documents.DocumentType docType;

                Guid documentId = Guid.NewGuid();
                switch (targetFileType.ToLowerInvariant())
                {
                    case "de-101":
                        fileToUpload = Source.FileGenerators.DE101.Fill(appInfo, representativeInfo, documentId, logger);
                        docType = Models.Documents.DocumentType.de101;
                        break;

                    case "de-112":
                        fileToUpload = Source.FileGenerators.DE112.Fill(appInfo, representativeInfo, documentId, logger);
                        docType = Models.Documents.DocumentType.de112;
                        break;

                    case "medical":
                        var medical = new Source.FileGenerators.Medical();
                        fileToUpload = medical.Fill(appInfo, representativeInfo, doctors, documentId, logger);
                        docType = Models.Documents.DocumentType.medical;
                        break;

                    case "de-101 pas":
                        {
                            fileToUpload = Source.FileGenerators.DE101.Fill(appInfo, GetMedicalRepresentativeObjectOrDie(), documentId, logger);
                            string targetFileName = Shared.Source.Mop.GetTempFileName(null, $"de-101_PAS_{documentId.ToString()}.pdf");
                            fileToUpload = Shared.Source.PDFWorker.StampPdf(fileToUpload, targetFileName, "Private Request PAS");
                            docType = Models.Documents.DocumentType.de101_pas;
                        }
                        break;

                    case "de-101 csra":
                        {
                            fileToUpload = Source.FileGenerators.DE101.Fill(appInfo, representativeInfo, documentId, logger);
                            string targetFileName = Shared.Source.Mop.GetTempFileName(null, $"de-101_SCRA_{documentId.ToString()}.pdf");
                            fileToUpload = Shared.Source.PDFWorker.StampPdf(fileToUpload, targetFileName, "CSRA");
                            docType = Models.Documents.DocumentType.de101_csra;
                        }
                        break;

                    default: throw new Exception($"Unknown target file type to generate: {targetFileType}");
                }

                //Upload DE/Medical generated
                UploadDocumentToAzureAndDb(fileToUpload, docType, documentId, null, null, false);

                RefreshApplicantDocuments(null);
                PreviewFile(fileToUpload);
            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private RepresentativeInfo GetRepresentativeObjectOrDie()
        {
            Guid? repId = GetSelectedRepresentativeId();
            if (repId == null)
            {
                throw new Exception("Please select a representative!");
            }

            string repData = representatives.Single(s => s.Id.Equals(repId)).Data;
            RepresentativeInfo representativeInfo = JsonConvert.DeserializeObject<RepresentativeInfo>(repData);
            return representativeInfo;
        }

        private RepresentativeInfo GetMedicalRepresentativeObjectOrDie()
        {
            Guid? repId = Mop.GetComboValue(comboMedicalRepresentative);
            if (repId == null)
            {
                throw new Exception("Please select a medical representative!");
            }

            string repData = representatives.Single(s => s.Id.Equals(repId)).Data;
            RepresentativeInfo representativeInfo = JsonConvert.DeserializeObject<RepresentativeInfo>(repData);
            return representativeInfo;
        }

        private ApplicantsDoctors GetFinancialWorkerObjectOrDie()
        {
            Guid? repId = Mop.GetComboValue(comboFin);
            if (repId == null)
            {
                throw new Exception("Please select a medical worker!");
            }

            return doctors.Single(s => s.Id.Equals(repId));
        }

        private ApplicantsRepresentatives GetApplicantsRepresentativesOrDie()
        {
            Guid? repId = GetSelectedRepresentativeId();
            if (repId == null)
            {
                throw new Exception("Please select a representative!");
            }

            return representatives.Single(s => s.Id.Equals(repId));
        }

        private void BtnEditRepresentative_Click(object sender, EventArgs e)
        {
            Guid? repId = GetSelectedRepresentativeId();
            if (repId == null)
            {
                MessageBox.Show("Please select a representative!");
                return;
            }

            ApplicantsRepresentatives rep = Db.LoadRepresentative(repId.Value, logger);

            AddOrEditRepresentative(FormModes.Edit, rep);
        }

        private void AddOrEditRepresentative(FormModes formMode, ApplicantsRepresentatives Representative)
        {
            FormRepresentative representativeForm = new FormRepresentative()
            {
                FormMode = formMode,
                Representative = Representative
            };

            DialogResult res = representativeForm.ShowDialog();

            switch (res)
            {
                case DialogResult.Abort:
                case DialogResult.Cancel:
                    break;

                case DialogResult.OK:
                    RefreshRepresentatives();
                    break;

                default:
                    throw new Exception($"Unknown dialog result: '{res}' occured!");
            }
        }

        private void BtnAddNewRepresentative_Click(object sender, EventArgs e)
        {
            AddOrEditRepresentative(FormModes.New, null);
        }

        private void LoadPDF(PdfiumViewer.PdfViewer pdfViewer, string fileName)
        {
            if (!string.IsNullOrWhiteSpace(fileName))
            {
                pdfViewer.Document?.Dispose();
                pdfViewer.Document = OpenPDFDocument(fileName);
                pdfViewer.ShowBookmarks = false;
                pdfViewer.ShowToolbar = true;
                pdfViewer.ZoomMode = PdfiumViewer.PdfViewerZoomMode.FitWidth;
            }
        }

        private PdfiumViewer.PdfDocument OpenPDFDocument(string fileName)
        {
            try
            {
                return PdfiumViewer.PdfDocument.Load(this, fileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private string DownloadAzureFile(Guid documentId)
        {
            string localFileName;
            if (documentsTemporatyPaths.ContainsKey(documentId))
            {
                localFileName = documentsTemporatyPaths[documentId];
            }
            else
            {
                ApplicantsDocuments applicantDocument = applicantDocuments.Single(s => s.Id.Equals(documentId));

                localFileName = Shared.Source.Mop.GetTempFileName(exactFileName: $"{Path.GetFileNameWithoutExtension(applicantDocument.DocumentName)}-{DateTime.Now.ToString("yyyy-MM-dd--hhmmss")}{Path.GetExtension(applicantDocument.DocumentName)}");
                Shared.Source.Azure.StorageManager.DownloadFileFromAzure(ApplicantData.Id, applicantDocument.Id.ToString(), localFileName);

                documentsTemporatyPaths.Add(documentId, localFileName);
            }

            return localFileName;
        }

        private void PreviewFile(string localFileName)
        {
            string fileExtension = Path.GetExtension(localFileName);

            switch (fileExtension.ToLowerInvariant())
            {
                case ".pdf":
                    pdfViewer.BringToFront();
                    LoadPDF(pdfViewer, localFileName);
                    break;

                case ".docx":
                case ".doc":
                case ".txt":
                case ".csv":
                    richEditControl.BringToFront();
                    richEditControl.LoadDocument(localFileName);
                    break;

                case ".jpg":
                case ".jpeg":
                case ".png":
                case ".bmp":
                case ".gif":
                case ".giff":
                case ".tiff":
                    pictureViewer.BringToFront();
                    pictureViewer.Image = Image.FromFile(localFileName);
                    break;

                default:
                    System.Diagnostics.Process.Start(localFileName);
                    break;
            }
        }

        private void CustomerCurrentLiving_Other_CheckedChanged(object sender, EventArgs e)
        {
            CustomerCurrentLivingOther_Text.Enabled = CustomerCurrentLiving_Other.Checked;
        }

        private void AccomodationsForPrintedLetters_Yes_CheckedChanged(object sender, EventArgs e)
        {
            AccomodationsForPrintedLetters_Group.Enabled = AccomodationsForPrintedLetters_Yes.Checked;
            if (AccomodationsForPrintedLetters_Group.Enabled)
            {
                AccomodationsForPrintedLetters_OtherText.Enabled = AccomodationsForPrintedLetters_Other.Checked;
            }

            AccomodationsForPrintedLetters_WhoIfYes.Enabled = AccomodationsForPrintedLetters_Yes.Checked;
            AccomodationsForPrintedLetters_LargePrint.Enabled = AccomodationsForPrintedLetters_Yes.Checked;
            AccomodationsForPrintedLetters_Email.Enabled = AccomodationsForPrintedLetters_Yes.Checked;
            AccomodationsForPrintedLetters_Other.Enabled = AccomodationsForPrintedLetters_Yes.Checked;
        }

        private void AccomodationsForPrintedLetters_Other_CheckedChanged(object sender, EventArgs e)
        {
            AccomodationsForPrintedLetters_OtherText.Enabled = AccomodationsForPrintedLetters_Other.Checked;
        }

        private void AdditionalQuestions_1_Yes_CheckedChanged(object sender, EventArgs e)
        {
            AdditionalQuestions_1_Yes_Month1.Enabled = AdditionalQuestions_1_Yes.Checked;
            AdditionalQuestions_1_Yes_Month2.Enabled = AdditionalQuestions_1_Yes.Checked;
            AdditionalQuestions_1_Yes_Month3.Enabled = AdditionalQuestions_1_Yes.Checked;
        }

        private void AdditionalQuestions_2_Yes_CheckedChanged(object sender, EventArgs e)
        {
            AdditionalQuestions_2_Yes_Date.Enabled = AdditionalQuestions_2_Yes.Checked;
        }

        private void InterviewInterpretter_Yes_CheckedChanged(object sender, EventArgs e)
        {
            InterviewInterpretter_Language.Enabled = InterviewInterpretter_Yes.Checked;
        }

        private void BtnMedicalParties_CreatreNewDOctor_Click(object sender, EventArgs e)
        {
            using (var doctorForm = new Forms.FormDoctor
            {
                FormMode = FormModes.New,
                Doctor = null
            })
            {
                DialogResult res = doctorForm.ShowDialog();

                switch (res)
                {
                    case DialogResult.Abort:
                    case DialogResult.Cancel:
                        break;

                    case DialogResult.OK:
                        RefreshDoctors();
                        break;

                    default:
                        throw new Exception($"Unknown dialog result: '{res}' occured!");
                }
            }
        }

        private void BtnMedicalParties_AddDoctor_Click(object sender, EventArgs e)
        {
            if (comboMedical_Parties.SelectedItem == null)
            {
                MessageBox.Show("Please select a doctor / physician!");
                if (comboMedical_Parties.CanFocus) comboMedical_Parties.Focus();
                return;
            }

            ComboboxItem comboItem = (ComboboxItem)comboMedical_Parties.SelectedItem;

            foreach (DataGridViewRow row in gridMedical_Parties.Rows)
            {
                Guid rowId = Guid.Parse(row.Cells[0].Value.ToString());
                if (comboItem.Value.Equals(rowId))
                {
                    MessageBox.Show($"The doctor / physician '{comboItem.Text}' is already added to the list!");
                    return;
                }
            }

            AddDoctorInfoToDoctorsGrid((Guid)comboItem.Value);
        }

        private void Interview_Monday_CheckedChanged(object sender, EventArgs e)
        {
            Interview_Monday_Time.Enabled = Interview_Monday.Checked;
        }

        private void Interview_Wednesday_CheckedChanged(object sender, EventArgs e)
        {
            Interview_Wednesday_Time.Enabled = Interview_Wednesday.Checked;
        }

        private void Interview_Friday_CheckedChanged(object sender, EventArgs e)
        {
            Interview_Friday_Time.Enabled = Interview_Friday.Checked;
        }

        private void Interview_Tuesday_CheckedChanged(object sender, EventArgs e)
        {
            Interview_Tuesday_Time.Enabled = Interview_Tuesday.Checked;
        }

        private void Interview_Thursday_CheckedChanged(object sender, EventArgs e)
        {
            Interview_Thursday_Time.Enabled = Interview_Thursday.Checked;
        }

        private void ContextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            medicalContextRemotePhysicians.Enabled = gridMedical_Parties.SelectedRows.Count > 0;
        }

        private void MedicalContextRemotePhysicians_Click(object sender, EventArgs e)
        {
            if (gridMedical_Parties.SelectedRows.Count <= 0)
            {
                MessageBox.Show("Please select a physician/doctor you want to remove from the list!");
                if (gridMedical_Parties.CanFocus)
                    gridMedical_Parties.Focus();

                return;
            }

            foreach (DataGridViewRow selectedRow in gridMedical_Parties.SelectedRows)
            {
                gridMedical_Parties.Rows.Remove(selectedRow);
            }
        }

        private List<AzureToLocalFileDTO> GetAzureBundle(List<ApplicantsDocuments> documents)
        {
            EnsureEditModeOrDie();

            if (documents.Count <= 0)
            {
                MessageBox.Show("Please select at lesat one document");
                return null;
            }

            progress.ShowProgressBar("Preparing documents bundle...");
            try
            {
                var filesDto = new List<AzureToLocalFileDTO>();

                foreach (var document in documents)
                {
                    string localFile = DownloadAzureFile(document.Id);

                    progress.SetProgressText($"Downloading {document.DocumentName}");
                    filesDto.Add(new AzureToLocalFileDTO()
                    {
                        documentId = document.Id,
                        documentType = Enums.Parse<Models.Documents.DocumentType>(document.DocumentType, true),
                        humanFileName = document.DocumentName,
                        localFilePath = localFile,
                    });
                }

                return filesDto;
            }

            finally
            {
                progress.HideProgressBar();
            }
        }

        private void BtnDownloadDocuments_Click(object sender, EventArgs e)
        {
            List<ApplicantsDocuments> applicantsDocuments = GetCheckedDocumentsOnlyOrDie(true, "No documents are selected, which means that all documents related to the applicant will be downloaded!");

            //applicantsDocuments = applicantsDocuments.Where(w => w.DocumentExtraStatus != (int)Models.Documents.DocumentExtraStatus.justPlaceHolder).ToList();

            List<AzureToLocalFileDTO> files = GetAzureBundle(applicantsDocuments);

            if (files == null)
                return;

            string dirName = Path.Combine("bundles", Shared.Source.Mop.ForceValidDir(ApplicantData.Name), DateTime.Now.ToString("yyyy-MM-dd hh-mm-ss"));

            Directory.CreateDirectory(dirName);
            foreach (var file in files)
            {
                string targetFileName = Path.Combine(dirName, Path.GetFileName(file.localFilePath));

                if (File.Exists(targetFileName))
                {
                    targetFileName = Path.GetFileNameWithoutExtension(targetFileName) + "_1" + Path.GetExtension(targetFileName);
                }

                File.Copy(file.localFilePath, targetFileName);
            }

            System.Diagnostics.Process.Start(dirName);
        }

        private List<ApplicantsDocuments> GetCheckedDocumentsOnlyOrDie(bool acceptNothing, string warningIfNothing, bool noneIsOkay = false)
        {
            List<TreeNode> checkedNodes = treeDocsAndTasks
                            .Nodes.Descendants()
                            .Where(w => w.Checked)
                            .ToList();

            if (checkedNodes.Count == 0)
            {
                if (noneIsOkay)
                {
                    return null;
                }

                if (acceptNothing)
                {
                    MessageBox.Show(warningIfNothing);
                    checkedNodes = treeDocsAndTasks
                            .Nodes.Descendants()
                            .ToList();
                }
                else
                {
                    throw new Exception("You need to check/tick at least one document!");
                }
            }

            var documentIds = new List<Guid>();
            foreach (var node in checkedNodes)
            {
                TreeNodeTagInfo nodeInfo = (TreeNodeTagInfo)node.Tag;

                //if single 'leaf' is selected, add it
                if (
                    nodeInfo.nodeType == NodeTypes.documentLeaf
                    || nodeInfo.nodeType == NodeTypes.documentMisc
                    || nodeInfo.nodeType == NodeTypes.documentUncategorized
                    )
                {
                    if (!documentIds.Contains(nodeInfo.id_guid))
                    {
                        documentIds.Add(nodeInfo.id_guid);
                    }

                }

                //if a parent 'placeholder' is selected - add all child documetns
                if (nodeInfo.nodeType == NodeTypes.documentRFI
                    || nodeInfo.nodeType == NodeTypes.documentRejected)
                {
                    foreach (TreeNode childNode in node.Nodes)
                    {
                        TreeNodeTagInfo childNodeInfo = (TreeNodeTagInfo)childNode.Tag;
                        if (!documentIds.Contains(childNodeInfo.id_guid))
                        {
                            documentIds.Add(childNodeInfo.id_guid);
                        }
                    }
                }
            }

            List<ApplicantsDocuments> applicantsDocuments = applicantDocuments
                .Where(w => documentIds.Contains(w.Id))
                .ToList();

            if (applicantsDocuments.Count == 0)
                throw new Exception("You need to check/tick at least one document! There are checked nodes, but not documents!");

            return applicantsDocuments;
        }

        private void SimpleButton3_Click(object sender, EventArgs e)
        {

        }

        private void ComboRepresentatives_SelectedIndexChanged(object sender, EventArgs e)
        {
            RepresentativeInfo repInfo = GetRepresentativeObjectOrDie();

            if (string.IsNullOrWhiteSpace(applicant_Phone.Text?.Trim()))
            {
                applicant_Phone.Text = repInfo.phoneNumber;
            }

            if (string.IsNullOrWhiteSpace(applicant_Email.Text?.Trim()))
            {
                applicant_Email.Text = repInfo.email;
            }

            if (string.IsNullOrWhiteSpace(PersonCompleteingTheForm_Name.Text.Trim()))
            {
                PersonCompleteingTheForm_Name.Text = repInfo.name;
            }

            if (string.IsNullOrWhiteSpace(PersonCompleteingTheForm_Phone.Text.Trim()))
            {
                PersonCompleteingTheForm_Phone.Text = repInfo.phoneNumber;

            }
        }

        private void AddDocumentPlaceholderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode selectedCategoryNode = GetSingleSelectedNode();

            var nodeInfo = (TreeNodeTagInfo)selectedCategoryNode.Tag;
            AssertNodeType(nodeInfo, NodeTypes.categoryRFI);

            using (var form = new FormTextPicker()
            {
                FormMode = FormModes.New,
                ValueCaption = "Select document placeholder name",
                value = ""
            })
            {
                if (form.ShowDialog() != DialogResult.OK)
                    return;

                progress.ShowProgressBar("Inserting the placeholder...");
                try
                {
                    var document = Db.AddApplicantDocument(new ApplicantsDocuments()
                    {
                        ApplicantId = ApplicantData.Id,
                        CorrelationId = null,
                        Id = Guid.NewGuid(),
                        DocumentCategoryId = nodeInfo.Id_int,
                        DocumentExtraStatus = (int)Models.Documents.DocumentExtraStatus.justPlaceHolder,
                        DocumentName = form.value,
                        DocumentType = Models.Documents.DocumentType.other.ToString(),
                        DocumentTypeId = (int)Models.Documents.DocumentTreeType.clientRFI,
                        Status = (int)Models.Documents.DocumentStatus.active,
                        Version = 1
                    }, logger);

                    Db.InsertAction(logger, ApplicantData.Id, ReportActions.RFIAddPlaceholder, document.Id, document.DocumentName);

                    RefreshApplicantDocuments(treeDocsAndTasks.SelectedNode.Name);
                }
                finally
                {
                    progress.HideProgressBar();
                }
            }

        }

        private static void AssertNodeType(TreeNodeTagInfo nodeInfo, NodeTypes expectedNodeType)
        {
            if (nodeInfo.nodeType != expectedNodeType)
            {
                throw new Exception("Unexpected selected node: " + JsonConvert.SerializeObject(nodeInfo));
            }
        }

        private TreeNode GetSingleSelectedNode()
        {
            var selectedNodes = treeDocsAndTasks.Nodes.Descendants().ToList().Where(n => n.IsSelected);
            if (selectedNodes.Count() > 1)
                throw new Exception("Please select only one node!");

            if (selectedNodes.Count() == 0)
                throw new Exception("Please select a node!");

            return selectedNodes.Single();
        }

        private ApplicantsDocuments GetSelectedSingleDocument(TreeNode selectedDocument = null)
        {
            if (selectedDocument == null)
            {
                selectedDocument = GetSingleSelectedNode();
            }

            var nodeInfo = (TreeNodeTagInfo)selectedDocument.Tag;

            //we are pretty sure it's Guid because all documetns holds their id in the tag
            ApplicantsDocuments document = applicantDocuments.Single(s => s.Id.Equals(nodeInfo.id_guid));
            return document;
        }

        private void contextRFI_DocumentClick_Opening(object sender, CancelEventArgs e)
        {
            uploadDocumentToolStripMenuItem.Enabled = true;
            deletePlaceholderToolStripMenuItem.Enabled = true;
            renamePlaceholderToolStripMenuItem.Enabled = true;
            rejectDocumentToolStripMenuItem.Enabled = true;
            approveDocumentToolStripMenuItem.Enabled = true;
        }

        private void deletePlaceholderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteSelectedDocument();
        }

        private void uploadDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ApplicantsDocuments document = GetSelectedSingleDocument();

            var pickFile = new OpenFileDialog
            {
                RestoreDirectory = true,
                Title = "Pick document to upload to the selected placeholder",
                CheckFileExists = true,
                Multiselect = false
            };

            if (pickFile.ShowDialog() != DialogResult.OK)
                return;

            //Upload document to placeholder
            progress.ShowProgressBar("Importing files to placeholder...");
            try
            {
                UploadDocumentToAzureAndDb(
                    pickFile.FileName,
                    Models.Documents.DocumentType.other,
                    null,
                    null,
                    document,
                    true);

                //string newDocName = Path.GetFileNameWithoutExtension(document.DocumentName) + Path.GetExtension(pickFile.FileName);

                //Db.ChangeDocumentRejectReason(document.Id, null, logger);
                //Db.ChangeApplicantDocumentName(document.Id, newDocName, logger);

                RefreshApplicantDocuments(treeDocsAndTasks.SelectedNode.Name);
                PreviewFile(pickFile.FileName);

                ApplicantsDocuments applicantsDocument = Shared.Source.DB.Documents.GetDocumentById(Guid.Parse(treeDocsAndTasks.SelectedNode.Name), logger);

                //Unsigned RFI
                if (applicantsDocument.DocumentCategoryId == 5005)
                {
                    DialogResult result = MessageBox.Show($"Do you want to automatically sent email for email and phone number of PAS assessor?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == DialogResult.Yes)
                    {
                        string info = "", errors = "";

                        Db.DeleteAllScheduledEmails(ApplicantData.Id, logger, Models.Communication.EmailTypes.PasAssssorAutoRequest);

                        MailGenerator.ScheduleEMail(
                            logger,
                            Db.GetUserId(),
                            ApplicantData.Id,
                            FillApplicantModelFromForm(),
                            Models.Communication.EmailTypes.PasAssssorAutoRequest,
                            default(DateTime));

                        BodyGenerators.EmailDataResponse resp = BodyGenerators.GeneratePASInfoRequestData(new BodyGenerators.EmailDataRequest()
                        {
                            applicantModel = applicantModel
                        });

                        //TODO: medical worker or medical representative or something else?
                        ApplicantsDoctors medicalRep = GetFinancialWorkerObjectOrDie();
                        if (string.IsNullOrWhiteSpace(medicalRep.Email) || !medicalRep.Email.Contains("@"))
                            throw new Exception("Empty or invalid financial worker email address!");

                        MailGenerator.SendEmail(
                            ReportActions.ApplicantEmailSendToAskForPASassssor,
                            Guid.Parse(treeDocsAndTasks.SelectedNode.Name),
                            ref info,
                            ref errors,
                            null,
                            null,
                            new string[] { medicalRep.Email },
                            resp.emailBody,
                            resp.subject,
                            logger,
                            ApplicantData.Id,
                            0,
                            resp.representative,
                            Properties.Resources.ResourceManager.GetObject("emailFooter").ToString(),
                            Db.GetUserId()
                        );
                    }
                }
                else

                //Signed RFI
                if (applicantsDocument.DocumentCategoryId == 5003)
                {                    
                    string info = "", errors = "";

                    Db.DeleteAllScheduledEmails(ApplicantData.Id, logger, Models.Communication.EmailTypes.PasAssssorAutoRequest);

                    MailGenerator.ScheduleEMail(
                        logger,
                        Db.GetUserId(),
                        ApplicantData.Id,
                        FillApplicantModelFromForm(),
                        Models.Communication.EmailTypes.PasAssssorAutoRequest,
                        default(DateTime));

                    BodyGenerators.EmailDataResponse resp = BodyGenerators.GeneratePASInfoRequestData(new BodyGenerators.EmailDataRequest()
                    {
                        applicantModel = applicantModel
                    });

                    //TODO: medical worker or medical representative or something else?
                    ApplicantsDoctors finWorkerRep = GetFinancialWorkerObjectOrDie();
                    if (string.IsNullOrWhiteSpace(finWorkerRep.Email) || !finWorkerRep.Email.Contains("@"))
                        throw new Exception("Empty or invalid financial worker email address!");

                    MailGenerator.SendEmail(
                        ReportActions.ApplicantEmailSendToAskForPASassssor,
                        Guid.Parse(treeDocsAndTasks.SelectedNode.Name),
                        ref info,
                        ref errors,
                        null,
                        null,
                        new string[] { finWorkerRep.Email },
                        resp.emailBody,
                        resp.subject,
                        logger,
                        ApplicantData.Id,
                        0,
                        resp.representative,
                        Properties.Resources.ResourceManager.GetObject("emailFooter").ToString(),
                        Db.GetUserId()
                    );                    
                }
            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void deleteDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteSelectedDocument();
        }

        private void DeleteSelectedDocument()
        {
            ApplicantsDocuments document = GetSelectedSingleDocument();

            DialogResult result = MessageBox.Show($"Are you sure you want to delete '{document.DocumentName}'?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.No || result == DialogResult.Cancel)
            {
                return;
            }

            progress.ShowProgressBar("Removing the selected document...");
            try
            {
                //Works for documents AND placeholders
                Db.DeletePlaceholderAndAllRelatedDocuments(document.Id, logger);

                RefreshApplicantDocuments(treeDocsAndTasks.SelectedNode.Name);
            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void viewDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewSelectedDocument();
        }

        private void ViewSelectedDocument()
        {
            ApplicantsDocuments document = GetSelectedSingleDocument();

            progress.ShowProgressBar("Loading file");
            try
            {
                string localFileName = DownloadAzureFile(document.Id);

                PreviewFile(localFileName);

                progress.SetProgressText($"Adding audit action...");
                Db.InsertAction(
                    logger,
                    ApplicantData.Id,
                    ReportActions.RFIViewDocument,
                    document.Id,
                    data: JsonConvert.SerializeObject(new { document.Id, localFileName })
                );

            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void renameDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RenameDocumentOrPlaceholder();
        }

        private void RenameDocumentOrPlaceholder()
        {
            ApplicantsDocuments document = GetSelectedSingleDocument();

            using (var form = new FormTextPicker()
            {
                FormMode = FormModes.New,
                ValueCaption = "Enter new name",
                value = document.DocumentName
            })
            {
                if (form.ShowDialog() != DialogResult.OK)
                    return;

                form.value = Shared.Source.Mop.MakeValidFileName(form.value).Trim();

                progress.ShowProgressBar("Renaming...");
                try
                {
                    Db.ChangeApplicantDocumentName(document.Id, form.value, logger);

                    progress.SetProgressText($"Adding audit action...");
                    Db.InsertAction(
                        logger,
                        ApplicantData.Id,
                        ReportActions.RFIRenameDocument,
                        document.Id,
                        data: JsonConvert.SerializeObject(new
                        {
                            from = document.DocumentName,
                            to = form.value
                        })
                    );

                    RefreshApplicantDocuments(treeDocsAndTasks.SelectedNode.Name);
                }
                finally
                {
                    progress.HideProgressBar();
                }
            }
        }

        private void rejectDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ApplicantsDocuments document = GetSelectedSingleDocument();

            progress.ShowProgressBar("Rejecting document...");
            try
            {
                using (var form = new FormTextPicker()
                {
                    FormMode = FormModes.New,
                    ValueCaption = "Enter reject reason",
                    value = string.Empty
                })
                {
                    if (form.ShowDialog() != DialogResult.OK)
                        return;

                    Db.ChangeApplicantDocumentTreeType(document.Id, Models.Documents.DocumentTreeType.rejectedDocument, logger);
                    Db.ChangeDocumentRejectReason(document.Id, form.value, logger);

                    //insert placeholder with the same name
                    Db.AddApplicantDocument(new ApplicantsDocuments()
                    {
                        ApplicantId = ApplicantData.Id,
                        CorrelationId = null,
                        Id = Guid.NewGuid(),
                        DocumentCategoryId = document.DocumentCategoryId,
                        DocumentExtraStatus = (int)Models.Documents.DocumentExtraStatus.justPlaceHolder,
                        DocumentName = document.DocumentName,
                        DocumentType = Models.Documents.DocumentType.other.ToString(),
                        DocumentTypeId = (int)Models.Documents.DocumentTreeType.clientRFI,
                        Status = (int)Models.Documents.DocumentStatus.active,
                        Version = 1,
                        RejectReason = form.value
                    }, logger);

                    progress.SetProgressText($"Adding audit action...");
                    Db.InsertAction(
                        logger,
                        ApplicantData.Id,
                        ReportActions.RFIRejectDocument,
                        document.Id,
                        form.value);

                    RefreshApplicantDocuments(treeDocsAndTasks.SelectedNode.Name);
                }

            }
            finally
            {
                progress.HideProgressBar();
            }

        }

        private void approveDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ApplicantsDocuments document = GetSelectedSingleDocument();

            progress.ShowProgressBar("Approving document...");
            try
            {
                Db.ChangeApplicantDocumentExtraStatus(document.Id, Models.Documents.DocumentExtraStatus.fileIsUploadedAndAccepted, logger);

                progress.SetProgressText($"Adding audit action...");
                Db.InsertAction(
                    logger,
                    ApplicantData.Id,
                    ReportActions.RFIApproveDocument,
                    document.Id,
                    null);

                RefreshApplicantDocuments(treeDocsAndTasks.SelectedNode.Name);
            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void treeDocsAndTasks_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (!documentsLoaded)
            {
                return;
            }

            //Now we need to understand what is the new selection.
            TreeNodeTagInfo nodeInfo = (TreeNodeTagInfo)(e.Node?.Tag ?? treeDocsAndTasks.SelectedNode.Tag);

            //Models.Form.NodeTypes selectedNode = GetCurrentNodeTreeSelection(e.Node);

            textRejectReason.ReadOnly = true;
            textRejectReason.Text = "N/A";

            switch (nodeInfo.nodeType)
            {
                case Models.Form.NodeTypes.rootMiscDocuments:
                    textDocumentNotes.Text = "This is the place where All Misc Doducments are";
                    textDocumentNotes.ReadOnly = true;
                    break;

                case Models.Form.NodeTypes.rootRejectedOcuments:
                    textDocumentNotes.Text = "This is the place where All Rejected Doducments are";
                    textDocumentNotes.ReadOnly = true;
                    break;

                case Models.Form.NodeTypes.rootRFI:
                    textDocumentNotes.Text = "This is the place where All RFI Doducments are";
                    textDocumentNotes.ReadOnly = true;
                    break;

                case Models.Form.NodeTypes.rootUncategorizedDocuments:
                    textDocumentNotes.Text = "This is the place where All Uncategorized Doducments are";
                    textDocumentNotes.ReadOnly = true;
                    break;

                case Models.Form.NodeTypes.rootTasksClientFacing:
                    textDocumentNotes.Text = "This is the place where All Tasks which are Client Facing are";
                    textDocumentNotes.ReadOnly = true;
                    break;

                case Models.Form.NodeTypes.rootTasksNonClientFacing:
                    textDocumentNotes.Text = "This is the place where All Tasks which are Not Client Facing are";
                    textDocumentNotes.ReadOnly = true;
                    break;

                case Models.Form.NodeTypes.documentMisc:
                case Models.Form.NodeTypes.documentRejected:
                case Models.Form.NodeTypes.documentUncategorized:
                case Models.Form.NodeTypes.documentRFI:
                    ApplicantsDocuments document = GetSelectedSingleDocument(e.Node);

                    textDocumentNotes.Text = document.Notes;
                    textDocumentNotes.ReadOnly = false;

                    if (Models.Form.NodeTypes.documentRejected == nodeInfo.nodeType
                        || Models.Form.NodeTypes.documentRFI == nodeInfo.nodeType)
                    {
                        textRejectReason.ReadOnly = false;
                        textRejectReason.Text = document.RejectReason;
                    }

                    break;

                case Models.Form.NodeTypes.tasksClientFacing:
                    throw new NotImplementedException();

                case Models.Form.NodeTypes.tasksNonClientFacing:
                    throw new NotImplementedException();

                case Models.Form.NodeTypes.categoryRFI:
                    int rfiCategoryId = nodeInfo.Id_int;

                    //var selectedNodes = nodeRFI.Nodes.Descendants().ToList().Where(n => (n.Tag is int) && (int)n.Tag == rfiCategoryId);
                    //if (selectedNodes.Count() > 1) throw new Exception($"Multiple categories with {rfiCategoryId} tag?");
                    //if (selectedNodes.Count() == 0) throw new Exception($"A category with id {rfiCategoryId} can't be found");
                    //var selectedCategory = selectedNodes.Single();

                    textDocumentNotes.Text = Shared.Source.Mop.GetCategoryDescriptionWithRespectToCustom(rfiCategoryId, customCategoryDescriptions, documentCategories);

                    break;
            }

            btnSaveNotes.Enabled = !textDocumentNotes.ReadOnly;
            btnSaveRejectReason.Enabled = !textRejectReason.ReadOnly;
        }

        private void btnSendEmailType_Click(object sender, EventArgs e)
        {
            EnsureEditModeOrDie();

            List<TreeNode> allChecked = treeDocsAndTasks
                    .Nodes.Descendants()
                    .Where(w => w.Checked)
                    .ToList();

            ApplicantsRepresentatives representativeInfo = GetSelectedRepresentativeOrDie();

            List<ApplicantsDocuments> attachedDocuments;
            string emailBody;
            string subject;
            Models.Communication.EmailTypes emailType;

            switch (comboEmailTypes.SelectedIndex)
            {
                //RFI
                case 0:
                    subject = "Outstanding Documents Needed";
                    //No attached documents when RFI

                    attachedDocuments = null;
                    emailType = Models.Communication.EmailTypes.RFI;

                    if (allChecked.Count() > 0)
                    {
                        MessageBox.Show("The generated 'request for documents' e-mail will include all applicant documents and (client facing) tasks so far. " +
                            "Your selected documents and tasks will not be respected. If you want that changed: \n" +
                            " - if nothing is selected generate all (like now)\n " +
                            " - if only some stuff is selected - respect it\n" +
                            "Can be done, if needed - request it, and I will do it.\n" +
                            "If you need it right now - just edit the body of the email manually be deleting the not required stuff.");
                    }

                    emailBody = Shared.Source.MailGeneration.BodyGenerators.GenerateRFIBody(applicantDocuments, documentCategories, customCategoryDescriptions, ApplicantData, out int requestedDocumentsCount);
                    break;

                //Blank with attachments
                case 1:
                    //Get all checked documents
                    attachedDocuments = GetCheckedDocumentsOnlyOrDie(false, null, true);

                    ////skip placeholders
                    //attachedDocuments = attachedDocuments
                    //    .Where(w => w.DocumentExtraStatus != (int)Models.Documents.DocumentExtraStatus.justPlaceHolder)
                    //    .ToList();

                    emailBody = "Feel free to express your email message as a text. You can use some <b>html decorators</b> if you wish to.";
                    subject = "The blank email subject";
                    emailType = Models.Communication.EmailTypes.FreeText;

                    break;

                default:
                    throw new Exception($"The email type with index {comboEmailTypes.SelectedIndex} is not implemented");
            }

            List<AzureToLocalFileDTO> files = null;
            if (attachedDocuments != null && attachedDocuments.Count > 0)
            {
                files = GetAzureBundle(attachedDocuments);
            }

            new FormSendEmail()
            {
                FilesToSend = files,
                ApplicantData = ApplicantData,
                Representative = representativeInfo,
                Body = emailBody,
                To = poc_Email.Text,
                FromSpecialAccount = 1,
                Subject = subject,
                emailType = emailType
            }.ShowDialog();
        }

        private void btnSaveNotes_Click(object sender, EventArgs e)
        {
            //Models.Form.NodeTypes selectedNode = GetCurrentNodeTreeSelection(null);
            TreeNodeTagInfo nodeInfo = (TreeNodeTagInfo)treeDocsAndTasks.SelectedNode.Tag;

            switch (nodeInfo.nodeType)
            {
                case NodeTypes.documentMisc:
                case NodeTypes.documentRejected:
                case NodeTypes.documentUncategorized:
                case NodeTypes.documentRFI:
                    ApplicantsDocuments document = GetSelectedSingleDocument(treeDocsAndTasks.SelectedNode);

                    progress.ShowProgressBar("Saving document notes...");
                    try
                    {
                        Db.ChangeDocumentNotes(document.Id, textDocumentNotes.Text, logger);

                        progress.SetProgressText($"Adding audit action...");
                        Db.InsertAction(
                            logger,
                            ApplicantData.Id,
                            ReportActions.ChangeDocumentNotes,
                            document.Id,
                            textDocumentNotes.Text);

                        RefreshApplicantDocuments(treeDocsAndTasks.SelectedNode.Name);
                    }
                    finally
                    {
                        progress.HideProgressBar();
                    }

                    break;

                case Models.Form.NodeTypes.tasksClientFacing:
                    throw new NotImplementedException();

                case Models.Form.NodeTypes.tasksNonClientFacing:
                    throw new NotImplementedException();

                case Models.Form.NodeTypes.categoryRFI:
                    int categoryId = nodeInfo.Id_int;

                    Models.Documents.CustomCategoryDescription customCategory = customCategoryDescriptions.SingleOrDefault(s => s.categoryId == categoryId);
                    if (customCategory != null)
                    {
                        customCategory.categoryDescription = textDocumentNotes.Text;
                    }
                    else
                    {
                        customCategoryDescriptions.Add(new Models.Documents.CustomCategoryDescription()
                        {
                            categoryDescription = textDocumentNotes.Text,
                            categoryId = categoryId
                        });
                    }

                    progress.ShowProgressBar("Saving custom cateogry desctiption...");
                    try
                    {
                        Db.ChangeApplicantCustomCategoryDescriptions(
                            ApplicantData.Id,
                            JsonConvert.SerializeObject(customCategoryDescriptions),
                            logger);

                        progress.SetProgressText($"Adding audit action...");

                        Db.InsertAction(
                            logger,
                            ApplicantData.Id,
                            ReportActions.RFICategoryAddCustomDescription,
                            null,
                            JsonConvert.SerializeObject(new Models.Documents.CustomCategoryDescription()
                            {
                                categoryDescription = textDocumentNotes.Text,
                                categoryId = categoryId
                            })
                        );
                    }
                    finally
                    {
                        progress.HideProgressBar();
                    }

                    break;
            }

        }

        private void renamePlaceholderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RenameDocumentOrPlaceholder();
        }

        private void btnDeleteDocuments_Click(object sender, EventArgs e)
        {
            //Make it work again

            List<ApplicantsDocuments> applicantsDocuments = GetCheckedDocumentsOnlyOrDie(false, null);

            DialogResult result = MessageBox.Show("Are you sure you want to delete the selected documents?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.No || result == DialogResult.Cancel)
            {
                return;
            }

            progress.ShowProgressBar("Removing documents...");
            try
            {
                foreach (var document in applicantsDocuments)
                {
                    Db.DeletePlaceholderAndAllRelatedDocuments(document.Id, logger);
                }
                //Db.ChangeApplicantDocumentsStatus(
                //    applicantsDocuments.Select(s => s.Id),
                //    ApplicantData.Id,
                //    Models.Documents.DocumentStatus.deleted,
                //    logger);

                //progress.SetProgressText($"Adding audit action...");

                //Db.InsertActions(
                //    logger,
                //    ApplicantData.Id,
                //    ReportActions.DocumentDelete,
                //    applicantsDocuments.Select(s => s.Id).ToList(),
                //    datas: applicantsDocuments.Select(s => s.Id.ToString()).ToList()
                //);

                RefreshApplicantDocuments(null);
            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void viewDocumentToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ViewSelectedDocument();
        }

        private void deleteDocumentToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DeleteSelectedDocument();
        }

        private void renameDocumentToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            RenameDocumentOrPlaceholder();
        }

        private void btnSendFaxType_Click(object sender, EventArgs e)
        {
            List<ApplicantsDocuments> applicantsDocuments = GetCheckedDocumentsOnlyOrDie(false, null);

            //applicantsDocuments = applicantsDocuments.Where(w => w.DocumentExtraStatus != (int)Models.Documents.DocumentExtraStatus.justPlaceHolder).ToList();

            List<AzureToLocalFileDTO> files = GetAzureBundle(applicantsDocuments);

            if (files == null)
                return;

            var formFaxSend = new FormFaxSend()
            {
                ApplicantData = ApplicantData,
                Representative = GetApplicantsRepresentativesOrDie(),
                FilesToSend = files
            };

            formFaxSend.ShowDialog();
        }

        private void manageRFICategoriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var form = new FormCategoriesRegister())
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    RefreshCategories(true, null);
                }
            }
        }

        private void newRFICategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var form = new FormCategoryDetails())
            {
                form.FormMode = FormModes.New;

                if (form.ShowDialog() == DialogResult.OK)
                {
                    RefreshCategories(true, null);
                }
            }
        }

        private void editCategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var form = new FormCategoryDetails())
            {
                TreeNodeTagInfo nodeInfo = (TreeNodeTagInfo)treeDocsAndTasks.SelectedNode.Tag;

                form.FormMode = FormModes.Edit;
                form.Category = documentCategories.Single(s => s.Id.Equals(nodeInfo.Id_int));

                if (form.ShowDialog() == DialogResult.OK)
                {
                    RefreshCategories(true, null);
                }
            }
        }

        private void deleteCategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNodeTagInfo nodeInfo = (TreeNodeTagInfo)treeDocsAndTasks.SelectedNode.Tag;
            int categoryId = nodeInfo.Id_int;

            DialogResult result = MessageBox.Show("Are you sure you want to delete the selected category? This will also delete all documents related to that category!", "Warning", MessageBoxButtons.YesNo);
            if (result == DialogResult.No || result == DialogResult.Cancel)
            {
                return;
            }

            Shared.Source.DB.Documents.DeleteCategory(categoryId, logger);
            RefreshCategories(true, null);
        }

        private void autoEmail_RFI_Enabled_CheckedChanged(object sender, EventArgs e)
        {
            HandleAutoEmailEvents();
        }

        private void treeDocsAndTasks_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            treeDocsAndTasks.SelectedNode = e.Node;
        }

        private void treeDocsAndTasks_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
            //groupControl1.Text = e.Data.GetData(DataFormats.FileDrop).ToString();
        }

        private void treeDocsAndTasks_ItemDrag(object sender, ItemDragEventArgs e)
        {

        }

        private void treeDocsAndTasks_DragDrop(object sender, DragEventArgs e)
        {
            // Retrieve the client coordinates of the drop location.  
            Point targetPoint = treeDocsAndTasks.PointToClient(new Point(e.X, e.Y));

            // Retrieve the node at the drop location.  
            TreeNode targetNode = treeDocsAndTasks.GetNodeAt(targetPoint);
            TreeNodeTagInfo nodeInfo = (TreeNodeTagInfo)targetNode.Tag;

            if (nodeInfo.nodeType != NodeTypes.documentRFI)
            {
                MessageBox.Show("You need to select a document/placeholder in which to drop the selected files!");
                return;
            }

            string[] filesToDrop = (string[])e.Data.GetData("FileDrop");

            DialogResult result = MessageBox.Show($"You are about to drop the following documents into {targetNode.Text}. Are you sure you want to continue?" + Environment.NewLine + string.Join(Environment.NewLine, filesToDrop), "Files drop", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.No || result == DialogResult.Cancel)
            {
                return;
            }

            progress.ShowProgressBar("Uploading documents...");
            try
            {
                var parentDocument = applicantDocuments.Single(s => s.Id.Equals(((TreeNodeTagInfo)targetNode.Tag).id_guid));

                foreach (var file in filesToDrop)
                {
                    //upload doc to placeholder
                    UploadDocumentToAzureAndDb(
                        file,
                        Models.Documents.DocumentType.other,
                        Guid.NewGuid(),
                        null,
                        parentDocument,
                        true);

                    //Db.InsertAction(logger, ApplicantData.Id, ReportActions.RFIUploadDocumentDragAndDrop, document.Id, document.DocumentName);
                }

                RefreshApplicantDocuments(treeDocsAndTasks.SelectedNode.Name);
            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void treeDocsAndTasks_DragLeave(object sender, EventArgs e)
        {

        }

        private void treeDocsAndTasks_DragOver(object sender, DragEventArgs e)
        {
            // Retrieve the client coordinates of the mouse position.  
            Point targetPoint = treeDocsAndTasks.PointToClient(new Point(e.X, e.Y));

            // Select the node at the mouse position.  
            treeDocsAndTasks.SelectedNode = treeDocsAndTasks.GetNodeAt(targetPoint);
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            return;
            //using (var db = new DbModel())
            //{
            //    var documents = db.ApplicantsDocuments
            //        .Where(w => !w.ApplicantId.Equals(ApplicantData.Id))
            //        .Where(w => w.Status.Equals(1))
            //        .Where(w => w.DocumentTypeId == 1 || w.DocumentTypeId == 6)
            //        .Where(w => w.DocumentCategoryId != null)
            //        .ToList();

            //    foreach (var document in documents)
            //    {
            //        var placeholder = db.ApplicantsDocuments.Add(new ApplicantsDocuments()
            //        {
            //            ApplicantId = document.ApplicantId,
            //            CorrelationId = null,
            //            CreatedByUserId = document.CreatedByUserId,
            //            CreatedOn = document.CreatedOn,
            //            DocumentCategoryId = document.DocumentCategoryId,
            //            DocumentExtraStatus = document.DocumentExtraStatus,
            //            DocumentName = Path.GetFileNameWithoutExtension(document.DocumentName),
            //            DocumentType = document.DocumentType,
            //            DocumentTypeId = document.DocumentTypeId,
            //            Id = Guid.NewGuid(),
            //            Notes = document.Notes,
            //            RejectReason = document.RejectReason,
            //            Status = (int)Models.Documents.DocumentStatus.active,
            //            Version = 1,
            //            DocumentParentId = null,
            //        });

            //        //Files in placeholders don't have 
            //        //   - Category Id, because they belong to the placeholder, which HAS categoryId
            //        //   - extra Status,  because the whole document is approved/rejected

            //        if (document.DocumentExtraStatus == (int)Models.Documents.DocumentExtraStatus.justPlaceHolder)
            //        {
            //            //this means that there is NO file in the placeholder
            //            document.Status = (int)Models.Documents.DocumentStatus.deleted;
            //        }

            //        document.DocumentParentId = placeholder.Id;
            //        document.DocumentCategoryId = null;
            //        document.DocumentExtraStatus = null;
            //        document.RejectReason = null;

            //    }

            //    db.SaveChanges();
            //}

            //RefreshApplicantDocuments(null);
        }

        private void viewDocumentToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ViewSelectedDocument();
        }

        private void renameDocumentToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            RenameDocumentOrPlaceholder();
        }

        private void deleteDocumentToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            DeleteSelectedDocument();
        }

        private void combineSelectedPDFsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var documentIDsToCombine = GetCheckedDocumentsOnlyOrDie(false, null, false);
            Guid parentId = documentIDsToCombine.First().DocumentParentId.Value;
            var parentDoc = applicantDocuments.Single(s => s.Id == parentId);

            progress.ShowProgressBar("Importing files to placeholder...");
            try
            {
                List<AzureToLocalFileDTO> files = GetAzureBundle(documentIDsToCombine);

                string targetFileName = Shared.Source.Mop.GetTempFileName(extension: ".pdf");

                Shared.Source.PDFWorker.MergePDFFiles(files.Select(s => s.localFilePath), targetFileName, logger);

                UploadDocumentToAzureAndDb(
                    targetFileName,
                    Models.Documents.DocumentType.other,
                    null,
                    null,
                    parentDoc,
                    true);

                RefreshApplicantDocuments(treeDocsAndTasks.SelectedNode.Name);
                PreviewFile(targetFileName);
            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void btnStamp_Click(object sender, EventArgs e)
        {
            var documentIDsToCombine = GetCheckedDocumentsOnlyOrDie(false, null, false);
            //Guid parentId = documentIDsToCombine.First().DocumentParentId.Value;
            //var parentDoc = applicantDocuments.Single(s => s.Id == parentId);

            progress.ShowProgressBar("Importing files to placeholder...");
            try
            {
                List<AzureToLocalFileDTO> files = GetAzureBundle(documentIDsToCombine);

                string targetFileName = Shared.Source.Mop.GetTempFileName(extension: ".pdf");
                targetFileName = Shared.Source.PDFWorker.StampPdf(files.First().localFilePath, targetFileName, "test text");
            }
            finally
            {
                progress.HideProgressBar();
            }
        }

        private void btnOpenWorkersRegister_Click(object sender, EventArgs e)
        {
            OpenDoctorsRegister();
        }

        private void OpenDoctorsRegister()
        {
            using (var doctorsRegister = new FormDoctorsRegister())
            {
                doctorsRegister.ShowDialog();
            }

            RefreshDoctors();
        }

        private void btnViewFin_Click(object sender, EventArgs e)
        {
            OpenDoctorsRegister();
        }

        private void comboFin_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid? selectedWorkerId = Mop.GetComboValue(comboFin);
            if (selectedWorkerId == null) return;

            ApplicantsDoctors worker = doctors.Single(s => s.Id.Equals(selectedWorkerId));

            txtFinWorkerAddress.Text = worker.Address;
            txtFinWorkerEmail.Text = worker.Email;
            txtFinWorkerFax.Text = worker.OfficeFax;
            txtFinWorkerPhone.Text = worker.OfficeTelephoneNumber;
        }

        private void btnViewFinSuper__Click(object sender, EventArgs e)
        {
            OpenDoctorsRegister();
        }

        private void comboFinSup_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid? selectedWorkerId = Mop.GetComboValue(comboFinSup);
            if (selectedWorkerId == null) return;

            ApplicantsDoctors worker = doctors.Single(s => s.Id.Equals(selectedWorkerId));

            txtFinSuperAddress.Text = worker.Address;
            txtFinSuperEmail.Text = worker.Email;
            txtFinSuperFax.Text = worker.OfficeFax;
            txtFinSuperPhone.Text = worker.OfficeTelephoneNumber;
        }

        private void btnCiewMed_Click(object sender, EventArgs e)
        {
            OpenDoctorsRegister();
        }

        private void comboMed_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid? selectedWorkerId = Mop.GetComboValue(comboMed);
            if (selectedWorkerId == null) return;

            ApplicantsDoctors worker = doctors.Single(s => s.Id.Equals(selectedWorkerId));

            txtMedAddress.Text = worker.Address;
            txtMedEmail.Text = worker.Email;
            txtMedFax.Text = worker.OfficeFax;
            txtMedPhone.Text = worker.OfficeTelephoneNumber;
        }

        private void btnAddNewMedicalRepresentative_Click(object sender, EventArgs e)
        {
            AddOrEditRepresentative(FormModes.New, null);
        }

        private void btnEditSelectedMedicalRepresentative_Click(object sender, EventArgs e)
        {
            Guid? selectedMedRepId = Mop.GetComboValue(comboMed);

            if (selectedMedRepId == null)
            {
                MessageBox.Show("Please select a medical representative!");
                return;
            }

            ApplicantsRepresentatives rep = Db.LoadRepresentative(selectedMedRepId.Value, logger);
            AddOrEditRepresentative(FormModes.Edit, rep);
        }

        private void checkMedicalCaseWorker_MedAPprovalDate_CheckedChanged(object sender, EventArgs e)
        {
            dateMedicalCaseWorker_MedAPprovalDate.Enabled = checkMedicalCaseWorker_MedAPprovalDate.Checked;
        }

        private void btnMedApproveDateSendEmail_Click(object sender, EventArgs e)
        {
            if (!checkMedicalCaseWorker_MedAPprovalDate.Checked)
            {
                MessageBox.Show("Please tick the box and enter exam date, then click this button, thank you");
                return;
            }

            DialogResult result = MessageBox.Show($"Do you want to sent email that the applicant does not need a med evaluator?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                Db.DeleteAllScheduledEmails(ApplicantData.Id, logger, Models.Communication.EmailTypes.PasAssssorAutoRequest);

                BodyGenerators.EmailDataResponse resp = BodyGenerators.GenerateMedicalEvaluationConfirmationEmail(dateMedicalCaseWorker_MedAPprovalDate.Value.ToString(Resources.USADateOnly));

                string info = string.Empty;
                string errors = string.Empty;

                MailGenerator.SendEmail(
                    ReportActions.ApplicantEmailSendToConfirmMedicalExac,
                    ApplicantData.Id,
                    ref info,
                    ref errors,
                    null,
                    null,
                    new string[] { poc_Email.Text },
                    resp.emailBody,
                    resp.subject,
                    logger,
                    ApplicantData.Id,
                    0,
                    GetApplicantsRepresentativesOrDie(),
                    Properties.Resources.ResourceManager.GetObject("emailFooter").ToString(),
                    Db.GetUserId()
                );

                MessageBox.Show("Email sent successfully! Don't forget to click Save! Thank you!");
            }
        }

        private void autoEmail_LiteRFI_IsEnabled_Click(object sender, EventArgs e)
        {
            string pocEmail = JsonConvert.DeserializeObject<FullApplicantInfo>(ApplicantData.Data).pointOfContact?.email;

            if (string.IsNullOrWhiteSpace(pocEmail))
            {
                autoEmail_LiteRFI_IsEnabled.Checked = false;
                MessageBox.Show("The POC email is empty. Please fill it and come back here again.");
                return;
            }

            if (pocEmail.Trim().EndsWith("SENIORPLANNING.ORG", StringComparison.OrdinalIgnoreCase))
            {
                MessageBox.Show($"ERROR!!! This is being sent to {pocEmail}!!! This is internal email and should not be done.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                autoEmail_LiteRFI_IsEnabled.Checked = false;
                return;
            }
        }
    }
}