[5/2/2019 11:16:18 PM] Svetoslav Mitov: I think I understand what's needed, I have few questions


Can one entry field from the program be used for both DE-101 and DE-112, for example "Name of the Customer’s Authorized Representative" from DE-101 and and "Representative’s Name" from DE-112? This would makes filling the forms faster, because the same information will not be entered twise (if it's the same, but for my perspective there are some fields which looks the same)
Is there an editable version of DE-101 and DE-112, I mean version in which you can enter the data manually? Or .docx file? If not, it's not a problem, I will just try to edit the pdf documents, which if fails (possible), I will re-create them using html, fill that html and convert to pdf. Still I think the first option will work. I will test tomorrow.
Can you send me a scanned example of the 12 paged document which will be shown on the left side of the screen? If not - it's not a problem, I can use something else.
For sending faxes I guess we will need to use https://www.twilio.com/fax or something like that, what do you think?
About Sending email - who will be the sender? Should the user/pass be stored? Should there be login form (with Remember me) as it is with the Assisted living program? If yes it can use the same user database as the Assisted Living program. Or offcourse use it's own, or not have any login, just fields for eMail sender - then enter user/pass. Up to you.
About the files which will be stored - they will be stored on the local system? Do you need auto backup for them?
About the audit trail - it should be able to show the audit information on only the local system, or from all computer systems which are used?
Important You already have and pay for database on smarterasp, do you want me to use it for this project too, which will help in "centralizing" the statistics, audit trails, the users, etc, or you prefer each system to be totally stand alone and having no connection to anything remote?


[5/2/2019 11:23:23 PM] Jacob Gottlieb:
Yes, fields that call from the same information on both forms can be filled the same way.


[5/2/2019 11:24:09 PM] Jacob Gottlieb:
I am not aware of an editable PDF verison.


[5/2/2019 11:24:29 PM] Jacob Gottlieb:
yes, I will send that in a moment.


[5/2/2019 11:25:05 PM] Jacob Gottlieb:
We use smart fax right now, but we are open to using something else if that doesn't work


[5/2/2019 11:26:43 PM] Jacob Gottlieb:
The sender should be the individual user. For this program we probably want to call for the username and password on each log in. It will have the same users as the assisted living program.


[5/2/2019 11:27:28 PM] Jacob Gottlieb:
Probably not local storage for the files. All users need to be able to access the files.


[5/2/2019 11:28:21 PM] Jacob Gottlieb:
Audit trail should include all user activity, and should be admin level only.


[5/2/2019 11:28:44 PM] Jacob Gottlieb:
That should be fine for now.


[5/2/2019 11:29:58 PM] Svetoslav Mitov:
You are using this - https://www.smartfax.com ?


[5/2/2019 11:30:09 PM] Jacob Gottlieb: yeah - we would want twilio

[5/2/2019 11:31:11 PM] Svetoslav Mitov: great, I am familiar with twilio because of the sms thing, so I will integrate the fax faster, twilio also have really nice api to use

[5/2/2019 11:31:44 PM] Jacob Gottlieb: yeah and eventually we want reconciliation and each user to have their own fax line but now that isn't required but twilio is way more robust

[5/2/2019 11:31:52 PM] Jacob Gottlieb: and api exists

[5/2/2019 11:32:15 PM] Svetoslav Mitov: okay

[5/2/2019 11:32:22 PM] Svetoslav Mitov:
So have login form, but not "Remember me" thick, right?


[5/2/2019 11:36:34 PM] Jacob Gottlieb: just individual logins

[5/2/2019 11:36:37 PM] Jacob Gottlieb: same as the other program

[5/2/2019 11:36:57 PM] Jacob Gottlieb: I wouldn't do remember me with this system

[5/2/2019 11:37:20 PM] Svetoslav Mitov:
There are a couple of options, but some "cloud based" solution will need to be used. Generally storing files is very cheep, you will pay less than a dollar per month on Azure or some other service. And you will have the files auto backed uped, available to all users on any pc, etc, which is really nice.
Audit trail for all user activity definitely requires remote database. If you want other database - it's possible but would cost you some more money, so we can use the same, except you definitely think it should be different.


[5/2/2019 11:38:17 PM] Svetoslav Mitov: 5 - okay

[5/2/2019 11:43:19 PM] Jacob Gottlieb: Sounds good thanks

[5/2/2019 11:44:19 PM] Svetoslav Mitov: Okay, great, anything is clear for now, I will start tomorrow (my time, it's 11:44pm) and I will keep you updated

[5/2/2019 11:44:52 PM] Jacob Gottlieb: what kind of time frame do you think in regards to this initial project? This is stage 1 and we have some other add ons as well

[5/2/2019 11:47:36 PM] Svetoslav Mitov: I won't be able to work those Sathurday and Sunday, sadly and Monday, it's official national off-day - Saint George's Day
so.. maybe the end of the next week 10th-12th of May I will have a running application with the discussed functionality.