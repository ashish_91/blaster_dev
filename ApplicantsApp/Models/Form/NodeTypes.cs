﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Form
{
    public enum NodeTypes
    {
        //root
        rootRFI,
        rootMiscDocuments,
        rootRejectedOcuments,
        rootUncategorizedDocuments,
        rootTasksClientFacing,
        rootTasksNonClientFacing,

        //1st level
        categoryRFI,
        documentMisc,
        documentRejected,
        documentUncategorized,
        tasksClientFacing,
        tasksNonClientFacing,

        //2nd level
        documentRFI,

        //3th level   (document page)
        documentLeaf
    }
}