﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicantsApp.Models.Form
{
    public class TreeNodeTagInfo
    {
        public NodeTypes nodeType;
        public int Id_int;
        public Guid id_guid;
        public Guid correlationId;
        public object link;

        public TreeNodeTagInfo(NodeTypes nodeTypes)
        {
            this.nodeType = nodeTypes;
        }

        public TreeNodeTagInfo(NodeTypes nodeTypes, int id_int) : this(nodeTypes)
        {
            Id_int = id_int;
        }

        public TreeNodeTagInfo(NodeTypes nodeTypes, Guid id_guid) : this(nodeTypes)
        {
            this.id_guid = id_guid;
        }
    }
}
