﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicantsApp.UserControls
{
    [System.Runtime.InteropServices.ComVisible(true)]
    [System.Runtime.InteropServices.ClassInterface(System.Runtime.InteropServices.ClassInterfaceType.AutoDispatch)]
    public class RFIGroupTemplate : System.Windows.Forms.UserControl
    {
        private System.Windows.Forms.GroupBox groupTaskTemplate;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textTaskNotes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox checkedListAttachedFiles;
        private System.Windows.Forms.Button btnAddFile;
        private System.Windows.Forms.Button btnDeleteFile;
        private System.Windows.Forms.CheckBox checkTaskIsDone;
        private System.Windows.Forms.Button btnEditTask;
        private System.Windows.Forms.Button btnDeleteTask;
        private System.Windows.Forms.Button btnViewFile;
        private System.Windows.Forms.Button AddNewTask;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox textTaskName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboTask;

        public RFIGroupTemplate(long taskId)
        {
            InitializeComponent(taskId);
        }

        private void InitializeComponent(long taskId)
        {
            this.groupTaskTemplate = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.textTaskNotes = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkedListAttachedFiles = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnDeleteFile = new System.Windows.Forms.Button();
            this.btnAddFile = new System.Windows.Forms.Button();
            this.checkTaskIsDone = new System.Windows.Forms.CheckBox();
            this.btnDeleteTask = new System.Windows.Forms.Button();
            this.btnEditTask = new System.Windows.Forms.Button();
            this.btnViewFile = new System.Windows.Forms.Button();
            this.AddNewTask = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.comboTask = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textTaskName = new System.Windows.Forms.TextBox();

            // 
            // groupTaskTemplate
            // 
            this.groupTaskTemplate.Controls.Add(this.textTaskName);
            this.groupTaskTemplate.Controls.Add(this.label4);
            this.groupTaskTemplate.Controls.Add(this.label3);
            this.groupTaskTemplate.Controls.Add(this.comboTask);
            this.groupTaskTemplate.Controls.Add(this.btnViewFile);
            this.groupTaskTemplate.Controls.Add(this.checkTaskIsDone);
            this.groupTaskTemplate.Controls.Add(this.btnAddFile);
            this.groupTaskTemplate.Controls.Add(this.btnEditTask);
            this.groupTaskTemplate.Controls.Add(this.btnDeleteTask);
            this.groupTaskTemplate.Controls.Add(this.btnDeleteFile);
            this.groupTaskTemplate.Controls.Add(this.label2);
            this.groupTaskTemplate.Controls.Add(this.checkedListAttachedFiles);
            this.groupTaskTemplate.Controls.Add(this.label1);
            this.groupTaskTemplate.Controls.Add(this.textTaskNotes);
            this.groupTaskTemplate.Controls.Add(this.checkBox1);
            this.groupTaskTemplate.Location = new System.Drawing.Point(0, 0);
            this.groupTaskTemplate.Name = "groupTaskTemplate_" + taskId;
            this.groupTaskTemplate.Size = new System.Drawing.Size(503, 187);
            this.groupTaskTemplate.TabIndex = 1;
            this.groupTaskTemplate.TabStop = true;
            this.groupTaskTemplate.Visible = true;
            this.groupTaskTemplate.Tag = taskId;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(7, 2);
            this.checkBox1.Name = "checkTaskSelected_" + taskId;
            this.checkBox1.Size = new System.Drawing.Size(83, 17);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "Select Task";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Tag = taskId;
            // 
            // textTaskNotes
            // 
            this.textTaskNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textTaskNotes.Location = new System.Drawing.Point(210, 82);
            this.textTaskNotes.Multiline = true;
            this.textTaskNotes.Name = "textTaskNotes_" + taskId;
            this.textTaskNotes.Size = new System.Drawing.Size(283, 64);
            this.textTaskNotes.TabIndex = 1;
            this.textTaskNotes.Tag = taskId;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(207, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Task notes:";
            this.label1.Tag = taskId;
            // 
            // checkedListAttachedFiles
            // 
            this.checkedListAttachedFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkedListAttachedFiles.FormattingEnabled = true;
            //this.checkedListAttachedFiles.Items.AddRange(new object[] {
            //"File1.jpg",
            //"SomethingScanned.pdf",
            //"some-other-file.pdf",
            //"something-else.pdf",
            //"scheme.xml"});
            this.checkedListAttachedFiles.Location = new System.Drawing.Point(6, 82);
            this.checkedListAttachedFiles.Name = "checkedListAttachedFiles_" + taskId;
            this.checkedListAttachedFiles.Size = new System.Drawing.Size(198, 64);
            this.checkedListAttachedFiles.TabIndex = 3;
            this.checkedListAttachedFiles.Tag = taskId;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Attached files:";
            this.label2.Tag = taskId;
            // 
            // btnDeleteFile
            // 
            this.btnDeleteFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeleteFile.Location = new System.Drawing.Point(7, 152);
            this.btnDeleteFile.Name = "btnDeleteFile_" + taskId;
            this.btnDeleteFile.Size = new System.Drawing.Size(47, 23);
            this.btnDeleteFile.TabIndex = 5;
            this.btnDeleteFile.Text = "Delete";
            this.btnDeleteFile.UseVisualStyleBackColor = true;
            this.btnDeleteFile.Tag = taskId;
            // 
            // btnAddFile
            // 
            this.btnAddFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddFile.Location = new System.Drawing.Point(60, 152);
            this.btnAddFile.Name = "btnAddFile_" + taskId;
            this.btnAddFile.Size = new System.Drawing.Size(47, 23);
            this.btnAddFile.TabIndex = 6;
            this.btnAddFile.Text = "Add";
            this.btnAddFile.UseVisualStyleBackColor = true;
            this.btnAddFile.Tag = taskId;
            // 
            // checkTaskIsDone
            // 
            this.checkTaskIsDone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkTaskIsDone.AutoSize = true;
            this.checkTaskIsDone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkTaskIsDone.Location = new System.Drawing.Point(210, 155);
            this.checkTaskIsDone.Name = "checkTaskIsDone_" + taskId;
            this.checkTaskIsDone.Size = new System.Drawing.Size(87, 17);
            this.checkTaskIsDone.TabIndex = 7;
            this.checkTaskIsDone.Text = "Task is done";
            this.checkTaskIsDone.UseVisualStyleBackColor = true;
            this.checkTaskIsDone.Tag = taskId;
            // 
            // btnDeleteTask
            // 
            this.btnDeleteTask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeleteTask.Location = new System.Drawing.Point(420, 151);
            this.btnDeleteTask.Name = "btnDeleteTask_" + taskId;
            this.btnDeleteTask.Size = new System.Drawing.Size(74, 23);
            this.btnDeleteTask.TabIndex = 5;
            this.btnDeleteTask.Text = "Delete task";
            this.btnDeleteTask.UseVisualStyleBackColor = true;
            this.btnDeleteTask.Tag = taskId;
            // 
            // btnEditTask
            // 
            this.btnEditTask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEditTask.Location = new System.Drawing.Point(340, 151);
            this.btnEditTask.Name = "btnEditTask_" + taskId;
            this.btnEditTask.Size = new System.Drawing.Size(74, 23);
            this.btnEditTask.TabIndex = 5;
            this.btnEditTask.Text = "Save changes";
            this.btnEditTask.UseVisualStyleBackColor = true;
            this.btnEditTask.Tag = taskId;
            // 
            // btnViewFile
            // 
            this.btnViewFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnViewFile.Location = new System.Drawing.Point(113, 152);
            this.btnViewFile.Name = "btnViewFile_" + taskId;
            this.btnViewFile.Size = new System.Drawing.Size(47, 23);
            this.btnViewFile.TabIndex = 8;
            this.btnViewFile.Text = "View";
            this.btnViewFile.UseVisualStyleBackColor = true;
            this.btnViewFile.Tag = taskId;            
            // 
            // comboTask
            // 
            this.comboTask.FormattingEnabled = true;
            this.comboTask.Location = new System.Drawing.Point(8, 36);
            this.comboTask.Name = "comboTaskCategory_" + taskId;
            this.comboTask.Size = new System.Drawing.Size(196, 21);
            this.comboTask.TabIndex = 9;
            this.comboTask.Tag = taskId;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Category:";
            this.label3.Tag = taskId;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(207, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Task name:";
            this.label4.Tag = taskId;
            // 
            // textTaskName
            // 
            this.textTaskName.Location = new System.Drawing.Point(210, 36);
            this.textTaskName.Name = "textTaskName_" + taskId;
            this.textTaskName.Size = new System.Drawing.Size(283, 20);
            this.textTaskName.TabIndex = 11;
            this.textTaskName.Tag = taskId;

            /////////////////////////////
            Controls.Add(groupTaskTemplate);
            groupTaskTemplate.Paint += new PaintEventHandler(UserControl1_Paint);

        }

        private void UserControl1_Paint(object sender, PaintEventArgs e)
        {
            var rect = new Rectangle(0, 8, 502, 177);
            ControlPaint.DrawBorder(e.Graphics, rect, Color.Black, ButtonBorderStyle.Dashed);
        }

    }

    
}
